import numpy
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import decorator_filter_errors, get_backend
import h5py

logger = set_logger(__name__)

arr = get_backend()

def _loop_manager(func):
    """
    This function manages the size of the calculation according to available memory.
    If the array to construct is too big (too many elements and frequencies) it will raise an MemoryError
    If there are too many traces, this function will automatically create a for loop
    to reduce the amount of traces and concatenate the results later.
    Returns:

    """
    def wrapped_f(self, *args, **kwargs):
        #if self.backend == 'dask':
        #    return func(self, *args, **kwargs)
        from fracwave.solvers.parallel_suppport import aboutCPU
        from fracwave.utils.help_decorators import convert_size
        cpu = aboutCPU()
        freq = numpy.empty((self.ntraces, self.nfrequencies))
        n_bytes = 16  # Complex128 is composed of 16 bytes
        size_1_trace = self.nelements * self.nfrequencies * 3 * 3 * n_bytes
        free = int(cpu.mem_free * 0.5)  # Use 50% of the free memory to avoid MemoryError
        if size_1_trace > free:
            s = '{}\nNot possible to allocate {} array into memory.\n' \
                'Consider reducing the amount of elements or frequencies.'.format(
                cpu, convert_size(size_1_trace))
            logger.critical(s)
            raise MemoryError(s)
        max_amount_traces = self.ntraces * size_1_trace
        if max_amount_traces < free:
            freq = func(self, *args, **kwargs)
            return freq
        elif self.engine == 'loop':
            freq = func(self, *args, **kwargs)
            return freq
        else:
            opt_trace = free//size_1_trace
            s = '{}\nToo many traces to compute in the same array.\n' \
                'Not possible to allocate {} array into memory.\n' \
                'Please reduce the number of traces and try again, ' \
                'or switch to different solver.\n' \
                'Maximum amount of traces is {}'.format(cpu, convert_size(max_amount_traces), opt_trace)
            logger.critical(s)
            from tqdm.autonotebook import tqdm
            for i in tqdm(range(self.ntraces //opt_trace)):
                start = i*opt_trace
                end = start + opt_trace
                sl = slice(start, end)
                freq[sl] = func(self, sl=sl, *args, **kwargs)
            return freq
    return wrapped_f

#@_loop_manager
def forward_solver(self):
    """
    Function to manage the forward calculation for the electromagnetic wave response
    Args:
        save_steps: To prevent that the tensors are stored in RAM, save to disc
        overwrite: If the file has already an attribute, overwrite the attribute
        recalculate: Recalculate all ignoring if the file exists in file
    Returns:

    """
    global arr
    arr = get_backend()


    orient_tx = self.file_read('antenna/orient_Tx') if self.backend != 'torch' else self.file_read('antenna/orient_Tx').type(arr.complex128)
    orient_rx = self.file_read('antenna/orient_Rx') if self.backend != 'torch' else self.file_read('antenna/orient_Tx').type(arr.complex128)
    Tx = self.file_read('antenna/Tx')
    Rx = self.file_read('antenna/Rx')
    source = self.file_read('source/source')


    midpoint = self.file_read('mesh/geometry/midpoint')

    nx = self.file_read('mesh/geometry/nx') if self.backend != 'torch' else self.file_read('mesh/geometry/nx').type(arr.complex128)
    ny = self.file_read('mesh/geometry/nx') if self.backend != 'torch' else self.file_read('mesh/geometry/nx').type(arr.complex128)
    nz = self.file_read('mesh/geometry/nx') if self.backend != 'torch' else self.file_read('mesh/geometry/nx').type(arr.complex128)

    area = self.file_read('mesh/geometry/area')
    aperture = self.file_read('mesh/properties/aperture')
    epsilon = self.file_read('mesh/properties/elec_permeability')
    sigma = self.file_read('mesh/properties/elec_conductivity')

    mask_b = self.file_read('simulation/mask_elements')
    if self.backend == 'dask': mask_b = mask_b.compute()

    logger.info('Starting calculation for {} amount of traces. \n'
                #'Trace {} to {}.'.format(sl.indices(self._ntraces)[1] - sl.indices(self._ntraces)[0],
                #                         sl.indices(self._ntraces)[0],
                #                         sl.indices(self._ntraces)[1])
                )
    omega = 2 * numpy.pi * (10 ** 9) * self.file_read('source/frequency')
    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)
    # ------------------ Propagation to Fault
    logger.info('1) ------- Calculating propagation to fracture -------')
    #if not self._simulation['incoming_field'] or recalculate:
        # Outer product (t: traces, f: frequencies)
        # Ending shape needs to be (traces x elements x frequencies x 3 )
    source_matrix = arr.einsum('ti, f -> tfi',
                               orient_tx,
                               source)[:, None, :, :]  # should be 'tefi'
    if self.mode == 'propagation':
        incoming_field = propagation(self,
                                     omega=omega,
                                     src_vec=source_matrix,
                                     s_loc=Tx[:, None, :],
                                     # Fit to traces x elements x 3
                                     obs_loc=Rx[:, None, :]
                                     # Fit to traces x elements x 3
                                     )
    else:
        incoming_field = propagation(self,
                                     omega=omega,
                                     src_vec=source_matrix,
                                     s_loc=Tx[:, None, :],
                                     # Fit to traces x elements x 3
                                     obs_loc=midpoint[None, :, :]
                                     # Fit to traces x elements x 3
                                     )

        #if self.backend == 'dask':
        #    incoming_field = incoming_field.compute()

        # There are elements that have energy close to 0.
        # Mask all those elements saying that anything below 1% of the max energy goes to 0
        # They don't contribute to the overall energy at the fault
        #if self.filter_energy:
        #    logger.debug('Filtering according to max energy...')
        #    max = arr.max(arr.abs(incoming_field), axis=1)[:,
        #          None]  # Max value at the fracture for each trace, frequency and component
        #    cut = max * self._filter_percentage  # 1% of the max energy
        #    incoming_field[arr.abs(incoming_field) < cut] = 0
        #    #logger.debug('Filtering finished')

        #with h5py.File(self._filename, "r+") as f:
        #    if overwrite and 'simulation/incoming_field' in f:
        #        logger.info('Overwriting existing "simulation/incoming_field" information')
        #        del f['simulation/incoming_field']
        #    f.create_dataset(name='simulation/incoming_field', data=incoming_field)
        #    self._simulation['incoming_field'] = True
        #logger.info('"simulation/incoming_field" saved')
        #del incoming_field, source_matrix
        del source_matrix
    #else:
    #    logger.info('found previous "simulation/incoming_field"')
    if self.mode in ['propagation', 'incoming-field']:
        logger.info('++++ {} mode finished ++++'.format(self.mode))
        return incoming_field

    # ------------------- Electric components on plane
    logger.info('2) ------- Projecting incoming field to the tangential and normal '
                'vectors of the element -------')
    #if not self._simulation['projected'] or recalculate:
    tfx = orthogonal_projection(incoming_field,
                                nx)
    tfy = orthogonal_projection(incoming_field,
                                ny)
    nfz = orthogonal_projection(incoming_field,
                                nz)

        #with h5py.File(self._filename, "r+") as f:
        #    if overwrite and 'simulation/projected' in f:
        #        logger.info('Overwriting existing "simulation/projected" information')
        #        del f['simulation/projected']
        #    f.create_dataset(name='simulation/projected/tfx', data=tfx)
        #    f.create_dataset(name='simulation/projected/tfy', data=tfy)
        #    f.create_dataset(name='simulation/projected/nfz', data=nfz)
        #    self._simulation['projected'] = True
        #logger.info('"simulation/projected" saved')
        #del tfx, tfy, nfz
    #else:
    #    logger.info('found previous "simulation/projected"')
    #
    # ------------------- SourceEM to dipole. Polarization of electric field
    #logger.info(
    #    '3) ------- Calculating electric field to dipole using "{}" coefficients on fracture ------- '.format(
    #        self.mode))
    #if not self._simulation['induced_dipole'] or recalculate:
    kwargs_vector_fields = dict()
    if self.mode in ['reflection', 'full-reflection']:
        kwargs_vector_fields = dict(tangential_field_x=tfx,
                                    tangential_field_y=tfy,
                                    normal_field_z=nfz)

    induced_dipole = source_to_dipole(self,
                                      omega=omega,
                                      area=area,
                                      aperture=aperture,
                                      fracture_epsilon=epsilon,
                                      fracture_sigma=sigma,
                                      **kwargs_vector_fields
                                      )

        #with h5py.File(self._filename, "r+") as f:
        #    if overwrite and 'simulation/induced_dipole' in f:
        #        logger.info('Overwriting existing "simulation/induced_dipole" information')
        #        del f['simulation/induced_dipole']
        #    f.create_dataset(name='simulation/induced_dipole', data=induced_dipole)
        #    self._simulation['induced_dipole'] = True
        #logger.info('"simulation/induced_dipole" saved')
        #del induced_dipole
    #else:
    #    logger.info('found previous "simulation/induced_dipole"')

    if self.mode == 'dipole-response':
        logger.info('++++ {} mode finished ++++'.format(self.mode))
        return induced_dipole

    # ------------------- Propagation from dipole of fracture element to receiver
    logger.info('4) -------  Propagating electric field from fracture to receiver antenna ------- ')
    #if not self._simulation['receiver'] or recalculate:
    receiver_results = propagation(self,
                                   omega=omega,
                                   src_vec=induced_dipole,
                                   s_loc=midpoint[None, :, :],
                                   # Fit to traces x elements x 3
                                   obs_loc=Rx[:, None, :],
                                   # Fit to traces x elements x 3
                                   )

        #with h5py.File(self._filename, "r+") as f:
        #    if overwrite and 'simulation/receiver' in f:
        #        logger.info('Overwriting existing "simulation/receiver" information')
        #        del f['simulation/receiver']
        #    f.create_dataset(name='simulation/receiver', data=receiver_results)
        #    self._simulation['receiver'] = True
        #logger.info('"simulation/receiver" saved')
        #del receiver_results
    #else:
    #    logger.info('found previous "simulation/receiver"')

    # --------------- Summed response
    logger.info('5) -------  Summing response to antennas ------- ')
    #if not self._simulation['summed_response'] or recalculate:
    if self.mode == 'full-reflection':
        summed_response = arr.einsum('tefi -> tfi',
                                     receiver_results,
                                     )
    elif self.mode == 'reflection':
        summed_response = arr.einsum('tefi, ti -> tf',
                                     receiver_results,
                                     orient_rx)
        #del orx
    else:
        raise KeyError(self.mode)

        #with h5py.File(self._filename, "r+") as f:
        #    if overwrite and 'simulation/summed_response' in f:
        #        logger.info('Overwriting existing "simulation/summed_response" information')
        #        del f['simulation/summed_response']
        #    f.create_dataset(name='simulation/summed_response', data=summed_response)
        #    self._simulation['summed_response'] = True
        #logger.info('"simulation/summed_response" saved')
        #del summed_response
    #else:
    #    logger.info('found previous "simulation/summed_response"')

    logger.info('6) ------- End of Calculation ------- ')
    summed_response = summed_response.compute()
    return summed_response

def propagation(self, omega, src_vec, s_loc, obs_loc):
    """
    Function to compute the propagation of the electric field from a point dipole
    Args:
        omega: angular frequencies
        epsilon: Relative electrical permittivity
        sigma: Electrical conductivity
        src_vec: source_dipole
        s_loc: SourceEM location
        obs_loc: Observer location

    Returns:
        Electric field
    """
    k = self.wav_num[None, None, :]  # to fit to the dimensions of the tensor (traces x elements x frequency)
    # self.wavenumber(omega[None, None, :],
    #                epsilon, sigma)
    # k = np.broadcast_to(k1, (self._ntraces,self._nelements, self._nfrequencies) )
    # Unit vector pointing from the dipole to the point of observation
    R_vec = obs_loc - s_loc  # Distance vector (ntraces, nelements, 3)
    R = arr.linalg.norm(R_vec, axis=-1)[..., None]  # (ntraces, nelements, 1)
    RR = arr.einsum('tei, tej -> teij', R_vec, R_vec)[:, :, None, :,
         :]  # outer product vector (t: trace, e:element)

    # Addition of dimension to account for the frequency

    @decorator_filter_errors
    def Green():
        """
        Everything is rearranged to construct tensor (traces, elements, frequencies, (3x3; green))
        Returns:
            (traces, elements, frequencies, (3x3; green))
        """
        temp = (k ** 2) * (R ** 2)
        I = numpy.identity(3)  # Identity matrix
        if self.backend == 'dask': I = arr.from_array(I)
        if self.backend == 'torch': I = arr.from_numpy(I)
        return arr.divide(arr.exp(1j * k * R), 4 * numpy.pi * R)[:, :, :, None, None] * \
               ((1 + arr.divide(1j * k * R - 1, temp))[:, :, :, None, None] *
                arr.broadcast_to(I, (RR.shape[0], RR.shape[1], k.shape[-1], 3, 3)) +
                arr.divide(3 - 3j * k * R - temp, temp)[:, :, :, None, None] *
                arr.divide(RR, (R ** 2)[:, :, :, None, None]))

    efield = (omega[None, None, :, None] ** 2 * self.m0) * arr.sum(Green() * src_vec[..., None], axis=3)
    return efield

@decorator_filter_errors
def source_to_dipole(self,
                     omega,
                     area,
                     aperture,
                     fracture_epsilon,
                     fracture_sigma,
                     tangential_field_x=None,
                     tangential_field_y=None,
                     normal_field_z=None):
    """
    Calculate the induced dipole

    Args:
        omega: Angular frequency
        area: Area of fracture at each element
        aperture: Aperture of the fracture at each element
        fracture_epsilon: Electric permittivity of fracture at each element
        fracture_sigma: Electric conductivity of fracture at each element
        tangential_field_x: Projected incoming field
        tangential_field_y: Projected incoming field
        normal_field_z: Projected incoming field
    Returns:
        induced dipole

    """
    k_rock = self.wav_num[None, :]
    rock_complex_epsilon = self.complex_epsilon(omega,
                                                self.rock_epsilon,
                                                self.rock_sigma)[None, :]
    k2_frac = self.wavenumber(omega[None, :],  # To fit nelement x frequencies
                              fracture_epsilon[:, None],
                              fracture_sigma[:, None])
    fracture_complex_epsilon = self.complex_epsilon(omega[None, :],
                                                    fracture_epsilon[:, None],
                                                    fracture_sigma[:, None])

    if self.mode in ['reflection', 'full-reflection']:
        modulationTE, modulationTM = self.reflection(e1=rock_complex_epsilon,
                                                     e2=fracture_complex_epsilon,
                                                     k1=k_rock,
                                                     k2=k2_frac,
                                                     aperture=aperture[:, None])
        modulation = arr.einsum('tefi, ef -> tefi', tangential_field_x, modulationTE) + \
                     arr.einsum('tefi, ef -> tefi', tangential_field_y, modulationTE) + \
                     arr.einsum('tefi, ef -> tefi', normal_field_z, modulationTM)

    elif self.mode == 'transmission':
        modulation = self.transmission(k1=k_rock,
                                       k2=k2_frac,
                                       aperture=aperture[:, None])[None, ..., None]
    else:
        modulation = arr.ones((1,
                               fracture_epsilon.shape[0],
                               omega.shape[0],
                               3))  # traces, elements, frequencies, 3
    return arr.einsum('e, tefi -> tefi',
                      area*aperture,
                      arr.divide(modulation, 1j*omega[None, None, :, None]))

def orthogonal_projection(vector_in, projection_vector):
    """
    The projection of a vector $\overrightarrow{u}$ onto a plane is calculated by
    subtracting the component of $\overrightarrow{u}$ which is orthogonal to the plane
    from $\overrightarrow{u}$.
    Args:
        vector_in:
        projection_vector:

    Returns:

    """
    if projection_vector.ndim == 1:
        projection_vector = projection_vector[None, :]
    if vector_in.ndim == 1:
        vector_in = vector_in[None, :]
    # We need to resize the projection_vector to the shape the vector_in (apply case for tensor)
    # We know that the vector_in have shape of (traces x elements x frequencies x 3)
    if projection_vector.ndim != vector_in.ndim:
        projection_vector = projection_vector[None, :, None, :]

    divisor = arr.sum(projection_vector ** 2, axis=-1)
    dot_product = arr.sum(projection_vector * vector_in, axis=-1)
    return (dot_product / divisor)[..., None] * projection_vector  # equivalent to outer product
