import numpy
import datetime
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import get_backend, decorator_filter_errors
from tqdm.autonotebook import tqdm
import time
import numpy as np

logger = set_logger(__name__)

arr = get_backend()

import torch # TODO
def forward_solver(self, save_hd5=False, ):
    """
    Function to manage the forward calculation for the electromagnetic wave response
    Args:
        save_steps: To prevent that the tensors are stored in RAM, save to disc
        overwrite: If the file has already an attribute, overwrite the attribute
        recalculate: Recalculate all ignoring if the file exists in file
    Returns:

    """
    global arr
    arr = get_backend()
    logger.info('Starting calculation for {} amount of traces'.format(self.ntraces))
    start_time1 = time.time()

    if self.solve_for_fracture is not None:
        mask_element_fracture = self.file_read(f'geometry/fractures/{self.solve_for_fracture}')
        mask_b = self.file_read('simulation/mask_elements')
        m = arr.zeros(self.nelements, dtype=bool)
        m[mask_element_fracture] = True
        mask_b = mask_b * m[None]
        self.file_save('simulation/mask_elements', mask_b)
        del mask_b, m, mask_element_fracture

    if self._fast_calculation_incoming_field:
        get_mask_incoming_field(self)

    mask_frequencies = self.file_read('simulation/mask_frequencies')
    sum_mask_frequencies = sum(mask_frequencies) if self.backend != 'cupy' else arr.sum(mask_frequencies).item()
    if sum_mask_frequencies == 0:
        raise ValueError('No frequencies to calculate. Turn the self._fast_calculation_incoming_field to False')

    f = self.file_read('source/frequency')
    source = self.file_read('source/source')[mask_frequencies]
    omega = 2 * numpy.pi * (10 ** 9) * f[mask_frequencies]
    logger.info('Using {} frequencies out of {} possible frequencies'.format(sum_mask_frequencies, len(f)))
    del f
    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)

    if not save_hd5:
        orient_tx = self.file_read('antenna/orient_Tx') if self.backend != 'torch' else self.file_read('antenna/orient_Tx').type(arr.complex128)
        orient_rx = self.file_read('antenna/orient_Rx') if self.backend != 'torch' else self.file_read('antenna/orient_Rx').type(arr.complex128)
        tx = self.file_read('antenna/Tx')
        Rx = self.file_read('antenna/Rx')
        # source = self.file_read('source/source')
        if self.mode != 'propagation':
            midpoint = self.file_read('mesh/geometry/midpoint')
            if self._old_solver:
                nx = self.file_read('mesh/geometry/nx') if self.backend != 'torch' else self.file_read('mesh/geometry/nx').type(arr.complex128)
                ny = self.file_read('mesh/geometry/ny') if self.backend != 'torch' else self.file_read('mesh/geometry/ny').type(arr.complex128)
                nz = self.file_read('mesh/geometry/nz') if self.backend != 'torch' else self.file_read('mesh/geometry/nz').type(arr.complex128)

            area = self.file_read('mesh/geometry/area')
            aperture = self.file_read('mesh/properties/aperture')
            epsilon = self.file_read('mesh/properties/elec_permeability')
            sigma = self.file_read('mesh/properties/elec_conductivity')

            mask_b = self.file_read('simulation/mask_elements')
            if self.backend == 'dask': mask_b = mask_b.compute()

    if self._old_solver:
        if self._scaling and self.mode != 'propagation':
            # Calculate the scaling factor based on the element orientation
            txyz = self.file_read('antenna/Tx') if save_hd5 else tx
            exyz = self.file_read(f'mesh/geometry/midpoint') if save_hd5 else midpoint
            nxyz = self.file_read('mesh/geometry/nz')

            txyz = txyz[:, None, :]  # Expand dimensions to become 'tei'
            exyz = exyz[None, :, :]
            nxyz = nxyz[None, :, :]
            R = (exyz - txyz) / arr.linalg.norm(exyz - txyz, axis=-1)[..., None]
            if self.backend == 'torch':
                nxyz = nxyz.float()
            scaling = arr.absolute(arr.einsum('tei,tei->te', R, nxyz))
            del txyz, exyz, nxyz, R
            if save_hd5:
                self.file_save('simulation/scaling', scaling)
                del scaling
    else:
        # Here we need to calculate the reflection angle for calculating the scaling factor
        if self.mode != 'propagation':
            # Calculate the scaling factor based on the element orientation
            txyz = self.file_read('antenna/Tx') if save_hd5 else tx
            exyz = self.file_read(f'mesh/geometry/midpoint') if save_hd5 else midpoint
            nxyz = self.file_read('mesh/geometry/nz')  # if save_hd5 else nz

            txyz = txyz[:, None, :]  # Expand dimensions to become 'tei'
            exyz = exyz[None, :, :]

            # nxyz = nxyz[None, :, :]
            R = (exyz - txyz) / arr.linalg.norm(exyz - txyz, axis=-1)[..., None]

            if self.backend == 'torch':
                nxyz = nxyz.float()
            # Calculate the angle between the direction of propagation and the normal of elements
            cos_angle = arr.absolute(arr.einsum('tei,ei->te', R, nxyz))
            theta = arr.arccos(cos_angle)
            # calculate the projected orientation vector on the plane of the element. This will be the new polarization of the element

            antenna_orient = self.file_read('antenna/orient_Tx')  # if save_hd5 else orient_tx
            # dot_product = arr.einsum('ei, ti -> te', nxyz, antenna_orient)  # Both are unitary vectors already
            # Calculate the new orientation of the element
            new_orient_dipole = antenna_orient[:, None] - arr.einsum('ei, ti, ei -> tei', nxyz, antenna_orient, nxyz)

            self.file_save('simulation/theta', theta)
            self.file_save('simulation/new_orient_dipole', new_orient_dipole)
            del txyz, exyz, nxyz, R, cos_angle, antenna_orient
            if save_hd5:
                del theta, new_orient_dipole
                # self.file_save('simulation/scaling', scaling)
    # ------------------- Loop through all the traces
    sum_rep = []
    incomming_field = []
    # incomming_field_full = []
    if self.solve_for_profile is not None:
        #  This means that we are going to use specific profile to compute everything
        index_traces = self.file_read(f'antenna/profiles/{self.solve_for_profile}')
    else:
        index_traces = range(self.ntraces)

    if self._propagation_mode == 1: propagation = propagation1
    else: propagation = propagation2



    l = (pbar := tqdm(index_traces)) if self.backend != 'dask' else index_traces
    for index in l:
        pbar.set_description('1) Calculating propagation to fracture --------------------- Trace: {}'.format(index)) if self.backend != 'dask' else None

        #logger.debug('1) ------- Calculating propagation to fracture  ---------')
        #if not self._simulation['incoming_field'] or recalculate:
        source_matrix = arr.einsum('i, f -> fi',
                                   self.file_read('antenna/orient_Tx', sl=index) if save_hd5 else orient_tx[index],
                                   # self.file_read('source/source') if save_hd5 else source)[None]  # For extra dimension
                                   source)[None]  # For extra dimension
        if self.mode == 'propagation':
            inc_field = propagation(self,
                                    omega=omega,
                                    src_vec=source_matrix,
                                    s_loc=self.file_read('antenna/Tx', sl=index)[None],
                                    obs_loc=self.file_read('antenna/Rx', sl=index)[None],
                                    )
            # inc_field *= self._scaling_propagation  # TODO: Here is to scale everything to gprMax

            self.file_save(value='simulation/prop_full',
                           data=inc_field[0],
                           sl=(index, mask_frequencies)) if save_hd5 else incomming_field.append(inc_field[0])
        else:

            # inc_field = arr.zeros((self.nelements, sum_mask_frequencies, 3))
            # if self.backend == 'torch': inc_field = inc_field.type(arr.complex128)
            # else: inc_field = inc_field.astype(complex)

            mask = self.file_read('simulation/mask_elements', sl=index) if save_hd5 else mask_b[index]
            nelements = mask.sum()
            if self.backend == 'cupy': nelements = nelements.item()

            if not (True in mask):
                # Nothing to calculate
                if not save_hd5:
                    incomming_field.append(arr.zeros(self.nelements))
                    sum_rep.append(arr.zeros(sum_mask_frequencies))
                continue
            if self.backend == 'dask' and save_hd5: mask = mask.compute()
            inc_field = propagation(self,
                                          omega=omega,
                                          src_vec=source_matrix,
                                          s_loc=self.file_read('antenna/Tx', sl=index) if save_hd5 else tx[index],
                                          obs_loc=self.file_read('mesh/geometry/midpoint', sl=mask) if save_hd5 else midpoint[mask],
                                          )
            # inc_field *= self._scaling_propagation  # TODO: Here is to scale everything to gprMax
            incfied = arr.einsum('ef->e',
                                 arr.linalg.norm(inc_field, axis=-1))

            # arr.absolute(inc_field))  # Collapse the frequency axis and the components


            self.file_save('simulation/fracture_field', data=incfied,
                           sl=(index, mask)) if save_hd5 else incomming_field.append(incfied)

            # if self._fracture_field and not save_hd5:
            #     self.file_save('simulation/fracture_field', data=incfied, sl=(index, mask))

            del incfied

            # if self.mode =='incoming_field':
            self.file_save(value='simulation/incoming_field',
                           data=inc_field,
                           sl=(index, mask)) if save_hd5 else None#incomming_field_full.append(inc_field)

        if save_hd5: del inc_field
        if self.mode in ['propagation', 'incoming_field']:
            continue
        del source_matrix
        # ------------------- Electric components on plane
        #logger.info('2) ------- Projecting incoming field to the tangential and normal '
        #            'vectors of the element -------')
        # Here create new!
        # Todo

        if self._old_solver:
            def proj(field: str, index):
                assert field in ['x', 'y', 'z'], 'field must be one of "x", "y", "z"'
                if not save_hd5:
                    if field == 'x': geom = nx[mask]
                    if field == 'y': geom = ny[mask]
                    if field == 'z': geom = nz[mask]
                else:
                    geom = self.file_read(f'mesh/geometry/n{field}',sl=mask) if self.backend != 'torch' \
                        else self.file_read(f'mesh/geometry/n{field}', sl=mask).type(arr.complex128)

                val = orthogonal_projection(self.file_read('simulation/incoming_field', sl=(index, mask))
                                                  if save_hd5 else inc_field,
                                                  geom)
                del geom

                if self._scaling:
                    val = arr.einsum('efi, e->efi',
                                     val,
                                     self.file_read('simulation/scaling', sl=(index, mask)) if save_hd5 else scaling[index, mask],
                                     )
                if save_hd5:
                    if field == 'z':
                        na = 'nfz'
                    elif field == 'y' or field == 'x':
                        na = 'tf{}'.format(field)
                    self.file_save(value='simulation/projected/{}'.format(na),
                                   data=val,
                                   sl=(index, mask))
                    del val
                    return None
                else:
                    return val
            # Calulate the angles between the element and the incoming field

            pbar.set_description('2) Projecting incoming field to tan and norm vectors ------- Trace: {}'.format(index)) if self.backend != 'dask' else None

            x = proj('x', index)
            y = proj('y', index)
            z = proj('z', index)

        #logger.info('"simulation/projected" saved')
        #self._simulation['projected'] = True

        # ------------------- SourceEM to dipole. Polarization of electric field
        #logger.info(
        #    '3) ------- Calculating electric field to dipole using "{}" coefficients on fracture ------- '.format(
        #       self.mode))


        pbar.set_description('2) Calculating electric field to dipole -------------------- Trace: {}'.format(index)) if self.backend != 'dask' else None

        # mask = self.file_read('simulation/mask_elements', sl=index) if save_hd5 else mask
        # if self.backend == 'dask' and save_hd5: mask = mask.compute()

        if self.mode in ['reflection', 'full-reflection']:
            if self._old_solver:
                kwargs_vector_fields = dict(tangential_field_x=self.file_read('simulation/projected/tfx', sl=(index, mask)) if save_hd5 else x,
                                            tangential_field_y=self.file_read('simulation/projected/tfy', sl=(index, mask)) if save_hd5 else y,
                                            normal_field_z=self.file_read('simulation/projected/nfz', sl=(index, mask)) if save_hd5 else z)
            else:
                kwargs_vector_fields = dict(inc_field=self.file_read('simulation/incoming_field', sl=(index, mask)) if save_hd5 else inc_field,
                                            theta=self.file_read('simulation/theta', sl=(index, mask)) if save_hd5 else theta[index, mask],
                                            orient_dipole=self.file_read('simulation/new_orient_dipole', sl=(index, mask)) if save_hd5 else  new_orient_dipole[index, mask])

        else:
            kwargs_vector_fields = dict()
        in_dip = source_to_dipole(self,
                                  omega=omega,
                                  area=self.file_read('mesh/geometry/area', sl=mask) if save_hd5 else area[mask],
                                  aperture=self.file_read('mesh/properties/aperture', sl=mask) if save_hd5 else
                                  aperture[mask],
                                  fracture_epsilon=self.file_read(
                                      'mesh/properties/elec_permeability', sl=mask) if save_hd5 else epsilon[mask],
                                  fracture_sigma=self.file_read(
                                      'mesh/properties/elec_conductivity', sl=mask) if save_hd5 else sigma[mask],
                                  **kwargs_vector_fields)

        del kwargs_vector_fields
        self.file_save(value='simulation/induced_dipole',
                       data=in_dip,
                       sl=(index, mask)) if save_hd5 else None


        #self._simulation['induced_dipole'] = True
        #logger.info('"simulation/induced_dipole" saved')
        if save_hd5:
            del in_dip

        #else:
        #    logger.info('found previous "simulation/induced_dipole"')

        if self.mode == 'dipole-response':
            logger.info('++++ {} mode finished ++++'.format(self.mode))
            return self.file_read('simulation/induced_dipole') if save_hd5 else in_dip

            # ------------------- Propagation from dipole of fracture element to receiver
        #logger.info('4) -------  Propagating electric field from fracture to receiver antenna ------- ')
        #if not self._simulation['receiver'] or recalculate:


        pbar.set_description('3) Propagating electric field to receiver antenna ---------- Trace: {}'.format(index)) if self.backend != 'dask' else None

        rec_re = propagation(self,
                             omega=omega,
                             src_vec=self.file_read('simulation/induced_dipole',
                                                    sl=(index, mask)) if save_hd5 else in_dip,
                             s_loc=self.file_read('mesh/geometry/midpoint', sl=mask) if save_hd5 else midpoint[mask],
                             # Fit to traces x elements x 3
                             obs_loc=self.file_read('antenna/Rx', sl=index) if save_hd5 else Rx[index],
                             # Fit to traces x elements x 3
                             )
        self.file_save(value='simulation/receiver',
                       data=rec_re,
                       sl=(index, mask)) if save_hd5 else None

        #self._simulation['receiver'] = True
        #logger.info('"simulation/receiver" saved')
        if save_hd5:
            del rec_re
        #else:
        #    logger.info('found previous "simulation/receiver"')

        # --------------- Summed response
        #logger.info('5) -------  Summing response to antennas ------- ')
        #if not self._simulation['summed_response'] or recalculate:
        #summed_response = []
        #for index in (pbar := tqdm(range(self.ntraces))):
        pbar.set_description('4) Summing response to antennas ---------------------------- Trace: {}'.format(index)) if self.backend != 'dask' else None

        if self.mode == 'full-reflection':
            sum_r = arr.einsum('efi -> fi',
                               self.file_read('simulation/receiver', sl=(index, mask)) if save_hd5 else rec_re
                               )
            self.file_save(value='simulation/full_response',
                           data=sum_r,
                           sl=(index, mask_frequencies)) if save_hd5 else sum_rep.append(sum_r)
        elif self.mode == 'reflection':
            if save_hd5:
                orx = self.file_read('antenna/orient_Rx', sl=index) if self.backend != 'torch' else self.file_read('antenna/orient_Rx', sl=index).type(arr.complex64)
            else:
                orx = orient_rx[index]
            # sum_r = arr.einsum('efi, i -> f',
            #                              self.file_read('simulation/receiver', sl=(index, mask)) if save_hd5 else rec_re[mask],
            #                              orx) #TODO, also mask?
            if len(self.name_fractures) > 1:  # This means we are solving for multiple fractures, so we could separate them
                sr = arr.einsum('efi, i -> ef',
                                self.file_read('simulation/receiver', sl=(index, mask)) if save_hd5 else rec_re,
                                orx)
                sum_r = arr.zeros(sum_mask_frequencies) + 0j
                for i, n in enumerate(self.name_fractures):
                    if self.solve_for_fracture is not None and self.solve_for_fracture != n:
                        continue

                    if self.solve_for_fracture is not None and self.solve_for_fracture == n:
                        sr_trace = np.einsum('ef->f', sr)
                    else:
                        mask_element_fracture = self.file_read(f'geometry/fractures/{n}')
                        sr_trace = np.einsum('ef->f', sr[mask_element_fracture])

                    self.file_save(value='simulation/individual_fracture_response',
                                   data=sr_trace,
                                   sl=(index, i))

                    sum_r += sr_trace

                del sr, sr_trace

            else:

                sum_r = arr.einsum('efi, i -> f',
                                   self.file_read('simulation/receiver', sl=(index, mask)) if save_hd5 else rec_re,
                                   orx)
            del orx
            self.file_save(value='simulation/summed_response',
                           data=sum_r,
                           sl=(index, mask_frequencies)) if save_hd5 else sum_rep.append(sum_r)
        else:
            raise KeyError(self.mode)



        #lf._simulation['summed_response'] = True
        #logger.info('"simulation/summed_response" saved')

        pbar.set_description('6) End ----------------------------------------------------- Trace: {}'.format(index)) if self.backend != 'dask' else None
        #else:
        #    logger.info('found previous "simulation/summed_response"')
    if self.mode == 'propagation':
        logger.info('++++ "propagation" mode finished ++++')
        if save_hd5:
            incomming_field = self.file_read("simulation/prop_full", sl=slice(None, mask_frequencies))
            orient_rx = self.file_read('antenna/orient_Rx') if self.backend != 'torch' else self.file_read(
                'antenna/orient_Rx').type(arr.complex128)
        else:
            if self.backend == 'torch' or self.backend == 'numpy' or self.backend == 'cupy':
                incomming_field = arr.stack(incomming_field)
            if self.backend == 'dask':
                incomming_field = arr.stack(incomming_field).compute()
            self.file_save('simulation/prop_full', data=incomming_field, sl=(None, mask_frequencies))

        prop = arr.einsum('tfi, ti -> tf', incomming_field, orient_rx)


        prop_final = np.zeros((self.ntraces, self.nfrequencies)) + 0j # Make it complex #TODO. Having numpy here will make it difficult one day
                   # arr.concat(incomming_field) if arr.__name__ == 'torch' else arr.concatenate(incomming_field)
        if self.backend == 'cupy':
            prop_final = arr.asarray(prop_final)
        prop_final[:, mask_frequencies] = prop

        #if self.backend == 'torch': # prop_final = arr.from_numpy(prop_final)
        self.file_save('simulation/propagation', data=prop_final)
        return prop_final#arr.concat(incomming_field) if arr.__name__ == 'torch' else arr.concatenate(incomming_field)

    if self.mode == 'incoming_field':
        logger.info('++++ "incoming_field" mode finished ++++')
        if save_hd5:
            inc_final = np.zeros((self.ntraces, self.nelements, self.nfrequencies, 3)) + 0j
            inc_final[:, :, mask_frequencies, :] = self.file_read("simulation/incoming_field")

        else:
            logger.error('Not possible to return the complete "incoming_field", '
                         'please use save_hd5=True to return the complete field. '
                         'Returning fracture field instead')

            inc_final = arr.stack(incomming_field)
            self.file_save('simulation/fracture_field', data=inc_final)

        return inc_final

    if not save_hd5:
        if self.backend == 'torch' or self.backend == 'numpy' or self.backend == 'cupy':
            sum_rep = arr.stack(sum_rep)
            incomming_field = arr.stack(incomming_field)
        if self.backend == 'dask':
            sum_rep = arr.stack(sum_rep).compute()
            incomming_field = arr.stack(incomming_field).compute()

        # sum_rep_final = np.zeros((self.ntraces, self.nfrequencies)) + 0j
        # if self.backend == 'cupy':
        #     sum_rep_final = arr.asarray(sum_rep_final)

        # sum_rep_final[:, mask_frequencies] = sum_rep
        #if self.backend == 'torch': sum_rep_final = arr.from_numpy(sum_rep_final)
        for index in index_traces:
            mask = self.file_read('simulation/mask_elements', sl=index) if save_hd5 else mask_b[index]
            self.file_save('simulation/fracture_field', data=incomming_field[index], sl=(index, mask)) #if not self._fracture_field else None
        if self.mode == 'reflection':
            self.file_save('simulation/summed_response', data=sum_rep, sl=(None, mask_frequencies))
        elif self.mode == 'full-reflection':
            self.file_save('simulation/full_response', data=sum_rep, sl=(None, mask_frequencies))


    logger.info(' ------- End of Calculation. Elapsed time: {} ------- '.format(
        datetime.timedelta(seconds=time.time() - start_time1)))
    if self.mode == 'reflection':
        return self.file_read('simulation/summed_response')
    elif self.mode == 'full-reflection':
        return self.file_read('simulation/full_response')


def propagation1(self, omega, src_vec, s_loc, obs_loc):
    """
    Function to compute the propagation of the electric field from a point dipole
    Args:
        omega: angular frequency
        epsilon: Relative electrical permittivity
        sigma: Electrical conductivity
        src_vec: source_dipole
        s_loc: SourceEM location
        obs_loc: Observer location

    Returns:
        Electric field
    """
    k = self.wav_num
    R_vec = obs_loc - s_loc  # Distance vector (nelements, 3)
    if arr.__name__ == 'torch' and R_vec.type() == 'torch.LongTensor':
        R_vec = R_vec.type(dtype=arr.float)
    R = arr.linalg.norm(R_vec, axis=1)
    R_vec = R_vec / R[:,None]
    RR = arr.einsum('ei, ej -> eij', R_vec, R_vec)  # (nfrequencies, nelements, 3, 3)

    @decorator_filter_errors
    def Green():
        temp1 = arr.einsum('f, e -> ef', k, R) + 0j  # kR
        temp2 = arr.einsum('f, e -> ef', k ** 2, R ** 2) + 0j    # K2R2
        I = numpy.identity(3)  # Identity matrix
        if self.backend == 'dask':
            I = arr.from_array(I)
        elif self.backend == 'torch':
            I = arr.from_numpy(I)
        elif self.backend == 'cupy':
            I = arr.asarray(I)
        t2 = arr.divide(RR, (R ** 2)[:, None, None])
        first_term = arr.divide(arr.exp(-1j * temp1), 4 * numpy.pi * R[:, None])  # TODO: Here the negative 1j sign fixes everything
        if self._green_function_fields is None:
            second_term = arr.einsum('ef, ij -> efij', (1 + arr.divide(-1j * temp1 - 1, temp2)), I) # TODO: Here the negative 1j sign fixes everything
            t1 = arr.divide(3 + 3j * temp1 - temp2, temp2) # TODO: Here the negative 1j sign fixes everything
            # t1 = arr.divide(3 - 3j * temp1 - temp2, R[:,None]**2)
            # t2 = arr.divide(RR, (R ** 2)[:, None, None])
            if self.backend == 'torch': t2 = t2.type(arr.complex128)  # Operands can only operate when everything is complex
            third_term = arr.einsum('ef, eij -> efij', t1, t2)
            return arr.einsum('ef, efij->efij', first_term, (second_term + third_term))
        else:
            g = arr.zeros((R.shape[0], omega.shape[0], 3, 3)) + 0j
            I = I[None]
            if 'NF' in self._green_function_fields:
                second_term = arr.divide(1, temp2)
                third_term = -I + (3 * t2)
                gnf = np.einsum('ef, ef, eij->efij', first_term, second_term, third_term)
                g = g + gnf
            if 'IF' in self._green_function_fields:
                second_term = np.divide(-1j, temp1) # TODO: Here the negative 1j sign fixes everything
                third_term = I - (3 * t2)
                gif = np.einsum('ef, ef, eij->efij', first_term, second_term, third_term)
                g = g + gif
            if 'FF' in self._green_function_fields:
                third_term = I - t2
                gff = np.einsum('ef, eij->efij', first_term, third_term)
                g = g + gff
            return g

    efield = arr.einsum('f, efij, efi -> efj', omega**2 * self.m0**2, Green(), src_vec)
    return efield

@decorator_filter_errors
def propagation2(self, omega, src_vec, s_loc, obs_loc):
    """
    Function to compute the propagation of the electric field from a point dipole
    Args:
        omega: angular frequency
        epsilon: Relative electrical permittivity
        sigma: Electrical conductivity
        src_vec: source_dipole
        s_loc: SourceEM location
        obs_loc: Observer location

    Returns:
        Electric field
    """
    # r_vec = obs_loc - s_loc
    R_vec = obs_loc - s_loc  # Distance vector (nelements, 3)
    if arr.__name__ == 'torch' and R_vec.type() == 'torch.LongTensor':
        R_vec = R_vec.type(dtype=arr.float)

    R = arr.linalg.norm(R_vec, axis=1)[:,None]

    Rh = R_vec / R
    if arr.__name__ == 'torch':
        Rh = Rh.type(dtype=arr.double) + 0j
    else:
        Rh = Rh + 0j
    speed = omega / self.wav_num
    R = R[..., None]
    speed = speed[None, :, None]

    const = 1 / (4*np.pi*self.rce)[None, :, None]

    p = arr.einsum('rfi, rf->rfi', src_vec, arr.exp(-1j * arr.einsum('f, rfi ->rf', omega, R / speed)))
    p1 = arr.einsum('f, rfi->rfi', 1j * omega, p)
    p2 = arr.einsum('f, rfi->rfi', -omega ** 2, p)

    t01 = - arr.divide(p2, speed ** 2 * R)
    t02 = - arr.divide(p1, speed * R ** 2)
    t03 = arr.divide(arr.einsum('ef, ei->efi',
                                 3 * arr.einsum('efi, ei -> ef', p1, Rh),
                                 Rh),
                         speed * R ** 4)
    t04 = arr.divide(arr.einsum('ef, ei->efi',
                                arr.einsum('efi, ei -> ef', p2, Rh),
                                Rh),
                     speed ** 2 * R ** 3)
    t05 = arr.divide(arr.einsum('ef, ei->efi',
                                3 * arr.einsum('ei, efi -> ef', Rh, p),
                                Rh),
                         R ** 5)
    t06 = - arr.divide(p, R ** 3)

    if self._green_function_fields is None:
        return (t01 + t02 + t03 + t04 + t05 + t06) * const
    else:
        t_all = arr.zeros((R.shape[0], omega.shape[0], 3)) + 0j
        if 'NF' in self._green_function_fields:
            t_all = t_all + t05 + t06
        if 'FF' in self._green_function_fields:
            t_all = t_all + t01 + t04
        if 'IF' in self._green_function_fields:
            t_all = t_all + t02 + t03
        return t_all * const


def causality(self, omega, s_loc, obs_loc, mask):
    """
    Function to compute the causality of the electric field from a point dipole. Elements that are too close to the source
    1 wavelength, are filtered out as well.
    Args:
        omega: angular frequencies
        s_loc: SourceEM location
        obs_loc: Observer location
        mask: Mask of the simulation window
    Returns:
        mask with the elements that are outside the time window
    """
    if mask is not None:
        R_vec = obs_loc[mask] - s_loc  # apply mask to ignore elements that we don't want to include in the simulation
    else:
        R_vec = obs_loc - s_loc
    R = arr.linalg.norm(R_vec, axis=1)
    cond = (arr.einsum('e, f->ef', R, self.wav_num) / omega)
    cond[arr.isnan(cond)] = 0
    if self.backend == 'torch':
        cond, _ = arr.max(cond.abs(), axis=1)
        mask_b = mask.clone()
    else:
        cond = arr.max(arr.absolute(cond), axis=1)
        mask_b = mask.copy()
    #TODO: There are elements that are intersected by the antenna. We avoid being to close by setting the minimum distance to 1/4 wavelength
    causality_index = (cond <= (self.time_window * 1e-9)) & (R>=self.wavelength*0.5)  # To have from nano to seconds
    #logger.info('Part of the signal is not causal and is omitted')
    if self.backend == 'dask':
        causality_index = causality_index.compute()
    mask_b[mask] = mask[mask] * causality_index
    return mask_b

def _causality_all(self, omega, s_loc, obs_loc, mask): # TODO: Important for speed of calculations
    """

    Function to compute the causality of the electric field from all point dipoles to all elements.
    Elements that are too close to the source by 1 wavelength, are filtered out as well.
    Args:
        omega: angular frequencies
        s_loc: SourceEM location
        obs_loc: Observer location
        mask: Mask of the simulation window
    Returns:
        mask with the elements that are outside the time window
    """
    if mask is not None:
        R_vec = obs_loc[mask] - s_loc  # apply mask to ignore elements that we don't want to include in the simulation
    else:
        R_vec = obs_loc - s_loc
    R = arr.linalg.norm(R_vec, axis=1)
    cond = (arr.einsum('e, f->ef', R, self.wav_num) / omega)
    cond[arr.isnan(cond)] = 0
    if self.backend == 'torch':
        cond, _ = arr.max(cond.abs(), axis=1)
        mask_b = mask.clone()
    else:
        cond = arr.max(arr.absolute(cond), axis=1)
        mask_b = mask.copy()
    #TODO: There are elements that are intersected by the antenna. We avoid being to close by setting the minimum distance to 1/4 wavelength
    causality_index = (cond <= (self.time_window * 1e-9)) & (R>=self.wavelength*0.5)  # To have from nano to seconds
    #logger.info('Part of the signal is not causal and is omitted')
    if self.backend == 'dask':
        causality_index = causality_index.compute()
    mask_b[mask] = mask[mask] * causality_index
    return mask_b

def orthogonal_projection(vector_in, projection_vector):
    """
    The projection of a vector $\overrightarrow{u}$ onto a plane is calculated by
    subtracting the component of $\overrightarrow{u}$ which is orthogonal to the plane
    from $\overrightarrow{u}$.
    Args:
        vector_in:
        projection_vector:

    Returns:

    """
    divisor = arr.linalg.norm(projection_vector, axis=1)
    dot_product = arr.einsum('ei, efi -> ef', projection_vector, vector_in)
    return arr.einsum('ef,ei->efi', dot_product / divisor[:, None], projection_vector)  # equivalent to outer product

@decorator_filter_errors
def source_to_dipole(self,
                     omega,
                     area,
                     aperture,
                     fracture_epsilon,
                     fracture_sigma,
                     tangential_field_x=None,
                     tangential_field_y=None,
                     normal_field_z=None,
                     inc_field=None,
                     theta=None,
                     orient_dipole=None
                     ):
    """
    Calculate the induced dipole

    Args:
        omega: Angular frequency
        area: Area of fracture at each element
        aperture: Aperture of the fracture at each element
        fracture_epsilon: Electric permittivity of fracture at each element
        fracture_sigma: Electric conductivity of fracture at each element
        tangential_field_x: Projected incoming field
        tangential_field_y: Projected incoming field
        normal_field_z: Projected incoming field
    Returns:
        induced dipole

    """
    k2_frac = self.wavenumber(omega[None, :],  # To fit nelement x frequencies
                              fracture_epsilon[:, None],
                              fracture_sigma[:, None])


    if self.mode in ['reflection', 'full-reflection']:
        if self._old_solver:
            fracture_complex_epsilon = self.complex_epsilon(omega[None, :],
                                                            fracture_epsilon[:, None],
                                                            fracture_sigma[:, None])
            modulationTE, modulationTM  = self.reflection(e1=self.rce[None, :],
                                                         e2=fracture_complex_epsilon,
                                                         k1=self.wav_num[None, :],
                                                         k2=k2_frac,
                                                         aperture=aperture[:, None])
            polarized_electric_field = arr.einsum('efi, ef -> efi', tangential_field_x, modulationTE) + \
                     arr.einsum('efi, ef -> efi', tangential_field_y, modulationTE) #+ \
            # arr.einsum('efi, ef -> efi', normal_field_z, modulationTM)
        else:
            modulationTE = self.reflection_all(k1=self.wav_num[None, :],
                                               k2=k2_frac,
                                               aperture=aperture[:, None],
                                               theta=theta[:, None],)
            # modulation = arr.einsum('efi, ef -> efi', inc_field, modulationTE)

            if self.backend=='torch':
                orient_dipole = orient_dipole.type(arr.complex128)
            # polarized_electric_field = arr.einsum('efi,ei->efi', modulation, orient_dipole)

            polarized_electric_field = arr.einsum('efi, ef, ei -> efi', inc_field, modulationTE, orient_dipole)

    elif self.mode == 'transmission':
        raise NotImplementedError
        modulation = self.transmission(k1=self.wav_num[None, :],
                                       k2=k2_frac,
                                       aperture=aperture[:, None])[..., None]
    else:
        raise NotImplementedError
        modulation = arr.ones((fracture_epsilon.shape[0],
                               omega.shape[0],
                               3))
    # return arr.einsum('e, efi -> efi',
    #                   area*aperture,
    #                   arr.divide(modulation, 1j*omega[None, :, None]))
    if self._old_solver:
        return arr.einsum('e, efi -> efi',
                          area * aperture,
                          arr.divide(polarized_electric_field, 1j * omega[None, :, None]))
    else:
        return arr.einsum('e,efi->efi',
                          area*aperture, # Elemental volume
                          # (fracture_epsilon-1) * self.e0, # (Electric suceptibility = Relative permittivity - 1)
                          # (fracture_complex_epsilon-1) * self.e0, # (Electric susceptibility = Relative permittivity - 1)
                            arr.divide(polarized_electric_field, -1j*omega[None, :, None])) #* self._scaling_reflection
                            # TODO: Why do I need to multiply by a negative value here? Signal is upside down
                        # polarized_electric_field)
    # return arr.einsum('e,efi->efi',
    #                   area*aperture, # Elemental volume
    #                   # (fracture_epsilon-1) * self.e0, # (Electric suceptibility = Relative permittivity - 1)
    #                    # (Electric susceptibility = Relative permittivity - 1)
    #                     arr.divide(polarized_electric_field, 1j*omega[None, :, None])) #* self._scaling_reflection
    #                     # polarized_electric_field)



def get_mask_incoming_field(self, **kwargs):
    """
    Get the mask matrix for the elements that have almost zero energy.
    Incoming field at fracture
    Args:
        self:

    Returns:

    """
    logger.debug('+++Calculating the mask at the fracture field+++')
    f = self.file_read('source/frequency')
    source = self.file_read('source/source')

    mask_freq = get_mask_frequencies(source,
                                     threshold=kwargs.pop('threshold', 0.8),
                                     levels=kwargs.pop('levels', 10))
    if self.mode == 'propagation':
        return

    f = f[mask_freq]
    omega = 2 * numpy.pi * (10 ** 9) * f
    source = source[mask_freq]

    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)

    if self.solve_for_profile is not None:
        #  This means that we are going to use specific profile to compute everything
        index_traces = self.file_read(f'antenna/profiles/{self.solve_for_profile}')
    else:
        index_traces = range(self.ntraces)

    if self._propagation_mode == 1: propagation = propagation1
    else: propagation = propagation2

    l = (pbar := tqdm(index_traces)) if self.backend != 'dask' else index_traces
    for index in l:
        pbar.set_description('Calculating mask at fracture --------------------- Trace: {}'.format(
            index + 1)) if self.backend != 'dask' else None

        mask_fracture = self.file_read('simulation/mask_elements', sl=index)
        if self.apply_causality: # and self.backend != 'torch':
            pbar.set_description('Calculating causality --------------------- Trace: {}'.format(
                index + 1)) if self.backend != 'dask' else None
            mask_fracture = causality(self,
                                      omega,
                                      s_loc=self.file_read('antenna/Tx', sl=index),
                                      obs_loc=self.file_read('mesh/geometry/midpoint'),
                                      mask=mask_fracture)
            # here is possible to have all outside, so don't compute anymore.
            if not (True in mask_fracture):
                self.file_save(value='simulation/mask_elements',
                               data=mask_fracture,
                               sl=index)
                pbar.set_description('End mask calculation --------------------- Trace: {}'.format(
                    index + 1)) if self.backend != 'dask' else None
                continue

        if self.filter_energy:
            pbar.set_description('Filtering according to max energy --------------------- Trace: {}'.format(
                index + 1)) if self.backend != 'dask' else None

            source_matrix = arr.einsum('i, f -> fi',
                                       self.file_read('antenna/orient_Tx', sl=index),
                                       source)[None]  # For extra dimension

            inc_field = arr.zeros((self.nelements, len(omega), 3))
            if self.backend == 'torch':
                inc_field = inc_field.type(arr.complex128)
            else:
                inc_field = inc_field.astype(complex)

            inc_field[mask_fracture] = propagation(self,
                                          omega=omega,
                                          src_vec=source_matrix,
                                          s_loc=self.file_read('antenna/Tx', sl=((index),)),
                                          obs_loc=self.file_read('mesh/geometry/midpoint', sl=mask_fracture),
                                          )
            # incfield = arr.einsum('efi->e',
            incfield = arr.einsum('ef->e',
                                 arr.linalg.norm(inc_field, axis=-1))  # Collapse the frequency axis and the components
                                 # arr.absolute(inc_field))  # Collapse the frequency axis and the components


            # logger.debug('Filtering according to max energy...')
            # Max value at the fracture for each trace, frequency and component
            if self.backend == 'torch':
                max_, _ = arr.max(incfield, axis=-1)
                # max_ = max_[:, None]  # [:, None] to make it broadcastable
            else:
                max_ = arr.max(incfield, axis=-1)  # [:, None]
            cut = max_ * self._filter_percentage  # 1% of the max energy
            mask_fracture = (incfield >= cut) * mask_fracture
            if self.backend == 'dask':
                mask_fracture = mask_fracture.compute()
                # incoming_field = incoming_field.compute()

            self.file_save(value='simulation/mask_elements',
                           data=mask_fracture,
                           sl=index)

        if self.filter_fresnel:
            # midpoints = self.file_read('mesh/geometry/midpoint', sl=mask_fracture)
            # midpoints = self.file_read('mesh/geometry/midpoint')
            pass


        pbar.set_description('End mask calculation --------------------- Trace: {}'.format(
            index + 1)) if self.backend != 'dask' else None


def get_mask_frequencies(source, threshold=0.8, levels=10):
    """

    Args:
        source: Values of the source
        threshold: Value where to ignore all sources below this threshold
        levels: Index the rest of the

    Returns:

    """
    arr = get_backend()
    a = arr.absolute(source)
    if arr.__name__ == 'dask.array':
        pos_max_freq = arr.where(a == a.max())[0]
    else:
        pos_max_freq = arr.where(a == a.max())[0][0]

    mask_freq = a > threshold

    zones = arr.round(sum(mask_freq) / 2 / levels)  # Equally spaced levels to divide the frequencies
    mask_int = arr.zeros(len(mask_freq))

    if arr.__name__ == 'torch':# or isinstance(zones, torch.Tensor):
        # arr = torch
        zones = zones.type(arr.int)
        mask_int = mask_int.type(arr.bool)
    else:
        zones = zones.astype(int)
        mask_int = mask_int.astype(bool)

    for l in range(levels):
        l_c = pos_max_freq - zones * (l + 1) if pos_max_freq - zones * (l + 1) > 0 else 0
        r_1 = np.arange(l_c if arr.__name__ != 'cupy' else arr.asnumpy(l_c),
                        pos_max_freq - zones * l if arr.__name__ != 'cupy' else arr.asnumpy(pos_max_freq - zones * l),
                        l + 1)
        r_c = pos_max_freq + zones * (l + 1) if pos_max_freq + zones * (l + 1) < len(mask_freq) else len(mask_freq)
        r_2 = np.arange(pos_max_freq + zones * l if arr.__name__ != 'cupy' else arr.asnumpy(pos_max_freq + zones * l),
                        r_c if arr.__name__ != 'cupy' else arr.asnumpy(r_c),
                        l + 1)
        mask_int[r_1] = True
        mask_int[r_2] = True
    mask_freq *= mask_int
    logger.debug(f'Using {sum(mask_freq)} frequencies out of {len(a)} possible')
    return mask_freq

