import os.path
import dask.array
import numpy
import numpy as np
import torch
try:
    import cupy
    _cupy_flag=True
except:
    _cupy_flag=False
import h5py
import time
import datetime
from fracwave.utils.help_decorators import MetaData, generate_tree_h5_file, generate_tree_h5_file_attrs
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import (decorator_filter_errors, convert_size, convert_frequency, set_backend,
                                            check_and_convert_boolean_lists)
from nptyping import NDArray, Shape, Complex, Float, Int
from typing import Union, Optional, Any


logger = set_logger(__name__)
numpy.errstate(all="ignore")
numpy.seterr(divide='ignore', invalid='ignore')

arr = None
backend = 'numpy'
arr = set_backend(backend)


class FracEM(object):
    """
    Solver for the forward calculation of the Electromagnetic propagation problem

    """
    def __init__(self,
                 rock_epsilon: float = None,
                 rock_sigma: float = None,
                 units: str = 'SI',
                 mode: str = 'reflection',
                 engine: str = 'tensor_trace',
                 name: str = 'Effective-Dipole model'
                 ):
        """
        Args:
            rock_epsilon: Electric permittivity of the medium
            rock_sigma: Electrical conductivity of the rock medium
            units: System of units
                'SI' for international system: Seconds, meters, kilogram
                'CGS' for centimeter gram second
            mode: 'transmission' or 'reflection'
            propagation_terms:
            apply_causality:
            engine:
        """
        self._meta = MetaData(name)
        self._cluster = None
        self._units = units
        self._mode = mode
        self._rock_medium = {'rock_epsilon': rock_epsilon,  # Electric permittivity
                             'rock_sigma': rock_sigma}  # Electrical conductivity
        #self.propagation_terms = propagation_terms
        self.apply_causality = True
        self.filter_energy = True
        self._solver = None
        self._engine = None
        self.engine = engine
        self._filename = None
        self._filename_simulation = None

        self._nelements = None
        self._nfrequencies = None
        self._center_frequency = 0
        self._ntraces = None
        self._name_profiles = []
        self._solve_for_profile = None
        self._name_fractures = []
        self._solve_for_fracture = None
        self.time_window = 0

        self._wav_med = None  # k wavenumber for medium

        self._simulation = {'init': False,
                            'incoming_field': False,
                            'projected': False,
                            'induced_dipole': False,
                            'receiver': False,
                            'summed_response': False}
        self._backend = backend

        self._mask_elements = None  # Mask that tells the program to ignore this elements for each calculation
        self._filter_percentage = 0.01  # Percentage of the energy to be filtered
        self.gpu = False  # When using torch, this flag tells if the calculation is done on the GPU or not
        self._fast_calculation_incoming_field=True
        self._t0_correction = None
        self._dummy_time_response = False
        self._green_function_fields = None#['NF', 'IF', 'FF']  # Near field, Intermediate field, Far field
        self._propagation_mode = 1
        self.filter_fresnel = False
        # self._fracture_field = False
        self._mask_frequencies = True
        self._old_solver = True
        self._scaling = True


    @property
    def _scaling_propagation(self): # TOOO: Not working
        """
        When comparing the propagation to gprMax, the propagation needs to be scaled by a factor.
        Returns:
            Scaling factor that multiplies the propagation term
        """
        if self._propagation_mode == 1:
            return 0.0008611927984500647
        elif self._propagation_mode == 2:
            return 1.0822067576762372e-09
        else: raise AttributeError('Propagation mode not defined. Choose 1 or 2 for _propaation_mode')

    @property
    def _scaling_reflection(self): # TODO: Not working
        """
        When comparing to the reflection values from gprMax, the values are scaled by a factor
        Returns:

        """
        return 33520240938.62645

    @property
    def solve_for_profile(self): return self._solve_for_profile
    @solve_for_profile.setter
    def solve_for_profile(self, name):
        if name == 'all':
            self._solve_for_profile = None
        elif name in self.name_profiles:
            self._solve_for_profile = name
            logger.debug(f'Profile {name} set. Solving only for this profile')
        else:
            e = f'{name} not in {self.name_profiles}'
            logger.error(e)
            raise ValueError(e)

    @property
    def solve_for_fracture(self): return self._solve_for_fracture

    @solve_for_fracture.setter
    def solve_for_fracture(self, name):
        if name == 'all':
            self._solve_for_fracture = None
        elif name in self.name_fractures:
            self._solve_for_fracture = name
            logger.debug(f'Fracture {name} set. Solving only for this fracture')
        else:
            e = f'{name} not in {self.name_fractures}'
            logger.error(e)
            raise ValueError(e)
    @property
    def engine(self):
        """
        Engine that the solver uses to run the forward simulation
        Returns:
        """
        return self._engine

    @engine.setter
    def engine(self, engine):
        """
        Choose between ['loop', 'tensor', 'tensor_all, 'tensor_trace', 'tensor_dask]
        Returns:
        """
        options = ['loop', 'tensor', 'tensor_all', 'tensor_trace',
                   'tensor_trace_orig',
                   'tensor_dask', 'tensor_numba', 'tensor_opt', 'dummy',
                   'matlab']
        if engine in options:
            self._engine = engine
        else:
            e = 'Not possible to set {} as engine. \nOptions are: {}'.format(engine, options)
            logger.error(e)
            raise AttributeError(e)

    @property
    def nelements(self): return self._nelements
    @property
    def nfrequencies(self): return self._nfrequencies
    @property
    def ntraces(self): return self._ntraces
    @property
    def name_profiles(self): return self._name_profiles

    @property
    def name_fractures(self): return self._name_fractures

    @property
    def backend(self): return self._backend
    @backend.setter
    def backend(self, backend):
        global arr
        arr = set_backend(backend)
        self._backend = backend
        if self._backend == 'dask':
            from fracwave.solvers.parallel_suppport import ClusterDask, aboutCPU
            self._cluster = ClusterDask(processes=False)
            logger.info(self._cluster)
            self.gpu = False
            self._devices = aboutCPU()
            self.device = arr.device('cpu')
            logger.info(self._devices)
        elif self.backend == 'torch':
            self.gpu = False
            if self.gpu: # TODO: Still not implemented
                from fracwave.solvers.cuda_support import aboutCudaDevices
                self._devices = aboutCudaDevices()
                self.device = arr.device('cuda:{}'.format(arr.cuda.current_device()))
                logger.info(self._devices)
            else:
                from fracwave.solvers.parallel_suppport import aboutCPU
                self._devices = aboutCPU()
                self.device = arr.device('cpu')
                logger.info(self._devices)
        elif self.backend == 'cupy':
            self.gpu = True
            from fracwave.solvers.cuda_support import aboutCudaDevices
            self._devices = aboutCudaDevices()
            logger.info(self._devices)
        else:
            from fracwave.solvers.parallel_suppport import aboutCPU
            self._devices = aboutCPU()
            logger.info(self._devices)


    def __repr__(self):# Siemens = kg−1⋅m−2⋅s3⋅A2  # Farad = kg−1⋅m−2⋅s4⋅A2.
        t0 = f"{self._t0_correction:.2f}" if self._t0_correction is not None else None
        return f"""
            {self._meta.project_name + ' ' + self._meta.date}
            Calculating in: "{self.mode}" mode
            Using: "{self.units}" units
            
            Backend computation:
                engine: {self.engine}
                backend: {self.backend}
                gpu: {self.gpu}
            
            Rock properties:
                sigma: {self.rock_sigma} (S/m) 
                epsilon: {self.rock_epsilon} (F/m)
                velocity: {self.velocity:.5f} (m/ns)
                center_frequency: {convert_frequency(self._center_frequency * 1e9)}
                wavelength: {self.wavelength:.2f} (m)
            
            Calculating for:
                no. Profiles: {len(self.name_profiles)}
                no. Traces: {self.ntraces}
                no. Fractures: {len(self.name_fractures)}
                no. Elements (dipoles): {self.nelements}
                no. Frequencies: {self.nfrequencies}
            
            Simulation:
                Time window: {self.time_window:.2f} (ns)
                Depth penetration: {self.penetretation_depth:.2f} (m) 
                    or for single hole: {self.penetretation_depth*0.5:.2f} (m)
                Time zero correction: {t0} ns to remove
                """

    @property
    def rock_epsilon(self): return self._rock_medium['rock_epsilon']
    @property
    def rock_sigma(self): return self._rock_medium['rock_sigma']
    @rock_epsilon.setter
    def rock_epsilon(self, rock_epsilon):
        self._rock_medium['rock_epsilon'] = rock_epsilon if isinstance(rock_epsilon, (float,int)) \
            else logger.error('{}({}) needs to be a float'.format(rock_epsilon, type(rock_epsilon)))

    @rock_sigma.setter
    def rock_sigma(self, rock_sigma):
        self._rock_medium['rock_sigma'] = rock_sigma if isinstance(rock_sigma, (float,int)) \
            else logger.error('{}({}) needs to be a float'.format(rock_sigma, type(rock_sigma)))

    @property
    def velocity(self):
        """Velocity of the wave in the medium (m/ns)"""
        return self.c0 * 1e-9 / np.sqrt(self.rock_epsilon) if self.rock_epsilon is not None else 0

    @property
    def penetretation_depth(self):
        """Penetration depth according """
        return self.velocity * self.time_window

    @property
    def wavelength(self):
        """"""
        return self.velocity/self._center_frequency if self._center_frequency > 0 else 0
    @property
    def units(self):
        """
        'SI' for international system: Seconds, meters, kilogram
        'CGS' for centimeter gram seconds 'CGS'
        """
        return self._units

    @units.setter
    def units(self, units):
        """ SI or CGS
        Args:
            units: ['SI', 'CGS']
        """
        if units in ['SI', 'CGS']:
            self._units = units
        else:
            logger.error("'{}' not recognized as unit system, use 'SI' or 'CGS'".format(units))

    @property
    def mode(self):
        """ ['transmission', 'reflection', 'full-reflection', 'incoming_field', 'dipole_response', 'propagation'] """
        return self._mode

    @mode.setter
    def mode(self, mode):
        """
        ['transmission', 'reflection', 'full-reflection', 'incoming_field', 'dipole_response', 'propagation']
        Args:
            mode: ['transmission', 'reflection', 'full-reflection', 'incoming_field', 'dipole_response', 'propagation']
        Returns:

        """
        if mode in ['transmission', 'reflection', 'full-reflection', 'incoming_field', 'dipole_response', 'propagation']:
            self._mode = mode
        else:
            print('Not possible to set {} as mode'.format(mode))
            logger.error('Not possible to set {} as mode'.format(mode))

    @property
    def c0(self):
        """speed of light in free space"""
        if self.units == 'SI': return 299_792_458  # (m/s)
        elif self.units == 'CGS': return 0.299_792_458  # (m/ns)

    @property
    def m0(self):
        """Magnetic permeability in vacumm (\mu)"""
        if self.units == 'SI': return 4 * numpy.pi * 10 ** -7  # (Henries per meter - H/m) (H - 1 kg⋅m2⋅s−2⋅A−2)
        elif self.units == 'CGS': return 1  # In CGS is unitary (Unitless)

    @property
    def e0(self):
        """
        Absolute electric permittivity in vacuum \epsilon
        """
        if self.units == 'SI':
            one = arr.tensor(1) if self.backend == 'torch' else 1
            return arr.divide(one, self.m0 * self.c0 ** 2)  # (Farads per meter) (F/m)
        elif self.units == 'CGS': return 1  # Unitless

    @property
    def wav_num(self):
        if self._wav_med is None:
            raise AttributeError('Rock epsilon and Rock Sigma not set')
        return self._wav_med

    @wav_num.setter
    def wav_num(self, k): self._wav_med = k

    def open_file(self, filename=None):
        """
        Args:
            filename: location of the .h5 file with the previous loaded mesh geometry, antenna, source
        Returns:
        """
        if filename is None:
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'

        try:
            with h5py.File(filename, "r") as f:  # Read
                if self._mode != 'propagation':
                    self._nelements = f['mesh'].attrs['nelements']
                    _name_fractures = np.asarray(list(f['mesh/geometry/fractures'].keys()))
                    order = [f[f'mesh/geometry/fractures/{n}'].attrs['order'] for n in _name_fractures]
                    self._name_fractures = _name_fractures[np.argsort(order)].tolist()
                else:
                    logger.info('Propagation mode is selected. Geometry is not needed')

                self._nfrequencies = f['source'].attrs['nfreq']
                self.time_window = f['source'].attrs['time_window']
                self._center_frequency = f['source'].attrs['center_frequency']

                self._ntraces = f['antenna'].attrs['ntraces']

                _name_profiles = np.asarray(list(f['antenna/profiles'].keys()))
                orderp = [f[f'antenna/profiles/{n}'].attrs['order'] for n in _name_profiles]
                self._name_profiles = _name_profiles[np.argsort(orderp)].tolist()

            self._filename = filename
            self._filename_simulation = filename.replace('.', '_simulation.')
            logger.info('File correctly loaded. Ready to start.\n {}'.format(self.__repr__()))
            return self.__repr__()
        except KeyError as e:
            logger.error('File is lacking some initial parameters', exc_info=e)
            raise KeyError(e)

    def load_hdf5(self, filename=None):
        """
        Args:
            filename: location of the .h5 file to load the mesh geometry and the source receiver geometry of the antenas
        Returns:

        """
        logger.info('Loading file...')
        _ = self.open_file(filename)
        with h5py.File(self._filename_simulation, "r+") as f:
            if 'simulation' in f:
                self._simulation['init'] = True
                self.units = f['simulation'].attrs['units']
                self.mode = f['simulation'].attrs['mode']
                self.engine = f['simulation'].attrs['engine']
                self.rock_epsilon = f['simulation'].attrs['rock_epsilon']
                self.rock_sigma = f['simulation'].attrs['rock_sigma']

                logger.info('"simulation" attributes found. Calculations in {} units'.format(self.units))
                if 'simulation/propagation1' in f:
                    self._simulation['propagation1'] = True
                    logger.info('"simulation/propagation1" found: Propagation of EM wave from antenna to fault')
                    if 'simulation/projected' in f:
                        self._simulation['projected'] = True
                        logger.info('"simulation/projected" found: Projected vector components to '
                                    'local coordinates. "nx,ny,nz"')
                        if 'simulation/polarization' in f:
                            self._simulation['polarization'] = True
                            logger.info('"simulation/polarization" found: Polarized wave '
                                        'according to {} mode'.format(self.mode))
                            if 'simulation/dipole' in f:
                                self._simulation['dipole'] = True
                                logger.info('"simulation/dipole" found: EM wave that has been propagated, '
                                            'polarized and modulated to be a dipole')
                                if 'simulation/propagation2' in f:
                                    self._simulation['propagation2'] = True
                                    logger.info('"simulation/propagation2" found: EM wave that has been propagated, '
                                                'from the fracture to the receiver antenna')
                                else:
                                    logger.warning('"simulation/propagation2" missing. '
                                                   'Run "forward_solver" to continue calculation')
                            else:
                                logger.warning('"simulation/dipole" missing. '
                                               'Run "forward_solver" to continue calculation')
                        else:
                            logger.warning('"simulation/polarization" missing. '
                                           'Run "forward_solver" to continue calculation')
                    else:
                        logger.warning('"simulation/projected" missing. Run "forward_solver" to continue calculation')
                else:
                    logger.warning('"simulation/propagation1" missing. Run "forward_solver" to continue calculation')
            else:
                logger.warning('No previous simulation found')
            if 't0_correction' in f:
                self._t0_correction = f['t0_correction'].attrs['samples']
                logger.debug('Time zero correction loaded')
            else:
                logger.warning('No previous time zero correction found')
        print(self.__repr__())

    @decorator_filter_errors
    def complex_epsilon(self, omega: float, epsilon: float, sigma: float) -> float:
        """
        Complex electric permittivity: This can be expressed as a complex number.
        This accounts for the coefficient of attenuation that represents the part of the EM wave
        energy that is irreversibly converted into heat.
        Args:
            omega: Angular frequency
            epsilon: Relative electrical permittivity
            sigma: Electrical conductivity

        Returns:

        """
        if self.backend == 'torch':
            sigma = arr.tensor(sigma)
        # return self.e0 * epsilon + 1j * arr.divide(sigma, omega)   # According to slob 2010
        # return self.e0 * epsilon - arr.divide(1j * sigma, omega)   # According to slob 2010  # TODO: To have it working is positive
        if self._old_solver:
            # i = 1j
            i = -1j # TODO: lets see if its work
        else:
            i = -1j
        return self.e0 * epsilon + arr.divide(i * sigma, omega)   # According to slob 2010  # TODO: To have it working is positive

    @decorator_filter_errors
    def wavenumber(self, omega, epsilon, sigma):
        """
        Complex wavenumber computation
        Args:
            omega: Angular frequency
            epsilon: Relative electrical permittivity
            sigma: Electrical conductivity
        Returns:
            k

        """
        if self.units == 'SI':
            return omega * arr.sqrt(self.m0 * self.complex_epsilon(omega, epsilon, sigma))
        elif self.units == 'CGS':
            sigma = sigma / 9
            numerator = arr.sqrt(1 + (4 * numpy.pi * sigma / (omega * epsilon)) ** 2)
            return arr.sqrt(epsilon) * (omega / self.c0) * (
                    arr.sqrt(arr.divide(numerator + 1, 2)) + 1j * arr.sqrt(arr.divide(numerator - 1, 2)))

    def file_read(self, value: str, sl: slice = slice(None)):
        """
        Open the saved file and return the desired value
        Args:
            value: 'value'
            sl: slices. An slice for each dimension needs to be passed as a tuple
        Returns:
            array
        """
        v = []
        def get_value(name, obj):
            if value in name:
                v.append(name)

        def find_exact_string(string_to_find, list_of_strings):
            exact_matches = [s for s in list_of_strings if s == string_to_find]
            return exact_matches

        for file in [self._filename, self._filename_simulation]:
            with h5py.File(file, "r") as f:
                f.visititems(get_value)
                if len(v) == 0:
                    continue
                if len(v) > 1:
                    e = 'Several values found with similar name: {}\n{}'.format(value, v)
                    v1 = find_exact_string(value, v)
                    if len(v1) == 1:
                        logger.warning(e + '\nUsing: {}'.format(v1[0]))
                        v = v1
                    else:
                        logger.error(e)
                        raise AttributeError(e)
                if isinstance(sl, tuple):
                    # check if inside the tuple are boolean arrays
                    sl_new = ()
                    for s in sl:
                        sl_new += (check_and_convert_boolean_lists(s),)
                else:
                    sl_new = check_and_convert_boolean_lists(sl)

                val = f[v[0]][sl_new]
            if self.backend == 'torch':
                val = arr.from_numpy(val)  # .to(self.device)
            elif self.backend == 'dask':
                val = arr.from_array(val)
            elif self.backend == 'cupy':
                val = arr.asarray(val)
            return val
        e_message = 'Value: "{}" not found in files {}'.format(value, [self._filename_simulation, self._filename])
        logger.error(e_message)
        raise AttributeError(e_message)

    def file_save(self, value: str, data, sl: slice = slice(None), overwrite=False):
        """
        Save the data in the file
        Args:
            value: 'value'
            data: data to be saved
            sl: slices. An slice for each dimension needs to be passed as a tuple
        """
        if self.backend == 'dask' and not isinstance(data, numpy.ndarray):
            data2 = data.compute()
        elif self.backend == 'cupy' and not isinstance(data, numpy.ndarray):
            data2 = data.get()
        elif self.backend == 'torch' and not isinstance(data, numpy.ndarray):
            # data2 = arr.from_numpy(data)
            data2 = data.numpy()
        else:
            data2 = data.copy()
        with h5py.File(self._filename_simulation, "r+") as f:
            if value in f:
                if overwrite:
                    del f[value]
                    logger.info(f'Overwriting data in field {value}')
                    f.create_dataset(name=value, data=data2)
                if isinstance(sl, tuple):
                    # check if inside the tuple are boolean arrays
                    sl_new = ()
                    for s in sl:
                        sl_new += (check_and_convert_boolean_lists(s),)
                else:
                    sl_new = check_and_convert_boolean_lists(sl)

                if f[value][sl_new].shape != data2.shape:
                    logger.error('{}. Shape of data does not match with the shape '
                                 'of the data in the file. {} != {}'.format(value, f[value][sl_new].shape, data2.shape))
                    return
                f[value][sl_new] = data2
            else:
                f.create_dataset(name=value, data=data2)
            #logger.info('"{}" saved'.format(value))

    def file_read_attribute(self, value: str):
        """
        Check for the attribute value of the different classes
        Args:
            value:

        Returns:

        """
        a = []  # Full path to the attribute
        k = []  # key name of the attribute
        p = []  # Path without attribute name
        def attribute_unique(name, obj):
            for key, val in obj.attrs.items():
                full = name + '/' + key
                if value in full:
                    a.append(full)
                    k.append(key)
                    p.append(name)

        for file in [self._filename, self._filename_simulation]:
            with h5py.File(file, "r") as f:
                f.visititems(attribute_unique)
                if len(a) == 0:
                    continue
                if len(a) > 1:
                    e = 'Several values found with name: {}\n' \
                        '{}'.format(value, a)
                    logger.error(e)
                    # return
                    raise AttributeError(e)

                val = f[p[0]].attrs[k[0]]
            return val
        e_message = 'Attribute: "{}" not found in files {}'.format(value, [self._filename_simulation, self._filename])
        logger.error(e_message)
        return AttributeError(e_message)


    def forward_pass(self, overwrite=False, recalculate=False, sl=None, save_hd5=False, mask_frequencies=True,
                     ):
        """
        Function to manage the forward calculation for the electromagnetic wave response
        Args:
            overwrite: If the file has already an attribute, overwrite the attribute
            recalculate: Recalculate all ignoring if the file exists in file
            mask_frequencies: This is a boolean vector to mask the frequencies. #TODO
        Returns:

        """
        logger.info('\n+++++++++++++++++++++ Start Simulation +++++++++++++++++++++\n')
        start_time1 = time.time()

        if not self._simulation['init'] or overwrite:

            with h5py.File(self._filename_simulation, "a") as f:
                if 'simulation' in f:
                    del f['simulation']
                    simulation = f.create_group('simulation')
                else:
                    simulation = f.create_group('simulation')
                simulation.attrs['units'] = self.units
                simulation.attrs['mode'] = self.mode
                simulation.attrs['engine'] = self.engine
                simulation.attrs['rock_epsilon'] = self.rock_epsilon
                simulation.attrs['rock_sigma'] = self.rock_sigma
                logger.info('"simulation" attributes created')

                # if mask_frequencies is None or mask_frequencies is True or self._mask_frequencies:
                if self._mask_frequencies:
                    mask_frequencies = arr.zeros(self.nfrequencies, dtype=bool)
                    mask_frequencies[arr.where(arr.absolute(self.file_read('source/source'))/arr.absolute(self.file_read('source/source')).max() >
                                               1e-4)[0]] = True  # Don't Look at anything below 0.01% of the energy of the frequencies

                else:
                    mask_frequencies = arr.ones(self.nfrequencies, dtype=bool)

                nfrequencies = sum(mask_frequencies)
                if _cupy_flag:
                    if isinstance(nfrequencies, cupy.ndarray):
                        nfrequencies = nfrequencies.item()

                simulation.create_dataset(name='fracture_field',
                                          shape=(self.ntraces, self.nelements),
                                          dtype=numpy.float64,
                                          chunks=True) if self.mode != 'propagation' else None
                # Do we need this?

                if save_hd5:
                    simulation.create_dataset(name='incoming_field',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None
                    # # Projected incoming field. Do we need this?
                    simulation.create_dataset(name='projected/tfx',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None
                    simulation.create_dataset(name='projected/tfy',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None
                    simulation.create_dataset(name='projected/nfz',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None

                    # Induced field
                    simulation.create_dataset(name='induced_dipole',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None

                    # Propagation back to field
                    simulation.create_dataset(name='receiver',
                                              shape=(self.ntraces, self.nelements, nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True) if self.mode != 'propagation' else None

                if self.mode == 'propagation':
                    simulation.create_dataset(name='propagation',
                                              # shape=(self.ntraces, len(self.file_read('antenna/Rx')), nfrequencies, 3),
                                              shape=(self.ntraces, self.nfrequencies),
                                              dtype=numpy.complex64,
                                              chunks=True)
                    simulation.create_dataset(name='prop_full',
                                              # shape=(self.ntraces, len(self.file_read('antenna/Rx')), nfrequencies, 3),
                                              shape=(self.ntraces, self.nfrequencies, 3),
                                              dtype=numpy.complex64,
                                              chunks=True)

                else:
                    # Summed response
                    simulation.create_dataset(name='full_response',
                                              shape=(self.ntraces, self.nfrequencies, 3),
                                              dtype=numpy.complex64 if self.engine != 'dummy' else int,
                                              chunks=True)
                    simulation.create_dataset(name='summed_response',
                                              shape=(self.ntraces, self.nfrequencies),
                                              dtype=numpy.complex64 if self.engine != 'dummy' else int,
                                              chunks=True)
                # Mask for the frequencies
                if _cupy_flag:
                    if isinstance(mask_frequencies, cupy.ndarray):
                        mf =  mask_frequencies.get()
                    else:
                        mf = mask_frequencies
                else:
                    mf = mask_frequencies

                simulation.create_dataset(name='mask_frequencies',
                                          data=mf,
                                          chunks=True)

                # Mask for the elements
                simulation.create_dataset(name='mask_elements',
                                          data=arr.ones((self.ntraces, self.nelements), dtype=bool)
                                          if self.backend != 'cupy'
                                          else arr.ones((self.ntraces, self.nelements), dtype=bool).get(),
                                          chunks=None) if self.mode != 'propagation' else None  #TODO: chunks=True is changing the shape of the array

                if self.engine == 'dummy': # TODO: Deprecated (?)
                    # Dummy solutions of the travel times
                    simulation.create_dataset(name='dummy_solution/first_arrival',
                                              data=arr.zeros(self.ntraces),
                                              dtype=numpy.float64,
                                              chunks=True)
                    simulation.create_dataset(name='dummy_solution/time_vector',
                                              data=arr.zeros(self.nfrequencies),
                                              dtype=numpy.float64,
                                              chunks=True)
                    simulation.create_dataset(name='dummy_solution/time_response',
                                              data=arr.zeros(self.ntraces, self.nfrequencies),
                                              dtype=int,
                                              chunks=True)

                if self.name_fractures is not None and len(self.name_fractures) > 1:
                    simulation.create_dataset(name='individual_fracture_response',
                                              shape=(self.ntraces, len(self.name_fractures), nfrequencies),
                                              dtype=numpy.complex64,
                                              chunks=True)

            self._simulation['init'] = True

        dat = self.calculate(self, overwrite=overwrite, recalculate=recalculate, sl=sl, save_hd5=save_hd5)

        logger.info('\n+++++++++++++++++++++ End simulation +++++++++++++++++++++\n'
                    '\n+++++++++++++++++++++ Time elapsed: {} +++++++++++++++++++++\n'.format(
            datetime.timedelta(seconds=time.time() - start_time1)
        ))

        if self.mode == 'propagation':
            _ = numpy.save(self._filename.replace('.h5', '_propagation.npy'), dat)
            # return arr.einsum('tfi->tf', dat)
            return dat

        elif self.mode == 'incoming_field':
            _ = numpy.save(self._filename.replace('.h5', '_incoming_field.npy'), dat)
            return dat

        if self.solve_for_profile is not None:
            #  This means that we are going to use specific profile to compute everything
            index_traces = self.file_read(f'antenna/profiles/{self.solve_for_profile}')
            self._simulation['summed_response'] = True
            if self.mode == 'full-reflection':
                # self.file_save('simulation/full_response', dat, sl=index_traces)
                # logger.info('"simulation/full_response" saved')
                _ = numpy.save(self._filename.replace('.h5', '_full_response.npy'), dat)
                return dat
            else:
                # self.file_save('simulation/summed_response', dat, sl=index_traces)
                # logger.info('"simulation/summed_response" saved')
                _ = numpy.save(self._filename.replace('.h5', '_summed_response.npy'), dat)
                return dat
        else:
            self._simulation['summed_response'] = True
            if self.mode == 'full-reflection':
                # self.file_save('simulation/full_response', dat)
                # logger.info('"simulation/full_response" saved')
                _ = numpy.save(self._filename.replace('.h5', '_full_response.npy'), dat)
                return dat
            else:
                # self.file_save('simulation/summed_response', dat)
                # logger.info('"simulation/summed_response" saved')
                _ = numpy.save(self._filename.replace('.h5', '_summed_response.npy'), dat)
                return dat

    # @conjugate_fix
    @staticmethod
    def calculate(self, overwrite=False, recalculate=False, sl=None, save_hd5=False):
        """

        Returns:

        """
        if self.engine == 'tensor_all':
            from fracwave.solvers.fracEM.tensor_solver import forward_solver
            dat = forward_solver(self, overwrite, recalculate, sl)

        elif self.engine == 'tensor':
            from fracwave.solvers.fracEM.tensor_element_solver import forward_solver
            dat = forward_solver(self, overwrite, recalculate,)
        elif self.engine == 'tensor_trace':
            from fracwave.solvers.fracEM.tensor_element_solver_gnloop import forward_solver
            dat = forward_solver(self, save_hd5=save_hd5)
        elif self.engine == 'tensor_trace_orig':
            from fracwave.solvers.fracEM.tensor_element_solver_gnloop_orig import forward_solver
            dat = forward_solver(self, save_hd5=save_hd5)
        elif self.engine == 'tensor_opt':
            from fracwave.solvers.fracEM.tensor_element_solver_opt import forward_solver
            dat = forward_solver(self, save_hd5=save_hd5)
        elif self.engine == 'tensor_numba':
            from fracwave.solvers.fracEM.tensor_element_solver_numba import forward_solver
            dat = forward_solver(self)

        elif self.engine == 'tensor_dask':
            from fracwave.solvers.fracEM.tensor_solver_dask import forward_solver
            dat = forward_solver(self)

        elif self.engine == 'dummy':
            from fracwave.solvers.fracEM.dummy_geometry import forward_solver
            dat = forward_solver(self)

        elif self.engine == 'loop':


            # Using old alexis solver
            if self.mode == 'propagation':
                dipole_grid = np.asarray([0])
            else:
                midpoint = self.file_read('mesh/geometry/midpoint')
                dip = self.file_read('mesh/geometry/dip')
                azimuth = self.file_read('mesh/geometry/azimuth') - 90  # Because this solver takes into account the strike and not the dip_dir
                mask_az = azimuth < 0
                azimuth[mask_az] = azimuth[mask_az] + 360
                area = self.file_read('mesh/geometry/area')
                aperture = self.file_read('mesh/properties/aperture')
                elec_permeability = self.file_read('mesh/properties/elec_permeability')
                elec_conductivity = self.file_read('mesh/properties/elec_conductivity')
                dipole_grid = numpy.c_[midpoint[:, 0], midpoint[:, 1], midpoint[:, 2],
                dip, azimuth, area, aperture,
                elec_permeability, elec_conductivity]

            from fracwave.fracEM.core.loop_solvers import fracEM_SI
            solver = fracEM_SI()
            rep = []
            if self.backend == 'dask':
                from dask import delayed
            if self.solve_for_profile is not None:
                #  This means that we are going to use specific profile to compute everything
                index_traces = self.file_read(f'antenna/profiles/{self.solve_for_profile}')
            else:
                index_traces = range(self.ntraces)

            for index in index_traces:
                if self.backend == 'dask':
                    rep.append(
                        delayed(solver.forward_pass)(self.file_read('source/frequency'),
                                                     self.rock_epsilon,
                                                     self.rock_sigma,
                                                     self.file_read('source/source'),
                                                     self.file_read('antenna/Tx')[index, :],
                                                     self.file_read('antenna/orient_Tx')[index, :],
                                                     self.file_read('antenna/Rx')[index, :],
                                                     self.file_read('antenna/orient_Rx')[index, :],
                                                     dipole_grid,
                                                     self.mode)
                    )
                else:
                    # Force to compute with numpy, is not working for torch
                    self.backend = 'numpy'
                    start_time = time.time()
                    rep.append(solver.forward_pass(self.file_read('source/frequency'),
                                                   self.rock_epsilon,
                                                   self.rock_sigma,
                                                   self.file_read('source/source'),
                                                   self.file_read('antenna/Tx')[index, :],
                                                   self.file_read('antenna/orient_Tx')[index, :],
                                                   self.file_read('antenna/Rx')[index, :],
                                                   self.file_read('antenna/orient_Rx')[index, :],
                                                   dipole_grid,
                                                   mode=self.mode)
                               )
                    logger.debug(f"--- {time.time() - start_time} seconds for trace: {index}---")
            if self.backend == 'dask':
                dat = arr.compute(rep)
            else:
                dat = numpy.asarray(rep)

            # TODO: here the signal is mirrored so we need to conj
            dat = arr.conj(dat)
            self.file_save(value='simulation/prop_full',
                           data=dat)

        # if self._old_solver:
        #     dat = arr.conj(dat)
        #     if self.backend == 'torch':
        #         dat = dat.resolve_conj()
        return dat

    @staticmethod
    @decorator_filter_errors
    def _reflection(e1, e2, k1, k2, aperture):
        """
        function to compute the polarization of a dipole element (using effective reflection coefficients)
        Args:
            k1: Wavenumber of the rock medium
            k2: Wavenumber of the fracture
            aperture: Aperture of the fracture at each element

        Returns:
            modulation
        """
        tan_term = arr.tan(k2 * aperture)
        # Rte = arr.divide(1j * (k1 ** 2 / k2 - k2) * tan_term, 2 * k1 + 1j * (k1 ** 2 / k2 + k2) * tan_term)
        Rte = arr.divide(-1j * (k1 ** 2 / k2 - k2) * tan_term, 2 * k1 - 1j * (k1 ** 2 / k2 + k2) * tan_term)
        Rtm = arr.divide(-1j * (k1 ** 2 * e2 / k2 - k2 * e1 ** 2 / e2) * tan_term,
                         2 * k1 * e1 - 1j * (k1 ** 2 * e2 / k2 + k2 * e1 ** 2 / e2) * tan_term)
        return Rte, Rtm

    @staticmethod
    @decorator_filter_errors
    def reflection(e1, e2, k1, k2, aperture):
        rcTe = (k1 - k2) / (k1 + k2)
        rcTm = (k1 / k2) * rcTe
        exponential = np.exp(-2j * k2 * aperture)

        modulationTE = rcTe * (1 - exponential) / (1 - (rcTe ** 2) * exponential)
        modulationTM = rcTm * (1 - exponential) / (1 - (rcTm ** 2) * exponential)
        return modulationTE, modulationTM

    @staticmethod
    @decorator_filter_errors
    def _reflection_all(k1, k2, theta, aperture, mode='reflection'):
        """
        Reflection coefficients bassed on Sassen 2009
        """

        # t1 = k1 * arr.cos(theta)
        t1 = k1 * arr.cos(theta)
        # t2 = k2 * arr.cos(theta_t)
        t2 = arr.sqrt(k2 ** 2 - (k1 ** 2 * arr.sin(theta) ** 2))

        Rte = (t1 - t2) / (t1 + t2)

        Tte = 2*t1 / (t1 + t2)

        # t3 = k2**2*arr.cos(theta)

        # Rtm = (k1 * t2 - t3) / (k1 * t2 + t3)
        # Ttm = 2*k1*t2 / (t3 + k1 * t2)
        # Rte = (k1 * arr.cos(theta) - arr.sqrt(k2**2 - k1**2 * arr.sin(theta)**2)) / (k1 * arr.cos(theta) + arr.sqrt(k2**2 - k1**2 * arr.sin(theta)**2))
        theta_t = arr.arcsin(k1 * arr.sin(theta) / k2)
        exponential = np.exp(-2j * k2 * aperture * arr.cos(theta_t))
        if mode=='reflection':
            Reff = R12 + ((T12 *  T21 * R21 * exponential) / (1 - (R21 * R21 * exponential)))
            return Reff
        elif mode=='transmission':
            Teff = (T12 * T21 * (-1j * k2 * aperture)) / (1 - R21 * R21 * exponential)
            return Teff


        # modulationTE = Rte * (1 - exponential) / (1 - (Rte ** 2) * exponential)
        # return modulationTE
        #
        # R12 = (k1 - k2) / (k1 + k2)
        # R21 = (k2 - k1) / (k2 + k1)
        #
        # T12 = 2 * k1 / (k1 + k2)
        # T21 = 2 * k2 / (k1 + k2)
        #
        # exponential = np.exp(-2j * k2 * aperture)
        # if mode=='reflection':
        #     Reff = R12 + ((T12 *  T21 * R21 * exponential) / (1 - (R21 * R21 * exponential)))
        #     return Reff
        # elif mode=='transmission':
        #     Teff = (T12 * T21 * (-1j * k2 * aperture)) / (1 - R21 * R21 * exponential)
        #     return Teff

    def reflection_coefficients(self,k1, k2, theta=0):
        t1 = k1 * arr.cos(theta)
        t2 = arr.sqrt(k2 ** 2 - (k1 ** 2 * arr.sin(theta) ** 2))
        Rte = (t1 - t2) / (t1 + t2)
        return Rte

    @decorator_filter_errors
    def reflection_all(self, k1, k2, theta, aperture):
        """
        Reflection coefficients bassed on Sassen 2009
        Args:
            e1:
            e2:
            k1:
            k2:
            aperture:

        Returns:
        """
        Rte = self.reflection_coefficients(k1, k2, theta)
        theta_t = arr.arcsin(k1 * arr.sin(theta) / k2)

        exponential = np.exp(-2j * k2 * aperture * arr.cos(theta_t))
        modulationTE = Rte * (1 - exponential) / (1 - (Rte ** 2) * exponential)
        return modulationTE


    @staticmethod
    @decorator_filter_errors
    def transmission(k1, k2, aperture):
        """
        function to compute the polarization of a dipole element (using effective reflection coefficients)
        Args:
            k1: Wavenumber of the rock medium
            k2: Wavenumber of the fracture
            aperture: Aperture of the fracture at each element

        Returns:
            modulation
        """
        r21 = arr.divide(k1 - k2, k1 + k2)
        t12 = arr.divide(2 * k2, k1 + k2)
        t21 = arr.divide(2 * k1, k1 + k2)
        expn = arr.exp(-2j * aperture * k2)
        return arr.divide(t12 * t21 * expn, 1 - r21 * r21 * expn)

    def get_ifft(self, input_signal, pad: int = 0, apply_t0: bool = True):
        """

        Args:
            input_signal: Shape (traces x frequencies)
            pad: int specifying the amount of zeroes to pad at the end of the frequency solution to
                increase the sampling time.
            apply_t0: bool specifying if the t0 should be applied to the time vector


        Returns:
            time_response:
            time_vector:

        """
        if isinstance(input_signal, torch.Tensor):
            input_signal = input_signal.numpy()
        if _cupy_flag and isinstance(input_signal, cupy.ndarray):
            input_signal = input_signal.get()
        iterations = self.file_read_attribute('source/ntime')
        if pad != 0:
            iterations += pad

        time_response = np.real_if_close(np.fft.irfft(input_signal, axis=1, n=iterations))
        time_vector = np.linspace(0, self.file_read_attribute('source/time_window'), iterations)

        if self._t0_correction is not None and apply_t0:
            time_vector -= self._t0_correction
            logger.info(f'Applying time zero correction to data. Removing {self._t0_correction:.2f} ns from data')# == {samples_to_remove} samples')
        else:
            logger.info('No time zero correction')
        return time_response, time_vector


    def time_zero_correction(self, show_plot=True):
        """
        Correct for the delay included in the source function
        Returns:
            Number of samples to delete
        """
        import shutil
        from fracwave import DATA_DIR
        if os.path.isdir(DATA_DIR+'time_correction'):
            shutil.rmtree(DATA_DIR + 'time_correction')
            os.mkdir(DATA_DIR+'time_correction')
        else:
            os.mkdir(DATA_DIR + 'time_correction')
        copy_filename = shutil.copy2(self._filename, DATA_DIR + 'time_correction/file.h5')

        from fracwave import Antenna
        ant = Antenna(name='Time zero positions')

        # Lets make the distances based on the wavelength distance
        distances = np.asarray([self.wavelength * 2, self.wavelength *3, self.wavelength *4, self.wavelength *5])
        Rx = np.asarray([[d, 0, 0] for d in distances])
        Tx = np.zeros(Rx.shape)
        orient_tx = np.zeros(Rx.shape)
        orient_tx[:, -1] = 1
        orient_rx = np.zeros(Rx.shape)
        orient_rx[:, -1] = 1

        ant.set_profile(name_profile='TimeZero',
                        receivers=Rx,
                        transmitters=Tx,
                        orient_transmitters= orient_tx,
                        orient_receivers=orient_rx)

        ant.export_to_hdf5(copy_filename, overwrite=True)

        self.child = FracEM(name='Time zero correction')
        self.child.rock_epsilon = self.rock_epsilon
        self.child.rock_sigma = self.rock_sigma
        if self.engine == 'dummy':
            eng = 'tensor_trace'
            bck = 'torch'
        else:
            eng = self.engine
            bck = self.backend
        self.child.backend = bck
        self.child.engine = eng
        self.child.mode = 'propagation'
        self.child.filter_energy = False
        self.child.apply_causality = False
        self.child._fast_calculation_incoming_field = False

        self.child.open_file(copy_filename)

        freq = self.child.forward_pass(overwrite=True, recalculate=True, save_hd5=False, mask_frequencies=False)

        time_response, time_vector = self.child.get_ifft(freq, pad=1000)
        from fracwave.utils.help_decorators import pick_first_arrival
        time_response /= np.max(np.abs(time_response))
        pick = pick_first_arrival(time_response.T, threshold=0.01)

        time_pick = time_vector[pick[:, 1]]
        P = np.polyfit(distances, time_pick, deg=1)
        t0 = P[1] #np.round(P[1]).astype(int)

        # Here is a visual inspection of how is looking
        extent = (0, self.wavelength * 10)
        if show_plot:
            logger.info("Creating plot... If you don't want to display the plot then pass the argument 'show_plot=False'")
            import matplotlib.pyplot as plt
            from fracwave.utils.help_decorators import convert_meter
            fig, (ax1, ax2) = plt.subplots(2,1, figsize=(15,10))
            # distance_vector = time_vector * self.velocity
            for i, d in enumerate(distances):
                l = ax1.plot(time_vector, time_response[i, :], label=convert_meter(d) + f' ({i+2} λ)')
                color = l[0].get_color()
                ax2.plot(time_vector-t0, time_response[i, :], color=color, label=convert_meter(d) + f' ({i+2} λ)')

                for (ax, tv) in ((ax1, time_vector), (ax2, time_vector-t0)):
                    ax.set_xlabel('Time (ns) (10 λ)')
                    ax.set_ylabel('Amplitude')
                    ax.axvline(tv[pick[i, 1]], color=color, linestyle=':')
                    ax.set_xlim(min(extent) / self.velocity, max(extent) / self.velocity)
                    ax.grid()

                    ax3 = ax.twiny()
                    ax3.plot()
                    ax3.set_xlabel('Depth (m)')
                    ax3.axvline(d, color=color, linestyle='-')
                    ax3.set_xlim(min(extent), max(extent))

            ax1.set_title('Before t0')
            ax2.set_title('After t0 = {:.2f} ns'.format(t0))
            ax1.legend()
            ax2.legend()
            plt.tight_layout()
            plt.show()

        if t0 < 0:
            e = f'You have a problem with the source function time shift. ' \
                f'Try to add a delay to the source function and repeat. t0 = {t0:.2f} ns to remove. ' \
                f'Look at the plot to see the problem.'
            logger.error(e)
            # raise RuntimeError(e)
        self._t0_correction = t0
        with h5py.File(self._filename_simulation, "a") as f:
            if 't0_correction' in f:
                del f['t0_correction']
                correction = f.create_group('t0_correction')
            else:
                correction = f.create_group('t0_correction')
            correction.attrs['samples'] = t0
            logger.info(f'"t0_correction" attribute created with {t0:.2f} ns to remove')

        return freq, t0

    def plot(self,
             time_response: NDArray[Shape["* traces, * samples"], Complex],
             time_vector: NDArray[Shape["* samples"], Any],
             depth_vector: NDArray[Shape["* traces"], Any],
             xlim: Optional[Union[list, tuple]] = None,
             ylim: Optional[Union[list, tuple]] = None,
             clim: Optional[float] = None,
             two_way_time: Optional[bool] = True,
             vertical: Optional[bool] = False,
             figsize: Optional[tuple]=None):
        """

        Args:
            time_response:
            time_vector:
            depth_vector:
            xlim:
            ylim:
            clim:
            two_way_time:
            vertical:

        Returns:

        """
        import matplotlib.pyplot as plt
        distance = time_vector * self.velocity
        if two_way_time:
            distance *= 0.5
        traces, samples = time_response.shape
        t = np.arange(traces)
        s = np.arange(samples)

        if vertical and figsize is None:
            figsize = (7.15, 10.4)
        elif not vertical and figsize is None:
            figsize = (10.4, 7.15)

        fig, ax = plt.subplots(figsize=figsize)  #

        if clim is None:
            vmax = np.abs(time_response).max()  # /100
        else:
            vmax = clim

        if vertical:
            cax = ax.imshow(time_response,
                            aspect='auto',
                            cmap='seismic',
                            vmax=vmax,
                            vmin=-vmax,
                            extent=(distance[0], distance[-1], t[-1], t[0]),
                            interpolation='None'
                            )
        else:
            cax = ax.imshow(time_response.T,
                            aspect='auto',
                            cmap='seismic',
                            vmax=vmax,
                            vmin=-vmax,
                            extent=(t[0], t[-1], distance[-1], distance[0]),
                            interpolation='None'
                            )

        if vertical:

            axt = ax.twiny()
            axt.set_xlabel('Time (ns)')
            # axsa = ax.twinx()
            # axsa.set_ylabel('No. Samples')

            if xlim is not None:
                mask = [(distance <= np.max(xlim)) & (distance >= np.min(xlim))]
                # s = s[mask]
                tv = time_vector[mask]
                ax.set_xlim(xlim)
                if xlim[1] < xlim[0]:
                    tv = np.flip(tv)
                    # s = np.flip(s)
            else:
                # s = s
                tv = time_vector

            axt.set_xlim(tv[0], tv[-1])
            # ax.set_xlim(xlim)

            # axsa.set_ylim(s[-1], s[0])
            # axsa.spines.right.set_position(("axes", 1.15))
            ax.set_xlabel('Radial distance (m)')

            ax.set_ylabel('Traces')


            axbr = ax.twinx()
            axbr.set_ylabel('Borehole depth (m)')

            if ylim is not None:

                mask = [(depth_vector <= np.max(ylim)) & (depth_vector >= np.min(ylim))]
                dv = depth_vector[mask]
                t = t[mask]
                if ylim[0] > ylim[1]:
                    dv = np.flip(dv)
                    t = np.flip(t)
            else:
                dv = depth_vector
                t = t

            ax.set_ylim(t[-1], t[0])
            axbr.set_ylim(dv[-1], dv[0])
            ax.grid(axis='x', linestyle='--')

        else:
            axbr = ax.twiny()
            axbr.set_xlabel('Borehole depth (m)')
            if xlim is not None:

                mask = [(depth_vector <= np.max(xlim)) & (depth_vector >= np.min(xlim))]
                dv = depth_vector[mask]
                t = t[mask]
                if xlim[0] > xlim[1]:
                    dv = np.flip(dv)
                    t = np.flip(t)
            else:
                dv = depth_vector
                t = t

            ax.set_xlim(t[0], t[-1])
            axbr.set_xlim(dv[0], dv[-1])

            axt = ax.twinx()
            axt.set_ylabel('Time (ns)')
            # axsa = ax.twinx()
            # axsa.set_ylabel('No. Samples')

            if ylim is not None:
                mask = [(distance <= np.max(ylim)) & (distance >= np.min(ylim))]
                # s = s[mask]
                tv = time_vector[mask]
                ax.set_ylim(ylim)
                if ylim[0] < ylim[1]:
                    tv = np.flip(tv)
                    # s = np.flip(s)
            else:
                # s = s
                tv = time_vector

            axt.set_ylim(tv[-1], tv[0])
            # axsa.set_ylim(s[-1], s[0])
            # axsa.spines.right.set_position(("axes", 1.15))

            ax.set_xlabel('Traces')
            ax.set_ylabel('Radial distance (m)')
            ax.grid(axis='y', linestyle='--')
        axbr.grid(linestyle='--')
        ax.tick_params(axis='both', which='both', direction='out', length=5)
        axt.tick_params(axis='both', which='both', direction='out', length=5)
        # axsa.tick_params(axis='both', which='both', direction='out', length=5)
        axbr.tick_params(axis='both', which='both', direction='out', length=5)

        # ax.set_title('Single surface - constant aperture')
        plt.tight_layout(pad=0.5)  # , w_pad=0.2, h_pad=0.5)
        fig.colorbar(cax, ax=ax, label='Amplitude (-)', orientation='horizontal')
        # plt.show()
        # fig.save
        return fig, ax

    def get_freq_from_fracture(self, name_fracture: Union[int, list] = 'all'):
        """

        Args:
            name_fracture:

        Returns:

        """
        if name_fracture == 'all' or len(self.name_fractures) == 1:
            return self.file_read('simulation/summed_response')

        if isinstance(name_fracture, str):
            name_fracture = [name_fracture]
        mask_frequencies = self.file_read('simulation/mask_frequencies')
        freq = arr.zeros((self.ntraces, self.nfrequencies)) + 0j
        for i, n in enumerate(self.name_fractures):
            if n in name_fracture:
                sr = self.file_read('simulation/individual_fracture_response', sl=(slice(None), i))
                freq[:, mask_frequencies] += sr
        return freq

    def get_freq(self, name_fracture: Union[int, list] = 'all',
                 name_profile: Union[int, list] = 'all'):
        """

        Args:
            name_fracture:
            name_profile:

        Returns:

        """
        if (name_fracture == 'all' or len(self.name_fractures) == 1) and \
                (name_profile =='all' or len(self.name_profiles) == 1):
            if self.mode == 'propagation':
                return self.file_read('simulation/propagation')
            elif self.mode == 'full-reflection':
                return self.file_read('simulation/full_response')  # TODO: Not tested
            else:
                return self.file_read('simulation/summed_response')

        if isinstance(name_fracture, str):
            name_fracture = [name_fracture]

        if isinstance(name_profile, str):
            name_profile = [name_profile]

        if name_fracture[0] != 'all' and len(self.name_fractures) > 1:
            mask_frequencies = self.file_read('simulation/mask_frequencies')
            if self.mode == 'full-reflection':
                freq = arr.zeros((self.ntraces, self.nfrequencies, 3)) + 0j
            else:
                freq = arr.zeros((self.ntraces, self.nfrequencies)) + 0j
            for i, n in enumerate(self.name_fractures):
                if n in name_fracture:
                    sr = self.file_read('simulation/individual_fracture_response', sl=(slice(None), i))
                    freq[:, mask_frequencies] += sr
        else:
            if self.mode == 'propagation':
                freq = self.file_read('simulation/propagation')
            elif self.mode == 'full-reflection':
                freq = self.file_read('simulation/full_response')
            else:
                freq = self.file_read('simulation/summed_response')

        if name_profile[0] != 'all' and len(self.name_profiles) > 1:
            mask_traces = []
            for i, n in enumerate(self.name_profiles):
                if n in name_profile:
                    mask_traces.append(self.file_read(f'antenna/profiles/{n}'))
            freq = freq[arr.hstack(mask_traces)]

        return freq

    def h5_tree(self):
        print('Geometry files\n\n')
        generate_tree_h5_file(self._filename)
        print('\n\nSimulation files\n\n')
        generate_tree_h5_file(self._filename_simulation)
        print('\n\n++++++++++++++++++++++++++++\n\nGeometry files attributes\n\n')
        generate_tree_h5_file_attrs(self._filename)
        print('\n\nSimulation files attributes\n\n')
        generate_tree_h5_file_attrs(self._filename_simulation)

    @staticmethod
    def plot_wiggle(traces, scale=1.0, extent=None, ax=None, fill=True, normalize_to_value=None, **kwargs):
        """
        Plot seismic traces in a wiggle plot.

        Parameters
        ----------
        traces : ndarray
            2D array (nsamples x ntraces)
        scale : float
            Scaling factor for traces.
        extent : list or tuple
            (xmin, xmax, ymin, ymax)
        ax : Matplotlib Axes
            Axes to plot on. If None, a new figure is created.
        fill: Bool
            Fill the wiggle
        normalize_to_value: float
            Normalize the traces to a value. If None, the traces are normalized to the maximum value.
        """
        nsamples, ntraces = traces.shape
        if extent is None:
            offsets = np.arange(ntraces)
            t = np.arange(nsamples)
        else:
            offsets = np.linspace(np.min(extent[:2]), np.max(extent[:2]), ntraces)
            t = np.linspace(np.min(extent[2:]), np.max(extent[2:]), nsamples)

        if ax is None:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots()

        if normalize_to_value is None:
            normalize_to_value = np.abs(traces).max()
        traces = traces.copy() / normalize_to_value  # Normalize
        color = kwargs.pop('color', 'black')
        for offset, tr in zip(offsets, traces.T):
            x = offset + tr * scale
            ax.plot(x, t, color=color, **kwargs)
            if 'label' in kwargs:
                kwargs.pop('label')
            if fill:
                ax.fill_betweenx(t, offset, x, where=(x > offset), color=color)
        return ax
