import os
import numpy as np
import h5py
from scipy import optimize
import markdown
import matplotlib.pyplot as plt
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import MetaData, convert_frequency, decorator_filter_errors
import panel as pn


pn.extension()

logger = set_logger(__name__)


class SourceEM(object):
    """
    Class to manage the EM source
    """

    def __init__(self):
        self._meta = MetaData('EM SourceEM')
        self._frequency_v = None  # Hz
        self._time_v = None
        self._dtime = None
        self._dfreq = None
        self._nsamples = None
        self._nfreq = None
        self._delay = 30
        self._time_window = None
        self._sampling_frequency = None
        self._nyquist = None

        self._frequency = None

        self._center_frequency = 0.1
        #self._units = units
        self.source = None
        self.source_params = {'a': 3,
                              'c': 1,
                              'loc': 0,
                              'scale': 0.1,
                              'ph': 0,
                              't': 30}
        # self._figure = None
        self._option_wavelets = ['generalgamma', 'ricker', 'gaussian', 'gaussiandot', 'gaussianprime', 'gaussiandotnorm',
                                 'gaussiandotdot', 'gaussiandoubleprime', 'gaussiandotdotnorm', 'sine',
                                 'contsine', 'impulse']
        self._type = self._option_wavelets[0]

    def __repr__(self):
        return f"""
        {self._meta.project_name} {self._meta.date}
        
        Type of source: {self.type}
        
        Frequency:
            nfrequencies: {self.nfrequencies if self.nfrequencies is not None else None}
            Sampling frequency: {convert_frequency(self.sampling_frequency * 1e9) if self.sampling_frequency else None}
            Nyquist frequency: {convert_frequency(self.nyquist * 1e9) if self.nyquist else None} 
            delta f: {convert_frequency(self.delta_f * 1e9) if self.delta_f is not None else None} / sample
            Central frequency: {convert_frequency(self.center_frequency * 1e9) if self.center_frequency else None}
            
        Time:
            nsamples: {self.ntime if self.ntime is not None else None} 
            Time Window: {np.around(self.time_window, 2) if self.time_window is not None else None} ns
            delta t: {np.around(self.dtime, 2) if self.dtime is not None else None} ns / sample
            Time delay: {np.around(self.delay,2) if self.delay is not None else None} ns
            """

    #@property
    #def units(self): return self._units

    #@property
    #def c0(self):
    #    """speed of light in free space"""
    #    if self.units == 'SI':
    #        return 299_792_458  # (m/s)
    #    elif self.units == 'CGS':
    #        return 29.9792458  # (cm/ns)

    @property
    def frequency(self):
        return self._frequency

    @property
    def frequency_complete(self):
        return self._frequency_v

    @property
    def nfrequencies(self):
        return self._nfreq

    @property
    def ntime(self):
        return self._nsamples
    @property
    def center_frequency(self):
        """
        Takes the maximum value of the source and returns the frequency
        Returns:
            Frequency in GHz
        """
        return self._center_frequency

    @property
    def delta_f(self):
        return self._dfreq

    @property
    def sampling_frequency(self):
        """
        In GHz
        Returns:
        """
        return self._sampling_frequency

    @property
    def dtime(self):
        """
        Taking into account that the modeling part uses half of the real data according to nyquist sampling theorem.
        Therefore, when modeling signals in the frequency domain, we are modeling half of the spectrum, so we need
        to double the frequency when going to time
        Returns:

        """
        return self._dtime

    @property
    def time_window(self):
        return self._time_window

    @property
    def time(self):
        return self._time_v

    @property
    def nyquist(self):
        return self._nyquist
    @property
    def delay(self):
        return self._delay

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        if value == 'user_defined':
            logger.warning("You selected the 'user_defined' option. Please, provide the source, frequency, "
                           "center_frequency and delay yourself. "
                           "You need to overwrite the 'frequency', 'source', 'waveform_time' and 'waveform_frequency' "
                           "attributes.")
            self._type = value
        elif value in self._option_wavelets:
            self._type = value
        else:
            raise ValueError(f'Wavelet type {value} not available. Available options are: {self._option_wavelets}')

    def generalized_gamma(self,
                          a: float = None,
                          c: float = None,
                          loc: float = None,
                          scale: float = None,
                          ph: float = None,
                          t: float = None) -> np.ndarray:
        """
        This function generates a dipole moment that is used for the antenna
        source. The dipole moment is a generalized gamma distribution that is
        generated using 2 shape parameters (a, c), a complex phase (ph), a location (loc) and scale (scale).
        Implementation is following scipy: https://docs.scipy.org/doc/scipy/tutorial/stats/continuous_gengamma.html
        Args:
            a: (>0) positive value controlling the shape of the dipole moment distribution
            c: (!=0)
            loc:
            scale:
            ph: Orientation of the antenna
            t: time shift of the signal
        Notes:
            Special cases are Weibull (a=1),
            half-normal (a=0.5, c=2) and
            ordinary gamma distributions (c=1).
            If (c=-1) then it is the inverted gamma distribution.
        Returns:
            Dipole moment of the source

        """
        from scipy.stats import gengamma
        if a is None: a = self.source_params['a']
        else: self.set_source_params(a=a)
        if c is None: c = self.source_params['c']
        else: self.set_source_params(c=c)
        if loc is None: loc = self.source_params['loc']
        else: self.set_source_params(loc=loc)
        if scale is None: scale = self.source_params['scale']
        else: self.set_source_params(scale=scale)
        if ph is None: ph = self.source_params['ph']
        else: self.set_source_params(ph=ph)
        if t is None: t = self.source_params['t']
        else: self.set_delay(t)

        rv = gengamma(a, c, loc=loc, scale=scale)
        source_spectrum = rv.pdf(self.frequency)
        source_spectrum[np.isinf(source_spectrum)] = 0
        source_spectrum = source_spectrum * np.exp(1j * ph) * np.exp(-2j * np.pi * t * self.frequency)
        source_spectrum[np.isnan(source_spectrum)] = 0
        source_spectrum[np.isinf(source_spectrum)] = 0
        self.source = source_spectrum / np.max(np.abs(source_spectrum))
        logger.debug('SourceEM set')
        return self.source

    def create_source(self):
        """Taken and modified from GPRMax
        Calculates value of the waveform at a specific time.
        Returns:
            ampvalue (float): Calculated value for waveform.
        """
        delay = self._delay
        center_frequency = self._center_frequency
        if self.type == 'gaussian' or self.type == 'gaussiandot' or self.type == 'gaussiandotnorm' or self.type == 'gaussianprime' or self.type == 'gaussiandoubleprime':
            if delay is None:
                chi = 1 / center_frequency
            else:
                chi = delay
            zeta = 2 * np.pi ** 2 * center_frequency ** 2
        elif self.type == 'gaussiandotdot' or self.type == 'gaussiandotdotnorm' or self.type == 'ricker':
            if delay is None:
                chi = np.sqrt(2) / center_frequency
            else:
                chi = delay
            zeta = np.pi ** 2 * center_frequency ** 2

        # Waveforms
        if self.type == 'gaussian':
            delay = self._time_v - chi
            ampvalue = np.exp(-zeta * delay ** 2)

        elif self.type == 'gaussiandot' or self.type == 'gaussianprime':
            delay = self._time_v - chi
            ampvalue = -2 * zeta * delay * np.exp(-zeta * delay ** 2)

        elif self.type == 'gaussiandotnorm':
            delay = self._time_v - chi
            normalise = np.sqrt(np.exp(1) / (2 * zeta))
            ampvalue = -2 * zeta * delay * np.exp(-zeta * delay ** 2) * normalise

        elif self.type == 'gaussiandotdot' or self.type == 'gaussiandoubleprime':
            delay = self._time_v - chi
            ampvalue = 2 * zeta * (2 * zeta * delay ** 2 - 1) * np.exp(-zeta * delay ** 2)

        elif self.type == 'gaussiandotdotnorm':
            delay = self._time_v - chi
            normalise = 1 / (2 * zeta)
            ampvalue = 2 * zeta * (2 * zeta * delay ** 2 - 1) * np.exp(-zeta * delay ** 2) * normalise

        elif self.type == 'ricker':
            delay = self._time_v - chi
            normalise = 1 / (2 * zeta)
            ampvalue = - (
                    2 * zeta * (2 * zeta * delay ** 2 - 1) * np.exp(-zeta * delay ** 2)) * normalise

        elif self.type == 'sine':
            ampvalue = np.sin(2 * np.pi * center_frequency * self._time_v)
            # c = self._time_v * center_frequency > 1
            ampvalue[self._time_v * center_frequency > 1] = 0

        elif self.type == 'contsine':
            rampamp = 0.25
            ramp = rampamp * self._time_v * center_frequency
            ramp[ramp > 1]= 1
            ampvalue = ramp * np.sin(2 * np.pi * center_frequency * self._time_v)

        elif self.type == 'impulse':
            # time < dt condition required to do impulsive magnetic dipole
            ampvalue = np.zeros(self._time_v.size)
            ampvalue[(self._time_v == 0) | (self._time_v < self._dtime)] = 1

        elif self.type == 'generalgamma':
            source = self.generalized_gamma()
            source[np.isclose(np.abs(source), 0)] = 0
            self.source = source
            try:
                self._center_frequency = self.frequency[np.where(np.abs(self.source) == max(np.abs(self.source)))][0]
            except:
                logger.error('Not possible to calculate center frequency')
                self._center_frequency = np.NaN
            _, (waveform_time, waveform_freq, source) = self.from_frequency_to_time(source_freq=source)
            self.waveform_freq = waveform_freq
            self.waveform_time = waveform_time
            return self.source
        else:
            raise ValueError('Unknown waveform type: {}. \nOptions are: {}'.format(self.type, self._option_wavelets))
        # To avoid a lot of headache later we will set all those values close to 0 to be exactly 0
        ampvalue[np.isclose(np.abs(ampvalue), 0)] = 0
        _,  (waveform_time, waveform_freq, source_freq) = self.from_time_to_frequency(source_time=ampvalue)
        self.waveform_time = waveform_time
        waveform_freq[np.isclose(np.abs(waveform_freq), 0, atol=1e-4)] = 0
        source_freq[np.isclose(np.abs(source_freq), 0, atol=1e-4)] = 0
        # waveform_freq[waveform_freq < np.abs(waveform_freq).max() * 1e-5] = 0 # being the 1e-5 the percentage to filter out
        self.waveform_freq = waveform_freq
        # same in frequency domain
        self.source = source_freq
        return self.source

    def fit_source(self, real_source):
        """
        Takes real data (frequency and source) and fits the response with a generalized gamma distribution
        Args:
            real_source:
        Returns:
        """
        def opt_source(x):
            """
                Optimization function
                Args:
                    x: vector to optimize. Parameters of source function
                Returns:

                """
            source_in = self.create_source(x[0], x[1], x[2], x[3], x[4])
            return np.sqrt(np.sum(np.square(source_in - real_source)))

        x = [self.source_params['a'],
             self.source_params['b'],
             self.source_params['g'],
             self.source_params['ph'],
             self.source_params['t']]

        res = optimize.minimize(opt_source,
                                x,
                                method='nelder-mead',
                                options={'maxfun': 10000,
                                         'xatol': 1e-6,
                                         'disp': False})

        self.source_params['a'] = res.x[0]
        self.source_params['b'] = res.x[1]
        self.source_params['g'] = res.x[2]
        self.source_params['ph'] = res.x[3]
        self.source_params['t'] = res.x[4]

        return self.create_source()

    @staticmethod
    def from_frequency_to_time(f=None, source_freq=None):
        """

        Args:
            frequencies: Only the positive frequencies
            source_freq:

        Returns:

        """
        if f is None and source_freq is None:
            e ='You need to provide either the frequencies or the source frequencies'
            logger.error(e)
            raise ValueError(e)
        if f is not None:
            info = dict()
            nyquist = np.abs(f).max()
            sampling_frequency = 2 * nyquist
            dtime = 1 / sampling_frequency
            frequency = f  # GHz
            dfreq = np.diff(f).mean()
            nfreq = len(f)
            # Here we don't know if its even or odd number of samples, so we assume they are odd numbers
            r = np.flip(f[1:] * -1)
            frequency_v = np.concatenate((f, r))

            nsamples = (nfreq - 1) * 2 + 1
            # self._dtime = 1 / (self._nsamples * self._dfreq)
            time_window = (nsamples - 1) * dtime  # ns
            time_v, test_dtime = np.linspace(0, time_window, nsamples, retstep=True)

            assert np.isclose(test_dtime, dtime), 'Something is wrong with the time vector'
            info['nyquist'] = nyquist
            info['sampling_frequency'] = sampling_frequency
            info['dtime'] = dtime
            info['frequency'] = frequency
            info['dfreq'] = dfreq
            info['nfreq'] = nfreq
            info['frequency_complete'] = frequency_v
            info['nsamples'] = nsamples
            info['time_window'] = time_window
            info['time'] = time_v
            if source_freq is None:
                return info, tuple()
        if source_freq is not None:
            source = source_freq.copy()
            # source[np.isclose(np.abs(source), 0)] = 0
            # We assume here they are odd values
            waveform_freq = np.concatenate((source, np.flip(np.conj(source[1:]))))
            waveform_time = np.real_if_close(np.fft.ifft(waveform_freq))
            if f is None:
                return dict(), (waveform_time, waveform_freq, source)
        return info, (waveform_time, waveform_freq, source)


    @staticmethod
    def from_time_to_frequency(t=None, source_time=None):
        """
        Args:
            time:
            source:

        Returns:

        """
        if t is None and source_time is None:
            e = 'You need to provide either the time or the source time'
            logger.error(e)
            raise ValueError(e)
        if t is not None:
            # assert len(t) % 2 == 0, 'Time vector must be even number of samples. This will make your life easier in future steps'
            time_v = t
            tw = t[-1]
            dtime = np.diff(t).mean()
            nsamples = len(t)
            sampling_frequency = 1 / dtime
            nyquist = sampling_frequency * 0.5
            dfreq = 1 / (nsamples * dtime)
            frequency_v = np.fft.fftfreq(nsamples, d=dtime)
            # For practical purposes, I will only use the positive frequencies
            # If the nsamples is even, the nyquist frequency is included in the frequency vector as a negative value.
            # I will include it in my positive frequency vector
            fv = frequency_v.copy()
            if nsamples % 2 == 0:
                logger.warning('You are setting an even number of time samples. '
                               'The nyquist frequency will be included in the positive frequency vector and we '
                               'will treat the spectrum as odd. This can lead to numerical inaccuracies. '
                               'We recommend to change the number of samples to an odd number')
                fv[nsamples // 2] = np.abs(fv[nsamples // 2])
        # For odd values it is included already
            frequency = fv[fv >= 0]
            nfreq = len(frequency)
            time_window = (nsamples - 1) * dtime
            assert np.isclose(time_window, tw), f'Time window, {time_window} =! {tw}'

            info = dict()
            info['nyquist'] = nyquist # GHz
            info['sampling_frequency'] = sampling_frequency # GHz
            info['dtime'] = dtime # ns
            info['frequency'] = frequency # GHz
            info['dfreq'] = dfreq # GHz
            info['nfreq'] = nfreq
            info['frequency_complete'] = frequency_v # GHz
            info['nsamples'] = nsamples # GHz
            info['time_window'] = time_window # ns
            info['time'] = time_v # ns
            if source_time is None:
                return info, tuple()
        if source_time is not None:
            # source_time[np.isclose(np.abs(source_time), 0)] = 0
            waveform_time = source_time
            waveform_freq = np.fft.fft(waveform_time)
            # waveform_freq[np.isclose(np.abs(waveform_freq), 0)] = 0
            nsamples = waveform_time.size
            M = 1 + nsamples // 2
            source_freq = waveform_freq[:M]
            if t is None:
                return dict(), (waveform_time, waveform_freq, source_freq)
        return info, (waveform_time, waveform_freq, source_freq)

    def export_to_hdf5(self, filename=None, overwrite=False):
        """
        Hierarchical Data Format
        Returns:

        """
        if filename is None:
            logger.info('Saving into cache...')
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'

        directory = os.path.dirname(filename)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        del directory

        with h5py.File(filename, "a") as f:  # Read/write if exists, create otherwise
            if overwrite and 'source' in f:
                logger.info('Overwriting existing "source" information')
                del f['source']

            source = f.create_group('source')
            source.attrs['name'] = self._meta.project_name
            source.attrs['date'] = self._meta.date
            source.attrs['nfreq'] = self.nfrequencies
            source.attrs['sampling_frequency'] = self.sampling_frequency
            source.attrs['nyquist_frequency'] = self.nyquist
            source.attrs['dfreq'] = self.delta_f

            source.attrs['ntime'] = self.ntime
            source.attrs['time_window'] = self.time_window
            source.attrs['dtime'] = self.dtime

            source.create_dataset(name='frequency', data=self.frequency)
            source.create_dataset(name='time_vector', data=self._time_v)
            source.create_dataset(name='freq_vector', data=self._frequency_v)

            if self.source is not None:
                source.attrs['type'] = self.type
                if self.type == 'generalgamma':
                    # sp = source.create_group('s_params')
                    source.attrs['a'] = self.source_params['a']
                    source.attrs['c'] = self.source_params['c']
                    source.attrs['ph'] = self.source_params['ph']
                    source.attrs['loc'] = self.source_params['loc']
                    source.attrs['scale'] = self.source_params['scale']
                    source.attrs['t'] = self.source_params['t']

                source.attrs['center_frequency'] = self.center_frequency
                source.attrs['time_delay'] = self.delay
                source.create_dataset(name='source', data=self.source)
                source.create_dataset(name='time_waveform', data=self.waveform_time)
                source.create_dataset(name='freq_waveform', data=self.waveform_freq)
            else:
                logger.warning('Source function not saved')
        logger.info('File successfully saved in: {}'.format(filename))

    def load_hdf5(self, filename=None):
        """

        Args:
            filename:

        Returns:

        """
        if filename is None:
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'
            logger.info('Loading from cache: {}'.format(filename))

        with h5py.File(filename, "r+") as f:
            source = f['source']
            self.type = source.attrs['type']
            if self.type == 'generalgamma':
                self.source_params['a'] = source.attrs['a']
                self.source_params['c'] = source.attrs['c']
                self.source_params['loc'] = source.attrs['loc']
                self.source_params['scale'] = source.attrs['scale']
                self.source_params['ph'] = source.attrs['ph']
                self.source_params['t'] = source.attrs['t']

            frequency = source['frequency'][:]
            self.set_frequency_vector(frequency)
            self.set_center_frequency(source.attrs['center_frequency'])
            self.set_delay(source.attrs['time_delay'])
            if self.type == 'user_defined':
                self.waveform_time = source['time_waveform'][:]
                self.waveform_freq = source['freq_waveform'][:]
                self.source = source['source'][:]
            else:
                s = self.create_source()
                source_m = source['source'][:]
                if not np.allclose(s, source_m, atol=1e-4):
                    s = 'Source wavelet not loaded correctly. File may be corrupted. Using old values.'
                    logger.error(s)
                    # raise ValueError(s)
                    self.waveform_time = source['time_waveform'][:]
                    self.waveform_freq = source['freq_waveform'][:]
                    self.source = source['source'][:]

        logger.info('Loaded successfully')

    def set_frequency_vector(self, f):
        """

        Args:
            f: GHz
        Returns:

        """
        info, _ = self.from_frequency_to_time(f=f)
        self._nyquist = info['nyquist']
        self._sampling_frequency = info['sampling_frequency']
        self._dtime = info['dtime']
        self._frequency = info['frequency']  # GHz
        self._dfreq = info['dfreq']  # GHz
        self._nfreq = info['nfreq']
        self._frequency_v = info['frequency_complete']
        self._nsamples = info['nsamples']
        self._time_window = info['time_window'] # ns
        self._time_v = info['time'] # ns


    def set_time_vector(self, t):
        """

        Args:
            t:

        Returns:

        """
        info , _ = self.from_time_to_frequency(t=t)
        self._time_v = info['time']
        self._dtime = info['dtime']
        self._nsamples = info['nsamples']
        self._sampling_frequency = info['sampling_frequency']
        self._nyquist = info['nyquist']
        self._dfreq = info['dfreq']
        self._frequency_v = info['frequency_complete']
        self._frequency = info['frequency']
        self._nfreq = info['nfreq']
        self._time_window = info['time_window']

    def set_center_frequency(self, cf):
        self._center_frequency = cf

    def set_delay(self, delay):
        self._delay = delay
        self.set_source_params(t=delay)

    def set_source_params(self,a=None, c=None, loc=None, scale=None, ph=None, t=None):  # Scipy
        if a is not None:
            assert a > 0, 'a must be positive'
            self.source_params['a'] = a
        if c is not None:
            assert c != 0, 'c cannot be zero'
            self.source_params['c'] = c
        if loc is not None:
            self.source_params['loc'] = loc
        if scale is not None:
            assert scale != 0, 'scale cannot be zero'
            self.source_params['scale'] = scale
        if ph is not None:
            self.source_params['ph'] = ph
        if t is not None:
            self.source_params['t'] = t
            self._delay = t

    def plot_waveforms_complete(self):
        """

        Returns:

        """
        mask = self._frequency_v >= 0
        fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, facecolor='w', edgecolor='w', figsize=(10,5))

        # Plot waveform
        ax1.plot(self._time_v, self.waveform_time, 'r', lw=2)
        ax1.set_xlabel('Time (ns)')
        ax1.set_ylabel('Amplitude (Cm)')

        ax2.plot(self._frequency_v[mask], np.abs(self.waveform_freq[mask]),
                 linestyle='-',
                 color='r')
        if self._center_frequency is not None and self._center_frequency > 0 and not np.isnan(self._center_frequency):
            ax2.axvline(x=self._center_frequency, ymin=0, ymax=1,
                        label='cf ~ %s ' % convert_frequency(self._center_frequency * 1e9),
                        linestyle='--', color='green')
        ax2.legend()
        ax2.set_xlabel('Frequency (GHz)')
        ax2.set_ylabel('Amplitude spectrum (Cm)')
        ax1.grid(linestyle='--')
        ax2.grid(linestyle='--')
        plt.tight_layout()
        # plt.show()
        plt.close()
        return fig
    def plot_waveforms_zoom(self):
        tempf = np.abs(self.source) > np.abs(self.source).max() * 0.01  # Display everything that is above 1% of the max value
        pltrangef = np.argmax(np.arange(0, tempf.size) * tempf)
        mask_f = np.s_[0:pltrangef + pltrangef //4]

        tempt = np.abs(self.waveform_time) > np.abs(
            self.waveform_time).max() * 0.01
        pltranget = np.argmax(np.arange(0, tempt.size) * tempt)
        mask_t = np.s_[0:pltranget + pltranget //4]

        fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, num=self.type, figsize=(10, 5), facecolor='w', edgecolor='w')

        # Plot waveform
        ax1.plot(self._time_v[mask_t], self.waveform_time[mask_t], 'r*-')
        ax1.set_xlabel('Time (ns)')
        ax1.set_ylabel('Amplitude (Cm)')

        ax2.plot(self._frequency[mask_f], np.abs(self.source[mask_f]), 'r*-')
        if self._center_frequency is not None and self._center_frequency > 0 and not np.isnan(self._center_frequency):
            ax2.axvline(x=self._center_frequency, ymin=0, ymax=1,
                        label='cf ~ %s ' % convert_frequency(self._center_frequency * 1e9),
                        # GHz' % np.round(df, decimals=3),
                        linestyle='--', color='green')
        ax2.legend()
        ax2.set_xlabel('Frequency (GHz)')
        ax2.set_ylabel('Amplitude spectrum (Cm)')

        ax1.grid(linestyle='--')
        ax2.grid(linestyle='--')
        plt.tight_layout()
        # plt.show()
        plt.close()
        return fig

    def _create_widgets(self):
        if self.source is None:
            cf = 0.3
            self.set_center_frequency(cf)
            self.set_delay(1 / cf)
            self.set_time_vector(np.linspace(0, 30, 101))  # Just take any and then change it
            _ = self.create_source()
        fig1 = self.plot_waveforms_complete()
        fig2 = self.plot_waveforms_zoom()
        self._figure1 = pn.pane.Matplotlib(fig1, dpi=100)
        self._figure2 = pn.pane.Matplotlib(fig2, dpi=100)
        self._widget_time_window = pn.widgets.FloatInput(name='Time window (ns)',
                                                         value=self._time_window,
                                                         step=1)
        self._widget_time_window.param.watch(self._callback_time_window, 'value', onlychanged=True, queued=False)

        self._widget_nsamples = pn.widgets.IntInput(name='Number of samples',
                                                    value=self._nsamples,
                                                    step=2)
        self._widget_nsamples.param.watch(self._callback_nsamples, 'value', onlychanged=True, queued=False)
        self._widget_time_delay = pn.widgets.FloatInput(name='Time delay (ns)',
                                                        value=self._delay,
                                                        step=1)
        self._widget_time_delay.param.watch(self._callback_time_delay, 'value', onlychanged=True, queued=False)

        self._widget_sampling_frequency = pn.widgets.FloatInput(name='Sampling frequency (GHz)',
                                                                value=self._sampling_frequency,
                                                                )
        self._widget_sampling_frequency.param.watch(self._callback_sampling_frequency, 'value', onlychanged=True, queued=False)
        self._widget_nfreq = pn.widgets.IntInput(name='Number of frequencies',
                                                 value=self._nfreq,
                                                 step=1)
        self._widget_nfreq.param.watch(self._callback_nfreq, 'value', onlychanged=True, queued=False)
        self._widget_center_frequency = pn.widgets.FloatInput(name='Center frequency (GHz)',
                                                              value=self._center_frequency,
                                                              step=0.05,
                                                              disabled=True if self.type == 'generalgamma' else False)
        self._widget_center_frequency.param.watch(self._callback_center_frequency, 'value', onlychanged=True, queued=False)

        self._widget_type = pn.widgets.Select(name='Type of source',
                                              options=self._option_wavelets,
                                              value= self.type)
        self._widget_type.param.watch(self._callback_type, 'value', onlychanged=True, queued=False)

        self._widget_source_a = pn.widgets.FloatInput(name='a (alpha: Controls the center of the distribution)',
                                                      value=self.source_params['a'],
                                                      # start=0,
                                                      step=0.1, disabled=False if self.type == 'generalgamma' else True)
        self._widget_source_a.param.watch(self._callback_s_a, 'value', onlychanged=True, queued=False)
        self._widget_source_ph = pn.widgets.FloatInput(name='ph (Antenna orientation)',
                                                       value=self.source_params['ph'],
                                                       # start=0,
                                                       step=0.1,disabled=False if self.type == 'generalgamma' else True)
        self._widget_source_ph.param.watch(self._callback_s_ph, 'value', onlychanged=True, queued=False)

        self._widget_source_c = pn.widgets.FloatInput(name='c',
                                                       value=self.source_params['c'],
                                                       # start=0,
                                                       step=0.1,disabled=False if self.type == 'generalgamma' else True)
        self._widget_source_c.param.watch(self._callback_s_c, 'value', onlychanged=True, queued=False)
        self._widget_source_loc = pn.widgets.FloatInput(name='loc',
                                                       value=self.source_params['loc'],
                                                       # start=0,
                                                       step=0.1,disabled=False if self.type == 'generalgamma' else True)
        self._widget_source_loc.param.watch(self._callback_s_loc, 'value', onlychanged=True, queued=False)
        self._widget_source_scale = pn.widgets.FloatInput(name='scale',
                                                       value=self.source_params['scale'],
                                                       # start=0,
                                                       step=0.1,disabled=False if self.type == 'generalgamma' else True)
        self._widget_source_scale.param.watch(self._callback_s_scale, 'value', onlychanged=True, queued=False)


        self._widget_padding = pn.widgets.IntInput(name='Add zero padding',
                                                   value=0,
                                                   width=120)
        self._widget_button_padding = pn.widgets.Button(name='Pad', button_style='outline', button_type='primary',
                                                        height=50)
        self._widget_button_padding.param.watch(self._callback_pad, 'value', onlychanged=True, queued=False)

        self._widget_markdown = pn.pane.Markdown(markdown.markdown(self.__repr__()))

    def _update_all(self):
        self._widget_type.value = self.type
        self._widget_center_frequency.value = self._center_frequency
        self._widget_time_delay.value = self._delay
        self._widget_time_window.value = self._time_window
        self._widget_nsamples.value = self._nsamples
        self._widget_sampling_frequency.value = self._sampling_frequency
        self._widget_nfreq.value = self._nfreq
        _ = self.create_source()
        if self.type == 'generalgamma':  # After each update the center changes
            self._widget_center_frequency.value = self._center_frequency
        self._widget_markdown.object = markdown.markdown(self.__repr__())
        fig = self.plot_waveforms_complete()
        self._figure1.object = fig
        self._figure1.param.trigger('object')

        fig2 = self.plot_waveforms_zoom()
        self._figure2.object = fig2
        self._figure2.param.trigger('object')

    def _callback_pad(self, event):
        self._widget_button_padding.disabled = True
        self._widget_padding.disabled = True
        npad = self._widget_padding.value
        freq_v = np.linspace(0, self._nyquist, self._nfreq)
        to_pad = np.arange(0, npad) * self._dfreq + self._nyquist + self._dfreq
        new_freq = np.concatenate((freq_v, to_pad))
        self.set_frequency_vector(new_freq)
        self._update_all()
        self._widget_padding.value = 0
        self._widget_padding.disabled = False
        self._widget_button_padding.disabled = False

    def _callback_s_ph(self, event):
        self.set_source_params(ph=event.new)
        self._update_all()

    def _callback_s_a(self, event):
        self.set_source_params(a=event.new)
        self._update_all()

    def _callback_s_b(self, event):
        self.set_source_params(b=event.new)
        self._update_all()

    def _callback_s_g(self, event):
        self.set_source_params(g=event.new)
        self._update_all()

    def _callback_s_c(self, event):
        self.set_source_params(c=event.new)
        self._update_all()

    def _callback_s_loc(self, event):
        self.set_source_params(loc=event.new)
        self._update_all()

    def _callback_s_scale(self, event):
        self.set_source_params(scale=event.new)
        self._update_all()

    def _callback_time_window(self, event):
        self._time_window = event.new
        time_v = np.linspace(0, self._time_window, self._nsamples)
        self.set_time_vector(time_v)
        self._update_all()

    def _callback_nsamples(self, event):
        self._nsamples = event.new
        time_v = np.linspace(0, self._time_window, self._nsamples)
        self.set_time_vector(time_v)
        self._update_all()

    def _callback_time_delay(self, event):
        self.set_delay(event.new)
        self._update_all()

    def _callback_sampling_frequency(self, event):
        sampling_frequency = event.new
        freq_v = np.linspace(0, sampling_frequency*0.5, self._nfreq)
        self.set_frequency_vector(freq_v)
        self._update_all()

    def _callback_nfreq(self, event):
        nfreq = event.new
        freq_v = np.linspace(0, self._nyquist, nfreq)
        self.set_frequency_vector(freq_v)
        self._update_all()

    def _callback_center_frequency(self, event):
        self._center_frequency = event.new
        self._update_all()

    def _callback_type(self, event):
        self.type = event.new
        if self.type == 'generalgamma':
            self._widget_source_a.disabled=False
            self._widget_source_c.disabled=False
            self._widget_source_loc.disabled=False
            self._widget_source_scale.disabled=False
            # self._widget_source_b.disabled=False
            # self._widget_source_g.disabled=False
            self._widget_source_ph.disabled=False
            self._widget_center_frequency.disabled=True
        else:
            self._widget_source_a.disabled = True
            self._widget_source_c.disabled=True
            self._widget_source_loc.disabled=True
            self._widget_source_scale.disabled=True
            # self._widget_source_b.disabled = True
            # self._widget_source_g.disabled = True
            self._widget_source_ph.disabled = True
            self._widget_center_frequency.disabled = False

        self._update_all()

    def widgets(self):
        self._create_widgets()
        p = pn.Column(self._widget_markdown,
                pn.Row(pn.Column('# Controllers:',
                             pn.WidgetBox('## Time',
                                          self._widget_time_window,
                                          self._widget_nsamples,
                                          self._widget_time_delay),
                             pn.WidgetBox('## Frequency',
                                          self._widget_sampling_frequency,
                                          self._widget_nfreq,
                                          self._widget_center_frequency,
                                          pn.Row(self._widget_padding,
                                                 self._widget_button_padding)),
                             pn.WidgetBox('## Customized Source:',
                                          self._widget_source_a,
                                          self._widget_source_c,
                                          self._widget_source_loc,
                                          self._widget_source_scale,
                                          # self._widget_source_b,
                                          # self._widget_source_g,
                                          self._widget_source_ph),
                             ),
                   pn.Column('<b>Select source wavelet</b>:',
                              self._widget_type,
                              '### Full wavelet:',
                              self._figure1,
                              '### Zoomed wavelet:',
                              self._figure2)
                   ))
        return p