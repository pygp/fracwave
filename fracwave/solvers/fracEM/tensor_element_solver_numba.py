import numpy
import datetime
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import get_backend, decorator_filter_errors
from tqdm.autonotebook import tqdm
import time
import numpy as np
from numba import jit

logger = set_logger(__name__)

arr = np


def forward_solver(self, save_hd5=False):
    """
    Function to manage the forward calculation for the electromagnetic wave response
    Args:
        save_steps: To prevent that the tensors are stored in RAM, save to disc
        overwrite: If the file has already an attribute, overwrite the attribute
        recalculate: Recalculate all ignoring if the file exists in file
    Returns:

    """
    logger.info('Starting calculation for {} amount of traces'.format(self.ntraces))
    start_time1 = time.time()

    if self._fast_calculation_incoming_field:
        get_mask_incoming_field(self)


    f = self.file_read('source/frequency')
    source = self.file_read('source/source')
    omega = 2 * numpy.pi * (10 ** 9) * f
    # del f
    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)


    orient_tx = self.file_read('antenna/orient_Tx')
    orient_rx = self.file_read('antenna/orient_Rx')
    tx = self.file_read('antenna/Tx')
    Rx = self.file_read('antenna/Rx')
    # source = self.file_read('source/source')


    midpoint = self.file_read('mesh/geometry/midpoint')

    nx = self.file_read('mesh/geometry/nx')
    ny = self.file_read('mesh/geometry/nx')
    nz = self.file_read('mesh/geometry/nx')

    area = self.file_read('mesh/geometry/area')
    aperture = self.file_read('mesh/properties/aperture')
    epsilon = self.file_read('mesh/properties/elec_permeability')
    sigma = self.file_read('mesh/properties/elec_conductivity')

    mask_b = self.file_read('simulation/mask_elements')


    # ------------------- Loop through all the traces
    sum_rep = []
    incomming_field = []
    l = (pbar := tqdm(range(self.ntraces))) #if self.backend != 'dask' else range(self.ntraces)
    for index in l:
        pbar.set_description('1) Calculating propagation to fracture --------------------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None

        #logger.debug('1) ------- Calculating propagation to fracture  ---------')
        #if not self._simulation['incoming_field'] or recalculate:
        source_matrix = arr.einsum('i, f -> fi',
                                   orient_tx[index],
                                   # self.file_read('source/source') if save_hd5 else source)[None]  # For extra dimension
                                   source)[None]  # For extra dimension

        if self.mode == 'propagation':
            inc_field = propagation(self,
                                    omega=omega,
                                    src_vec=source_matrix,
                                    s_loc=tx[index, :],
                                    obs_loc=Rx,
                                    )
            self.file_save(value='simulation/propagation',
                           data=inc_field,
                           sl=((index),))
        else:

            inc_field = arr.zeros((self.nelements, self.nfrequencies, 3))
            inc_field = inc_field.astype(complex)

            mask = mask_b[index]

            inc_field[mask] = propagation(self,
                                          omega=omega,
                                          src_vec=source_matrix,
                                          s_loc=tx[index],
                                          obs_loc=midpoint[mask],
                                          )
            incfied = arr.einsum('efi->e',
                                 arr.absolute(inc_field))  # Collapse the frequency axis and the components


            incomming_field.append(incfied)


        if self.mode in ['propagation', 'incoming_field']:
            continue
        # del source_matrix
        # if save_hd5: del inc_field
        # ------------------- Electric components on plane
        # logger.info('2) ------- Projecting incoming field to the tangential and normal '
        #             'vectors of the element -------')

        def proj(field: str, index):
            assert field in ['x', 'y', 'z'], 'field must be one of "x", "y", "z"'
            val = arr.zeros((self.nelements, self.nfrequencies, 3))


            val = val.astype(complex)


            if field == 'x': geom = nx[mask]
            if field == 'y': geom = ny[mask]
            if field == 'z': geom = nz[mask]

            val[mask] = orthogonal_projection(inc_field[mask],
                                              geom)

            return val

        pbar.set_description('2) Projecting incoming field to tan and norm vectors ------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None
        x = proj('x', index)
        y = proj('y', index)
        z = proj('z', index)

        #logger.info('"simulation/projected" saved')
        #self._simulation['projected'] = True

        # ------------------- SourceEM to dipole. Polarization of electric field
        #logger.info(
        #    '3) ------- Calculating electric field to dipole using "{}" coefficients on fracture ------- '.format(
         #       self.mode))

        kwargs_vector_fields = dict()

        pbar.set_description('3) Calculating electric field to dipole -------------------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None


        if self.mode in ['reflection', 'full-reflection']:
            tmp = source_to_dipole(self,
                                   omega=omega,
                                   area=area[mask],
                                   aperture=aperture[mask],
                                   fracture_epsilon=epsilon[mask],
                                   fracture_sigma=sigma[mask],
                                   tangential_field_x=x[mask],
                                   tangential_field_y=y[mask],
                                   normal_field_z=z[mask]
                                   )
        else:
            tmp = source_to_dipole(self,
                                   omega=omega,
                                   area=area[mask],
                                   aperture=aperture[mask],
                                   fracture_epsilon=epsilon[mask],
                                   fracture_sigma=sigma[mask],)
        in_dip = arr.zeros((self.nelements, self.nfrequencies, 3), dtype=complex)

        # in_dip = in_dip.astype(complex)

        in_dip[mask] = tmp

            #self._simulation['induced_dipole'] = True
            #logger.info('"simulation/induced_dipole" saved')

        #else:
        #    logger.info('found previous "simulation/induced_dipole"')

        if self.mode == 'dipole-response':
            logger.info('++++ {} mode finished ++++'.format(self.mode))
            return in_dip

            # ------------------- Propagation from dipole of fracture element to receiver
        #logger.info('4) -------  Propagating electric field from fracture to receiver antenna ------- ')
        #if not self._simulation['receiver'] or recalculate:


        pbar.set_description('4) Propagating electric field to receiver antenna ---------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None

        rec_re = arr.zeros((self.nelements, self.nfrequencies, 3))

        rec_re = rec_re.astype(complex)

        rec_re[mask] = propagation(self,
                                   omega=omega,
                                   src_vec=in_dip[mask],
                                   s_loc=midpoint[mask],
                                   # Fit to traces x elements x 3
                                   obs_loc=Rx[index],
                                   # Fit to traces x elements x 3
                                   )


        #self._simulation['receiver'] = True
        #logger.info('"simulation/receiver" saved')

        #else:
        #    logger.info('found previous "simulation/receiver"')

        # --------------- Summed response
        #logger.info('5) -------  Summing response to antennas ------- ')
        #if not self._simulation['summed_response'] or recalculate:
            #summed_response = []
            #for index in (pbar := tqdm(range(self.ntraces))):
        pbar.set_description('5) Summing response to antennas ---------------------------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None

        if self.mode == 'full-reflection':
            sum_r = arr.einsum('efi -> fi',
                                         rec_re[mask]
                                         )
        elif self.mode == 'reflection':
            orx = orient_rx[index]
            sum_r = arr.einsum('efi, i -> f',
                                         rec_re[mask],
                                         orx)
            # del orx
        else:
            raise KeyError(self.mode)

        sum_rep.append(sum_r)

            #lf._simulation['summed_response'] = True
            #logger.info('"simulation/summed_response" saved')

        pbar.set_description('6) End ----------------------------------------------------- Trace: {}'.format(index + 1)) if self.backend != 'dask' else None
        #else:
        #    logger.info('found previous "simulation/summed_response"')
    if self.mode in ['propagation', 'incoming_field']:
        logger.info('++++ {} mode finished ++++'.format(self.mode))
        return inc_field



    sum_rep = arr.stack(sum_rep)
    incomming_field = arr.stack(incomming_field)

    self.file_save('simulation/fracture_field', data=incomming_field)

    logger.info(' ------- End of Calculation. Elapsed time: {} ------- '.format(
        datetime.timedelta(seconds=time.time() - start_time1)))
    return sum_rep

@jit(nopython=False)
def propagation(self, omega, src_vec, s_loc, obs_loc):
    """
    Function to compute the propagation of the electric field from a point dipole
    Args:
        omega: angular frequencies
        epsilon: Relative electrical permittivity
        sigma: Electrical conductivity
        src_vec: source_dipole
        s_loc: SourceEM location
        obs_loc: Observer location

    Returns:
        Electric field
    """
    k = self.wav_num
    R_vec = obs_loc - s_loc  # Distance vector (nelements, 3)
    R = arr.linalg.norm(R_vec, axis=1)
    RR = arr.einsum('ei, ej -> eij', R_vec, R_vec)  # (nfrequencies, nelements, 3, 3)

    #@decorator_filter_errors
    #def Green():
    """
    Everything is rearranged to construct tensor (traces, elements, frequencies, (3x3; green))
    Returns:
        (traces, elements, frequencies, (3x3; green))
    """
    temp1 = arr.einsum('f, e -> ef', k, R)
    temp2 = arr.einsum('f, e -> ef', k ** 2, R ** 2)
    I = numpy.identity(3)  # Identity matrix

    first_term = arr.divide(arr.exp(1j * temp1), 4 * numpy.pi * R[:, None])
    second_term = arr.einsum('ef, ij -> efij', (1 + arr.divide(1j * temp1 - 1, temp2)), I)
    t1 = arr.divide(3 - 3j * temp1 - temp2, temp2)
    t2 = arr.divide(RR, (R ** 2)[:,None, None])
    third_term = arr.einsum('ef, eij -> efij', t1, t2)
    green = arr.einsum('ef, efij->efij', first_term, (second_term + third_term))

    efield = arr.einsum('f, efi -> efi',
                        omega ** 2 * self.m0,
                        arr.einsum('efij, efi -> efj', green, src_vec))
    return efield

@jit
def causality(self, omega, s_loc, obs_loc, mask):
    """
    Function to compute the causality of the electric field from a point dipole
    Args:
        omega: angular frequencies
        s_loc: SourceEM location
        obs_loc: Observer location
        mask: Mask of the simulation window
    Returns:
        mask with the elements that are outside the time window
    """
    R_vec = obs_loc - s_loc
    R = arr.linalg.norm(R_vec, axis=1)
    cond = (arr.einsum('e, f->ef', R, self.wav_num) / omega)
    cond[arr.isnan(cond)] = 0

    cond = arr.max(arr.absolute(cond), axis=1)
    causality_index = cond <= (self.time_window * 1e-9)  # To have from nano to seconds
    #logger.info('Part of the signal is not causal and is omitted')
    mask *= causality_index
    return mask

@jit
def orthogonal_projection(vector_in, projection_vector):
    """
    The projection of a vector $\overrightarrow{u}$ onto a plane is calculated by
    subtracting the component of $\overrightarrow{u}$ which is orthogonal to the plane
    from $\overrightarrow{u}$.
    Args:
        vector_in:
        projection_vector:

    Returns:

    """
    divisor = arr.linalg.norm(projection_vector, axis=1)
    dot_product = arr.einsum('ei, efi -> ef', projection_vector, vector_in)
    return arr.einsum('ef,ei->efi', dot_product / divisor[:, None], projection_vector)  # equivalent to outer product

@decorator_filter_errors
@jit
def source_to_dipole(self,
                     omega,
                     area,
                     aperture,
                     fracture_epsilon,
                     fracture_sigma,
                     tangential_field_x=None,
                     tangential_field_y=None,
                     normal_field_z=None):
    """
    Calculate the induced dipole

    Args:
        omega: Angular frequency
        area: Area of fracture at each element
        aperture: Aperture of the fracture at each element
        fracture_epsilon: Electric permittivity of fracture at each element
        fracture_sigma: Electric conductivity of fracture at each element
        tangential_field_x: Projected incoming field
        tangential_field_y: Projected incoming field
        normal_field_z: Projected incoming field
    Returns:
        induced dipole

    """
    k2_frac = self.wavenumber(omega[None, :],  # To fit nelement x frequencies
                              fracture_epsilon[:, None],
                              fracture_sigma[:, None])
    fracture_complex_epsilon = self.complex_epsilon(omega[None, :],
                                                    fracture_epsilon[:, None],
                                                    fracture_sigma[:, None])

    if self.mode in ['reflection', 'full-reflection']:
        modulationTE, modulationTM = self.reflection(e1=self.rce[None, :],
                                                     e2=fracture_complex_epsilon,
                                                     k1=self.wav_num[None, :],
                                                     k2=k2_frac,
                                                     aperture=aperture[:, None])
        modulation = arr.einsum('efi, ef -> efi', tangential_field_x, modulationTE) + \
                     arr.einsum('efi, ef -> efi', tangential_field_y, modulationTE) + \
                     arr.einsum('efi, ef -> efi', normal_field_z, modulationTM)

    elif self.mode == 'transmission':
        modulation = self.transmission(k1=self.wav_num[None, :],
                                       k2=k2_frac,
                                       aperture=aperture[:, None])[..., None]
    else:
        modulation = arr.ones((fracture_epsilon.shape[0],
                               omega.shape[0],
                               3))
    return arr.einsum('e, efi -> efi',
                      area*aperture,
                      arr.divide(modulation, 1j*omega[None, :, None]))


def get_mask_incoming_field(self):
    """
    Get the mask matrix for the elements that have almost zero energy.
    Incoming field at fracture
    Args:
        self:

    Returns:

    """
    logger.debug('Calculating the mask at the fracture field...')
    f = self.file_read('source/frequency')
    source = self.file_read('source/source')

    a = arr.absolute(source)
    if self.backend == 'dask':
        pos_max_freq = arr.where(a == a.max())[0]
    else:
        pos_max_freq = arr.where(a == a.max())[0][0]
    threshold = 0.8  # take the values higher than this one

    mask_freq = a > threshold
    levels = 10
    zones = arr.round(sum(mask_freq) / 2 / levels) # Equally spaced levels to divide the frequencies
    mask_int = arr.zeros(len(mask_freq))


    zones = zones.astype(int)
    mask_int = mask_int.astype(bool)

    for l in range(levels):
        l_c = pos_max_freq - zones * (l + 1) if pos_max_freq - zones * (l + 1) > 0 else 0
        r_1 = np.arange(l_c, pos_max_freq - zones * l, l + 1)
        r_c = pos_max_freq + zones * (l + 1) if pos_max_freq + zones * (l + 1) < len(mask_freq) else len(mask_freq)
        r_2 = np.arange(pos_max_freq + zones * l, r_c, l + 1)
        mask_int[r_1] = True
        mask_int[r_2] = True

    mask_freq *= mask_int

    f = f[mask_freq]
    omega = 2 * numpy.pi * (10 ** 9) * f
    source = source[mask_freq]

    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)

    l = (pbar := tqdm(range(self.ntraces))) if self.backend != 'dask' else range(self.ntraces)
    for index in l:
        pbar.set_description('Calculating mask at fracture --------------------- Trace: {}'.format(
            index + 1)) if self.backend != 'dask' else None

        source_matrix = arr.einsum('i, f -> fi',
                                   self.file_read('antenna/orient_Tx')[index],
                                   source)[None]  # For extra dimension

        if self.apply_causality: # and self.backend != 'torch':
            pbar.set_description('Calculating causality --------------------- Trace: {}'.format(
                index + 1)) if self.backend != 'dask' else None
            mask_fracture = causality(self,
                                      omega,
                                      s_loc=self.file_read('antenna/Tx')[index],
                                      obs_loc=self.file_read('mesh/geometry/midpoint'),
                                      mask=self.file_read('simulation/mask_elements')[index])

        inc_field = arr.zeros((self.nelements, len(omega), 3))
        if self.backend == 'torch':
            inc_field = inc_field.type(arr.complex128)
        else:
            inc_field = inc_field.astype(complex)
        inc_field[mask_fracture] = propagation(self,
                                      omega=omega,
                                      src_vec=source_matrix,
                                      s_loc=self.file_read('antenna/Tx', sl=((index),)),
                                      obs_loc=self.file_read('mesh/geometry/midpoint')[mask_fracture],
                                      )
        incfield = arr.einsum('efi->e',
                             arr.absolute(inc_field))  # Collapse the frequency axis and the components

        if self.filter_energy:
            pbar.set_description('Filtering according to max energy --------------------- Trace: {}'.format(
                index + 1)) if self.backend != 'dask' else None
            # logger.debug('Filtering according to max energy...')
            # Max value at the fracture for each trace, frequency and component
            if self.backend == 'torch':
                max_, _ = arr.max(incfield, axis=-1)
                # max_ = max_[:, None]  # [:, None] to make it broadcastable
            else:
                max_ = arr.max(incfield, axis=-1)  # [:, None]
            cut = max_ * self._filter_percentage  # 1% of the max energy
            mask_fracture = (incfield >= cut) * mask_fracture
            if self.backend == 'dask':
                mask_fracture = mask_fracture.compute()
                # incoming_field = incoming_field.compute()

        self.file_save(value='simulation/mask_elements',
                       data=mask_fracture,
                       sl=((index),))

        pbar.set_description('End mask calculation --------------------- Trace: {}'.format(
            index + 1)) if self.backend != 'dask' else None





