import numpy
from fracwave.utils.logger import set_logger
from fracwave.utils.help_decorators import get_backend, decorator_filter_errors
from tqdm.autonotebook import tqdm
import h5py

logger = set_logger(__name__)

arr = get_backend()


def forward_solver(self, overwrite=False, recalculate=False):
    """
    Function to manage the forward calculation for the electromagnetic wave response
    Args:
        save_steps: To prevent that the tensors are stored in RAM, save to disc
        overwrite: If the file has already an attribute, overwrite the attribute
        recalculate: Recalculate all ignoring if the file exists in file
    Returns:

    """
    global arr
    arr = get_backend()
    logger.info('Starting calculation for {} amount of traces'.format(self.ntraces))

    omega = 2 * numpy.pi * (10 ** 9) * self.file_read('source/frequency')
    self.wav_num = self.wavenumber(omega, self.rock_epsilon, self.rock_sigma)
    self.rce = self.complex_epsilon(omega, self.rock_epsilon, self.rock_sigma)

    # ------------------- Loop through all the traces
    logger.info('1) ------- Calculating propagation to fracture  ---------')
    if not self._simulation['incoming_field'] or recalculate:
        for index in (pbar := tqdm(range(self.ntraces))):
            pbar.set_description('Calculating incoming field on trace: {}'.format(index+1))
            source_matrix = arr.einsum('i, f -> fi',
                                       self.file_read('antenna/orient_Tx')[index],
                                       self.file_read('source/source'))[None]  # For extra dimension

            if self.mode == 'propagation':
                inc_field = propagation(self,
                                        omega=omega,
                                        src_vec=source_matrix,
                                        s_loc=self.file_read('antenna/Tx')[index, :],
                                        obs_loc=self.file_read('antenna/Rx'),
                                        )
                self.file_save(value='simulation/propagation',
                               data=inc_field,
                               sl=((index),))
            else:
                if self.apply_causality and self.backend != 'torch':
                    mask = causality(self,
                                     omega,
                                     s_loc=self.file_read('antenna/Tx')[index, :],
                                     obs_loc=self.file_read('mesh/geometry/midpoint'),
                                     mask=self.file_read('simulation/mask_elements')[index])
                    self.file_save(value='simulation/mask_elements',
                                   data=mask,
                                   sl=((index),))

                mask = self.file_read('simulation/mask_elements', sl=((index,)))
                if self.backend == 'dask': mask = mask.compute()
                inc_field = arr.zeros((self.nelements, self.nfrequencies, 3))
                if self.backend == 'torch': inc_field = inc_field.type(arr.complex128)
                else: inc_field = inc_field.astype(complex)

                inc_field[mask] = propagation(self,
                                              omega=omega,
                                              src_vec=source_matrix,
                                              s_loc=self.file_read('antenna/Tx', sl=((index),)),
                                              obs_loc=self.file_read('mesh/geometry/midpoint')[mask],
                                              )
                if self.filter_energy:
                    #logger.debug('Filtering according to max energy...')
                    incfied = arr.einsum('efi->e',
                                         arr.absolute(inc_field))  # Collapse the frequency axis and the components
                    # Max value at the fracture for each trace, frequency and component
                    if self.backend == 'torch':
                        max_, _ = arr.max(incfied, axis=-1)
                        #max_ = max_[:, None]  # [:, None] to make it broadcastable
                    else:
                        max_ = arr.max(incfied, axis=-1)#[:, None]
                    cut = max_ * self._filter_percentage  # 1% of the max energy
                    mask = (incfied >= cut) * mask
                    if self.backend == 'dask':
                        mask = mask.compute()
                        #incoming_field = incoming_field.compute()
                    inc_field[~mask] = 0
                    self.file_save(value='simulation/mask_elements',
                                   data=mask,
                                   sl=((index),))
                self.file_save(value='simulation/incoming_field',
                               data=inc_field,
                               sl=((index),))

        self._simulation['incoming_field'] = True
        del source_matrix, inc_field
    else:
        logger.info('found previous "simulation/{}" information'.format(self.mode))
    if self.mode in ['propagation', 'incoming_field']:
        logger.info('++++ {} mode finished ++++'.format(self.mode))
        return self.file_read("simulation/{}".format(self.mode))

    # ------------------- Electric components on plane
    logger.info('2) ------- Projecting incoming field to the tangential and normal '
                'vectors of the element -------')
    if not self._simulation['projected'] or recalculate:

        def proj(field: str, index):
            assert field in ['x', 'y', 'z'], 'field must be one of "x", "y", "z"'
            mask = self.file_read('simulation/mask_elements', sl=((index),))  # Don't care about the frequencies
            if self.backend == 'dask': mask = mask.compute()
            val = arr.zeros((self.nelements, self.nfrequencies, 3))

            if self.backend == 'torch':
                val = val.type(arr.complex128)
            else:
                val = val.astype(complex)

            val[mask] = orthogonal_projection(self.file_read('simulation/incoming_field', sl=((index),))[mask],
                                              self.file_read('mesh/geometry/n{}'.format(field))[mask]
                                              if self.backend != 'torch'
                                              else self.file_read('mesh/geometry/n{}'.format(field))[
                                                  mask].type(arr.complex128))
            if field == 'z':
                na = 'nfz'
            elif field == 'y' or field == 'x':
                na = 'tf{}'.format(field)
            self.file_save(value='simulation/projected/{}'.format(na),
                           data=val,
                           sl=((index),))
            del val


        for index in (pbar := tqdm(range(self.ntraces))):
            pbar.set_description('Projecting incoming field at trace: {}'.format(index+1))
            proj('x', index)
            proj('y', index)
            proj('z', index)

        logger.info('"simulation/projected" saved')
        self._simulation['projected'] = True
    else:
        logger.info('found previous "simulation/projected"')
    #
    # ------------------- SourceEM to dipole. Polarization of electric field
    logger.info(
        '3) ------- Calculating electric field to dipole using "{}" coefficients on fracture ------- '.format(
            self.mode))
    if not self._simulation['induced_dipole'] or recalculate:
        kwargs_vector_fields = dict()
        for index in (pbar := tqdm(range(self.ntraces))):
            pbar.set_description('Polarizing elements at trace: {}'.format(index + 1))
            mask = self.file_read('simulation/mask_elements', sl=((index),))
            if self.backend == 'dask': mask = mask.compute()
            if self.mode in ['reflection', 'full-reflection']:
                kwargs_vector_fields = dict(tangential_field_x=self.file_read('simulation/projected/tfx', sl=((index),))[mask],
                                            tangential_field_y=self.file_read('simulation/projected/tfy',sl=((index),))[mask],
                                            normal_field_z=self.file_read('simulation/projected/nfz', sl=((index),))[mask])
            in_dip = arr.zeros((self.nelements, self.nfrequencies, 3))
            if self.backend == 'torch':
                in_dip = in_dip.type(arr.complex128)
            else:
                in_dip = in_dip.astype(complex)
            in_dip[mask] = source_to_dipole(self,
                                              omega=omega,
                                              area=self.file_read('mesh/geometry/area')[mask],
                                              aperture=self.file_read('mesh/properties/aperture')[mask],
                                              fracture_epsilon=self.file_read(
                                                  'mesh/properties/elec_permeability')[mask],
                                              fracture_sigma=self.file_read(
                                                  'mesh/properties/elec_conductivity')[mask],
                                              **kwargs_vector_fields
                                              )
            self.file_save(value='simulation/induced_dipole',
                           data=in_dip,
                           sl=((index),))


        self._simulation['induced_dipole'] = True
        logger.info('"simulation/induced_dipole" saved')
        del in_dip, kwargs_vector_fields
    else:
        logger.info('found previous "simulation/induced_dipole"')

    if self.mode == 'dipole-response':
        logger.info('++++ {} mode finished ++++'.format(self.mode))
        return self.file_read('simulation/induced_dipole')

        # ------------------- Propagation from dipole of fracture element to receiver
    logger.info('4) -------  Propagating electric field from fracture to receiver antenna ------- ')
    if not self._simulation['receiver'] or recalculate:

        for index in (pbar := tqdm(range(self.ntraces))):
            pbar.set_description('Propagating back electric field to receiver at trace: {}'.format(index + 1))
            mask = self.file_read('simulation/mask_elements', sl=((index),))  # Don't care about the frequencies
            if self.backend == 'dask': mask = mask.compute()
            rec_re = arr.zeros((self.nelements, self.nfrequencies, 3))
            if self.backend == 'torch':
                rec_re = rec_re.type(arr.complex128)
            else:
                rec_re = rec_re.astype(complex)

            rec_re[mask] = propagation(self,
                                       omega=omega,
                                       src_vec=self.file_read('simulation/induced_dipole', sl=((index),))[mask],
                                       s_loc=self.file_read('mesh/geometry/midpoint')[mask],
                                       # Fit to traces x elements x 3
                                       obs_loc=self.file_read('antenna/Rx', sl=((index),)),
                                       # Fit to traces x elements x 3
                                       )
            self.file_save(value='simulation/receiver',
                           data=rec_re,
                           sl=((index),))

        self._simulation['receiver'] = True
        logger.info('"simulation/receiver" saved')
        del rec_re
    else:
        logger.info('found previous "simulation/receiver"')

    # --------------- Summed response
    logger.info('5) -------  Summing response to antennas ------- ')
    if not self._simulation['summed_response'] or recalculate:
        #summed_response = []
        for index in (pbar := tqdm(range(self.ntraces))):
            pbar.set_description('Summing response at trace: {}'.format(index + 1))

            if self.mode == 'full-reflection':
                sum_r = arr.einsum('efi -> fi',
                                             self.file_read('simulation/receiver', sl=((index),)),
                                             )
            elif self.mode == 'reflection':
                orx = self.file_read('antenna/orient_Rx', sl=((index),)).type(
                    arr.complex128) if self.backend == 'torch' else self.file_read('antenna/orient_Rx', sl=((index),))
                sum_r = arr.einsum('efi, i -> f',
                                             self.file_read('simulation/receiver', sl=((index),)),
                                             orx)
                del orx
            else:
                raise KeyError(self.mode)

            self.file_save(value='simulation/summed_response',
                           data=sum_r,
                           sl=((index),))

        self._simulation['summed_response'] = True
        logger.info('"simulation/summed_response" saved')
        del sum_r
    else:
        logger.info('found previous "simulation/summed_response"')

    logger.info('6) ------- End of Calculation ------- ')
    return self.file_read('simulation/summed_response')


def propagation(self, omega, src_vec, s_loc, obs_loc):
    """
    Function to compute the propagation of the electric field from a point dipole
    Args:
        omega: angular frequencies
        epsilon: Relative electrical permittivity
        sigma: Electrical conductivity
        src_vec: source_dipole
        s_loc: SourceEM location
        obs_loc: Observer location

    Returns:
        Electric field
    """
    k = self.wav_num
    R_vec = obs_loc - s_loc  # Distance vector (nelements, 3)
    R = arr.linalg.norm(R_vec, axis=1)
    RR = arr.einsum('ei, ej -> eij', R_vec, R_vec)  # (nfrequencies, nelements, 3, 3)

    @decorator_filter_errors
    def Green():
        """
        Everything is rearranged to construct tensor (traces, elements, frequencies, (3x3; green))
        Returns:
            (traces, elements, frequencies, (3x3; green))
        """
        temp1 = arr.einsum('f, e -> ef', k, R)
        temp2 = arr.einsum('f, e -> ef', k ** 2, R ** 2)
        I = numpy.identity(3)  # Identity matrix
        if self.backend == 'dask':
            I = arr.from_array(I)
            temp1 = temp1 + 0j
            temp2 = temp2 + 0j
        elif self.backend == 'torch':
            I = arr.from_numpy(I)
        first_term = arr.divide(arr.exp(1j * temp1), 4 * numpy.pi * R[:, None])
        second_term = arr.einsum('ef, ij -> efij', (1 + arr.divide(1j * temp1 - 1, temp2)), I)
        t1 = arr.divide(3 - 3j * temp1 - temp2, temp2)
        t2 = arr.divide(RR, (R ** 2)[:,None, None])
        if self.backend == 'torch': t2 = t2.type(arr.complex128)  # Operands can only operate when everythong is complex
        third_term = arr.einsum('ef, eij -> efij', t1, t2)
        return arr.einsum('ef, efij->efij', first_term, (second_term + third_term))

    efield = arr.einsum('f, efi -> efi',
                        omega ** 2 * self.m0,
                        arr.einsum('efij, efi -> efj', Green(), src_vec))
    return efield


def causality(self, omega, s_loc, obs_loc, mask):
    """
    Function to compute the causality of the electric field from a point dipole
    Args:
        omega: angular frequencies
        s_loc: SourceEM location
        obs_loc: Observer location
        mask: Mask of the simulation window
    Returns:
        mask with the elements that are outside of the time window
    """
    R_vec = obs_loc - s_loc
    R = arr.linalg.norm(R_vec, axis=1)
    cond = (arr.einsum('e, f->ef', R, self.wav_num) / omega)
    cond[arr.isnan(cond)] = 0
    cond=cond.max(axis=1)
    causality_index = cond <= (self.time_window * 1e-9)  # To have from nano to seconds
    #logger.info('Part of the signal is not causal and is omitted')
    mask *= causality_index
    return mask

def orthogonal_projection(vector_in, projection_vector):
    """
    The projection of a vector $\overrightarrow{u}$ onto a plane is calculated by
    subtracting the component of $\overrightarrow{u}$ which is orthogonal to the plane
    from $\overrightarrow{u}$.
    Args:
        vector_in:
        projection_vector:

    Returns:

    """
    divisor = arr.linalg.norm(projection_vector, axis=1)
    dot_product = arr.einsum('ei, efi -> ef', projection_vector, vector_in)
    return arr.einsum('ef,ei->efi', dot_product / divisor[:, None], projection_vector)  # equivalent to outer product

@decorator_filter_errors
def source_to_dipole(self,
                     omega,
                     area,
                     aperture,
                     fracture_epsilon,
                     fracture_sigma,
                     tangential_field_x=None,
                     tangential_field_y=None,
                     normal_field_z=None):
    """
    Calculate the induced dipole

    Args:
        omega: Angular frequency
        area: Area of fracture at each element
        aperture: Aperture of the fracture at each element
        fracture_epsilon: Electric permittivity of fracture at each element
        fracture_sigma: Electric conductivity of fracture at each element
        tangential_field_x: Projected incoming field
        tangential_field_y: Projected incoming field
        normal_field_z: Projected incoming field
    Returns:
        induced dipole

    """
    k2_frac = self.wavenumber(omega[None, :],  # To fit nelement x frequencies
                              fracture_epsilon[:, None],
                              fracture_sigma[:, None])
    fracture_complex_epsilon = self.complex_epsilon(omega[None, :],
                                                    fracture_epsilon[:, None],
                                                    fracture_sigma[:, None])

    if self.mode in ['reflection', 'full-reflection']:
        modulationTE, modulationTM = self.reflection(e1=self.rce[None, :],
                                                     e2=fracture_complex_epsilon,
                                                     k1=self.wav_num[None, :],
                                                     k2=k2_frac,
                                                     aperture=aperture[:, None])
        modulation = arr.einsum('efi, ef -> efi', tangential_field_x, modulationTE) + \
                     arr.einsum('efi, ef -> efi', tangential_field_y, modulationTE) + \
                     arr.einsum('efi, ef -> efi', normal_field_z, modulationTM)

    elif self.mode == 'transmission':
        modulation = self.transmission(k1=self.wav_num[None, :],
                                       k2=k2_frac,
                                       aperture=aperture[:, None])[..., None]
    else:
        modulation = arr.ones((fracture_epsilon.shape[0],
                               omega.shape[0],
                               3))
    return arr.einsum('e, efi -> efi',
                      area*aperture,
                      arr.divide(modulation, 1j*omega[None, :, None]))