import numpy as np
from scipy.spatial.distance import cdist

def forward_solver(self):
    """
    The dummy forward model calculates the travel time based on the geometry and the borehole.
    Args:
        self:

    Returns:
    """
    if self.solve_for_profile is not None:
        #  This means that we are going to use specific profile to compute everything
        index_traces = self.file_read(f'antenna/profiles/{self.solve_for_profile}')
    else:
        index_traces = np.arange(self.ntraces)

    element_positions = self.file_read('mesh/geometry/midpoint')
    tx = self.file_read('antenna/Tx', sl=index_traces)
    rx = self.file_read('antenna/Rx', sl=index_traces)

    t_e = cdist(tx, element_positions)
    r_e = cdist(rx, element_positions)
    distance = t_e + r_e
    time = distance / self.velocity

    # Get the travel time corresponding to the minimum distance

    first_arrival = np.min(time, axis=1)
    # find the element position
    # if self._dummy_time_response
    #     all_elements=[]
    #     for i, fa in enumerate(first_arrival):
    #         all_elements.append(np.where(time[i]==fa)[0])

    self.file_save('simulation/dummy_solution/first_arrival', first_arrival, sl=index_traces)#overwrite=True)

    # make dummy first arrival
    tw = self.file_read_attribute('source/time_window')
    time_vector = np.linspace(0,
                            tw,
                            self.nfrequencies)

    self.file_save('simulation/dummy_solution/time_vector', time_vector)#overwrite=True)

    # pick_sample = np.zeros(rows)  # Corresponding with the number of samples, traces
    #
    # # In sample?
    # for i, fa in enumerate(first_arrival):
    #     # find where the time is arriving
    #     temp = np.abs(time_vector - fa)
    #     s = np.argsort(temp)
    #     pos = np.where(s == 0)[0][0]
    #     pick_sample[i] = pos

    rows, columns = len(index_traces), self.nfrequencies
    dat = np.zeros((rows, columns))  # Corresponding with the number of samples, traces
    if self._dummy_time_response:
        dt = time_vector[1] - time_vector[0]
        from tqdm.autonotebook import tqdm
        # dat_minimum = np.zeros(rows)
        # t_minimum = np.zeros(rows)
        for r, tr in tqdm(enumerate(time), total=rows):
            # pos_t = np.where(tr == tr.min())[0][0]
            # dat_minimum[r] = pos_t
            # t_minimum[r] = tr[pos_t]
            # mask out all those elements that are outside of the time window
            tr = tr[tr < tw]
            tr = tr[tr<(first_arrival[r]+dt*20)]
            # mask out all elements that are too far away from the first arrival

            for d in tr:
                # find where the time is arriving
                temp = np.abs(time_vector - d)
                s = np.argsort(temp)
                pos = np.where(s == 0)[0][0]
                dat[r, pos] += 1
                # break

    self.file_save('simulation/dummy_solution/time_response', dat, sl=index_traces)# overwrite=True)
    return dat





