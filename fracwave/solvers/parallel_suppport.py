import os, platform, subprocess, re
import multiprocessing
import psutil
from fracwave.utils.help_decorators import convert_size


class aboutCPU:
    """Help to give information about your CPU devices for using parallel computing of dask"""

    def __repr__(self):
        memory = self.cpu_use()
        return '\nOperative system: {} \n' \
               'Model name : {} \n' \
               '    CPU core(s): {}\n' \
               '    Processes: {}\n' \
               '    Threads: {}\n' \
               'Memory:\n'\
               '    Total: {}\n'\
               '    Available: {}\n'\
               '    Used: {} ({}%)\n'\
               '    Free: {}\n'.format(self.system,
                                           self.processor_name,
                                           self.cores,
                                           self.processes,
                                           self.threads_per_core,
                                           convert_size(memory['total']),
                                           convert_size(memory['available']),
                                           convert_size(memory['used']),
                                           memory['percent'],
                                           convert_size(memory['free']))

    @property
    def cores(self):
        return multiprocessing.cpu_count()

    @property
    def system(self):
        return platform.system()

    @property
    def processor_name(self):
        if self.system == "Windows":
            return platform.processor()
        elif self.system == "Darwin":
            os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
            command = "sysctl -n machdep.cpu.brand_string"
            return subprocess.check_output(command).strip()
        elif self.system == "Linux":
            command = "cat /proc/cpuinfo"
            all_info = subprocess.check_output(command, shell=True).strip().decode()
            for line in all_info.split("\n"):
                if "model name" in line:
                    return re.sub(".*model name.*:", "", line, 1)
        return ""

    @property
    def processes(self):
        # from dask.utils import factors
        def factors(n):
            """
            Return the factors of an integer
            https://stackoverflow.com/a/6800214/616616
            """

            import functools
            seq = ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0)
            return set(functools.reduce(list.__add__, seq))
        import math
        if self.cores <= 4:
            return self.cores
        else:
            return min(f for f in factors(self.cores) if f >= math.sqrt(self.cores))
        return self.cores

    @property
    def threads_per_core(self):
        return self.cores // self.processes

    @staticmethod
    def cpu_use():
        return dict(psutil.virtual_memory()._asdict())

    @staticmethod
    def cpu_frequency():
        return psutil.cpu_freq()

    @property
    def mem_free(self):
        return self.cpu_use()['free']

class ClusterDask:
    """Creates and manages any dask process information"""

    def __init__(self, **kwargs):
        from dask.distributed import Client, LocalCluster
        """http://distributed.dask.org/en/stable/"""
        self.cluster = LocalCluster(**kwargs)
        self.client = Client(self.cluster)

    def close(self):
        """Close ports"""
        self.cluster.close()
        self.client.close()

    @property
    def workers(self):
        return len(self.client.ncores())

    @property
    def threads(self):
        return sum([val for val in self.client.nthreads().values()])

    @property
    def threads_worker(self):
        for val in self.client.nthreads().values():
            return val

    @property
    def tot_memory(self):
        inf = self.client._scheduler_identity
        workers = inf.get('workers', {})
        return sum([w["memory_limit"] for w in workers.values()])

    @property
    def memory_worker(self):
        inf = self.client._scheduler_identity
        workers = inf.get('workers', {})
        for w in workers.values():
            if w["memory_limit"]:
                return w["memory_limit"]

    @property
    def dashboard_link(self):
        return self.client.dashboard_link

    def __repr__(self):
        return '\nLocalCluster running with:\n' \
               '    Total memory: {:.3f} GB\n' \
               '    Threads: {} \n' \
               '    Workers: {}\n'\
               '        Threads per worker: {}\n'\
               '        Memory per workers: {:.3f} GB\n' \
               'Open "{}" for more detailed information.'.format(self.tot_memory / 1024 ** 3,
                                                                 self.threads,
                                                                 self.workers,
                                                                 self.threads_worker,
                                                                 self.memory_worker / 1024 ** 3,
                                                                 self.dashboard_link)