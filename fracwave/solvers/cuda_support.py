# A simple class to know about your cuda devices
from fracwave.utils.help_decorators import get_backend, convert_size
arr = get_backend()
try:
    import torch
    _torch_flag = True
except:
    _torch_flag = False
try:
    import cupy
    _cupy_flag = True
except:
    _cupy_flag = False

class aboutCudaDevices():
    """Helps to give information about your GPU devices. Only works when torch or cupy is active as backend"""
    def __init__(self):
        assert _torch_flag or _cupy_flag, 'Not possible to use GPU. Cupy or Torch not installed. '
        assert arr.cuda.is_available(), 'No GPU founded'

    @property
    def num_devices(self):
        """Return number of devices connected."""
        if arr.__name__ == 'cupy':
            return arr.cuda.runtime.getDeviceCount()
        elif arr.__name__ == 'torch':
            return torch.cuda.device_count()

    def devices(self):
        """Get info on all devices connected."""
        num = self.num_devices
        print("%d device(s) found:" % num)
        for i in range(num):
            print(torch.cuda.get_device_name(i), "(Id: %d)" % i)
        print('Current cuda device ', torch.cuda.current_device())

    def attributes(self, device_id=0):
        """Get attributes of device"""
        return torch.cuda.get_device_properties(device_id)

    def mem_info(self, device_id:int=0):
        """
        Get available and total memory of device.
        Args:
            device_id:
        Returns:
            (Total memory,
            memory allocated,
            memory reserved)
        """
        if _cupy_flag:
            mempool = cupy.get_default_memory_pool()
        # pinned_mempool = cupy.get_default_pinned_memory_pool()

            return (self.attributes(device_id).total_memory,
                    mempool.used_bytes(),
                    mempool.total_bytes())
        else:
            return (self.attributes(device_id).total_memory, 0, 0)
        # return (torch.cuda.get_device_properties(device_id).total_memory,
        #         torch.cuda.memory_allocated(device_id),
        #         torch.cuda.memory_reserved(device_id) )


    def __repr__(self):
        """Class representation as number of devices connected and about them."""
        num = self.num_devices
        string = ""
        string += "{} device(s) found:\n".format(num)
        for i in range(num):
            tmem, talloc, tcach = self.mem_info(i)
            string += 'Name: {} \n' \
                      'Total memory: {} \n' \
                      'Allocated:    {} \n' \
                      'Cached:       {} \n\n'.format(torch.cuda.get_device_name(i),
                                                        convert_size(tmem),
                                                        convert_size(talloc),
                                                        convert_size(tcach))
        return string

    @property
    def mem_free(self):
        self.release_memory()
        tmem, talloc, tcach = self.mem_info()
        return tmem - talloc


    def release_memory(self):
        """Release all unused memory held by cuda."""
        torch.cuda.empty_cache()
        if _cupy_flag:
            mempool = cupy.get_default_memory_pool()
            pinned_mempool = cupy.get_default_pinned_memory_pool()
            mempool.free_all_blocks()
            pinned_mempool.free_all_blocks()


if __name__ == '__main__':
    print(aboutCudaDevices())
