from fracwave import PACKAGE_DIR
import sys
import logging


# Record the logger of all the packages for debugging and error handling
verbose = True
if verbose:
    logging.basicConfig(filename=PACKAGE_DIR+"../main.log",
                        filemode='w',
                        level=logging.INFO,
                        format='%(asctime)s | %(name)s | %(levelname)s | %(message)s',
                        #datefmt='%Y/%m/%d %I:%M:%S %p'
                        )

# Get the root formatter
logger = logging.getLogger("fracwave")
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler(PACKAGE_DIR+'../fracwave.log', mode='w')
frm = logging.Formatter('%(asctime)s | %(name)-18s | %(levelname)-8s | %(message)s')
fh.setFormatter(frm)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

# create console handler
console = logging.StreamHandler(sys.stdout)
console.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
frm2 = logging.Formatter('%(asctime)s | %(levelname)-8s | %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
console.setFormatter(frm2)
# add the handlers to the logger
logger.addHandler(console)

def set_logger(name, level = logging.DEBUG):
    """
    Create a new handle in any destination
    Args:
        name: name of new handle
    Returns:
    """
    if name[:9] != "fracwave.":
        name = "fracwave."+name
    if not logging.getLogger(name).hasHandlers():
        logging.getLogger(name).addHandler(name)

    log = logging.getLogger(name)
    log.setLevel(level)
    return log

def set_error_level(name_level: str):
    """
    Set the logger level on the console
    Args:
        name_level: [DEBUG, INFO, WARNING, ERROR, CRITICAL]

    Returns:

    """
    n = name_level.lower()
    if n == "debug":
        level = logging.DEBUG
    elif n == "info":
        level = logging.INFO
    elif n == "warning":
        level = logging.WARNING
    elif n == "error":
        level = logging.ERROR
    elif n == "critical":
        level = logging.CRITICAL
    else:
        console.error(name_level + " not recognized. Use any of the following [DEBUG, INFO, WARNING, ERROR, CRITICAL]")
        return
    logger.info('Console output set to "{}:{}"'.format(n.upper(),level))
    console.setLevel(level)
