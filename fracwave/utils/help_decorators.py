import numpy as np
import math
from fracwave.utils.logger import set_logger

logger = set_logger(__name__)

try:
    import torch
    _torch_flag = True
except:
    logger.info('PyTorch not installed')
    _torch_flag = False

try:
    import dask.array
    _dask_flag = True
except:
    logger.info('Dask not installed')
    _dask_flag = False

try:
    import cupy
    _cupy_flag = True
except:
    logger.info('CuPy not installed')
    _cupy_flag = False

class MetaData:
    """Class containing metadata of the project.
       Set of attributes and methods that are not related directly to the fracture geometry but more with the project
       Args:
           project_name (str): Name of the project. This is used as the default value for some I/O actions
       Attributes:
           date (str): Time of the creation of the project
           project_name (str): Name of the project. This is used as the default value for some I/O actions
       """

    def __init__(self, project_name='default_project'):
        import datetime
        now = datetime.datetime.now()
        self.date = now.strftime(" %Y-%m-%d %H:%M")
        if project_name == 'default_project':
            project_name += self.date
        self.project_name = project_name


def decorator_filter_errors(func):
    """Set Nan and Inf values to 0"""

    def wrapper(*args, **kwargs):
        array = func(*args, **kwargs)
        if (not hasattr(array, '__len__')) and (not isinstance(array, str)): # Check if array is scalar value
            if _torch_flag and type(array) is torch.Tensor:
                arr = torch
            else:
                arr = np
            if arr.isnan(array): return 0
            elif arr.isinf(array): return 0
            else: return array
        if isinstance(array, tuple):
            array = list(array)
        # import torch
        if isinstance(array, list):
            if _torch_flag and type(array[0]) is torch.Tensor:
                arr = torch
            else:
                arr = np
            for i, a in enumerate(array):
                a[arr.isnan(a)] = 0
                a[arr.isinf(a)] = 0

        else:
            if _torch_flag and type(array) is torch.Tensor:
                arr = torch
            else:
                arr = np
            array[arr.isnan(array)] = 0
            array[arr.isinf(array)] = 0
        return array

    return wrapper


def convert_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    return "{:.2f} {}".format(size_bytes / p, size_name[i])

def convert_frequency(size_hertz):
    if size_hertz == 0:
        return "0Hz"
    size_name = ("Hz","KHz", "MHz", "GHz", "THz")
    i = int(math.floor(math.log(size_hertz, 1000)))
    p = math.pow(1000, i)
    return "{:.2f} {}".format(size_hertz / p, size_name[i])

def convert_meter(size_meter):
    if size_meter == 0:
        return "0 m"
    units = {
        1e9: "Gm",
        1e6: "Mm",
        1e3: "Km",
        1e0: "m",
        1e-2: "cm",
        1e-3: "mm",
        1e-6: "\u03BCm",
        1e-9: "nm",
        1e-12: "pm",
    }
    for unit_value, unit_name in units.items():
        if abs(size_meter) >= unit_value:
            converted_value = size_meter / unit_value
            return f"{converted_value:.3f} {unit_name}"


def byte_size(array):
    """

    Args:
        array:

    Returns:

    """
    if isinstance(array, np.ndarray):
        b = array.nbytes
    else:
        from sys import getsizeof
        b = getsizeof(array)
    return print('Total size: ', convert_size(b))

backend_arr = None
def set_backend(backend='numpy'):
    """
    Choose between
    'CPU' -> Numpy,
    'Parallel CPU' -> Dask,
    'GPU' -> Pytorch
    'GPU' -> Cupy
    Args:
        backend: ['numpy', 'dask', 'torch']
    Returns:
    """
    global backend_arr
    if backend not in ['numpy', 'dask', 'torch', 'cupy']:
        e = "Not possible to set {} backend. Options are ['numpy', 'dask', 'torch', 'cupy']".format(backend)
        logger.error(e)
        raise AttributeError(e)
    if backend == 'numpy':
        import numpy as arr
    elif backend == 'dask':
        import dask.array as arr
    elif backend == 'torch':
        import torch as arr
    elif backend == 'cupy':
        import cupy as arr
    backend_arr = arr
    return arr

backend_arr = set_backend('numpy')

def get_backend():
    global backend_arr
    return backend_arr

def check_and_convert_boolean_lists(sl):
    if _cupy_flag:
        ca=cupy.ndarray
    else:
        ca = np.ndarray
    if sl is None:
        sl = slice(None)
    elif isinstance(sl, list):
        if isinstance(sl[0], bool):
            indexes = np.arange(len(sl))
            sl = indexes[sl]
    elif isinstance(sl, (np.ndarray, torch.Tensor, dask.array.Array, ca)):
        if sl.dtype == 'bool':
            indexes = np.arange(len(sl))
            if _cupy_flag and isinstance(sl, cupy.ndarray):
                indexes = cupy.array(indexes)
            sl = indexes[sl]
        elif (isinstance(sl, torch.Tensor) and sl.dtype == torch.bool):
            if len(sl) == 1 and isinstance(sl, torch.Tensor):  # Meaning only 1 element, only problematic with torch
                sl = 0 #TODO: Not really working, use numpy when only 1 element
            else:
                indexes = np.arange(len(sl))
                sl = indexes[sl]

        if _cupy_flag and isinstance(sl, cupy.ndarray):
            sl = cupy.asnumpy(sl)
    return sl

def remesh_multiple_surfaces(func):
    """
    Decorator to manage either 2 surfaces or 1.
    Args:
        func: Function to be decorated
    Returns:
        Decorated function
    """
    def wrapper(self, points, *args, **kwargs):
        """

        Args:
            self: Instance of the class
            points:
            **kwargs:

        Returns:

        """
        if isinstance(points, dict):
            grid = {}
            vertices = {}
            faces = {}
            for key, v in points.items():
                gr, ve, fa = func(self, v, *args, **kwargs)
                grid[key] = gr
                vertices[key] = ve
                faces[key] = fa
            return grid, vertices, faces

        else:
            return func(self, points, *args, **kwargs)
    return wrapper

def mask_multiple_surfaces(func):
    """
    Decorator to manage either 2 surfaces or 1.
    Args:
        func: Function to be decorated
    Returns:
        Decorated function
    """
    def wrapper(self, vert, fac, *args, **kwargs):
        """

        Args:
            self: Instance of the class
            points:
            **kwargs:

        Returns:

        """
        if isinstance(vert, dict):
            grid = {}
            vertices = {}
            faces = {}
            for key in vert.keys():
                gr, ve, fa = func(self, vert[key], fac[key], *args, **kwargs)
                grid[key] = gr
                vertices[key] = ve
                faces[key] = fa
            return grid, vertices, faces

        else:
            return func(self, vert, fac, *args, **kwargs)
    return wrapper

# def conjugate_fix(func):
#     """
#     Decorator that fix the signal when is mirrored
#     Args:
#         func:
#
#     Returns:
#
#     """""
#     def wrapper(*args, **kwargs):
#         array = func(*args, **kwargs)
#         arr = get_backend()
#         return arr.conjugate(array)
#     return wrapper

def generate_points_on_plane(normal_vector, reference_point, num_points):
    # Normalize the normal vector
    normal_vector = normal_vector / np.linalg.norm(normal_vector)

    # Calculate the plane's equation coefficients (Ax + By + Cz + D = 0)
    A, B, C = normal_vector
    D = -np.dot(normal_vector, reference_point)

    # Generate points on the plane
    points = []
    for i in range(num_points):
        # Generate random values for x and y
        x = reference_point[0] + np.random.uniform(-1, 1)
        y = reference_point[1] + np.random.uniform(-1, 1)
        if i>0:
            x += np.random.uniform(-0.3, 0.3)
            y += np.random.uniform(-0.3, 0.3)
        # Calculate z coordinate using the plane's equation
        z = (-A * x - B * y - D) / C

        # Append the point to the list
        points.append((x, y, z))

    return points

def generate_tree_h5_file(filename_hdf):
    """
    Generate a tree of the hdf file structure
    Args:
        filename_hdf: Path to the hdf file
    Returns:
        None
    """
    import h5py
    def h5_tree(val, pre=''):
        items = len(val)
        for key, val in val.items():
            items -= 1
            if items == 0:
                # the last item
                if type(val) == h5py.Group:
                    print(pre + '└── ' + key + f' [Group] ({len(val)} items)')
                    h5_tree(val, pre + '    ')
                elif type(val) == h5py.Dataset:
                    print(pre + '└── ' + key + f' [Dataset: {val.shape}]')
                else:
                    print(pre + '└── ' + key + f' {val.shape}')
            else:
                if type(val) == h5py.Group:
                    print(pre + '├── ' + key + f' [Group] ({len(val)} items)')
                    h5_tree(val, pre + '│   ')
                elif type(val) == h5py.Dataset:
                    print(pre + '├── ' + key + f' [Dataset: {val.shape}]')
                else:
                    print(pre + '├── ' + key + f' {val.shape}')

    with h5py.File(filename_hdf, 'r') as hf:
        print(hf)
        h5_tree(hf)

def generate_tree_h5_file_attrs(filename_hdf):
    """
    Generate a tree of the hdf file structure showing all the attributes
    Args:
        filename_hdf: Path to the hdf file
    Returns:
        None
    """
    import h5py
    def h5_tree(val, pre=''):
        items = len(val)
        if type(val) == h5py.File:
            a = len(val.attrs)
            print(str(val) + ' [File] ({} attrs)'.format(a))
            if a > 0:
                h5_tree_attrs(val, pre + '│   ')

        for key, val in val.items():
            items -= 1
            a = len(val.attrs)
            if items == 0:
                if type(val) == h5py.Group:
                    print(pre + '└── ' + key + f' [Group] ({a} attrs)')
                    if a>0:
                        h5_tree_attrs(val, pre + '    ')
                    else:
                        h5_tree(val, pre + '    ')
                else:
                    h5_tree_attrs(val, pre + '└── ')
                    # a = len(val.attrs)
            else:
                if type(val) == h5py.Group:
                    print(pre + '├── ' + key + f' [Group] ({a} attrs)')
                    if a>0:
                        h5_tree_attrs(val, pre + '│   ')
                    else:
                        h5_tree(val, pre + '│   ')
                else:
                    h5_tree_attrs(val, pre + '│── ')

    def h5_tree_attrs(val, pre=''):
        l = len(val.attrs)
        for i, (k, v) in enumerate(val.attrs.items()):
            if i == l - 1:
                print(pre + '    └── ' + k + f': {v}')
                break
            print(pre + '    ├── ' + k + f': {v}')


    with h5py.File(filename_hdf, 'r') as hf:
        h5_tree(hf)


def loop_manager(func):
    """
    This function manages the size of the calculation according to the available memory by splitting the tensor in traces
    in a for loop. If the array is still too big, then it will be splitted by elements in another for loop so it fits in memory.

    If the array to construct is too big (too many frequencies) it will raise an MemoryError

    Returns:

    """
    def wrapped_f(self, *args, **kwargs):
        #if self.backend == 'dask':
        #    return func(self, *args, **kwargs)

        from fracwave.utils.help_decorators import convert_size
        # if self.gpu:
        #     from fracwave.solvers.cuda_support import aboutCudaDevices
        #     device = aboutCudaDevices()
        # else:
        #     from fracwave.solvers.parallel_suppport import aboutCPU
        #     device = aboutCPU()
        device = self._devices

        free_memory = device.mem_free

        # freq = np.empty((self.ntraces, self.nfrequencies))
        n_bytes = 8  # Complex64 is composed of 8 bytes or 64 bits
        # n_bytes = arr.ones(1, dtype=arr.complex128)
        size_per_freq = self.nfrequencies * 3 * 3 * n_bytes
        free = int(free_memory * 0.2)  # Use 50% of the free memory to avoid MemoryError

        if size_per_freq > free:
            s = '{}\nNot possible to allocate {} array into memory.\n' \
                'Consider reducing the amount of frequencies.'.format(device, convert_size(size_per_freq))
            logger.critical(s)
            raise MemoryError(s)

        if self.mode == 'propagation':
            size_1_trace = size_per_freq
        else:
            size_1_trace = self.nelements * size_per_freq

        # max_amount_traces = self.ntraces * size_1_trace

        if size_1_trace < free:
            freq = func(self, *args, **kwargs)
            return freq
        # elif self.engine == 'loop':
        #     freq = func(self, *args, **kwargs)
        #     return freq
        else:
            opt_elements = free // size_per_freq
            s = '{}\nToo many elements to compute in the same array.\n' \
                'Not possible to allocate {} array into memory.\n' \
                'Splitting into groups of elements...\n' \
                'Maximum amount of elements per iteration is {}'.format(device, convert_size(size_1_trace), opt_elements)
            logger.critical(s)
            arr = get_backend()
            freq = arr.zeros((self.ntraces, self.nfrequencies))
            if self.backend == 'torch':
                freq = freq.type(arr.complex64)
            else:
                freq = freq.astype(arr.complex64)
            # from tqdm.autonotebook import tqdm
            original_mask = self.file_read('simulation/mask_elements')
            for i in range(np.ceil(self.nelements/opt_elements).astype(int)):
                temp_mask = self.file_read('simulation/mask_elements')
                start = i * opt_elements
                end = start + opt_elements
                sl = slice(start, end)
                temp_mask[:]=False
                temp_mask[:, sl] = True
                temp_mask *= original_mask
                logger.info('\n####################\n'
                            'Computing elements {} to {}'
                            '\n####################\n'.format(start, end))
                self.file_save('simulation/mask_elements', temp_mask, overwrite=True)
                freq += func(self, *args, **kwargs)
                if self.gpu:
                    device.release_memory()
            return freq

    return wrapped_f


def pick_first_arrival(img: np.ndarray, threshold: float = 0.5):
    """
    Pick the first arrival point of the image, based on the cumulative energy of the trace
    Args:
        img: numpy array of the image. Columns are traces and Rows are the samples
        threshold: threshold of the image
    Returns:
        x, y: the first arrival point of the image
    """
    samples, traces = img.shape
    # d = np.diff(img.copy(), axis=0)
    d = np.abs(img.copy())
    # d = np.divide(d, _t, where=_t != 0)

    cd = np.cumsum(d, axis=0)
    _t = cd[-1]
    cd_n = np.divide(cd, _t, where=_t != 0)
    pick = []
    for trace in range(traces):
        for sample in range(samples - 1):
            if cd_n[sample, trace] > threshold:
                pick.append((trace, sample))
                break
    return np.asarray(pick)
