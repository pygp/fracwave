"""
Name of package will change in the future.

Module initialisation for FracWave
Created on 03/06/2021
@authors: Daniel Escallon, Alexis Shakas
"""
import os
PACKAGE_DIR = os.path.dirname(__file__) + os.sep

DATA_DIR = os.path.abspath(PACKAGE_DIR + '../data') + os.sep
TEMP_DATA = DATA_DIR + '.temp' + os.sep
OUTPUT_DIR = os.path.abspath(PACKAGE_DIR + '../output') + os.sep

# Create folders if not existing
if not os.path.isdir(DATA_DIR): os.mkdir(DATA_DIR)
if not os.path.isdir(TEMP_DATA): os.mkdir(TEMP_DATA)
if not os.path.isdir(OUTPUT_DIR): os.mkdir(OUTPUT_DIR)
if not os.path.isdir(OUTPUT_DIR+'tests'): os.mkdir(OUTPUT_DIR+'tests')


from fracwave.geometry import Antenna, FractureGeo
from fracwave.solvers import SourceEM, FracEM
from fracwave.utils.logger import set_logger, set_error_level