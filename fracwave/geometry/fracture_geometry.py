# https://htmlpreview.github.io/?https://raw.githubusercontent.com/andieie/t21_hacksubsurface/main/gempy_to_pygimli.html
import numpy as np
import pandas as pd
import pyvista as pv
import h5py
import scipy.interpolate as interp
from nptyping import NDArray, Int, Shape, Float
from nptyping import DataFrame, Structure as S
from typing import Any, Union, Tuple

import gstools as gs

import fracwave.geometry.vector_calculus
from fracwave.utils.logger import set_logger

logger = set_logger(__name__)

from fracwave.geometry.vector_calculus import align_plane_with_dip_and_azimuth, rotation_matrix, get_normal_vector_from_azimuth_and_dip
from fracwave.utils.help_decorators import MetaData, remesh_multiple_surfaces, mask_multiple_surfaces

try:
    import gempy as gp
    import os
    _GEMPY_FLAG = True
    from packaging import version
    if version.parse(gp.__version__) < version.parse('3.0.0'):
        _GEMPY_OLD = True
        ## Monkey-patch NumPy to avoid theano error
        # https://stackoverflow.com/questions/70839312/module-numpy-distutils-config-has-no-attribute-blas-opt-info
        try:
            np.distutils.__config__.blas_opt_info = np.distutils.__config__.blas_ilp64_opt_info
        except Exception:
            pass
        os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cpu"
        logger.warning(f"Using Gempy version {gp.__version__}. Please consider upgrading to Gempy V3.0 or higher to avoid this warning")
    else:
        _GEMPY_OLD = False
        logger.info(f"Using Gempy version {gp.__version__}")
        raise NotImplementedError('Gempy V3.0 is not fully supported yet. Wait for next release. In the mean time work with Gempy V2')
except Exception as exc_info:
    _GEMPY_FLAG = False
    logger.exception("Gempy is not installed. Please install gempy to use this module. \n",
                 exc_info=exc_info)
from scipy.spatial import KDTree



def extrapolate_point(X, Y, data, order=2):
    """
    If the point needs to be interpolated. This function will replace the nan value with the extrapolated point
    Args:
        X: mshgrid of X values
        Y: meshgrid of Y values
        data: coordinates xyz
        order: 1=Liner, 2=Quadratic, 3=Cubic
    Returns:
        Z: meshgrid of z values

    """
    XX = X.flatten()
    YY = Y.flatten()

    import scipy
    if order == 1:
        # best-fit linear plane
        A = np.c_[data[:, 0], data[:, 1], np.ones(data.shape[0])]
        C, _, _, _ = scipy.linalg.lstsq(A, data[:, 2])  # coefficients

        # evaluate it on grid
        # Z = C[0]*X + C[1]*Y + C[2]

        # or expressed using matrix/vector product
        Z = np.dot(np.c_[XX, YY, np.ones(XX.shape)], C).reshape(X.shape)

    elif order == 2:
        # best-fit quadratic curve
        # M = [ones(size(x)), x, y, x.*y, x.^2 y.^2]
        A = np.c_[np.ones(data.shape[0]), data[:, :2], np.prod(data[:, :2], axis=1), data[:, :2] ** 2]
        C, _, _, _ = scipy.linalg.lstsq(A, data[:, 2])

        # evaluate it on a grid
        Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX * YY, XX ** 2, YY ** 2], C).reshape(X.shape)

    elif order == 3:
        # M = [ones(size(x)), x, y, x.^2, x.*y, y.^2, x.^3, x.^2.*y, x.*y.^2, y.^3]
        A = np.c_[np.ones(data.shape[0]), data[:, :2], data[:, 0] ** 2, np.prod(data[:, :2], axis=1), \
                  data[:, 1] ** 2, data[:, 0] ** 3, np.prod(np.c_[data[:, 0] ** 2, data[:, 1]], axis=1), \
                  np.prod(np.c_[data[:, 0], data[:, 1] ** 2], axis=1), data[:, 1] ** 3]
        C, _, _, _ = scipy.linalg.lstsq(A, data[:, 2])

        Z = np.dot(
            np.c_[np.ones(
                XX.shape), XX, YY, XX ** 2, XX * YY, YY ** 2, XX ** 3, XX ** 2 * YY, XX * YY ** 2, YY ** 3],
            C).reshape(X.shape)
    return Z


class FractureGeo(object):
    """
    Class to manage
    """

    def __init__(self, fracture_name: str = None):
        fracture_name = 'Fracture Geometry' if fracture_name is None else fracture_name
        self._meta = MetaData(fracture_name)
        # self._dip = None
        # self._dip_dir = None
        self._vertices = None
        self._faces = None
        self._surface = None
        # self._normals = None
        self._fractures = pd.DataFrame(columns=['x',
                                                'y',
                                                'z',
                                                'azimuth',
                                                'dip',
                                                'area',
                                                'aperture',
                                                'elec_permeability',
                                                'elec_conductivity',
                                                'faces',
                                                'vertices',
                                                # 'nx',
                                                # 'ny',
                                                'nz',
                                                'name',
                                                'index',
                                                'plane'
                                                ])

        self._2_surfaces = False  # This will tell if the gempy surface representing the fracture has an aperture or not
        self.geo_model = None
        logger.info('Fracture object loaded')

    @property
    def name_fractures(self):
        return list(self.fractures['name'].unique().tolist())

    @property
    def fractures(self):
        return self._fractures.copy()
    @property
    def df(self):
        return self.fractures

    def set_fracture(self,
                     name_fracture: str,
                     vertices,
                     faces,
                     aperture,
                     electrical_conductivity,
                     electrical_permeability,
                     plane=-1,
                     overwrite=False):
        """
        Set
        Args:
            name_fracture:
            vertices:
            faces:
            aperture:
            electrical_conductivity:
            electrical_permeability:
            plane:
            overwrite:

        Returns:

        """
        if name_fracture in self.name_fractures:
            if overwrite:
                pos = 0
                for n in self.name_fractures:
                    if n == name_fracture:
                        mask = self._fractures['name'] == n
                        if self.vertices is not None:
                            old_faces = np.vstack(self._fractures.loc[mask, 'faces'])
                            mask_vertices = np.zeros(len(self.vertices), dtype=bool)
                            mask_vertices[pos:old_faces.max() + 1 + pos] = True
                            self.vertices = self.vertices[~mask_vertices]
                            self.faces = self.faces[~mask]
                        self._fractures = self._fractures.loc[~mask]
                        break
                    else:
                        faces = np.vstack(self._fractures.loc[self._fractures['name'] == n, 'faces'])
                        pos += faces.max() + 1

            else:
                s = 'Fracture name already exists. Set "overwrite=True" to replace this value'
                logger.error(s)
                raise AttributeError(s)

        vertices = np.asarray(vertices, dtype=np.float32)
        faces = np.asarray(faces, dtype=np.int32)

        if self.vertices is not None:
            self.faces = np.vstack((self.faces, faces + len(self.vertices)))
            self.vertices = np.vstack((self.vertices, vertices))
        else:
            self.faces = faces
            self.vertices = vertices
        # Check if is triangular or squared mesh
        elements = vertices[faces]
        if elements.ndim == 2:  # This means there is only 1 elements
            elements = elements[None]

        midpoints, azimuth, dip, area, nx, ny, nz = self.calc_area_and_orientation_polygon(elements)
        # from fracwave.geometry.vector_calculus import get_orientation_of_plane
        # nz, nx, ny = get_orientation_of_plane(azimuth, dip) #TODO: Deprecated
        # from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip
        # nz = get_normal_vector_from_azimuth_and_dip(azimuth, dip)
        if plane is None:
            logger.warning('Plane not set. Setting projection plane... This may take some time. '
                           'If you know your projection please set it manually. Options are 0: yz, 1: xz and  2: xy. '
                           'Set to -1 to not calculate')
            plane = self.get_projection_plane(midpoints)
        if plane not in [0, 1, 2]:
            logger.warning('Plane not stored. This may cause errors in the future. Specially in the forward simualtions.')
        df = pd.DataFrame.from_dict({'vertices': [val for val in elements],
                                     'faces': [val for val in faces],
                                     'aperture': aperture,
                                     'elec_permeability': electrical_permeability,
                                     'elec_conductivity': electrical_conductivity,
                                     'x': midpoints[:, 0],
                                     'y': midpoints[:, 1],
                                     'z': midpoints[:, 2],
                                     'azimuth': azimuth,
                                     'dip': dip,
                                     'area': area,
                                     'nx': [X for X in nx],
                                     'ny': [Y for Y in ny],
                                     'nz': [Z for Z in nz],
                                     'name': name_fracture,
                                     'plane': plane
                                     }
                                    )
        df.reset_index(inplace=True)
        self._fractures = pd.concat((self._fractures, df), ignore_index=True)
        logger.debug(f'Fracture {name_fracture} set')
        return df

    def __repr__(self):
        return f"""
        {self._meta.project_name} {self._meta.date}
            number of fractures: {len(self.name_fractures)}
            number of elements: {self.nelements}
            surface area: {self.surface_area:,.2f} (m2)
        """

    def __add__(self, otherFracture):
        """This works when a frac is holding the information of single fracture.
        When adding, all information is joined keeping the previous information """
        f = FractureGeo()
        f._vertices = np.vstack((self._vertices, otherFracture._vertices))
        f._faces = np.vstack((self._faces, otherFracture._faces))
        f._surface = self._surface + otherFracture._surface
        # self._normals = None
        f._fractures = pd.concat([self.fractures,
                                  otherFracture.fractures], ignore_index=True)
        return f

    @property
    def vertices(self):
        return self._vertices

    @vertices.setter
    def vertices(self, vertices):
        logger.debug('Vertices set')
        self._vertices = np.asarray(vertices, dtype=np.float32)

    @property
    def faces(self):
        return self._faces

    @faces.setter
    def faces(self, faces):
        logger.debug('Faces set')
        self._faces = np.asarray(faces, dtype=np.int32)

    @property
    def surface_area(self):
        a = self.fractures.get('area')
        return a.sum() if a is not None else None

    @property
    def nelements(self):
        return len(self.fractures)

    @property
    def midpoints(self):
        return self.fractures[['x', 'y', 'z']].to_numpy()

    def get_surface(self, name_fractures: Union[str, list] = 'all'):
        """

        Args:
            name_fractures:

        Returns:

        """

        if name_fractures == 'all':
            name_fractures = self.name_fractures
        elif isinstance(name_fractures, str):
            name_fractures = [name_fractures]

        s = pv.PolyData()
        pos = 0
        for n in self.name_fractures:
            df = self.fractures.loc[self.fractures['name'] == n]
            # faces = np.hstack([np.hstack([len(f), f]) for f in df['faces']])
            faces = np.vstack(df['faces'].to_numpy()) #df['faces']
            if n in name_fractures:
                vertices = self.vertices[pos: faces.max() + 1 + pos]
                s_temp = self.create_pyvista_mesh(vertices=vertices,
                                                  faces=faces,
                                                  aperture=df['aperture'],
                                                  elec_permeability=df['elec_permeability'],
                                                  elec_conductivity=df['elec_conductivity'],
                                                  name=n)
                # s_temp = pv.PolyData(vertices, faces)
                # s_temp['aperture'] = df['aperture']
                # s_temp['elec_permeability'] = df['elec_permeability']
                # s_temp['elec_conductivity'] = df['elec_conductivity']
                # s_temp['name'] = [n for _ in range(len(df))]
                s += s_temp
            pos += faces.max() + 1
        self._surface = s
        return self._surface

    def create_pyvista_mesh(self,
                            vertices: NDArray[Shape["* vertices, 3"], Float],
                            faces: NDArray[Shape["* faces, 3"], Float],
                            **kwargs_properties) -> pv.PolyData:
        """
        Create a pyvista mesh using the vertices and faces.
        You can pass arrays to be added to the field as common arguments
        Args:
            vertices:
            faces:
            **kwargs_properties:
                Scalar field values (e.g. aperture, elec_permeability, name)

        Returns:
            Pyvista PolyData
        """
        facest = np.hstack([np.hstack([len(f), f]) for f in faces])
        s_temp = pv.PolyData(vertices, facest)
        for key, val in kwargs_properties.items():
            if isinstance(val, pd.Series):
                val = val.to_numpy()
            if len(val) == len(vertices) or len(val) == len(faces):
                s_temp[key] = val
            else:
                s_temp[key] = [val for _ in range(len(vertices))]
        return s_temp

    def init_geo_model(self, extent, resolution=(50, 50, 50),
                       ):
        """
        Run this function to create a gempy geo_model.
        Args:
            extent: (xmin,xmax,ymin,ymax,zmin,zmax) of the fracture
            resolution: Resolution of the geometry

        Returns:

        """
        if _GEMPY_OLD:
            geo_model = gp.create_model()
            geo_model = gp.init_data(geo_model, extent=extent, resolution=resolution)
            gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
            geo_model.add_surfaces(['A', "basement"])
        else:
            geo_model = gp.create_geomodel(extent=extent, resolution=resolution,
                                           structural_frame=gp.data.StructuralFrame.initialize_default_structure())

        self.geo_model = geo_model
        self._2_surfaces = False

    def init_2surface_geo_model(self, extent, resolution=(50, 50, 50)):
        """
        Run this function to create a gempy geo_model with 2 surfaces representing fracture aperture.
        Args:
            extent: (xmin,xmax,ymin,ymax,zmin,zmax) of the fracture
            resolution: Resolution of the geometry
        Returns:

        """
        if _GEMPY_OLD:
            geo_model = gp.create_model()
            geo_model = gp.init_data(geo_model, extent=extent, resolution=resolution)
            gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
            geo_model.add_surfaces(['A', 'B', 'basement'])
        else:
            geo_model = gp.create_geomodel(extent=extent, resolution=resolution,
                                           structural_frame=gp.data.StructuralFrame.initialize_default_structure())

        self.geo_model = geo_model
        self._2_surfaces = True

    def check_minimum_area(self, wavelength):
        """
        Sanity check to verify that each dipole element in the geometry
        is at least 1/4 of the wavelength from the source signal.
        The way I am checking is looking at the distance between vertices of the element.
        This should be the same or smaller than 1/4 of the wavelength.
        Otherwise, this will result in numerical errors.

        Args:
            wavelength: wavelength in meters.
        Returns:

        """
        from scipy.spatial.distance import euclidean
        minimum = wavelength / 4
        faces_too_big = {}
        for count, face in enumerate(self.faces):
            ver = self.vertices[face]
            for i in range(len(ver)):
                d = euclidean(ver[i - 1], ver[i])
                if d > minimum:
                    faces_too_big[count] = d
                    break
        if len(faces_too_big) > 0:
            logger.warning(f'Some elements are too big. Maximum size per element should be {minimum}. '
                           f'\n Check faces number: \n{str(faces_too_big)}')
            return faces_too_big
        else:
            logger.info(f'All elements smaller than maximum element size of {minimum}')
            return

    def set_fracture_properties(self,
                                aperture: float or list = 0.0005,
                                electrical_permeability: float or list = 81,
                                electrical_conductvity: float or list = 0.001) -> pd.DataFrame:
        """

        Args:
            aperture: aperture fault [m]
            electrical_permeability: relative elecrical permeability
            electrical_conductvity: Conductivity [S/m]
        Returns:
            pd.Dataframe: fracture_properties with keys: ['vertices', 'faces', 'aperture', 'elec_permeability',
                                            'elec_conductivity', 'x', 'y', 'z', 'azimuth', 'dip', 'area']

        """
        logger.debug('Assigning fracture properties for all elements...')
        if len(self.fracture_properties) > 0:  # Means that is not empty and will raise an error in the future
            self.fracture_properties = pd.DataFrame(columns=['x',
                                                             'y',
                                                             'z',
                                                             'azimuth',
                                                             'dip',
                                                             'area',
                                                             'aperture',
                                                             'elec_permeability',
                                                             'elec_conductivity',
                                                             'faces',
                                                             'vertices',
                                                             'nx',
                                                             'ny',
                                                             'nz'
                                                             ])
        # Check if is triangular or squared mesh
        elements = self.vertices[self.faces]
        if elements.ndim == 2:  # This means there is only 1 elements
            elements = elements[None]
        self.fracture_properties['vertices'] = [val for val in elements]
        self.fracture_properties['faces'] = [val for val in self.faces]
        self.fracture_properties['aperture'] = aperture
        self.fracture_properties['elec_permeability'] = electrical_permeability
        self.fracture_properties['elec_conductivity'] = electrical_conductvity

        # arr = np.asarray([self.calc_area_and_orientation_polygon(points) for points in elements])
        midpoints, azimuth, dip, area, nx, ny, nz = self.calc_area_and_orientation_polygon(elements)

        self.fracture_properties['x'] = midpoints[:, 0]
        self.fracture_properties['y'] = midpoints[:, 1]
        self.fracture_properties['z'] = midpoints[:, 2]
        self.fracture_properties['azimuth'] = azimuth
        self.fracture_properties['dip'] = dip
        self.fracture_properties['area'] = area

        # from fracwave.geometry.vector_calculus import get_orientation_of_plane
        # nx, ny, nz = get_orientation_of_plane(azimuth, dip)
        self.fracture_properties['nx'] = [X for X in nx]
        self.fracture_properties['ny'] = [Y for Y in ny]
        self.fracture_properties['nz'] = [Z for Z in nz]
        logger.debug('Fracture properties set')
        return self.fracture_properties

    def create_regular_squared_mesh(self,
                                    width: float,
                                    length: float,
                                    resolution: [int, int],
                                    dip: float,
                                    azimuth: float,
                                    center: tuple = (0,0,0),
                                    **kwargs):
        """
        Create a regular grid with squared elements of width and length centered at origin (0,0,0)
        and moved from radius from axis.
        Args:
            width: extent in x direction
            length: extent in y direction
            resolution: discretization of width and length
            dip: inclination of the plane from [0,1,0]
            azimuth: inclination of plane form [1,0,0]
            center: separation from origin (0,0,0)
            kwargs: use_dask= False, for parallel computing
        Returns:
            vertices, faces
        """
        lb = length / 2
        wb = width / 2

        X = np.array([-wb, wb, resolution[1]])
        Y = np.array([-lb, lb, resolution[0]])

        vertices = align_plane_with_dip_and_azimuth(X, Y, dip, azimuth)

        vertices[:, 0] += center[0]
        vertices[:, 1] += center[1]
        vertices[:, 2] += center[2]
        # self.vertices = vertices
        faces = self._squared_element_topology(resolution[0], resolution[1], **kwargs)
        # self.dip = np.ones(len(self.faces)) * dip
        # self.azimuth = np.ones(len(self.faces)) * azimuth
        return vertices, faces

    @staticmethod
    def _squared_element_topology(nx: int, ny: int, use_dask: bool = False):
        """
        Function that return the element topology of the vertices.
        It assumes the first index the lower left point and then counts by rows each column
        until upper right most point.
        Args:
            nx: resolution,  number of elements in xi direction (-1)
            ny: resolution, number of element in eta direction (-1)
            use_dask: run the loop in parallel
        Returns:
            IEN: Mesh counter counterclockwise indicating the face of the mesh
        """

        # Utility functions
        def face(column, row):
            """ Create a single face """
            return (column * ny + row,
                    (column + 1) * ny + row,
                    (column + 1) * ny + 1 + row,
                    column * ny + 1 + row)

        # if DASK_INSTALLED and use_dask:
        #    # TODO: this step is taking 100 times more than normal
        #    co = [delayed(face)(x, y) for y in range(nx-1) for x in range(ny-1)]
        #    IEN = da.compute(*co)
        # else:
        #    IEN = [face(x, y) for y in range(nx-1) for x in range(ny-1)]
        IEN = [face(x, y) for y in range(nx - 1) for x in range(ny - 1)]
        return np.asarray(IEN)

    def create_regular_triangular_mesh(self,
                                       width: float,
                                       length: float,
                                       resolution: [int, int],
                                       dip: float,
                                       azimuth: float,
                                       radius: float,
                                       axis: int = 0,
                                       **kwargs):
        """
        Construct triangular grid representing a finite fault
        Args:
            width: extent in x direction
            length: extent in y direction
            resolution: discretization of width and length
            dip: inclination of the plane from [0,1,0]
            azimuth: inclination of plane form [1,0,0]
            radius: separation from origin (0,0,0)
            axis: 0 -> x, 1 -> y, 2 -> z
            kwargs: use_dask= False, for parallel computing

        Returns:
            vertices, faces

        """
        lb = length / 2
        wb = width / 2

        X = np.array([-lb, lb, resolution[0]])
        Y = np.array([-wb, wb, resolution[1]])

        vert = align_plane_with_dip_and_azimuth(X, Y, dip, azimuth)

        vert[:, axis] = vert[:, axis] + radius
        # Triangulation of points
        vert, fac, self.normals = self._point_cloud_2d_triangulation(vert)
        # self.vertices = vert
        # self.faces = fac
        # self.dip = np.ones(len(self.faces)) * dip
        # self.azimuth = np.ones(len(self.faces)) * azimuth
        return vert, fac

    def create_irregular_triangular_mesh(self,
                                         width: float,
                                         length: float,
                                         dip: float,
                                         azimuth: float,
                                         radius: float,
                                         axis: int = 0,
                                         **kwargs):
        """
        Construct triangular grid representing a finite fault
        Args:
            width: extent in x direction
            length: extent in y direction
            mesh_size: size of the elements in mesh
            dip: inclination of the plane from [0,1,0]
            azimuth: inclination of plane form [1,0,0]
            radius: separation from origin (0,0,0)
            axis: 0 -> x, 1 -> y, 2 -> z
            kwargs: use_dask= False, for parallel computing

        Returns:
            vertices, faces

        """
        lb = length / 2
        wb = width / 2

        X = np.array([-lb, lb])
        Y = np.array([-wb, wb])

        xyz = np.array([[X[0], Y[0], 0],
                        [X[0], Y[1], 0],
                        [X[1], Y[1], 0],
                        [X[1], Y[0], 0]]).T

        from fracwave.geometry.vector_calculus import arbitrary_rotation
        rot_dip = arbitrary_rotation(np.array([dip]), [0, 1, 0]).squeeze()
        # Change the sign of the azimuth degree to rotate the matrix clockwise following the geological convention
        # Subtract 90 degrees so the North is the 0 degree angle
        new_azimuth = (azimuth - 90) * -1
        rot_azimuth = arbitrary_rotation(np.array([new_azimuth]), [0, 0, 1]).squeeze()

        vert = np.matmul(rot_azimuth, np.matmul(rot_dip, xyz)).T

        vert[:, axis] = vert[:, axis] + radius

        # Triangulation of points using gmsh
        vert, fac = self._construct_polygon(vert, **kwargs)
        # self.vertices = vert
        # self.faces = fac
        # self.dip = np.ones(len(self.faces)) * dip
        # self.azimuth = np.ones(len(self.faces)) * azimuth
        return vert, fac

    def _construct_polygon(self, vertices, mesh_size, refine=False):
        """
        TODO: DEPRECATED
        Args:
            vertices:
            mesh_size:

        Returns:

        """
        import pygmsh
        with pygmsh.occ.Geometry() as geom:

            if refine:
                s1 = geom.add_polygon(vertices)  # , mesh_size=mesh_size)
                if callable(refine):
                    geom.set_mesh_size_callback(refine)
                else:
                    geom.set_mesh_size_callback(
                        # lambda dim, tag, x, y, z:  abs(np.sqrt(x ** 2 + y ** 2 + z ** 2) - 0.5) + 0.1
                        lambda dim, tag, x, y, z: abs(np.sqrt((x - 5) ** 2 + z ** 2)) + mesh_size
                    )
            else:
                s1 = geom.add_polygon(vertices, mesh_size=mesh_size)
            mesh = geom.generate_mesh()
        points = mesh.points
        faces = mesh.cells_dict['triangle']
        self._mesh = mesh
        return points, faces

    @staticmethod
    def calc_area_and_orientation_polygon(points):
        """
        Calculate the area, centroid (x,y,z, coordinates), dip and azimuth of the polygon.
        3 points is a triangle and 4 points is a square.
        Any amount of points is valid as long as the points are oriented counter or clockwise.
        Args:
            points: (x,y,z)

        Returns:
            x: x coordinate of the center of the polygon
            y: y coordinate of the center of the polygon
            z: z coordinate of the center of the polygon
            azimuth: Dip direction of the polygon
            dip: dip angle of the polygon
            area: surface area of the polygon

        """
        if points.ndim == 2:  # This means there is only 1 element
            points = points[None]
        ele, npo, coord = points.shape
        if npo < 3:  #
            logger.error('Not a plane - no area: %s' % npo)
            raise ValueError

        total = np.zeros((ele, coord))
        for i in range(npo):
            vi1 = points[:, i, :]
            vi2 = points[:, (i + 1) % npo, :]

            prodi = np.cross(vi1, vi2, axis=1)
            total[:, 0] += prodi[:, 0]
            total[:, 1] += prodi[:, 1]
            total[:, 2] += prodi[:, 2]

        normal = total / np.linalg.norm(total, axis=1)[:, None]
        # result = np.dot(total, normal)
        area = 0.5 * np.abs(np.einsum('ei,ei->e', total, normal))
        # if normal[-1] < 0:
        #     normal *= -1
        from fracwave.geometry.vector_calculus import get_azimuth_and_dip_from_normal_vector, get_nx_ny_from_nz
        nx, ny, nz = get_nx_ny_from_nz(normal)
        azimuth, dip = get_azimuth_and_dip_from_normal_vector(normal)
        midpoints = points.mean(axis=1)
        return midpoints, azimuth, dip, area, nx, ny, nz

    # @staticmethod
    # def _calc_area_and_orientation_polygon(points):
    #     """
    #     Calculate the area, centroid (x,y,z, coordinates), dip and azimuth of the polygon.
    #     3 points is a triangle and 4 points is a square.
    #     Any amount of points is valid as long as the points are oriented counter or clockwise.
    #     Args:
    #         points: (x,y,z)
    #
    #     Returns:
    #         x: x coordinate of the center of the polygon
    #         y: y coordinate of the center of the polygon
    #         z: z coordinate of the center of the polygon
    #         azimuth: Dip direction of the polygon
    #         dip: dip angle of the polygon
    #         area: surface area of the polygon
    #
    #     """
    #     if not isinstance(points, np.ndarray):
    #         points = np.asarray(points)
    #     npo = len(points)
    #     if npo < 3:  #
    #         logger.error('Not a plane - no area: %s' % npo)
    #         return 0.0
    #
    #     total = [0, 0, 0]
    #     for i in range(npo):
    #         vi1 = points[i]
    #         vi2 = points[(i + 1) % npo]
    #
    #         prod = np.cross(vi1, vi2)
    #         total[0] += prod[0]
    #         total[1] += prod[1]
    #         total[2] += prod[2]
    #
    #     normal = total / np.linalg.norm(total)  # unitary normal vector
    #     result = np.dot(total, normal)
    #     area = abs(result / 2)
    #
    #     if normal[-1] < 0:
    #         normal *= -1
    #
    #     from fracwave.geometry.vector_calculus import get_azimuth_and_dip_from_normal_vector
    #     azimuth, dip = get_azimuth_and_dip_from_normal_vector(normal)
    #     x, y, z = sum(points) / len(points)
    #
    #     return x, y, z, azimuth, dip, area

    @remesh_multiple_surfaces
    def remesh_automatic(self, points, spacing=[10, 10], order=2, max_element_size: float = None,
               extrapolate: bool = False):
        """
        Take a point cloud and create a mesh. The point cloud should be as close as posisble to a plane.
        The specified plane is like projecting the geometry to x, y or z
        Args:
            points: Pointcloud with x,y,z coordinates
            plane: projection onto plane 0 -> yz, 1 -> xz, 2 -> xy
            spacing: Amount of elements in x and y direction
            order: If values need to be extrapolated use a 1-linear,  2-quadratic, 3-cubic fit
            max_element_size: Check if the element is bigger than the min_resolution, otherwise refine the mesh automatically
            extrapolate: Since we are filling up a grid, some values does not have values to interpolate, so we do an
                extrapolation of the points based on the internal points. False will return a mesh with holes
        Returns:

        """


        mean = np.mean(points, axis=0)
        points_temp = points - mean
        U, S, V = np.linalg.svd(points_temp)
        normal_vector = V[-1]
        target_normal = np.array([0, 0, 1])
        # Get the rotation axis and angle between the two normal vectors
        rot_axis = np.cross(normal_vector, target_normal)
        theta = np.arccos(
            np.dot(normal_vector, target_normal) / (np.linalg.norm(normal_vector) * np.linalg.norm(target_normal)))

        rot_matrix = rotation_matrix(theta, rot_axis)
        rotated_points = np.dot(points_temp, rot_matrix)

        if max_element_size is not None and isinstance(max_element_size, (int, float)):
            x = rotated_points[:, 0].max() - rotated_points[:, 0].min()
            y = rotated_points[:, 1].max() - rotated_points[:, 1].min()

            min_resol_h = np.ceil(x / max_element_size).astype(int)
            min_resol_v = np.ceil(y / max_element_size).astype(int)

            if spacing[0] < min_resol_h:
                logger.debug(f'horizontal resolution too big ({spacing[0]}) setting to {min_resol_h} ...')
                spacing[0] = min_resol_h
            if spacing[1] < min_resol_v:
                logger.debug(f'vertical resolution too big ({spacing[1]}) setting to {min_resol_v} ...')
                spacing[1] = min_resol_v

        logger.debug(f'Meshing to resolution: {spacing}...')
        x = np.linspace(rotated_points[:, 0].min(), rotated_points[:, 0].max(), spacing[0])
        y = np.linspace(rotated_points[:, 1].min(), rotated_points[:, 1].max(), spacing[1])
        xx, yy = np.meshgrid(x, y)

        interpolator = interp.CloughTocher2DInterpolator(np.c_[rotated_points[:, 0], rotated_points[:, 1]], rotated_points[:, 2])
        # interpolator = interp.LinearNDInterpolator(np.c_[points[:, 0], points[:, 1]], points[:, 2])
        zz = interpolator(xx, yy)

        grid = pv.StructuredGrid(xx, yy, zz)
        if True in np.isnan(zz):
            if extrapolate:
                Z = extrapolate_point(X=xx, Y=yy,
                                      data=np.c_[points[:, 0], points[:, 1], points[:, 2]],
                                      order=order)
                mask = np.isnan(zz)
                zz[mask] = Z[mask]
            else:
                faces = np.asarray([grid.cell_point_ids(i) for i in range(grid.GetNumberOfCells())])
                # faces = np.asarray([grid.get_cell(i).points_ids for i in range(grid.GetNumberOfCells())])
                pos = np.argwhere(np.isnan(grid.points))[:, 0]

                face_id = [i for i, f in enumerate(faces) if len(np.intersect1d(pos, f)) > 0]
                grid = grid.cast_to_unstructured_grid()
                grid = grid.remove_cells(face_id)

        grid.rotate_vector(rot_axis, np.rad2deg(theta), inplace=True)
        grid.translate(mean, inplace = True)
        logger.debug(f'Meshing done')
        vertices, faces = self.extract_vertices_and_faces_from_pyvista_mesh(geom=grid)
        return grid, vertices, faces

    @remesh_multiple_surfaces
    def remesh(self, points, plane=None, spacing=[10, 10], order=2, max_element_size: float = None,
               extrapolate: bool = False) -> Tuple[pv.StructuredGrid, NDArray, NDArray]:
        """
        Take a point cloud and create a mesh. The point cloud should be as close as posisble to a plane.
        The specified plane is like projecting the geometry to x, y or z
        Args:
            points: Pointcloud with x,y,z coordinates
            plane: projection onto plane 0 -> yz, 1 -> xz, 2 -> xy
            spacing: Amount of elements in x and y direction
            order: If values need to be extrapolated use a 1-linear,  2-quadratic, 3-cubic fit
            max_element_size: Check if the element is bigger than the min_resolution, otherwise refine the mesh automatically
            extrapolate: Since we are filling up a grid, some values does not have values to interpolate, so we do an
                extrapolation of the points based on the internal points. False will return a mesh with hole
        Returns:
            Pyvista mesh, vertices and faces

        """

        def mesh(points, plane, spacing, order, extrapolate):
            if plane == 2:
                x = np.linspace(points[:, 0].min(), points[:, 0].max(), spacing[0])
                y = np.linspace(points[:, 1].min(), points[:, 1].max(), spacing[1])
                xx, yy = np.meshgrid(x, y)

                interpolator = interp.CloughTocher2DInterpolator(np.c_[points[:, 0], points[:, 1]], points[:, 2])
                # interpolator = interp.LinearNDInterpolator(np.c_[points[:, 0], points[:, 1]], points[:, 2])
                zz = interpolator(xx, yy)
                if True in np.isnan(zz) and extrapolate:
                    Z = extrapolate_point(X=xx, Y=yy,
                                          data=np.c_[points[:, 0], points[:, 1], points[:, 2]],
                                          order=order)
                    mask = np.isnan(zz)
                    zz[mask] = Z[mask]

            elif plane == 1:
                x = np.linspace(points[:, 0].min(), points[:, 0].max(), spacing[0])
                z = np.linspace(points[:, 2].min(), points[:, 2].max(), spacing[1])
                xx, zz = np.meshgrid(x, z)
                interpolator = interp.CloughTocher2DInterpolator(np.c_[points[:, 0], points[:, 2]], points[:, 1])
                # interpolator = interp.LinearNDInterpolator(np.c_[points[:, 0], points[:, 2]], points[:, 1])
                yy = interpolator(xx, zz)

                if True in np.isnan(yy) and extrapolate:
                    Y = extrapolate_point(X=xx, Y=zz,
                                          data=np.c_[points[:, 0], points[:, 2], points[:, 1]],
                                          order=order)
                    mask = np.isnan(yy)
                    yy[mask] = Y[mask]

            elif plane == 0:
                y = np.linspace(points[:, 1].min(), points[:, 1].max(), spacing[0])
                z = np.linspace(points[:, 2].min(), points[:, 2].max(), spacing[1])
                yy, zz = np.meshgrid(y, z)

                interpolator = interp.CloughTocher2DInterpolator(np.c_[points[:, 1], points[:, 2]], points[:, 0])
                # interpolator = interp.LinearNDInterpolator(np.c_[points[:, 1], points[:, 2]], points[:, 0])
                xx = interpolator(yy, zz)

                if True in np.isnan(xx) and extrapolate:
                    X = extrapolate_point(X=yy, Y=zz,
                                          data=np.c_[points[:, 1], points[:, 2], points[:, 0]],
                                          order=order)
                    mask = np.isnan(xx)
                    xx[mask] = X[mask]

            geom = pv.StructuredGrid(xx, yy, zz)
            if True in np.isnan((xx, yy, zz)):
                geom = geom.cast_to_unstructured_grid()
                center = geom.cell_centers()
                ghosts = np.argwhere(np.isnan(center.points))
                geom = geom.remove_cells(ghosts)
            return geom
        if plane is None:
            logger.info('No plane provided! It can have a wrong decision. This can take a while...')
            plane = self.get_projection_plane(points)
            logger.info(f'Plane of projection={plane}')

        if max_element_size is not None and isinstance(max_element_size, (int, float)):
            x = points[:, 0].max() - points[:, 0].min()
            y = points[:, 1].max() - points[:, 1].min()
            z = points[:, 2].max() - points[:, 2].min()

            if plane == 0:  # yz
                min_resol_h = np.ceil(y / max_element_size).astype(int)
                min_resol_v = np.ceil(z / max_element_size).astype(int)
            elif plane == 1:  # xz
                min_resol_h = np.ceil(x / max_element_size).astype(int)
                min_resol_v = np.ceil(z / max_element_size).astype(int)
            elif plane == 2:  # xy
                min_resol_h = np.ceil(x / max_element_size).astype(int)
                min_resol_v = np.ceil(y / max_element_size).astype(int)
            else:
                raise AttributeError(f'plane {plane} not accepted. Use plane = 0,1, or 2')
            if spacing[0] < min_resol_h:
                logger.debug(f'horizontal resolution too big ({spacing[0]}) setting to {min_resol_h} ...')
                spacing[0] = min_resol_h
            if spacing[1] < min_resol_v:
                logger.debug(f'vertical resolution too big ({spacing[1]}) setting to {min_resol_v} ...')
                spacing[1] = min_resol_v

        logger.debug(f'Meshing to resolution: {spacing}...')
        grid = mesh(points=points, plane=plane, spacing=spacing, order=order, extrapolate=extrapolate)
        logger.debug(f'Meshing done')
        vertices, faces = self.extract_vertices_and_faces_from_pyvista_mesh(geom=grid)
        return grid, vertices, faces


    def refine_mesh(self, vertices, faces, max_element_size):
        pass

    @mask_multiple_surfaces
    def mask_mesh(self,
                  vertices: NDArray[Shape["* vertices, 3"], Float]=None,
                  faces: NDArray[Shape["* faces, 3"], Float]=None,
                  mask: NDArray[Shape["* faces, 3"], Float] = None,
                  from_maximum_distance_borehole: Float = None,
                  from_maximum_distance_surface: Float = None,
                  positions: NDArray[Shape["* boreholepositions, 3"], Float] =None,
                  grid:pv.StructuredGrid=None,
                  ) -> Tuple[pv.PolyData, NDArray, NDArray]:
        """
        Mask a pyvista mesh
        Args:
            vertices:
            faces:
            mask: If mask is passed then no calculation is performed
            from_maximum_distance_borehole: Filters out based on the radial distance from the borehole positions
            from_maximum_distance_surface: Filters based on the reflection point on the surface at a radial distance
            positions: Position of the boreholes

        Returns:
            Pyvista mesh, vertives, faces

        """
        if grid is None:
            grid = self.create_pyvista_mesh(vertices, faces)
        if mask is not None:
            assert len(mask) == grid.n_cells, "Number of mask elements need " \
                                            f"to be the same as faces. {len(mask)} != {len(faces)}"
            grid.remove_cells(mask, inplace=True)

        else:
            if from_maximum_distance_borehole is not None or from_maximum_distance_surface:

                centers =  grid.cell_centers().points
                # from scipy.spatial.distance import cdist
                mask = np.zeros(len(centers), dtype=bool)
                # dist = cdist(positions, centers)
                if from_maximum_distance_borehole is not None:
                    tree1 = KDTree(positions)
                    dists1, _ = tree1.query(centers, workers=-1)  # , distance_upper_bound=from_maximum_distance_traveltime)
                    mask |= (dists1 < from_maximum_distance_borehole)
                    # mask |= (dist < from_maximum_distance_traveltime).sum(axis=0).astype(bool)

                if from_maximum_distance_surface:
                    # Get the minimum position = Reflection point
                    tree2 = KDTree(centers)
                    _, min_idx = tree2.query(positions, workers=-1)
                    min_idx = np.unique(min_idx)
                    tree3 = KDTree(centers[min_idx])
                    dists3, _ = tree3.query(centers, workers=-1)
                    mask |= (dists3 < from_maximum_distance_surface)
                    # min_dist = np.unique(np.argmin(dist, axis=1))
                    # points = centers[min_dist]
                    # dist2 = cdist(points, centers)
                    # mask |= (dist2 < from_maximum_distance_surface).sum(axis=0).astype(bool)
                grid = grid.remove_cells(~mask)
        vertices, faces = self.extract_vertices_and_faces_from_pyvista_mesh(grid)
        return grid, vertices, faces




    @staticmethod
    def _point_cloud_2d_triangulation(points, decimation=False, target_reduction=0.7):
        """
        Triangulate a 3D point cloud by a surface
        Args:
            points: [x, y, z]
            decimation: reduce the amount of elements
            target_reduction: percentage of the mesh to reduce
        Returns:
            IEN: connectivity array

        """
        # from scipy.spatial import Delaunay
        # tri = Delaunay(d_loc[:, :2])
        import pyvista as pv
        cloud = pv.PolyData(points)
        surf = cloud.delaunay_2d()
        if decimation:
            surf = surf.decimate(target_reduction)
        vertices = np.asarray(surf.points)
        # VTK convention [cell0_nverts, cell0_v0, cell0_v1, cell0_v2, cell1_nverts, ...]
        faces = np.asarray(surf.faces).reshape((-1, 4))[:, 1:]
        normals = np.asarray(surf.point_normals)

        return vertices, faces, normals

    def _generate_heterogeneous_apertures(self, width: float,
                                          length: float,
                                          resolution: [int, int],
                                          roughness: float,
                                          b_mean: float,
                                          correlation_length: float,
                                          H: float):
        """
        Generation of random fractures with different apertures using correlated random fields.
        Args:
            width: of fracture
            length: of fracture
            resolution: Amount of elements in each axis
            roughness: roughness of the surfaces
            b_mean: Mean aperture distance
            correlation_length: This corresponds roughly to the distance between two summits or two valleys.
            H: Hurst exponent. H=1 corresponds to regular surfaces with Gaussian correlations, while the surfaces
                are self-affine for H<1.
        Returns:

        """
        lb = length / 2
        wb = width / 2

        extent = [-wb, wb, -lb, lb]

        X = np.random.randn(*resolution)

        top_surface = self.generate_crf(X, extent, roughness, correlation_length, H)

        X = np.random.randn(*resolution)
        bottom_surface = self.generate_crf(X, extent, roughness, correlation_length, H)

        apertures = top_surface + b_mean - bottom_surface
        apertures[apertures < 0] = 0
        return apertures

    def generate_heterogeneous_apertures(self,
                                         b_mean: float=0.3,
                                         seeds=1234,
                                         **kwargs) -> NDArray:
        """
        Arg:
            b_mean: Mean aperture between surfaces
            seed2: Random seed. Can be a tuple or single seed
            kwargs:
                midpoints: Point cloud
                var: Variance
                len_scale: Correlation lenght
                plane: Plane for interpolation (0,1 or 3)
                model: 'exponential' or 'gaussian'
            optional kwargs: (Check https://geostat-framework.readthedocs.io/projects/gstools/en/stable/examples/01_random_field/02_fancier.html)
                angles: In case we want to create some anisotropy
                ani:
        Returns:

        """
        if isinstance(seeds, int):
            from gstools.random import MasterRNG
            s = MasterRNG(seeds)
            seeds = (s(), s())

        top_surface, srft = self.generate_crf(seed=seeds[0], **kwargs)
        bottom_surface, srfb = self.generate_crf(seed=seeds[1], **kwargs)

        apertures = top_surface + b_mean - bottom_surface
        apertures[apertures < 0] = 0
        return apertures

    def generate_crf(self, points: NDArray,
                     var: float=0.1,
                     len_scale: float=5,
                     plane: int=None,
                     seed: int=1234,
                     model: str='exponential',
                     **kwargs) -> Tuple[NDArray, gs.SRF]:
        """

        Args:
            points: Point cloud
            var: Variance
            len_scale: Correlation lenght
            plane: Plane for interpolation (0,1 or 3)
            seed: Random seed
            model: 'exponential' or 'gaussian'
            kwargs: (Check https://geostat-framework.readthedocs.io/projects/gstools/en/stable/examples/01_random_field/02_fancier.html)
                angles: In case we want to create some anisotropy
                ani:
        Returns:

        """
        if plane is None:
            logger.info('No plane provided! It may have a wrong decision. This can take a while...')
            plane = self.get_projection_plane(points)
            logger.info(f'Plane of projection={plane}')

        if plane == 0:
            x = points[:, 1]
            y = points[:, 2]
        elif plane == 1:
            x = points[:, 0]
            y = points[:, 2]
        elif plane == 2:
            x = points[:, 0]
            y = points[:, 1]
        else:
            raise AttributeError('Plane should be 0, 1, or 2')


        logger.debug('Generating random field...')
        if model == 'exponential':
            model = gs.Exponential(dim=2, var=var, len_scale=len_scale, **kwargs)  # , angles=np.pi / 8)
        elif model == 'gaussian':
            model = gs.Gaussian(dim=2, var=var, len_scale=len_scale, **kwargs)  # , angles=np.pi / 8)
        else:
            if isinstance(model, str):
                raise AttributeError(f"model {model} not valid. Choose between 'exponential' or 'gaussian' or pass your own model")
            else:
                model = model(dim=2, var=var, len_scale=len_scale, **kwargs)

        srf = gs.SRF(model, seed=seed)
        field = srf((x, y))
        logger.debug('Done generation random field.')
        return field, srf

    def _generate_crf(self, X, extent, sh, cl, H):
        """
        Generate a correlate random field for the creation of fracture geometries
        Args:
            X: random surface from (X = np.random.randn(length, width)) with the size of the fracture
            extent: extent of the fracture [-x, x, -y, y]
            sh: roughness of the surfaces (sh**2 = variance)
            cl: Upper cut-off length for self-affinity, above which all correlations disappear.
            H: Hurst exponent; H=1 corresponds to regular surfaces with Gaussian correlations, while the surfaces
                are self-affine for H<1.

        Returns:

        """
        Nc = X.shape

        pNc = np.prod(Nc)
        spNc = np.sqrt(pNc)

        # Covariance matrix
        [Xn, Yn] = np.meshgrid(np.abs(np.linspace(extent[0], extent[1], Nc[0])),
                               np.abs(np.linspace(extent[2], extent[3], Nc[1])))
        Lag = np.sqrt(Xn ** 2 + Yn ** 2)
        CM = np.broadcast_to((sh ** 2) * np.exp(-(Lag / cl) ** (2 * H)), (Nc[1], Nc[0], Nc[1], Nc[0]))

        vNc0 = np.broadcast_to(np.arange(Nc[0]), (Nc[1], Nc[0], Nc[1], Nc[0]))
        vNc1 = np.broadcast_to(np.arange(Nc[1]), (Nc[1], Nc[0], Nc[0], Nc[1])).transpose(0, 1, 3, 2)

        sv = vNc0.transpose(2, 3, 0, 1)
        tv = vNc1.transpose(2, 3, 0, 1)

        Xh = np.sum(np.sum(X.T * np.exp(2j * np.pi * (vNc0 * sv + vNc1 * tv) / spNc), axis=2), axis=2)
        S = np.sum(np.sum(CM * np.exp(2j * np.pi * (vNc0 * sv + vNc1 * tv) / spNc), axis=2), axis=2) / pNc
        # Correlated field
        SqS = np.sqrt(S)
        SqS[np.isnan(SqS)] = 0
        CRF = np.sum(np.sum(SqS * Xh * np.exp(2j * np.pi * (vNc0 * sv + vNc1 * tv) / spNc), axis=2), axis=2) / pNc

        return np.real(CRF) * spNc


    def plot_geometry(self, p=None, backend='pyvista', **kwargs):
        """
        Plot the mesh
        Args:
            p: Axes of Matplotlib or plotter from pyvista
            back: 'pyvista' or 'mpl' (matplotlib)

        Returns:
        """
        show = kwargs.pop('show', True)
        if backend == 'pyvista':
            from fracwave.plot.geometry_plot import plot_mesh_pyvista
            return plot_mesh_pyvista(surf=self.get_surface(), plotter=p, show=show, **kwargs)
        elif backend == 'mpl':
            from fracwave.plot.geometry_plot import plot_mesh_mpl
            return plot_mesh_mpl(self.vertices, self.faces, ax=p, show=show, **kwargs)

    def plot_vectors(self, plotter=None, **kwargs):
        """

        Args:
            p:
            **kwargs:

        Returns:

        """
        from fracwave.plot.geometry_plot import plot_vectors
        return plot_vectors(plotter=plotter,
                            midpoint=self.fractures[['x', 'y', 'z']].to_numpy(),
                            nx_dir=np.vstack(self.fractures['nx'].to_numpy()),
                            ny_dir=np.vstack(self.fractures['ny'].to_numpy()),
                            nz_dir=np.vstack(self.fractures['nz'].to_numpy()),
                            **kwargs)

    def extract_vertices_and_faces_from_pyvista_mesh(self, geom):
        """
        Use a pyvista geometry and extract the faces and vertices to construct the mesh.
        Take into account that a pyvista mesh can have irregular the elements. This will cause noise in the data.
        A workaround is to smooth the mesh: 'geom.triangulate().subdivide(nsub=1).smooth(n_iter=1000)'
        Args:
            geom: Pyvista geometry
        Returns:
            Vertices and faces
        """
        logger.debug('Extracting vertices and faces...')
        vertices = geom.points
        faces = []

        if isinstance(geom, (pv.StructuredGrid, pv.UnstructuredGrid)):
            try:
                fa = geom.cells
            except:
                try:
                    fa = geom.faces
                except:
                    # e = []
                    # l = geom.GetNumberOfCells()
                    faces = np.asarray([geom.cell_point_ids(i) for i in range(geom.GetNumberOfCells())])
                    # faces = np.asarray([geom.get_cell(i).points_ids for i in range(geom.GetNumberOfCells())])
                    # for i in range(l):
                    #     cell = geom.GetCell(i)
                    #     n_ids = cell.GetPointIds().GetNumberOfIds()
                    #     # pv._vtk.vtk_to_numpy(cell.GetPointId())
                    #     for f in range(n_ids):
                    #         # e.append(cell.GetPointIds().GetId(f))
                    #         e.append(cell.GetPointId(f))
                    #     faces.append(e)
                    #     e = []
                    # faces = np.asarray(faces)
                    logger.debug('Extract done')
                    return vertices, faces
        else:
            fa = geom.faces
        c = None
        i = 1
        e = []
        for f in fa:
            if c is None:
                c = f
                continue
            if i <= c:
                e.append(f)
                i += 1
                continue
            faces.append(e)
            i = 1
            e = []
            c = f
        faces.append(e)
        faces = np.asarray(faces)

        # self.vertices = vertices
        # self.faces = faces
        # self._surface = geom.copy()
        logger.debug('Extract done')
        return vertices, faces

    def export_to_hdf5(self, filename=None, overwrite=False):
        """
        Hierarchical Data Format
        Returns:

        """
        if filename is None:
            logger.info('Saving into cache...')
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'

        directory = os.path.dirname(filename)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        del directory

        with h5py.File(filename, "a") as f:  # Read/write if exists, create otherwise
            if overwrite and 'mesh' in f:
                logger.info('Overwriting existing "mesh" information')
                del f['mesh']
            # Store Mesh properties
            mesh = f.create_group('mesh')
            mesh.attrs['date'] = self._meta.date
            mesh.attrs['name'] = self._meta.project_name
            mesh.attrs['nelements'] = self.nelements
            mesh.attrs['surface'] = self.surface_area

            geom = mesh.create_group('geometry')
            geom.create_dataset(name='vertices', shape=self.vertices.shape, data=self.vertices)
            geom.create_dataset(name='faces', shape=self.faces.shape, data=self.faces)
            geom.create_dataset(name='dip', shape=self.fractures['dip'].shape, data=self.fractures['dip'].to_numpy())
            geom.create_dataset(name='azimuth', shape=self.fractures['azimuth'].shape,
                                data=self.fractures['azimuth'].to_numpy())
            geom.create_dataset(name='area', shape=self.fractures['area'].shape, data=self.fractures['area'].to_numpy())
            geom.create_dataset(name='midpoint', shape=self.fractures[['x', 'y', 'z']].shape,
                                data=self.fractures[['x', 'y', 'z']].to_numpy())
            geom.create_dataset(name='nx', data=np.vstack(self.fractures['nx'].to_numpy()))
            geom.create_dataset(name='ny', data=np.vstack(self.fractures['ny'].to_numpy()))
            geom.create_dataset(name='nz', data=np.vstack(self.fractures['nz'].to_numpy()))

            prop = mesh.create_group('properties')
            prop.create_dataset(name='aperture', shape=self.fractures['aperture'].shape,
                                data=self.fractures['aperture'].to_numpy())
            prop.create_dataset(name='elec_permeability', shape=self.fractures['elec_permeability'].shape,
                                data=self.fractures['elec_permeability'].to_numpy().astype(float))
            prop.create_dataset(name='elec_conductivity', shape=self.fractures['elec_conductivity'].shape,
                                data=self.fractures['elec_conductivity'].to_numpy().astype(float))

            fr = geom.create_group('fractures')

            for i, name in enumerate(self.name_fractures):
                fr.create_dataset(name=name,
                                  data=list(self.fractures.loc[self.fractures['name'] == name].index))
                fr[name].attrs['order'] = i
                fr[name].attrs['plane'] = self.fractures.loc[self.fractures['name'] == name, 'plane'].unique()[0]

        logger.info('File successfully saved in: {}'.format(filename))

    def load_hdf5(self, filename=None):
        """

        Args:
            filename:

        Returns:

        """
        if filename is None:
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'
            logger.info('Loading from cache: {}'.format(filename))

        if self.nelements > 0:
            self._fractures = self._fractures.drop(self._fractures.index)

        with h5py.File(filename, "r+") as f:
            geom = f['mesh/geometry']
            self.vertices = geom['vertices'][:]
            self.faces = geom['faces'][:]

            dip = geom['dip'][:]
            azimuth = geom['azimuth'][:]
            area = geom['area'][:]
            m = geom['midpoint'][:]
            x = m[:, 0]
            y = m[:, 1]
            z = m[:, 2]
            # nx = geom['nx'][:]
            # ny = geom['ny'][:]
            nz = geom['nz'][:]

            prop = f['mesh/properties']
            aperture = prop['aperture'][:]
            elec_permeability = prop['elec_permeability'][:]
            elec_conductivity = prop['elec_conductivity'][:]

            frac = geom['fractures']

            _name_fractures = np.asarray(list(frac.keys()))
            order = [frac[f'{n}'].attrs['order'] for n in _name_fractures]
            n_fractures = _name_fractures[np.argsort(order)].tolist()

            pos = 0
            for n in n_fractures:
                index = frac[n][:]
                fac = self.faces[index]
                df = pd.DataFrame.from_dict({'vertices': [val for val in self.vertices[fac]],
                                             'faces': [val for val in fac - pos],
                                             'aperture': aperture[index],
                                             'elec_permeability': elec_permeability[index],
                                             'elec_conductivity': elec_conductivity[index],
                                             'x': x[index],
                                             'y': y[index],
                                             'z': z[index],
                                             'azimuth': azimuth[index],
                                             'dip': dip[index],
                                             'area': area[index],
                                             # 'nx': [X for X in nx[index]],
                                             # 'ny': [Y for Y in ny[index]],
                                             'nz': [Z for Z in nz[index]],
                                             'name': n,
                                             }
                                            )
                try:
                    df['plane'] = frac[n].attrs['plane']
                except:
                    logger.warning('You are using an old version of the fractureGeo class. '
                                   'Please save the geometry again to update it')
                    df['plane'] = self.get_projection_plane(df[['x', 'y', 'z']].to_numpy())
                df.reset_index(inplace=True)
                self._fractures = pd.concat((self._fractures, df), ignore_index=True)
                logger.debug('Fracture set')
                pos = fac.max() + 1
        logger.info('Loaded successfully')

    def create_df_control_points(self, pointsA, pointsB=None):
        """

        Args:
            pointsA:
            pointsB:

        Returns:

        """
        df = pd.DataFrame(pointsA, columns=['X', 'Y', 'Z'])
        df['surface'] = 'A'
        if pointsB is not None:
            df2 = pd.DataFrame(pointsB, columns=['X', 'Y', 'Z'])
            df2['surface'] = 'B'
            df = pd.concat([df, df2], ignore_index=True)
        return df

    def create_df_orientations(self, pointsA, dipsA, azimuthA,
                               pointsB=None, dipsB=None, azimuthB=None):
        """

        Args:
            pointsA:
            dipsA:
            azimuthA:
            pointsB:
            dipsB:
            azimuthB:

        Returns:

        """

        if np.ndim(pointsA)==1:
            pointsA=np.asarray(pointsA)[None]
        normal = get_normal_vector_from_azimuth_and_dip(azimuth=azimuthA, dip=dipsA)
        if normal.ndim == 1:
            normal = normal[None]
        df = pd.DataFrame(np.hstack((pointsA, normal)), columns=['X', 'Y', 'Z', 'G_x', 'G_y', 'G_z'])
        df['surface'] = 'A'
        if pointsB is not None:
            if np.ndim(pointsB)==1:
                pointsB = np.asarray(pointsB)[None]
            normal2 = get_normal_vector_from_azimuth_and_dip(azimuth=azimuthB, dip=dipsB)
            if normal2.ndim == 1:
                normal2 = normal2[None]
            df2 = pd.DataFrame(np.hstack((pointsB, normal2)), columns=['X', 'Y', 'Z', 'G_x', 'G_y', 'G_z'])
            df2['surface'] = 'B'
            df = pd.concat([df, df2], ignore_index=True)
        return df

    def create_gempy_geometry(self,
                              fixed_points: DataFrame[S["X: Float, Y:Float, Z:Float, smooth: Float"]],
                              fixed_orientations: DataFrame[S["X: Float, Y:Float, Z:Float, "
                                                              "G_x: Float, G_y:Float, G_z:Float, smooth: Float"]],
                              move_points: DataFrame[S["X: Float, Y:Float, Z:Float, smooth: Float"]] = None,
                              ) -> Tuple:
        """
        Make the update of the geometry
        Args:
            geo_model: Same geo_model, so it is initialize once
            fixed_points: Points that will constrain the geometry and will be kept fixed like the points from
            logging or tunnel measurements
            fixed_orientations: Tuple with position (xyz) and the orientations (Azimuth and dip) of the points
            from logging and tunnel measurements.
            move_points: These are the proposed points that will shape the geometry and help constrain it
        Returns:
            Vertices and faces of the geometry
        """
        # Include all fixed points and orientations
        if self.geo_model is None:
            raise ValueError('No gempy model initialized')
        logger.debug(
            f'Setting {len(fixed_points)} fixed points, {len(fixed_orientations)} orientations from borehole '
            f'logging and tunnel observations')
        if _GEMPY_OLD:
            [self.geo_model.delete_orientations(i) for i in list(self.geo_model.orientations.df.index)]
            [self.geo_model.delete_surface_points(i) for i in list(self.geo_model.surface_points.df.index)]

            for i, df in fixed_points.iterrows():
                self.geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Z'], surface=df['surface'] if 'surface' in df else 'A')
            if 'smooth' in fixed_points:
                self.geo_model.surface_points.df.smooth = fixed_points.smooth

            for i, df in fixed_orientations.iterrows():
                self.geo_model.add_orientations(X=df['X'], Y=df['Y'], Z=df['Z'],
                                           surface=df['surface'] if 'surface' in df else 'A',
                                           pole_vector=df[['G_x', 'G_y', 'G_z']])

            if 'smooth' in fixed_orientations:
                self.geo_model.orientations.df.smooth = fixed_orientations.smooth
            # id_orientation_fixed.append(geo_model.surface_points.df.index[-1])

            if move_points is not None:
                logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
                for i, df in move_points.iterrows():
                    self.geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Z'], surface=df['surface'] if 'surface' in df else 'A')
                if 'smooth' in move_points:
                    self.geo_model.surface_points.df.smooth = move_points.smooth
                    # id_points_move.append(geo_model.surface_points.df.index[-1])

            gp.compute_model(self.geo_model)
            logger.debug('Model computed')
            vertices = self.geo_model.solutions.vertices
            faces = self.geo_model.solutions.edges
            if len(vertices) == 1 and len(faces) == 1:
                return vertices[0], faces[0]
            else:
                vert = {}
                fac = {}
                for i in range(len(vertices)):
                    surface = self.geo_model.surfaces.df['surface'][i]
                    vert[surface] = vertices[i]
                    fac[surface] = faces[i]
                return vert, fac
        else:
            raise NotImplementedError
            # [self.geo_model.delete_orientations(i) for i in list(self.geo_model.orientations.df.index)]
            [self.geo_model.delete_surface_points(i) for i in list(self.geo_model.surface_points.df.index)]
            [self.geo_model.orientations for i in list(self.geo_model.orientations.df.index)]


            for i, df in fixed_points.iterrows():
                self.geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Z'],
                                                  surface=df['surface'] if 'surface' in df else 'A')
            if 'smooth' in fixed_points:
                self.geo_model.surface_points.df.smooth = fixed_points.smooth

            for i, df in fixed_orientations.iterrows():
                self.geo_model.add_orientations(X=df['X'], Y=df['Y'], Z=df['Z'],
                                                surface=df['surface'] if 'surface' in df else 'A',
                                                pole_vector=df[['G_x', 'G_y', 'G_z']])

            if 'smooth' in fixed_orientations:
                self.geo_model.orientations.df.smooth = fixed_orientations.smooth
            # id_orientation_fixed.append(geo_model.surface_points.df.index[-1])

            if move_points is not None:
                logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
                for i, df in move_points.iterrows():
                    self.geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Z'],
                                                      surface=df['surface'] if 'surface' in df else 'A')
                if 'smooth' in move_points:
                    self.geo_model.surface_points.df.smooth = move_points.smooth
                    # id_points_move.append(geo_model.surface_points.df.index[-1])

            gp.compute_model(self.geo_model)
            logger.debug('Model computed')
            vertices = self.geo_model.solutions.vertices
            faces = self.geo_model.solutions.edges
            if len(vertices) == 1 and len(faces) == 1:
                return vertices[0], faces[0]
            else:
                vert = {}
                fac = {}
                for i in range(len(vertices)):
                    surface = self.geo_model.surfaces.df['surface'][i]
                    vert[surface] = vertices[i]
                    fac[surface] = faces[i]
                return vert, fac

    def get_coupling_between_fractures(self, midpoints: NDArray[Shape["* elements, 3"], Any],
                                   normals: NDArray[Shape["* elements, 3"], Any]) -> NDArray[Shape["* elements"], Any]:
        """
        Args:
            midpoints: xyz coordinates of the elements to compare
            normals: Normal vector at midpoints
        Returns:

        Proof:
            pos2 = 20
            pos1 = 40
            m1 = midpoints[pos1]
            m2 = midpoints[pos2]
            n1 = normal[pos1]
            n2 = normal[pos2]

            md = m1-m2
            md /= np.linalg.norm(md)

            d1 = np.abs(np.dot(n1, md))
            d2 = np.abs(np.dot(n2, md))

            result = d1*d2

            assert final[pos1, pos2] == result
        """
        e, i = midpoints.shape

        a = np.broadcast_to(midpoints, (e, e, i))
        b = np.transpose(a, axes=(1, 0, 2))
        # Get the separation vector
        midpoints_v = b - a
        norm = np.linalg.norm(midpoints_v, axis=-1)
        midpoints_v = midpoints_v / norm[..., None]

        # Get the dot product between the separation vector and all normals
        final_p = np.abs(np.einsum('abi, ai -> ab', midpoints_v, normals))
        # compare the product of both dot products
        final = final_p * final_p.T
        # final[np.isnan(final)] = 0
        final[np.isnan(final)] = 0

        return final.max(axis=0)

    def get_position_reflection_point(self, midpoints, tx, rx):
        """

        Args:
            midpoints:
            tx:
            rx:

        Returns:

        """
        # This means that we have 2 surfaces, so we need to know which surface have the minimum distance
        from fracwave.geometry.vector_calculus import get_position_reflection_point
        return get_position_reflection_point(midpoints, tx, rx)


    def get_midpoints(self, vertices=None, faces=None, name_fractures=None):
        """

        Args:
            vertices:
            faces:

        Returns:

        """
        if name_fractures is not None:
            if isinstance(name_fractures, str): name_fractures = [name_fractures]
            mask = np.ones(len(self.fractures), dtype=bool)
            for n in name_fractures:
                if n not in self.fractures['name'].to_list():
                    raise AttributeError(f'{n} is not a fracture name. Options are {self.name_fractures}')
                mask &= self.fractures['name'] == n
            return self.fractures.loc[mask, ['x', 'y', 'z']].to_numpy()

        if vertices is None and faces is None:
            vertices = self.vertices
            faces = self.faces
        if isinstance(vertices, dict) and isinstance(faces, dict):
            midpoints = {}
            for key in vertices.keys():
                elements = vertices[key][faces[key]]
                midpoints[key] = elements.mean(axis=1)
            return midpoints
        else:
            elements = vertices[faces]
            return elements.mean(axis=1)

    def get_fracture(self, name_fracture: None or str='all'):
        if len(name_fracture) == 0 or name_fracture is None or name_fracture == 'all':
            return self.fractures
        else:
            mask = np.zeros(len(self.fractures), dtype=bool)
            if isinstance(name_fracture, str):
                name_fracture = [name_fracture]
            for n in name_fracture:
                if n not in self.name_fractures:
                    raise AttributeError(f'{n} not a fracture name. Options are: {self.name_fractures}')
                mask |= self.fractures['name'] == n
            return self.fractures[mask]

    def get_projection_plane(self, points: NDArray) -> int:
        """
        Looking at a point cloud, we calculate the plane that cover the biggest area.
        The bigger area is assumed to be the plane
        Args:
            points:
        Returns:

        """
        from fracwave.geometry.vector_calculus import get_projection_plane_from_pointcloud
        return get_projection_plane_from_pointcloud(points)
