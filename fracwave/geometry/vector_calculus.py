"""
Created on Thu Nov  5 15:49:17 2020

@author: Alexis Shakas, Daniel Escallon
"""
import numpy as np
from fracwave.utils.logger import set_logger
from nptyping import NDArray, Shape
from typing import Optional, Any, Union, Tuple
from scipy import linalg as sli
from tqdm.autonotebook import tqdm
from scipy.spatial import KDTree, ConvexHull
import pandas as pd

logger = set_logger(__name__)
try:
    import dask.array as da
    DASK_INSTALLED = True
except ImportError:
    logger.warning('Dask not installed')
    DASK_INSTALLED = False


def rotation_along_Z(angles: Union[float, list, NDArray]):
    """
    Rotation matrix along z axis
    Args:
        angles: Angle in degrees to rotate
    Returns:
        R: rotation matrix

    """
    if isinstance(angles, list):
        angles = np.asarray(angles)
    angle = np.deg2rad(angles)
    zeros = np.zeros(angles.shape[0]) if isinstance(angles, np.ndarray) else 0
    ones = np.ones(angles.shape[0]) if isinstance(angles, np.ndarray) else 1
    R = np.array([[np.cos(angle), np.sin(angle), zeros],
                  [-np.sin(angle), np.cos(angle), zeros],
                  [zeros, zeros, ones]])
    if len(R.shape) > 2:
        return R.transpose(2, 0, 1)
    else:
        return R


def arbitrary_rotation(angle, u):
    """
    3D rotation matrix for a rotation around a unit vector (ux, uy, uz) passing through the origin
    Args:
        angle: rotation angle
        u: unit vector

    Returns:
        R: 3D rotation matrix

    """
    theta = np.deg2rad(angle)
    R = np.array([[np.cos(theta) + u[0] ** 2 * (1 - np.cos(theta)),
                   u[0] * u[1] * (1 - np.cos(theta)) - u[2] * np.sin(theta),
                   u[0] * u[2] * (1 - np.cos(theta)) + u[1] * np.sin(theta)],
                  [u[0] * u[1] * (1 - np.cos(theta)) + u[2] * np.sin(theta),
                   np.cos(theta) + u[1] ** 2 * (1 - np.cos(theta)),
                   u[1] * u[2] * (1 - np.cos(theta)) - u[0] * np.sin(theta)],
                  [u[0] * u[2] * (1 - np.cos(theta)) - u[1] * np.sin(theta),
                   u[1] * u[2] * (1 - np.cos(theta)) + u[0] * np.sin(theta),
                   np.cos(theta) + u[2] ** 2 * (1 - np.cos(theta))]])
    return R

def rotation_matrix(theta, rot_axis):
    """
    3D rotation matrix for a rotation around a unit vector (ux, uy, uz) passing through the origin
    Args:
        theta: in radians
        rot_axis: Rotation axis

    Returns:
        R: 3D rotation matrix
    """
    return np.array([[np.cos(theta) + rot_axis[0] ** 2 * (1 - np.cos(theta)),
                       rot_axis[0] * rot_axis[1] * (1 - np.cos(theta)) - rot_axis[2] * np.sin(theta),
                       rot_axis[0] * rot_axis[2] * (1 - np.cos(theta)) + rot_axis[1] * np.sin(theta)],
                      [rot_axis[1] * rot_axis[0] * (1 - np.cos(theta)) + rot_axis[2] * np.sin(theta),
                       np.cos(theta) + rot_axis[1] ** 2 * (1 - np.cos(theta)),
                       rot_axis[1] * rot_axis[2] * (1 - np.cos(theta)) - rot_axis[0] * np.sin(theta)],
                      [rot_axis[2] * rot_axis[0] * (1 - np.cos(theta)) - rot_axis[1] * np.sin(theta),
                       rot_axis[2] * rot_axis[1] * (1 - np.cos(theta)) + rot_axis[0] * np.sin(theta),
                       np.cos(theta) + rot_axis[2] ** 2 * (1 - np.cos(theta))]])

def get_orientation_of_plane(azimuth: Union[NDArray, list, float], dips: Union[NDArray, list, float]) -> Union[NDArray, float]:
    """
    Get the normal vector to the plane given by the strikes and dips
    Args:
        azimuth: angles
        dips: angles
    Returns:
        Normals vector:
        t1_vector: vector [1,0,0] onto plane
        t2_vector: vector [0,1,0] onto plane

    """
    azimuth_rotation = arbitrary_rotation(azimuth, [0, 0, 1])
    dip_rotation = arbitrary_rotation(dips, [1, 0, 0])
    t1_vector = np.array([1, 0, 0])
    t2_vector = np.array([0, 1, 0])
    normal_vector = np.array([0, 0, 1])

    if azimuth_rotation.ndim == 3:  # This means that the azimuth and dips are vectors
        rotation_matrix = np.einsum('nkj, kmj ->jnm', azimuth_rotation, dip_rotation)
        normal_vector = np.einsum('kij, j -> ki', rotation_matrix, normal_vector)
        t1_vector = np.einsum('kij, j -> ki', rotation_matrix, t1_vector)
        t2_vector = np.einsum('kij, j -> ki', rotation_matrix, t2_vector)

    else:
        rotation_matrix = np.matmul(azimuth_rotation, dip_rotation)
        normal_vector = np.dot(rotation_matrix, normal_vector)
        t1_vector = np.dot(rotation_matrix, t1_vector)
        t2_vector = np.dot(rotation_matrix, t2_vector)
    return normal_vector, t1_vector, t2_vector



def align_plane_with_dip_and_azimuth(Nx, Ny, dip, azimuth):
    """
    Create a grid and rotate the points according to the strike and dip.
    Args:
        Nx: [Min x value, Max x value, Spacing in x]
        Ny: [Min y value, Max y value, Spacing in y]
        dip: angle or list of angles
        azimuth: angle or list of angles

    Returns:
        [x, y, z] points of the rotated plane

    """
    [yy, xx] = np.meshgrid(np.linspace(Nx[0], Nx[1], int(Nx[2])),
                           np.linspace(Ny[0], Ny[1], int(Ny[2])))
    zz = np.zeros(xx.shape)
    xyz = np.array([xx.ravel(), yy.ravel(), zz.ravel()])

    rot_dip = arbitrary_rotation(np.array([dip]), [0, 1, 0]).squeeze()
    # Change the sign of the azimuth degree to rotate the matrix clockwise following the geological convention
    # Subtract 90 degrees so the North is the 0 degree angle
    new_azimuth = (azimuth-90)*-1
    rot_azimuth = arbitrary_rotation(np.array([new_azimuth]), [0, 0, 1]).squeeze()

    return np.matmul(rot_azimuth, np.matmul(rot_dip, xyz)).T

def get_circle_along_normal(center: np.array = None,
                            normal: np.array = None,
                            radius: Union[int, float, list, NDArray] = 1,
                            points_per_m: Union[int, float, list, NDArray] = 1,
                            scatter_points: bool = True):
    """
    Get a circle of points of given radius given a center and a normal
    Args:
        center: a numpy n by 3 array containing points that define the center of the circle
        normal: a numpy n by 3 array containing the normal to the plane that contains the circle
        radius: The radii of each circle
        points_per_m: the number of points to compute per meter along the circle
        scatter_points: Scatters uniformly the points around the circle so that they do not always start at the same theta

    Returns:
        normal: The normal to the fitted plane
        XYZ: a point that lies within the plane
    """
    cone = np.empty((0, 3))
    if isinstance(radius, int) or isinstance(radius, float):
        radius = [radius] * center.shape[0]
    points = np.round(radius / points_per_m)
    for row in range(radius.shape[0]):
        if scatter_points:
            theta = np.linspace(0, 2 * np.pi, int(points[row])) + np.random.uniform() * np.pi
        else:
            theta = np.linspace(0, 2 * np.pi, int(points[row]))
        v = sli.null_space([normal[row, :]])
        circle = center[row, :] + radius[row] * (
                np.outer(v[:, 0], np.cos(theta)) + np.outer(v[:, 1], np.sin(theta))).T
        cone = np.concatenate((cone, circle), axis=0)
    return cone

def rotate_points_according_to_dip_and_azimuth(points, dip, azimuth, center_rotation=None):
    if center_rotation is None:
        center_rotation = points.mean(axis=0)

    if center_rotation.ndim == 1:
        center_rotation = center_rotation[None]
    else:
        assert (len(points) == len(center_rotation)), 'Number of points need to coincide with number of center_rotations'
    points = points - center_rotation

    # Change the sign of the azimuth degree to rotate the matrix clockwise following the geological convention
    # Subtract 90 degrees so the North is the 0 degre angle
    new_azimuth = azimuth - 90
    new_dip = 90 - dip
    rot_azimuth = arbitrary_rotation(new_azimuth, [0, 0, 1]).T
    rot_dip = arbitrary_rotation(new_dip, [0, 1, 0]).T

    # points = np.matmul(rot_azimuth, np.matmul(rot_dip, points.T)).T
    if rot_azimuth.ndim == 3:
        # new_points = np.einsum('aij, aij, pl->apl', rot_azimuth, rot_dip, points).reshape(-1,3)
        # TODO: Not working
        new_points = np.einsum('ijk, jkl -> il', rot_azimuth,
                               np.einsum('ijk, jl -> jkl', rot_dip, points)).reshape(-1,3)

    else:
        new_points = np.matmul(rot_azimuth, np.matmul(rot_dip, points.T)).T

    return new_points + center_rotation

def get_normal_vector_from_azimuth_and_dip(azimuth: Union[float, list, NDArray],
                                           dip: [float, list, NDArray]) -> \
        NDArray[Shape["*, 3"], Any]:
    """
    From the azimuth (dip direction) and dip angle, get the normal vector to the plane following geologic convention.
    Args:
        azimuth: angle between 0 and 360 degrees with respect to the North
        dip: angle between 0 and 90 degrees with respect to the horizontal plane

    Returns:
        normal_vector
    """
    if isinstance(azimuth, list):
        azimuth = np.asarray(azimuth)
    elif isinstance(azimuth, (int,float)):
        azimuth = np.asarray([azimuth])
    if isinstance(dip, list):
        dip = np.asarray(dip)
    elif isinstance(dip, (int,float)):
        dip = np.asarray([dip])

    if azimuth.shape != dip.shape:
        print('Input arrays must have the same shape')
        return np.empty()
    # To account for the geological convention of the north being the 0 degree and dip going down
    azimuth = np.deg2rad(90 - azimuth)
    dip = np.deg2rad(-dip)
    azimuth_vector = np.array([np.cos(azimuth) * np.cos(dip),
                               np.sin(azimuth) * np.cos(dip),
                               np.sin(dip)]).T
    # orthogonal vector to the dip_direction
    strike_vector = np.array([-np.sin(azimuth),
                              np.cos(azimuth),
                              np.zeros(azimuth.shape[0])]).T

    normals = np.cross(azimuth_vector, strike_vector, axis=1)
    return normals / np.linalg.norm(normals, axis=1)[:, None]


def get_azimuth_and_dip_from_normal_vector(normal: NDArray[Shape["* points, 3"], Any],
                                           apply_shift: Optional[bool]=True) -> \
        Tuple[NDArray[Shape["* azimuth"], Any], NDArray[Shape["* dips"], Any]]:
    """
    Compute the azimuth and dip based on the vector normal to the plane.
     We follow the geological convention:
        Dip of 0 deg corresponds to the horizontal plane.
        Positive is downwards and negative is upwards
        Azimuth 0 points north and increase clockwise
    Args:
        normal: vector normal to the plane
        all_positive: If angles include negative values, shift everything to show dips between 0 and 90 and
            azimuth between 0 and 360
    Returns:
        azimuth, dip
    """
    # The unit normal up vector to the earth's surface is [0 0 1].
    # The angle between the plane and the normal is the same as the dipping angle
    if isinstance(normal, list): normal = np.asarray(normal)
    vertical = [0, 0, 1]
    if normal.ndim == 2:
        normal /= np.linalg.norm(normal, axis=1)[:, None]
        azimuth = np.degrees(np.arctan2(normal[:, 0], normal[:, 1]))  # To get the azimuth
        # Divide the dot product by the magnitude of each vector. Use the inverse of cosine on this result.
        dip = np.degrees(np.arccos(np.dot(normal, vertical)))
    else:
        normal = normal / np.linalg.norm(normal)
        azimuth = np.degrees(np.arctan2(normal[0], normal[1]))  # To get the azimuth
        dip = np.degrees(np.arccos(np.dot(normal, vertical)))
    if apply_shift:
        azimuth, dip = apply_shifts_to_azimuth_and_dip(azimuth, dip, all_positive=True)
    return azimuth, dip

def get_nx_ny_from_nz(nz):
    """
    Given the normal vector nz, find the other 2 orthogonal vectors nx and ny
    Args:
        nz:

    Returns:

    """
    nx = get_any_orthonormal_vector(nz)
    ny = np.cross(nz, nx)
    # nx = rot.dot([1, 0, 0])
    # ny = rot.dot([0, 1, 0])
    if nz.ndim == 1:
        nx /= np.linalg.norm(nx)
        ny /= np.linalg.norm(ny)
    else:
        nx /= np.linalg.norm(nx, axis=1, keepdims=True)
        ny /= np.linalg.norm(ny, axis=1, keepdims=True)
    return nx, ny, nz
    # vf = (0,0,1)  # Being the normal component pointing upwards
    # Axis of rotation

    # axis = np.cross(nz, vf)
    # axis = np.cross(v0, vf)
    # Angle of rotation
    # theta = np.arccos(np.dot(nz / np.linalg.norm(nz), vf / np.linalg.norm(vf)))
    # theta = np.arccos(np.dot(v0 / np.linalg.norm(v0), vf / np.linalg.norm(vf)))
    # Rotation matrix
    #
    # if nz.ndim == 1:
    #     rot = rotation_matrix(theta, axis)
    #     # v_roted = rot.dot(nz)
    #     v_roted = rot.dot(nz)
    #     v_roted /= np.linalg.norm(v_roted)
    #     assert np.allclose(v_roted, vf), 'Vectors are not orthogonal, there was a problem with the rotation matrix'
    #     nx = get_any_orthonormal_vector(nz)
    #     ny = np.cross(nz, nx)
    #     # nx = rot.dot([1, 0, 0])
    #     # ny = rot.dot([0, 1, 0])
    #     nx /= np.linalg.norm(nx)
    #     ny /= np.linalg.norm(ny)
    # else:
    #     rot = rotation_matrix(theta, axis.T)
    #     v_roted = np.einsum('jie, ei -> ej', rot, nz)
    #     v_roted /= np.linalg.norm(v_roted, axis=1,keepdims=True)
    #     assert np.allclose(np.abs(v_roted) - vf, 0), 'Vectors are not orthogonal, there was a problem with the rotation matrix.'
    #     nx = np.einsum('jie, i -> ej', rot, [1,0,0])
    #     ny = np.einsum('jie, i -> ej', rot, [0,1,0])
    #     nx /= np.linalg.norm(nx, axis=1, keepdims=True)
    #     ny /= np.linalg.norm(ny, axis=1, keepdims=True)
    # return nx, ny, nz

def apply_shifts_to_azimuth_and_dip(azimuth, dip, all_positive=True):
    """
    To follow the geological convention, any set of azimuth and dip angles need to be always between the angle range:
    Azimuth is the angle with respect to north increasing clockwise
    Dip is the angle with respect to the horizontal. Pointing upwards is negative and pointing negative is positive
    Args:
        azimuth: any azimuth angle
        dip: any dip angle
        all_positive: If all positive this means that the dip value can only be from 0 and 90
            This is often desired when we are interested on the normal of the plane to be always positive
    Returns:
        (azimuth between 0 and 360, dip between -90 and 90)
    """
    if isinstance(dip, NDArray):
        assert len(dip) == len(azimuth), 'Length of dip and azimuth need to be the same'
        azimuth %= 360
        if any(dip < -90):  # Rotation counter clockwise
            shift = (dip[dip < -90] // -90)

            extra_rev = np.zeros(len(shift)) + 180
            extra_rev[(270 < azimuth[dip < -90]) & (azimuth[dip < -90] < 360)] = 0
            extra_rev[(90 < azimuth[dip < -90]) & (azimuth[dip < -90] < 180)] = 0
            #
            # if 270 < azimuth[dip < -90] < 360 or 90 < azimuth[dip < -90] < 180:  # This means that 1 movement will change the orientation by 180
            #     extra_rev = 0
            # else:
            #     extra_rev = 180
            azimuth[dip < -90] += ((shift // 2) * 180) + extra_rev  # Account for the first turn
            dip[dip < -90] = -(dip[dip < -90] + (shift * 90) + 90)
            shift - dip
        if any(dip > 90):
            shift = (dip[dip > 90] // 90)
            extra_rev = np.zeros(len(shift))
            extra_rev[(270 < azimuth[dip > 90]) & (azimuth[dip > 90] < 360)] = 180
            extra_rev[(90 < azimuth[dip > 90]) & (azimuth[dip > 90] < 180)] = 180
            # if 270 < azimuth[dip > 90] < 360 or 90 < azimuth[dip > 90] < 180:  # This means that 1 movement will change the orientation by 180
            #     extra_rev = 180
            # else:
            #     extra_rev = 0
            azimuth[dip > 90] += ((shift // 2) * 180) + extra_rev
            dip[dip > 90] = dip[dip > 90] - (shift * 90)
        azimuth %= 360
        if all_positive and any(dip < 0):
            azimuth[dip < 0] = (azimuth[dip < 0] + 180) % 360
            dip[dip < 0] = np.abs(dip[dip < 0])
    else:
        azimuth %= 360
        if dip < -90:
            shift = (dip // -90)  # 360 in case the angle do a whole revolution
            dip = -((shift * 90) + dip + 90)
            if 270 < azimuth < 360 or 90 < azimuth < 180:
                extra_rev = 0
            else:
                extra_rev = 180
            azimuth += ((shift // 2) * 180) + extra_rev

        if dip > 90:
            # shift = (dip // 180) * 180  # Take all values that are higher than 180
            # if shift == 0:
            shift = (dip // 90)
            dip = dip - (shift * 90)
            if 270 < azimuth < 360 or 90 < azimuth < 180:
                extra_rev = 180
            else:
                extra_rev = 0
            azimuth += ((shift // 2) * 180) + extra_rev
        azimuth %= 360
        if all_positive and dip < 0:
            dip = np.abs(dip)
            azimuth = (azimuth + 180) % 360
    return azimuth, dip

def get_any_orthonormal_vector(v):
    """
    Get any orthonormal vector to the input vector
    Args:
        v: vector

    Returns:
        Any orthonormal vector to the input vector
    """
    if isinstance(v, list): v = np.asarray(v)
    if v.ndim == 1:
        v = v / np.linalg.norm(v)
        if v[0] == 0 and v[1] == 0 and v[2] != 0:
            u = np.array([1, 0, 0])
        elif v[2] == 0:
            u = np.array([0, 0, 1])
        else:
            u = np.array([-v[1], v[0], 0])
        assert np.dot(u, v) == 0, 'Vectors are not orthogonal'
        return u
    else:
        v = v / np.linalg.norm(v, axis=1)[:, None]
        u = np.zeros(v.shape)

        mask2 = (v[:, 2] == 0)
        mask1 = (v[:, 0] == 0) & (v[:, 1] == 0) & ~mask2
        mask3 = ~(mask1 | mask2)
        u[mask1, 0] = 1
        u[mask2, 2] = 1
        u[mask3, 0] = -v[mask3, 1]
        u[mask3, 1] = v[mask3, 0]

        assert(np.all(np.einsum('ti, ti->t', u,v) == 0)), 'Not all vectors are not orthogonal'

        return u

# def create_fresnel_cone_of_points(distance, frequency, wavelength, zones=3):

def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5


def create_fresnel_cone_of_points(center, orientation, distance, wavelength, n_zone=4, **kwargs):
    """
    Calculates the cone of the n fresnel zone for a given distance, frequency and wavelength.
    Args:
        center: center of the fresnel zone (Transmitter antenna position)
        orientation: Antenna orientation
        distance: distance between the transmitter and receiver
        wavelength: wavelength of the signal
        n_zone: number of the fresnel zone
        kwargs:
            n_a: number of points for each distance = 100
            s: spacing between revolution of cones = 1
    Returns:
        radius of the first fresnel zone
    """


    if not isinstance(distance, (list, NDArray)):
        distance = np.asarray([distance])
    if center.ndim > 1:
        assert(len(distance) == len(center)), 'distance and center must have the same length'
    else:
        center = center[None]

    n_a = kwargs.pop('n_a', 100)
    angles = np.linspace(0, 2 * np.pi, n_a, endpoint=False)
    s = kwargs.pop('s', 1)
    angles_azimuth = np.arange(0, 360, s)
    u = get_any_orthonormal_vector(orientation)
    cones = []
    distance_all = []
    for i in tqdm(range(len(distance))):
        d = np.arange(0, distance[i]+wavelength*0.25, wavelength*0.25)
        # Create a cone for each source location
        radius = fresnel(d, wavelength, n_zone)
        x = np.einsum('d, a -> da', radius, np.cos(angles)).flatten()
        y = np.einsum('d, a -> da', radius, np.sin(angles)).flatten()

        xyz = np.vstack((np.repeat(d, n_a, axis=0).flatten(), x, y)).T
        points = xyz + center[i]


        rot_angle = arbitrary_rotation(angles_azimuth, orientation[i])
        direction = np.einsum('ija, i -> aj', rot_angle, u[i])
        azimuth, dip = get_azimuth_and_dip_from_normal_vector(direction, apply_shift=False)

        new_points = np.vstack([rotate_points_according_to_dip_and_azimuth(points,
                                                                dip=dip[a],
                                                                azimuth=azimuth[a],
                                                                center_rotation=center[i])
                                for a in range(len(dip))]
                               )
        cones.append(new_points)
        distance_all.append(np.linalg.norm(new_points - center[i], axis=-1))

    return cones, distance_all

def find_points_near_surface(surface_points, other_points, minimum_distance=1e-4):
    """
    Find the points on the surface that intersect with the other points
    Args:
        surface_points:
        other_points:

    Returns:

    """
    def find_intersection(p1, p2, distance):
        tree1 = KDTree(p1)
        dists1, _ = tree1.query(p2, workers=-1)
        mask = (dists1 <= distance)
        return p2[mask], mask

    if isinstance(other_points, list) or other_points.ndim == 3:
        mask_all = []
        points = []
        for p in tqdm(other_points):
            result = find_intersection(p, surface_points, minimum_distance)
            points.append(result[0])
            mask_all.append(result[1])
        return points, mask_all
    else:
        return find_intersection(other_points, surface_points, minimum_distance)


def get_projection_plane_from_pointcloud(points: NDArray) -> int:
    """
    Looking at a point cloud, we calculate the plane that cover the biggest area.
    The bigger area is assumed to be the plane
    Args:
        points:
    Returns:

    """

    def measure_area(points):
        from scipy.stats import gaussian_kde
        from scipy.integrate import dblquad
        kde = gaussian_kde(points.T)

        def kde_func(x, y):
            return kde.evaluate([x, y])

        x_min, y_min = np.min(points, axis=0)
        x_max, y_max = np.max(points, axis=0)
        area, _ = dblquad(kde_func, x_min, x_max, lambda x: y_min, lambda x: y_max)
        return area

    a = measure_area(points[:, (1, 2)])  # Plane 0
    b = measure_area(points[:, (0, 2)])  # Plane 1
    c = measure_area(points[:, (0, 1)])  # Plane 2
    if a > b and a > c:
        plane = 0
    elif b > a and b > c:
        plane = 1
    elif c > a and c > b:
        plane = 2
    return plane

def convex_hull_from_pointcloud(points, plane=None):
    """
    Get the convex hull from a point cloud
    Args:
        points: point cloud
        plane: plane to project the points to

    Returns:
        hull_points: points that form the convex hull
        hull_vertices: indices of the points that form the convex hull
    """
    if plane is None:
        plane = get_projection_plane_from_pointcloud(points)
    points_hull = points[plane]
    if plane == 0:  # yz
        scalar = points_hull[:, 0]
        xy = points_hull[:, (1, 2)]
    elif plane == 1:  # xz
        scalar = points_hull[:, 1]
        xy = points_hull[:, (0, 2)]
    elif plane == 2:  # xy
        scalar = points_hull[:, 2]
        xy = points_hull[:, (0, 1)]

    cv = ConvexHull(xy)
    hull_points = points_hull[cv.vertices]
    hull_points = np.vstack((hull_points, hull_points[0]))
    return hull_points

def get_position_reflection_point(midpoints, tx, rx):
    """

    Args:
        midpoints:
        tx:
        rx:

    Returns:

    """
    # This means that we have 2 surfaces, so we need to know which surface have the minimum distance
    s2 = False
    if isinstance(midpoints, dict):
        s2 = True
        mid = pd.DataFrame()
        for key, arr in midpoints.items():
            temp = pd.DataFrame(arr, columns=['X','Y','Z'])
            temp['surface'] = key
            mid = pd.concat((mid, temp), ignore_index=True)
        midpoints = mid[['X', 'Y', 'Z']].to_numpy()
    # Construct the KDTree with element_positions
    # tree = KDTree(midpoints)

    # Perform nearest neighbor search for tx and rx
    # distances_t, _ = tree.query(tx, k=len(midpoints), workers=-1)
    # distances_r, _ = tree.query(rx, k=len(midpoints), workers=-1)
    from scipy.spatial.distance import cdist
    t_e = cdist(tx, midpoints)
    r_e = cdist(rx, midpoints)
    # Compute the combined distances
    distance_t = t_e + r_e
    # distance_t = distances_t + distances_r

    # Get the minimum distances and positions
    pos_min_distance_t = np.argmin(distance_t, axis=1)
    min_distance_t = distance_t[np.arange(len(pos_min_distance_t)), pos_min_distance_t]
    min_position_t = midpoints[pos_min_distance_t]
    if s2:

        surface = mid.loc[pos_min_distance_t, 'surface'].to_list()
        return min_distance_t, min_position_t, surface
    else:
        return min_distance_t, min_position_t, ['A']*len(min_distance_t)