import os.path
from builtins import ValueError

import h5py
import numpy as np
import pandas as pd

from fracwave.utils.help_decorators import MetaData
from fracwave.utils.logger import set_logger

logger = set_logger(__name__)


class Antenna(object):
    """
    Class to manage the GPR antenna geometry
    """
    def __init__(self, name='Receiver-Transmitter'):
        self._Rx = None
        self._Tx = None
        self._orient_Rx = None
        self._orient_Tx = None
        self._meta = MetaData(name)
        # This wil contain the ids that defines the borehole
        self._profiles = pd.DataFrame(columns=['Rx','Ry','Rz',
                                               'Tx','Ty','Tz',
                                               'orient_Rx','orient_Ry','orient_Rz',
                                               'orient_Tx','orient_Ty','orient_Tz', 'profile', 'index'])  # This will be the one in charge of setting the different measurements to be done
        # self._profiles.set_index(['profile'], inplace=True)
        # in all together

    def __repr__(self):
        return f"""
        {self._meta.project_name} {self._meta.date}
            nprofiles: {len(self.name_profiles)}
            ntraces: {self.ntraces}
            
        """
    @property
    def name_profiles(self):
        return list(self.profiles['profile'].unique().tolist())

    def set_profile(self, name_profile, receivers, transmitters, orient_receivers, orient_transmitters,
                    overwrite =False, depth_Rx=None, depth_Tx=None):
        """
        A profile is a continuous collection of data that is displayed in the Radargram.
        Various profiles can be diplayed in the same class.
        Args:
            name_profile: Name of the profile to add
            Rx: Receiver position (positions X 3)
            Tx: Transmitter position (positions X 3)
            orient_Rx: xyz receiver orientation from the coordinates
            orient_Tx: xyz transmitter orientation from the coordinates
            overwrite: If "name_profile" already exists, this will overwrite the information
        Returns:

        """
        assert len(receivers) == len(transmitters) == len(orient_receivers) == len(orient_transmitters),\
            'All transmitter and receiver positions need to have the same size. '
        Rx = self._check_Rx(receivers)
        Tx = self._check_Tx(transmitters)
        orient_Rx = self._check_orient_Rx(orient_receivers)
        orient_Tx = self._check_orient_Tx(orient_transmitters)

        if name_profile in self._profiles['profile'].unique():
            if overwrite:
                self._profiles = self._profiles[self._profiles['profile'] != name_profile]
            else:
                raise AttributeError('Profile name already exists. Set "overwrite=True" to replace this value')
        df = pd.DataFrame.from_dict({'Rx': Rx[:, 0],
                                     'Ry': Rx[:, 1],
                                     'Rz': Rx[:, 2],
                                     'Tx': Tx[:, 0],
                                     'Ty': Tx[:, 1],
                                     'Tz': Tx[:, 2],
                                     'orient_Rx': orient_Rx[:, 0],
                                     'orient_Ry': orient_Rx[:, 1],
                                     'orient_Rz': orient_Rx[:, 2],
                                     'orient_Tx': orient_Tx[:, 0],
                                     'orient_Ty': orient_Tx[:, 1],
                                     'orient_Tz': orient_Tx[:, 2],
                                     'profile': name_profile})
        if depth_Tx is not None and depth_Rx is not None:
            df['depth_Rx'] = depth_Rx
            df['depth_Tx'] = depth_Tx
        df.reset_index(inplace=True)
        self._profiles = pd.concat((self._profiles, df), ignore_index=True)

    @property
    def profiles(self):
        return self._profiles.copy()
    @property
    def df(self):
        return self._profiles.copy()
    @property
    def Receiver(self): return self.profiles[['Rx', 'Ry', 'Rz']].to_numpy().astype(np.float32)
    @property
    def Transmitter(self): return self.profiles[['Tx', 'Ty', 'Tz']].to_numpy().astype(np.float32)

    def get_midpoints(self, name_profile:list or str='all'):
        """
        Get the midpoints between the transmitter and receiver
        Args:
            name_profile: list of names of the profiles to get the midpoints
        Returns:
        """
        if len(name_profile) == 0 or name_profile is None or name_profile == 'all':
            return (self.Receiver + self.Transmitter) / 2
        else:
            if isinstance(name_profile, str): name_profile = [name_profile]
            midpoints = []
            for n_p in name_profile:
                mask = self.profiles['profile'] == n_p
                midpoints.append((self.Receiver[mask] + self.Transmitter[mask]) / 2)
            return np.vstack(midpoints)

    def get_profile(self, name_profile: list or str = 'all'):
        if len(name_profile) == 0 or name_profile is None or name_profile == 'all':
            return self.profiles
        else:
            mask = np.zeros(len(self.profiles), dtype=bool)
            if isinstance(name_profile, str):
                name_profile = [name_profile]
            for n in name_profile:
                if n not in self.name_profiles:
                    raise AttributeError(f'{n} not a profile name. Options are: {self.name_profiles}')
                mask |= self.profiles['profile'] == n
            return self.profiles[mask]

    def _check_Tx(self, Tx):
        """
        Set the xyz transmitter coordinates
        Args:
            Tx:
        Returns:
        """
        if isinstance(Tx, list): Tx = np.asarray(Tx)
        if Tx.ndim != 2:
            e = 'Tx needs to have 2 dimensions: (traces x 3). dim is {}'.format(Tx.ndim)
            logger.error(e)
            raise ValueError(e)
        if Tx.shape[1] == 3:
            logger.debug('Tx set')
            return Tx
        else:
            if Tx.T.shape[1] != 3:
                e = 'Cannot set Tx. {} does not have a shape of (traces x 3).'.format(Tx.shape)
                logger.error(e)
                raise ValueError(e)
            logger.debug('Tx.T set')
            return Tx.T

    def _check_Rx(self, Rx):
        """
        Set the xyz transmitter coordinates
        Args:
            Rx:
        Returns:
        """
        if isinstance(Rx, list): Rx = np.asarray(Rx)
        if Rx.ndim != 2:
            e = 'Rx needs to have 2 dimensions: (traces x 3). dim is {}'.format(Rx.ndim)
            logger.error(e)
            raise ValueError(e)
        if Rx.shape[1] == 3:
            logger.debug('Rx set')
            return Rx
        else:
            if Rx.T.shape[1] != 3:
                e = 'Cannot set Rx. {} does not have a shape of (traces X 3).'.format(Rx.shape)
                logger.error(e)
                raise ValueError(e)
            logger.debug('Rx.T set')
            return Rx.T

    @property
    def orient_Receiver(self): return self.profiles[['orient_Rx','orient_Ry','orient_Rz']].to_numpy().astype(np.float32)
    @property
    def orient_Transmitter(self): return self.profiles[['orient_Tx','orient_Ty','orient_Tz']].to_numpy().astype(np.float32)

    @property
    def depth_Transmitter(self): return self.profiles['depth_Tx'].to_numpy().astype(np.float32) if 'depth_Tx' in self.profiles else np.nan
    @property
    def depth_Receiver(self): return self.profiles['depth_Rx'].to_numpy().astype(np.float32) if 'depth_Rx' in self.profiles else np.nan


    def _check_orient_Tx(self, orient_Tx):
        """
        Set the xyz transmitter orientation from the coordinates
        Args:
            orient_Tx:
        Returns:
        """
        if isinstance(orient_Tx, list): orient_Tx = np.asarray(orient_Tx)
        if orient_Tx.shape[1] == 3:
            pass
        else:
            if orient_Tx.T.shape[1] != 3:
                e = 'Cannot set orient_Tx. {} does not have a shape of (traces x 3).'.format(orient_Tx.shape)
                logger.error(e)
                raise ValueError(e)
            return orient_Tx.T
        logger.debug('orient_Tx set')
        return orient_Tx


    def _check_orient_Rx(self, orient_Rx):
        """
        Set the xyz transmitter orientation from the coordinates
        Args:
            orient_Rx:
        Returns:
        """
        if isinstance(orient_Rx, list): orient_Rx = np.asarray(orient_Rx)
        if orient_Rx.shape[1] == 3:
            pass
        else:
            if orient_Rx.T.shape[1] != 3:
                e = 'Cannot set orient_Rx. {} does not have a shape of (traces x 3).'.format(orient_Rx.shape)
                logger.error(e)
                raise ValueError(e)
            return orient_Rx.T

        logger.debug('orient_Rx set')
        return orient_Rx

    @property
    def ntraces(self):
        return len(self.profiles)

    def export_to_hdf5(self, filename=None, overwrite=False):
        """

        Args:
            filename:

        Returns:

        """
        if filename is None:
            logger.info('Saving into cache...')
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'

        if len(self.Receiver) != len(self.Transmitter):
            logger.error('Number of sources is different than the number of receivers. {} \b'
                         'Not possible to save.'.format((self.Receiver.shape, self.Transmitter.shape)))
            return

        directory = os.path.dirname(filename)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        del directory

        with h5py.File(filename, "a") as f:  # Read/write if exists, create otherwise
            if overwrite and 'antenna' in f:
                logger.info('Overwriting existing "antenna" information')
                del f['antenna']
            ant = f.create_group('antenna')
            ant.attrs['date'] = self._meta.date
            ant.attrs['name'] = self._meta.project_name
            ant.attrs['ntraces'] = self.ntraces
            ant.attrs['nprofiles'] = len(self.name_profiles)

            ant.create_dataset(name='Rx', data=self.Receiver)
            ant.create_dataset(name='Tx', data=self.Transmitter)
            ant.create_dataset(name='orient_Rx', data=self.orient_Receiver)
            ant.create_dataset(name='orient_Tx', data=self.orient_Transmitter)
            ant.create_dataset(name='depth_Rx', data=self.depth_Receiver) if 'depth_Rx' in self.profiles else None
            ant.create_dataset(name='depth_Tx', data=self.depth_Transmitter) if 'depth_Tx' in self.profiles else None
            prf = ant.create_group('profiles')

            for i, name in enumerate(self.name_profiles):
                prf.create_dataset(name=name,
                                data=list(self.profiles[self.profiles['profile'] == name].index))
                prf[name].attrs['order'] = i

        logger.info('File successfully saved in: {}'.format(filename))

    def load_hdf5(self, filename=None):
        if filename is None:
            from fracwave import TEMP_DATA
            filename = TEMP_DATA + 'default_FractureGeometry.h5'
            logger.info('Loading from cache: {}'.format(filename))

        if self.ntraces > 0:
            self._profiles = self._profiles.drop(self._profiles.index)
        with h5py.File(filename, "r+") as f:
            ant = f['antenna']
            R = ant['Rx'][:]
            T = ant['Tx'][:]
            orient_T = ant['orient_Tx'][:]
            orient_R = ant['orient_Rx'][:]

            if 'depth_Tx' in ant:
                depth_tx = ant['depth_Tx'][:]
                depth_rx = ant['depth_Rx'][:]

            profiles = ant['profiles']
            _name_profiles = np.asarray(list(profiles.keys()))
            orderp = [profiles[f'{n}'].attrs['order'] for n in _name_profiles]
            nprof = _name_profiles[np.argsort(orderp)].tolist()

            for p in nprof:
                mask = profiles[p][:]
                self.set_profile(name_profile=p,
                                 receivers=R[mask],
                                 transmitters=T[mask],
                                 orient_receivers=orient_R[mask],
                                 orient_transmitters=orient_T[mask],
                                 depth_Tx=depth_tx[mask] if 'depth_Tx' in ant else None,
                                 depth_Rx=depth_rx[mask] if 'depth_Rx' in ant else None
                                 )

        logger.info('Loaded successfully')

    def get_geometry(self, **kwargs):
        """
        Function that returns the pyvista object of the geometry
        Returns:

        """
        import pyvista as pv
        show = kwargs.pop('show', True)
        Tx = pv.PolyData(self.Transmitter)
        Rx = pv.PolyData(self.Receiver)
        Tx_arrows = pv.PolyData()
        for i in range(len(self.orient_Transmitter)):
            Tx_arrows += pv.Arrow(start=self.Transmitter[i], direction=self.orient_Transmitter[i], **kwargs)
        Rx_arrows = pv.PolyData()
        for i in range(len(self.orient_Receiver)):
            Rx_arrows += pv.Arrow(start=self.Receiver[i], direction=self.orient_Receiver[i], **kwargs)

        if show:
            import pyvistaqt as pvqt
            plotter = pvqt.BackgroundPlotter()
            plotter.show_bounds()
            plotter.show_axes()
            plotter.add_mesh(Tx, color='red')
            plotter.add_mesh(Rx, color='blue')
            plotter.add_mesh(Tx_arrows, color='blue')
            plotter.add_mesh(Rx_arrows, color='blue')
            plotter.show()
        return Tx, Rx, Tx_arrows, Rx_arrows

    def plot(self, p=None, backend='pyvista', arrows=False, **kwargs):
        """
        Plot the antenna locations mesh
        Args:
            p: Axes of Matplotlib or plotter from pyvista
            backend: 'pyvista' or 'mpl' (matplotlib)
            arrows: Plot the orientation vector as arrows. (Carefull, when too many arrows it may crash)
        Returns:
        """
        show = kwargs.pop('show', True)

        if backend == 'pyvista':
            from fracwave.plot.geometry_plot import plot_source_receiver_pyvista
            return plot_source_receiver_pyvista(Tx=self.Transmitter, Rx=self.Receiver,
                                                orient_Tx=self.orient_Transmitter, orient_Rx=self.orient_Receiver,
                                                plotter=p, show=show, arrows=arrows, **kwargs)
        elif backend == 'mpl':
            from fracwave.plot.geometry_plot import plot_source_receiver_mpl
            return plot_source_receiver_mpl(Tx=self.Transmitter, Rx=self.Receiver, ax=p, show=show, **kwargs)