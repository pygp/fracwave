#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 09:40:23 2022

@author: alexis
"""
from fracwave.utils.help_decorators import get_backend
# md = get_backend()
solver = 'numpy'
if solver == 'pytorch':
    import torch as md
elif solver == 'numpy':
    import numpy as md

pi = 3.141592653589793
c_s = 299_792_458
c_ns = 0.299_792_458
m0 = 4*pi*10**-7 # Henries per meter
e0 = md.divide(1,m0*c_s**2) # Fahrads per meter

def filter_errors(array):
    array[md.isnan(array)] = 0
    array[md.isinf(array)] = 0
    return array

def source(a, b, g, ph, t0, f):
    source_spectrum = (f**(a*g-1))*md.exp(-(f/b)**g)*md.exp(1j*ph)*md.exp(-2j*pi*t0*f)
    return source_spectrum/md.max(md.abs(source_spectrum))

def complex_epsilon(omega, epsilon, sigma):
    return e0*epsilon + md.divide(1j*sigma, omega)

def wavenumber(omega, epsilon, sigma, units='SI'):
        # Greiner pg 316
    if units == 'cgs':
        sigma = sigma/9
        c = c_s
        numerator = md.sqrt(1 + (4*pi*sigma/(omega*epsilon))**2)
        k = md.sqrt(epsilon)*(omega/c)*(md.sqrt(md.divide(numerator+1,2)) + 1j*md.sqrt(md.divide(numerator-1,2)))
    if units == 'SI':
        c = c_s
        k = omega*md.sqrt(m0*complex_epsilon(omega, epsilon, sigma))
    k[md.isnan(k)] = 0
    return k
def reflection(k1, k2, aperture):
    rte = md.divide(k1 - k2, k1 + k2)
    expn = md.exp(-2j*aperture*k2)
    pol = md.divide(rte*(1-expn), 1-(rte**2)*expn)
    return filter_errors(pol)

def _reflection(omega, e1, e2, k1, k2, aperture):
    rte = md.divide(k1 - k2, k1 + k2)
    rtm = md.divide(k1, k2) * rte
    expn = md.exp(-2j*aperture*k2)
    tbrTE = rte * md.divide(1 - expn, 1-(rte**2)*expn)
    tbrTM = rtm * md.divide(1 - expn, 1-(rtm**2)*expn)
    # pol = md.divide(rte*(1-expn), 1-(rte**2)*expn)
    # return filter_errors(pol)
    return filter_errors(tbrTE), filter_errors(tbrTM)


def reflectionBD(omega, e1, e2, k1, k2, aperture):
    tan_term = md.tan(k2*aperture)
    Rte = md.divide(-1j*(k1**2/k2 -k2)*tan_term, 2*k1 - 1j*(k1**2/k2 +k2)*tan_term)
    Rtm = md.divide(-1j*(k1**2*e2/k2 -k2*e1**2/e2)*tan_term, 2*k1*e1 - 1j*(k1**2*e2/k2 + k2*e1**2/e2)*tan_term)
    return filter_errors(Rte), filter_errors(Rtm)


# function to compute the polarization of a dipole element (using effective reflection coefficients)
def transmission(k1, k2, aperture):
    r21 = md.divide(k1 - k2, k1 + k2)
    t12 = md.divide(2*k2, k1 + k2)
    t21 = md.divide(2*k1, k1 + k2)
    expn = md.exp(-2j*aperture*k2)
    pol = md.divide(t12*t21*expn, 1 - r21*r21*expn)
    return filter_errors(pol)

def rotation_along_Z(angles):
    angle = md.deg2rad(angles)
    R = md.array([[md.cos(angle),   md.sin(angle),  0],
                [-md.sin(angle),    md.cos(angle),  0],
                [0,                 0,       1]])
    return R

def arbitrary_rotation(angle, u):
    theta = md.deg2rad(angle)
    R = md.array([[md.cos(theta) + u[0]**2 * (1-md.cos(theta)), 
                  u[0] * u[1] * (1-md.cos(theta)) - u[2] * md.sin(theta), 
                  u[0] * u[2] * (1 - md.cos(theta)) + u[1] * md.sin(theta)],
                [u[0] * u[1] * (1-md.cos(theta)) + u[2] * md.sin(theta),
                  md.cos(theta) + u[1]**2 * (1-md.cos(theta)),
                  u[1] * u[2] * (1 - md.cos(theta)) - u[0] * md.sin(theta)],
                [u[0] * u[2] * (1-md.cos(theta)) - u[1] * md.sin(theta),
                  u[1] * u[2] * (1-md.cos(theta)) + u[0] * md.sin(theta),
                  md.cos(theta) + u[2]**2 * (1-md.cos(theta))]])
    return R


def orthogonal_projection(input_tensor, projection_tensor):
    divisor = md.tile(md.power(md.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).permute(1,2,0)
    dot_product = md.tile(md.sum(projection_tensor*input_tensor, axis=2),(3,1,1)).permute(1,2,0)
    return dot_product*projection_tensor/divisor

def normalize_components(input_tensor):
    input_tensor[md.isnan(input_tensor)] = 0
    return input_tensor/md.tile(md.linalg.norm(input_tensor,axis=2),(3,1,1)).permute(1,2,0)

def get_orthonormal_basis_of_plane(strike, dip):
    strike_rotation = rotation_along_Z(strike)
    dip_dir = strike + 90
    if dip_dir > 360:
        dip_dir = dip_dir - 360
    dip_dir_rotation = rotation_along_Z(dip_dir)
    vec1 = md.matmul(strike_rotation,[0,1,0])
    vec_dip_dir = md.matmul(dip_dir_rotation,[0,1,0])
    dip_rotation = arbitrary_rotation(dip, vec1)
    vec2 = md.matmul(dip_rotation,vec_dip_dir)
    normal_vector = md.cross(vec1, vec2)
    normal_vector = normal_vector/md.sqrt(md.sum((normal_vector)**2))
    vec1 = vec1/md.sqrt(md.sum((vec1)**2))
    vec2 = vec2/md.sqrt(md.sum((vec2)**2))
    return normal_vector, vec1, vec2


def get_vector_components_on_plane(tensor_in, strikes, dips):
    n_freq = tensor_in.shape[1]
    from fracwave.geometry.vector_calculus import get_orientation_of_plane
    #normal_vec, t1, t2 = get_normal_vector_of_plane(strikes, dips)
    t1, t2, normal_vec = get_orientation_of_plane(strikes, dips)
    normal_tensor = md.tile(normal_vec, (n_freq,1,1)).permute(1,0,2)
    t1_tensor = md.tile(t1, (n_freq,1,1)).permute(1,0,2)
    t2_tensor = md.tile(t2, (n_freq,1,1)).permute(1,0,2)
    normal_component = orthogonal_projection(tensor_in,normal_tensor)
    t1_component = orthogonal_projection(tensor_in,t1_tensor)
    t2_component = orthogonal_projection(tensor_in,t2_tensor)
    return normalize_components(normal_component), normalize_components(t1_component), normalize_components(t2_component)


class fracEM_SI:

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.propagation_terms = 'total-field'
        self.mode = 'transmission'
        self.apply_causality = False

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, omega, k_medium, source_dipole, s_loc, obs_loc):
        R_vec = obs_loc - s_loc
        n_frequencies = omega.shape[0]
        R = md.sqrt(md.sum(R_vec**2,axis=0))
        k = md.broadcast_to(k_medium,(3,3,n_frequencies))
        R_outer = md.outer(R_vec, R_vec)
        R_outer = md.broadcast_to(R_outer,(n_frequencies,3,3)).transpose(1,2,0)
        first_term = md.divide(md.exp(1j*k*R), 4*pi*R)
        second_term = (1 + md.divide(1j*k*R - 1, (k**2)*(R**2)))
        I = md.broadcast_to(md.identity(3),(n_frequencies,3,3)).transpose(1,2,0)
        third_term = md.divide(3 - 3j*k*R - (k**2)*(R**2),(k**2)*(R**2))*md.divide(R_outer,R**2)
        Green = first_term*(second_term*I + third_term)
        Green[md.isnan(Green)] = 0
        source_tensor = md.broadcast_to(source_dipole,(3,3,n_frequencies))
        Efield = omega**2*m0**2*md.sum(Green*source_tensor, axis=1)
        if(self.apply_causality == True):
            causality_index = k_medium*R/(omega) > n_frequencies/(2*md.max(omega.ravel()))
            if(md.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[:,causality_index] = 0
        Efield = filter_errors(Efield)
        return Efield

    def _propagation(self, omega, k_medium, src_vec, s_loc, obs_loc):
        import numpy as np
        print("""
        
        
        
        We are inside the function
        
        
        
        """)
        r_vec = obs_loc - s_loc
        nr = np.sqrt(np.sum(np.power(r_vec,2)))  # R
        rh = np.tile(r_vec/nr, (omega.shape[0],1)).transpose(1,0)
        k = np.tile(k_medium,(3,1))
        k2 = np.power(k,2)
        dot1 = np.tile(np.sum(src_vec*rh,axis=0),(3,1))
        dtprd = rh*dot1
        # Efield = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr)
        Efield = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*(np.exp(1j*k*nr)/4*np.pi*nr)

        #    Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
        Efield = filter_errors(Efield)
        if(self.apply_causality == True):
            causality_index = k*nr/(omega) > 2*np.pi*omega.shape[1]/(2*np.max(omega.ravel()))
            if(np.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[causality_index] = 0
        return Efield
    
    def only_propagation(self, omega, k_medium, source_matrix, s_loc, s_or, r_loc, r_or):
        return self.propagation(omega, k_medium, source_matrix, s_loc, r_loc).T

    def source_to_dipole(self, omega, rock_complex_epsilon, k_medium, incoming_field, dipole, modulation_type='c'):
        area = dipole[5]
        aperture = dipole[6]
        permittivity = dipole[7]
        conductivity = dipole[8]
        
        element_complex_epsilon = complex_epsilon(omega, permittivity, conductivity)
        k_element = wavenumber(omega, permittivity, conductivity, 'SI')

        normal_vec, tv1,tv2 = get_orthonormal_basis_of_plane(dipole[4], dipole[3])
        # Here we project the incoming field to the tangential and normal vectors of the element
        tangential_field_1 = md.outer(md.matmul(incoming_field.T, tv1), tv1).T
        tangential_field_2 = md.outer(md.matmul(incoming_field.T, tv2), tv2).T
        normal_field = md.outer(md.matmul(incoming_field.T, normal_vec), normal_vec).T
        
        if modulation_type == 'c':
            if any(self.mode == x for x in ['reflection', 'full-reflection', 'dipole_response']):
                modulationTE, modulationTM = reflectionBD(omega,
                # modulationTE, modulationTM = reflection(omega,
                                                          rock_complex_epsilon,
                                                          element_complex_epsilon, 
                                                          k_medium, 
                                                          k_element, 
                                                          aperture)
                modulation = tangential_field_1*modulationTE + \
                                tangential_field_2*modulationTE #+ \
                                    #normal_field*modulationTM
            elif(self.mode == 'transmission'):
                modulation = transmission(k_medium, k_element, aperture)
            
            else:
                modulation = md.ones((3,omega.shape[0]))
                
            induced_dipole = area*aperture*modulation/(1j*omega)
            
        # I'm leaving it here just for some later tests but we use the mode above for now
        if modulation_type == 'm':
            e2 = filter_errors(complex_epsilon(omega, permittivity, conductivity))
            scaling = rock_complex_epsilon/e2
#            volume = area*aperture*(e2-e0)/(1j*omega)
            volume = area*aperture*(e2-e0)
            induced_dipole = volume*(normal_field*scaling + tangential_field)
        return filter_errors(induced_dipole)
    
    
    
    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, mode='full-reflection'):
        md = get_backend()
        n_dipoles = dipoles.shape[0]
        n_freq = f.shape[0]
        omega = 2*pi*f*10**9
        if(mode in ['transmission', 'reflection', 'full-reflection', 'incoming_field', 'dipole_response', 'propagation']):
            self.mode = mode
            print('Running in ' + mode + ' mode for ' + str(n_dipoles) + ' dipoles and ' + str(n_freq) + ' frequencies.')
        else:
            print('Mode not specified correctly or not at all. Using default: ' + self.mode)
        
        # these only need to be computed once
        k_medium = wavenumber(omega, beps, bsig, 'SI')
        rock_complex_epsilon = complex_epsilon(omega, beps, bsig)
        
        source_matrix = md.outer(s_or,source_in)
        incoming_field = md.empty((3, n_freq, n_dipoles),dtype=complex)
        dipole_response = md.empty((3, n_freq, n_dipoles),dtype=complex)
        receiver = md.empty((3, n_freq, n_dipoles),dtype=complex)
        
        if mode == 'propagation':
            return self.only_propagation(omega, k_medium, source_matrix, s_loc, s_or, r_loc, r_or)

        for n in range(n_dipoles):
            incoming_field[:,:,n] = self.propagation(omega, k_medium, source_matrix, s_loc, dipoles[n, 0:3])
        if mode == 'incoming_field':
            return incoming_field
        
        
        for n in range(n_dipoles):
            dipole_response[:,:,n] = self.source_to_dipole(omega, rock_complex_epsilon, k_medium, incoming_field[:,:,n], dipoles[n,:],)

        if mode == 'dipole_response':
            return dipole_response
        
        for n in range(n_dipoles):
            receiver[:,:,n] = self.propagation(omega, k_medium, dipole_response[:,:,n], dipoles[n,0:3], r_loc)
        summed_response = md.sum(receiver,axis=2)
        
        if mode == 'full-reflection':
                    return summed_response.T
        if mode == 'reflection':
            return md.sum(summed_response.T*r_or, axis=1)



# class fracEM_cgs:
#
#     def __init__(self):
#         self.main_dir = '/forward-modeling/'
#         self.save_dir = '/output/'
#         self.apply_causality = False
#         self.mode = 'reflection'
#
#     # function to compute the propagation of the electric field from a point dipole
#     def propagation(self, omega, k_medium, src_vec, s_loc, obs_loc):
#         r_vec = obs_loc - s_loc
#         nr = md.sqrt(md.sum(md.power(r_vec,2)))
#         rh = md.tile(r_vec/nr, (omega.shape[0],1)).transpose(1,0)
#         k = md.tile(k_medium,(3,1))
#         k2 = md.power(k,2)
#         dot1 = md.tile(md.sum(src_vec*rh,axis=0),(3,1))
#         dtprd = rh*dot1
#         Efield = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*md.exp(-1j*k*nr)
#         #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*md.exp(-1j*k*nr);
#         Efield = filter_errors(Efield)
#         if(self.apply_causality == True):
#             causality_index = k*nr/(omega) > 2*pi*omega.shape[1]/(2*md.max(omega.ravel()))
#             if(md.sum(causality_index) > 0):
#                 print('Part of the signal is not causal and is omitted')
#             Efield[causality_index] = 0
#         return Efield
#
#
#     def only_propagation(self, omega, k_medium, source_matrix, s_loc, s_or, r_loc, r_or):
#         Efield = self.propagation(omega, k_medium, source_matrix, s_loc, r_loc)
#         Efield = md.sum(Efield*md.tile(r_or,(omega.shape[0],1)).transpose(1,0), axis=0)
#         return Efield
#
#     def source_to_dipole(self, omega, k1, incoming_field, dipole):
#         area = dipole[5]
#         aperture = dipole[6]
#         permittivity = dipole[7]
#         conductivity = dipole[8]
#
#         k2 = wavenumber(omega, permittivity, conductivity, units='cgs')
#
#         if(self.mode == 'reflection'):
#             modulation = reflection(k1, k2, aperture)
#         elif(self.mode == 'transmission'):
#             modulation = transmission(k1, k2, aperture)
#         else:
#             modulation = md.zeros((3,omega.shape[0]))
#
# #        nc, t1c, t2c = self.vc.get_vector_components_on_plane(incoming_field, dipole[3], dipole[4])
#         # modulated_field = modulation*incoming_field
#         #radiated_field = area*md.divide(1j*incoming_field*modulation, omega)
#         radiated_field = area*1j*incoming_field*modulation
#         return radiated_field
#
#
#     def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, mode='reflection'):
#
#         n_dipoles = dipoles.shape[0]
#         n_freq = f.shape[0]
#         omega = 2*pi*f*10**9
#         if(mode in ['transmission', 'reflection', 'incoming-field', 'dipole-response', 'propagation']):
#             self.mode = mode
#             print('Running in ' + mode + ' mode for ' + str(n_dipoles) + ' dipoles and ' + str(n_freq) + ' frequencies.')
#         else:
#             print('Mode not specified correctly or not at all. Using default: ' + self.mode)
#
#         k_medium = wavenumber(omega, beps, bsig, 'cgs')
#
#         source_matrix = md.outer(s_or,source_in)
#         incoming_field = md.empty((3, n_freq, n_dipoles),dtype=complex)
#         dipole_response = md.empty((3, n_freq, n_dipoles),dtype=complex)
#         receiver = md.empty((3, n_freq, n_dipoles),dtype=complex)
#         if mode == 'propagation':
#             return self.only_propagation(omega, k_medium, source_matrix, s_loc, s_or, r_loc, r_or)
#         for n in range(n_dipoles):
#             incoming_field[:,:,n] = self.propagation(omega, k_medium, source_matrix, s_loc, dipoles[n, 0:3])
#         if mode == 'incoming-field':
#             return incoming_field
#
#         for n in range(n_dipoles):
#             dipole_response[:,:,n] = self.source_to_dipole(omega, k_medium, incoming_field[:,:,n], dipoles[n,:])
#
#         if mode == 'dipole-response':
#             return dipole_response
#
#         for n in range(n_dipoles):
#             receiver[:,:,n] = self.propagation(omega, k_medium, dipole_response[:,:,n], dipoles[n,0:3], r_loc)
#         summed_response = md.sum(receiver,axis=2)
#         receiver_orientation_tensor = md.tile(r_or,(n_freq,1)).transpose(1,0)
#         axis_response = md.sum(receiver_orientation_tensor*summed_response,axis=0)
#
#         return axis_response