"""
Created on Thu Nov  5 15:49:17 2020

@author: Alexis Shakas, shakasa@lilo.org
"""
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import torch as pt

with np.warnings.catch_warnings():
    np.warnings.filterwarnings('ignore', r'ComplexWarning:')

from fracwave import set_logger


logger = set_logger(__name__)


class fracEM_pt_SI:
    """
    X Y Z strike dip AREA THICKNESS PERM COND
    # tensor format: omega, dipole, xyz,
    """

    def __init__(self):
        #self.main_dir = '/forward-modeling/'
        #self.save_dir = '/output/'
        self.c = 299792458  # m/ns
        self.propagation_terms = 'total-field'
        self.mode = 'transmission'
        self.apply_causality = True
        self.m0 = 4*np.pi*10**-7  # Henries per meter
        # self.m0 = 4*pt.pi*10**11 # Henries per meter (ns)
        self.e0 = np.divide(1.,self.m0*self.c**2) # Fahrads per meter
        
        
    def source(self, a: float, b: float, g: float, ph: float, t: float, f: np.ndarray) -> np.ndarray:
        """
        This function generates a dipole moment that is used for the antenna
        source. The dipole moment is a generalized gamma distribution that is
        generated using three shape parameters (a, b, g) and a complex phase (ph).
        Args:
            a: (0.5) positive value controling the shape of the dipole moment distribution
            b: (standard deviation) positive value controling the shape of the dipole moment distribution
            g: (2) positive value controling the shape of the dipole moment distribution
            ph: Orientation of the antenna
            t: TODO: Not in use.
            f: frequency array

        Returns:

        """
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)
        C = (10**12)/(4*np.pi*self.c**2)
        src_vec = source_spectrum*C
        src_vec = src_vec/np.max(np.abs(src_vec))
        return src_vec
    
    def complex_epsilon(self, omega, epsilon, sigma):
        """
        Complex electric permittivity: This can be expressed as a complex number.
        This accounts for the coefficient of attenuation that represents the part of the EM wave
        energy that is irreversibly converted into heat.
        Args:
            omega:
            epsilon:
            sigma:

        Returns:

        """
        return self.e0*epsilon + pt.divide(1j*sigma, omega)
    
    def wavenumber(self, omega, epsilon, sigma):
        """
        Complex wavenumber computation
        Args:
            omega:
            epsilon:
            sigma:

        Returns:

        """
        # k = pt.divide(omega, self.c)*pt.sqrt(epsilon + pt.divide(1j*sigma,(10**9)*omega*self.e0))
        k = omega*pt.sqrt(self.m0*self.complex_epsilon(omega, epsilon, sigma))
        k[pt.isnan(k)] = 0
        return k

    def matrix_vector_multiply(self, matrix, vector):
        vector2matrix = pt.broadcast_to(vector.permute(2,0,1), (3,3,vector.shape[0], vector.shape[1])).permute(2,3,1,0)

    def propagation(self, omega, eps, cond, source_dipole, s_loc, obs_loc):
        """
        Function to compute the propagation of the electric field from a point dipole
        Args:
            omega:
            eps:
            cond:
            source_dipole:
            s_loc:
            obs_loc:

        Returns:

        """
        R_vec = obs_loc - s_loc
        nr = pt.tile(pt.linalg.norm(R_vec, axis=2),(3,1,1)).permute(1,2,0)
        R = pt.broadcast_to(nr.permute(2,0,1),(3,3,omega.shape[0],omega.shape[1])).permute(2,3,0,1)
        RR = pt.broadcast_to(R_vec.permute(2,0,1),(3,3,omega.shape[0],omega.shape[1])).permute(2,3,0,1)
        k = self.wavenumber(omega,eps,cond)
        kt = pt.broadcast_to(k.permute(2,0,1),(3,3,omega.shape[0],omega.shape[1])).permute(2,3,0,1)
        R_outer = pt.multiply(RR,RR.permute(0,1,3,2))
        Green = pt.divide(pt.exp(1j*kt*R), 4*np.pi*R)*((1 + pt.divide(1j*kt*R - 1, (kt**2)*(R**2))) + pt.divide(3 - 3j*kt*R - (kt**2)*(R**2),(kt**2)*(R**2))*pt.divide(R_outer,R))
        Green[pt.isnan(Green)] = 0
        source_tensor = pt.broadcast_to(source_dipole.permute(2,0,1),(3,3,omega.shape[0],omega.shape[1])).permute(2,3,1,0) # LAST TWO DIMENSIONS FLIPPED 
        Efield = ((omega*self.m0)**2)*pt.sum(Green*source_tensor,axis=3)
        # Efield = ((omega)**2)*pt.sum(Green*source_tensor,axis=3)
        # if(self.apply_causality == True):
        #       COMPARE THE WINDOW TO THE TRAVEL TIME (t > WINDOW)
        #       Filter ot elements of my solution that are too far away
        #     causality_index = k*nr/(2*pt.pi*frq) > frq.shape[2]/(2*pt.max(frq.ravel()))
        #     Fr[causality_index] = 0
        Efield[pt.isnan(Efield)] = 0
        return Efield
    
    def source_to_dipoles(self, omega, beps, bsig, source_tensor, source_location_tensor, dipole_tensor):
        incoming_field = self.propagation(omega, beps, bsig, source_tensor, source_location_tensor, dipole_tensor[:,:,0:3])
        area = pt.tile(dipole_tensor[:,:,5],(3,1,1)).permute(1,2,0)
        aperture = pt.tile(dipole_tensor[:,:,6],(3,1,1)).permute(1,2,0)
        permittivity = pt.tile(dipole_tensor[:,:,7],(3,1,1)).permute(1,2,0)
        conductivity = pt.tile(dipole_tensor[:,:,8],(3,1,1)).permute(1,2,0)
        
        if(self.mode == 'reflection'):
            modulation = self.reflection(omega, beps, bsig, area, aperture, permittivity, conductivity)
        elif(self.mode == 'transmission'):
            modulation = self.transmission(omega, beps, bsig, area, aperture, permittivity, conductivity)
        else:
            print('Choose between -transmission- and -reflection- polarization settings.')
            return
        #def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        nc, t1c, t2c = self.get_vector_components_on_plane(incoming_field, dipole_tensor[:,0,3], dipole_tensor[:,0,4])
        modulated_field = self.efield_to_dipole(modulation*incoming_field, permittivity, conductivity,omega)
        dipole_field = modulated_field*nc + modulated_field*t1c + modulated_field*t2c
        #return modulated_field
        return dipole_field

    def efield_to_dipole(self, Efield, epsilon, sigma, omega):
        Efield = pt.divide(1j*Efield,omega)
        Efield[pt.isnan(Efield)] = 0
        return Efield

    def reflection(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        """
        function to compute the polarization of a dipole element (using effective reflection coefficients)
        Args:
            omega:
            beps:
            bsig:
            area:
            aperture:
            permittivity:
            conductivity:

        Returns:

        """
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        rte = pt.divide(k1 - k2, k1 + k2)
        expn = pt.exp(-2j*aperture*k2)
        pol = area*pt.divide(rte*(1-expn), 1-(rte**2)*expn)
        # pol = pt.divide(area*1j*tbrte, omega)
        pol[pt.isnan(pol)] = 0
        pol[pt.isinf(pol)] = 0
        return pol

    def transmission(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        """
        function to compute the polarization of a dipole element (using effective reflection coefficients)
        Args:
            omega:
            beps:
            bsig:
            area:
            aperture:
            permittivity:
            conductivity:

        Returns:

        """
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        r21 = pt.divide(k1 - k2, k1 + k2)
        t12 = pt.divide(2*k2, k1 + k2)
        t21 = pt.divide(2*k1, k1 + k2)
        pol = area*pt.divide(t12*t21*pt.exp(-2j*aperture*k2), 1 - r21*r21*pt.exp(-2j*k2*aperture))
        # pol = pt.divide(area*1j*tbrte, omega)
        pol[pt.isnan(pol)] = 0
        pol[pt.isinf(pol)] = 0
        return pol

    def only_propagation(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or):
        n_freq = f.shape[0]
        n_dipoles = 1
        source_tensor = pt.tile(pt.outer(s_or,source_in), (n_dipoles,1,1)).permute(0,2,1)
        omega_tensor = 2*np.pi*(10**9)*pt.tile(f,(n_dipoles,3,1)).permute(0,2,1)
        source_location_tensor = pt.tile(s_loc,(n_freq,n_dipoles,1)).permute(1,0,2)
        receiver_location_tensor = pt.tile(r_loc,(n_freq,n_dipoles,1)).permute(1,0,2)
        direct_wave = self.propagation(omega_tensor, beps, bsig, source_tensor, source_location_tensor, receiver_location_tensor)
        summed_response = pt.sum(direct_wave,axis=0)
        receiver_orientation_tensor = pt.tile(r_or,(n_freq,1))
        direct_response = pt.sum(receiver_orientation_tensor*summed_response,axis=1)
        return direct_response


    def forward_pass(self,
                     f,
                     medium_epsilon,
                     medium_sigma,
                     source_in,
                     source_location,
                     source_orientation,
                     receiver_location,
                     receiver_orientation,
                     dipoles,
                     coupling=0,
                     mode='transmission'):
# done
        if(mode in ['transmission', 'reflection']):
            self.mode = mode
        logger.info('Computing in ' + str(self.mode) + ' mode.')
        
        
        n_dipoles = dipoles.shape[0]
        n_freq = f.shape[0]
        f = pt.tensor(f, dtype=pt.float32)
        source_in = pt.tensor(source_in, dtype=pt.float32)
        medium_epsilon = pt.tensor(medium_epsilon, dtype=pt.float32)
        medium_sigma = pt.tensor(medium_sigma, dtype=pt.float32)
        source_location = pt.tensor(source_location, dtype=pt.float32)
        source_orientation = pt.tensor(source_orientation, dtype=pt.float32)
        receiver_location = pt.tensor(receiver_location, dtype=pt.float32)
        receiver_orientation = pt.tensor(receiver_orientation, dtype=pt.float32)
        dipoles = pt.tensor(dipoles, dtype=pt.float32)
        source_tensor = pt.tile(pt.outer(source_orientation,source_in), (n_dipoles,1,1)).permute(0,2,1)
        omega_tensor = 2*np.pi*(10**9)*pt.tile(f,(n_dipoles,3,1)).permute(0,2,1)
        source_location_tensor = pt.tile(source_location,(n_freq,n_dipoles,1)).permute(1,0,2)
        receiver_location_tensor = pt.tile(receiver_location,(n_freq,n_dipoles,1)).permute(1,0,2)
        dipole_tensor = pt.tile(dipoles,(n_freq,1,1)).permute(1,0,2)
        dipole_response = self.source_to_dipoles(omega_tensor, medium_epsilon, medium_sigma, source_tensor, source_location_tensor, dipole_tensor)
        receiver_response = self.propagation(omega_tensor, medium_epsilon, medium_sigma, dipole_response, dipole_tensor[:,:,0:3], receiver_location_tensor)
        direct_response = []
        summed_response = pt.sum(receiver_response,axis=0)
        receiver_orientation_tensor = pt.tile(receiver_orientation,(n_freq,1))
        fracture_response = pt.sum(receiver_orientation_tensor*summed_response,axis=1)
        if(self.mode == 'transmission'):
            direct_response = self.only_propagation(f, medium_epsilon, medium_sigma, source_in, source_location, source_orientation, receiver_location, receiver_orientation)
            return fracture_response, direct_response
        else:
            return fracture_response


    def rotation_along_Z(self, angles):
        angle = pt.deg2rad(angles)
        zeros = pt.zeros(angles.shape[0])
        ones = pt.ones(angles.shape[0])
        #R = pt.array([[pt.cos(angle),   pt.sin(angle),  zeros],
        #            [-pt.sin(angle),    pt.cos(angle),  zeros],
        #            [zeros,                 zeros,       ones]])
        R = pt.stack([pt.stack([pt.cos(angle), pt.sin(angle), zeros]),
                      pt.stack([-pt.sin(angle), pt.cos(angle), zeros]),
                      pt.stack([zeros, zeros, ones])])

        return R.permute(2,0,1)

    def arbitrary_rotation(self, angle, u):
        theta = pt.deg2rad(angle)
        #R = pt.array([[pt.cos(theta) + u[:,0]**2 * (1-pt.cos(theta)),
        #              u[:,0] * u[:,1] * (1-pt.cos(theta)) - u[:,2] * pt.sin(theta),
        #              u[:,0] * u[:,2] * (1 - pt.cos(theta)) + u[:,1] * pt.sin(theta)],
        #            [u[:,0] * u[:,1] * (1-pt.cos(theta)) + u[:,2] * pt.sin(theta),
        #              pt.cos(theta) + u[:,1]**2 * (1-pt.cos(theta)),
        #              u[:,1] * u[:,2] * (1 - pt.cos(theta)) - u[:,0] * pt.sin(theta)],
        #            [u[:,0] * u[:,2] * (1-pt.cos(theta)) - u[:,1] * pt.sin(theta),
        #              u[:,1] * u[:,2] * (1-pt.cos(theta)) + u[:,0] * pt.sin(theta),
        #              pt.cos(theta) + u[:,2]**2 * (1-pt.cos(theta))]])
        R = pt.stack([pt.stack([pt.cos(theta) + u[:, 0] ** 2 * (1 - pt.cos(theta)),
                                u[:, 0] * u[:, 1] * (1 - pt.cos(theta)) - u[:, 2] * pt.sin(theta),
                                u[:, 0] * u[:, 2] * (1 - pt.cos(theta)) + u[:, 1] * pt.sin(theta)]),
                      pt.stack([u[:, 0] * u[:, 1] * (1 - pt.cos(theta)) + u[:, 2] * pt.sin(theta),
                                pt.cos(theta) + u[:, 1] ** 2 * (1 - pt.cos(theta)),
                                u[:, 1] * u[:, 2] * (1 - pt.cos(theta)) - u[:, 0] * pt.sin(theta)]),
                      pt.stack([u[:, 0] * u[:, 2] * (1 - pt.cos(theta)) - u[:, 1] * pt.sin(theta),
                                u[:, 1] * u[:, 2] * (1 - pt.cos(theta)) + u[:, 0] * pt.sin(theta),
                                pt.cos(theta) + u[:, 2] ** 2 * (1 - pt.cos(theta))])])

        return R.permute(2,0,1)


    def orthogonal_projection(self, iptut_tensor, projection_tensor):
        #divisor = pt.tile(pt.power(pt.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).permute(1,2,0)
        divisor = pt.tile(pt.pow(pt.linalg.norm(projection_tensor, axis=2), 2), (3, 1, 1)).permute(1, 2, 0)

        dot_product = pt.tile(pt.sum(projection_tensor*iptut_tensor, axis=2),(3,1,1)).permute(1,2,0)
        return dot_product*projection_tensor/divisor
    
    def normalize_components(self,iptut_tensor):
        iptut_tensor[pt.isnan(iptut_tensor)] = 0
        return iptut_tensor/pt.tile(pt.linalg.norm(iptut_tensor,axis=2),(3,1,1)).permute(1,2,0)

    def get_normal_vector_of_plane(self, strikes, dips):
        strike_rotation = self.rotation_along_Z(strikes)
        dip_dir = strikes + 90
        dip_dir[dip_dir > 360] = dip_dir[dip_dir > 360] - 360
        dip_dir_rotation = self.rotation_along_Z(dip_dir)
        #vec1 = pt.matmul(strike_rotation,[0,1,0])
        vec1 = pt.matmul(strike_rotation, pt.tensor([0, 1, 0], dtype=pt.float32))

        #vec_dip_dir = pt.tile(pt.matmul(dip_dir_rotation,[0,1,0]),(3,1,1)).permute(1,2,0) # permute right ?
        vec_dip_dir = pt.tile(pt.matmul(dip_dir_rotation, pt.tensor([0, 1, 0], dtype=pt.float32)), (3, 1, 1)).permute(1,
                                                                                                                      2,
                                                                                                                      0)  # permute right ?

        dip_rotation = self.arbitrary_rotation(dips, vec1)
        vec2 = pt.sum(dip_rotation*vec_dip_dir,axis=2)
        normal_vector = pt.cross(vec2, vec1, axis=1)
        return normal_vector, vec1, vec2
    

    def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        ## Todo; Use azimuth and dip or dip direction
        n_freq = tensor_in.shape[1]
        normal_vec, t1, t2 = self.get_normal_vector_of_plane(strikes, dips)
        normal_tensor = pt.tile(normal_vec, (n_freq,1,1)).permute(1,0,2)
        t1_tensor = pt.tile(t1, (n_freq,1,1)).permute(1,0,2)
        t2_tensor = pt.tile(t2, (n_freq,1,1)).permute(1,0,2)
        normal_component = self.orthogonal_projection(tensor_in,normal_tensor)
        t1_component = self.orthogonal_projection(tensor_in,t1_tensor)
        t2_component = self.orthogonal_projection(tensor_in,t2_tensor)
        return self.normalize_components(normal_component), self.normalize_components(t1_component), self.normalize_components(t2_component)


















class fracEM_np_SI:
# X Y Z strike dip AREA THICKNESS PERM COND

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.c = 299792458 # m/ns
        self.propagation_terms = 'total-field'
        self.mode = 'transmission'
        self.apply_causality = True
        self.m0 = 4*np.pi*10**-7 # Henries per meter
        # self.m0 = 4*np.pi*10**11 # Henries per meter (ns)
        self.e0 = np.divide(1,self.m0*self.c**2) # Fahrads per meter
        
        
    def source(self, a, b, g, ph,t, f):
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)
        C = (10**12)/(4*np.pi*self.c**2)
        src_vec = source_spectrum*C
        src_vec = src_vec/np.max(np.abs(src_vec))
        return src_vec
    
    def complex_epsilon(self, omega, epsilon, sigma):
        return self.e0*epsilon + np.divide(1j*sigma, omega)
    
    def wavenumber(self, omega, epsilon, sigma):
        # k = np.divide(omega, self.c)*np.sqrt(epsilon + np.divide(1j*sigma,(10**9)*omega*self.e0))
        k = omega*np.sqrt(self.m0*self.complex_epsilon(omega, epsilon, sigma))
        k[np.isnan(k)] = 0
        return k

    def matrix_vector_multiply(self, matrix, vector):
        vector2matrix = np.broadcast_to(vector.transpose(2,0,1), (3,3,vector.shape[0], vector.shape[1])).transpose(2,3,1,0)
        
        

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, omega, eps, cond, source_dipole, s_loc, obs_loc):
        R_vec = obs_loc - s_loc
        nr = np.tile(np.linalg.norm(R_vec, axis=2),(3,1,1)).transpose(1,2,0)
        R = np.broadcast_to(nr.transpose(2,0,1),(3,3,omega.shape[0],omega.shape[1])).transpose(2,3,0,1)
        RR = np.broadcast_to(R_vec.transpose(2,0,1),(3,3,omega.shape[0],omega.shape[1])).transpose(2,3,0,1)
        k = self.wavenumber(omega,eps,cond)
        kt = np.broadcast_to(k.transpose(2,0,1),(3,3,omega.shape[0],omega.shape[1])).transpose(2,3,0,1)
        R_outer = np.multiply(RR,RR.transpose(0,1,3,2))
        Green = np.divide(np.exp(1j*kt*R), 4*np.pi*R)*((1 + np.divide(1j*kt*R - 1, (kt**2)*(R**2))) + np.divide(3 - 3j*kt*R - (kt**2)*(R**2),(kt**2)*(R**2))*np.divide(R_outer,R))
        Green[np.isnan(Green)] = 0
        source_tensor = np.broadcast_to(source_dipole.transpose(2,0,1),(3,3,omega.shape[0],omega.shape[1])).transpose(2,3,1,0) # LAST TWO DIMENSIONS FLIPPED 
        Efield = ((omega*self.m0)**2)*np.sum(Green*source_tensor,axis=3)
        # Efield = ((omega)**2)*np.sum(Green*source_tensor,axis=3)
        # if(self.apply_causality == True):
        #     causality_index = k*nr/(2*np.pi*frq) > frq.shape[2]/(2*np.max(frq.ravel()))
        #     Fr[causality_index] = 0
        Efield[np.isnan(Efield)] = 0
        return Efield
    
    def source_to_dipoles(self, omega, beps, bsig, source_tensor, source_location_tensor, dipole_tensor):
        incoming_field = self.propagation(omega, beps, bsig, source_tensor, source_location_tensor, dipole_tensor[:,:,0:3])
        area = np.tile(dipole_tensor[:,:,5],(3,1,1)).transpose(1,2,0)
        aperture = np.tile(dipole_tensor[:,:,6],(3,1,1)).transpose(1,2,0)
        permittivity = np.tile(dipole_tensor[:,:,7],(3,1,1)).transpose(1,2,0)
        conductivity = np.tile(dipole_tensor[:,:,8],(3,1,1)).transpose(1,2,0)
        
        if(self.mode == 'reflection'):
            modulation = self.reflection(omega, beps, bsig, area, aperture, permittivity, conductivity)
        elif(self.mode == 'transmission'):
            modulation = self.transmission(omega, beps, bsig, area, aperture, permittivity, conductivity)
        else:
            print('Choose between -transmission- and -reflection- polarization settings.')
            return
        # def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        # nc, t1c, t2c = self.get_vector_components_on_plane(incoming_field, dipole_tensor[:,0,3], dipole_tensor[:,0,4])
        modulated_field = self.efield_to_dipole(modulation*incoming_field, permittivity, conductivity,omega)
        # dipole_field = modulated_field*nc + modulated_field*t1c + modulated_field*t2c
        return modulated_field
    
    def efield_to_dipole(self, Efield, epsilon, sigma, omega):
        Efield = np.divide(Efield,1j*omega)
        Efield[np.isnan(Efield)] = 0
        return Efield
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def reflection(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        rte = np.divide(k1 - k2, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        pol = area*np.divide(rte*(1-expn), 1-(rte**2)*expn)
        # pol = np.divide(area*1j*tbrte, omega)
        pol[np.isnan(pol)] = 0
        pol[np.isinf(pol)] = 0
        return pol

    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def transmission(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        r21 = np.divide(k1 - k2, k1 + k2)
        t12 = np.divide(2*k2, k1 + k2)
        t21 = np.divide(2*k1, k1 + k2)
        pol = area*np.divide(t12*t21*np.exp(-2j*aperture*k2), 1 - r21*r21*np.exp(-2j*k2*aperture))
        # pol = np.divide(area*1j*tbrte, omega)
        pol[np.isnan(pol)] = 0
        pol[np.isinf(pol)] = 0
        return pol


    def only_propagation(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or):
        n_freq = f.shape[0]
        n_dipoles = 1
        source_tensor = np.tile(np.outer(s_or,source_in), (n_dipoles,1,1)).transpose(0,2,1)
        omega_tensor = 2*np.pi*(10**9)*np.tile(f,(n_dipoles,3,1)).transpose(0,2,1)
        source_location_tensor = np.tile(s_loc,(n_freq,n_dipoles,1)).transpose(1,0,2)
        receiver_location_tensor = np.tile(r_loc,(n_freq,n_dipoles,1)).transpose(1,0,2)
        direct_wave = self.propagation(omega_tensor, beps, bsig, source_tensor, source_location_tensor, receiver_location_tensor)
        summed_response = np.sum(direct_wave,axis=0)
        receiver_orientation_tensor = np.tile(r_or,(n_freq,1))
        direct_response = np.sum(receiver_orientation_tensor*summed_response,axis=1)
        return direct_response


    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, coupling=0, mode='transmission'):
        if(mode in ['transmission', 'reflection']):
            self.mode = mode
        print('Computing in ' + str(self.mode) + ' mode.')
        n_dipoles = dipoles.shape[0]
        n_freq = f.shape[0]
        source_tensor = np.tile(np.outer(s_or,source_in), (n_dipoles,1,1)).transpose(0,2,1)
        omega_tensor = 2*np.pi*(10**9)*np.tile(f,(n_dipoles,3,1)).transpose(0,2,1)
        source_location_tensor = np.tile(s_loc,(n_freq,n_dipoles,1)).transpose(1,0,2)
        receiver_location_tensor = np.tile(r_loc,(n_freq,n_dipoles,1)).transpose(1,0,2)
        dipole_tensor = np.tile(dipoles,(n_freq,1,1)).transpose(1,0,2)
        dipole_response = self.source_to_dipoles(omega_tensor, beps, bsig, source_tensor, source_location_tensor, dipole_tensor)
        receiver_response = self.propagation(omega_tensor, beps, bsig, dipole_response, dipole_tensor[:,:,0:3], receiver_location_tensor)
        direct_response = []
        summed_response = np.sum(receiver_response,axis=0)
        receiver_orientation_tensor = np.tile(r_or,(n_freq,1))
        fracture_response = np.sum(receiver_orientation_tensor*summed_response,axis=1)
        if(self.mode == 'transmission'):
            direct_response = self.only_propagation(f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or)
            return fracture_response, direct_response
        else:
            return fracture_response


    def rotation_along_Z(self, angles):
        angle = np.deg2rad(angles)
        zeros = np.zeros(angles.shape[0])
        ones = np.ones(angles.shape[0])
        R = np.array([[np.cos(angle),   np.sin(angle),  zeros],
                    [-np.sin(angle),    np.cos(angle),  zeros],
                    [zeros,                 zeros,       ones]])
        return R.transpose(2,0,1)

    def arbitrary_rotation(self, angle, u):
        theta = np.deg2rad(angle)
        R = np.array([[np.cos(theta) + u[:,0]**2 * (1-np.cos(theta)), 
                      u[:,0] * u[:,1] * (1-np.cos(theta)) - u[:,2] * np.sin(theta), 
                      u[:,0] * u[:,2] * (1 - np.cos(theta)) + u[:,1] * np.sin(theta)],
                    [u[:,0] * u[:,1] * (1-np.cos(theta)) + u[:,2] * np.sin(theta),
                      np.cos(theta) + u[:,1]**2 * (1-np.cos(theta)),
                      u[:,1] * u[:,2] * (1 - np.cos(theta)) - u[:,0] * np.sin(theta)],
                    [u[:,0] * u[:,2] * (1-np.cos(theta)) - u[:,1] * np.sin(theta),
                      u[:,1] * u[:,2] * (1-np.cos(theta)) + u[:,0] * np.sin(theta),
                      np.cos(theta) + u[:,2]**2 * (1-np.cos(theta))]])
        return R.transpose(2,0,1)


    def orthogonal_projection(self, input_tensor, projection_tensor):
        divisor = np.tile(np.power(np.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).transpose(1,2,0)
        dot_product = np.tile(np.sum(projection_tensor*input_tensor, axis=2),(3,1,1)).transpose(1,2,0)
        return dot_product*projection_tensor/divisor
    
    def normalize_components(self,input_tensor):
        input_tensor[np.isnan(input_tensor)] = 0
        return input_tensor/np.tile(np.linalg.norm(input_tensor,axis=2),(3,1,1)).transpose(1,2,0)

    def get_normal_vector_of_plane(self, strikes, dips):
        strike_rotation = self.rotation_along_Z(strikes)
        dip_dir = strikes + 90
        dip_dir[dip_dir > 360] = dip_dir[dip_dir > 360] - 360
        dip_dir_rotation = self.rotation_along_Z(dip_dir)
        vec1 = np.matmul(strike_rotation,[0,1,0])
        vec_dip_dir = np.tile(np.matmul(dip_dir_rotation,[0,1,0]),(3,1,1)).transpose(1,2,0) # transpose right ?
        dip_rotation = self.arbitrary_rotation(dips, vec1)
        vec2 = np.sum(dip_rotation*vec_dip_dir,axis=2)
        normal_vector = np.cross(vec2, vec1, axis=1)
        return normal_vector, vec1, vec2
    

    def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        n_freq = tensor_in.shape[1]
        normal_vec, t1, t2 = self.get_normal_vector_of_plane(strikes, dips)
        normal_tensor = np.tile(normal_vec, (n_freq,1,1)).transpose(1,0,2)
        t1_tensor = np.tile(t1, (n_freq,1,1)).transpose(1,0,2)
        t2_tensor = np.tile(t2, (n_freq,1,1)).transpose(1,0,2)
        normal_component = self.orthogonal_projection(tensor_in,normal_tensor)
        t1_component = self.orthogonal_projection(tensor_in,t1_tensor)
        t2_component = self.orthogonal_projection(tensor_in,t2_tensor)
        return self.normalize_components(normal_component), self.normalize_components(t1_component), self.normalize_components(t2_component)

