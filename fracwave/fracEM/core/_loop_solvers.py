"""
Created on Thu Nov  5 15:49:17 2020

@author: Alexis Shakas, shakasa@lilo.org
"""
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
from fracwave.fracEM.core import _deprecated_fracture_math
from numba import jit
from fracwave.geometry.vector_calculus import vector_calculus

class fracEM_SI:
    # X Y Z strike dip AREA THICKNESS PERM COND

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.c = 299792458
        self.propagation_terms = 'total-field'
        self.mode = 'transmission'
        self.apply_causality = False
        self.m0 = 4*np.pi*10**-7 # Henries per meter
        self.e0 = np.divide(1,self.m0*self.c**2) # Fahrads per meter
        self.vc = vector_calculus()
        
    def source(self, a, b, g, ph, t0, f):
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)*np.exp(2j*np.pi*f*t0)
        C = (10**12)/(4*np.pi*self.c**2)
        src_vec = source_spectrum*C
        src_vec = src_vec/np.max(np.abs(src_vec))
        return src_vec
    
    def complex_epsilon(self, omega, epsilon, sigma):
        return self.e0*epsilon - np.divide(1j*sigma, omega)
    
    def wavenumber(self, omega, epsilon, sigma):
        # k = np.divide(omega, self.c)*np.sqrt(epsilon - np.divide(1j*sigma,omega*self.e0))
        k = omega*np.sqrt(self.m0*self.complex_epsilon(omega, epsilon, sigma))
        k[np.isnan(k)] = 0
        return k

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, omega, eps, cond, source_dipole, s_loc, obs_loc):
        R_vec = obs_loc - s_loc
        n_frequencies = omega.shape[0]
        R = np.sqrt(np.sum(R_vec**2,axis=0))
        k = self.wavenumber(omega,eps,cond)
        kt = np.broadcast_to(k,(3,3,n_frequencies))
        R_outer = np.outer(R_vec, R_vec)
        R_outer = np.broadcast_to(R_outer,(n_frequencies,3,3)).transpose(1,2,0)
        first_term = np.divide(np.exp(1j*kt*R), 4*np.pi*R)
        second_term = (1 + np.divide(1j*kt*R - 1, (kt**2)*(R**2)))
        I = np.broadcast_to(np.identity(3),(n_frequencies,3,3)).transpose(1,2,0)
        third_term = np.divide(3 - 3j*kt*R - (kt**2)*(R**2),(kt**2)*(R**2))*np.divide(R_outer,R**2)
        Green = first_term*(second_term*I + third_term)
        Green[np.isnan(Green)] = 0
        source_tensor = np.broadcast_to(source_dipole,(3,3,n_frequencies))
        Efield = ((omega*self.m0)**2)*np.sum(Green*source_tensor, axis=1)
        if(self.apply_causality == True):
            causality_index = kt*R/(omega) > n_frequencies/(2*np.max(omega.ravel()))
            if(np.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[causality_index] = 0
        Efield[np.isnan(Efield)] = 0
        return Efield
    
    # # function to compute the propagation of the electric field from a point dipole
    # def propagation(self, omega, eps, cond, src_vec, s_loc, obs_loc):
    #     r_vec = obs_loc - s_loc
    #     nr = np.sqrt(np.sum(r_vec**2))
    #     rh = np.broadcast_to(r_vec/nr, (omega.shape[0],3)).transpose(1,0)
    #     k = self.wavenumber(omega, eps, cond)
    #     k = np.broadcast_to(k,(3,omega.shape[0]))
    #     k2 = k**2
    #     dot1 = np.broadcast_to(np.sum(src_vec*rh,axis=0),(3,omega.shape[0]))
    #     dtprd = rh*dot1
    #     Fr = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr)
    #     #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
    #     Fr[np.isnan(Fr)] = 0
    #     return Fr
    
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def reflection(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        rte = np.divide(k1-k2, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        pol = np.divide(2j*area*tbrte, omega)
        # pol = area*tbrte
        pol[np.isnan(pol)] = 0
        pol[np.isinf(pol)] = 0
        return pol

    # # function to compute the polarization of a dipole element (using effective reflection coefficients)
    # def transmission(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
    #     k1 = self.wavenumber(omega, beps, bsig)
    #     k2 = self.wavenumber(omega, permittivity, conductivity)
    #     r21 = np.divide(k1 - k2, k1 + k2)
    #     t12 = np.divide(2*k2, k1 + k2)
    #     t21 = np.divide(2*k1, k1 + k2)
    #     expn = np.exp(-2j*aperture*k2)
    #     pol = area*np.divide(t12*t21*expn, 1 - r21*r21*expn)
    #     # pol = np.divide(area*1j*tbrte, omega)
    #     pol[np.isnan(pol)] = 0
    #     pol[np.isinf(pol)] = 0
    #     return pol



    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, coupling=0, mode='transmission'):
        if(mode in ['transmission', 'reflection', 'dipole-field']):
            self.mode = mode
        source_tensor = np.outer(source_in, s_or).T
        n_dipoles = dipoles.shape[0]
        n_frequencies = f.shape[0]
        omega = 2*np.pi*f*10**9
        incoming_field = np.zeros((3,n_frequencies,n_dipoles))
        modulation = np.zeros((n_frequencies,n_dipoles))
        dipole_response = np.zeros((3,n_frequencies,n_dipoles))
        print('Computing in ' + str(self.mode) + ' mode for ' + str(n_dipoles) + ' dipole elements and ' + str(n_frequencies) + ' frequencies.')
        for n, dipole in enumerate(dipoles):
            incoming_field[:,:,n] = self.propagation(omega, beps, bsig, source_tensor, s_loc, dipole[0:3])
            area = dipole[5]
            aperture = dipole[6]
            permittivity = dipole[7]
            conductivity = dipole[8]
            if(self.mode == 'reflection'):
                modulation[:,n] = self.reflection(omega, beps, bsig, area, aperture, permittivity, conductivity)
            elif(self.mode == 'transmission'):
                modulation[:,n] = self.transmission(omega, beps, bsig, area, aperture, permittivity, conductivity)
            else:
                print('Choose between -transmission- and -reflection- polarization settings.')
                return
        
        dipole_response = np.broadcast_to(modulation, (3,n_frequencies,n_dipoles))*incoming_field

        if(self.mode == 'dipole-field'):
            return dipole_response

        receiver_response = np.zeros((3,n_frequencies,n_dipoles))
        for n, dipole in enumerate(dipoles):
            receiver_response[:,:,n] = self.propagation(omega, beps, bsig, dipole_response[:,:,n],  dipole[0:3], r_loc)
        
        summed_response = np.sum(receiver_response,axis=2) # sum over all dipoles
        fracture_response = np.matmul(summed_response.T,r_or) # sum over the dipole orientation
        if(self.mode == 'transmission'):
            direct_response = self.only_propagation(f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or)
            return fracture_response, direct_response
        else:
            return fracture_response



class fracEM_cgs:
# X Y Z strike dip AREA THICKNESS PERM COND

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.c = 29.9792458 # cm/ns
        self.vc = vector_calculus()
        self.apply_causality = False
        self.mode = 'reflection'
        self._inc = []
    
    def source(self, a, b, g, ph, t0, f):
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)*np.exp(2j*np.pi*f*t0)
        return source_spectrum/np.max(np.abs(source_spectrum))

    def filter_errors(self,array):
        array[np.isnan(array)] = 0
        array[np.isinf(array)] = 0
        return array
    
    def wavenumber(self, f, eps, sig):
        F = np.sqrt(eps)*2*np.pi*f*(np.sqrt(np.sqrt(0.25 + (9*sig/(f*eps))**2) + 0.5) - 1j*np.sqrt(np.sqrt(0.25 + (9*sig/(f*eps))**2) - 0.5))/self.c
        return F        

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, f, eps, cond, src_vec, s_loc, obs_loc):
        r_vec = obs_loc - s_loc
        nr = np.sqrt(np.sum(np.power(r_vec,2)))
        rh = np.tile(r_vec/nr, (f.shape[0],1)).transpose(1,0)
        k = self.wavenumber(f, eps, cond)
        k = np.tile(k,(3,1))
        k2 = np.power(k,2)
        dot1 = np.tile(np.sum(src_vec*rh,axis=0),(3,1))
        dtprd = rh*dot1
        Efield = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr)
        #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
        Efield = self.filter_errors(Efield)
        if(self.apply_causality == True):
            causality_index = k*nr/(2*np.pi*f) > f.shape[1]/(2*2*np.pi*np.max(f.ravel()))
            if(np.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[causality_index] = 0
        return Efield
    
    
    def only_propagation(self, f, eps, cond, src_vec, s_loc, obs_loc, r_or):
        Efield = self.propagation(f, eps, cond, src_vec, s_loc, obs_loc)
        Efield = np.sum(Efield*np.tile(r_or,(f.shape[0],1)).transpose(1,0), axis=0)
        return Efield

    def field_to_dipole(self, f, Efield):
        dipole = np.divide(1j*Efield,2*np.pi*np.tile(f,(3,1)))
        dipole = self.filter_errors(dipole)
        return dipole
    

    def source_to_dipole(self, f, beps, bsig, source_tensor, source_location_tensor, dipole):
        incoming_field = self.propagation(f, beps, bsig, source_tensor, source_location_tensor, dipole[0:3])

        TE, TM = self.polarization(f, beps, bsig, dipole[5::])
        # ---------- New to save the matrix
        self._inc.append(TE.copy())
        # ------------
        # 3 is strike and 4 is dip ?????????
        nc, t1c, t2c = self.vc.get_vector_components_on_plane(incoming_field, dipole[3], dipole[4])
        # modulated_field = modulation*incoming_field
        radiated_field = TE*nc + TM*t1c + TM*t2c
        radiated_field = self.field_to_dipole(f,radiated_field)
        return radiated_field
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def polarization(self, f, beps, bsig, dipole_tensor):
        area = dipole_tensor[0]
        aperture = dipole_tensor[1]
        permittivity = dipole_tensor[2]
        conductivity = dipole_tensor[3]
        k1 = self.wavenumber(f, beps, bsig)
        k2 = self.wavenumber(f, permittivity, conductivity)
        if(self.mode == 'reflection'):
            TE, TM = self.reflection(k1, k2, aperture)
        elif(self.mode == 'transmission'):
            TE, TM = self.transmission(k1, k2, aperture)
        elif(self.mode == 'dipole-field'):
            TE = np.zeros((3,f.shape[0]))
            TM = np.zeros((3,f.shape[0]))
        
        TM = self.filter_errors(TM)
        TE = self.filter_errors(TE)
        return TE*area, TM*area
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def reflection(self, k1, k2, aperture):
        rte = np.divide(k1-k2, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        tbrtm = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        return tbrte, tbrtm

    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def transmission(self, k1, k2, aperture):
        r21 = np.divide(k1 - k2, k1 + k2)
        t12 = np.divide(2*k2, k1 + k2)
        t21 = np.divide(2*k1, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(t12*t21*expn, 1 - r21*r21*expn)
        tbrtm = np.divide(t12*t21*expn, 1 - r21*r21*expn)
        return tbrte, tbrtm

    # @jit(nopython=True)
    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, mode='reflection'):
        
        n_dipoles = dipoles.shape[0]
        n_freq = f.shape[0]
        
        if(mode in ['transmission', 'reflection', 'dipole-field']):
            self.mode = mode
            print('Running in ' + mode + ' mode for ' + str(n_dipoles) + ' dipoles and ' + str(n_freq) + ' frequencies.')
        else:
            print('Mode not specified correctly or not at all. Using default: ' + self.mode)
        

        source_matrix = np.outer(s_or,source_in)
        dipole_response = np.empty((3, n_freq, n_dipoles),dtype=complex)
        receiver = np.empty((3, n_freq, n_dipoles),dtype=complex)
        for n in range(n_dipoles):
            dipole_response[:,:,n] = self.source_to_dipole(f, beps, bsig, source_matrix, s_loc, dipoles[n,:])
            
        for n in range(n_dipoles):
            receiver[:,:,n] = self.propagation(f, beps, bsig, dipole_response[:,:,n], dipoles[n,0:3], r_loc)
        summed_response = np.sum(receiver,axis=2)
        receiver_orientation_tensor = np.tile(r_or,(n_freq,1)).transpose(1,0)
        axis_response = np.sum(receiver_orientation_tensor*summed_response,axis=0)

        return axis_response
#
#
            
        
        
        
        
#        
#class fracEM_SI:
## X Y Z strike dip AREA THICKNESS PERM COND
#
#    def __init__(self):
#        self.main_dir = '/forward-modeling/'
#        self.save_dir = '/output/'
#        self.c = 299792458 # m/s
#        self.propagation_terms = 'total-field'
#        self.mode = 'transmission'
#        self.apply_causality = False
#        self.m0 = 4*np.pi*(10**-7) # Henries per meter
#        self.e0 = np.divide(1,self.m0*self.c**2) # Fahrads per meter
#        self.vc = fracture_math.vector_calculus()
#        
#    def source(self, a, b, g, ph, t0, f):
#        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)*np.exp(2j*np.pi*f*t0)
#        return source_spectrum/np.max(np.abs(source_spectrum))
#
#
#
#    def filter_errors(self,array):
#        array[np.isnan(array)] = 0
#        array[np.isinf(array)] = 0
#        return array
#    
#    def complex_epsilon(self, omega, epsilon, sigma):
#        return self.e0*epsilon - np.divide(1j*sigma, omega)
#    
#    def wavenumber(self, omega, epsilon, sigma):
#        # k = np.divide(omega, self.c)*np.sqrt(epsilon + np.divide(1j*sigma,(10**9)*omega*self.e0))
#        k = omega*np.sqrt(self.m0*self.complex_epsilon(omega, epsilon, sigma))
#        k[np.isnan(k)] = 0
#        return k
#
#    def matrix_vector_multiply(self, matrix, vector):
#        # first cast the vector into a matrix, then flip the axis, multiply and sum the rows
#        n_frequencies = vector.shape[1]
#        vector2matrix = np.broadcast_to(vector, (3,3,n_frequencies))
#        return np.multiply(matrix, vector2matrix).sum(axis=1)
#
#    def check_matrix_symmetric(self, a, rtol=1e-05, atol=1e-08):
#        return np.allclose(a, a.T, rtol=rtol, atol=atol)
#    
#    def check_tensor_symmetric(self, a, rtol=1e-05, atol=1e-08):
#        b = np.reshape(a,(3,3,np.prod((a.shape[2],a.shape[3]))))
#        sym = np.ones((b.shape[2]))
#        for index in range(b.shape[2]):
#            if(self.check_matrix_symmetric(b[:,:,index]) == True):
#                sym[index] = 0
#        print('Non symmetric entries: ' + str(np.sum(sym)))
#    
#    # function to compute the propagation of the electric field from a point dipole
#    def propagation(self, omega, eps, cond, source_dipole, s_loc, obs_loc):
#        R_vec = obs_loc - s_loc
#        R = np.sqrt(np.sum(R_vec**2,axis=0))
#        k = self.wavenumber(omega,eps,cond)
#        kb = np.broadcast_to(k,(3,3,omega.shape[0]))
#        R_outer = np.outer(R_vec, R_vec)
#        term_3b = np.broadcast_to(np.divide(R_outer,R**2), (k.shape[0],3,3)).transpose(1,2,0)
#        I = np.broadcast_to(np.identity(3), (k.shape[0],3,3)).transpose(1,2,0)
#        first_term = np.divide(np.exp(1j*kb*R), 4*np.pi*R)
#        second_term = (1 + np.divide(1j*kb*R - 1, (kb**2)*(R**2)))*I
#        third_term = np.divide(3 - 3j*kb*R - (kb**2)*(R**2),(kb**2)*(R**2))*term_3b
#        Green = first_term*(second_term + third_term)
#        Green[np.isnan(Green)] = 0
#        Efield = ((omega*self.m0)**2)*self.matrix_vector_multiply(Green, source_dipole)
#        if(self.apply_causality == True):
#            causality_index = k*R/(omega) > omega.shape[1]/(2*np.max(omega.ravel()))
#            if(np.sum(causality_index) > 0):
#                print('Part of the signal is not causal and is omitted')
#            Efield[causality_index] = 0
#        Efield[np.isnan(Efield)] = 0
#        return Efield
#
#    def efield_to_dipole(self, Efield, omega):
#        Efield = np.divide(2j*Efield,omega)
#        Efield = self.filter_errors(Efield)
#        return Efield
#
#
#    def source_to_dipoles(self, omega, beps, bsig, source, source_location, dipole):
#        incoming_field = self.propagation(omega, beps, bsig, source, source_location, dipole[0:3])
#        TE, TM = self.polarization(omega, beps, bsig, dipole[5::])
#        nc, t1c, t2c = self.vc.get_vector_components_on_plane(incoming_field, dipole[3], dipole[4])
#        # modulated_field = modulation*incoming_field
#        radiated_field = TE*nc + TM*t1c + TM*t2c
#        radiated_field = self.efield_to_dipole(radiated_field,omega)
#        return radiated_field
#
#
#    def reflection(self, k1, k2, aperture):
#        rte = np.divide(k1-k2, k1 + k2)
#        expn = np.exp(-2j*aperture*k2)
#        tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
#        tbrtm = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
#        return tbrte, tbrtm
#
#    def transmission(self, k1, k2, aperture):
#        r21 = np.divide(k1 - k2, k1 + k2)
#        t12 = np.divide(2*k2, k1 + k2)
#        t21 = np.divide(2*k1, k1 + k2)
#        expn = np.exp(-2j*aperture*k2)
#        tbrte = np.divide(t12*t21*expn, 1 - r21*r21*expn)
#        tbrtm = np.divide(t12*t21*expn, 1 - r21*r21*expn)
#        return tbrte, tbrtm
#
#
#    def polarization(self, f, beps, bsig, dipole_tensor):
#        area = dipole_tensor[0]
#        aperture = dipole_tensor[1]
#        permittivity = dipole_tensor[2]
#        conductivity = dipole_tensor[3]
#        k1 = self.wavenumber(f, beps, bsig)
#        k2 = self.wavenumber(f, permittivity, conductivity)
#        if(self.mode == 'reflection'):
#            TE, TM = self.reflection(k1, k2, aperture)
#        elif(self.mode == 'transmission'):
#            TE, TM = self.transmission(k1, k2, aperture)
#        elif(self.mode == 'dipole-field'):
#            TE = np.zeros((3,f.shape[0]))
#            TM = np.zeros((3,f.shape[0]))
#        
#        TM = self.filter_errors(TM)
#        TE = self.filter_errors(TE)
#        return TE*area, TM*area
#
#    def only_propagation(self, omega, eps, cond, src_vec, s_loc, obs_loc, r_or):
#        Efield = self.propagation(omega, eps, cond, src_vec, s_loc, obs_loc)
#        Efield = np.sum(Efield*np.tile(r_or,(omega.shape[0],1)).transpose(1,0), axis=0)
#        return Efield
#
#
#    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, coupling=0, mode='transmission'):
#        n_dipoles = dipoles.shape[0]
#        n_frequencies = f.shape[0]
#        if(mode in ['transmission', 'reflection', 'dipole-field']):
#            self.mode = mode
#            print('Running in ' + mode + ' mode for ' + str(n_dipoles) + ' dipoles and ' + str(n_frequencies) + ' frequencies.')
#        else:
#            print('Mode not specified correctly or not at all. Using default: ' + self.mode)
#        print('Computing in ' + str(self.mode) + ' mode for ' + str(n_dipoles) + ' dipole elements and ' + str(n_frequencies) + ' frequencies.')
#
#        omega = 2*np.pi*f*(10**9)          
#        source_matrix = np.outer(s_or,source_in)
#        dipole_response = np.empty((3, n_frequencies, n_dipoles),dtype=complex)
#        receiver = np.empty((3, n_frequencies, n_dipoles),dtype=complex)
#        for n in range(n_dipoles):
#            dipole_response[:,:,n] = self.source_to_dipoles(omega, beps, bsig, source_matrix, s_loc, dipoles[n,:])
#            
#        for n in range(n_dipoles):
#            receiver[:,:,n] = self.propagation(omega, beps, bsig, dipole_response[:,:,n], dipoles[n,0:3], r_loc)
#        summed_response = np.sum(receiver,axis=2)
#        receiver_orientation_tensor = np.tile(r_or,(n_frequencies,1)).transpose(1,0)
#        fracture_response = np.sum(receiver_orientation_tensor*summed_response,axis=0)
#
#        if(self.mode == 'transmission'):
#            direct_response = self.only_propagation(omega, beps, bsig, source_in, s_loc, s_or, r_loc, r_or)
#            return fracture_response, direct_response
#        else:
#            return fracture_response

        
