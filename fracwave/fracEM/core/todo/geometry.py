import numpy as np
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D

class VectorCalculus():
    
    def rotation_along_Z(self, angle):
        angle = np.deg2rad(angle)
        return np.array([[np.cos(angle),   np.sin(angle),  0],
                        [-np.sin(angle),    np.cos(angle),  0],
                        [0,                 0,              1]])
    
    
    def arbitrary_rotation(self, angle, u):
        theta = np.deg2rad(angle)
        return [[np.cos(theta) + u[0]**2 * (1-np.cos(theta)), 
                 u[0] * u[1] * (1-np.cos(theta)) - u[2] * np.sin(theta), 
                 u[0] * u[2] * (1 - np.cos(theta)) + u[1] * np.sin(theta)],
                [u[0] * u[1] * (1-np.cos(theta)) + u[2] * np.sin(theta),
                 np.cos(theta) + u[1]**2 * (1-np.cos(theta)),
                 u[1] * u[2] * (1 - np.cos(theta)) - u[0] * np.sin(theta)],
                [u[0] * u[2] * (1-np.cos(theta)) - u[1] * np.sin(theta),
                 u[1] * u[2] * (1-np.cos(theta)) + u[0] * np.sin(theta),
                 np.cos(theta) + u[2]**2 * (1-np.cos(theta))]]
                    
    def orthogonal_projection(self, input_vector, projection_vector):
        divisor = np.power(np.linalg.norm(projection_vector),2)
        for row, value in enumerate(input_vector):
            input_vector[row,:] = np.dot(projection_vector,input_vector[row,:])*projection_vector/divisor
        return input_vector
    
    def get_vector_components_on_fault(self, vector_in, strike, dip):
        normal_vec, t1, t2 = self.get_fault_normal(strike, dip)
        normal_component = self.orthogonal_projection(vector_in,normal_vec)
        t1_component = self.orthogonal_projection(vector_in,t1)
        t2_component = self.orthogonal_projection(vector_in,t2)
#        print('norm original vector' + str(np.linalg.norm(vector_in)))
#        print('norm component vector' + str(np.linalg.norm(normal_component + t1_component + t2_component)))
        return normal_component, t1_component, t2_component
    
    
    def get_fault_normal(self, strike, dip):
        strike_rotation = self.rotation_along_Z(strike)
        dip_dir = strike + 90
        if(dip_dir > 360):
            dip_dir = dip_dir - 360
        dip_dir_rotation = self.rotation_along_Z(dip_dir)
        vec1 = np.matmul(strike_rotation,[0,1,0])
        vec_dip_dir = np.matmul(dip_dir_rotation,[0,1,0])
        dip_rotation = self.arbitrary_rotation(dip, vec1)
        vec2 = np.matmul(dip_rotation,vec_dip_dir)
        normal_vector = np.cross(vec2, vec1)
        return normal_vector, vec1, vec2
        
    def rotate_points(self, points,strike,dip):
        strike_rotation = self.rotation_along_Z(strike)
        vec1 = np.matmul(strike_rotation,[0,1,0])
        dip_rotation = self.arbitrary_rotation(dip, vec1)
        for index, point in enumerate(points):
            points[index,:] = np.matmul(dip_rotation, np.matmul(strike_rotation, points[index,:]))
        return points    
    
    def plane_from_normal(self,strike,dip):
        xx, yy = np.meshgrid([-1,0,1], [-1,0,1])
        initial_plane = np.zeros((9,3))
        initial_plane[:,0] = xx.ravel()
        initial_plane[:,1] = yy.ravel()
        return self.rotate_points(initial_plane,strike,dip)

        
    def plot_vec(self,vec_in,axes):
        axes.plot([0,vec_in[0]],[0,vec_in[1]],[0,vec_in[2]])
    
    def plot_unit_sphere(self,ax):
        N=50
        stride=2
        u = np.linspace(0, 2 * np.pi, N)
        v = np.linspace(0, np.pi, N)
        x = np.outer(np.cos(u), np.sin(v))
        y = np.outer(np.sin(u), np.sin(v))
        z = np.outer(np.ones(np.size(u)), np.cos(v))
        ax.plot_surface(x, y, z, linewidth=0.0, cstride=stride, rstride=stride,alpha=0.3)
    
    
#    def rotate_vector(self,dip,azimuth):
#        dip = np.deg2rad(dip)
#        azimuth = np.deg2rad(azimuth)
#        cd = np.cos(dip)
#        sd = np.sin(dip)
#        ca = np.cos(azimuth)
#        sa = np.sin(azimuth)
#        return np.array([[ca*cd, sa, -ca*sd], [-sa*cd, ca, sd*sa], [sd, 0., cd]])
    
    