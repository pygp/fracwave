import numpy as np
np.seterr(divide='ignore', invalid='ignore')
from core import fracture_math





class forward_solver:
# X Y Z strike dip AREA THICKNESS PERM COND

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.c = 29.9792458 # cm/ns
        self.vc = fracture_math.vector_calculus()
        self.apply_causality = False
        self.mode = 'reflection'
    
    def source(self, a, b, g, ph, t0, f):
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)*np.exp(2j*np.pi*f*t0)
        return source_spectrum/np.max(np.abs(source_spectrum))

    def filter_errors(self,array):
        array[np.isnan(array)] = 0
        array[np.isinf(array)] = 0
        return array
    
    def wavenumber(self, f, eps, sig):
        F = np.sqrt(eps)*2*np.pi*f*(np.sqrt(np.sqrt(0.25 + (9*sig/(f*eps))**2) + 0.5) - 1j*np.sqrt(np.sqrt(0.25 + (9*sig/(f*eps))**2) - 0.5))/self.c
        return F        

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, f, eps, cond, src_vec, s_loc, obs_loc):
        r_vec = obs_loc - s_loc
        nr = np.sqrt(np.sum(np.power(r_vec,2)))
        rh = np.tile(r_vec/nr, (f.shape[0],1)).transpose(1,0)
        k = self.wavenumber(f, eps, cond)
        k = np.tile(k,(3,1))
        k2 = np.power(k,2)
        dot1 = np.tile(np.sum(src_vec*rh,axis=0),(3,1))
        dtprd = rh*dot1
        Efield = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr)
        #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
        Efield = self.filter_errors(Efield)
        if(self.apply_causality == True):
            causality_index = k*nr/(2*np.pi*f) > f.shape[1]/(2*2*np.pi*np.max(f.ravel()))
            if(np.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[causality_index] = 0
        return Efield
    
    
    def only_propagation(self, f, eps, cond, src_vec, s_loc, obs_loc, r_or):
        Efield = self.propagation(f, eps, cond, src_vec, s_loc, obs_loc)
        Efield = np.sum(Efield*np.tile(r_or,(f.shape[0],1)).transpose(1,0), axis=0)
        return Efield

    def field_to_dipole(self, f, Efield):
        dipole = np.divide(2j*Efield,2*np.pi*np.tile(f,(3,1)))
        dipole = self.filter_errors(dipole)
        return dipole
    

    def source_to_dipole(self, f, beps, bsig, source_tensor, source_location_tensor, dipole):
        incoming_field = self.propagation(f, beps, bsig, source_tensor, source_location_tensor, dipole[0:3])
        TE, TM = self.polarization(f, beps, bsig, dipole[5::])
        # 3 is strike and 4 is dip ?????????
        nc, t1c, t2c = self.vc.get_vector_components_on_plane(incoming_field, dipole[3], dipole[4])
        # modulated_field = modulation*incoming_field
        radiated_field = TE*nc + TM*t1c + TM*t2c
        radiated_field = self.field_to_dipole(f,radiated_field)
        return radiated_field
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def polarization(self, f, beps, bsig, dipole_tensor):
        area = dipole_tensor[0]
        aperture = dipole_tensor[1]
        permittivity = dipole_tensor[2]
        conductivity = dipole_tensor[3]
        k1 = self.wavenumber(f, beps, bsig)
        k2 = self.wavenumber(f, permittivity, conductivity)
        if(self.mode == 'reflection'):
            TE, TM = self.reflection(k1, k2, aperture)
        elif(self.mode == 'transmission'):
            TE, TM = self.transmission(k1, k2, aperture)
        elif(self.mode == 'dipole-field'):
            TE = np.zeros((3,f.shape[0]))
            TM = np.zeros((3,f.shape[0]))
        
        TM = self.filter_errors(TM)
        TE = self.filter_errors(TE)
        return TE*area, TM*area
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def reflection(self, k1, k2, aperture):
        rte = np.divide(k1-k2, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        tbrtm = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        return tbrte, tbrtm

    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def transmission(self, k1, k2, aperture):
        r21 = np.divide(k1 - k2, k1 + k2)
        t12 = np.divide(2*k2, k1 + k2)
        t21 = np.divide(2*k1, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(t12*t21*expn, 1 - r21*r21*expn)
        tbrtm = np.divide(t12*t21*expn, 1 - r21*r21*expn)
        return tbrte, tbrtm
    
    
    
    
    
    
    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, mode='reflection'):
        
        n_dipoles = dipoles.shape[0]
        n_freq = f.shape[0]
        
        if(mode in ['transmission', 'reflection', 'dipole-field']):
            self.mode = mode
            print('Running in ' + mode + ' mode for ' + str(n_dipoles) + ' dipoles and ' + str(n_freq) + ' frequencies.')
        else:
            print('Mode not specified correctly or not at all. Using default: ' + self.mode)
        

        source_matrix = np.outer(s_or,source_in)
        dipole_response = np.empty((3, n_freq, n_dipoles),dtype=complex)
        receiver = np.empty((3, n_freq, n_dipoles),dtype=complex)
        for n in range(n_dipoles):
            dipole_response[:,:,n] = self.source_to_dipole(f, beps, bsig, source_matrix, s_loc, dipoles[n,:])
            
        for n in range(n_dipoles):
            receiver[:,:,n] = self.propagation(f, beps, bsig, dipole_response[:,:,n], dipoles[n,0:3], r_loc)
        summed_response = np.sum(receiver,axis=2)
        receiver_orientation_tensor = np.tile(r_or,(n_freq,1)).transpose(1,0)
        axis_response = np.sum(receiver_orientation_tensor*summed_response,axis=0)

        return axis_response



# class EffectiveDipoles:
# # X Y Z strike dip AREA THICKNESS PERM COND

#     def __init__(self):
#         self.main_dir = '/forward-modeling/'
#         self.save_dir = '/output/'
#         self.c = 29.9792458 # cm/ns


# # function co compute the source using the generalized gamma distribution
#     def source(self, a, b, g, ph, f):
#         source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph);
#         C = (10**12)/(4*np.pi*self.c**2);
#         src_vec = source_spectrum*C;
#         return src_vec

# # function to compute the complex wavenumber given frequency, permittivity and conductivity
#     def wavenumber(self, frq, eps, con):
#         Fr=(np.sqrt(eps)*2*np.pi*frq*(np.sqrt(np.sqrt(0.25+(9*np.divide(con,frq*eps))**2)+0.5))-1j*np.sqrt(np.sqrt(0.25+(9*np.divide(con,frq*eps))**2)-0.5))/self.c
#         Fr[np.isnan(Fr)] = 0
#         return Fr

#     # function to compute the propagation of the electric field from a point dipole
#     def propagation(self,frq, eps, cond, src_vec, s_loc, obs_loc):
#         r_vec = obs_loc - s_loc
#         nr = np.tile(np.sqrt(np.sum(r_vec**2, axis=1)),(3,1,1)).transpose(1,0,2)
#         rh = r_vec/nr;
#         k = self.wavenumber(frq, eps, cond)
#         k2 = k**2
#         dot1 = np.tile(np.sum(src_vec*rh,axis=1),(3,1,1)).transpose(1,0,2)
#         dtprd = rh*dot1
#         Fr = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr);
#         #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
#         Fr[np.isnan(Fr)] = 0;
#         return Fr

#     # function to compute the polarization of a dipole element (using effective reflection coefficients)
#     def polarization(self,frq, eps, con, area, thck, perm, cond):
#         k1 = self.wavenumber(frq, eps, con)
#         k2 = self.wavenumber(frq, perm, cond)
#         rte = np.divide(k1 - k2, k1 + k2)
#         expn = np.exp(-2j*thck*k2)
#         tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)
#         pol = np.divide(area*1j*tbrte, np.pi*frq)
#         pol[np.isnan(pol)] = 0
#         pol[np.isinf(pol)] = 0
#         return pol


#     def source_to_dipole(self,f, beps, bsig, source_tensor, source_location_tensor, dipoles):
#         n_freq = f.shape[2]
#         dipole_location_tensor = np.tile(dipoles[:,0:3],(n_freq,1,1)).transpose(1,2,0)
#         incoming_field = self.propagation(f, beps, bsig, source_tensor, source_location_tensor, dipole_location_tensor) 
#         nc, t1c, t2c = self.get_vector_components_on_plane(incoming_field, dipoles[:,3], dipoles[:,4])
#         modulation = self.polarization(f, beps, bsig, dipoles[:,5], dipoles[:,6], dipoles[:,7], dipoles[:,8])
#         out_er = np.array([[1,1,1]])
#         dipole_response = np.multiply(np.outer(modulation[0],out_er),nc) + np.outer(modulation[1],out_er)*t1c + np.outer(modulation[2],out_er)*t2c
#         return dipole_response

# def dipole_dipole(self, f, beps, bsig, src_vec, s_loc, dipole):
# d_c = self.rotate_source(dipole[3], dipole[4])
# p_d = self.propagation(f, beps, bsig, src_vec, s_loc, dipole[0:3])
# d_p = self.polarization(f, beps, bsig, dipole[5], dipole[6], dipole[7], dipole[8])  
# F = p_d*np.mathmult(d_c,d_p)
# return F


# # main function to compute effective dipoles response for a collection of elements
#     def effective_dipoles(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, coupling=0):
#         n_dipoles = dipoles.shape[0]
#         n_freq = f.shape[0]
#         source_tensor = np.tile(np.outer(s_or,source_in), (n_dipoles,1,1))
#         frequency_tensor = np.tile(f,(n_dipoles,3,1))
#         dipole_response = np.empty((n_dipoles, 3, n_freq),dtype=complex)
#         receiver = np.empty((n_dipoles, 3, n_freq),dtype=complex)
        
#         dipole_location_tensor = np.tile(dipoles[:,0:3],(n_freq,1,1)).transpose(1,2,0)
#         source_location_tensor = np.tile(s_loc,(n_freq,n_dipoles,1)).transpose(1,2,0)
        
#         dipole_response = self.source_to_dipole(frequency_tensor, beps, bsig, source_tensor, source_location_tensor, dipoles)
#         receiver = self.propagation(frequency_tensor, beps, bsig, dipole_response, dipoles[:,0:3], r_loc)
#         Fv = np.sum(receiver,2)
#         F = np.matmul(r_or, Fv)
#         # for d in index:
#         # if(coupling):
#         # d_ss = np.setdiff1d(index,d)
#         # for dd in d_ss:
#         # coupled_response[:,:,d] = dipole_response[:,:,d] + \
#         # 				self.dipole_dipole(f, beps, bsig, dipole_response[:,:,dd], dipoles[dd,0:3], dipoles[d,:])
#         return F



