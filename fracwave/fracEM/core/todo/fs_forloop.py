"""
Created on Thu Nov  5 15:49:17 2020

@author: Alexis Shakas, shakasa@lilo.org
"""
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
from core import fracture_math



class fracEM:
# X Y Z strike dip AREA THICKNESS PERM COND

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.c = 299792458
        self.propagation_terms = 'total-field'
        self.mode = 'transmission'
        self.apply_causality = False
        self.m0 = 4*np.pi*10**-7 # Henries per meter
        self.e0 = np.divide(1,self.m0*self.c**2) # Fahrads per meter
        self.vc = fracture_math.vector_calculus()
        
    def source(self, a, b, g, ph, t0, f):
        source_spectrum = (f**(a*g-1))*np.exp(-(f/b)**g)*np.exp(1j*ph)*np.exp(2j*np.pi*f*t0)
        C = (10**12)/(4*np.pi*self.c**2)
        src_vec = source_spectrum*C
        src_vec = src_vec/np.max(np.abs(src_vec))
        return src_vec
    
    def complex_epsilon(self, omega, epsilon, sigma):
        return self.e0*epsilon - np.divide(1j*sigma, omega)
    
    def wavenumber(self, omega, epsilon, sigma):
        # k = np.divide(omega, self.c)*np.sqrt(epsilon - np.divide(1j*sigma,omega*self.e0))
        k = omega*np.sqrt(self.m0*self.complex_epsilon(omega, epsilon, sigma))
        k[np.isnan(k)] = 0
        return k

    # function to compute the propagation of the electric field from a point dipole
    def propagation(self, omega, eps, cond, source_dipole, s_loc, obs_loc):
        R_vec = obs_loc - s_loc
        n_frequencies = omega.shape[0]
        R = np.sqrt(np.sum(R_vec**2,axis=0))
        k = self.wavenumber(omega,eps,cond)
        kt = np.broadcast_to(k,(3,3,n_frequencies))
        R_outer = np.outer(R_vec, R_vec)
        R_outer = np.broadcast_to(R_outer,(n_frequencies,3,3)).transpose(1,2,0)
        first_term = np.divide(np.exp(1j*kt*R), 4*np.pi*R)
        second_term = (1 + np.divide(1j*kt*R - 1, (kt**2)*(R**2)))
        I = np.broadcast_to(np.identity(3),(n_frequencies,3,3)).transpose(1,2,0)
        third_term = np.divide(3 - 3j*kt*R - (kt**2)*(R**2),(kt**2)*(R**2))*np.divide(R_outer,R**2)
        Green = first_term*(second_term*I + third_term)
        Green[np.isnan(Green)] = 0
        source_tensor = np.broadcast_to(source_dipole,(3,3,n_frequencies))
        Efield = ((omega*self.m0)**2)*np.sum(Green*source_tensor, axis=1)
        if(self.apply_causality == True):
            causality_index = kt*R/(omega) > n_frequencies/(2*np.max(omega.ravel()))
            if(np.sum(causality_index) > 0):
                print('Part of the signal is not causal and is omitted')
            Efield[causality_index] = 0
        Efield[np.isnan(Efield)] = 0
        return Efield
    
    # # function to compute the propagation of the electric field from a point dipole
    # def propagation(self, omega, eps, cond, src_vec, s_loc, obs_loc):
    #     r_vec = obs_loc - s_loc
    #     nr = np.sqrt(np.sum(r_vec**2))
    #     rh = np.broadcast_to(r_vec/nr, (omega.shape[0],3)).transpose(1,0)
    #     k = self.wavenumber(omega, eps, cond)
    #     k = np.broadcast_to(k,(3,omega.shape[0]))
    #     k2 = k**2
    #     dot1 = np.broadcast_to(np.sum(src_vec*rh,axis=0),(3,omega.shape[0]))
    #     dtprd = rh*dot1
    #     Fr = (k2*src_vec/nr + 1j*k*src_vec/nr**2 - 3j*k*dtprd/nr**4 - (k2*dtprd - src_vec)/nr**3 + 3*dtprd/nr**5)*np.exp(-1j*k*nr)
    #     #	Fr = ((3*dtprd/nr**2 - src_vec)/nr**3 + k*(src_vec - 3j*dtprd/nr**2)/nr**2 + k**2*(src_vec - dtprd/nr**2)/nr)*np.exp(-1j*k*nr);
    #     Fr[np.isnan(Fr)] = 0
    #     return Fr
    
    
    # function to compute the polarization of a dipole element (using effective reflection coefficients)
    def reflection(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
        k1 = self.wavenumber(omega, beps, bsig)
        k2 = self.wavenumber(omega, permittivity, conductivity)
        rte = np.divide(k1-k2, k1 + k2)
        expn = np.exp(-2j*aperture*k2)
        tbrte = np.divide(rte*(1-expn), 1-(rte**2)*expn)        
        pol = np.divide(2j*area*tbrte, omega)
        # pol = area*tbrte
        pol[np.isnan(pol)] = 0
        pol[np.isinf(pol)] = 0
        return pol

    # # function to compute the polarization of a dipole element (using effective reflection coefficients)
    # def transmission(self, omega, beps, bsig, area, aperture, permittivity, conductivity):
    #     k1 = self.wavenumber(omega, beps, bsig)
    #     k2 = self.wavenumber(omega, permittivity, conductivity)
    #     r21 = np.divide(k1 - k2, k1 + k2)
    #     t12 = np.divide(2*k2, k1 + k2)
    #     t21 = np.divide(2*k1, k1 + k2)
    #     expn = np.exp(-2j*aperture*k2)
    #     pol = area*np.divide(t12*t21*expn, 1 - r21*r21*expn)
    #     # pol = np.divide(area*1j*tbrte, omega)
    #     pol[np.isnan(pol)] = 0
    #     pol[np.isinf(pol)] = 0
    #     return pol



    def forward_pass(self, f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or, dipoles, coupling=0, mode='transmission'):
        if(mode in ['transmission', 'reflection', 'dipole-field']):
            self.mode = mode
        source_tensor = np.outer(source_in, s_or).T
        n_dipoles = dipoles.shape[0]
        n_frequencies = f.shape[0]
        omega = 2*np.pi*f*10**9
        incoming_field = np.zeros((3,n_frequencies,n_dipoles))
        modulation = np.zeros((n_frequencies,n_dipoles))
        dipole_response = np.zeros((3,n_frequencies,n_dipoles))
        print('Computing in ' + str(self.mode) + ' mode for ' + str(n_dipoles) + ' dipole elements and ' + str(n_frequencies) + ' frequencies.')
        for n, dipole in enumerate(dipoles):
            incoming_field[:,:,n] = self.propagation(omega, beps, bsig, source_tensor, s_loc, dipole[0:3])
            area = dipole[5]
            aperture = dipole[6]
            permittivity = dipole[7]
            conductivity = dipole[8]
            if(self.mode == 'reflection'):
                modulation[:,n] = self.reflection(omega, beps, bsig, area, aperture, permittivity, conductivity)
            elif(self.mode == 'transmission'):
                modulation[:,n] = self.transmission(omega, beps, bsig, area, aperture, permittivity, conductivity)
            else:
                print('Choose between -transmission- and -reflection- polarization settings.')
                return
        
        dipole_response = np.broadcast_to(modulation, (3,n_frequencies,n_dipoles))*incoming_field

        if(self.mode == 'dipole-field'):
            return dipole_response

        receiver_response = np.zeros((3,n_frequencies,n_dipoles))
        for n, dipole in enumerate(dipoles):
            receiver_response[:,:,n] = self.propagation(omega, beps, bsig, dipole_response[:,:,n],  dipole[0:3], r_loc)
        
        summed_response = np.sum(receiver_response,axis=2) # sum over all dipoles
        fracture_response = np.matmul(summed_response.T,r_or) # sum over the dipole orientation
        if(self.mode == 'transmission'):
            direct_response = self.only_propagation(f, beps, bsig, source_in, s_loc, s_or, r_loc, r_or)
            return fracture_response, direct_response
        else:
            return fracture_response



