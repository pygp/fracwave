"""
Created on Tue Nov 17 17:22:54 2020

@author: Alexis Shakas, shakasa@lilo.org
"""
import numpy as np
try:
    import dask.array as da
except:
    print('Dask not installed and can not be used. Running the normal version')
from fracwave.fracEM.core import _deprecated_fracture_math


with np.warnings.catch_warnings():
    np.warnings.filterwarnings('ignore', r'ComplexWarning:')


class fracture_properties:
    
    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.vc = fracture_math.vector_calculus()
        self.vertices = None
        self.faces = None
        self.normals = None
        self.fracture_properties = None

    def dipole_grid(self, r, Nc, dip, strike, thck, perm, sigma, dx):
        """
        Construct squared grid representing finite fault
        Args:
            r: radius distance from source. How far away the fracture is from the origin
            Nc: Fracture size [L, W]
            dip: angle
            strike: angle
            thck: aperture fault [m]
            perm: relative elecrical permeability
            sigma: Conductivity [S/m]
            dx: Fracture discretization

        Returns:
            Output array of shape (x, 9): [x, y, z,
                                           dip,
                                           strike,
                                           area,
                                           thickness,
                                           electrical permeability,
                                           electrical conductivity]

        """
        Lb = (Nc[0]) / 2
        Wb = (Nc[1]) / 2
        A = dx**2
        X = np.array([-Lb, Lb, dx])
        Y = np.array([-Wb, Wb, dx])

        d_loc = self.vc.align_plane_with_dip_and_strike(X, Y, dip, strike)
        
        d_loc[:,0] = d_loc[:,0] + r
        output_array = np.zeros((d_loc.shape[0], 9), dtype=np.float)
        output_array[:, 0] = d_loc[:, 0]
        output_array[:, 1] = d_loc[:, 1]
        output_array[:, 2] = d_loc[:, 2]
        output_array[:, 3] = dip
        output_array[:, 4] = strike
        output_array[:, 5] = A
        output_array[:, 6] = thck
        output_array[:, 7] = perm
        output_array[:, 8] = sigma
        return output_array

    def triangular_regular_grid(self, r, Nc, dip, strike, thck, perm, sigma, dx):
        """
        Construct triangular grid representing a finite fault
        Args:
            r: radius distance from source. How far away the fracture is from the origin
            Nc: Fracture size
            dip: angle
            strike: angle
            thck: aperture fault [m]
            perm: relative elecrical permeability
            sigma: Conductivity [S/m]
            dx: Fracture discretization

        Returns:
            Output array of shape (x, 9): [x, y, z,
                                           dip,
                                           strike,
                                           area,
                                           thickness,
                                           electrical permeability,
                                           electrical conductivity]

        """
        Lb = (Nc[0] ) / 2
        Wb = (Nc[1] ) / 2

        X = np.array([-Lb, Lb, dx])
        Y = np.array([-Wb, Wb, dx])
        d_loc = self.vc.align_plane_with_dip_and_strike(X, Y, dip, strike)

        # Triangulation of points
        self.vertices, self.faces, self.normals = self.point_cloud_2d_triangulation(d_loc)

        elements = len(self.faces)

        self.vertices[:,0] =  self.vertices[:,0] + r # Separation from source

        output_array = np.empty((elements, 9), dtype=np.float)
        arr = np.asarray([self.calc_area_and_direction(triangles) for triangles in self.vertices[self.faces]])
        output_array[:, :6] = arr
        output_array[:, 6] = thck
        output_array[:, 7] = perm
        output_array[:, 8] = sigma
        self.fracture_properties = output_array
        return output_array

    def triangular_irregular_grid(self, r, Nc, dip, strike, thck, perm, sigma, dx):
        """
        TODO: Not working
        Construct an irregular triangular grid representing a finite fault
        Args:
            r: radius distance from source. How far away the fracture is from the origin
            Nc: Fracture size
            dip: angle
            strike: angle
            thck: aperture fault [m]
            perm: relative elecrical permeability
            sigma: Conductivity [S/m]
            dx: Fracture discretization

        Returns:
            Output array of shape (x, 9): [x, y, z,
                                           dip,
                                           strike,
                                           area,
                                           thickness,
                                           electrical permeability,
                                           electrical conductivity]

        """
        Lb = (Nc[0]) / 2
        Wb = (Nc[1]) / 2

        X = np.array([-Lb, Lb, dx])
        Y = np.array([-Wb, Wb, dx])
        d_loc = self.vc.align_plane_with_dip_and_strike(X, Y, dip, strike)

        # Triangulation of points
        self.vertices, self.faces, self.normals = self.point_cloud_2d_triangulation(d_loc, decimation=True,
                                                                                    target_reduction=0.7)

        elements = len(self.faces)

        self.vertices[:,0] =  self.vertices[:,0] + r # Separation from source

        output_array = np.empty((elements, 9), dtype=np.float)
        arr = np.asarray([self.calc_area_and_direction(triangles) for triangles in self.vertices[self.faces]])
        output_array[:, :6] = arr
        output_array[:, 6] = thck
        output_array[:, 7] = perm
        output_array[:, 8] = sigma
        self.fracture_properties = output_array
        return output_array

    @ staticmethod
    def calc_area_and_direction(points):
        """
        Calculate the area of the triangle given 3 points in space.
        A = 1/2 (||AB x AC ||)
        Args:
            points: [[x1,y1,z1],[x2,y2,z2],[x3,y3,z3]]
        Returns:
            area: int
        """
        pA = points[0]
        pB = points[1]
        pC = points[2]
        AB = pA - pB
        AC = pA - pC
        normal = np.cross(AB, AC)  # Normal to the plane

        norm = np.linalg.norm(normal)
        area = norm / 2  # Area triangle

        # if negative we need to make it positive giving you a unit normal with positive z component.
        if normal[-1] < 0:
            normal *= -1
        normal = normal / norm  # Get unitary normal
        # The unit normal up vector to the earth's surface is [0 0 1].
        # The angle between the plane and the fault plane is the same as the angle between their normals
        dip = np.degrees(np.arccos(np.dot(normal, [1, 0, 0])))
        azimuth = np.degrees(np.arctan2(normal[0], normal[1]))   # To get the azimuth
        # project normal to plane x, y
        x, y, z = (pA+pB+pC)/3

        return x, y, z, azimuth, dip, area

    @ staticmethod
    def point_cloud_2d_triangulation(points, decimation = False, target_reduction = 0.7 ):
        """
        Triangulate a 3D point cloud by a surface
        Args:
            points: [x, y, z]
            decimation: reduce the amount of elements
            target_reduction: percentage of the mesh to reduce
        Returns:
            conectivity array

        """
        #from scipy.spatial import Delaunay
        #tri = Delaunay(d_loc[:, :2])
        import pyvista as pv
        cloud = pv.PolyData(points)
        surf = cloud.delaunay_2d()
        if decimation:
            surf = surf.decimate(target_reduction)
        vertices = np.asarray(surf.points)
        # VTK convention [cell0_nverts, cell0_v0, cell0_v1, cell0_v2, cell1_nverts, ...]
        faces = np.asarray(surf.faces).reshape((-1, 4))[:, 1:]
        normals = np.asarray(surf.point_normals)

        return vertices, faces, normals

    def translate_grid(self, dipole_grid, translation):
        dipole_grid[:,0:3] = dipole_grid[:,0:3] + np.tile(translation.T,(dipole_grid.shape[0],1))
        return dipole_grid
    
    def filter_dipole_grid(self, dipole_grid, Tx, Rx, minimum_radius):
        xyz = dipole_grid[:,0:3]
        SR = np.concatenate((Tx,Rx),axis=0)
        SR_tensor = np.tile(SR,(xyz.shape[0],1,1)).transpose(0,1,2)
        xyz_tensor = np.tile(xyz,(SR.shape[0],1,1)).transpose(1,0,2)
        distances = np.sqrt(np.sum((SR_tensor - xyz_tensor)**2, axis=2))
        index = np.logical_not(np.sum(distances < minimum_radius, axis=1) > 0)
        dipole_grid[~index,6] = 0
        print('A total of ' + str(np.sum(~index)) + ' dipoles have been forced to zero aperture')
        return dipole_grid
    
    def generate_heterogeneous_apertures(self, Nc, dx, roughness, b_mean, correlation_length, H):
        X = np.random.randn(Nc[0],Nc[1])
        top_surface = self.generate_crf(X, dx, roughness, correlation_length, H)
        X = np.random.randn(Nc[0],Nc[1])
        bottom_surface = self.generate_crf(X, dx, roughness, correlation_length, H)
        apertures = top_surface + b_mean - bottom_surface
        apertures[apertures<0] = 0
        return apertures
    
    def generate_crf(self, X, dx, sh, cl, H):
        Nc = X.shape
        pNc = np.prod(Nc)
        spNc = np.sqrt(pNc)
        Lx = Nc[0]*dx
        Ly = Nc[1]*dx
        # Covariance matrix
        [Xn, Yn] = np.meshgrid(np.abs(np.linspace(-Lx/2,Lx/2,Nc[0])),np.abs(np.linspace(-Ly/2,Ly/2,Nc[1])))
        Lag = np.sqrt(Xn**2 + Yn**2)
        CM = np.broadcast_to((sh**2)*np.exp(-(Lag/cl)**(2*H)),(Nc[1],Nc[0],Nc[1],Nc[0]))
                
        vNc0 = np.broadcast_to(np.arange(Nc[0]),(Nc[1],Nc[0],Nc[1],Nc[0]))
        vNc1 = np.broadcast_to(np.arange(Nc[1]),(Nc[1],Nc[0],Nc[0],Nc[1])).transpose(0,1,3,2)
        
        sv = vNc0.transpose(2,3,0,1)
        tv = vNc1.transpose(2,3,0,1)
        
        Xh = np.sum(np.sum(X.T*np.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)
        S = np.sum(np.sum(CM*np.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)/pNc
        # Correlated field
        SqS = np.sqrt(S)
        SqS[np.isnan(SqS)] = 0
        CRF = np.sum(np.sum(SqS*Xh*np.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)/pNc
    
        return np.real(CRF)*spNc
    
    
    
    
    

class dask_fracture_properties:
    
    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        self.vc = fracture_math.dask_vector_calculus()

    def dipole_grid(self, r, Nc, dip, strike, thck, perm, sigma, dx):
        Lb = (Nc[0]*dx)/2
        Wb = (Nc[1]*dx)/2
        A = dx**2
        X = np.array([-Lb, Lb, Nc[0]])
        Y = np.array([-Wb, Wb, Nc[1]])
        d_loc = self.vc.align_plane_with_dip_and_strike(X, Y, dip, strike).compute()

        d_loc[:,0] = d_loc[:,0] + r
        output_array = np.zeros((d_loc.shape[0],9),dtype=np.float)
        output_array[:,0] = d_loc[:,0]
        output_array[:,1] = d_loc[:,1]
        output_array[:,2] = d_loc[:,2]
        output_array[:,3] = dip
        output_array[:,4] = strike
        output_array[:,5] = A
        output_array[:,6] = thck
        output_array[:,7] = perm
        output_array[:,8] = sigma
        return output_array
        
    
    def filter_dipole_grid(self, dipole_grid, Tx, Rx, minimum_radius):
        xyz = dipole_grid[:,0:3]
        SR = da.concatenate((Tx,Rx),axis=0)
        SR_tensor = da.tile(SR,(xyz.shape[0],1,1)).transpose(0,1,2)
        xyz_tensor = da.tile(xyz,(SR.shape[0],1,1)).transpose(1,0,2)
        distances = da.sqrt(da.sum((SR_tensor - xyz_tensor)**2, axis=2))
        index = da.logical_not(da.sum(distances < minimum_radius, axis=1) > 0)
        return dipole_grid[index,:]
    
    def generate_heterogeneous_apertures(self, Nc, dx, roughness, b_mean, correlation_length, H):
        X = np.random.randn(Nc[0],Nc[1])
        top_surface = self.generate_crf(X, dx, roughness, correlation_length, H)
        X = np.random.randn(Nc[0],Nc[1])
        bottom_surface = self.generate_crf(X, dx, roughness, correlation_length, H)
        apertures = top_surface + b_mean - bottom_surface
        apertures[apertures<0] = 0
        return apertures
    
    def generate_crf(self, X, dx, sh, cl, H):
        Nc = X.shape
        pNc = da.prod(np.array([Nc[0],Nc[1]]))
        spNc = da.sqrt(pNc)
        Lx = Nc[0]*dx
        Ly = Nc[1]*dx
        # Covariance matrix
        [Xn, Yn] = da.meshgrid(da.absolute(da.linspace(-Lx/2,Lx/2,Nc[0])),da.absolute(da.linspace(-Ly/2,Ly/2,Nc[1])))
        Lag = da.sqrt(Xn**2 + Yn**2)
        CM = da.broadcast_to((sh**2)*da.exp(-(Lag/cl)**(2*H)),(Nc[1],Nc[0],Nc[1],Nc[0]))
                
        vNc0 = da.broadcast_to(da.arange(Nc[0]),(Nc[1],Nc[0],Nc[1],Nc[0]))
        vNc1 = da.broadcast_to(da.arange(Nc[1]),(Nc[1],Nc[0],Nc[0],Nc[1])).transpose(0,1,3,2)
        
        sv = vNc0.transpose(2,3,0,1)
        tv = vNc1.transpose(2,3,0,1)
        
        Xh = da.sum(da.sum(X.T*da.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)
        S = da.sum(da.sum(CM*da.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)/pNc
        # Correlated field
        SqS = da.sqrt(S)
        SqS[da.isnan(SqS)] = 0
        CRF = da.sum(da.sum(SqS*Xh*da.exp(2j*np.pi*(vNc0*sv+vNc1*tv)/spNc), axis=2), axis=2)/pNc
    
        return da.real(CRF)*spNc
    