from fracwave import Antenna, SourceEM, FracEM

class SourceInversion():
    def __init__(self, ant: Antenna, sou: SourceEM, solv: FracEM):
        """

        Args:
            ant:
            sou:
            solv:
        """
        self.ant = ant
        self.sou = sou
        self.solv = solv

    def invert_source(self):
        """

        Returns:

        """
        self.solv.mode = 'propagation'
        from fracwave.solvers.fracEM.tensor_element_solver_gnloop import forward_solver
        dat = forward_solver(self.solv)