"""
Created on Thu Nov  5 15:49:17 2020

@author: Alexis Shakas, shakasa@lilo.org
"""
import numpy as np
try:
    import dask.array as da
except:
    print('Dask not installed')
    

class vector_calculus:

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'

    @ staticmethod
    def rotation_along_Z(angles):
        """
        Rotation matrix along z axis
        Args:
            angles: Angle in degrees to rotate
        Returns:
            R: rotation matrix

        """
        angle = np.deg2rad(angles)
        zeros = np.zeros(angles.shape[0])
        ones = np.ones(angles.shape[0])
        R = np.array([[np.cos(angle),   np.sin(angle),  zeros],
                      [-np.sin(angle),    np.cos(angle),  zeros],
                      [zeros,                 zeros,       ones]])
        return R.transpose(2, 0, 1)

    @ staticmethod
    def arbitrary_rotation(angle, u):
        """
        3D rotation matrix for a rotation around a unit vector (ux, uy, uz) passing through the origin
        Args:
            angle: rotation angle
            u: unit vector

        Returns:
            R: 3D rotation matrix

        """
        theta = np.deg2rad(angle)
        R = np.array([[np.cos(theta) + u[0]**2 * (1-np.cos(theta)),
                       u[0] * u[1] * (1-np.cos(theta)) - u[2] * np.sin(theta),
                       u[0] * u[2] * (1 - np.cos(theta)) + u[1] * np.sin(theta)],
                      [u[0] * u[1] * (1-np.cos(theta)) + u[2] * np.sin(theta),
                       np.cos(theta) + u[1]**2 * (1-np.cos(theta)),
                       u[1] * u[2] * (1 - np.cos(theta)) - u[0] * np.sin(theta)],
                      [u[0] * u[2] * (1-np.cos(theta)) - u[1] * np.sin(theta),
                       u[1] * u[2] * (1-np.cos(theta)) + u[0] * np.sin(theta),
                       np.cos(theta) + u[2]**2 * (1-np.cos(theta))]])
        return R

    def get_orientation_of_plane(self, strikes, dips):
        """
        Get the normal vector to the plane given by the strikes and dips
        Args:
            strikes: angles
            dips: angles
        Returns:
            Normals vector:
            t1_vector: vector [1,0,0] onto plane
            t2_vector: vector [0,1,0] onto plane

        """
        strike_rotation = self.arbitrary_rotation(strikes, [0, 0, 1])
        dip_rotation = self.arbitrary_rotation(dips, [0, 1, 0])
        rotation_matrix = np.matmul(strike_rotation, dip_rotation)
        t1_vector = np.array([1, 0, 0])
        t2_vector = np.array([0, 1, 0])
        normal_vector = np.array([0, 0, 1])
        normal_vector = np.dot(rotation_matrix, normal_vector)
        t1_vector = np.dot(rotation_matrix, t1_vector)
        t2_vector = np.dot(rotation_matrix, t2_vector)
        return normal_vector, t1_vector, t2_vector

    def align_plane_with_dip_and_strike(self,Nx, Ny, dip, strike):
        """
        From the array from both directions (Nx, Ny) create a grid and rotate
        the points according to the strike and dip.
        Args:
            Nx: [Min x value, Max x value, Spacing in x]
            Ny: [Min y value, Max y value, Spacing in y]
            dip: angle
            strike: angle

        Returns:
            [x, y, z] points of the rotated plane

        """
        # Create a vertical plane and the rotate it
        [xx, yy] = np.meshgrid(np.arange(Nx[0], Nx[1]+0.000001, Nx[2]),
                               np.arange(Ny[0], Ny[1]+0.000001, Ny[2]))
        zz = np.zeros(xx.shape)
        xyz = np.array([xx.ravel(), yy.ravel(), zz.ravel()])

        rot_dip = self.arbitrary_rotation(np.array([dip]), [0, 1, 0]).squeeze()
        rot_strike = self.arbitrary_rotation(np.array([strike]), [0, 0, 1]).squeeze()

        return np.matmul(rot_strike, np.matmul(rot_dip, xyz)).T


    def orthogonal_projection(self, vector_in, projection_vector):
        # divisor = np.tile(np.power(np.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).transpose(1,2,0)
        projection_tensor = np.tile(projection_vector,(vector_in.shape[1],1)).T
        divisor = np.tile(np.sum(projection_tensor**2, axis=0),(3,1))
        dot_product = np.tile(np.sum(projection_tensor*vector_in, axis=0),(3,1))
        return dot_product*projection_tensor/divisor

    def get_vector_components_on_plane(self, vector_in, strikes, dips):
        normal_vec, t1, t2 = self.get_orientation_of_plane(strikes, dips)
        normal_component = self.orthogonal_projection(vector_in,normal_vec)
        t1_component = self.orthogonal_projection(vector_in,t1)
        t2_component = self.orthogonal_projection(vector_in,t2)
        return normal_component, t1_component, t2_component
    





class vector_calculus_on_tensors: 
    
    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        
        
    def rotation_along_Z(self, angles):
        angle = np.deg2rad(angles)
        zeros = np.zeros(angles.shape[0])
        ones = np.ones(angles.shape[0])
        R = np.array([[np.cos(angle),   np.sin(angle),  zeros],
                    [-np.sin(angle),    np.cos(angle),  zeros],
                    [zeros,                 zeros,       ones]])
        return R.transpose(2,0,1)
    
    def arbitrary_rotation(self, angle, u):
        theta = np.deg2rad(angle)
        R = np.array([[np.cos(theta) + u[0]**2 * (1-np.cos(theta)), 
                      u[0] * u[1] * (1-np.cos(theta)) - u[2] * np.sin(theta), 
                      u[0] * u[2] * (1 - np.cos(theta)) + u[1] * np.sin(theta)],
                    [u[0] * u[1] * (1-np.cos(theta)) + u[2] * np.sin(theta),
                      np.cos(theta) + u[1]**2 * (1-np.cos(theta)),
                      u[1] * u[2] * (1 - np.cos(theta)) - u[0] * np.sin(theta)],
                    [u[0] * u[2] * (1-np.cos(theta)) - u[1] * np.sin(theta),
                      u[1] * u[2] * (1-np.cos(theta)) + u[0] * np.sin(theta),
                      np.cos(theta) + u[2]**2 * (1-np.cos(theta))]])
        return R
    
    def orthogonal_projection(self, input_tensor, projection_tensor):
        # divisor = np.tile(np.power(np.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).transpose(1,2,0)
        divisor = np.tile(np.sum(projection_tensor**2,axis=2),(3,1,1)).transpose(1,2,0)
        dot_product = np.tile(np.sum(projection_tensor*input_tensor, axis=2),(3,1,1)).transpose(1,2,0)
        return dot_product*projection_tensor/divisor
    

    def get_orientation_of_plane(self, strikes, dips):
        strike_rotation = self.arbitrary_rotation(strikes, [0,0,1]).transpose(2,0,1)
        dip_rotation = self.arbitrary_rotation(dips, [0,1,0]).transpose(2,0,1)
        rotation_matrix = np.matmul(strike_rotation,dip_rotation)
        t1_vector = np.broadcast_to(np.array([[1,0,0]]),(dips.shape[0],3,3))
        t2_vector = np.broadcast_to(np.array([[0,1,0]]),(dips.shape[0],3,3))
        normal_vector = np.broadcast_to(np.array([[0,0,1]]),(dips.shape[0],3,3))
        normal_vector = np.sum(rotation_matrix*normal_vector, axis=2)
        t1_vector = np.sum(rotation_matrix*t1_vector, axis=2)
        t2_vector = np.sum(rotation_matrix*t2_vector, axis=2)
        return normal_vector, t1_vector, t2_vector
    
    def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        _, n_frequencies, n_dipoles = tensor_in.shape
        tensor_in = tensor_in.transpose(2,1,0)
        normal_vec, t1, t2 = self.get_orientation_of_plane(strikes, dips)
        normal_tensor = np.tile(normal_vec, (n_frequencies,1,1)).transpose(1,0,2)
        t1_tensor = np.tile(t1, (n_frequencies,1,1)).transpose(1,0,2)
        t2_tensor = np.tile(t2, (n_frequencies,1,1)).transpose(1,0,2)
        normal_component = self.orthogonal_projection(tensor_in,normal_tensor)
        t1_component = self.orthogonal_projection(tensor_in,t1_tensor)
        t2_component = self.orthogonal_projection(tensor_in,t2_tensor)
        return normal_component, t1_component, t2_component
    
    def align_plane_with_dip_and_strike(self,Nx, Ny,dip, strike):
        [xx,yy] = np.meshgrid(np.linspace(Nx[0],Nx[1],int(Nx[2])),np.linspace(Ny[0],Ny[1],int(Ny[2])))
        zz = np.zeros(xx.shape)
        xyz = np.array([xx.ravel(),yy.ravel(),zz.ravel()])
        rot_dip = self.arbitrary_rotation(np.array([dip]), [0,1,0]).squeeze()
        rot_strike = self.arbitrary_rotation(np.array([strike]), [0,0,1]).squeeze()
        return np.matmul(rot_strike, np.matmul(rot_dip, xyz)).T



class dask_vector_calculus:

    def __init__(self):
        self.main_dir = '/forward-modeling/'
        self.save_dir = '/output/'
        
        
    def rotation_along_Z(self, angles):
        angle = da.deg2rad(angles)
        zeros = da.zeros(angles.shape[0])
        ones = da.ones(angles.shape[0])
        R = da.array([[da.cos(angle),   da.sin(angle),  zeros],
                    [-da.sin(angle),    da.cos(angle),  zeros],
                    [zeros,                 zeros,       ones]])
        return R.transpose(2,0,1)
    
    def arbitrary_rotation(self, angle, u):
        theta = da.deg2rad(angle)
        R = da.array([[da.cos(theta) + u[0]**2 * (1-da.cos(theta)), 
                      u[0] * u[1] * (1-da.cos(theta)) - u[2] * da.sin(theta), 
                      u[0] * u[2] * (1 - da.cos(theta)) + u[1] * da.sin(theta)],
                    [u[0] * u[1] * (1-da.cos(theta)) + u[2] * da.sin(theta),
                      da.cos(theta) + u[1]**2 * (1-da.cos(theta)),
                      u[1] * u[2] * (1 - da.cos(theta)) - u[0] * da.sin(theta)],
                    [u[0] * u[2] * (1-da.cos(theta)) - u[1] * da.sin(theta),
                      u[1] * u[2] * (1-da.cos(theta)) + u[0] * da.sin(theta),
                      da.cos(theta) + u[2]**2 * (1-da.cos(theta))]])
        return R
    
    def orthogonal_projection(self, input_tensor, projection_tensor):
        # divisor = np.tile(np.power(np.linalg.norm(projection_tensor,axis=2),2),(3,1,1)).transpose(1,2,0)
        divisor = da.tile(da.sum(projection_tensor**2,axis=2),(3,1,1)).transpose(1,2,0)
        dot_product = da.tile(da.sum(projection_tensor*input_tensor, axis=2),(3,1,1)).transpose(1,2,0)
        return dot_product*projection_tensor/divisor
    

    def get_orientation_of_plane(self, strikes, dips):
        strike_rotation = self.arbitrary_rotation(strikes, [0,0,1]).transpose(2,0,1)
        dip_rotation = self.arbitrary_rotation(dips, [0,1,0]).transpose(2,0,1)
        rotation_matrix = da.matmul(strike_rotation,dip_rotation)
        t1_vector = da.broadcast_to(da.array([[1,0,0]]),(dips.shape[0],3,3))
        t2_vector = da.broadcast_to(da.array([[0,1,0]]),(dips.shape[0],3,3))
        normal_vector = da.broadcast_to(da.array([[0,0,1]]),(dips.shape[0],3,3))
        normal_vector = da.sum(rotation_matrix*normal_vector, axis=2)
        t1_vector = da.sum(rotation_matrix*t1_vector, axis=2)
        t2_vector = da.sum(rotation_matrix*t2_vector, axis=2)
        return normal_vector, t1_vector, t2_vector
    
    def get_vector_components_on_plane(self, tensor_in, strikes, dips):
        _, n_frequencies, n_dipoles = tensor_in.shape
        tensor_in = tensor_in.transpose(2,1,0)
        normal_vec, t1, t2 = self.get_orientation_of_plane(strikes, dips)
        normal_tensor = da.tile(normal_vec, (n_frequencies,1,1)).transpose(1,0,2)
        t1_tensor = da.tile(t1, (n_frequencies,1,1)).transpose(1,0,2)
        t2_tensor = da.tile(t2, (n_frequencies,1,1)).transpose(1,0,2)
        normal_component = self.orthogonal_projection(tensor_in,normal_tensor)
        t1_component = self.orthogonal_projection(tensor_in,t1_tensor)
        t2_component = self.orthogonal_projection(tensor_in,t2_tensor)
        return normal_component, t1_component, t2_component
    
    def align_plane_with_dip_and_strike(self,Nx, Ny,dip, strike):
        [xx,yy] = da.meshgrid(da.linspace(Nx[0],Nx[1],int(Nx[2])),da.linspace(Ny[0],Ny[1],int(Ny[2])))
        zz = da.zeros(xx.shape)
        xyz = da.array([xx.ravel(),yy.ravel(),zz.ravel()])
        rot_dip = self.arbitrary_rotation(da.array([dip]), [0,1,0]).squeeze()
        rot_strike = self.arbitrary_rotation(da.array([strike]), [0,0,1]).squeeze()
        return da.matmul(rot_strike, da.matmul(rot_dip, xyz)).T
    
    
    
    
    
    
    
    
    
    
    
# class vector_computations():
    
#     def __init__(self):
#         self.ID = None
    
#     def rotation_along_Z(self, angles):
#         angle = np.deg2rad(angles)
#         zeros = np.zeros(angles.shape[0])
#         ones = np.ones(angles.shape[0])
#         R = np.array([[np.cos(angle),   np.sin(angle),  zeros],
#                     [-np.sin(angle),    np.cos(angle),  zeros],
#                     [zeros,                 zeros,       ones]])
#         return R.transpose(2,0,1)

#     def arbitrary_rotation(self, angle, u):
#         theta = np.deg2rad(angle)
#         R = np.array([[np.cos(theta) + u[:,0]**2 * (1-np.cos(theta)), 
#                       u[:,0] * u[:,1] * (1-np.cos(theta)) - u[:,2] * np.sin(theta), 
#                       u[:,0] * u[:,2] * (1 - np.cos(theta)) + u[:,1] * np.sin(theta)],
#                     [u[:,0] * u[:,1] * (1-np.cos(theta)) + u[:,2] * np.sin(theta),
#                       np.cos(theta) + u[:,1]**2 * (1-np.cos(theta)),
#                       u[:,1] * u[:,2] * (1 - np.cos(theta)) - u[:,0] * np.sin(theta)],
#                     [u[:,0] * u[:,2] * (1-np.cos(theta)) - u[:,1] * np.sin(theta),
#                       u[:,1] * u[:,2] * (1-np.cos(theta)) + u[:,0] * np.sin(theta),
#                       np.cos(theta) + u[:,2]**2 * (1-np.cos(theta))]])
#         return R.transpose(2,0,1)


#     def orthogonal_projection(self, input_tensor, projection_tensor):
#         divisor = np.tile(np.power(np.linalg.norm(projection_tensor,axis=1),2),(3,1,1)).transpose(1,0,2)
#         dot_product = np.tile(np.sum(projection_tensor*input_tensor, axis=1),(3,1,1)).transpose(1,0,2)
#         return dot_product*projection_tensor/divisor
    
#     def normalize_components(self,input_tensor):
#         input_tensor[np.isnan(input_tensor)] = 0
#         return input_tensor/np.tile(np.linalg.norm(input_tensor,axis=1),(3,1,1)).transpose(1,0,2)

#     def get_normal_vector_of_plane(self, strikes, dips):
#         strike_rotation = self.rotation_along_Z(strikes)
#         dip_dir = strikes + 90
#         dip_dir[dip_dir > 360] = dip_dir[dip_dir > 360] - 360
#         dip_dir_rotation = self.rotation_along_Z(dip_dir)
#         vec1 = np.matmul(strike_rotation,[0,1,0])
#         vec_dip_dir = np.tile(np.matmul(dip_dir_rotation,[0,1,0]),(3,1,1)).transpose(1,2,0) # transpose right ?
#         dip_rotation = self.arbitrary_rotation(dips, vec1)
#         vec2 = np.sum(dip_rotation*vec_dip_dir,axis=2)
#         normal_vector = np.cross(vec2, vec1, axis=1)
#         return normal_vector, vec1, vec2
    

#     def get_vector_components_on_plane(self, tensor_in, strikes, dips):
#         n_freq = tensor_in.shape[1]
#         normal_vec, t1, t2 = self.get_normal_vector_of_plane(strikes, dips)
#         normal_tensor = np.tile(normal_vec, (n_freq,1,1)).transpose(1,0)
#         t1_tensor = np.tile(t1, (n_freq,1,1)).transpose(1,2,0)
#         t2_tensor = np.tile(t2, (n_freq,1,1)).transpose(1,2,0)
#         normal_component = self.orthogonal_projection(tensor_in,normal_tensor)
#         t1_component = self.orthogonal_projection(tensor_in,t1_tensor)
#         t2_component = self.orthogonal_projection(tensor_in,t2_tensor)
#         return self.normalize_components(normal_component), self.normalize_components(t1_component), self.normalize_components(t2_component)




    
    
    
    
    
    