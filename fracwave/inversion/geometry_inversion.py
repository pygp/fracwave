import numpy as np
import shutil
import os
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from fracwave import FractureGeo, FracEM, set_logger, Antenna
import pandas as pd
logger = set_logger(__name__)
# frac = FractureGeo()
frac = None
# solv = FracEM()
solv = None
c=0

from typing import List
from nptyping import NDArray, Shape, Float
from nptyping import DataFrame, Structure as S

class GeometryInversion:
    """This class manages the inversion based on travel times"""
    def __init__(self,
                 frac: FractureGeo,
                 extent: List[float],
                 velocity: float,
                 # Tx: NDArray[Shape["* points, 3"], Float],
                 # Rx: NDArray[Shape["* points, 3"], Float],
                 antenna: Antenna,
                 # measured_travel_time: NDArray[Shape["* points"], Float]
                 measured_travel_time: dict
                 ):
        """

        Args:
            frac: Instance of fracture geometry
            extent: [xmin, xmax, ymin, ymax, zmin, zmax]
            velocity: (m/ns)
            Tx: Transmitter xyz positions
            Rx: Receiver xyz positions
            measured_travel_time: Real data
        """
        self.extent = extent
        # self.frac = FractureGeo()
        # self.frac.init_geo_model(extent, gempy_resolution)
        self.frac = frac
        self.velocity = velocity

        self.fixed_control_points = None
        self.temp_control_points = None
        self.fixed_orientations = None

        self.ant = antenna
        self.map_antenna = dict()

        self.measured_time_dict = measured_travel_time
        transmitter = []
        receiver = []
        measured_time = []
        self.boreholes = self.ant.name_profiles
        for bn in self.boreholes:
            self.map_antenna[bn] = np.asarray(self.ant.profiles.loc[self.ant.profiles['profile'] == bn].index.values)
            transmitter.append(self.ant.profiles.loc[self.ant.profiles['profile'] == bn, ('Tx', 'Ty', 'Tz')].to_numpy())
            receiver.append(self.ant.profiles.loc[self.ant.profiles['profile'] == bn, ('Rx', 'Ry', 'Rz')].to_numpy())
            measured_time.append(measured_travel_time[bn]) if measured_travel_time is not None else None

        self.Tx = np.vstack(transmitter)
        self.Rx = np.vstack(receiver)
        self.measured_travel_time = np.hstack(measured_time) if measured_travel_time is not None else None

        self.midpoints = None
        self.vertices = None
        self.faces = None
        self.grid = None


        self.c = 0

        self.kwargs_remesh = dict()
        self.max_distance_from_borehole = None
        self.max_distance_from_surface_minimum_distance = None

        self.tol_pos = 3  # Time difference (ns)
        self.tol_all = 5  # Time difference (ns)
        self.max_iter_pos = 10  # Maximum iteration when inverting for single position
        self.iter_pos = 0  # Maximum iteration when inverting for single position
        self.max_iter_all = 20  # Maximum iteration when inverting for single position
        self.iter_all = 0  # Maximum iteration when inverting for single position
        self.surrounding = 3  # Ignore the values surrounding the position so the position is not that close between each other

        self.error_pos = None
        self.error_all = None

        self.time_current = None
        self.time_initial = None

        self.currrent_pos = None
        self.ymin = None
        self.ymax = None
        # self.already_optimized_points = []
        self.already_optimized_points = pd.DataFrame()

        self.directory = ''
        self.filenames_pics = []

        self.show_plot = True

        self.rejected_points = pd.DataFrame()

        self.error_history = []
        self.std_threshold = 0.3
        self.std_average = 3

        self.error_all_history = []
        self.points_all_history = []
        self._accepted_position = False
        self.resting_period = 3  # The amount of iterations to pass to be able to consider the point again
        self.ylim = 0, 1200


    def clear(self):
        self.fixed_control_points = None
        self.temp_control_points = None
        self.fixed_orientations = None

        self.midpoints = None
        self.vertices = None
        self.faces = None
        self.grid = None

        self.c = 0

        self.kwargs_remesh = dict()
        self.max_distance_from_borehole = None

        self.tol_pos = 3  # Time difference (ns)
        self.tol_all = 5  # Time difference (ns)
        self.max_iter_pos = 10  # Maximum iteration when inverting for single position
        self.iter_pos = 0  # Maximum iteration when inverting for single position
        self.max_iter_all = 20  # Maximum iteration when inverting for single position
        self.iter_all = 0  # Maximum iteration when inverting for single position
        self.surrounding = 3  # Ignore the values surrounding the position so the position is not that close between each other

        self.error_pos = None
        self.error_all = None

        self.time_current = None
        self.time_initial = None

        self.currrent_pos = None
        self.ymin = None
        self.ymax = None
        # self.already_optimized_points = []
        self.already_optimized_points = pd.DataFrame()

        self.directory = ''
        self.filenames_pics = []
        self.rejected_points = pd.DataFrame()
        self.error_history = []
        self.std_threshold = 0.3
        self.std_average= 3

    def set_initial_points(self,
                           fixed_points: DataFrame[S["X: Float, Y: Float, Z: Float"]],
                           fixed_orientations: DataFrame[S["X: Float, Y: Float, Z: Float, "
                                                           "G_x: Float, G_y: Float, G_z: Float"]],
                           ):
        """

        Args:
            fixed_points:
            fixed_orientations:

        Returns:

        """
        self.fixed_control_points = fixed_points
        self.fixed_orientations = fixed_orientations

    def forward_solver(self):
        """

        Returns:

        """
        v, f = self.frac.create_gempy_geometry(fixed_points=self.fixed_control_points,
                                               fixed_orientations=self.fixed_orientations,
                                               move_points=self.temp_control_points)

        convert_to_grid = self.kwargs_remesh.pop('convert_to_grid', True)
        kwargs = self.kwargs_remesh.copy()
        if convert_to_grid:
            self.grid, self.vertices, self.faces = self.frac.remesh(v, **kwargs)
        else:
            self.grid, self.vertices, self.faces = self.frac.refine_mesh(v, f, **kwargs)

        if self.max_distance_from_borehole is not None or self.max_distance_from_surface_minimum_distance is not None:
            self.grid, self.vertices, self.faces =self.frac.mask_mesh(self.vertices,
                                                                      self.faces,
                                                                      positions=np.vstack((self.Rx, self.Tx)),
                                                                      from_maximum_distance_borehole=self.max_distance_from_borehole,
                                                                      from_maximum_distance_surface=self.max_distance_from_surface_minimum_distance)


        self.midpoints = self.frac.get_midpoints(self.vertices, self.faces)

        min_distance, min_position, surface = self.frac.get_position_reflection_point(self.midpoints,
                                                                             self.Tx,
                                                                             self.Rx)
        first_arrival = min_distance / self.velocity
        self.time_current = first_arrival
        return first_arrival, min_position, surface

    def inversion_position(self, pos: int):
        """

        Args:
            pos: Position

        Returns:

        """
        self._accepted_position = False
        self.currrent_pos = pos
        time_init, element_init, surface = self.forward_solver()
        tpos = time_init[pos]
        spos = surface[pos]
        bpos = (self.Tx[pos] + self.Rx[pos]) * 0.5  # Here we assume that the middle position between the antennas is the one to move
        mpos = element_init[pos]
        ttruepos = self.measured_travel_time[pos]
        t_diff = ttruepos - tpos
        self.iter_pos = 0
        self.error_pos = np.abs(t_diff)
        tp = self.tol_pos
        if isinstance(tp, (list, NDArray)):
            tp = tp[pos]
        time_new = time_init
        # this will allow to change the step direction.
        # Usefull when I want to move the geometry to the other direction because
        # we are getting farther away from the real solution
        _direction = 1
        new_point = mpos
        while self.error_pos > tp and self.iter_pos < self.max_iter_pos:
            vector = (mpos - bpos) / np.linalg.norm(mpos - bpos)
            if np.linalg.norm(mpos - bpos) < np.linalg.norm(new_point - bpos):
                _direction *= -1
            vector *= _direction # Try to the other direction to see if this works
            step = self.velocity * t_diff * 0.5  # Divide by 2 because the two-way-travel time.
            new_point = mpos + (vector * step)
            if new_point[0] < self.extent[0]: new_point[0] = self.extent[0]
            if new_point[1] < self.extent[2]: new_point[1] = self.extent[2]
            if new_point[2] < self.extent[4]: new_point[2] = self.extent[4]

            if new_point[0] > self.extent[1]: new_point[0] = self.extent[1]
            if new_point[1] > self.extent[3]: new_point[1] = self.extent[3]
            if new_point[2] > self.extent[5]: new_point[2] = self.extent[5]
            logger.debug(f'New point: {new_point}')
            self.points_all_history.append(new_point.tolist() + [pos])
            self.temp_control_points = pd.DataFrame(np.hstack([[new_point], [[spos]]]), columns=['X', 'Y', 'Z', 'surface'])
            time_new, element_new, surf = self.forward_solver()
            self.error_all = np.sqrt(np.square(self.measured_travel_time - time_new).mean())
            # Second criteria of convergence. See if the last values are not moving
            self.error_history.append(self.error_all)
            t_diff = ttruepos - time_new[pos]
            self.error_pos = np.abs(t_diff)
            if self.error_pos <= tp:
                self.temp_control_points['pos'] = pos
                self.fixed_control_points = pd.concat((self.fixed_control_points, self.temp_control_points))
                self.temp_control_points = None
                logger.info(f'Position {new_point} fixed\n')
                self._accepted_position = True
            else:
                mpos = element_new[pos]
                self._accepted_position = False
            self.iter_pos += 1
            self.c += 1
            if self.show_plot:
                self.plot_step()
        self.temp_control_points = None
        self.error_all_history.append((self.error_all, self.c))
        return time_new

    def run_inversion(self):
        """

        Returns:

        """
        self.ymin = self.measured_travel_time.min() - 5
        self.ymax = self.measured_travel_time.max() + 5
        time_init, element_init, surface_init = self.forward_solver()
        self.time_initial = time_init
        t_diff = np.abs(self.measured_travel_time - time_init)
        error = np.sqrt(np.square(t_diff).mean())
        self.iter_all = 0
        self.c = 0
        self.already_optimized_points = pd.DataFrame()
        self.error_all = error
        self.error_history.append(self.error_all)
        while error > self.tol_all and self.iter_all < self.max_iter_all:
            positions = np.arange(len(self.measured_travel_time))
            pos_sorted = positions[np.argsort(t_diff)]
            if len(self.already_optimized_points) > 0:

                self.already_optimized_points['counter'] -=1
                self.already_optimized_points = self.already_optimized_points.loc[self.already_optimized_points['counter']!=0]  # Take out from the exclusion list all those values that finished the resting period

                pos_sorted_after = np.asarray([i for i in pos_sorted if i not in self.already_optimized_points['positions'].to_numpy()])
            else:
                pos_sorted_after = pos_sorted
            if len(pos_sorted_after) == 0:
                logger.error('All positions already optimized. The optimization did not converge')
                return self.fixed_control_points
            pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error

            time_new = self.inversion_position(pos=pos)

            self._manage_position_optimized(pos=pos)

            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []
            if len(fix_points) > 0:
                e = np.abs(self.measured_travel_time[fix_points] - time_new[fix_points])
                mask = e > self.tol_pos[fix_points]
                mask_pos = np.argsort(e[mask])
                for p in fix_points[mask_pos]:
                    e = np.abs(self.measured_travel_time[p] - time_new[p])
                    tp = self.tol_pos
                    if isinstance(tp, (list, NDArray)):
                        tp = tp[pos]
                    if e > tp:  # This means that it moved a lot, and it needs to be fixed again.
                        logger.warning(
                            f'Rejected: Position {p} needs to be relocated. Point: {self.fixed_control_points[self.fixed_control_points["pos"] == p]}')
                        self.rejected_points = pd.concat((self.rejected_points, self.fixed_control_points[self.fixed_control_points['pos'] == p]))
                        self.fixed_control_points = self.fixed_control_points[self.fixed_control_points['pos'] != p]
                        time_new = self.inversion_position(pos=p)
                        self._manage_position_optimized(pos=p)

            t_diff = np.abs(self.measured_travel_time - time_new)
            error = np.sqrt(np.square(t_diff).mean())
            self.iter_all += 1
            self.error_all = error
            if self.show_plot:
                self.plot_step()
            # Now we are going to check if the second criteria of convergence applies.
            # Look at all the RMS and look if the value have reached a plateu
            if len(self.error_history) > 10:  # More than 10 data values to start looking if this have converged
                # (self.max_iter_pos*3) means that it will look at the values from the previous 3 control points.
                window_size = self.max_iter_pos
                moving_avg = np.convolve(self.error_history, np.ones(window_size) / window_size, mode='valid')
                plateu = np.std(moving_avg[-self.std_average:])  # Take the last 3 averages and see if we are in a plateu
                # y = np.asarray(self.error_history[-(self.max_iter_pos*2):])  # Look at the slope and check if its horizontal
                # x = np.arange(len(y))
                # slope, _ = np.polyfit(x, y, 1)
                if plateu <= self.std_threshold:
                    logger.info('Second convergence criteria achieved. Model have reached to a local minima')
                    break
        return self.fixed_control_points

    def _plot_step(self):
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.plot(self.time_current, 'blue', label='Simulated')
        ax.plot(self.measured_travel_time, 'g--', label='Real', alpha=0.3)
        ax.plot(self.time_initial, 'r--', label='Starting', alpha=0.3)
        ax.vlines(self.currrent_pos, color='black', ymin=self.ymin, ymax=self.ymax, label='Current position')

        if len(self.already_optimized_points) > 0:
            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []

            ax.vlines(fix_points, color='blue', linestyles='--', ymin=self.ymin, ymax=self.ymax, alpha=0.3,
                          label='Fixed points')
        ax.set_xlabel('Trace no.')
        ax.set_ylabel('Travel Time')
        ax.set_ylim(self.ymin, self.ymax)
        ax.grid()
        handles, labels = ax.get_legend_handles_labels()
        fig.legend(handles, labels)
        ax.set_title(
            f'Position/Iter: {self.currrent_pos}/{self.iter_pos}, Total iter:{self.c}\nRMS position: {self.error_pos:.2f}, RMS all: {self.error_all:.2f}')
        fig.tight_layout()

        if not os.path.isdir(self.directory + os.sep + 'pics'): os.mkdir(self.directory + os.sep + 'pics')

        filename_pic = self.directory + os.sep + 'pics' + os.sep + f'{c}.png'
        self.filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()

    def plot_step(self):
        fig, ax = plt.subplots(figsize=(15,8))
        j = 0
        xticks = [0]
        sall = []
        colors = {'MB1': 'orange',
                  'MB2': 'green',
                  # 'MB3':(-54,-8),
                  'MB4': 'black',
                  'MB5': 'blue',
                  # 'MB7':(-40, -5),
                  # 'MB8':(12,6),
                  'ST1': 'magenta',
                  'ST2': 'red'}
        np.random.seed(1234)
        for bn in self.boreholes:
            measu_t = self.measured_time_dict[bn]
            simul_t = self.time_current[self.map_antenna[bn]]
            # sall += simul_t.tolist()
            first_arrival_initial = self.time_initial[self.map_antenna[bn]]

            hnd_m, = ax.plot(np.arange(j, j + len(measu_t)), measu_t, color='green', linestyle='--', alpha=0.8, label='Field data')
            hnd_i, = ax.plot(np.arange(j, j + len(first_arrival_initial)), first_arrival_initial, color='red',
                             linestyle='--', alpha=0.8, label='Initial model')
            hnd_s, = ax.plot(np.arange(j, j + len(simul_t)), simul_t, color='blue', linestyle='-', label='Simulated data')
            ax.axvspan(j, j + len(first_arrival_initial), color=colors[bn] if bn in colors else np.random.rand(3), alpha=0.1)
            ax.text(j + len(first_arrival_initial) / 4, 80, bn,
                    bbox=dict(edgecolor=colors[bn]  if bn in colors else np.random.rand(3), linewidth=1,
                              facecolor='white'), fontsize=22)
            j += len(first_arrival_initial)  # + 10
            xticks.append(j)

        optimized_positions = []
        if len(self.already_optimized_points) > 0:
            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []

            hnd_o = ax.vlines(fix_points, color='gray', linestyles=':',
                              #ymin=self.ymin, ymax=self.ymax,
                              ymin=0, ymax=1200,
                              alpha=0.9,
                      label='Optimized CP')
            optimized_positions = fix_points.tolist() if len(fix_points)>0 else []

        hnd_c = ax.vlines(self.currrent_pos, color='orange',
                          #ymin=self.ymin, ymax=self.ymax,
                          ymin=0, ymax=1200,
                          linestyles=':', label='CP current location')
        optimized_positions += [self.currrent_pos]
        from matplotlib.patches import Ellipse
        circle1 = Ellipse((self.currrent_pos, self.time_current[self.currrent_pos]), 20,30, zorder=10000, color='orange', linewidth=3, fill=False)
        ax.add_patch(circle1, )

        ax1 = ax.twiny()
        ax1.set_xlabel('Optimized Source-Receiver pair (ID number)', color='blue')
        start, end = 0, len(self.measured_travel_time)
        ax1.set_xlim(start, end)
        ax1.set_xticks(optimized_positions)
        # ax.grid()
        ax.set_xlabel('Source-Receiver pair (ID number)')
        ax.set_ylabel('Two way travel time (ns)')
        ax.set_xlim(start, end)
        ax.set_ylim(self.ylim)
        ax.invert_yaxis()
        ax.set_xticks(xticks, )
        # handles, labels = ax.get_legend_handles_labels()
        # ax.legend(handles, labels, loc='lower right',
        #           frameon=True)
        handles = [hnd_m, hnd_i, hnd_s, hnd_c, hnd_o] if len(self.already_optimized_points) > 0 else [hnd_m, hnd_i, hnd_s, hnd_c]
        labels = ['Field data', 'Initial model', 'Simulated data', 'Current location CP', 'Optimized CP'] if len(self.already_optimized_points) > 0 else ['Field data', 'Initial model', 'Simulated data', 'CP current location']
        ax.legend(handles,
                  labels,
                  loc='lower right',
                  frameon=True,
                  fontsize=20)
        ax.tick_params(axis='both', which='both', direction='out', length=5)
        ax1.tick_params(axis='both', which='both', direction='out', length=5, colors='blue', labelrotation=45)
        ax1.spines["top"].set_edgecolor('blue')
        ax.set_title(f'Position: {self.currrent_pos} | (RMS position={self.error_pos:.2f} ns; RMS all= {self.error_all:.2f} ns)  | '
                     f'(Iter. position={self.iter_pos}; Global iter.={self.c})\n', fontsize=24)

        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels() +
                     [ax1.xaxis.label] + ax1.get_xticklabels() ):
            item.set_fontsize(24)
        fig.tight_layout()

        if not os.path.isdir(self.directory + 'pics'): os.mkdir(self.directory + 'pics')
        #
        filename_pic = self.directory + 'pics' + os.sep + f'{self.c}.png'
        self.filenames_pics.append(filename_pic)
        #
        # # save frame
        fig.tight_layout()
        fig.savefig(filename_pic)
        fig.savefig(filename_pic.split('.')[0]+'.pdf')
        fig.show()

    def _manage_position_optimized(self, pos):
        """

        Args:
            pos:

        Returns:

        """
        if self._accepted_position:
            counter = -1
        else:
            counter = self.resting_period

        # [self.already_optimized_points.append(i) for i in np.arange(pos - self.surrounding, pos + self.surrounding + 1)]
        # df_pos = pd.DataFrame(np.arange(pos - self.surrounding, pos + self.surrounding + 1), columns=['positions'])
        values_to_exclude = np.arange(pos - self.surrounding, pos + self.surrounding + 1)
        if len(self.already_optimized_points) == 0:
            self.already_optimized_points['positions'] = values_to_exclude
            self.already_optimized_points['counter'] = counter
        else:

            val = []
            for i in values_to_exclude:
                if i not in self.already_optimized_points['positions'].to_numpy():
                    val.append((i, counter))
                else:
                    self.already_optimized_points.loc[self.already_optimized_points['positions'] == i, 'counter'] = counter
            if len(val) > 0:
                self.already_optimized_points = pd.concat(
                    (self.already_optimized_points, pd.DataFrame(np.asarray(val), columns=['positions', 'counter'])))
