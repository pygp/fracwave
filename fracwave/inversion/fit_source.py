"""
Here you will find all the relevant information to perform the inversion of the source wavelet.
created by Daniel Escallon
Date: 09-08-2023
"""
from fracwave import Antenna, SourceEM, FracEM, DATA_DIR
import shutil, os
# from nptyping import NDArray

class FitSource:
    def __init__(self, ant: Antenna, sou: SourceEM, solv: FracEM):
        self.ant = ant
        self.sou = sou
        self.solv = solv

        if os.path.isdir(DATA_DIR + 'fit_source'):
            shutil.rmtree(DATA_DIR + 'fit_source')
        else:
            os.mkdir(DATA_DIR + 'fit_source')
        self.filename  = DATA_DIR + 'fit_source/file_to_fit_source.h5'

        self.solv = FracEM()
        self.solv.open_file(self.filename)



        new_fname = shutil.copy2(self.solv._filename, self.filename)
        self.filename =

    def invert_source(self, time_vector, #: NDArray[Shape["*nsamples"]] ,
                      time_response, #:NDArray[Shape["*nsamples, *ntraces"]]
                      ):
        """

        Args:
            time_vector:
            time_response:

        Returns:

        """
        def opt_source(x):
            """
                Optimization function
                Args:
                    x: vector to optimize. Parameters of source function
                Returns:

                """
            source_in = self.sou.create_source(x[0], x[1], x[2], x[3], x[4])
            self.sou.export_to_hdf5()
            return np.sqrt(np.sum(np.square(source_in - real_source)))

        self.sou.set_time_vector(time_vector)

        x = [self.sou.source_params['a'],
             self.sou.source_params['b'],
             self.sou.source_params['g'],
             self.sou.source_params['ph'],
             self.sou.source_params['t']]


