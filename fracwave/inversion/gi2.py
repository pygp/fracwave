import numpy as np
import shutil
import os
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from fracwave import FractureGeo, FracEM, set_logger, Antenna
import pandas as pd
logger = set_logger(__name__)
# frac = FractureGeo()
frac = None
# solv = FracEM()
solv = None
c=0

from typing import List
from nptyping import NDArray, Shape, Float
from nptyping import DataFrame, Structure as S

def set_fracture_geometry(f):
    global frac
    frac = f
    logger.info('Fracture geometry set')

def set_fracture_solver(f):
    global solv
    solv = f
    logger.info('Fracture solver set')

def forward_operator(constrain_gempy_points,
                     fixed,
                     fixed_or,
                     filename,
                     max_element_size,
                     plane=None,
                     order=2,
                     kwargs_fracture_properties=dict(),
                     compute_response=False
                     ):
    """

    Returns:

    """
    global frac, solv

    file = filename.split('.')[0]
    if not os.path.isfile(file+'_iter_0.h5'):
        shutil.copy2(filename, file+'_iter_0.h5')
    c = 0
    while True:
        filename = file+ f'_iter_{c}.h5'
        if os.path.isfile(filename):
            filename_next = file + f'_iter_{c+1}.h5'
            if os.path.isfile(filename_next):
                c += 1
                continue
            shutil.copy2(filename, filename_next)
            filename = filename_next
            break
        else:
            shutil.copy2(file+ f'_iter_{c-1}.h5', filename)
            break

    v, _ = frac.create_gempy_geometry(fixed_points=fixed,
                                      fixed_orientations=fixed_or,
                                      move_points=constrain_gempy_points)
    grid, vertices, faces = frac.remesh(v,
                                        plane=plane,
                                        order=order,
                                        max_element_size=max_element_size,
                                        extrapolate=False)

    frac1 = FractureGeo()
    frac1.set_fracture(name_fracture='frac',
                       vertices=vertices,
                       faces=faces,
                       **kwargs_fracture_properties)

    frac1.export_to_hdf5(filename, overwrite=True)

    # return solv_frac(filename)
    solv.engine = 'dummy'
    solv.backend = 'numpy'
    solv.open_file(filename)
    # solv._fast_calculation_incoming_field = True
    # solv.mode = 'incoming_field'

    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    pick = solv.file_read('dummy_solution/first_arrival')
    if compute_response:
        solv.engine = 'tensor_trace'
        solv.backend = 'torch'
        freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    return pick, solv

def get_position_element(step, points, Tx, Rx):
    """
    Get the element value based on the minimum distance to the element form the Tx-Rx position
    Args:
        pos:
        solv:

    Returns:

    """
    global frac
    gempy_extent = frac.geo_model._grid.regular_grid.extent
    t_p = cdist(Tx[None], points)
    r_p = cdist(Rx[None], points)
    distance = t_p[0] + r_p[0]
    pos_min = np.where(distance == distance.min())[0][0]

    point_high_energy = points[pos_min]
    # borehole position
    pos = (Tx + Rx) * 0.5 # Assuming that pos is the midpoint between Tx and Rx
    vector = (point_high_energy - pos) / np.linalg.norm(point_high_energy - pos)
    move = vector * step

    new_point = point_high_energy + move
    if new_point[0] < gempy_extent[0]: new_point[0] = gempy_extent[0]
    if new_point[1] < gempy_extent[2]: new_point[1] = gempy_extent[2]
    if new_point[2] < gempy_extent[4]: new_point[2] = gempy_extent[4]

    if new_point[0] > gempy_extent[1]: new_point[0] = gempy_extent[1]
    if new_point[1] > gempy_extent[3]: new_point[1] = gempy_extent[3]
    if new_point[2] > gempy_extent[5]: new_point[2] = gempy_extent[5]

    return new_point

def _run_inversion(pick_true,
                  fixed_points_geometry,
                  fixed_oientations_geometry,
                  constrained_points_geometry,
                  filename,
                  plot_step=False,
                  kwargs_forward_solver = dict(),
                  kwargs_fracture_properties = dict(),
                  kwargs_cost_function=dict()):
    """

    Args:
        pick_true:
        fixed_points_geometry:
        fixed_oientations_geometry:
        constrained_points_geometry:
        filename:
        velocity:

    Returns:

    """

    di = os.path.dirname(filename)
    # filename = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'
    pick, solv = forward_operator(constrained_points_geometry,
                                  fixed_points_geometry,
                                  fixed_oientations_geometry,
                                  filename=filename,
                                  kwargs_fracture_properties=kwargs_fracture_properties,
                                  **kwargs_forward_solver)
    velocity = solv.velocity
    start_pick = pick

    error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
    max_iter = 0
    dtime = solv.file_read_attribute('source/dtime')
    tol_pos = kwargs_cost_function.pop('tol_pos', 0.3)
    tol_all = kwargs_cost_function.pop('tol_all', 0.5)
    max_iter_all = kwargs_cost_function.pop('max_iter_all',20)
    max_iter_pos = kwargs_cost_function.pop('max_iter_pos',10)
    surrounding = kwargs_cost_function.pop('surrounding', 3)
    step_speed = kwargs_cost_function.pop('step_speed', 3)
    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]
    fix_points = []

    if plot_step:
        ymin, ymax = pick_true.min() - 10, pick_true.max() + 10
        plt.plot(pick_true, 'g--', label='Objective', alpha=0.3)
        plt.plot(start_pick, 'r--', label='Starting', alpha=0.3)

        # plt.plot(pick_plan[:,1], 'r--', label='Objective')
        # plt.ylim(60, 120)
        plt.ylim(ymin, ymax)
        plt.xlim(-1, len(pick_true))
        plt.xlabel('Traces')
        plt.ylabel('Samples')
        plt.title(
            f'Position/Iter: Nan/{c}, Total iter:0\nRMS all: {error_all:.2f}')
        plt.legend()
        # Build gif
        if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

        filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
        filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()

    while error_all > tol_all and max_iter < max_iter_all:
        dif = np.abs(pick_true - pick)
        pos_sorted = np.argsort(dif)
        pos_sorted_after = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
        pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error
        # Now that we have the max position we identify the elements from which the borehole is sensitive
        sample_diff = pick_true[pos] - pick[pos]
        step = sample_diff * dtime * velocity / 2

        file = filename.split('.')[0]
        if not os.path.isfile(file + f'_pos_{pos}.h5'):
            shutil.copy2(filename, file + f'_pos_{pos}.h5')
        file = file + f'_pos_{pos}.h5'
        error_pos = 10000
        max_iter2 = 0
        while error_pos > tol_pos and max_iter2 < max_iter_pos:
            new_point = get_position_element(step/step_speed,
                                             solv.file_read('mesh/geometry/midpoint'),
                                             solv.file_read('antenna/Tx', sl=pos),
                                             solv.file_read('antenna/Rx', sl=pos),
                                             )

            logger.debug(f'New point: {new_point}')
            pick, solv_new = forward_operator(new_point,
                                              fixed_points_geometry,
                                              fixed_oientations_geometry,
                                              filename=file,
                                              kwargs_fracture_properties=kwargs_fracture_properties,
                                              **kwargs_forward_solver)
            sample_diff = pick_true[pos] - pick[pos]
            step = sample_diff * dtime * velocity / 2


            e = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
            if e < error_pos:
                solv = solv_new
            else:
                step /= 2
            error_pos = e
            error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
            max_iter2 += 1
            logger.info(f'\n+++++++++++++++++RMS trace: {error_pos}, for pos:{pos}, iter:{max_iter2}/10 +++++++++++++++++\n')
            if error_pos < tol_pos:
                fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
                fix_points.append(pos)
                logger.info(f'Position {new_point} fixed\n')
            if max_iter2 == 10 or error_pos < tol_pos:
                [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding+ 1)]
            c+=1
            if plot_step:
                ymin, ymax = pick_true.min()-10, pick_true.max()+10
                plt.plot(pick, 'blue', label='Simulated')
                plt.plot(pick_true, 'g--', label='Objective', alpha=0.3)
                plt.plot(start_pick, 'r--', label='Starting', alpha=0.3)
                plt.vlines(pos, color ='black', ymin=ymin, ymax=ymax, label='Current position')

                if len(fix_points) > 0:
                    plt.vlines(fix_points, color='blue', linestyles='--', ymin=ymin, ymax=ymax, alpha=0.3, label='Fixed points')
                # plt.plot(pick_plan[:,1], 'r--', label='Objective')
                # plt.ylim(60, 120)
                plt.ylim(ymin, ymax)
                plt.xlim(-1, len(pick_true))
                plt.xlabel('Traces')
                plt.ylabel('Samples')
                plt.title(f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}, RMS all: {error_all:.2f}')
                plt.legend()
            # Build gif
                if not os.path.isdir(di+os.sep+'pics'): os.mkdir(di+os.sep+'pics')

                filename_pic = di+os.sep+'pics'+ os.sep+f'{c}.png'
                filenames_pics.append(filename_pic)

                # save frame
                plt.savefig(filename_pic)
                plt.show()

            # p.add_object(solv_new.file_read('mesh/geometry/midpoint'), name='mesh2', color='yellow', opacity=0.5,
            #              label='Inverted Geometry')
            # p.add_object(pv.PolyData(fixed_points_geometry), name='point_fixed', color='black', point_size=20)
            # p.add_object(pv.PolyData(new_point), name='point', color='blue', point_size=15)

        error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        logger.debug(f'\n+++++++++++++++++RMS all: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
        max_iter +=1

    if plot_step:
        import imageio
        with imageio.get_writer(di+os.sep+'pics'+ os.sep +'mygif.gif', mode='I') as writer:
            for fnp in filenames_pics:
                image = imageio.v2.imread(fnp)
                writer.append_data(image)
                # os.remove(fnp)
            print('Ready')

    return fixed_points_geometry, solv







def run_inversion(pick_true,
                  fixed_points_geometry,
                  fixed_oientations_geometry,
                  constrained_points_geometry,
                  rx, tx,
                  filename,
                  plot_step=False,
                  kwargs_forward_solver = dict(),
                  kwargs_fracture_properties = dict(),
                  kwargs_cost_function=dict()):
    """

    Args:
        pick_true:
        fixed_points_geometry:
        fixed_oientations_geometry:
        constrained_points_geometry:
        filename:
        velocity:

    Returns:

    """
    global c
    di = os.path.dirname(filename)

    pick, elements = forward_solver(constrain_gempy_points=constrained_points_geometry,
                                      fixed=fixed_points_geometry,
                                      fixed_or=fixed_oientations_geometry,
                                    **kwargs_forward_solver
                                      )
    start_pick = pick

    error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
    max_iter = 0
    # dtime = solv.file_read_attribute('source/dtime')
    tol_pos = kwargs_cost_function.pop('tol_pos', 0.3)
    tol_all = kwargs_cost_function.pop('tol_all', 0.5)
    max_iter_all = kwargs_cost_function.pop('max_iter_all',20)
    max_iter_pos = kwargs_cost_function.pop('max_iter_pos',10)
    surrounding = kwargs_cost_function.pop('surrounding', 3)

    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]

    if plot_step:
        fig, ax = plt.subplots(figsize=(10, 20))
        ax.plot(pick_true, 'g--', label='Real', alpha=0.3)
        ax.plot(start_pick, 'r--', label='Starting', alpha=0.3)
        ax.set_xlabel('Trace no.')
        ax.set_ylabel('Travel Time (ns)')
        ax.set_ylim(pick_true.min()-10, pick_true.max()+10)
        ax.grid()

        fig.legend()
        fig.suptitle(
            f'Position/Iter: Nan/{c}, Total iter:0\nRMS all: {error_all:.2f}')
        fig.tight_layout()

        if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

        filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
        filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()
    c = 0
    while error_all > tol_all and max_iter < max_iter_all:
        positions = np.arange(len(pick_true))
        dif = np.abs(pick_true - pick)
        pos_sorted = positions[np.argsort(dif)]
        pos_sorted_after = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
        if len(pos_sorted_after) == 0:
            logger.error('All positions already optimized. The optimization did not converge')
            return fixed_points_geometry, solv
        pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error

        fixed_points_geometry, solv, filenames_pics = inversion_position(
            filename=filename,
            pos=pos,
            solv=solv,
            pick_true_pos=pick_true[pos],
            pick_pos=pick[pos],
            tol_pos=tol_pos,
            max_iter_pos=max_iter_pos,
            # step_speed=step_speed,
            # step=step,
            fixed_oientations_geometry=fixed_oientations_geometry,
            fixed_points_geometry=fixed_points_geometry,
            kwargs_fracture_properties=kwargs_fracture_properties,
            kwargs_forward_solver=kwargs_forward_solver,
            plot_step=plot_step,
            ymin=ymin,
            ymax=ymax,
            filenames_pics=filenames_pics,
            )

        [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding + 1)]
        fix_points = fixed_points_geometry.loc[fixed_points_geometry['pos'].notna(), 'pos'].to_numpy(dtype=int) if 'pos' in fixed_points_geometry else []

        for p in fix_points:
            pick = solv.file_read('dummy_solution/first_arrival')
            e = np.sqrt(np.square(np.subtract(pick_true[p], pick[p])).mean())
            if e > tol_pos:  # This means that it moved a lot and it needs to be fixed again.
                logger.warning(f'Rejected: Position {p} needs to be relocated. Point: {fixed_points_geometry[fixed_points_geometry["pos"]==p]}')
                filename_t = filename.split('.')[0]
                if not os.path.isfile(filename_t + f'_again.h5'):
                    shutil.copy2(filename, filename_t + f'_again.h5')
                filename_t = filename_t + f'_again.h5'
                fixed_points_geometry, solv, filenames_pics = inversion_position(
                    filename=filename_t,
                    pos=p,
                    solv=solv,
                    pick_true_pos=pick_true[p],
                    pick_pos=pick[p],
                    tol_pos=tol_pos,
                    max_iter_pos=max_iter_pos,
                    # step_speed=step_speed,
                    # step=step,
                    fixed_oientations_geometry=fixed_oientations_geometry,
                    fixed_points_geometry=fixed_points_geometry[fixed_points_geometry['pos']!=p], # Exclude the control point and restart
                    kwargs_fracture_properties=kwargs_fracture_properties,
                    kwargs_forward_solver=kwargs_forward_solver,
                    plot_step=plot_step,
                    ymin=ymin,
                    ymax=ymax,
                    filenames_pics=filenames_pics,
                )
        pick = solv.file_read('dummy_solution/first_arrival')

        error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        logger.debug(f'\n+++++++++++++++++RMS all: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
        max_iter +=1

    if plot_step:
        import imageio
        with imageio.get_writer(di+os.sep+'pics'+ os.sep +'mygif.gif', mode='I') as writer:
            for fnp in filenames_pics:
                image = imageio.v2.imread(fnp)
                writer.append_data(image)
                # os.remove(fnp)
            print('Ready')

    return fixed_points_geometry.reset_index(drop=True), solv

def _run_inversion(pick_true,
                  fixed_points_geometry,
                  fixed_oientations_geometry,
                  constrained_points_geometry,
                  filename,
                  plot_step=False,
                  kwargs_forward_solver = dict(),
                  kwargs_fracture_properties = dict(),
                  kwargs_cost_function=dict()):
    """

    Args:
        pick_true:
        fixed_points_geometry:
        fixed_oientations_geometry:
        constrained_points_geometry:
        filename:
        velocity:

    Returns:

    """
    global c

    di = os.path.dirname(filename)
    # filename = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'
    pick, solv = forward_operator(constrained_points_geometry,
                                  fixed_points_geometry,
                                  fixed_oientations_geometry,
                                  filename=filename,
                                  kwargs_fracture_properties=kwargs_fracture_properties,
                                  **kwargs_forward_solver)
    start_pick = pick

    error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
    max_iter = 0
    # dtime = solv.file_read_attribute('source/dtime')
    tol_pos = kwargs_cost_function.pop('tol_pos', 0.3)
    tol_all = kwargs_cost_function.pop('tol_all', 0.5)
    max_iter_all = kwargs_cost_function.pop('max_iter_all',20)
    max_iter_pos = kwargs_cost_function.pop('max_iter_pos',10)
    surrounding = kwargs_cost_function.pop('surrounding', 3)
    step_speed = kwargs_cost_function.pop('step_speed', 3)
    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]
    fix_points = []

    if plot_step:
        ymin = dict()
        ymax = dict()
        for p in solv.name_profiles:
            trace_no = solv.file_read(f'antenna/profiles/{p}')
            ymin[p] = pick_true[trace_no].min() - 10
            ymax[p] = pick_true[trace_no].max() + 10

        fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
        if len(solv.name_profiles)==1: AX = [AX]
        for p, ax in zip(solv.name_profiles, AX):
            trace_no = solv.file_read(f'antenna/profiles/{p}')
            ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
            ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
            ax.set_xlabel('Trace no.')
            ax.set_ylabel('Travel Time (ns)')
            ax.set_ylim(ymin[p], ymax[p])
            ax.grid()
            ax.set_title(p)
        handles, labels = ax.get_legend_handles_labels()
        fig.legend(handles, labels)
        fig.suptitle(
            f'Position/Iter: Nan/{c}, Total iter:0\nRMS all: {error_all:.2f}')
        fig.tight_layout()

        if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

        filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
        filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()
    c = 0
    while error_all > tol_all and max_iter < max_iter_all:
        positions = np.arange(len(pick_true))
        dif = np.abs(pick_true - pick)
        pos_sorted = positions[np.argsort(dif)]
        pos_sorted_after = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
        if len(pos_sorted_after) == 0:
            logger.error('All positions already optimized. The optimization did not converge')
            return fixed_points_geometry, solv
        pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error
        # pos = np.where(pos_sorted_after == pos_sorted_after.max())[0][0]
        # Now that we have the max position we identify the elements from which the borehole is sensitive
        # sample_diff = pick_true[pos] - pick[pos]
        # step = sample_diff * dtime * velocity / 2
        # step = sample_diff * solv.velocity / 2
        #
        # file = filename.split('.')[0]
        # if not os.path.isfile(file + f'_pos_{pos}.h5'):
        #     shutil.copy2(filename, file + f'_pos_{pos}.h5')
        # file = file + f'_pos_{pos}.h5'
        # error_pos = 10000
        # max_iter2 = 0
        # step_speed_temp = step_speed
        # while error_pos > tol_pos and max_iter2 < max_iter_pos:
        #     new_point = get_position_element(step / step_speed_temp,
        #                                      solv.file_read('mesh/geometry/midpoint'),
        #                                      solv.file_read('antenna/Tx', sl=pos),
        #                                      solv.file_read('antenna/Rx', sl=pos),
        #                                      )
        #
        #     logger.debug(f'New point: {new_point}')
        #     df_new = pd.DataFrame({'X': [new_point[0]], 'Y': [new_point[1]], 'Z': [new_point[2]]})
        #     pick, solv_new = forward_operator(constrain_gempy_points=df_new,
        #                                       fixed=fixed_points_geometry,
        #                                       fixed_or=fixed_oientations_geometry,
        #                                       filename=file,
        #                                       kwargs_fracture_properties=kwargs_fracture_properties,
        #                                       **kwargs_forward_solver)
        #     sample_diff = pick_true[pos] - pick[pos]
        #     # step = sample_diff * dtime * velocity / 2
        #
        #     e = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
        #     if e < error_pos:
        #         solv = solv_new
        #         step = sample_diff * solv.velocity / 2
        #         error_pos = e
        #         error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        #
        #     else:
        #         step_speed_temp += 1
        #         logger.info(
        #             f'\n+++++++++++++++++Failed RMS trace: {e}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        #     max_iter2 += 1
        #     logger.info(f'\n+++++++++++++++++RMS trace: {error_pos}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        #     if error_pos <= tol_pos:
        #         # fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
        #         df_new['pos'] = pos
        #         fixed_points_geometry = pd.concat((fixed_points_geometry, df_new))
        #         fix_points.append(pos)
        #         logger.info(f'Position {new_point} fixed\n')
        #     if max_iter2 == max_iter_pos or error_pos <= tol_pos:
        #         [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding+ 1)]
        #     c+=1
        #     if plot_step:
        #         leg_dict = dict()
        #         fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
        #         if len(solv.name_profiles): AX = [AX]
        #         for p, ax in zip(solv.name_profiles, AX):
        #             trace_no = solv.file_read(f'antenna/profiles/{p}')
        #             ax.plot(trace_no, pick[trace_no], 'blue', label='Simulated')
        #             ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
        #             ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
        #             if pos in trace_no:
        #                 ax.vlines(pos, color='black', ymin=ymin[p], ymax=ymax[p], label='Current position')
        #
        #             if len(fix_points) > 0:
        #                 for fp in fix_points:
        #                     if fp in trace_no:
        #                         ax.vlines(fp, color='blue', linestyles='--', ymin=ymin[p], ymax=ymax[p], alpha=0.3,
        #                                    label='Fixed points')
        #             ax.set_xlabel('Trace no.')
        #             ax.set_ylabel('Travel Time (ns)')
        #             ax.set_ylim(ymin[p], ymax[p])
        #             ax.grid()
        #             ax.set_title(p)
        #             handles, labels = ax.get_legend_handles_labels()
        #             for l, h in zip(labels, handles):
        #                 if l not in leg_dict:
        #                     leg_dict[l] = h
        #
        #         fig.legend(leg_dict.values(), leg_dict.keys())
        #         fig.suptitle(f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}, RMS all: {error_all:.2f}')
        #         fig.tight_layout()
        #
        #         if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')
        #
        #         filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
        #         filenames_pics.append(filename_pic)
        #
        #         # save frame
        #         plt.savefig(filename_pic)
        #         plt.show()

        # After optimizing the point we will check if the new position have changed the optimization of the other points
        # and if that is the case try to fit the position again forgetting recalculating this point

        fixed_points_geometry, solv, filenames_pics = inversion_position(
            filename=filename,
            pos=pos,
            solv=solv,
            pick_true_pos=pick_true[pos],
            pick_pos=pick[pos],
            tol_pos=tol_pos,
            max_iter_pos=max_iter_pos,
            # step_speed=step_speed,
            # step=step,
            fixed_oientations_geometry=fixed_oientations_geometry,
            fixed_points_geometry=fixed_points_geometry,
            kwargs_fracture_properties=kwargs_fracture_properties,
            kwargs_forward_solver=kwargs_forward_solver,
            plot_step=plot_step,
            ymin=ymin,
            ymax=ymax,
            filenames_pics=filenames_pics,
            )

        [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding + 1)]
        fix_points = fixed_points_geometry.loc[fixed_points_geometry['pos'].notna(), 'pos'].to_numpy(dtype=int) if 'pos' in fixed_points_geometry else []

        for p in fix_points:
            pick = solv.file_read('dummy_solution/first_arrival')
            e = np.sqrt(np.square(np.subtract(pick_true[p], pick[p])).mean())
            if e > tol_pos:  # This means that it moved a lot and it needs to be fixed again.
                logger.warning(f'Rejected: Position {p} needs to be relocated. Point: {fixed_points_geometry[fixed_points_geometry["pos"]==p]}')
                filename_t = filename.split('.')[0]
                if not os.path.isfile(filename_t + f'_again.h5'):
                    shutil.copy2(filename, filename_t + f'_again.h5')
                filename_t = filename_t + f'_again.h5'
                fixed_points_geometry, solv, filenames_pics = inversion_position(
                    filename=filename_t,
                    pos=p,
                    solv=solv,
                    pick_true_pos=pick_true[p],
                    pick_pos=pick[p],
                    tol_pos=tol_pos,
                    max_iter_pos=max_iter_pos,
                    # step_speed=step_speed,
                    # step=step,
                    fixed_oientations_geometry=fixed_oientations_geometry,
                    fixed_points_geometry=fixed_points_geometry[fixed_points_geometry['pos']!=p], # Exclude the control point and restart
                    kwargs_fracture_properties=kwargs_fracture_properties,
                    kwargs_forward_solver=kwargs_forward_solver,
                    plot_step=plot_step,
                    ymin=ymin,
                    ymax=ymax,
                    filenames_pics=filenames_pics,
                )
        pick = solv.file_read('dummy_solution/first_arrival')

        error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        logger.debug(f'\n+++++++++++++++++RMS all: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
        max_iter +=1

    if plot_step:
        import imageio
        with imageio.get_writer(di+os.sep+'pics'+ os.sep +'mygif.gif', mode='I') as writer:
            for fnp in filenames_pics:
                image = imageio.v2.imread(fnp)
                writer.append_data(image)
                # os.remove(fnp)
            print('Ready')

    return fixed_points_geometry.reset_index(drop=True), solv

import numpy as np
from scipy.spatial.distance import cdist

def forward_solver(fixed, constrain_gempy_points, fixed_oientations, rx, tx, velocity=0.1234, max_distance=60, **kwargs_remesh):
    """
    The dummy forward model calculates the travel time based on the geometry and the borehole.
    Args:
        self:

    Returns:
    """
    global frac
    v, _ = frac.create_gempy_geometry(fixed_points=fixed,
                                      fixed_orientations=fixed_oientations,
                                      move_points=constrain_gempy_points)

    # Filter everything outside of this distance
    xyz = np.vstack((rx, tx))
    dist = cdist(xyz, v)
    mask = np.zeros(len(v), dtype=bool)
    mask |= (dist<max_distance).sum(axis=0).astype(bool)

    grid, vertices, faces = frac.remesh(v[mask], **kwargs_remesh)

    element_positions = np.asarray(grid.cell_centers().points)

    t_e = cdist(tx, element_positions)
    r_e = cdist(rx, element_positions)
    distance = t_e + r_e
    time = distance / velocity

    # Get the travel time corresponding to the minimum distance
    first_arrival = np.min(time, axis=1)

    return first_arrival, element_positions



def _inversion_position(filename,
                       pos,
                       solv,
                       pick_true_pos,
                       pick_pos,
                       tol_pos, max_iter_pos,
                       fixed_oientations_geometry, fixed_points_geometry,
                       kwargs_fracture_properties, kwargs_forward_solver,
                       plot_step, ymin, ymax, filenames_pics):
    """

    Args:
        filename:
        pos:
        solv:
        pick_true:
        tol_pos:
        max_iter_pos:
        step_speed:
        step:
        fixed_oientations_geometry:
        fixed_points_geometry:
        kwargs_fracture_properties:
        kwargs_forward_solver:
        plot_step:
        ymin:
        ymax:
        filenames_pics:

    Returns:

    """
    global c
    file = filename.split('.')[0]
    if not os.path.isfile(file + f'_pos_{pos}.h5'):
        shutil.copy2(filename, file + f'_pos_{pos}.h5')
    file = file + f'_pos_{pos}.h5'
    error_pos = 10000
    max_iter2 = 0

    sample_diff = pick_true_pos - pick_pos
    step = sample_diff * solv.velocity / 2

    # step_speed_temp = step_speed
    while error_pos > tol_pos and max_iter2 < max_iter_pos:
        new_point = get_position_element(step, #/ step_speed_temp,
                                         solv.file_read('mesh/geometry/midpoint'),
                                         solv.file_read('antenna/Tx', sl=pos),
                                         solv.file_read('antenna/Rx', sl=pos),
                                         )

        logger.debug(f'New point: {new_point}')
        df_new = pd.DataFrame({'X': [new_point[0]], 'Y': [new_point[1]], 'Z': [new_point[2]]})
        pick, solv = forward_operator(constrain_gempy_points=df_new,
                                          fixed=fixed_points_geometry,
                                          fixed_or=fixed_oientations_geometry,
                                          filename=file,
                                          kwargs_fracture_properties=kwargs_fracture_properties,
                                          **kwargs_forward_solver)
        sample_diff = pick_true_pos - pick[pos]
        # step = sample_diff * dtime * velocity / 2

        e = np.sqrt(np.square(np.subtract(pick_true_pos, pick[pos])).mean())
        if e < error_pos:
            # solv = solv_new
            pass
            # step = sample_diff * solv.velocity / 2
            # error_pos = e
            # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())

        else:
            # step /= 2
            logger.info(
                f'\n+++++++++++++++++Failed RMS trace: {e}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
                # f'\n+++++++++++++++++Failed RMS trace: {e}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        error_pos = e
        # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        step = sample_diff * solv.velocity / 2
        max_iter2 += 1
        logger.info(
            f'\n+++++++++++++++++RMS trace: {error_pos}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        if error_pos <= tol_pos:
            # fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
            df_new['pos'] = pos
            fixed_points_geometry = pd.concat((fixed_points_geometry, df_new))
            logger.info(f'Position {new_point} fixed\n')
        # if max_iter2 == max_iter_pos or error_pos <= tol_pos:
        #     [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding + 1)]
        c += 1
        fix_points = fixed_points_geometry.loc[fixed_points_geometry['pos'].notna(), 'pos'].to_numpy(dtype=int) if 'pos' in fixed_points_geometry else []
        if plot_step:
            leg_dict = dict()
            fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
            if len(solv.name_profiles)==1: AX = [AX]
            for p, ax in zip(solv.name_profiles, AX):
                trace_no = solv.file_read(f'antenna/profiles/{p}')
                ax.plot(trace_no, pick[trace_no], 'blue', label='Simulated')
                # ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
                # ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
                if pos in trace_no:
                    ax.vlines(pos, color='black', ymin=ymin[p], ymax=ymax[p], label='Current position')

                if len(fix_points) > 0:
                    for fp in fix_points:
                        if fp in trace_no:
                            ax.vlines(fp, color='blue', linestyles='--', ymin=ymin[p], ymax=ymax[p], alpha=0.3,
                                      label='Fixed points')
                ax.set_xlabel('Trace no.')
                ax.set_ylabel('Travel Time (ns)')
                ax.set_ylim(ymin[p], ymax[p])
                ax.grid()
                ax.set_title(p)
                handles, labels = ax.get_legend_handles_labels()
                for l, h in zip(labels, handles):
                    if l not in leg_dict:
                        leg_dict[l] = h

            fig.legend(leg_dict.values(), leg_dict.keys())
            fig.suptitle(
                f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}')
            fig.tight_layout()

            di = os.path.dirname(filename)
            if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

            filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
            filenames_pics.append(filename_pic)

            # save frame
            plt.savefig(filename_pic)
            plt.show()
    fixed_points_geometry.to_csv(file.split('.')[0] + '.csv')
    return fixed_points_geometry, solv, filenames_pics


def inversion_position(filename,
                       pos,
                       solv,
                       pick_true_pos,
                       pick_pos,
                       tol_pos, max_iter_pos,
                       fixed_oientations_geometry, fixed_points_geometry,
                       kwargs_fracture_properties, kwargs_forward_solver,
                       plot_step, ymin, ymax, filenames_pics):
    """

    Args:
        filename:
        pos:
        solv:
        pick_true:
        tol_pos:
        max_iter_pos:
        step_speed:
        step:
        fixed_oientations_geometry:
        fixed_points_geometry:
        kwargs_fracture_properties:
        kwargs_forward_solver:
        plot_step:
        ymin:
        ymax:
        filenames_pics:

    Returns:

    """
    global c
    file = filename.split('.')[0]
    if not os.path.isfile(file + f'_pos_{pos}.h5'):
        shutil.copy2(filename, file + f'_pos_{pos}.h5')
    file = file + f'_pos_{pos}.h5'
    error_pos = 10000
    max_iter2 = 0

    sample_diff = pick_true_pos - pick_pos
    step = sample_diff * solv.velocity / 2

    # step_speed_temp = step_speed
    while error_pos > tol_pos and max_iter2 < max_iter_pos:
        new_point = get_position_element(step, #/ step_speed_temp,
                                         solv.file_read('mesh/geometry/midpoint'),
                                         solv.file_read('antenna/Tx', sl=pos),
                                         solv.file_read('antenna/Rx', sl=pos),
                                         )

        logger.debug(f'New point: {new_point}')
        df_new = pd.DataFrame({'X': [new_point[0]], 'Y': [new_point[1]], 'Z': [new_point[2]]})
        pick, solv = forward_operator(constrain_gempy_points=df_new,
                                          fixed=fixed_points_geometry,
                                          fixed_or=fixed_oientations_geometry,
                                          filename=file,
                                          kwargs_fracture_properties=kwargs_fracture_properties,
                                          **kwargs_forward_solver)
        sample_diff = pick_true_pos - pick[pos]
        # step = sample_diff * dtime * velocity / 2

        e = np.sqrt(np.square(np.subtract(pick_true_pos, pick[pos])).mean())
        if e < error_pos:
            # solv = solv_new
            pass
            # step = sample_diff * solv.velocity / 2
            # error_pos = e
            # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())

        else:
            # step /= 2
            logger.info(
                f'\n+++++++++++++++++Failed RMS trace: {e}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
                # f'\n+++++++++++++++++Failed RMS trace: {e}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        error_pos = e
        # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        step = sample_diff * solv.velocity / 2
        max_iter2 += 1
        logger.info(
            f'\n+++++++++++++++++RMS trace: {error_pos}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} +++++++++++++++++\n')
        if error_pos <= tol_pos:
            # fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
            df_new['pos'] = pos
            fixed_points_geometry = pd.concat((fixed_points_geometry, df_new))
            logger.info(f'Position {new_point} fixed\n')
        # if max_iter2 == max_iter_pos or error_pos <= tol_pos:
        #     [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding + 1)]
        c += 1
        fix_points = fixed_points_geometry.loc[fixed_points_geometry['pos'].notna(), 'pos'].to_numpy(dtype=int) if 'pos' in fixed_points_geometry else []
        if plot_step:
            leg_dict = dict()
            fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
            if len(solv.name_profiles)==1: AX = [AX]
            for p, ax in zip(solv.name_profiles, AX):
                trace_no = solv.file_read(f'antenna/profiles/{p}')
                ax.plot(trace_no, pick[trace_no], 'blue', label='Simulated')
                # ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
                # ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
                if pos in trace_no:
                    ax.vlines(pos, color='black', ymin=ymin[p], ymax=ymax[p], label='Current position')

                if len(fix_points) > 0:
                    for fp in fix_points:
                        if fp in trace_no:
                            ax.vlines(fp, color='blue', linestyles='--', ymin=ymin[p], ymax=ymax[p], alpha=0.3,
                                      label='Fixed points')
                ax.set_xlabel('Trace no.')
                ax.set_ylabel('Travel Time (ns)')
                ax.set_ylim(ymin[p], ymax[p])
                ax.grid()
                ax.set_title(p)
                handles, labels = ax.get_legend_handles_labels()
                for l, h in zip(labels, handles):
                    if l not in leg_dict:
                        leg_dict[l] = h

            fig.legend(leg_dict.values(), leg_dict.keys())
            fig.suptitle(
                f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}')
            fig.tight_layout()

            di = os.path.dirname(filename)
            if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

            filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
            filenames_pics.append(filename_pic)

            # save frame
            plt.savefig(filename_pic)
            plt.show()
    fixed_points_geometry.to_csv(file.split('.')[0] + '.csv')
    return fixed_points_geometry, solv, filenames_pics

def run_inversion_per_profile(pick_true,
                  fixed_points_geometry,
                  fixed_oientations_geometry,
                  constrained_points_geometry,
                  filename,
                  plot_step=False,
                  kwargs_forward_solver = dict(),
                  kwargs_fracture_properties = dict(),
                  kwargs_cost_function=dict()):
    """

    Args:
        pick_true:
        fixed_points_geometry:
        fixed_oientations_geometry:
        constrained_points_geometry:
        filename:
        velocity:

    Returns:

    """

    di = os.path.dirname(filename)
    # filename = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'
    pick, solv = forward_operator(constrained_points_geometry,
                                  fixed_points_geometry,
                                  fixed_oientations_geometry,
                                  filename=filename,
                                  kwargs_fracture_properties=kwargs_fracture_properties,
                                  **kwargs_forward_solver)
    velocity = solv.velocity
    start_pick = pick

    error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
    max_iter = 0
    dtime = solv.file_read_attribute('source/dtime')
    tol_pos = kwargs_cost_function.pop('tol_pos', 0.3)
    tol_all = kwargs_cost_function.pop('tol_all', 0.5)
    max_iter_all = kwargs_cost_function.pop('max_iter_all',20)
    max_iter_pos = kwargs_cost_function.pop('max_iter_pos',10)
    surrounding = kwargs_cost_function.pop('surrounding', 3)
    step_speed = kwargs_cost_function.pop('step_speed', 3)
    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]
    fix_points = []

    if plot_step:
        ymin = dict()
        ymax = dict()
        for p in solv.name_profiles:
            trace_no = solv.file_read(f'antenna/profiles/{p}')
            ymin[p] = pick_true[trace_no].min() - 10
            ymax[p] = pick_true[trace_no].max() + 10

        fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
        for p, ax in zip(solv.name_profiles, AX):
            trace_no = solv.file_read(f'antenna/profiles/{p}')
            ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
            ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
            ax.set_xlabel('Trace no.')
            ax.set_ylabel('Travel Time')
            ax.set_ylim(ymin[p], ymax[p])
            ax.grid()
            ax.set_title(p)
        handles, labels = ax.get_legend_handles_labels()
        fig.legend(handles, labels)
        fig.suptitle(
            f'Position/Iter: Nan/{c}, Total iter:0\nRMS all: {error_all:.2f}')
        fig.tight_layout()

        if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

        filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
        filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()

    while error_all > tol_all and max_iter < max_iter_all:
        for name in solv.name_profiles:
            trace_no = solv.file_read(f'antenna/profiles/{name}')
            dif = np.abs(pick_true[trace_no] - pick[trace_no])
            error_current_trace = np.sqrt(np.square(dif).mean())
            if error_current_trace <= tol_all: # Jump the profile and continue to the next one
                continue
            pos_sorted = np.argsort(dif)
            pos_sorted = trace_no[pos_sorted]
            options = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
            # pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error
            pos = options[-1] # Take the last value which corresponds to the biggest error
            # Now that we have the max position we identify the elements from which the borehole is sensitive
            sample_diff = pick_true[pos] - pick[pos]
            step = sample_diff * dtime * velocity / 2

            file = filename.split('.')[0]
            if not os.path.isfile(file + f'_pos_{pos}.h5'):
                shutil.copy2(filename, file + f'_pos_{pos}.h5')
            file = file + f'_pos_{pos}.h5'
            error_pos = 10000
            max_iter2 = 0
            while error_pos > tol_pos and max_iter2 < max_iter_pos:
                new_point = get_position_element(step/step_speed,
                                                 solv.file_read('mesh/geometry/midpoint'),
                                                 solv.file_read('antenna/Tx', sl=pos),
                                                 solv.file_read('antenna/Rx', sl=pos),
                                                 )

                logger.debug(f'New point: {new_point}')
                pick, solv_new = forward_operator(new_point,
                                                  fixed_points_geometry,
                                                  fixed_oientations_geometry,
                                                  filename=file,
                                                  kwargs_fracture_properties=kwargs_fracture_properties,
                                                  **kwargs_forward_solver)
                sample_diff = pick_true[pos] - pick[pos]
                step = sample_diff * dtime * velocity / 2


                e = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
                if e < error_pos:
                    solv = solv_new
                else:
                    step /= 2
                error_pos = e
                error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
                max_iter2 += 1
                logger.info(f'\n+++++++++++++++++RMS trace: {error_pos}, for pos:{pos}, iter:{max_iter2}/{max_iter_pos} \nProfile: {name}+++++++++++++++++\n')
                if error_pos < tol_pos:
                    fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
                    fix_points.append(pos)
                    logger.info(f'Position {new_point} fixed\n')
                if max_iter2 == 10 or error_pos < tol_pos:
                    [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding+ 1)]
                c+=1
                if plot_step:
                    fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10, 20))
                    for p, ax in zip(solv.name_profiles, AX):
                        trace_no = solv.file_read(f'antenna/profiles/{p}')
                        ax.plot(trace_no, pick[trace_no], 'blue', label='Simulated')
                        ax.plot(trace_no, pick_true[trace_no], 'g--', label='Real', alpha=0.3)
                        ax.plot(trace_no, start_pick[trace_no], 'r--', label='Starting', alpha=0.3)
                        if pos in trace_no:
                            ax.vlines(pos, color='black', ymin=ymin[p], ymax=ymax[p], label='Current position')

                        if len(fix_points) > 0:
                            for fp in fix_points:
                                if fp in trace_no:
                                    ax.vlines(fp, color='blue', linestyles='--', ymin=ymin[p], ymax=ymax[p], alpha=0.3,
                                               label='Fixed points')
                        ax.set_xlabel('Trace no.')
                        ax.set_ylabel('Travel Time')
                        ax.set_ylim(ymin[p], ymax[p])
                        ax.grid()
                        ax.set_title(p)

                    handles, labels = ax.get_legend_handles_labels()
                    fig.legend(handles, labels)
                    fig.suptitle(f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}, RMS all: {error_all:.2f}\nProfile: {name}')
                    fig.tight_layout()

                    if not os.path.isdir(di + os.sep + 'pics'): os.mkdir(di + os.sep + 'pics')

                    filename_pic = di + os.sep + 'pics' + os.sep + f'{c}.png'
                    filenames_pics.append(filename_pic)

                    # save frame
                    plt.savefig(filename_pic)
                    plt.show()

            error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
            logger.debug(f'\n+++++++++++++++++RMS all: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
            max_iter +=1

    if plot_step:
        import imageio
        with imageio.get_writer(di+os.sep+'pics'+ os.sep +'mygif.gif', mode='I') as writer:
            for fnp in filenames_pics:
                image = imageio.v2.imread(fnp)
                writer.append_data(image)
                # os.remove(fnp)
            print('Ready')

    return fixed_points_geometry, solv


class GeometryInversion:
    """This class manages the inversion based on travel times"""
    def __init__(self,
                 gempy_extent: List[float],
                 gempy_resolution: List[int],
                 velocity: float,
                 # Tx: NDArray[Shape["* points, 3"], Float],
                 # Rx: NDArray[Shape["* points, 3"], Float],
                 antenna: Antenna,
                 # measured_travel_time: NDArray[Shape["* points"], Float]
                 measured_travel_time: dict
                 ):
        """

        Args:
            gempy_extent: [xmin, xmax, ymin, ymax, zmin, zmax]
            resolution: [xresol, yresol, zresol]
            velocity: (m/ns)
            Tx: Transmitter xyz positions
            Rx: Receiver xyz positions
            measured_travel_time: Real data
        """
        self.gempy_extent = gempy_extent
        self.gempy_resolution = gempy_resolution
        self.frac = FractureGeo()
        self.frac.init_geo_model(gempy_extent, gempy_resolution)
        self.velocity = velocity

        self.fixed_control_points = None
        self.temp_control_points = None
        self.fixed_orientations = None

        self.ant = antenna
        self.map_antenna = dict()

        self.measured_time_dict = measured_travel_time
        transmitter = []
        receiver = []
        measured_time = []
        self.boreholes = self.ant.name_profiles
        for bn in self.boreholes:
            self.map_antenna[bn] = np.asarray(self.ant.profiles.loc[self.ant.profiles['profile'] == bn].index.values)
            transmitter.append(self.ant.profiles.loc[self.ant.profiles['profile'] == bn, ('Tx', 'Ty', 'Tz')].to_numpy())
            receiver.append(self.ant.profiles.loc[self.ant.profiles['profile'] == bn, ('Rx', 'Ry', 'Rz')].to_numpy())
            measured_time.append(measured_travel_time[bn])

        self.Tx = np.vstack(transmitter)
        self.Rx = np.vstack(receiver)
        self.measured_travel_time = np.hstack(measured_time)

        self.midpoints = None
        self.vertices = None
        self.faces = None
        self.grid = None


        self.c = 0

        self.kwargs_remesh = dict()
        self.max_distance_from_borehole = None
        self.max_distance_from_surface_minimum_distance = None

        self.tol_pos = 3  # Time difference (ns)
        self.tol_all = 5  # Time difference (ns)
        self.max_iter_pos = 10  # Maximum iteration when inverting for single position
        self.iter_pos = 0  # Maximum iteration when inverting for single position
        self.max_iter_all = 20  # Maximum iteration when inverting for single position
        self.iter_all = 0  # Maximum iteration when inverting for single position
        self.surrounding = 3  # Ignore the values surrounding the position so the position is not that close between each other

        self.error_pos = None
        self.error_all = None

        self.time_current = None
        self.time_initial = None

        self.currrent_pos = None
        self.ymin = None
        self.ymax = None
        # self.already_optimized_points = []
        self.already_optimized_points = pd.DataFrame()

        self.directory = ''
        self.filenames_pics = []

        self.show_plot = True

        self.rejected_points = pd.DataFrame()

        self.error_history = []
        self.std_threshold = 0.3
        self.std_average = 3

        self.error_all_history = []
        self.points_all_history = []
        self._accepted_position = False
        self.resting_period = 3  # The amount of iterations to pass to be able to consider the point again

    def clear(self):
        self.fixed_control_points = None
        self.temp_control_points = None
        self.fixed_orientations = None

        self.midpoints = None
        self.vertices = None
        self.faces = None
        self.grid = None

        self.c = 0

        self.kwargs_remesh = dict()
        self.max_distance_from_borehole = None

        self.tol_pos = 3  # Time difference (ns)
        self.tol_all = 5  # Time difference (ns)
        self.max_iter_pos = 10  # Maximum iteration when inverting for single position
        self.iter_pos = 0  # Maximum iteration when inverting for single position
        self.max_iter_all = 20  # Maximum iteration when inverting for single position
        self.iter_all = 0  # Maximum iteration when inverting for single position
        self.surrounding = 3  # Ignore the values surrounding the position so the position is not that close between each other

        self.error_pos = None
        self.error_all = None

        self.time_current = None
        self.time_initial = None

        self.currrent_pos = None
        self.ymin = None
        self.ymax = None
        # self.already_optimized_points = []
        self.already_optimized_points = pd.DataFrame()

        self.directory = ''
        self.filenames_pics = []
        self.rejected_points = pd.DataFrame()
        self.error_history = []
        self.std_threshold = 0.3
        self.std_average= 3

    def set_initial_points(self,
                           fixed_points: DataFrame[S["X: Float, Y: Float, Z: Float"]],
                           fixed_orientations: DataFrame[S["X: Float, Y: Float, Z: Float, "
                                                           "G_x: Float, G_y: Float, G_z: Float"]],
                           ):
        """

        Args:
            fixed_points:
            fixed_orientations:

        Returns:

        """
        self.fixed_control_points = fixed_points
        self.fixed_orientations = fixed_orientations

    def forward_solver(self):
        """

        Returns:

        """
        v, f = self.frac.create_gempy_geometry(fixed_points=self.fixed_control_points,
                                               fixed_orientations=self.fixed_orientations,
                                               move_points=self.temp_control_points)

        convert_to_grid = self.kwargs_remesh.pop('convert_to_grid', True)
        kwargs = self.kwargs_remesh.copy()
        if convert_to_grid:
            self.grid, self.vertices, self.faces = self.frac.remesh(v, **kwargs)
        else:
            self.grid, self.vertices, self.faces = self.frac.refine_mesh(v, f, **kwargs)

        if self.max_distance_from_borehole is not None or self.max_distance_from_surface_minimum_distance is not None:
            self.grid, self.vertices, self.faces =self.frac.mask_mesh(self.vertices,
                                                                      self.faces,
                                                                      positions=np.vstack((self.Rx, self.Tx)),
                                                                      from_maximum_distance_borehole=self.max_distance_from_borehole,
                                                                      from_maximum_distance_surface=self.max_distance_from_surface_minimum_distance)

        elements = self.vertices[self.faces]
        if elements.ndim == 2:  # This means there is only 1 elements
            elements = elements[None]
        self.midpoints = elements.mean(axis=1)

        min_distance, min_position = self.frac.get_position_reflection_point(self.midpoints,
                                                                             self.Tx,
                                                                             self.Rx)
        first_arrival = min_distance / self.velocity
        self.time_current = first_arrival
        return first_arrival, min_position

    def inversion_position(self, pos: int):
        """

        Args:
            pos: Position

        Returns:

        """
        self._accepted_position = False
        self.currrent_pos = pos
        time_init, element_init = self.forward_solver()
        tpos = time_init[pos]
        bpos = (self.Tx[pos] + self.Rx[pos]) * 0.5  # Here we assume that the middle position between the antennas is the one to move
        mpos = element_init[pos]
        ttruepos = self.measured_travel_time[pos]
        t_diff = ttruepos - tpos
        self.iter_pos = 0
        self.error_pos = np.abs(t_diff)
        tp = self.tol_pos
        if isinstance(tp, (list, NDArray)):
            tp = tp[pos]
        time_new = time_init
        while self.error_pos > tp and self.iter_pos < self.max_iter_pos:
            vector = (mpos - bpos) / np.linalg.norm(mpos - bpos)
            step = self.velocity * t_diff * 0.5  # Divide by 2 because the two-way-travel time.
            new_point = mpos + (vector * step)
            if new_point[0] < self.gempy_extent[0]: new_point[0] = self.gempy_extent[0]
            if new_point[1] < self.gempy_extent[2]: new_point[1] = self.gempy_extent[2]
            if new_point[2] < self.gempy_extent[4]: new_point[2] = self.gempy_extent[4]

            if new_point[0] > self.gempy_extent[1]: new_point[0] = self.gempy_extent[1]
            if new_point[1] > self.gempy_extent[3]: new_point[1] = self.gempy_extent[3]
            if new_point[2] > self.gempy_extent[5]: new_point[2] = self.gempy_extent[5]
            logger.debug(f'New point: {new_point}')
            self.points_all_history.append(new_point.tolist() + [pos])
            self.temp_control_points = pd.DataFrame([new_point], columns=['X', 'Y', 'Z'])
            time_new, element_new = self.forward_solver()
            self.error_all = np.sqrt(np.square(self.measured_travel_time - time_new).mean())
            # Second criteria of convergence. See if the last values are not moving
            self.error_history.append(self.error_all)
            t_diff = ttruepos - time_new[pos]
            self.error_pos = np.abs(t_diff)
            if self.error_pos < tp:
                self.temp_control_points['pos'] = pos
                self.fixed_control_points = pd.concat((self.fixed_control_points, self.temp_control_points))
                self.temp_control_points = None
                logger.info(f'Position {new_point} fixed\n')
                self._accepted_position = True
            else:
                mpos = element_new[pos]
                self._accepted_position = False
            self.iter_pos += 1
            self.c += 1
            if self.show_plot:
                self.plot_step()
        self.temp_control_points = None
        self.error_all_history.append((self.error_all, self.c))
        return time_new

    def run_inversion(self):
        """

        Returns:

        """
        self.ymin = self.measured_travel_time.min() - 5
        self.ymax = self.measured_travel_time.max() + 5
        time_init, element_init = self.forward_solver()
        self.time_initial = time_init
        t_diff = np.abs(self.measured_travel_time - time_init)
        error = np.sqrt(np.square(t_diff).mean())
        self.iter_all = 0
        self.c = 0
        self.already_optimized_points = pd.DataFrame()
        self.error_all = error
        self.error_history.append(self.error_all)
        while error > self.tol_all and self.iter_all < self.max_iter_all:
            positions = np.arange(len(self.measured_travel_time))
            pos_sorted = positions[np.argsort(t_diff)]
            if len(self.already_optimized_points) > 0:

                self.already_optimized_points['counter'] -=1
                self.already_optimized_points = self.already_optimized_points.loc[self.already_optimized_points['counter']!=0]  # Take out from the exclusion list all those values that finished the resting period

                pos_sorted_after = np.asarray([i for i in pos_sorted if i not in self.already_optimized_points['positions'].to_numpy()])
            else:
                pos_sorted_after = pos_sorted
            if len(pos_sorted_after) == 0:
                logger.error('All positions already optimized. The optimization did not converge')
                return self.fixed_control_points
            pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error

            time_new = self.inversion_position(pos=pos)

            # if self._accepted_position:
            #     counter = -1
            # else:
            #     counter = self.resting_period
            #
            # # [self.already_optimized_points.append(i) for i in np.arange(pos - self.surrounding, pos + self.surrounding + 1)]
            # # df_pos = pd.DataFrame(np.arange(pos - self.surrounding, pos + self.surrounding + 1), columns=['positions'])
            # values_to_exclude = np.arange(pos - self.surrounding, pos + self.surrounding + 1)
            # val = []
            # for i in values_to_exclude:
            #     if i not in self.already_optimized_points['positions'].to_numpy():
            #         val.append((i, counter))
            #     else:
            #         self.already_optimized_points.loc[self.already_optimized_points['positions'] == i, 'counter'] = counter
            # self.already_optimized_points = pd.concat((self.already_optimized_points, pd.DataFrame(np.vstack(val), columns=['positions', 'counter'])))

            self._manage_position_optimized(pos=pos)

            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []
            if len(fix_points) > 0:
                e = np.abs(self.measured_travel_time[fix_points] - time_new[fix_points])
                mask = e > self.tol_pos[fix_points]
                mask_pos = np.argsort(e[mask])
                for p in fix_points[mask_pos]:
                    e = np.abs(self.measured_travel_time[p] - time_new[p])
                    tp = self.tol_pos
                    if isinstance(tp, (list, NDArray)):
                        tp = tp[pos]
                    if e > tp:  # This means that it moved a lot, and it needs to be fixed again.
                        logger.warning(
                            f'Rejected: Position {p} needs to be relocated. Point: {self.fixed_control_points[self.fixed_control_points["pos"] == p]}')
                        self.rejected_points = pd.concat((self.rejected_points, self.fixed_control_points[self.fixed_control_points['pos'] == p]))
                        self.fixed_control_points = self.fixed_control_points[self.fixed_control_points['pos'] != p]
                        time_new = self.inversion_position(pos=p)
                        self._manage_position_optimized(pos=p)

            t_diff = np.abs(self.measured_travel_time - time_new)
            error = np.sqrt(np.square(t_diff).mean())
            self.iter_all += 1
            self.error_all = error
            if self.show_plot:
                self.plot_step()
            # Now we are going to check if the second criteria of convergence applies.
            # Look at all the RMS and look if the value have reached a plateu
            if len(self.error_history) > 10:  # More than 10 data values to start looking if this have converged
                # (self.max_iter_pos*3) means that it will look at the values from the previous 3 control points.
                window_size = self.max_iter_pos
                moving_avg = np.convolve(self.error_history, np.ones(window_size) / window_size, mode='valid')
                plateu = np.std(moving_avg[-self.std_average:])  # Take the last 3 averages and see if we are in a plateu
                # y = np.asarray(self.error_history[-(self.max_iter_pos*2):])  # Look at the slope and check if its horizontal
                # x = np.arange(len(y))
                # slope, _ = np.polyfit(x, y, 1)
                if plateu <= self.std_threshold:
                    logger.info('Second convergence criteria achieved. Model have reached to a local minima')
                    break
        return self.fixed_control_points

    def _plot_step(self):
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.plot(self.time_current, 'blue', label='Simulated')
        ax.plot(self.measured_travel_time, 'g--', label='Real', alpha=0.3)
        ax.plot(self.time_initial, 'r--', label='Starting', alpha=0.3)
        ax.vlines(self.currrent_pos, color='black', ymin=self.ymin, ymax=self.ymax, label='Current position')

        if len(self.already_optimized_points) > 0:
            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []

            ax.vlines(fix_points, color='blue', linestyles='--', ymin=self.ymin, ymax=self.ymax, alpha=0.3,
                          label='Fixed points')
        ax.set_xlabel('Trace no.')
        ax.set_ylabel('Travel Time')
        ax.set_ylim(self.ymin, self.ymax)
        ax.grid()
        handles, labels = ax.get_legend_handles_labels()
        fig.legend(handles, labels)
        ax.set_title(
            f'Position/Iter: {self.currrent_pos}/{self.iter_pos}, Total iter:{self.c}\nRMS position: {self.error_pos:.2f}, RMS all: {self.error_all:.2f}')
        fig.tight_layout()

        if not os.path.isdir(self.directory + os.sep + 'pics'): os.mkdir(self.directory + os.sep + 'pics')

        filename_pic = self.directory + os.sep + 'pics' + os.sep + f'{c}.png'
        self.filenames_pics.append(filename_pic)

        # save frame
        plt.savefig(filename_pic)
        plt.show()

    def plot_step(self):
        fig, ax = plt.subplots(figsize=(15,8))
        j = 0
        xticks = [0]
        sall = []
        colors = {'MB1': 'orange',
                  'MB2': 'green',
                  # 'MB3':(-54,-8),
                  'MB4': 'black',
                  'MB5': 'blue',
                  # 'MB7':(-40, -5),
                  # 'MB8':(12,6),
                  'ST1': 'magenta',
                  'ST2': 'red'}
        for bn in self.boreholes:
            measu_t = self.measured_time_dict[bn]
            simul_t = self.time_current[self.map_antenna[bn]]
            # sall += simul_t.tolist()
            first_arrival_initial = self.time_initial[self.map_antenna[bn]]

            hnd_m, = ax.plot(np.arange(j, j + len(measu_t)), measu_t, color='green', linestyle='--', alpha=0.8, label='Field data')
            hnd_i, = ax.plot(np.arange(j, j + len(first_arrival_initial)), first_arrival_initial, color='red',
                             linestyle='--', alpha=0.8, label='Initial model')
            hnd_s, = ax.plot(np.arange(j, j + len(simul_t)), simul_t, color='blue', linestyle='-', label='Simulated data')
            ax.axvspan(j, j + len(first_arrival_initial), color=colors[bn], alpha=0.1)
            ax.text(j + len(first_arrival_initial) / 4, 80, bn,
                    bbox=dict(edgecolor=colors[bn], linewidth=1, facecolor='white'), fontsize=22)
            j += len(first_arrival_initial)  # + 10
            xticks.append(j)

        optimized_positions = []
        if len(self.already_optimized_points) > 0:
            fix_points = self.fixed_control_points.loc[self.fixed_control_points['pos'].notna(), 'pos'].to_numpy(
                dtype=int) if 'pos' in self.fixed_control_points else []

            hnd_o = ax.vlines(fix_points, color='gray', linestyles=':',
                              #ymin=self.ymin, ymax=self.ymax,
                              ymin=0, ymax=1200,
                              alpha=0.9,
                      label='Optimized CP')
            optimized_positions = fix_points.tolist()

        hnd_c = ax.vlines(self.currrent_pos, color='orange',
                          #ymin=self.ymin, ymax=self.ymax,
                          ymin=0, ymax=1200,
                          linestyles=':', label='CP current location')
        optimized_positions += [self.currrent_pos]
        from matplotlib.patches import Ellipse
        circle1 = Ellipse((self.currrent_pos, self.time_current[self.currrent_pos]), 20,30, zorder=10000, color='orange', linewidth=3, fill=False)
        ax.add_patch(circle1, )

        ax1 = ax.twiny()
        ax1.set_xlabel('Optimized Source-Receiver pair (ID number)', color='blue')
        start, end = 0, len(self.measured_travel_time)
        ax1.set_xlim(start, end)
        ax1.set_xticks(optimized_positions)
        # ax.grid()
        ax.set_xlabel('Source-Receiver pair (ID number)')
        ax.set_ylabel('Two way travel time (ns)')
        ax.set_xlim(start, end)
        ax.set_ylim(0, 1200)
        ax.invert_yaxis()
        ax.set_xticks(xticks, )
        # handles, labels = ax.get_legend_handles_labels()
        # ax.legend(handles, labels, loc='lower right',
        #           frameon=True)
        handles = [hnd_m, hnd_i, hnd_s, hnd_c, hnd_o] if len(self.already_optimized_points) > 0 else [hnd_m, hnd_i, hnd_s, hnd_c]
        labels = ['Field data', 'Initial model', 'Simulated data', 'Current location CP', 'Optimized CP'] if len(self.already_optimized_points) > 0 else ['Field data', 'Initial model', 'Simulated data', 'CP current location']
        ax.legend(handles,
                  labels,
                  loc='lower right',
                  frameon=True,
                  fontsize=20)
        ax.tick_params(axis='both', which='both', direction='out', length=5)
        ax1.tick_params(axis='both', which='both', direction='out', length=5, colors='blue', labelrotation=45)
        ax1.spines["top"].set_edgecolor('blue')
        ax.set_title(f'Position: {self.currrent_pos} | (RMS position={self.error_pos:.2f} ns; RMS all= {self.error_all:.2f} ns)  | '
                     f'(Iter. position={self.iter_pos}; Global iter.={self.c})\n', fontsize=24)

        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels() +
                     [ax1.xaxis.label] + ax1.get_xticklabels() ):
            item.set_fontsize(24)
        fig.tight_layout()

        if not os.path.isdir(self.directory + 'pics'): os.mkdir(self.directory + 'pics')
        #
        filename_pic = self.directory + 'pics' + os.sep + f'{self.c}.png'
        self.filenames_pics.append(filename_pic)
        #
        # # save frame
        fig.tight_layout()
        fig.savefig(filename_pic)
        fig.savefig(filename_pic.split('.')[0]+'.pdf')
        fig.show()

    def _manage_position_optimized(self, pos):
        """

        Args:
            pos:

        Returns:

        """
        if self._accepted_position:
            counter = -1
        else:
            counter = self.resting_period

        # [self.already_optimized_points.append(i) for i in np.arange(pos - self.surrounding, pos + self.surrounding + 1)]
        # df_pos = pd.DataFrame(np.arange(pos - self.surrounding, pos + self.surrounding + 1), columns=['positions'])
        values_to_exclude = np.arange(pos - self.surrounding, pos + self.surrounding + 1)
        if len(self.already_optimized_points) == 0:
            self.already_optimized_points['positions'] = values_to_exclude
            self.already_optimized_points['counter'] = counter
        else:

            val = []
            for i in values_to_exclude:
                if i not in self.already_optimized_points['positions'].to_numpy():
                    val.append((i, counter))
                else:
                    self.already_optimized_points.loc[self.already_optimized_points['positions'] == i, 'counter'] = counter
            if len(val) > 0:
                self.already_optimized_points = pd.concat(
                    (self.already_optimized_points, pd.DataFrame(np.asarray(val), columns=['positions', 'counter'])))
