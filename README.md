[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7898534.svg)](https://doi.org/10.5281/zenodo.7898534)
# FracWave
FracWave, short for Fracture Wave, is an open-source software package designed to provide fast and accurate 
electromagnetic simulations of Ground-Penetrating Radar (GPR) signals, using our module FracEM. The primary objective 
of FracWave is to enable the characterization of fracture and fault geometries within homogeneous mediums such as granitic rock.

Features
--------

FracWave offers the following key features:

1. Fast Forward Electromagnetic Simulation: FracEM provides efficient and precise simulations of GPR signals. 
By accurately modeling the interaction of electromagnetic waves with fractures and faults, FracEM enables the 
extraction of valuable information from the reflected phases.
2. Characterization of Fractures and Faults: By utilizing FracWave, users can analyze the geometry of fractures and 
faults within a homogeneous medium. The software leverages the wealth of information offered by GPR reflections, 
providing valuable insights for various applications.

Getting Started
---------------

To start using FracWave, follow these steps:
* We rely completely on several functionalities of the 
[Bedretto General Model](https://gitlab.com/pygp/general-model), including [Gempy](https://www.gempy.org/installation) 
for the geometric computations.
Please follow the instructions in the link [here](https://gitlab.com/pygp/general-model#installation) to 
install the python environment of the Bedretto General Model. 

For a quick start, you can use the following commands 
to install a python environment with the starting dependencies of Gempy and Bedretto. 
(if you run into any installation problems please refer to the installation instructions in the links above):
```bash
conda create -n fracwave python=3.10
conda activate fracwave
pip install gempy --pre
```
<div class="alert alert-block alert-warning"> 
You may skip this step if you are not interested in using Gempy and only want to use FracWave.
</div>

1. **Installation:** Clone the FracWave repository from GitHub to your local machine.
```bash
git clone https://gitlab.com/pygp/fracwave.git
cd fracwave
```
2. **Dependencies:** Ensure that you have the required dependencies installed. Check the requirements.txt file 
included in the repository for the specific Python packages needed. Use a package manager such as pip to install the 
dependencies. To be able to run the code, you need to have a local installation of FracWave in your environment. 
So please run 'python3 setup.py install' in the root directory of the repository. Or:
```bash
pip install -e .
```
Additionally, if you have a GPU, you can install CuPy for faster computations. 
You may follow the installation instructions [here](https://docs.cupy.dev/en/stable/install.html) according to 
your CUDA Toolkit version.

3. **Usage:** To be able to run the code, you need to have a local installation of FracWave in your environment. 
So please run 'python3 setup.py install' in the root directory of the repository. Or:
```bash
pip install jupyterlab notebook
conda install ipykernel
python -m ipykernel install --user --name fracwave --display-name="FracWave"
```
4. To get started, please refer to the tutorial notebooks [here](/notebooks/tutorials). 
Keep in mind that you need to install Jupyter notebook to run the notebooks. If your don't know how to install Jupyter  
you can follow the instructions [here](https://jupyter.org/install) or follow this commands:
```bash
pip install jupyterlab notebook
conda install ipykernel
python -m ipykernel install --user --name fracwave --display-name="FracWave"
```

Project Development
-------------------

FracWave is developed as part of the PhD project of Daniel Escallón under the supervision of Dr. Alexis Shakas at the
[Bedretto Underground Laboratory for Geosciences and Geoenergies (BULGG)](http://www.bedrettolab.ethz.ch/publications/publications/)
with the project acronym "SNF - EGS (Multi PhD)" at ETH Zürich University, Switzerland.

### Maintainers

* Daniel Escallón (daniel.escallon@erdw.ethz.ch)
* Alexis Shakas (alexis.shakas@erdw.ethz.ch)

 
License, use and attribution
----------------------------

Feel free to download and use the Bedretto general model in your work! We do not provide any
warranty and any guarantee for the use, but we aim to help and answer questions posted as Issues on the repository 
page as quickly as possible.

The model is published under an **GNU Lesser General Public License v3.0**, which
means that you are free to use it. If you do any modifications, especially for scientific and educational use,
then please _provide them back to the main project_ in the form of a pull request.

If you have questions on the procedure, feel free to contact us about it.


### CITE AS:

Escallon, Daniel, Shakas, Alexis, & Maurer, Hansruedi. (2023). FracWave (v0.1-pre). Zenodo. https://doi.org/10.5281/zenodo.7898534

```bibtex
@software{escallon_daniel_2023_7898534,
  author       = {Escallon, Daniel and
                  Shakas, Alexis and
                  Maurer, Hansruedi},
  title        = {FracWave},
  month        = sep,
  year         = 2023,
  publisher    = {Zenodo},
  version      = {v0.1-pre},
  doi          = {10.5281/zenodo.7898534},
  url          = {https://doi.org/10.5281/zenodo.7898534}
}
```

Where have been the software used?
-----------------------------------
The following publications have used the software:

1. Escallon, D., Shakas, A., & Maurer, H. (2024). Modelling and inferring fracture curvature from borehole GPR data: 
A case study from the Bedretto Laboratory, Switzerland. Near Surface Geophysics, 
22(2), 235–254. https://doi.org/10.1002/nsg.12286
   (The scripts to recreate the figures are available in the script folder [manuscript](scripts/fracture_curvature)).
2. Escallon, D., Shakas, A., Maurer, H., & Madonna, C. (2024). Extracting Spatial Correlation of Fracture Aperture 
Fields from Ground Penetrating Radar. Unpublished manuscript.
   (The scripts to recreate the figures are available in the script folder [manuscript2](scripts/fracture_aperture)).
