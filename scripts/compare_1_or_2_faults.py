import os, sys
import numpy as np
from fracwave import DATA_DIR, set_logger, OUTPUT_DIR
import pyvista as pv

from bedretto import data, geometry, model

from fracwave import FractureGeo, Antenna

file =OUTPUT_DIR + 'branch_fault.h5'
#%%
geom1 = pv.read(DATA_DIR + 'Faults-full_BB_boreholes.vtk')
geom2 = pv.read(DATA_DIR + 'Faults-full_BB_tunnel.vtk')
geom3 = pv.read(DATA_DIR + 'Faults-joined_inner.vtk')
geom4 = pv.read(DATA_DIR + 'Faults-joined_outer.vtk')

#%%
# Geometry
# ----------
bor_dat = data.BoreholeData()
tun_dat = data.TunnelData()

# Borehole
# -----------
bor_geom = geometry.BoreholeGeometry(bor_dat)
mb1 = bor_dat.get_borehole_data('MB1')
mb1_geometry = bor_geom.construct_borehole_geometry(borehole_info=mb1, radius=0.2)

tn = data.TunnelData()
tn_geom = geometry.TunnelGeometry(tn)
surf = tn_geom.get_tunnel_mesh([1940,2100])
#%%
p = model.ModelPlot()
#%%
p.add_object(mb1_geometry, name='mb1', opacity=1, color='black', label='mb1')
p.add_object(surf, name='tunnel', opacity=1, color='gray', label='tunnel')
# p.add_object(geom1, name='fault1', color='yellow')
# p.add_object(geom2, name='fault2', color='blue')
# p.add_object(geom3, name='fault3', color='green')
# p.add_object(geom4, name='fault4', color='red')
# p.add_object(geom2, name='fault2', color='blue')
#%% Geom 3 and 4 are the correct ones
bounds = [-200, 50, -100, 50, 1300, 1510]
clipped1 = geom1.clip_box(bounds=bounds, invert=False)
clipped2 = geom2.clip_box(bounds=bounds, invert=False)
clipped3 = geom3.clip_box(bounds=bounds, invert=False)
clipped4 = geom4.clip_box(bounds=bounds, invert=False)

#%%
wavelength = 6
frac = FractureGeo()

#%%
spacing=(200,200)
new_1 = frac.remesh(clipped1.points, plane=1, # projection onto plane yz
                    spacing=spacing)
_ = frac.check_minimum_area(wavelength=wavelength)
new_2 = frac.remesh(clipped2.points, plane=1, # projection onto plane yz
                    spacing=spacing)
_ = frac.check_minimum_area(wavelength=wavelength)
#%%
p.add_object(new_1, name='fault1', color='green')
p.add_object(new_2, name='fault2', color='red')
# p.add_object(clipped4, name='fault4', color='red')

#%%
frac.extract_vertices_and_faces_from_pyvista_mesh(new_1)

df = frac.set_fracture_properties(aperture=0.5,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)
#%%
frac2 = FractureGeo()
frac2.extract_vertices_and_faces_from_pyvista_mesh(new_2)

df2 = frac2.set_fracture_properties(aperture=0.5,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)

#%%
frac_all = frac + frac2

frac_all.export_to_hdf5(file, overwrite=True)

#%%
frac_all = FractureGeo()

frac_all.load_hdf5(file)

p.add_object(frac_all.surface, name='faults', color='yellow', opacity=0.8)

#%% # Geometry
# ----------
bor_dat = data.BoreholeData()
tun_dat = data.TunnelData()

# Borehole
# -----------
bor_geom = geometry.BoreholeGeometry(bor_dat)
mb1 = bor_dat.get_borehole_data('MB1')
mb1_geometry = bor_geom.construct_borehole_geometry(borehole_info=mb1, radius=0.2)

#if show:
#    plotter.add_mesh(mb1_geometry, color='black')

# Coordinates of dipoles
traces = 100
separation = 2.77
# Rx = 100
# Tx = Rx + separation

Rx = np.linspace(5, 200-separation, traces)
Tx = np.linspace(5 + separation, 200, traces)
#xyz_rx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Rx).T
#xyz_tx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Tx).T

xyz_rx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Rx).T
xyz_tx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Tx).T

# Orientation dipole
# -----------
# Calculate rx orientation by giving 10 cm before and 10 cm after
rx_1 = Rx - 0.1
rx_2 = Rx + 0.1

orient_rx = (bor_dat.get_coordinates_from_borehole_depth(mb1, depths=rx_1).T - \
             bor_dat.get_coordinates_from_borehole_depth(mb1, depths=rx_2).T)
#orient_rx /= np.linalg.norm(orient_rx, ord=1, axis=1)[:, np.newaxis]
orient_rx /= np.linalg.norm(orient_rx, ord=1)

# Calculate tx orientation by giving 10 cm before and 10 cm after
tx_1 = Tx - 0.1
tx_2 = Tx + 0.1

orient_tx = (bor_dat.get_coordinates_from_borehole_depth(mb1, depths=tx_1).T - \
             bor_dat.get_coordinates_from_borehole_depth(mb1, depths=tx_2).T)
#orient_tx /= np.linalg.norm(orient_tx, ord=1, axis=1)[:, np.newaxis]
orient_tx /= np.linalg.norm(orient_tx, ord=1)

ant = Antenna()
ant.Rx = xyz_rx
ant.Tx = xyz_tx
ant.orient_Rx = orient_rx
ant.orient_Tx = orient_tx

ant.export_to_hdf5(file, overwrite=True)

ant.plot(p=p.plotter, backend='pyvista', arrows=False)

#%%
ant = Antenna()
ant.load_hdf5(file)

#%%
import matplotlib.pyplot as plt
from fracwave import SourceEM

samples = 300
sou = SourceEM()
f = np.linspace(0, 0.1, samples)
sou.frequency = f

firstp = sou.create_source(a=1,
                           b=0.02,
                           t=5)
sou.plot()
plt.xlim(0,0.1)
plt.show()
sou.export_to_hdf5(file, overwrite=True)

#%% Simulation

from fracwave import FracEM

solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.engine = 'tensor_all'
solv.engine = 'tensor'
solv.units = 'SI'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv.backend = 'torch'

solv.open_file(file)

#%%
freq = solv.forward_pass(overwrite=True, recalculate=False, save_hd5=False)