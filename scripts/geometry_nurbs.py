import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')

x=np.linspace(-10,10, num=100)
y=np.linspace(-10,10, num=100)

x, y = np.meshgrid(x, y)

z = np.exp(-0.1*x**2-0.1*y**2)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(x,y,z, cmap=cm.jet)
plt.show()

#%%
from geomdl import CPGen
from geomdl import BSpline
from geomdl import utilities
from geomdl.visualization import VisMPL
from matplotlib import cm

# Generate a plane with the dimensions 50x100
surfgrid = CPGen.Grid(50, 100)

# Generate a grid of 25x30
surfgrid.generate(50, 60)

# Generate bumps on the grid
surfgrid.bumps(num_bumps=5, bump_height=20, base_extent=8)

# Create a BSpline surface instance
surf = BSpline.Surface()

# Set degrees
surf.degree_u = 3
surf.degree_v = 3

# Get the control points from the generated grid
surf.ctrlpts2d = surfgrid.grid

# Set knot vectors
surf.knotvector_u = utilities.generate_knot_vector(surf.degree_u, surf.ctrlpts_size_u)
surf.knotvector_v = utilities.generate_knot_vector(surf.degree_v, surf.ctrlpts_size_v)

# Set sample size
surf.sample_size = 100

# Set visualization component
surf.vis = VisMPL.VisSurface(ctrlpts=False, legend=False)

# Plot the surface
surf.render(colormap=cm.terrain)

#%%
from geomdl import BSpline
from geomdl.visualization import VisMPL

# Control points
ctrlpts = [
    [[-25.0, -25.0, -10.0], [-25.0, -15.0, -5.0], [-25.0, -5.0, 0.0], [-25.0, 5.0, 0.0], [-25.0, 15.0, -5.0], [-25.0, 25.0, -10.0]],
    [[-15.0, -25.0, -8.0], [-15.0, -15.0, -4.0], [-15.0, -5.0, -4.0], [-15.0, 5.0, -4.0], [-15.0, 15.0, -4.0], [-15.0, 25.0, -8.0]],
    [[-5.0, -25.0, -5.0], [-5.0, -15.0, -3.0], [-5.0, -5.0, -8.0], [-5.0, 5.0, -8.0], [-5.0, 15.0, -3.0], [-5.0, 25.0, -5.0]],
    [[5.0, -25.0, -3.0], [5.0, -15.0, -2.0], [5.0, -5.0, -8.0], [5.0, 5.0, -8.0], [5.0, 15.0, -2.0], [5.0, 25.0, -3.0]],
    [[15.0, -25.0, -8.0], [15.0, -15.0, -4.0], [15.0, -5.0, -4.0], [15.0, 5.0, -4.0], [15.0, 15.0, -4.0], [15.0, 25.0, -8.0]],
    [[25.0, -25.0, -10.0], [25.0, -15.0, -5.0], [25.0, -5.0, 2.0], [25.0, 5.0, 2.0], [25.0, 15.0, -5.0], [25.0, 25.0, -10.0]]
]

# Create a BSpline surface
surf = BSpline.Surface()

# Set degrees
surf.degree_u = 3
surf.degree_v = 3

# Set control points
surf.ctrlpts2d = ctrlpts

# Set knot vectors
surf.knotvector_u = [0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 3.0, 3.0, 3.0, 3.0]
surf.knotvector_v = [0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 3.0, 3.0, 3.0, 3.0]

# Set evaluation delta
surf.delta = 0.1

# Evaluate surface points
surf.evaluate()

# Import and use Matplotlib's colormaps
from matplotlib import cm

# Plot the control points grid and the evaluated surface
surf.vis = VisMPL.VisSurface()
surf.render(colormap=cm.cool)

#%%
from geomdl import fitting

degree = 3
xyz = np.asarray([x.ravel(), y.ravel(), z.ravel()]).T
a = fitting.approximate_surface(xyz[::20], size_u = 10, size_v = 10, degree_u = degree, degree_v = degree, centripetal=True)
a.evaluate()

#%%
# Import and use Matplotlib's colormaps\
from geomdl.visualization import VisMPL
from matplotlib import cm

# Plot the control points grid and the evaluated surface
a.vis = VisMPL.VisSurface()
a.render(colormap=cm.cool)

#%%
from scipy.interpolate import bisplrep, bisplev

tck = bisplrep(x.ravel(), y.ravel(), z.ravel(), )
vals = np.asarray([bisplev(xp, yp, tck) for xp, yp in np.asarray([x.ravel(), y.ravel()]).T])

#%%
import pyvista as pv
from bedretto import model

cptn1 = pv.PolyData(xyz)
cptn2 = pv.PolyData(np.asarray([x.ravel(), y.ravel(), vals]).T)
#poly = pv.PolyData(np.asarray(a.ctrlpts))
#poly = poly.triangulate(progress_bar=True)

p = model.ModelPlot()

#p.add_object(poly, name='points', color='yellow')
p.add_object(cptn1, name='ctpts1', color='red')
p.add_object(cptn2, name='ctpts2', color='blue')

#%%
from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR, DATA_DIR
from bedretto import data, geometry, model
import pyvista as pv
import numpy as np

#file = '/home/daniel/GitProjects/geophysical-forward-modeling/output/cluster_results/output/MB1_BB_20MHz_more_elements.h5'
#geom = FractureGeo()
#geom.load_hdf5(file)

surf = pv.read(DATA_DIR + 'bb_surf_boreholes_and_tunnel.vtk')

#%%
p = model.ModelPlot(title='BadBoy')

#%%
#s = surf.extract_feature_edges()

p.add_object(surf, name='bb', color='blue')


#%%

def sort_by_location(positions: list or np.ndarray):
    """

    Args:
        positions

    Returns:

    """
    sorted_position = []
    edges = np.asarray(positions).copy()

    from scipy.spatial.distance import cdist
    p1 = edges[0]
    sorted_position.append(p1)
    edges = np.delete(edges, 0, axis=0)

    while len(edges):
        m = cdist([p1], edges)[0]
        mask = np.argmin(m)
        p1 = edges[mask]
        sorted_position.append(p1)
        edges = np.delete(edges, mask, axis=0)
    return np.asarray(sorted_position)

def extract_edges(surf: pv.PolyData):
    """

    Args:
        surf:

    Returns:

    """
    vertices = np.asarray(surf.points).copy()
    s = surf.extract_feature_edges()
    edges = sort_by_location(s.points)
    from scipy.spatial.distance import cdist
    for e in edges:
        m = cdist([e], vertices)[0]
        mask = np.argmin(m)
        vertices = np.delete(vertices, mask, axis=0)
    return edges, vertices

def export_to_gmsh(surf: pv.PolyData, size=1):
    """

    Args:
        surf:

    Returns:

    """
    import pygmsh
    edges, inside = extract_edges(surf)
        #with pygmsh.occ.Geometry() as geom:
    geom = pygmsh.opencascade.Geometry()
    # Exclude from the vertices the edges
    #geom = pygmsh.occ.Geometry()

    points_edges = [geom.add_point(pos, size) for pos in edges]
    points_inside = [geom.add_point(pos, size) for pos in inside]
    line_edge = [geom.add_line(points_edges[i], points_edges[i+1]) for i in range(len(points_edges)-1)]
    line_edge.append(geom.add_line(points_edges[-1], points_edges[0]))
    #geom.add_polygon(edges, mesh_size=size)
    loop = geom.add_line_loop(line_edge)
    sur = geom.add_surface(loop)
    embeded = geom.embed()
    #pol = geom.add_polygon(edges, lcar=size)
    script = geom.get_code()
    with open(OUTPUT_DIR + 'geometry_generation.geo', 'w+') as f:
        f.write(script)
    mesh = pygmsh.generate_mesh(geom)
    return mesh


#%%
# from fracwave import OUTPUT_DIR
# mesh.write(OUTPUT_DIR + 'out.vtk')
#
# import pyvista as pv
# from bedretto import model
# #surf = pv.read(OUTPUT_DIR + 'out.vtk')
# p = model.ModelPlot()
# p.add_object(edges, 'mesh1', color='red')
# p.add_object(inside, 'mesh2', color='blue')

#%%
import scipy.interpolate as interp

interpolator = interp.CloughTocher2DInterpolator(np.c_[surf.points[:,0], surf.points[:,2]], surf.points[:, 1])

#%%
space =100
x = np.linspace(surf.points[:,0].min(), surf.points[:,0].max(), space)
z = np.linspace(surf.points[:,2].min(), surf.points[:,2].max(), space)
xx, zz = np.meshgrid(x,z)
#%%
yy = interpolator(xx, zz)
#a = pv.PolyData(np.asarray([surf.points[:,0], surf.points[:,1], z]).T)
#%%
grid = pv.StructuredGrid(xx, yy, zz)
p.add_object(grid, 'grid', color='yellow')
#%%
a.save(DATA_DIR + 'FZ1_2.vtk')
#%%
import meshio
mesh = meshio.read(DATA_DIR + 'FZ1_2.vtk', file_format='vtk')
#%%
mesh.write(OUTPUT_DIR + 'out.msh')

#%%
def move_surface(point:tuple, surf: pv.PolyData, direction: tuple or list, radius: float, magnitude: float):
    """

    Args:
        point: xyz coordinate to move that lays in the surface
        surf: Pyvista mesh
        direction:
        radius:

    Returns:

    """
    from scipy.spatial.distance import cdist
    def sigmoid(x, a, b, c, d):
        """ General sigmoid function
        a adjusts amplitude
        b adjusts y offset
        c adjusts x offset
        d adjusts slope """
        y = ((a - b) / (1 + np.exp(x - (c / 2)) ** d)) + b
        return y

    m = cdist([point], surf.points)[0]
    maskd = m < radius

    movement = sigmoid(m[maskd], a=magnitude, b=0, c=radius, d=0.3)

    direction = np.asarray(direction)
    direction = np.divide(direction, np.linalg.norm(direction))
    move = direction[None] * movement[:,None]

    surf.points[maskd] += move
    return surf

#%%
new_surf = move_surface(grid.points[2789],
                        surf = grid,
                        direction=(1,0,1),
                        radius=20,
                        magnitude=20)
#%%


#
# vertices = np.asarray(surf.points).copy()
# s = surf.extract_feature_edges()
# edges = sort_by_location(s.points)
# from scipy.spatial.distance import cdist
# for e in edges:
#     m = cdist([e], vertices)[0]
#     mask = np.argmin(m)
#     vertices = np.delete(vertices, mask, axis=0)
#
#     y = sigmoid(x, a=magnitude, b=0, c=radius, d=0.1)
#     return edges, vertices


#%%
from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR, DATA_DIR
from bedretto import data, geometry, model
import pyvista as pv
import numpy as np

#file = '/home/daniel/GitProjects/geophysical-forward-modeling/output/cluster_results/output/MB1_BB_20MHz_more_elements.h5'
#geom = FractureGeo()
#geom.load_hdf5(file)

surf = pv.read(DATA_DIR + 'bb_surf_boreholes_and_tunnel.vtk')

#%%
p = model.ModelPlot(title='BadBoy')
#%%
geom = FractureGeo()
geom.remesh(surf.points, plane=1, space =100)
#%%
stream = pv.convert_array(mesh.GetFaces())
locs = pv.convert_array(mesh.GetFaceLocations())

cells = []
for i in range(len(locs)):
    if i >= len(locs) - 1:
        stop = None
    else:
        stop = locs[i+1]
    cells.append(stream[locs[i]:stop])
len(cells)


faces = []
for cell in cells:
    cell_faces = []
    i, offset = 0, 1
    nf = cell[0]
    while i < nf:
        nn = cell[offset]
        cell_faces.append(cell[offset+1:offset+1+nn])
        offset += nn + 1
        i += 1
    faces.append(cell_faces)