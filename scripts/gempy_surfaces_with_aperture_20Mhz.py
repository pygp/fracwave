from bedretto import data, geometry, DATA_DIR, model
from bedretto.core.experiments import probabilities
import pandas as pd
import os
import numpy as np
from fracwave import OUTPUT_DIR, Antenna, SourceEM, FractureGeo, FracEM
import pyvista as pv
import matplotlib.pyplot as plt

# file = OUTPUT_DIR + 'full_model/full_model.h5'
file = OUTPUT_DIR + 'full_model_20MHz/full_model_mean_20MHz.h5'
file2 = OUTPUT_DIR + 'full_model_20MHz/full_model_variable_20MHz.h5'
file3 = OUTPUT_DIR + 'full_model_20MHz/full_model_random_20MHz.h5'
file4 = OUTPUT_DIR + 'full_model_20MHz/full_model_random2_20MHz.h5'
# %%
sou = SourceEM()
try:
    sou.load_hdf5(file)
except:
    sou.frequency = np.linspace(0, 0.2, 500)
    firstp = sou.create_source(a=1,
                               b=0.02,
                               g=2.0,
                               ph=0,
                               t=-75)
    sou.export_to_hdf5(file, overwrite=True)
    sou.export_to_hdf5(file2, overwrite=True)
    sou.export_to_hdf5(file3, overwrite=True)
    sou.export_to_hdf5(file4, overwrite=True)
ax = sou.plot()
# ax.plot(f, source)
ax.figure.show()
# %%
# Load data
borehole_data = data.BoreholeData()
bor_geom = geometry.BoreholeGeometry(borehole_data=borehole_data)
# boreholes =  borehole_data.boreholes_MB + borehole_data.boreholes_ST
boreholes = ['MB1']
bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_name=bn, data_type='KDE-ML') for bn in boreholes}

tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1984, 2070])

# %%
p = model.ModelPlot()
# %%
[p.add_object(bn, name=key, color='black') for key, bn in bh_geom_all.items()]
p.add_object(tunnel_mesh, name='tunnel', color='gray', opacity=0.2)
# %%
extent = p.bounds
# %%
bb_data = pd.read_csv(DATA_DIR + 'logging_data' + os.sep + 'BB_logging.csv')

# initialize classes
p_samp = probabilities.ProbabilisticSampling(borehole_data)
fg = geometry.fault_geometry.FaultGeometry()

bb_top = bb_data[bb_data['fault_id'] == 'BB-top']
bb_bottom = bb_data[bb_data['fault_id'] == 'BB-bottom']

logging_weights = 1e-3  # this will put a high priority on fitting the points
orientation_weights = 1000  # and a very low (negligible) priority on fitting the orientations

# Split the dataframe into top and bottom surfaces and sample (randomly) the location of the fault
# using borehole trajectory information
logging_df1 = p_samp.sample_logging_structures(structures_df=bb_top,
                                               borehole_dict=borehole_data.boreholes_kde_dict)
logging_df1['surface'] = 'top'
logging_df1['smooth'] = logging_weights / logging_df1['Weight (-)']

logging_df2 = p_samp.sample_logging_structures(structures_df=bb_bottom,
                                               borehole_dict=borehole_data.boreholes_kde_dict)
logging_df2['surface'] = 'bottom'
logging_df2['smooth'] = logging_weights / logging_df2['Weight (-)']
logging_df = pd.concat((logging_df1, logging_df2), ignore_index=True)

# create an input dataframe and feed it to gempy
input_data = {'logging': logging_df}
fields = ['logging']
surf_points_df, orientations_df = fg.create_gempy_df_from_dict(input_data, fields)
orientations_df['smooth'] = orientation_weights

bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
surf_points_df = pd.concat([surf_points_df, pd.DataFrame.from_dict({'X': [bb['Center'][0][0]],
                                                                    'Y': [bb['Center'][0][1]],
                                                                    'Z': [bb['Center'][0][2]],
                                                                    'surface': 'top',
                                                                    'smooth': 0.001})], ignore_index=True)
from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip

pole = get_normal_vector_from_azimuth_and_dip(bb['Azimuth (deg)'][0], bb['Dip (deg)'][0])
orientations_df = pd.concat([orientations_df, pd.DataFrame.from_dict({'X': [bb['Center'][0][0]],
                                                                      'Y': [bb['Center'][0][1]],
                                                                      'Z': [bb['Center'][0][2]],
                                                                      'G_x': pole[0],
                                                                      'G_y': pole[1],
                                                                      'G_z': pole[2],

                                                                      'surface': 'top',
                                                                      'smooth': 1000})], ignore_index=True)
# %%
import pyvista as pv

p.add_object(pv.PolyData(surf_points_df.loc[orientations_df['surface'] == 'top', ['X', 'Y', 'Z']].to_numpy()),
             name='top', color='red')
p.add_object(pv.PolyData(surf_points_df.loc[orientations_df['surface'] == 'bottom', ['X', 'Y', 'Z']].to_numpy()),
             name='bottom', color='green')
# %%
# logger.debug(
#     f'Setting {len(fixed_points)} fixed points, {len(fixed_orientations[0])} orientations from borehole '
#     f'logging and tunnel observations')
import gempy as gp

geo_model = gp.create_model()
geo_model = gp.init_data(geo_model, extent=extent, resolution=(50, 50, 50))
gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
geo_model.add_surfaces(['top', 'bottom', 'basement'])

# %%
fixed_points = surf_points_df
fixed_orientations = orientations_df
move_points = None

[geo_model.delete_orientations(i) for i in list(geo_model.orientations.df.index)]
[geo_model.delete_surface_points(i) for i in list(geo_model.surface_points.df.index)]

for i, df in fixed_points.iterrows():
    geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Z'], surface=df['surface'])
geo_model.surface_points.df.smooth = fixed_points.smooth

for i, df in fixed_orientations.iterrows():
    geo_model.add_orientations(X=df['X'], Y=df['Y'], Z=df['Z'],
                               surface=df['surface'],
                               pole_vector=df[['G_x', 'G_y', 'G_z']])
geo_model.orientations.df.smooth = fixed_orientations.smooth
# id_orientation_fixed.append(geo_model.surface_points.df.index[-1])
if move_points is not None:
    # logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
    # if len(id_points_move)>0:
    #     [geo_model.delete_surface_points(i) for i in id_points_move]
    #     id_points_move = []
    # if move_points.ndim == 1:
    #     move_points = move_points[None]
    # for p in move_points:
    #     self.geo_model.add_surface_points(X=p[0], Y=p[1], Z=p[2], surface='F')
    # id_points_move.append(geo_model.surface_points.df.index[-1])
    for i, df in move_points.iterrows():
        geo_model.add_surface_points(X=df['X'], Y=df['Y'], Z=df['Y'], surface=df['surface'])
    geo_model.surface_points.df.smooth = move_points.smooth

gp.compute_model(geo_model)
# logger.debug('Model computed')
vertices = geo_model.solutions.vertices
faces = geo_model.solutions.edges
# return vertices, face

# gp.plot_3d(geo_model, plotter_type='background')
# %%
s = [pv.PolyData(v, np.hstack([np.hstack([len(f), f]) for f in fa])) for v, fa in zip(vertices, faces)]
[surf.save(OUTPUT_DIR + f'full_model/s{i}.vtk') for i, surf in enumerate(s)]
# %% Read
s = [pv.read(OUTPUT_DIR + f'full_model/s{i}.vtk') for i in [0]]
# %%
[p.add_object(pv.PolyData(s[0].points), name='red', color='red')]#, style='wireframe', ) for surf, c in zip(s, ('red', 'green'))]
# %%
surf = s[0]
# %%

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / sou.peak_frequency  # m
max_element_size = wavelength / 6

frac = FractureGeo()
vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(s[0])
vertices2, faces2 = frac.extract_vertices_and_faces_from_pyvista_mesh(s[1])

# %%
# frac.load_hdf5(file)
grid, vert, fac = frac.remesh(vertices, plane=1, max_element_size=max_element_size, extrapolate=False)  # Resolution 145,143
grid2, vert2, fac2 = frac.remesh(vertices2, plane=1, max_element_size=max_element_size, extrapolate=False)  # Resolution
# %%
[surf.save(OUTPUT_DIR + f'full_model_20MHz/s_remeshed_20MHz{i}.vtk') for i, surf in enumerate([grid, grid2])]
# %%
s = [pv.read(OUTPUT_DIR + f'full_model_20MHz/s_remeshed_20MHz{i}.vtk') for i in [0]]
# %%
[p.add_object(surf, name=c, color=c,  style='wireframe') for surf, c in zip(s, ('red'))]#, 'green'))]
w
# [p.add_object(pv.PolyData(v, np.hstack([np.hstack([len(f), f]) for f in fa])), name=c, color=c) for v, fa, c in zip(vertices, faces, ('red', 'green'))]
# %%
# surf1, surf2 = fg.separation_between_surfaces(grid, grid2, method='kdtree')
# separation = surf1['Separation (m)']
# %%
# surf = frac2.surface
# p.add_object(surf, name='mesh', opacity=0.6, scalars='aperture', )
# p.add_object(frac2.surface, name='mesh', opacity=0.6, color='red')
#%% Generate random field
frac.load_hdf5(file)
apertures = frac.generate_heterogeneous_apertures(frac.midpoints,
                                                  b_mean=0.8, seeds=(100, 200), var=0.01,
                                                  len_scale=10, plane=1)

plt.scatter(frac.midpoints[:,0], frac.midpoints[:,2], c=apertures, cmap='viridis')
plt.colorbar()
plt.show()
frac.export_to_hdf5(file4, overwrite=True)
# %%
df1 = frac.set_fracture(name_fracture='Frac1', vertices=vert, faces=fac,
                         aperture=apertures,
                         electrical_conductivity=0, electrical_permeability=81,
                        overwrite=True)


frac2 = FractureGeo()
df2 = frac2.set_fracture(name_fracture='Frac1', vertices=vert2, faces=fac2,
                         aperture=0,
                         electrical_conductivity=0, electrical_permeability=81)

from scipy.spatial import KDTree

tree = KDTree(df1[['x', 'y', 'z']].to_numpy())
d_kdtree, idx = tree.query(df2[['x', 'y', 'z']].to_numpy())

frac._fractures['aperture'] = d_kdtree.mean()
frac2._fractures['aperture'] = d_kdtree

# file = OUTPUT_DIR + 'full_model/full_model_mean.h5'
frac.export_to_hdf5(file, overwrite=True)
# file2 = OUTPUT_DIR + 'full_model/full_model_variable.h5'
frac2.export_to_hdf5(file2, overwrite=True)

# %%
frac.load_hdf5(file3)
surf = frac.surface
p.add_object(surf, name='mesh', scalars='aperture')
# %%
viewup = [0.5, 0.5, 1]
path = p.plotter.generate_orbital_path(n_points=100, shift=tunnel_mesh.length/10)
p.plotter.open_gif(OUTPUT_DIR + "gifs/20MHz_aperture.gif")
p.plotter.orbit_on_path(path, write_frames=True)
#%%
frac2.check_minimum_area(wavelength)
# %% Antenna positions
ant = Antenna()
boreholes = ['MB1']
try:
    ant.load_hdf5(file)
except:
    from bedretto import data

    bor_dat = data.BoreholeData()

    traces_per_borehole = 1000
    for b in boreholes:
        bor = bor_dat.get_borehole_data(b, data_type='KDE-ML')
        end = bor_dat.get_borehole_end_coordinates(b)
        tx = np.linspace(10, end['Depth (m)'], traces_per_borehole)
        # tx = np.asarray([50])
        rx = tx - 2.77
        xyz_tx = bor_dat.get_coordinates_from_borehole_depth(bor, tx).T
        xyz_tx1 = bor_dat.get_coordinates_from_borehole_depth(bor, tx - 0.1).T
        xyz_rx = bor_dat.get_coordinates_from_borehole_depth(bor, rx).T
        xyz_rx1 = bor_dat.get_coordinates_from_borehole_depth(bor, rx - 0.1).T

        orient_tx = (xyz_tx1 - xyz_tx) / np.linalg.norm((xyz_tx1 - xyz_tx), ord=1, axis=1)[:, None]
        orient_rx = (xyz_rx1 - xyz_rx) / np.linalg.norm((xyz_rx1 - xyz_rx), ord=1, axis=1)[:, None]
        ant.set_profile(name_profile=b,
                        receivers=xyz_tx,
                        transmitters=xyz_rx,
                        orient_receivers=orient_rx,
                        orient_transmitters=orient_tx
                        )

    ant.export_to_hdf5(file, overwrite=True)
    ant.export_to_hdf5(file2, overwrite=True)
    ant.export_to_hdf5(file3, overwrite=True)
    ant.export_to_hdf5(file4, overwrite=True)
# %%
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')

n_arrows = 20
Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in np.linspace(1, len(ant.Transmitter) - 1, n_arrows, dtype=int):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=1)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=1)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')

# %%
solv = FracEM()

solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
# solv.engine = 'dummy'
# solv.units = 'SI'
solv.backend = 'torch'
# solv.backend = 'numpy'
solv.__filter_energy = 0.01
solv.open_file(file4)
solv.load_hdf5(OUTPUT_DIR + 'full_model/full_model_variable.h5')
freq_t0 = solv.time_zero_correction()

# %%
freq = solv.forward_pass(overwrite=False, recalculate=False, save_hd5=False, )

# %%
bor_dat = data.BoreholeData()
df = bor_dat.get_borehole_data('MB1', data_type='KDE-ML')
end = df['Depth (m)'][-1]

#%%
solv = FracEM()
solv.load_hdf5(file)
freq = solv.file_read('summed_response')
time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
depth_vector = np.linspace(10, end, time_response.shape[0])
depth_vector -= 2.77*0.5
solv.load_hdf5(file3)
freq2 = solv.file_read('summed_response')
time_response2, time_vector2 = solv.get_ifft(freq2)#, pad=1000)
# depth_vector2 = np.linspace(10, end, time_response2.shape[0])
# %%
xlim = (16.5, 220)
ylim=(60,0)
ax = solv.plot(time_response, time_vector, depth_vector, xlim=xlim, ylim=ylim, clim=np.abs(time_response).max()/100)
ax2 = solv.plot(time_response2, time_vector2, depth_vector, xlim=xlim, ylim=ylim, clim=np.abs(time_response2).max()/100)

#%%
import os
from gdp import DATA_DIR as dd
import scipy.io as io
filename = dd + 'VALTER' + os.sep + 'MB1_20MHz_B200108.mat'

mat = io.loadmat(filename)
depth = mat['depth'][:, 0]
processed_data = mat['processed_data']
# raw_dat = mat['raw_data']
travel_time = mat['travel_time'][0]
# radius = mat['radius'][0]
# Get rid of negative travel times
mask = travel_time >= 0
processed_data = processed_data[mask]
travel_time = travel_time[mask]

#%%
ax3 = solv.plot(processed_data.T, travel_time, depth, xlim=xlim, ylim=ylim,  clim=np.abs(processed_data).max()/5)

#%%
xlim = (16.5, 220)
ylim=(60,0)
ax = solv.plot(time_response, time_vector, depth_vector, xlim=xlim, ylim=ylim)
# ax2 = solv.plot(time_response2, time_vector2, depth_vector2, xlim=xlim, ylim=ylim)
ax3 = solv.plot(processed_data.T, travel_time, depth, xlim=xlim, ylim=ylim)

#%%
from gdp.processing.gain import apply_gain
gain, _ = apply_gain(time_response.T, gain_type = 'spherical', velocity=solv.velocity, sfreq=solv.file_read_attribute('sampling_frequency'))
ax = solv.plot(gain.T, time_vector, depth_vector, xlim=xlim, ylim=ylim, clim=np.abs(gain).max()/5)

gain, _ = apply_gain(time_response2.T, gain_type = 'spherical', velocity=solv.velocity, sfreq=solv.file_read_attribute('sampling_frequency'))
ax = solv.plot(gain.T, time_vector2, depth_vector, xlim=xlim, ylim=ylim, clim=np.abs(gain).max()/5)

sf = (1/ (travel_time[1]-travel_time[0]))/2
gain_real, _ = apply_gain(processed_data, gain_type = 'spherical', velocity=solv.velocity, sfreq=sf)
ax2 = solv.plot(gain_real.T, travel_time, depth, xlim=xlim, ylim=ylim, clim=np.abs(gain_real).max()/100)
# %%
plt.plot(time_response[500]);
plt.show()
# %%
plt.plot(time_response[500]);
plt.show()

# %%
frac = FractureGeo()
frac.load_hdf5(file)

#%%
p = model.ModelPlot()
#%%
frac = FractureGeo()
frac.load_hdf5(file)
surf = frac.surface


#%%
fracture_field = solv.file_read('fracture_field')
#%%
surf['fracture_field'] = fracture_field[600]
p.add_object(surf, name='mesh', scalars='fracture_field')
p.add_object(surf, name='mesh', scalars='aperture')

#%%
import gstools as gs

seeds=(100, 200)
b_mean = 0.3

seed = gs.random.MasterRNG(19970221)
rng = np.random.RandomState(seed())
x = rng.randint(0, 100, size=10000)
y = rng.randint(0, 100, size=10000)

model = gs.Exponential(dim=2, var=0.01, len_scale=5)
srf = gs.SRF(model, seed=seeds[0])
field = srf((x, y))
ax = srf.plot()

model2 = gs.Exponential(dim=2, var=0.01, len_scale=5)
srf2 = gs.SRF(model, seed=seeds[1])
field2 = srf2((x, y))
ax = srf2.plot()

aperture = field + b_mean - field2
aperture[aperture<0] = 0

plt.scatter(x, y, c=aperture)
plt.colorbar()
plt.show()

#%%
frac = FractureGeo()
frac.load_hdf5(file3)
midpoints = frac.midpoints
faces = frac.faces
apertures = frac.fractures['aperture']

mask = apertures <= 0.1
id_remove = frac.fractures['index'][mask].to_numpy().astype(int)


surf = frac.get_surface()
surf2 = surf.cast_to_unstructured_grid()
surf2 = surf.remove_cells(id_remove)

apertures2 = apertures[~mask]

surf2['apertures'] = apertures2

p.add_object(surf2, name='mesh', scalars='apertures')

#%%
viewup = [0.5, 0.5, 1]
path = p.plotter.generate_orbital_path(n_points=100, shift=tunnel_mesh.length/10)
p.plotter.open_gif(OUTPUT_DIR + "gifs/fracture_with_holes.gif")
p.plotter.orbit_on_path(path, write_frames=True)
