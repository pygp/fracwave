from bedretto import model
from fracwave import FractureGeo, FracEM, Antenna, SourceEM, OUTPUT_DIR
import pyvista as pv
import matplotlib.pyplot as plt
import numpy as np
file = OUTPUT_DIR + 'less_frequencies_few.h5'
# Simulation for the 20 MHz data
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.02 # m
#%%
p = model.ModelPlot()
#%%
geom = FractureGeo()
geom_geom = dict(width=50,
                 length=50,
                 resolution=(50, 50),
                 dip=75,
                 azimuth=0,
                 radius=0,
                 axis=0
                 )
geom_prop = dict(aperture=0.5,
                 electrical_conductvity=0.001,
                 electrical_permeability=81)
geom.create_regular_squared_mesh(**geom_geom)
geom.set_fracture_properties(**geom_prop)

#%%
geom.check_minimum_area(wavelength=wavelength)
#%%
geom.export_to_hdf5(file, overwrite=True)
#%%
p.add_object(geom.surface, name='geom', color='yellow')

ver = pv.PolyData(geom.fracture_properties[['x','y','z']].to_numpy())
p.add_object(ver, name='points')

#%% Antenna
samples = 300
sou = SourceEM()
f = np.linspace(0, 0.1, samples)
sou.frequency = f

firstp = sou.create_source(a=1,
                           b=0.02,
                           t=5)
sou.plot()
#plt.xlim(0,0.1)
plt.show()
sou.export_to_hdf5(file, overwrite=True)

#%% Less frequencies
# pos_max_freq = np.where(np.argsort(np.abs(sou.frequency - sou.peak_frequency)) == 0)[0][0]
a = np.abs(sou.source)
pos_max_freq = np.where(a == max(a))[0][0]

threshold = 0.8

mask_freq = np.abs(sou.source) > threshold

levels = 10

zones = np.round(sum(mask_freq)/ 2 / levels).astype(int) # Equally spaced levels to divide the frequencies

mask_int=np.zeros(len(mask_freq)).astype(bool)

for l in range(levels):
    l_c = pos_max_freq-zones*(l+1) if pos_max_freq-zones*(l+1) > 0 else 0
    r_1 = np.arange(l_c, pos_max_freq-zones*l, l+1)
    r_c = pos_max_freq+zones*(l+1) if pos_max_freq+zones*(l+1) < len(mask_freq) else len(mask_freq)
    r_2 = np.arange(pos_max_freq + zones*l, r_c, l+1)
    mask_int[r_1] = True
    mask_int[r_2] = True

mask_freq *= mask_int

#%%
fig, ax = plt.subplots()
sou.plot(ax=ax)
ax.plot(sou.frequency[mask_freq], np.abs(sou.source[mask_freq]), '*r')
plt.show()

#%%
sou.frequency = f[mask_freq]
sou.create_source()
#%%
fig, ax = plt.subplots()
sou.plot(ax=ax)
#ax.plot(sou.frequency[mask_freq], np.abs(sou.source[mask_freq]), '*r')
plt.show()
#%%
sou.export_to_hdf5(file,overwrite=True)

#%% Antenna
ant = Antenna()
z = np.arange(-50, 50)
x = np.zeros(len(z))
y = np.ones(len(z)) * 10
ant.Rx = np.c_[x,y,z]
ant.Tx = np.c_[x,y,z]
ant.orient_Rx = np.tile([0,0,1], (len(z),1))
ant.orient_Tx = np.tile([0,0,1], (len(z),1))

ant.export_to_hdf5(file, overwrite=True)
#%%
ant.plot(p=p.plotter, arrows=True)
#%%

solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.engine = 'tensor_all'
solv.engine = 'tensor_trace'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv.backend = 'torch'

solv.open_file(file)

#%%
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)

#%%
time_response, time_vector = solv.get_ifft(freq)
#%%
vmax = np.abs(time_response).max()

plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)

plt.colorbar()
plt.show()

#%%
file1 = OUTPUT_DIR + 'less_frequencies.h5'
file2 = OUTPUT_DIR + 'less_frequencies_all.h5'
file3 = OUTPUT_DIR + 'less_frequencies_few.h5'

#%%
solv1 = FracEM()
solv1.load_hdf5(file1)
frac_field1=solv1.file_read('simulation/fracture_field')
#%%
solv2 = FracEM()
solv2.load_hdf5(file2)
frac_field2 = solv2.file_read('simulation/fracture_field')

#%%
solv3 = FracEM()
solv3.load_hdf5(file3)
frac_field3 = solv3.file_read('simulation/fracture_field')

#%%
ver = pv.PolyData(geom.fracture_properties[['x','y','z']].to_numpy())
ver['frac'] = frac_field3[60]
p.add_object(ver, name='points', scalars='frac', cmap='viridis')

#%%
for f in range(frac_field1.shape[0]):
    por = np.where(frac_field2[f] == frac_field2[f].max())
    poi = np.where(frac_field3[f] == frac_field3[f].max())
    if por[0][0] != poi[0][0]:
        print(f'------f:{f} \n 1: {por} \n2: {poi}')

#%%

from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_incoming_field
mask = get_mask_incoming_field(solv)

