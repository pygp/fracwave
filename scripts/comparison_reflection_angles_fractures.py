from fracwave import (OUTPUT_DIR, SourceEM,
                      FractureGeo, FracEM, Antenna)
from bedretto import model
import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
# from fracwave import set_error_level
# set_error_level('debug')

file = OUTPUT_DIR + 'amplitude/multiple_fractures_square.h5'

#%% Source
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1  # m
max_element_size = wavelength / 6
sou = SourceEM()
try:
    sou.load_hdf5(file)
except:
    f = np.linspace(0, 0.25, 500)
    sou.frequency = f
    sou.create_source(t=-70)
    sou.export_to_hdf5(file, overwrite=True)
ax = sou.plot()
ax.figure.show()
#%%
ant = Antenna()
try:
    ant.load_hdf5(file)
except:
    traces = 600
    depths = np.linspace(-20,20,traces)
    borehole = np.zeros((traces, 3))
    borehole[:,2] = depths
    orient = np.zeros((traces, 3))
    orient[:, 2] = 1

    ant.set_profile(name_profile='Borehole1',
                    receivers=borehole,
                    transmitters=borehole,
                    orient_receivers=orient,
                    orient_transmitters=orient)
    ant.export_to_hdf5(file, overwrite=True)

#%%
p = model.ModelPlot()

#%%
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')

n_arrows = 20
Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in np.linspace(1, len(ant.Transmitter)-1, n_arrows, dtype=int):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=1)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=1)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')

#%%
frac = FractureGeo()
try:
    frac.load_hdf5(file)
except:
    disk = False
    import pyvista as pv
    from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip
    kwargs_properties = dict(aperture=0.0005,
                             electrical_conductivity=0.001,
                             electrical_permeability=81)
    frac = FractureGeo()

    n1 = get_normal_vector_from_azimuth_and_dip(azimuth=90, dip=90)
    if disk:
        d1 = pv.Disc(center=(-10, 0, -5), normal=n1, outer=10, c_res=50)
    else:
        d1 = pv.Plane(center=(-10, 0, -5), direction=n1, i_size=20, j_size=20)
    surf, vertices, faces = frac.remesh(points=d1.points,
                        max_element_size=max_element_size,
                        spacing=(10, 10),
                        plane=0,
                        extrapolate=False)
    df1 = frac.set_fracture('frac1',
                            vertices=vertices,
                            faces=faces,
                            **kwargs_properties)

    n2 = get_normal_vector_from_azimuth_and_dip(azimuth=270, dip=10)
    if disk:
        d2 = pv.Disc(center=(10, 0, 0), normal=n2, outer=10, c_res=50)
    else:
        d2 = pv.Plane(center=(10, 0, 0), direction=n2, i_size=20, j_size=20)
    surf, vertices, faces = frac.remesh(points=d2.points,
                        max_element_size=max_element_size,
                        spacing=(10, 10),
                        plane=0,
                        extrapolate=False)

    # frac1.check_minimum_area(wavelength)
    df2 = frac.set_fracture('frac2',
                            vertices=vertices,
                            faces=faces,
                            **kwargs_properties)


    n3 = get_normal_vector_from_azimuth_and_dip(azimuth=0, dip=45)
    if disk:
        d3 = pv.Disc(center=(0, 10, 5), normal=n3, outer=10, c_res=50)
    else:
        d3 = pv.Plane(center=(0, 10, 5), direction=n3, i_size=20, j_size=20)
    surf, vertices, faces = frac.remesh(points=d3.points,
                        max_element_size=max_element_size,
                        spacing=(10, 10),
                        plane=2,
                        extrapolate=False)

    df3 = frac.set_fracture('frac3',
                            vertices=vertices,
                            faces=faces,
                            **kwargs_properties)

    frac.check_minimum_area(wavelength=wavelength)
    frac.export_to_hdf5(file, overwrite=True)

#%%
p.add_object(frac.surface, scalars='name', opacity=0.5, name='vertices')

#%%

solv = FracEM()
#%%
solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
# solv.engine = 'dummy'
# solv.units = 'SI'
solv.backend = 'torch'
# solv.backend = 'numpy'
solv.open_file(file)
freq_t0 = solv.time_zero_correction()

#%%
freq = solv.forward_pass(save_hd5=False)
#%%
file = OUTPUT_DIR + 'amplitude/multiple_fractures.h5'
solv.load_hdf5(file)
freq_disk = solv.file_read('summed_response')
file = OUTPUT_DIR + 'amplitude/multiple_fractures_square.h5'
solv.load_hdf5(file)
freq_square = solv.file_read('summed_response')


#%%
time_response_disk, time_vector_disk = solv.get_ifft(freq_disk)
time_response_square, time_vector_square = solv.get_ifft(freq_square)
#%%
time_vector = time_vector_square
time_response = time_response_square #- time_response_square
distance =  time_vector * solv.velocity / 2
fig, AX = plt.subplots(1, 3, figsize=(25,5))

vmax = 0.5e-8
for ax, im, titl in zip(AX,
                        (time_response_disk,
                         time_response_square,
                         time_response_disk-time_response_square),
                        ('Disk', 'Square', 'Difference (Disk - Square)')):
    # vmax = im.max() / 100
    cax = ax.imshow(im.T,
                    aspect='auto',
                    cmap='seismic',
                    vmax=vmax,
                    vmin=-vmax,
                    extent=(-20, 20, distance[-1], distance[0])
                    )
    axt = ax.twinx()

    axt.set_ylim(time_vector[-1], time_vector[0])
# ax.set_yticks(ax.get_yticks())
    axt.set_ylabel('Time (ns)')

    ax.set_xlabel('Borehole depth (m)')
    ax.set_ylabel('Radial distance (m)')
    ax.set_ylim(30,0)
    ax.grid(linestyle='--')

    fig.colorbar(cax, ax=ax, pad=0.15)
    ax.tick_params(axis='both',  which='both', direction='out', length=5)
    axt.tick_params(axis='both',  which='both', direction='out', length=5)

    ax.set_title(titl)

plt.show()
#%%
fig.savefig(OUTPUT_DIR+'amplitude/multiple_fractures_difference_saturate.svg')

#%%
p.save_scene_as_html(OUTPUT_DIR + 'amplitude/scene_multiple_fractures_square')
#%%
