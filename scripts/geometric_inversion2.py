from fracwave import FracEM, FractureGeo, OUTPUT_DIR
from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
                                                   set_fracture_solver,
                                                   forward_operator,
                                                   run_inversion)
import numpy as np

#%%
frac = FractureGeo()
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)
resolution = (50,50,50)
frac.init_geo_model(gempy_extent, resolution)
solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
solv.backend = 'torch'
solv.engine='tensor_trace'

set_fracture_geometry(frac)
set_fracture_solver(solv)
#%%

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1 # m
max_element_size = wavelength/6
p1 = (70, 90, 0)

fixed = np.asarray([(0,0,p1[-1])])
fixed_or = [fixed.copy(), np.asarray([(p1[1],p1[0],1)])]

planar = np.asarray([[1.20713, 0, -3.3165622],
                          [0.653862, 0, -1.7964711],
                          [0.15089116, 0, -0.41457027],
                          [-0.3520796, 0, 0.96733063]])

curved = np.asarray([[0.89864941,  0.12210877, -3.37883444],
                                     [0.93922058,  0.27997581, -2.02387851],
                                     [-0.07264459,  0.30187218,  0.04356908],
                                     [0.02385303, -0.14218273,  0.96832576]])

pick_curved = np.load(OUTPUT_DIR + 'test_dummy_step/pick_curved_dummy.npy')
pick_plane = np.load(OUTPUT_DIR + 'test_dummy_step/pick_plane_dummy.npy')

kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductivty=0.001,
                                  electrical_permeability=81)

kwargs_forward_solver = dict(plane=0,
                             order=2,
                             max_element_size=max_element_size)

kwargs_cost_function = dict(tol_all=0.5,  # Tolerance for overall RMS error over the whole picks
                            tol=0.3, # Tolerance for a single trace
                            surrounding=3)

filename = OUTPUT_DIR + 'test_dummy_step/dummy_plane.h5'

fixed_points_geometry, solv = run_inversion(pick_true=pick_curved,
                                            fixed_points_geometry=fixed,
                                            fixed_oientations_geometry=fixed_or,
                                            constrained_points_geometry=planar,
                                            filename=filename,
                                            plot_step=True,
                                            kwargs_fracture_properties=kwargs_fracture_properties,
                                            kwargs_forward_solver=kwargs_forward_solver,
                                            kwargs_cost_function=kwargs_cost_function)

np.save(OUTPUT_DIR+'test_dummy_step/output_points.npy', fixed_points_geometry)
#%%
pick_p, solv_plane = forward_operator(planar,
                     fixed,
                     fixed_or,
                     filename_plane,
                     max_element_size,
                     plane=0,
                     order=2,
                     kwargs_fracture_properties=kwargs_fracture_properties,
                     compute_response=False)


pick_c, solv_curve = forward_operator(curved,
                     fixed,
                     fixed_or,
                     filename_curved,
                     max_element_size,
                     plane=0,
                     order=2,
                     kwargs_fracture_properties=kwargs_fracture_properties,
                     compute_response=True)
#
#
#%%
filename_curved = OUTPUT_DIR + 'dummy_step/dummy_curved.h5'
filename_plane = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'

from bedretto import model
p = model.ModelPlot(title='Inversion results')
solv_temp = FracEM()

solv_temp.load_hdf5(filename_plane)

p.add_object(solv_temp.file_read('antenna/Tx'), name='Tx', color='Red')
p.add_object(solv_temp.file_read('antenna/Rx'), name='Rx', color='blue')

frac = FractureGeo()
frac.load_hdf5(filename_plane)
p.add_object(frac.surface, name='mesh1', color='red', opacity=0.5, label ='Initial Geometry')

solv = FracEM()
frac = FractureGeo()
frac.load_hdf5(filename_plane.split('.')[0]+f'_pos_34_iter_14.h5')
solv.load_hdf5(filename_plane.split('.')[0]+f'_pos_34_iter_14.h5')
p.add_object(frac.surface, name='mesh2', color='blue', opacity=0.5, label ='Inverted Geometry')

solv_temp.open_file(filename_curved)

frac = FractureGeo()
frac.load_hdf5(filename_curved)
p.add_object(frac.surface, name='mesh3', color='green', opacity=0.5, label ='Real Geometry')
p.add_legend()
p.show(zxGrid2=False, yzGrid2=False)

import pyvista as pv
p.add_object(pv.PolyData(fixed_points_geometry), name='points', color='black', point_size=10)
# p.add_object(frac2.surface, name='fault2', color='green', opacity=1)
from bedretto.core.experiments import GPRtools

# gpr = GPRtools()
# GPR_cone, int_depth, int_radius = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
#                                                                         radius=((pick[:,1] * solv.file_read_attribute('source/dtime'))/2)*v,
#
#                                                                         # Radial distance
#                                                                         # radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         trajectory=bor_df,
#                                                                         depth_resolution=0.1,
#                                                                         radial_resolution=0.1
#                                                                         )
# p.add_object(pv.PolyData(GPR_cone), color='black', name='GPR cone', opacity=0.3)
#
# picked = solv.file_read('dummy_solution/first_arrival')
# GPR_cone, int_depth, int_radius = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
#                                                                         radius=(picked/2)*v,
#
#                    filename_plane                                                     # Radial distance
#                                                                         # radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         trajectory=bor_df,
#                                                                         depth_resolution=0.1,
#                                                                         radial_resolution=0.1
#                                                                         ) # This one works
# p.add_object(pv.PolyData(GPR_cone), color='red', name='GPR cone 2', opacity=0.3)
# p.show(zxGrid2=False, yzGrid2=False)
#%%
solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.backend = 'numpy'
solv.backend = 'torch'
# solv.backend = 'torch'
# solv.engine = 'loop'
solv.engine = 'tensor_trace'
# solv.open_file(filename_plane)
solv.open_file(filename_curved)
solv.units = 'SI'
solv.filter_energy = False
solv._filter_percentage = 0.5

# solv.backend = 'numpy'
# solv.time_zero_correction()
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
time_response, time_vector = solv.get_ifft(freq)
import matplotlib.pyplot as plt
plt.imshow(time_response.T,
           aspect='auto',
           cmap='seismic',
           vmax=np.abs(time_response).max(),
           vmin=-np.abs(time_response).max())
plt.show()

#%%
# solv.backend = 'numpy'
solv.backend = 'numpy'
# solv.engine = 'loop'
solv.engine = 'loop'
# solv.engine = 'tensor_trace'
solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
# solv.backend = 'numpy'

solv.open_file(filename_plane)

freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
time_response, time_vector = solv.get_ifft(solv.file_read('summed_response'))
import matplotlib.pyplot as plt
plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max())
plt.show()
from gdp.processing.image_processing import pick_first_arrival
pick = pick_first_arrival(time_response.T, threshold=threshold)

#%%
solv_real = FracEM()
solv_real.load_hdf5(OUTPUT_DIR + "synthetic_case_simulation_curved.h5")

solv_plane= FracEM()
solv_plane.load_hdf5(OUTPUT_DIR + "synthetic_case_simulation_plane.h5")
time_response2, time_vector2 = solv.get_ifft(solv_real.file_read('summed_response'))
time_response3, time_vector3 = solv.get_ifft(solv_plane.file_read('summed_response'))

from gdp.processing.image_processing import pick_first_arrival
pick = pick_first_arrival(time_response.T, threshold=threshold)
plt.imshow(time_response2.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max())
plt.show()

plt.imshow(time_response2.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max())
plt.show()

plt.imshow(time_response3.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response3).max(),
           vmin=-np.abs(time_response3).max())
plt.title('Planar')
plt.show()

#%%
def set_default(file):
    # ------------- SourceEM
    import matplotlib.pyplot as plt
    from fracwave import SourceEM

    samples = 300
    sou = SourceEM()
    f = np.linspace(0, 0.3, samples)
    sou.frequency = f

    firstp = sou.create_source() #a=1,
                               # b=0.02,
                               # t=5)
    # sou.plot()
    # plt.xlim(0,0.1)
    # plt.show()
    sou.export_to_hdf5(file, overwrite=True)
    # ------------- Antenna
    from fracwave import Antenna
    ant = Antenna()
    ant.Rx = borehole
    ant.Tx = borehole
    ant.orient_Rx = np.repeat([(0,0,1)], len(borehole), axis=0)
    ant.orient_Tx = np.repeat([(0,0,1)], len(borehole), axis=0)

    ant.export_to_hdf5(file, overwrite=True)
    return sou, ant
#%%
sou.export_to_hdf5(filename_plane, overwrite=True)
#%%
from fracwave import SourceEM

samples = 300
sou = SourceEM()
f = np.linspace(0, 0.3, samples)
sou.frequency = f

firstp = sou.create_source(a=15, b=0.02)
# b=0.02,
# t=5)
# sou.plot()
# plt.xlim(0,0.1)
# plt.show()
sou.plot()
plt.xlim(0,0.3)
plt.show()

#%%
# solv_plane.time_zero_correction()
time_response, time_vector = solv_plane.get_ifft(solv_plane.file_read('summed_response'))
import matplotlib.pyplot as plt
plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max())
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()

#%%
# solv_curve.time_zero_correction()
time_response, time_vector = solv_curve.get_ifft(solv_curve.file_read('summed_response'))
import matplotlib.pyplot as plt
plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max())
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()