from fracwave import FractureGeo, DATA_DIR
import pandas as pd
from bedretto import data

bor=data.BoreholeData()


data = ['BCS_D_01_HEAD_PLANNED.txt',
        'BCS_D_05_HEAD_PLANNED.txt',
        'BCS_D_06_HEAD_PLANNED.txt',
        'BCS_D_07_HEAD_PLANNED.txt']

folder = DATA_DIR + 'Montterri/'

data_b = ['BCS_D_01_09Nov2018_caliper_GR_DEV_DIL.txt',
          'BCS_D_05_04Oct2018_caliper_GR_DEV_DIL.txt',
          'BCS_D_06_11sep2018_caliper_GR_DEV_DIL.txt',
          'BCS_D_07_02Nov2018_caliper_GR_DEV_DIL.txt']

#%%
boreholes_head = []
for d in data:
    name = d[:8]
    # boreholes_head[name] =pd.read_table(folder + d, delim_whitespace=True)
    df =pd.read_table(folder + d, delim_whitespace=True)
    df['Borehole'] = [name]
    boreholes_head.append(df)

boreholes_head = pd.concat(boreholes_head, axis=0)

boreholes = {}
for d in data_b:
    df_temp = pd.read_table(folder+d,  sep='\t', skiprows=[1])
    df_temp.rename(columns=lambda x: x.strip(), inplace=True)
    df_temp = df_temp[['Depth','Easting', 'Northing', 'TVD']]
    df_temp = df_temp[~df_temp.isin([-999])].dropna()
    # Q/ How can I find and delete all lines that have a -999 in the Easting, Northing and Depth columns?
    df_temp = df_temp.loc[df_temp['Easting'] != -999]
    df_temp = df_temp.loc[df_temp['Northing'] != -999]
    df_temp = df_temp.loc[df_temp['TVD'] != -999]
    name = d[:8]

    df_temp['Easting'] = df_temp['Easting'].to_numpy() + boreholes_head.loc[boreholes_head['Borehole'] == name]['x_head'].to_numpy()
    df_temp['Northing'] = df_temp['Northing'].to_numpy() + boreholes_head.loc[boreholes_head['Borehole'] == name]['y_head'].to_numpy()
    df_temp['TVD'] = - df_temp['TVD'].to_numpy() + boreholes_head.loc[boreholes_head['Borehole'] == name]['z_head'].to_numpy()
    df_temp.rename({'Easting': 'Easting (m)', 'Northing': 'Northing (m)', 'TVD': 'Elevation (m)', 'Depth': 'Depth (m)'}, axis=1, inplace=True)
    boreholes[name] = df_temp

    df_temp.to_csv(folder + name + '_borehole_trajectory.csv', index=False)


#%%
from bedretto import data as data_bedretto
from bedretto import geometry
from bedretto import model

bor_dat = data_bedretto.BoreholeData()

top = []
bottom = []
for key, df in boreholes_head.iterrows():
    name = df['Borehole']
    top.append(bor_dat.get_coordinates_from_borehole_depth(borehole_information=boreholes[name], depths=df['fault_top']))
    bottom.append(bor_dat.get_coordinates_from_borehole_depth(borehole_information=boreholes[name], depths=df['fault_bottom']))

boreholes_head[['x_fault_top', 'y_fault_top', 'z_fault_top']] = top
boreholes_head[['x_fault_bottom', 'y_fault_bottom', 'z_fault_bottom']] = bottom

boreholes_head.to_csv(folder + 'fault_zone.csv', index=False)

bor_geom = geometry.BoreholeGeometry(bor_dat)
borehole_geometries = {key:bor_geom.construct_borehole_geometry(borehole_info = b, radius=0.1) for key, b in boreholes.items()}

p = model.ModelPlot()
[p.add_object(bor, name = key, label =key, color='black') for key, bor in borehole_geometries.items()]
p.add_label(boreholes_head[['x_head','y_head','z_head']].to_numpy(),
            labels=boreholes_head['Borehole'].to_list(), name='labels',
            font_size=10)

p.add_object(boreholes_head[['x_fault_top', 'y_fault_top', 'z_fault_top']].to_numpy(),
             name = 'top_fault', color='blue', render_points_as_spheres=True, point_size=10)
p.add_object(boreholes_head[['x_fault_bottom', 'y_fault_bottom', 'z_fault_bottom']] .to_numpy(),
             name = 'bot_fault', color='red', render_points_as_spheres=True, point_size=10)


#%%
frac = FractureGeo()
extent = [2579300, 2579325, 1247562, 1247586, 470, 515]
#%%
frac.init_2surface_geo_model(extent =extent, resolution = (50,50,50))

#%%
df = frac.create_df_control_points(pointsA=boreholes_head[['x_fault_top', 'y_fault_top', 'z_fault_top']].to_numpy(),
                                   pointsB=boreholes_head[['x_fault_bottom', 'y_fault_bottom', 'z_fault_bottom']].to_numpy())

df_orientation = frac.create_df_orientations(pointsA=boreholes_head[['x_fault_top', 'y_fault_top', 'z_fault_top']].to_numpy(),
                                             dipsA=boreholes_head['fault_dip'], azimuthA=boreholes_head['fault_azi'],
                                                pointsB=boreholes_head[['x_fault_bottom', 'y_fault_bottom', 'z_fault_bottom']].to_numpy(),
                                                dipsB=boreholes_head['fault_dip'], azimuthB=boreholes_head['fault_azi'])

v, f = frac.create_gempy_geometry(fixed_points=df, fixed_orientations=df_orientation)

surf, vertices, faces = frac.remesh(points=v, plane=2, max_element_size=0.5)

p.add_object(surf['A'], name='Top fault', color='blue', opacity=0.5)
p.add_object(surf['B'], name='Bottom fault', color='red', opacity=0.5)

surf['A'].save(folder+'top_fault.vtk')
surf['B'].save(folder+'bottom_fault.vtk')