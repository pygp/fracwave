import os, sys
import matplotlib
import pandas as pd

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
from fracwave import DATA_DIR, set_logger, OUTPUT_DIR
import pyvista as pv
import bedretto

from bedretto import data, geometry, model
from bedretto import data as d

from fracwave import FractureGeo, Antenna

#%% Read the GPR data - 100MHz MB1 reflection borehole. 200502_CB1_1000Mhz

from gdp.import_export import load_mala
dat, info = load_mala(DATA_DIR + 'temp_gpr_data/DAT_0794_A1.rd3')

def plot_dat(dat):
    vmax = np.max(np.abs(dat))
    plt.figure()
    plt.imshow(dat, interpolation=None, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)
    plt.colorbar()
    plt.show()

plot_dat(dat)
#%%
def load_gpr(filename):
    from scipy.io import loadmat
    a = loadmat(filename)
    borehole_wave = a['borehole_wave']
    depth = a['depth']
    processed_data = a['processed_data']
    radius = a['radius']
    raw_data = a['raw_data']
    travel_time = a['travel_time']
    return processed_data, travel_time, radius, depth

processed_data, travel_time, radius, depth = load_gpr('/home/daniel/Documents/gpr_data_mb1/MB1_100MHz_200218.mat')

#%%
pick_depths = [144.165838899804,
               161.156998035363,
               182.383913555992,
               244.813186640472,
               265.751300589391,
               221.131457760314,
               277.59216502947]

length = [247.388874751931,
          139.264366556157,
          252.578851145329,
          158.943027047788,
          141.859354752856,
          115.41057067395,
          63.6598644993273]

dat = processed_data
vmax = np.max(np.abs(dat))
plt.figure()
extent = (0, depth[-1,0], travel_time[0,-1], 0, )
plt.imshow(dat, interpolation=None, extent=extent, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)
for x in pick_depths:
    plt.axvline(x, ymin=extent[2], ymax=extent[3], color='blue')
plt.xlabel('Borehole depth')
plt.ylabel('Two-way Travel time (ns)')
plt.colorbar()
plt.show()
#%%
# plot_dat(raw_data)
#%% Time zero removal
# Time zero measureents
# 0789 60 cm spacing
# 0790 50 cm spacing
# 0791 40 cm spacing

dat6, info6 = load_mala(DATA_DIR + 'temp_gpr_data/DAT_0789_A1.rd3')
dat5, info5 = load_mala(DATA_DIR + 'temp_gpr_data/DAT_0790_A1.rd3')
dat4, info4 = load_mala(DATA_DIR + 'temp_gpr_data/DAT_0791_A1.rd3')

# Samples to remove to the data
from gdp.processing.image_processing import pick_first_arrival

pick6 = pick_first_arrival(dat6, threshold=0.1)
pick5 = pick_first_arrival(dat5, threshold=0.1)
pick4 = pick_first_arrival(dat4, threshold=0.1)

mean6 = np.mean(pick6[:,1])
mean5 = np.mean(pick5[:,1])
mean4 = np.mean(pick4[:,1])

from sklearn.linear_model import LinearRegression
lr = LinearRegression()
lr.fit(np.array([[60, 50, 40]]).T, np.array([[mean6, mean5, mean4]]).T)
samples = np.round(lr.intercept_).astype(int)[0]
data = dat[samples:]

plot_dat(data)
#%% Remove mean
from gdp.processing.filtering import detrend

data_d = detrend(data)
plot_dat(data_d)

#%%
from gdp.plotting.plot import plot_frequency_information
plot_frequency_information(data_d[:, 1000], dt=1/info['frequency'])
#%%
flt = (20, 200)
from gdp.processing.filtering import filter_data
data_f = filter_data(data_d, sfreq=info['frequency'], filter_type='acausal',
                     fq=flt, btype='bandpass')
plot_frequency_information(data_f[:, 1000], dt=1/info['frequency'])

#%%
plot_dat(data_d)
plot_dat(data_f)
#%% Remove borehole wave
from gdp.processing.image_processing import remove_svd

data_svd, svd_decomp = remove_svd(data_f)
plot_dat(data_svd)
plot_frequency_information(data_svd[:, 1000], dt=1/info['frequency'])

#%% Apply gain to enhance image
from gdp.processing.gain import apply_gain


data_g, gain_matrix = apply_gain(data_svd, gain_type='spherical', sfreq=info['frequency'], exponent=2,
                           velocity=0.1234, twoway=True)
plot_dat(data_g)
#%%

fig, ax = plt.subplots()
vmax = np.max(np.abs(data_g)) * 0.1
extent= (data_g[0] / (info['frequency']/1000), 0, 0, )
plt.imshow(np.fliplr(data_g), extent=extent,
           aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, interpolation=None)
plt.colorbar()

plt.grid()
plt.show()
#%% Here we start with the real problem
file =OUTPUT_DIR + 'multiple_fractures.h5'

#%%
# Geometry
# ----------
bor_dat = d.BoreholeData()
tun_dat = d.TunnelData()

# Borehole
# -----------
bor_geom = geometry.BoreholeGeometry(bor_dat)
mb1 = bor_dat.get_borehole_data('MB1')
mb1_geometry = bor_geom.construct_borehole_geometry(borehole_info=mb1, radius=0.2)

tn = d.TunnelData()
tn_geom = geometry.TunnelGeometry(tn)
surf = tn_geom.get_tunnel_mesh([1940,2100])
#%%
p = model.ModelPlot()
#%%
p.add_object(mb1_geometry, name='mb1', opacity=1, color='black', label='mb1')
p.add_object(surf, name='tunnel', opacity=1, color='gray', label='tunnel')

#%%
log_dat = d.LoggingData(borehole_data=bor_dat)

#%%
from cmath import rect, phase
from math import radians, degrees
import pandas as pd
def mean_angle(deg):
    d = degrees(phase(sum(rect(1, radians(d)) for d in deg)/len(deg)))
    if d < 0:
        d += 360
    return d

plane = pd.DataFrame()

for count, dep in enumerate(pick_depths):
    depth_bor = bor_dat.get_coordinates_from_borehole_depth(borehole_information=mb1, depths=dep)
    d = log_dat.get_structure_geometry_from_logging(borehole_name='MB1', depth_range=[dep-2, dep+2],
                                                           structure_type=['Open fracture', 'Shear Zone-mylonite'])
    plane = plane.append(pd.DataFrame({'Azimuth (deg)': mean_angle(d['Azimuth (deg)'].to_numpy()),
                                       'Dip (deg)': np.mean(d['Dip (deg)'].to_numpy()),
                                       'Depth (m)': dep,
                                       'X':depth_bor[0],
                                       'Y':depth_bor[1],
                                       'Z':depth_bor[2],
                                       'length': length[count],
                                       'Aperture (mm)':np.max(d['Aperture (mm)'])},
                              index=[count]))

#%% Construct planes with pyvista
from bedretto.core.vector_helpers import get_normal_vector_from_azimuth_and_dip

min_wavelength = 6 / 4
fractures = dict()
for c, df in plane.iterrows():
    normal = get_normal_vector_from_azimuth_and_dip(azimuth=df['Azimuth (deg)'], dip=df['Dip (deg)'])
    fractures[c] = pv.Plane(center=df[['X', 'Y', 'Z']].to_numpy(), direction=normal,
                             i_size=df['length']+20, j_size=df['length']+20,
                            i_resolution=np.ceil(df['length'] / min_wavelength).astype(int),
                            j_resolution=np.ceil(df['length'] / min_wavelength).astype(int))
#%%
np.random.seed(1111)
for key, f in fractures.items():
    random_color = list(np.random.choice(range(255), size=3))
    p.add_object(f, name=str(key), color=random_color,
                 opacity=0.7, label=str(key))

#%% make fractures
wavelength = 6
frac_properties = dict(aperture=0.1,
                       electrical_conductvity=0.001,
                       electrical_permeability=81)
frac_object = dict()
for key, frac in fractures.items():
    fr = FractureGeo(f'Fracture {key}')
    fr.extract_vertices_and_faces_from_pyvista_mesh(frac)
    fr.set_fracture_properties(**frac_properties)

    frac_object[key] = fr

#%%

all_fractures = frac_object[0]
for i in range(1, len(frac_object)):
    all_fractures += frac_object[i]
#%%
all_fractures.export_to_hdf5(file, overwrite=True)
#%%
p.add_object(all_fractures.surface, name='faults', color='yellow', opacity=0.8)

#%% Not working
frac_all = FractureGeo()

frac_all.load_hdf5(file)

p.add_object(frac_all.surface, name='faults', color='yellow', opacity=0.8)

#%% # Geometry
# ----------
bor_dat = data.BoreholeData()
tun_dat = data.TunnelData()

# Borehole
# -----------
bor_geom = geometry.BoreholeGeometry(bor_dat)
mb1 = bor_dat.get_borehole_data('MB1')
mb1_geometry = bor_geom.construct_borehole_geometry(borehole_info=mb1, radius=0.2)

#if show:
#    plotter.add_mesh(mb1_geometry, color='black')
#%%
# Coordinates of dipoles
traces = 500
separation = 2.77
# Rx = 100
# Tx = Rx + separation

Rx = np.linspace(5, 280-separation, traces)
Tx = np.linspace(5 + separation, 280, traces)
#xyz_rx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Rx).T
#xyz_tx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Tx).T

xyz_rx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Rx).T
xyz_tx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Tx).T

# Orientation dipole
# -----------
# Calculate rx orientation by giving 10 cm before and 10 cm after
rx_1 = Rx - 0.1
rx_2 = Rx + 0.1

orient_rx = (bor_dat.get_coordinates_from_borehole_depth(mb1, depths=rx_1).T - \
             bor_dat.get_coordinates_from_borehole_depth(mb1, depths=rx_2).T)
#orient_rx /= np.linalg.norm(orient_rx, ord=1, axis=1)[:, np.newaxis]
orient_rx /= np.linalg.norm(orient_rx, ord=1)

# Calculate tx orientation by giving 10 cm before and 10 cm after
tx_1 = Tx - 0.1
tx_2 = Tx + 0.1

orient_tx = (bor_dat.get_coordinates_from_borehole_depth(mb1, depths=tx_1).T - \
             bor_dat.get_coordinates_from_borehole_depth(mb1, depths=tx_2).T)
#orient_tx /= np.linalg.norm(orient_tx, ord=1, axis=1)[:, np.newaxis]
orient_tx /= np.linalg.norm(orient_tx, ord=1)

ant = Antenna()
ant.Rx = xyz_rx
ant.Tx = xyz_tx
ant.orient_Rx = orient_rx
ant.orient_Tx = orient_tx

ant.export_to_hdf5(file, overwrite=True)

ant.plot(p=p.plotter, backend='pyvista', arrows=False)

#%%
ant = Antenna()
ant.load_hdf5(file)

#%%
import matplotlib.pyplot as plt
from fracwave import SourceEM

samples = 300
sou = SourceEM()
f = np.linspace(0, 0.1, samples)
sou.frequency = f

firstp = sou.create_source(a=1,
                           b=0.02,
                           t=5)
sou.plot()
plt.xlim(0,0.1)
plt.show()
sou.export_to_hdf5(file, overwrite=True)

#%% Simulation

from fracwave import FracEM

solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.engine = 'tensor_all'
solv.engine = 'tensor'
solv.units = 'SI'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv.backend = 'torch'

solv.open_file(file)

#%%
freq = solv.forward_pass(overwrite=True, recalculate=False, save_hd5=False)

#%%
#file_output = OUTPUT_DIR + 'cluster_results/output/multiple_fractures.h5'
file_output = OUTPUT_DIR + 'cluster_results/output/branch_fault.h5'

#%%
freq = np.load(file_output.replace('.h5', '_summed_response.npy'))

#%%
solv = FracEM()
solv.load_hdf5(file_output)

geom = FractureGeo()
geom.load_hdf5(file_output)

sou = SourceEM()
sou.load_hdf5(file_output)

ant = Antenna()
ant.load_hdf5(file_output)
#%%
time_response, time_vector = solv.get_ifft(freq)


#%%
from gdp.processing.normalizing import normalize_data

no = normalize_data(time_response.T, typ='tracewise-max-abs')

#%%
#no = time_response.T
vmax = np.max(np.abs(no)) * 0.8

extent = [0, 280, time_vector[-1], 0]
plt.figure()
plt.imshow(no, aspect='auto', cmap='seismic', extent=extent, vmin=-vmax, vmax=vmax)
for x in pick_depths:
    plt.axvline(x, ymin=extent[2], ymax=extent[3], color='blue')
plt.ylim(1000,0)
plt.xlim(0,280)
plt.xlabel('Borehole depth')
plt.ylabel('Two-way Travel time (ns)')
plt.grid()
plt.show()

#%%
processed_data20, travel_time20, radius20, depth20 = load_gpr('/home/daniel/Documents/gpr_data_mb1/MB1_20MHz_B200108.mat')

processed_data20_no = normalize_data(processed_data20, typ='tracewise-max-abs')
processed_data20_no = processed_data20
vmax = np.max(np.abs(processed_data20_no))
plt.figure()
extent = (0, depth20[-1,0], travel_time20[0,-1], 0, )
plt.imshow(processed_data20_no, interpolation=None, extent=extent, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)
for x in pick_depths:
    plt.axvline(x, ymin=extent[2], ymax=extent[3], color='blue')
plt.xlabel('Borehole depth')
plt.ylabel('Two-way Travel time (ns)')
plt.xlim(0,280)
plt.ylim(1000,0)
plt.colorbar()
plt.grid()
plt.show()

#%%
from matplotlib.patches import Rectangle
plt.figure()
extent = (0, depth20[-1,0], travel_time20[0,-1], 0, )
plt.imshow(processed_data20_no, interpolation=None, extent=extent, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)

extent = [0, 280, time_vector[-1], 0]
masked_no = no.copy()
mask = np.abs(no) < 0.05
masked_no[mask] = np.nan
plt.imshow(masked_no, aspect='auto', cmap='viridis', extent=extent, interpolation=None,)

for x in pick_depths:
    #plt.axvline(x, ymin=extent[2], ymax=extent[3], color='blue')
    plt.gca().add_patch(Rectangle((x-2, 0),
                                  width=4,
                                  height=extent[2],
                                  color='blue',
                                  alpha=0.5))
    plt.xlabel('Borehole depth')
plt.ylabel('Two-way Travel time (ns)')
plt.xlim(0,280)
plt.ylim(1000,0)
plt.colorbar()
plt.grid()
plt.show()

