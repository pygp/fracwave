from bedretto import model
from fracwave import FracEM, SourceEM, Antenna, FractureGeo, OUTPUT_DIR
import numpy as np
import pyvista as pv
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os

filename = OUTPUT_DIR + 'bb_real/inversion_bb.h5'
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.02  # m
max_element_size = wavelength / 6
#%%
picks_fn = OUTPUT_DIR+'single_pick_all_boreholes.pkl'
if os.path.isfile(picks_fn):
    with open(picks_fn, 'rb') as f:
        picks = pickle.load(f)
else:

    with open(OUTPUT_DIR+'bb_real/saved_dictionary_all_picks.pkl', 'rb') as f:
        picks_all = pickle.load(f)

    # Use only 100MHz
    p100hz = picks_all['100MHz']
    # Join all in a single one
    boreholes = list(p100hz.keys())

    all_boreholes = dict()
    for bor in boreholes:
        if bor == 'ST1':
            continue
        all_dat = p100hz[bor]
        x = []
        y = []
        t = []
        for fn, (samples, transform) in all_dat.items():
            for (k1, rdata), (k2, tdata) in zip(samples.items(), transform.items()):
                t.append(rdata[:, 0])
                x.append(tdata[:, 0])
                y.append(tdata[:, 1])
        all_boreholes[bor] = (t, x, y)

    one_per_borehole = dict()

    for bor, (tl,xl,yl) in all_boreholes.items():
        tmax = max([a.max() for a in tl])
        tmin = min([a.min() for a in tl])
        xpos = []
        ypos = []
        for i in range(tmin, tmax+1):
            xdepth = []
            ytime = []
            for t,x,y in zip(tl, xl,yl):
                pos = np.where(i == t)[0]
                if len(pos) > 0:
                    xdepth.append(x[pos[0]])
                    ytime.append(y[pos[0]])
            if len(xdepth) > 0:
                xpos.append(np.mean(xdepth))
                ypos.append(np.mean(ytime))
        one_per_borehole[bor] =np.c_[xpos, ypos]

    with open(picks_fn, 'wb') as f:
        pickle.dump(one_per_borehole, f)
    picks = one_per_borehole
# downsample the vales
step = 10
for key, val in picks.items():
    picks[key] = val[::step]

#%% plot them all

fig, AX = plt.subplots(len(picks), figsize=(10,20))
np.random.seed(0)
for ax, (bor, p) in zip(AX, picks.items()):
    leg_dict = dict()
    ax.set_xlabel('Borehole depth (m)')
    ax.set_ylabel('Travel time (ns)')
    ax.set_title(bor)
    ax.grid()
    ax.plot(p[:,0], p[:,1], '*')
    # tl,xl,yl = all_boreholes[bor]
    # for i,(t,x,y) in enumerate(zip(tl, xl, yl)):
    #     ax.plot(x, y, label=i)
    # ax.legend()
fig.tight_layout()
plt.show()
# fig.savefig(DATA_DIR+'result.svg')
#%%
ant = Antenna()
boreholes = list(picks.keys())
try:
    ant.load_hdf5(filename)
except:
    from bedretto import data
    bor_dat = data.BoreholeData()
    antenna_center_separation = 2.77

    for b in boreholes:
        positions = picks[b]
        # Depths are from midpoint.
        xyz_Rx = bor_dat.get_xyz_coordinates(b, positions[:, 0]-antenna_center_separation*0.5).T
        xyz_Rx_t = bor_dat.get_xyz_coordinates(b, positions[:, 0]-antenna_center_separation*0.5+0.1).T
        xyz_Tx = bor_dat.get_xyz_coordinates(b, positions[:, 0]+antenna_center_separation*0.5).T
        xyz_Tx_t = bor_dat.get_xyz_coordinates(b, positions[:, 0]+antenna_center_separation*0.5+0.1).T


        orient_rx = (xyz_Rx - xyz_Rx_t) / np.linalg.norm((xyz_Rx - xyz_Rx_t), ord=1, axis=1)[:, None]
        orient_tx = (xyz_Tx - xyz_Tx_t) / np.linalg.norm((xyz_Tx - xyz_Tx_t), ord=1, axis=1)[:, None]


        ant.set_profile(name_profile=b,
                        receivers=xyz_Rx,
                        transmitters=xyz_Tx,
                        orient_receivers=orient_rx,
                        orient_transmitters=orient_rx
                        )

    ant.export_to_hdf5(filename, overwrite=True)

#%%

p = model.ModelPlot()
p.show(zxGrid2=False, yzGrid2=False)
#%%
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')
n_arrows = 20
Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in np.linspace(1, len(ant.Transmitter)-1, n_arrows, dtype=int):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=6)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=6)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')
#%% Setup all the geometry and inversion scheme
sou = SourceEM()
try:
    sou.load_hdf5(filename)
except:
    f = np.linspace(0, 0.05, 300)
    sou.frequency = f
    sou.create_source(a=1, b=0.02, t=-70)
    sou.export_to_hdf5(filename, overwrite=True)
ax = sou.plot()
ax.figure.show()

#%%
# Source function

frac = FractureGeo()
extent = (-250, 50, -150, 100, 1200, 1500)
if frac.geo_model is None:
    frac.init_geo_model(extent=extent, resolution=(50, 50, 50))
from bedretto import data
tun_dat = data.TunnelData()
bb = tun_dat.get_tunnel_fractures(structure_ID='-5')

fixed_points = np.asarray(bb['Pointcloud'].to_numpy()[0])
fixed_orientations = (np.asarray(bb['Center'].to_numpy()[0])[None],
                      np.c_[bb['Azimuth (deg)'].to_numpy(), bb['Dip (deg)'].to_numpy()[0]])
# def get_bb_points():
#
#     from bedretto import data
#     from bedretto import DATA_DIR as dat_bedretto
#     from bedretto.core.experiments import probabilities
#
#     # set numpy random seed
#     np.random.seed(0)
#     # initialize classes
#     borehole_data = data.BoreholeData()
#     p_samp = probabilities.ProbabilisticSampling(borehole_data)
#
#     bb_data = pd.read_csv(dat_bedretto + 'logging_data/BB_logging.csv')
#     bb_sample = p_samp.sample_logging_structures(bb_data)
#     orientations = bb_sample[['Nx', 'Ny', 'Nz']].to_numpy()
#     surface_points = bb_sample[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()
#
#     from fracwave.geometry.vector_calculus import get_azimuth_and_dip_from_normal_vector
#     dip, azimuth = get_azimuth_and_dip_from_normal_vector(orientations)
#
#     tun_dat = data.TunnelData()
#     bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
#     surface_points = np.vstack((bb['Center'].to_numpy()[0], surface_points))
#     dip = np.append(bb['Dip (deg)'].to_numpy(), dip)
#     azimuth = np.append(bb['Azimuth (deg)'].to_numpy(), azimuth)
#     return surface_points, dip, azimuth
#
# surface_points, dip, azimuth = get_bb_points()
# fixed_points = surface_points
# fixed_orientations = (surface_points, np.c_[azimuth, dip])
#%%
from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
                                                   set_fracture_solver,
                                                   forward_operator,
                                                   run_inversion,
                                                   run_inversion_per_profile)

#%%
solv = FracEM()
solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
# solv.engine = 'tensor_trace'
solv.engine = 'dummy'
solv.units = 'SI'
# solv.backend = 'torch'
solv.backend = 'numpy'

#%%
set_fracture_geometry(frac)
set_fracture_solver(solv)
#%%
kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81,
                                  set_properties=True)

kwargs_forward_solver = dict(plane=1,
                             order=2,
                             max_element_size=max_element_size)

kwargs_cost_function = dict(tol_all=30, # Tolerance for overall RMS error over the whole picks
                            tol_pos=10,# inversion_tolerance, # Tolerance for a single trace
                            max_iter_all=100, # Maximum amount of control points
                            max_iter_pos=10, # maximum number of trials per position
                            surrounding=10, # ignore traces in this surrounding
                            step_speed=15) # higher the number the step will be smaller

solv_out = None
fixed_points_geometry = None
pick_true = np.vstack(picks.values())[:,1]
fixed_points_geometry, solv_out = run_inversion(pick_true=pick_true,
                                            fixed_points_geometry=fixed_points,
                                            fixed_oientations_geometry=fixed_orientations,
                                            constrained_points_geometry=None,
                                            filename=filename,
                                            plot_step=True,
                                            kwargs_fracture_properties=kwargs_fracture_properties,
                                            kwargs_forward_solver=kwargs_forward_solver,
                                            kwargs_cost_function=kwargs_cost_function)



np.save(OUTPUT_DIR+'bb_real/output_points.npy', fixed_points_geometry)

#%%

p = model.ModelPlot()
p.show(zxGrid2=False, yzGrid2=False, xyGrid=True)
# Antennas positions
ant = Antenna()
ant.load_hdf5(filename)
p.add_object(ant.Transmitter, name='Tx', color='red')
p.add_object(ant.Receiver, name='Rx', color='blue')
# Starting geometry
frac_start = FractureGeo()
frac_start.load_hdf5(OUTPUT_DIR + 'bb/inversion_bb_iter_1.h5')
p.add_object(frac_start.surface, name='geometry_start',
             color='red',
             opacity=0.5,
             label='Starting model')
p.remove('geometry_start')
# Ending geometry
frac_out = FractureGeo()
if solv_out is None:
    frac_out.load_hdf5(OUTPUT_DIR + 'bb_real/inversion_bb_pos_1404_iter_10.h5')
else:
    frac_out.load_hdf5(solv_out._filename)
p.add_object(frac_out.surface, name='geometry_simulated',
             color='blue',
             opacity=0.5, label='Simulated model')
p.remove('geometry_simulated')

# # Real geometry
frac_true = FractureGeo()
frac_true.load_hdf5(OUTPUT_DIR + 'bb/inversion_bb.h5')
p.add_object(frac_true.surface, name='geometry_real', color='green',
             opacity=0.5, label='Real model', )
p.remove('geometry_real')
# if fixed_points_geometry is None:
#     fixed_points_geometry = np.load(OUTPUT_DIR+'bb/output_points.npy')
#
# p.add_object(pv.PolyData(fixed_points_geometry), name='cp', label='Control Points')

p.add_legend()