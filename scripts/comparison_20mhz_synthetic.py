import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
import scipy.io as sio

from fracwave.geometry import FractureGeo, Antenna
from fracwave.solvers import SourceEM, FracEM
from fracwave.utils.help_decorators import convert_frequency

from fracwave import DATA_DIR, OUTPUT_DIR
from bedretto import data, geometry, model
from gdp.processing.gain import apply_gain
import time

# file = OUTPUT_DIR + 'cluster_MB1_BB_20MHz_high_resol.h5'
# file = OUTPUT_DIR + 'cluster_results/output/MB1_BB_20MHz_high_resol_shift_right4.h5'
# file = OUTPUT_DIR + 'cluster_results/output/MB1_BB_20MHz_more_elements.h5'
file = OUTPUT_DIR + 'cluster_raster_results/branch_fault.h5'
# file = OUTPUT_DIR + 'test_1.h5'

#%%
freq = np.load(file.replace('.h5', '_summed_response.npy'))

#%%
solv = FracEM()
solv.load_hdf5(file)

geom = FractureGeo()
geom.load_hdf5(file)

sou = SourceEM()
sou.load_hdf5(file)

ant = Antenna()
ant.load_hdf5(file)
#%%
time_response, time_vector = solv.get_ifft(freq)


#%%
distance = 70
vb = 0.1234 # m/ns
#cut_time = distance / vb
#mask = np.where((time_vector/2) <= cut_time)
#tm = time_vector[mask] /2
#tr = time_response.T[mask]
#s = slice(None)#0,300)
vmax = np.abs(time_response).max() / 30
extent = [0, 200, time_vector[-1] * vb / 2, 0]
plt.title('Peak frequency: {}'.format(convert_frequency(sou.peak_frequency * 1e9)))#file.split('/')[-1])
plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, extent=extent)
plt.ylim(distance, 0)
plt.ylabel('Radial distance from borehole [m]')
plt.xlabel('Borehole depth [m]')
plt.colorbar()
plt.show()

#%%
distance = 70
vb = 0.1234 # m/ns
tm = time_vector /2
tr = time_response.T
tr = tr / tr.max(axis=0)

vmax = 1
extent = [0, 200, tm[-1] * vb, 0]
plt.title('Peak frequency: {}'.format(convert_frequency(sou.peak_frequency * 1e9)))#file.split('/')[-1])
plt.ylim(distance, 0)

plt.imshow(tr, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, extent=extent)
plt.ylabel('Radial distance from borehole [m]')
plt.xlabel('Borehole depth [m]')
plt.colorbar()
plt.show()

#%%
trace = 600
s = slice(None)#0,300)
plt.plot(time_vector[s], time_response[trace, s],'green', label='loop')

plt.xlabel('Time [ns]')
plt.ylabel('Amplitude [a.u.]')
plt.show()

#%%
p = model.ModelPlot()
#%%
trace = 0
energy = solv.file_read('fracture', sl=((trace),))
geom.surface['response'] = energy

p.add_object(geom.surface, name='fracture', cmap = 'viridis', scalars='response')

#%%
from scipy.io import loadmat
file = '/home/daniel/Documents/gpr_data_mb1/' + 'MB1_20MHz_B200108.mat'
data = loadmat(file)

travel_time = data['travel_time'][0]
depth = data['depth'][:,0]
data_raw = data['raw_data']
processed_data = data['processed_data']

#%%
extent = [depth[0], depth[-1], travel_time[-1], travel_time[0]]
vmax = processed_data.max() /10
plt.imshow(processed_data, cmap = 'seismic', vmin=-vmax,vmax=vmax, extent=extent, aspect='auto')
plt.colorbar()
plt.show()

#%%
time_cut = (1000,100)
depth_cut = (20,200)

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))
#travel_time2 = travel_time[tt_cut]
extent = [depth[0], depth[-1], travel_time[-1], travel_time[0]]

vmax = processed_data.max() /10
c1 = ax1.imshow(processed_data, cmap = 'seismic', vmin=-vmax,vmax=vmax, extent=extent, aspect='auto')
ax1.set_ylim(time_cut)
ax1.set_xlim(depth_cut)
ax1.set_ylabel('Time [ns]')
ax1.set_xlabel('Borehole depth [m]')
ax1.set_title('20MHz Processed data')



fig.colorbar(c1, ax=ax1)


vmax = np.abs(time_response).max() / 50
extent = [0, 200, time_vector[-1], 0]
ax2.set_title('Dipole-Model Simulation')
ax2.set_ylim(time_cut)
ax2.set_xlim(depth_cut)
c2 = ax2.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, extent=extent)
ax2.set_ylabel('Time [ns]')
ax2.set_xlabel('Borehole depth [m]')
fig.colorbar(c2, ax=ax2)

ax1.axhline(y=400, color='blue', linestyle='--')
ax2.axhline(y=400, color='blue', linestyle='--')
ax1.axvline(x=90,  color='blue', linestyle='--')
ax2.axvline(x=90,  color='blue', linestyle='--')



plt.show()

#%%

time_cut = (1000,100)
depth_cut = (20,200)

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))
#travel_time2 = travel_time[tt_cut]
extent = [depth[0], depth[-1], travel_time[-1], travel_time[0]]

vmax = processed_data.max() /10
c1 = ax1.imshow(processed_data, cmap = 'seismic', vmin=-vmax,vmax=vmax, extent=extent, aspect='auto')
ax1.set_ylim(time_cut)
ax1.set_xlim(depth_cut)
ax1.set_ylabel('Time [ns]')
ax1.set_xlabel('Borehole depth [m]')
ax1.set_title('20MHz Processed data')



fig.colorbar(c1, ax=ax1)


vmax = np.abs(time_response).max() / 50
extent = [0, 200, time_vector[-1], 0]
ax2.set_title('Dipole-Model Simulation')
ax2.set_ylim(time_cut)
ax2.set_xlim(depth_cut)
c2 = ax2.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, extent=extent)
ax2.set_ylabel('Time [ns]')
ax2.set_xlabel('Borehole depth [m]')
fig.colorbar(c2, ax=ax2)

ax1.axhline(y=400, color='blue', linestyle='--')
ax2.axhline(y=400, color='blue', linestyle='--')
ax1.axvline(x=90,  color='blue', linestyle='--')
ax2.axvline(x=90,  color='blue', linestyle='--')



plt.show()


#%%
new_time, g_m = apply_gain(time_response.T, sfreq=sou.sampling_frequency, exponent=2,
                           gain_type='spherical', velocity=0.1234, twoway=True)

time_cut = (100,0)
depth_cut = (20,200)

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))
#travel_time2 = travel_time[tt_cut]
extent = [depth[0], depth[-1], travel_time[-1], travel_time[0]]

pd = processed_data.copy()
vmax = np.abs(pd).max()

pd /= vmax /8
vmax=1
c1 = ax1.imshow(pd, interpolation='none', cmap = 'seismic', vmin=-vmax,vmax=vmax, extent=extent, aspect='auto')
ax1.set_ylim(time_cut)
ax1.set_xlim(depth_cut)
ax1.set_ylabel('Time (ns)')
ax1.set_xlabel('Borehole depth (m)')
ax1.set_title('20MHz Processed data')



fig.colorbar(c1, ax=ax1)

nt = new_time.copy()
vmax = np.abs(nt).max()

nt /= vmax
vmax = 1
extent = [0, 200, time_vector[-1], 0]
ax2.set_title('Dipole-Model Simulation')
ax2.set_ylim(time_cut)
ax2.set_xlim(depth_cut)
c2 = ax2.imshow(nt, interpolation='none', aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, extent=extent)
ax2.set_ylabel('Time (ns)')
ax2.set_xlabel('Borehole depth (m)')
fig.colorbar(c2, ax=ax2)

ax1.axhline(y=400, color='blue', linestyle='--')
ax2.axhline(y=400, color='blue', linestyle='--')
ax1.axvline(x=90,  color='blue', linestyle='--')
ax2.axvline(x=90,  color='blue', linestyle='--')

plt.tight_layout()
fig.savefig('/home/daniel/GitProjects/Research-Plan-/figures/FZ1 repsonse.png', transparent=True)

plt.show()

#%%
tracae = 200
plt.plot(time_vector, new_time[:, 600]/ new_time[:, 600].max(), label='With gain')
plt.plot(time_vector, time_response.T[:, 600] / time_response.T[:, 600].max(), label='No gain')
plt.legend()
plt.show()
