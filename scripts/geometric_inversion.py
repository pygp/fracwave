import os
import numpy as np
import shutil
import pandas as pd
from bedretto import model
os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cpu"
import gempy as gp

import pyvista as pv

import matplotlib.pyplot as plt
from fracwave import OUTPUT_DIR, FracEM, FractureGeo, Antenna, SourceEM
from fracwave import set_logger
logger = set_logger(__name__)

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1 # m
min_element_size = wavelength/6
p1 = (70, 90, 0)

filename_curved = OUTPUT_DIR + 'dummy_step/dummy_curved.h5'
filename_plane = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'

#%%
def init_geo_model(extent, resolution):
    geo_model = gp.create_model()
    geo_model = gp.init_data(geo_model, extent=extent, resolution=resolution)
    gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
    geo_model.add_surfaces(['F', "basement"])
    return geo_model

def _create_geometry(geo_model,
                    fixed_points,
                    fixed_orientations,
                    move_points,
                    id_points_fixed: list=[],
                    id_points_move:list=[],
                    id_orientation_fixed: list=[],
                    id_orientation_move: list=[],
                    ):
    """
    Make the update of the geometry
    Args:
        geo_model: Same geo_model so it is initiallice once
        fixed_points: Points that will not move like the points from logging or tunnel measurements
        fixed_orientations: The orientations (Azimuth and dip) of the points from logging and tunnel measurements.
        move_points: These are the proposed points that will shape the geometry and help constrain it
        extent: Bounding box for the fault geometry
        max_element_size: In the remeshing step, the size of the elements will not be bigger that this value (in meters)
        id_points_fixed: index number of the fixed points. This to not remove them
        id_points_move: index number of points to delete and or move
        id_orinetation_fixed:
        id_orientation_move:
        set_properties: boolean to indicate if properties are set to each element of the geometry
    Returns:
        Simulated response of the fracture
    """
    # Include all fixed points and orientations
    logger.debug(f'Setting {len(fixed_points)} fixed points and orientations from borehole '
                f'logging and tunnel observations')
    s = len(id_points_fixed)
    for i in range(len(fixed_points)):
        if s>0:
            if fixed_points[i] in geo_model.surface_points.df[['X', 'Y', 'Z']].to_numpy():
                continue
        geo_model.add_surface_points(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F')
        geo_model.add_orientations(X=fixed_orientations[0][i, 0], Y=fixed_orientations[0][i, 1], Z=fixed_orientations[0][i, 2], surface='F',
                                   orientation=[fixed_orientations[1][i, 0], fixed_orientations[1][i, 1], 1])
        id_points_fixed.append(geo_model.surface_points.df.index[-1])
        id_orientation_fixed.append(geo_model.surface_points.df.index[-1])
    for i in range(len(fixed_orientations[0])):
        geo_model.add_orientations(X=fixed_orientations[0][i, 0], Y=fixed_orientations[0][i, 1],
                                   Z=fixed_orientations[0][i, 2], surface='F',
                                   orientation=[fixed_orientations[1][i, 0], fixed_orientations[1][i, 1], 1])
        id_orientation_fixed.append(geo_model.surface_points.df.index[-1])
    # delete all other points
    extra_points = list(geo_model.surface_points.df.index)
    [extra_points.remove(i) for i in id_points_fixed if i in extra_points]
    [geo_model.delete_surface_points(i) for i in extra_points if i in list(geo_model.surface_points.df.index)]
    # Include all other points
    if move_points is not None:
        logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
        # if len(id_points_move)>0:
        #     [geo_model.delete_surface_points(i) for i in id_points_move]
        #     id_points_move = []
        for i in range(len(move_points)):
            geo_model.add_surface_points(X=move_points[i, 0], Y=move_points[i, 1], Z=move_points[i, 2], surface='F')
            id_points_move.append(geo_model.surface_points.df.index[-1])
        # TODO: extra orientations
    gp.compute_model(geo_model)
    logger.debug('Model computed')
    fixed_oientations_geometry = fixed_or.copy()
    filename = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'
    pick, time_response, time_vector, solv = forward_operator(constrain_gempy_points=constrained_points_geometry.ravel(),
                                                              fixed=fixed_points_geometry,
                                                              fixed_or=fixed_oientations_geometry,
                                                              filename=filename)
    error_all = 100

    max_iter = 0
    dtime = solv.file_read_attribute('source/dtime')
    tol = 1
    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]
    while error_all > tol and max_iter < 20:
        dif = np.abs(pick_true - pick)
        pos_sorted = np.argsort(dif)
        pos_sorted_after = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
        pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error
        # Now that we have the max position whe identify the elements from which the borehole is sensitive
        sample_diff = pick_true[pos] - pick[pos]
        step = sample_diff * dtime * v / 2

        file = filename.split('.')[0]
        if not os.path.isfile(file + f'_pos_{pos}.h5'):
            shutil.copy2(filename, file + f'_pos_{pos}.h5')
        file = file + f'_pos_{pos}.h5'
        error_pos = 100
        max_iter2 = 0
        tol = 0.2
        while error_pos > tol and max_iter2 < 10:
            new_point = get_position_element(step/3,
                                             solv.file_read('mesh/geometry/midpoint'),
                                             solv.file_read('antenna/Tx')[pos],
                                             solv.file_read('antenna/Rx')[pos])

            logger.debug(f'New point: {new_point}')
            pick, time_response, time_vector, solv = forward_operator(new_point.ravel(),
                                                                      fixed_points_geometry,
                                                                      fixed_oientations_geometry,
                                                                      filename=file)
            sample_diff = pick_true[pos] - pick[pos]
            step = sample_diff * dtime * v / 2
            error_pos = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
            error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())

            logger.info(f'\n+++++++++++++++++RMS: {error_pos}, for pos:{pos}, iter:{max_iter2}+++++++++++++++++\n')
            plt.plot(pick, 'b', label='Simulated')
            plt.plot(pick_curved, 'g--', label='Objective')
            plt.plot(pick_plane, 'r--', label='Starting')
            plt.vlines(pos, color ='black', ymin=60, ymax=120)
            if len(already_optimized_points) > 0:
                [plt.vlines(p, color='red', ymin=60, ymax=120, alpha=0.5) for p in already_optimized_points]
            # plt.plot(pick_plan[:,1], 'r--', label='Objective')
            plt.ylim(60, 120)
            plt.xlim(-1, 35)
            plt.xlabel('Traces')
            plt.ylabel('Samples')
            plt.title(f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}, RMS all: {error_all:.2f}' )
            plt.legend()
            # Build gif
            filename_pic = OUTPUT_DIR + f'dummy_step/pics/{c}.png'
            filenames_pics.append(filename_pic)

            # save frame
            plt.savefig(filename_pic)
            plt.show()
            max_iter2+=1
            p.add_object(solv.file_read('mesh/geometry/midpoint'), name='mesh2', color='yellow', opacity=0.5,
                         label='Inverted Geometry')
            p.add_object(pv.PolyData(fixed_points_geometry), name='point_fixed', color='black', point_size=20)
            p.add_object(pv.PolyData(new_point), name='point', color='blue', point_size=15)
            if error_pos < tol:
                fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
            if max_iter2 == 10 or error_pos < tol:
                surrounding = 3
                [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding)]
            c+=1
        # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        logger.debug(f'\n+++++++++++++++++RMS: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
        max_iter +=1
    vertices = geo_model.solutions.vertices[0]
    faces = geo_model.solutions.edges[0]
    return vertices, faces, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move

def create_geometry(geo_model,
                    fixed_points,
                    fixed_orientations,
                    move_points,
                    id_points_fixed: list=[],
                    id_points_move:list=[],
                    id_orientation_fixed: list=[],
                    id_orientation_move: list=[],
                    ):
    """
    Make the update of the geometry
    Args:
        geo_model: Same geo_model so it is initiallice once
        fixed_points: Points that will not move like the points from logging or tunnel measurements
        fixed_orientations: The orientations (Azimuth and dip) of the points from logging and tunnel measurements.
        move_points: These are the proposed points that will shape the geometry and help constrain it
        extent: Bounding box for the fault geometry
        max_element_size: In the remeshing step, the size of the elements will not be bigger that this value (in meters)
        id_points_fixed: index number of the fixed points. This to not remove them
        id_points_move: index number of points to delete and or move
        id_orinetation_fixed:
        id_orientation_move:
        set_properties: boolean to indicate if properties are set to each element of the geometry
    Returns:
        Simulated response of the fracture
    """
    # Include all fixed points and orientations
    logger.debug(f'Setting {len(fixed_points)} fixed points, {len(fixed_orientations[0])} orientations from borehole '
                f'logging and tunnel observations')

    [geo_model.delete_orientations(i) for i in list(geo_model.orientations.df.index)]
    [geo_model.delete_surface_points(i) for i in list(geo_model.surface_points.df.index)]

    for i in range(len(fixed_points)):
        geo_model.add_surface_points(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F')
        # id_points_fixed.append(geo_model.surface_points.df.index[-1])

    for i in range(len(fixed_orientations[0])):
        geo_model.add_orientations(X=fixed_orientations[0][i, 0], Y=fixed_orientations[0][i, 1],
                                   Z=fixed_orientations[0][i, 2], surface='F',
                                   orientation=[fixed_orientations[1][i, 0], fixed_orientations[1][i, 1], 1])
        # id_orientation_fixed.append(geo_model.surface_points.df.index[-1])
    # delete all other points
    # Include all other points
    if move_points is not None:
        logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
        # if len(id_points_move)>0:
        #     [geo_model.delete_surface_points(i) for i in id_points_move]
        #     id_points_move = []
        for i in range(len(move_points)):
            geo_model.add_surface_points(X=move_points[i, 0], Y=move_points[i, 1], Z=move_points[i, 2], surface='F')
            # id_points_move.append(geo_model.surface_points.df.index[-1])
        # TODO: extra orientations
    gp.compute_model(geo_model)
    logger.debug('Model computed')
    vertices = geo_model.solutions.vertices[0]
    faces = geo_model.solutions.edges[0]
    return vertices, faces, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move


def mesh_gempy_surface(vertices, max_element_size, set_properties: bool=True, plane=0, order=2):
    from fracwave import FractureGeo
    frac = FractureGeo()

    resolution = (20, 20)
    surf = frac.remesh(vertices, plane=plane, order=order, spacing=resolution, max_element_size=max_element_size)
    min_resol = np.ceil(surf.length / max_element_size).astype(int)
    resolution = (min_resol, min_resol)
    if surf.dimensions[0] < min_resol:
        logger.debug('Remeshing...')
        surf = frac.remesh(vertices, plane=plane, order=order, spacing=resolution)
    if set_properties:
        logger.debug(f'Setting properties to {frac.nelements} elements. This can take a few minutes...')
        df = frac.set_fracture_properties(aperture=0.5,
                                          electrical_conductvity=0.001,
                                          electrical_permeability=81)
    logger.debug('Done')
    return frac, surf

def surface_from_points(vertices, faces):
    faces_all_stacked = np.hstack([np.hstack([len(f), f]) for f in faces])
    surface = pv.PolyData(vertices, faces_all_stacked)
    return surface

def solv_frac(file):
    from fracwave import FracEM
    solv = FracEM()
    solv.rock_epsilon = 6.
    solv.rock_sigma = 0.0
    # solv.engine = 'tensor_all'
    solv.engine = 'dummy'
    # solv.engine = 'tensor_trace'
    # solv.engine = 'tensor_numba'

    solv.units = 'SI'
    solv.filter_energy = True
    solv._filter_percentage = 0.5
    # solv.backend = 'torch'
    solv.backend = 'numpy'

    solv.open_file(file)
    # solv._fast_calculation_incoming_field = True
    # solv.mode = 'incoming_field'

    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    return solv, freq


def forward_operator(constrain_gempy_points, fixed, fixed_or, filename):
    """

    Returns:

    """
    constrain_points = np.zeros((len(constrain_gempy_points) //3, 3))
    c = 0
    for i, p in enumerate(constrain_gempy_points):
        val = i % 3
        constrain_points[c, val] = p
        if val == 2:
            c += 1

    # filename = OUTPUT_DIR + 'synthetic_case_simulation_curved.h5'
    file = filename.split('.')[0]
    if not os.path.isfile(file+'_iter_0.h5'):
        shutil.copy2(filename, file+'_iter_0.h5')
    c = 0
    while True:
        filename = file+ f'_iter_{c}.h5'
        if os.path.isfile(filename):
            filename_next = file + f'_iter_{c+1}.h5'
            if os.path.isfile(filename_next):
                c += 1
                continue
            shutil.copy2(filename, filename_next)
            filename = filename_next
            break
        else:
            shutil.copy2(file+ f'_iter_{c-1}.h5', filename)
            break

    # Is this really neccesary?
    id_points_fixed2 = []
    id_orientation_fixed2 = []
    id_points_move2 = []
    # id_orientation_move1=[]
    vertices, faces, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move = \
        create_geometry(geo_model=geo_model,
                        fixed_points=fixed,
                        fixed_orientations=fixed_or,
                        move_points=constrain_points,
                        id_points_fixed=id_points_fixed2,
                        id_orientation_move=id_orientation_fixed2,
                        id_points_move=id_points_move2,
                        )
    # gemp_points = geo_model.surface_points.df[['X', 'Y', 'Z']].to_numpy()

    frac, surf = mesh_gempy_surface(vertices=vertices,
                                    max_element_size=min_element_size,
                                    plane=0,
                                    set_properties=True,
                                    order=2)
    frac.export_to_hdf5(filename, overwrite=True)

    # return solv_frac(filename)

    solv, freq = solv_frac(filename)
    if solv.engine != 'dummy':
        from gdp.processing.image_processing import pick_first_arrival
        time_response, time_vector = solv.get_ifft(freq)
        pick = pick_first_arrival(time_response.T, threshold=threshold)
        # np.save(filename.split('.')[0] + '_pick.npy', pick)
    else:
        time_response = None
        time_vector = None
        pick = solv.file_read('dummy_solution/first_arrival')
    # error = np.sqrt(np.square(np.subtract(pick_plan[:,1], pick[:,1])).mean())
    # error = np.sqrt(np.square(np.subtract(pick_curved[:,1], pick[:,1])).mean())
    # logger.info(f'Root Mean Squared error: {error}')
    # return pick, time_response, time_vector
    return pick, time_response, time_vector, solv


def objective_function(constrain_gempy_points, pick_true, fixed, fixed_or, filename):
    logger.info(f'Testing for constrain points: \n{constrain_gempy_points}')
    pick, time_response, time_vector, solv = forward_operator(constrain_gempy_points.ravel(),
                                                              fixed, fixed_or, filename)
    error = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
    logger.info(f'RMS: {error}')
    return error

def get_minimum_position(data_sim, data_true, delta_t, velocity):
    """
    Get the position of the biggest difference and a distance
    Args:
        data_sim: Simulated travel time
        data_true: True travel time
        delta_t: time per sample [ns / sample]
        velocity: Velocity of EM [m / ns]

    Returns:

    """
    dif = np.abs(data_true - data_sim)
    pos = np.where(dif == dif.max())[0][0]  # Take the first found value
    # Now that we have the max position whe identify the elements from which the borehole is sensitive
    sample_diff = data_true[pos] - data_sim[pos]
    step = sample_diff * delta_t * velocity / 2
    # Positive step means move towards borehole
    # Negative step means move away borehole
    return pos, step



def optimize_point(point, picked_true, fixed, fixed_or, filename ):
    from scipy.optimize import minimize
    res = minimize(fun=objective_function,
                   x0=point.ravel(),
                   args=(picked_true, fixed, fixed_or, filename),
                   #bounds=bounds,
                   method='nelder-mead',
                   options={  # 'maxfun': 10,
                       'xatol': 1,
                       'disp': True})
    return res

def run_simulation(pick_true, fixed_points_geometry, fixed_oientations_geometry, constrained_points_geometry):
    fixed_points_geometry = fixed.copy()
    fixed_oientations_geometry = fixed_or.copy()
    filename = OUTPUT_DIR + 'dummy_step/dummy_plane.h5'
    pick, time_response, time_vector, solv = forward_operator(constrained_points_geometry.ravel(),
                                                              fixed_points_geometry,
                                                              fixed_oientations_geometry,
                                                              filename=filename)
    error_all = 100

    max_iter = 0
    dtime = solv.file_read_attribute('source/dtime')
    tol_all = 0.5
    c=0 # Counter
    filenames_pics = []
    already_optimized_points =[]
    fix_points =[]
    while error_all > tol_all and max_iter < 20:
        dif = np.abs(pick_true - pick)
        pos_sorted = np.argsort(dif)
        pos_sorted_after = np.asarray([i for i in pos_sorted if i not in already_optimized_points])
        pos = pos_sorted_after[-1]  # Take the last value which corresponds to the biggest error
        # Now that we have the max position whe identify the elements from which the borehole is sensitive
        sample_diff = pick_true[pos] - pick[pos]
        step = sample_diff * dtime * v / 2

        file = filename.split('.')[0]
        if not os.path.isfile(file + f'_pos_{pos}.h5'):
            shutil.copy2(filename, file + f'_pos_{pos}.h5')
        file = file + f'_pos_{pos}.h5'
        error_pos = 100
        max_iter2 = 0
        tol = 0.3
        while error_pos > tol and max_iter2 < 10:
            new_point = get_position_element(step/3,
                                             solv.file_read('mesh/geometry/midpoint'),
                                             solv.file_read('antenna/Tx')[pos],
                                             solv.file_read('antenna/Rx')[pos])

            logger.debug(f'New point: {new_point}')
            pick, time_response, time_vector, solv_new = forward_operator(new_point.ravel(),
                                                                          fixed_points_geometry,
                                                                          fixed_oientations_geometry,
                                                                          filename=file)
            sample_diff = pick_true[pos] - pick[pos]
            step = sample_diff * dtime * v / 2

            logger.info(f'\n+++++++++++++++++RMS: {error_pos}, for pos:{pos}, iter:{max_iter2}+++++++++++++++++\n')
            e = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
            if e < error_pos:
                solv = solv_new
            else:
                step /= 2
            error_pos = np.sqrt(np.square(np.subtract(pick_true[pos], pick[pos])).mean())
            error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
            plt.plot(pick, 'blue', label='Simulated')
            plt.plot(pick_curved, 'g--', label='Objective', alpha=0.3)
            plt.plot(pick_plane, 'r--', label='Starting', alpha=0.3)
            plt.vlines(pos, color ='black', ymin=60, ymax=120, label='Current position')
            if len(fix_points) > 0:
                plt.vlines(fix_points, color='green', linestyles='--', ymin=60, ymax=120, alpha=0.3, label='Fixed points')
            # plt.plot(pick_plan[:,1], 'r--', label='Objective')
            plt.ylim(60, 120)
            plt.xlim(-1, 35)
            plt.xlabel('Traces')
            plt.ylabel('Samples')
            plt.title(f'Position/Iter: {pos}/{max_iter2}, Total iter:{c}\nRMS position: {error_pos:.2f}, RMS all: {error_all:.2f}' )
            plt.legend()
            # Build gif
            filename_pic = OUTPUT_DIR + f'dummy_step/pics/{c}.png'
            filenames_pics.append(filename_pic)

            # save frame
            plt.savefig(filename_pic)
            plt.show()
            max_iter2+=1
            p.add_object(solv_new.file_read('mesh/geometry/midpoint'), name='mesh2', color='yellow', opacity=0.5,
                         label='Inverted Geometry')
            p.add_object(pv.PolyData(fixed_points_geometry), name='point_fixed', color='black', point_size=20)
            p.add_object(pv.PolyData(new_point), name='point', color='blue', point_size=15)
            if error_pos < tol:
                fixed_points_geometry = np.vstack((fixed_points_geometry, new_point))
                fix_points.append(pos)
            if max_iter2 == 10 or error_pos < tol:
                surrounding = 3
                [already_optimized_points.append(i) for i in np.arange(pos - surrounding, pos + surrounding+ 1)]
            c+=1
        # error_all = np.sqrt(np.square(np.subtract(pick_true, pick)).mean())
        logger.debug(f'\n+++++++++++++++++RMS: {error_all}, after iter:{max_iter}+++++++++++++++++\n')
        max_iter +=1

    import imageio
    with imageio.get_writer(OUTPUT_DIR + 'dummy_step/pics/mygif.gif', mode='I') as writer:
        for fnp in filenames_pics:
            image = imageio.v2.imread(fnp)
            writer.append_data(image)
            os.remove(fnp)
        print('Ready')

# p.add_object(pv.PolyData(point_high_energy), name='test1', point_size=12)
def create_bounds(points, extent):
    bounds = []
    for ir, row in enumerate(points):
        for ic, col in enumerate(row):
            index = ic % 3
            if index == 0: # x
                b = (extent[0], extent[1])
            elif index == 1: # y
                b = (extent[2], extent[3])
            elif index == 2: # z
                b = (extent[4], extent[5])
            bounds.append(b)
    return bounds


#%%
# import shutil
#
# from gdp.processing.image_processing import pick_first_arrival
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)
resolution = (50,50,50)
geo_model = init_geo_model(gempy_extent, resolution)
fixed = np.asarray([(0,0,p1[-1])])
fixed_or = [fixed.copy(), np.asarray([(p1[1],p1[0],1)])]

# Initial Guess


threshold = 0.02
#%%
curved = np.asarray([[0.89864941,  0.12210877, -3.37883444],
                                     [0.93922058,  0.27997581, -2.02387851],
                                     [-0.07264459,  0.30187218,  0.04356908],
                                     [0.02385303, -0.14218273,  0.96832576]])

np.asarray([[ 3.23754889e-01,  3.50706333e-04, -2.03992868e+00],
 [8.92493718e-01,  5.61138347e-03, -1.94710497e+00],
 [2.48437307e-01, -7.23622450e-03,  -4.65674211e-01],
 [4.77223790e-02, -4.05177425e-04,  1.41951776e+00]])

planar = np.asarray([[1.20713, 0, -




3.3165622],
                          [0.653862, 0, -1.7964711],
                          [0.15089116, 0, -0.41457027],
                          [-0.3520796, 0, 0.96733063]])

# pick, time_response, time_vector, solv = forward_operator(planar.ravel(), fixed, fixed_or, filename=filename_plane)
# np.save(OUTPUT_DIR + 'pick_plane_dummy.npy', pick)
#%%
# pick_curved = np.load(OUTPUT_DIR + 'dummy_nelder_mead/pick_curved_dummy.npy')
# pick_plane = np.load(OUTPUT_DIR + 'dummy_nelder_mead/pick_plane_dummy.npy')
pick_curved = np.load(OUTPUT_DIR + 'dummy_step/pick_curved_dummy.npy')
pick_plane = np.load(OUTPUT_DIR + 'dummy_step/pick_plane_dummy.npy')
error = objective_function(planar.ravel(), pick_curved, fixed, fixed_or, filename_plane)

#%%
bounds = create_bounds(planar, gempy_extent)
#%%
from scipy.optimize import minimize

res = minimize(fun=objective_function,
               x0=planar.ravel(),
               args=(pick_curved, fixed, fixed_or, filename_plane),
               bounds=bounds,
               method='nelder-mead',
               options={#'maxfun': 10,
                        'xatol': 1,
                        'disp': True}
               )

#%% Try differential evolution
from scipy.optimize import differential_evolution
result = differential_evolution(func=objective_function,
                                #x0=planar.ravel(),
                                args=(pick_curved, fixed, fixed_or, filename_plane),
                                bounds=bounds,
                                tol=1)

#%% Read the values
max_iter = 467

from fracwave import FracEM
import matplotlib.pyplot as plt
# fig, ax = plt.subplots()
x = []
y = []
all_picks = []
solv = FracEM()
for i in range(1, max_iter):
    # filename = OUTPUT_DIR+f'synthetic_case_simulation_curved_iter_{i}.h5'
    filename = OUTPUT_DIR+f'dummy_nelder_mead/dummy_plane_iter_{i}.h5'


    solv.load_hdf5(filename)
    # freq = solv.file_read('dummy_/summed_response')
    # time_response, time_vector = solv.get_ifft(freq)
    # pick = pick_first_arrival(time_response.T, threshold=threshold)
    pick = solv.file_read('dummy_solution/first_arrival')
    all_picks.append(pick)
    # all_picks.append(pick[:,1])
    error = np.sqrt(np.square(np.subtract(pick_curved, pick)).mean())
    # error = np.sqrt(np.square(np.subtract(pick_plan[:,1], pick[:,1])).mean())

    x.append(i)
    y.append(error)

#%%
all_picks = np.asarray(all_picks)
from sklearn.metrics import mean_squared_error
# rmse = [mean_squared_error(pick_plan[:,1],pic, squared=False) for pic in all_picks]
rmse = [mean_squared_error(pick_curved, pic, squared=False) for pic in all_picks]

#%%
plt.plot(x, rmse)
plt.title('Inversion using Scipy Nelder-Mead')
plt.xlabel('# iterations')
plt.ylabel('RMSE')
plt.show()

#%%
filenames_pics = []
for i in range(1, max_iter):
    # plot the line chart
    # plt.plot(all_picks[i], 'b', label='Simulated')
    i=50
    filename = OUTPUT_DIR + f'dummy_nelder_mead/dummy_plane_iter_{i}.h5'

    solv.load_hdf5(filename)
    # freq = solv.file_read('dummy_/summed_response')
    # time_response, time_vector = solv.get_ifft(freq)
    # pick = pick_first_arrival(time_response.T, threshold=threshold)
    pick = solv.file_read('dummy_solution/first_arrival')
    plt.plot(pick, 'b', label='Simulated')
    plt.plot(pick_curved, 'r--', label='Objective')
    # plt.plot(pick_plan[:,1], 'r--', label='Objective')
    plt.ylim(60, 120)
    plt.xlabel('Traces')
    plt.ylabel('Samples')
    plt.title(f'Iteration: {i}')
    plt.legend()
    plt.show()

    # create file name and append it to a list
    filename_pic = OUTPUT_DIR + f'{i}.png'
    filenames_pics.append(filename_pic)

    # save frame
    plt.savefig(filename_pic)
    plt.close()  # build gif

import imageio
with imageio.get_writer(OUTPUT_DIR + 'mygif.gif', mode='I') as writer:
    for fnp in filenames_pics:
        image = imageio.v2.imread(fnp)
        writer.append_data(image)

# Remove files
for fi in set(filenames_pics):
    os.remove(fi)
#%%
p = model.ModelPlot(title='Inversion results')
solv = FracEM()
solv.load_hdf5(filename_plane)

p.add_object(solv.file_read('antenna/Tx'), name='Tx', color='Red')
p.add_object(solv.file_read('antenna/Rx'), name='Rx', color='blue')

p.add_object(solv.file_read('mesh/geometry/midpoint'), name='mesh1', color='red', opacity=0.5, label ='Initial Geometry')

i = 50
solv.load_hdf5(filename_plane.split('.')[0]+f'_pos_34_iter_14.h5')
p.add_object(solv.file_read('mesh/geometry/midpoint'), name='mesh2', color='yellow', opacity=0.5, label ='Inverted Geometry')
solv.load_hdf5(filename_curved)
p.add_object(solv.file_read('mesh/geometry/midpoint'), name='mesh3', color='green', opacity=0.5, label ='Real Geometry')
p.add_legend()
p.show(zxGrid2=False, yzGrid2=False)
# p.add_object(frac2.surface, name='fault2', color='green', opacity=1)
from bedretto.core.experiments import GPRtools

# gpr = GPRtools()
# GPR_cone, int_depth, int_radius = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
#                                                                         radius=((pick[:,1] * solv.file_read_attribute('source/dtime'))/2)*v,
#
#                                                                         # Radial distance
#                                                                         # radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         trajectory=bor_df,
#                                                                         depth_resolution=0.1,
#                                                                         radial_resolution=0.1
#                                                                         )
# p.add_object(pv.PolyData(GPR_cone), color='black', name='GPR cone', opacity=0.3)
#
# picked = solv.file_read('dummy_solution/first_arrival')
# GPR_cone, int_depth, int_radius = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
#                                                                         radius=(picked/2)*v,
#
#                                                                         # Radial distance
#                                                                         # radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
#                                                                         trajectory=bor_df,
#                                                                         depth_resolution=0.1,
#                                                                         radial_resolution=0.1
#                                                                         ) # This one works
# p.add_object(pv.PolyData(GPR_cone), color='red', name='GPR cone 2', opacity=0.3)
# p.show(zxGrid2=False, yzGrid2=False)


#%%
from fracwave import FracEM, FractureGeo

frac = FractureGeo()
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)
resolution = (50,50,50)
frac.init_geo_model(gempy_extent, resolution)

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1 # m
max_element_size = wavelength/6
p1 = (70, 90, 0)

fixed = np.asarray([(0,0,p1[-1])])
fixed_or = [fixed.copy(), np.asarray([(p1[1],p1[0],1)])]

planar = np.asarray([[1.20713, 0, -3.3165622],
                          [0.653862, 0, -1.7964711],
                          [0.15089116, 0, -0.41457027],
                          [-0.3520796, 0, 0.96733063]])
vertices, faces = frac.create_gempy_geometry(fixed, fixed_or, move_points = planar)

resolution = (20, 20)
plane = 0
order = 2
surf = frac.remesh(vertices,
                   plane=plane,
                   order=order,
                   spacing=resolution,
                   max_element_size=max_element_size)
min_resol = np.ceil(surf.length / max_element_size).astype(int)
resolution = (min_resol, min_resol)
if surf.dimensions[0] < min_resol:
    logger.debug('Remeshing...')
    surf = frac.remesh(vertices, plane=plane, order=order, spacing=resolution)

df = frac.set_fracture_properties(aperture=0.5,
                                  electrical_conductvity=0.001,
                                    electrical_permeability=81)

file = filename.split('.')[0]
if not os.path.isfile(file+'_iter_0.h5'):
    shutil.copy2(filename, file+'_iter_0.h5')
c = 0
while True:
    filename = file+ f'_iter_{c}.h5'
    if os.path.isfile(filename):
        filename_next = file + f'_iter_{c+1}.h5'
        if os.path.isfile(filename_next):
            c += 1
            continue
        shutil.copy2(filename, filename_next)
        filename = filename_next
        break
    else:
        shutil.copy2(file+ f'_iter_{c-1}.h5', filename)
        break

curved = np.asarray([[0.89864941,  0.12210877, -3.37883444],
                                     [0.93922058,  0.27997581, -2.02387851],
                                     [-0.07264459,  0.30187218,  0.04356908],
                                     [0.02385303, -0.14218273,  0.96832576]])

np.asarray([[ 3.23754889e-01,  3.50706333e-04, -2.03992868e+00],
 [8.92493718e-01,  5.61138347e-03, -1.94710497e+00],
 [2.48437307e-01, -7.23622450e-03,  -4.65674211e-01],
 [4.77223790e-02, -4.05177425e-04,  1.41951776e+00]])

