import re
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.gridspec import GridSpec

from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend, apply_gain
from fracwave import Antenna, OUTPUT_DIR

odir = OUTPUT_DIR + 'fracture_aperture/figures_paper/'
file_h5 = odir + 'sketch.h5'

ant = Antenna()
ant.load_hdf5(file_h5)
#%%
paths_all_apertures = {'0mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/0mm/SEGY_20240412_172224/',
                        '2mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/2mm/SEGY_20240412_172341/',
                        '3mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/3mm/SEGY_20240412_172435/',
                        'alum':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135'}
data_measu = {}
data_together = {}

# When pol=0 then we look at antenna orientation (1,0,0). ['B', 'C']
# When pol = 1 then antenna is (0,1,0). ['A, 'D']
pol = 1 # polarization of the antennas.
th = '2mm'
vmax = 0

out = [5, 29]
for key, base_path in paths_all_apertures.items():



    # Get all the files in the folder
    folders = os.listdir(base_path)
    # loop through all the folders and get the files. From the filename extract the information.
    # The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

    # Create a dictionary to store the information
    data_measu[key] = {'A':{},  # Up direction (0,1,0)
                       'B':{},  # Right dir (1,0,0)
                       'C':{},  # Up dir (1,0,0)
                       'D':{}}  # Right dir (0,1,0)

    vmax = 0

    for folder in folders:
        # Get the files
        files = os.listdir(os.path.join(base_path, folder))
        for file in files:
            if '.sgy' not in file:
                continue
            info = re.split('_', file)
            # identif = info[0]
            # Get the date
            # date = info[1]
            # Get the ID
            ID = info[0]
            # Get the path
            # experiment_number = int(info[2])
            experiment_number = int(info[1])
            n_e_id = ID + '_' + str(experiment_number)
            midpoints = ant.get_midpoints(n_e_id)
            # Patch
            ma = ((midpoints[:, 0] >= out[0] * 1e-2) & (midpoints[:, 0] <= out[1] * 1e-2) &
                 (midpoints[:, 1] >= out[0] * 1e-2) & (midpoints[:, 1] <= out[1] * 1e-2))
            if True in ma:
                print(n_e_id)
            else:
                print('no_' + n_e_id)
                continue
            lims_x = np.arange(len(ma))[ma][0], np.arange(len(ma))[ma][-1]

            # if experiment_number < out[0] or experiment_number > out[1]:
            #     print('no_'+n_e_id)
            #     continue
            # else:
            #     print(n_e_id)
            path = os.path.join(base_path, folder, file)
            # Get the data
            data_cube, header = load_sgy(path)

            # if ID =/= 'B':  # It is flipped
                # data_cube = np.fliplr(data_cube)
            # Store the data in the dictionary
            # d = {experiment_number: {'header':header, 'data': data_cube}}
            # Processing
            d_de = detrend(data_cube)
            sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
            c0 = 299_792_458  # Speed of light in m/s
            rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
            velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
            # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
            # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
            d_de_ga = d_de.copy()
            data_uncut = d_de_ga.copy()
            mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
            if len(ma) > len(mask_x):
                mask_x = ma[:len(mask_x)]
                midpoints = midpoints[:len(mask_x)]
            else:
                mask_x[:len(ma)] = ma
            # lims_y = (80, 100)
            lims_y = (70, 110)
            d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
            d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
            d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from left
            d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from right
            # d_de_ga[:, mask_x] = 0 # crop all reflections from right
            time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
            depth_vector = time_vector * velocity * 0.5
            vma = np.abs(d_de_ga).max()
            if vma > vmax: vmax=vma
            d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
                 'time_vector':time_vector, 'depth_vector':depth_vector,
                 'lims_x': lims_x, 'lims_y': lims_y,
                 'data_cut': data_uncut[:, mask_x],
                 'data_cut_all': data_uncut[lims_y[0]:lims_y[1], mask_x],
                 'mask_x': mask_x,
                 'midpoints':midpoints,
                 }
            data_measu[key][ID][experiment_number] = d

#%%
line = 10
trace = 35
vmax1 = 0
for n in ['0mm', '2mm', '3mm', 'alum']:
    inf = data_measu[n]['A'][15]
    vm = np.abs(inf[('data_uncut')]).max()
    if vm > vmax1:
        vmax1=vm

# fig = plt.figure(figsize=(20, 10))
# gs = GridSpec(2, 3, figure=fig, width_ratios=[2, 2, 1])

fig, [[ax0,ax1],[ax2,ax3]] = plt.subplots(2,2, figsize=(15,12), sharex=True, sharey=True)
# ax0 = fig.add_subplot(gs[0, 0])
# ax1 = fig.add_subplot(gs[0, 1])
# ax2 = fig.add_subplot(gs[1, 0])
# ax3 = fig.add_subplot(gs[1, 1])

# ax4 = fig.add_subplot(gs[:, 2])

for n, ax in zip(['0mm', '2mm', '3mm', 'alum'], [ax0,ax1,ax2,ax3]):
    inf = data_measu[n]['A'][line]
    d = inf['data_uncut']
    lims_x = inf['lims_x']
    lims_y = inf['lims_y']
    x=inf['midpoints'][:,1]*1e2

    time_vector = inf['time_vector']

    extent = (x.min(), x.max(), time_vector[-1], time_vector[0])

    cax = ax.imshow(d, aspect='auto', cmap='RdBu', interpolation='None', vmin=-vmax1, vmax=vmax1,
               extent=extent)

    rect = Rectangle(xy=(x[lims_x[0]], time_vector[lims_y[0]]),
                     width=x[lims_x[1]] - x[lims_x[0]] ,
                     height=time_vector[lims_y[1]] - time_vector[lims_y[0]],
                     linewidth=2, edgecolor='k', facecolor='none', linestyle='dashed')
    ax.add_patch(rect)

    # ax.vlines(x=x[trace], ymin=time_vector[lims_y[1]], ymax=time_vector[lims_y[0]], color='r', linestyle=':')

    ax.set_ylabel('Two way travel time [ns]')
    ax.set_xlabel('Distance [cm]')
    # plt.ylim(2.7,1.5)
    ax.set_ylim(3.5,0.5)
    ax.set_xlim(-3,37)
    # plt.xlim(20,80)
ax0.set_title(r'$\bf{(a)}$ Profile with heterogeneous apertures',  fontsize=20, pad=10)
ax1.set_title(r'$\bf{(b)}$ Profile with extra 2 mm pad',  fontsize=20, pad=10)
ax2.set_title(r'$\bf{(c)}$ Profile with extra 3 mm pad',  fontsize=20, pad=10)
ax3.set_title(r'$\bf{(d)}$ Profile of the aluminium foil',  fontsize=20, pad=10)

cbar=fig.colorbar(cax, ax=[ax0, ax1, ax2, ax3], label='Normalized amplitude [-]', orientation='vertical', pad =0.04,
                  aspect=40, fraction=0.06)
# plt.subplots_adjust(
    # left=0.1, right=0.9,
    # top=0.9,
    # bottom=0.1,
    # wspace=0.4, hspace=0.6
# )

# fig.tight_layout()
plt.show()

fig.savefig(odir+'fig03_raw_profiles.pdf', bbox_inches='tight')


#%%
#d.shape[1]//2
max_amp = np.abs(data_measu['alum']['A'][line]['data_uncut'][lims_y[0]:lims_y[1], trace]).max()

fig, ax4 = plt.subplots(figsize=(8,8))
for n, name, l, m in zip(['0mm', '2mm', '3mm', 'alum'], ['Normal', 'Extra 2 mm pad', 'Extra 3 mm pad', 'Aluminium foil'],
                         ['-', '--', '-.', ':'],
                         ['o', 's', 'D', '^']#, 'v', '<', '>', 'p', '*', 'h']
                         ):
    inf = data_measu[n]['A'][line]
    d = inf['data_uncut'] / max_amp
    lims_x = inf['lims_x']
    lims_y = inf['lims_y']
    x=inf['midpoints'][:,1]*1e2

    time_vector = inf['time_vector']  # * 100#[sl_time]

    ax4.plot(time_vector[lims_y[0]:lims_y[1]], d[lims_y[0]:lims_y[1], trace], label=name, linestyle=l, marker=m,
            markersize=8)

    ax4.set_ylabel('Normalized GPR Amplitude to Aluminuim foil [-]')
    ax4.set_xlabel('Two way travel time [ns]')

ax4.grid(linestyle='--')
ax4.set_xlim(1.75, 2.65)
leg = ax4.legend(loc='lower left', bbox_to_anchor=(0, 0), fancybox=True, edgecolor='black', frameon=True)
leg.get_frame().set_facecolor('white')
# fig.tight_layout()
plt.show()
#%%
fig.savefig(odir+'fig04_trace_from_profile.pdf', bbox_inches='tight')


