# This will be about the setup, the raw image, the time window, and the comparison between traces
# for the specific time window. This will be placed ant the data processing
import numpy as np

import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model

p = model.ModelPlot(plotter_type='basic')

from fracwave import FractureGeo, Antenna, OUTPUT_DIR

file_h5 = OUTPUT_DIR + 'fracture_aperture/figures_paper/sketch.h5'

window_size = (1900,2000)

cam  = [(-15.455137758037772, -20.148353549143433, 13.085369680465325),
 (0.5473828427661188, 0.8974323933810469, -1.5291263419702086),
 (0.26420340990556196, 0.40646584710680206, 0.8746325361716332)]
show_kwargs = dict(#xtitle_offset=0.08,
                   #ztitle_offset=0.04,
                   #ytitle_offset=0.15,
                   xtitle='X (cm)',
                   ytitle= 'Y (cm)',
                   ztitle='Z (cm)',
                   #xrange=(-2, 3),
                   #yrange=(-5,6),
                   #zrange=(-6,5),
                   number_of_divisions=10,
                   yzgrid2=True,
                   #xlabel_size=0.03,
                   #ylabel_size=0.03,
                   #zlabel_size=0.03,
                   #xtitle_size=0.03,
                   #ytitle_size=0.03,
                   #ztitle_size=0.03
                   )
odir = OUTPUT_DIR + 'fracture_aperture/figures_paper/'
#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%%
# With the geometry load the scan lines
line = pv.PolyData()
for n in ant.name_profiles:
    if n == 'A_10':
        continue
    po = ant.get_midpoints(n)*1e2
    line += pv.Spline(po)
p.add_object(line, name=f'lines', color='black', opacity=0.6)
p.add_object(pv.Spline(ant.get_midpoints('A_10')*1e2), name=f'lines_fix', color='red', opacity=1, line_width=5)
# p.add_object(ant.get_midpoints()*1e2, name='mid', color='black', opacity=0.3)

# p.remove('mid')
#%% Fracture
frac = FractureGeo()
resolution = (45,45)

def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2
extent_original=(midpoints_l[:,0].min(), midpoints_l[:,0].max(), midpoints_l[:,1].min(), midpoints_l[:,1].max())

apertures_original = np.asarray(rotate_matrix_clockwise( apertures_l.reshape(spacing), rotations=0)) *1e-2
mask = np.isclose(top_surface_m, bottom_surface_m)
try:
    frac.load_hdf5(file_h5)
    surf = frac.get_surface()
    apertures = frac.fractures['aperture'].to_numpy()
except:

    width = 0.30
    length = 0.30
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0,
                                                       resolution=resolution, center=(.17,.17,0))
    # surf, vertices, faces = frac.remesh(points=vertices, max_element_size=max_element_size, plane=2)

    surf = frac.create_pyvista_mesh(vertices, faces)

    p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)

    # outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
    # d = np.load(outname + 'files.npz')
    # top_surface_m = d['top_surface_m']
    # bottom_surface_m = d['bottom_surface_m']
    # apertures_l = d['apertures']
    # spacing = [600, 600]
    # midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2

    apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing),
                                                 rotations=1)

    scaling_factors = (np.array(resolution) / spacing).tolist()
    from scipy.ndimage import zoom
    # Perform the downsampling using zoom
    downsampled_image = zoom(apertures_original, scaling_factors)
    scaling_factors_up = (spacing / np.array(resolution)).tolist()
    upsampled_image = zoom(downsampled_image, scaling_factors_up)

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
    cax1 = ax1.imshow(apertures_original, cmap='viridis')
    cax2 = ax2.imshow(downsampled_image, cmap='viridis')
    cax3 = ax3.imshow(upsampled_image, cmap='viridis')
    cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.colorbar(cax3, ax=ax3)
    fig.colorbar(cax4, ax=ax4)
    ax1.set_title('Original')
    ax2.set_title('Downsampled')
    ax3.set_title('Upsampled')
    ax4.set_title('Original-Upsampled')
    fig.show()
# apertures = apertures.reshape(spacing)
    for i in [0, 2e-3,3e-3]:

        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2 + i
        kwarg_properties = dict(aperture=apertures, #0.01,
    #     kwarg_properties = dict(aperture=0.1,
                                    electrical_conductivity=0,
                                    electrical_permeability=1,
                                plane=2,
                                overwrite=True)
        frac.set_fracture(name_fracture=f'fracture_{int(i*1000)}',
                          vertices=vertices,
                          faces=faces,
                          **kwarg_properties)

        frac.export_to_hdf5(file_h5, overwrite=True)
# p.add_object(frac.get_midpoints(name_fractures='fracture_0'), name='fracture', scalars=frac.df.loc[frac.df['name']=='fracture_0','aperture'].to_numpy())
#%%
mesh_midpoints = pv.PolyData(midpoints_l[~mask]*1e2)
mesh_midpoints['Aperture (cm)'] = apertures_original.ravel()[~mask]*1e2

p.add_object(mesh_midpoints, name='fracture', scalars='Aperture (cm)', cmap='viridis', show_scalar_bar=True)

# p.plotter.add_scalar_bar(title='Aperture (cm)', n_labels=5, shadow=True, bold=True)
#%%

meshD = pv.Cube(center=(17,17,-5), x_length=34,y_length=34, z_length=10)
meshU = pv.Cube(center=(17,17, 5), x_length=34,y_length=34, z_length=10)
p.add_object(meshD, name='lower_cube', style='wireframe')
p.add_object(meshU, name='upper_cube', style='wireframe')

#%%
# p.add_object(ant2.Receiver, name='rx', color='blue', opacity=0.2)
# view = 'D'
# p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Rx','Ry','Rz']].to_numpy(), name='rx', color='blue', opacity=0.2)
# # p.add_object(ant2.Transmitter, name='tx', color='red', opacity=0.2)
# p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Tx','Ty','Tz']].to_numpy(), name='tx', color='red', opacity=0.2)
# p.remove('rx')
# p.remove('tx')
#%%


# p.plotter.add_axes(box=True, viewport=(0.11, 0.0, 0.3, 0.3))
# p.plotter.window_size = window_size#(1750, 1900)
# p.plotter.camera_position = cam
p.show(**show_kwargs)
#%%
p.snapshot('fig02b_sketch.pdf', save_dir=odir)

