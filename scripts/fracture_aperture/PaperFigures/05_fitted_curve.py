import re
import numpy as np
import os
import matplotlib.pyplot as plt
import gstools as gs

import scipy.stats
from scipy.interpolate import griddata


from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend, apply_gain

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM

odir = OUTPUT_DIR + 'fracture_aperture/figures_paper/'
file_h5 = odir + 'sketch.h5'

#%% Source
sou = SourceEM()
try:
    sou.load_hdf5(file_h5)
except:
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 655))

    sou.set_delay(delay=1)
    # sou.set_source_params(a=9.17809204,
    #                       c=0.95575954,
    #                       loc=0,
    #                       scale=0.28381535)
    sou.set_source_params(a=14.99999994,
                          c=0.68261749,
                          loc=0,
                          scale=0.05180656)
    sou.create_source()
    # sou.widgets().show()

    # fig = sou.plot_waveforms_zoom()
    fig = sou.plot_waveforms_complete()
    fig.show()
    sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5

#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5.split('.')[0] + '_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%% Fracture
frac = FractureGeo()
resolution = (45,45)

def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2
extent_original=(midpoints_l[:,0].min(), midpoints_l[:,0].max(), midpoints_l[:,1].min(), midpoints_l[:,1].max())

apertures_original = np.asarray(rotate_matrix_clockwise( apertures_l.reshape(spacing), rotations=0)) *1e-2
mask = np.isclose(top_surface_m, bottom_surface_m).reshape(spacing)
try:
    frac.load_hdf5(file_h5)
    surf = frac.get_surface()
    apertures = frac.fractures['aperture'].to_numpy()
except:

    width = 0.30
    length = 0.30
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0,
                                                       resolution=resolution, center=(.17,.17,0))

    surf = frac.create_pyvista_mesh(vertices, faces)

    apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing),
                                                 rotations=1)

    scaling_factors = (np.array(resolution) / spacing).tolist()
    from scipy.ndimage import zoom
    # Perform the downsampling using zoom
    downsampled_image = zoom(apertures_original, scaling_factors)
    scaling_factors_up = (spacing / np.array(resolution)).tolist()
    upsampled_image = zoom(downsampled_image, scaling_factors_up)

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
    cax1 = ax1.imshow(apertures_original, cmap='viridis')
    cax2 = ax2.imshow(downsampled_image, cmap='viridis')
    cax3 = ax3.imshow(upsampled_image, cmap='viridis')
    cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.colorbar(cax3, ax=ax3)
    fig.colorbar(cax4, ax=ax4)
    ax1.set_title('Original')
    ax2.set_title('Downsampled')
    ax3.set_title('Upsampled')
    ax4.set_title('Original-Upsampled')
    fig.show()
# apertures = apertures.reshape(spacing)
    for i in [0, 2e-3,3e-3]:

        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2 + i
        kwarg_properties = dict(aperture=apertures, #0.01,
    #     kwarg_properties = dict(aperture=0.1,
                                    electrical_conductivity=0,
                                    electrical_permeability=1,
                                plane=2,
                                overwrite=True)
        frac.set_fracture(name_fracture=f'fracture_{int(i*1000)}',
                          vertices=vertices,
                          faces=faces,
                          **kwarg_properties)

        frac.export_to_hdf5(file_h5, overwrite=True)

#%%
paths_all_apertures = {'0mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/0mm/SEGY_20240412_172224/',
                        '2mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/2mm/SEGY_20240412_172341/',
                        '3mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/3mm/SEGY_20240412_172435/',
                        'alum':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135'}
data_measu = {}
data_together = {}

# When pol=0 then we look at antenna orientation (1,0,0). ['B', 'C']
# When pol = 1 then antenna is (0,1,0). ['A, 'D']
pol = 1 # polarization of the antennas.
th = '2mm'
vmax = 0

out = [5, 29]
for key, base_path in paths_all_apertures.items():



    # Get all the files in the folder
    folders = os.listdir(base_path)
    # loop through all the folders and get the files. From the filename extract the information.
    # The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

    # Create a dictionary to store the information
    data_measu[key] = {'A':{},  # Up direction (0,1,0)
                       'B':{},  # Right dir (1,0,0)
                       'C':{},  # Up dir (1,0,0)
                       'D':{}}  # Right dir (0,1,0)

    vmax = 0

    for folder in folders:
        # Get the files
        files = os.listdir(os.path.join(base_path, folder))
        for file in files:
            if '.sgy' not in file:
                continue
            info = re.split('_', file)
            # identif = info[0]
            # Get the date
            # date = info[1]
            # Get the ID
            ID = info[0]
            # Get the path
            # experiment_number = int(info[2])
            experiment_number = int(info[1])
            n_e_id = ID + '_' + str(experiment_number)
            midpoints = ant.get_midpoints(n_e_id)
            # Patch
            ma = ((midpoints[:, 0] >= out[0] * 1e-2) & (midpoints[:, 0] <= out[1] * 1e-2) &
                 (midpoints[:, 1] >= out[0] * 1e-2) & (midpoints[:, 1] <= out[1] * 1e-2))
            if True in ma:
                print(n_e_id)
            else:
                print('no_' + n_e_id)
                continue
            lims_x = np.arange(len(ma))[ma][0], np.arange(len(ma))[ma][-1]

            # if experiment_number < out[0] or experiment_number > out[1]:
            #     print('no_'+n_e_id)
            #     continue
            # else:
            #     print(n_e_id)
            path = os.path.join(base_path, folder, file)
            # Get the data
            data_cube, header = load_sgy(path)

            # if ID =/= 'B':  # It is flipped
                # data_cube = np.fliplr(data_cube)
            # Store the data in the dictionary
            # d = {experiment_number: {'header':header, 'data': data_cube}}
            # Processing
            d_de = detrend(data_cube)
            sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
            c0 = 299_792_458  # Speed of light in m/s
            rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
            velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
            # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
            # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
            d_de_ga = d_de.copy()
            data_uncut = d_de_ga.copy()
            mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
            if len(ma) > len(mask_x):
                mask_x = ma[:len(mask_x)]
                midpoints = midpoints[:len(mask_x)]
            else:
                mask_x[:len(ma)] = ma
            # lims_y = (80, 100)
            lims_y = (70, 110)
            d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
            d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
            d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from left
            d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from right
            # d_de_ga[:, mask_x] = 0 # crop all reflections from right
            time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
            depth_vector = time_vector * velocity * 0.5
            vma = np.abs(d_de_ga).max()
            if vma > vmax: vmax=vma
            d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
                 'time_vector':time_vector, 'depth_vector':depth_vector,
                 'lims_x': lims_x, 'lims_y': lims_y,
                 'data_cut': data_uncut[:, mask_x],
                 'data_cut_all': data_uncut[lims_y[0]:lims_y[1], mask_x],
                 'mask_x': mask_x,
                 'midpoints':midpoints,
                 }
            data_measu[key][ID][experiment_number] = d

#%% lets get the aperture based on the position of the antennas
def find_nearest(x, y, positions):
  """
  Finds the nearest position (index) in a list of positions to a given point.

  Args:
      x: X-coordinate of the target point.
      y: Y-coordinate of the target point.
      positions: List of tuples containing (x, y) coordinates.

  Returns:
      The index of the nearest position in the positions list.
  """
  distance = ((positions[:, 0] - x) ** 2 + (positions[:, 1] - y) ** 2) ** 0.5
  nearest_idx = np.argmin(distance)
  return nearest_idx

#%%
midpoints = ant.get_midpoints()[:, :2]

# sort x
mask_x = (out[0]*1e-2 < midpoints[:,0]) & (midpoints[:,0] < out[1]*1e-2)
mask_y = (out[0]*1e-2 < midpoints[:,1]) & (midpoints[:,1] < out[1]*1e-2)
mask_idx = mask_x & mask_y

position_df = ant.df[mask_idx]
mid_ant_pos = (position_df[['Rx','Ry','Rz']].to_numpy() + position_df[['Tx','Ty','Tz']].to_numpy()) * 0.5

#%% Calculate reflection coefficients
midpoints = {}
apertures = {}
amplitudes = {}
for key, a in data_measu.items():
    if key == 'alum':
        continue
    midpoints[key] = []
    apertures[key] = []
    amplitudes[key] = []
    for key2, b in a.items():
        if key2 in ['B', 'C']:
            continue
        for key3, c in b.items():
            data_al = data_measu['alum'][key2][key3]['data_cut_all']
            # norm = np.abs(data_al).max(axis=0)
            norm = np.linalg.norm(data_al,axis=0, ord=2)
            data = np.linalg.norm(c['data_cut_all'],axis=0)  / norm  # Normalize to maximum of aluminium
            lims_x = c['lims_x']
            lims_y = c['lims_y']
            depth_vector = c['depth_vector']
            mask_x = c['mask_x'] #[:101]  # I have only 100 points always
            midp = c['midpoints']
            if len(mask_x) > len(midp):
                mask_x = mask_x[:len(midp)]
            midp = midp[mask_x]

            df_ap = frac.get_fracture(name_fracture=f'fracture_{key[0]}')

            nearest_idx_pos = np.asarray(
                [find_nearest(po[0], po[1], positions=df_ap[['x','y']].to_numpy()) for po in midp[:,:2]])
            aperture_from_near = df_ap['aperture'].to_numpy()[nearest_idx_pos]
            sigma_from_near = df_ap['elec_conductivity'].to_numpy()[nearest_idx_pos]
            epsilon_from_near = df_ap['elec_permeability'].to_numpy()[nearest_idx_pos]

            midpoints[key].append(midp)
            apertures[key].append(aperture_from_near)
            # amplitudes[key].append(np.abs(data).max(axis=0))
            amplitudes[key].append(data)

    midpoints[key] = np.vstack(midpoints[key])
    apertures[key] = np.hstack(apertures[key])
    amplitudes[key] = np.hstack(amplitudes[key])
#%%
solv = FracEM()
solv.open_file(file_h5)
rock_epsilon =6.5
solv.rock_epsilon =rock_epsilon

rock_sigma = 0.0
solv.rock_sigma = rock_sigma

epsilon = frac.df['elec_permeability'].unique()[0]
sigma = frac.df['elec_conductivity'].unique()[0]

solv.engine = 'tensor_trace'

from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_frequencies
mask_freq = get_mask_frequencies(sou.source, threshold=0.1)
omega = sou.frequency[mask_freq]* 2*np.pi *1e9
#%%
def average(data, type='arithmetic', distance=None):
    if type == 'arithmetic':
        return np.mean(data)
    elif type == 'median':
        return np.median(data)
    elif type == 'mode':
        counts = np.unique(data, return_counts=True)[1]
        # Find the maximum count
        max_count = np.max(counts)
        # Get all modes (values with max count)
        modes = data[counts == max_count]
        return modes.tolist()  # Convert to list for return
    elif type == 'geometric_mean':
        # Check for negative values or zeros
        if any(value <= 0 for value in data):
            raise ValueError("Geometric mean is not defined for non-positive values.")

        # Calculate the product and take the nth root
        product = np.prod(data)
        n = len(data)
        return np.power(product, 1 / n)
    elif type == 'harmonic_mean':
        if any(value <= 0 for value in data):
            raise ValueError("Harmonic mean is not defined for non-positive values.")

        # Calculate the sum of reciprocals and take the reciprocal
        reciprocals = 1 / np.array(data)
        return 1 / np.mean(reciprocals)
    elif type == 'distance_weight':
        weights = 1 / distance**2  # Weights based on inverse distance
        weights = weights / np.sum(weights)  # Normalize weights to sum to 1
        return np.average(data, weights=weights)
    else:
        raise AttributeError

#%%
def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5


distance = 0.10  # Top to the view
rock_epsilon = 6.5
c0 = 299_792_458
velocity = c0 * 1e-7 / np.sqrt(rock_epsilon)
frequency = 3.6 # GHz
wavelength = solv.velocity / sou.center_frequency  # Result in cm

r = fresnel(distance, wavelength, 1)

#%%
from tqdm.autonotebook import tqdm
# options are ['arithmetic', 'geometric_mean', 'harmonic_mean', 'median', 'mode', 'distance_weight]
typ = 'distance_weight'
try:
    # raise FileNotFoundError
    apertures_fresnel = {key:np.load(odir + f'apertures_fresnel_{typ}_{key}.npy') for key in midpoints.keys()}
except FileNotFoundError:

    def apertures_inside_circle(point_cloud, apertures, circle_origin, circle_radius):
        # Convert the point cloud to a NumPy array for easier calculations
        point_cloud_array = np.array(point_cloud) + 100
        circle_origin =  np.array(circle_origin) + 100

        # Calculate the distances from each point to the circle's origin
        distances = np.linalg.norm(point_cloud_array - circle_origin, axis=1)

        # Find indices of points that are inside the circle (distance < radius)
        inside_circle_indices = np.where(distances < circle_radius)[0]
        point_cloud_array -= 100
        circle_origin -= 100
        # Extract the points that are inside the circle
        points_inside_circle = point_cloud_array[inside_circle_indices]
        apertures_inside_cirlce = apertures[inside_circle_indices]
        distance = distances[inside_circle_indices]

        return points_inside_circle, apertures_inside_cirlce, distance
    apertures_fresnel ={}
    for key in midpoints.keys():
        mid = midpoints[key]
        apertures_fresnel2 = []
        position_fresnel = []
        dist_frenel = []
        mask = frac.df['name'] == f'fracture_{key[0]}'
        for m in tqdm(mid):#middle_points):
            points, apert, dista = apertures_inside_circle(frac.get_midpoints()[mask,:2],
                                                    frac.df['aperture'][mask].to_numpy(), m[:2], r)
            if points.size == 0:
                apertures_fresnel2.append(0)
                position_fresnel.append([0])
                dist_frenel.append([0])
                continue
            # if type in ['geometric_mean', 'harmonic_mean']:
            apert = np.abs(apert)

            apertures_fresnel2.append(average(apert, type=typ, distance=dista))
            position_fresnel.append(points)
            dist_frenel.append(dista)
        # break
        apertures_fresnel[key] = np.c_[mid[:,0],mid[:,1], apertures_fresnel2]

    [np.save(odir + f'apertures_fresnel_{typ}_{key}.npy', a) for key, a in apertures_fresnel.items()]


#%%
x_mid = 0.036
z = 0.1
theta = np.arctan(x_mid/(2*0.1))
# fe = np.array([1600 *1e6])# * 2 * np.pi#omega
# fe = np.array([sou.center_frequency * 1e9])
# fe = np.array([sou.center_frequency * 1e9])
# ome = omega #fe  * 2 * np.pi
# ome = [omega[0], sou.center_frequency * 1e9 * 2 * np.pi, omega[-1]]
ome = [omega[0], sou.center_frequency * 1e9 * 2 * np.pi, omega[-1]]
re = solv.rock_epsilon  # 9.5
rs = solv.rock_sigma  # 0.002
fe = frac.df['elec_permeability'].unique()[0] # 16
fs = frac.df['elec_conductivity'].unique()[0]  # 0.1

# v = solv.c0 / np.sqrt(re)
# o = sou.center_frequency *1e9
wavelength = solv.wavelength
# m = 0.3 * wavelength #0.02 # m
m = 2 * wavelength #0.02 # m
d = np.linspace(0, m, 1000)[None, :]
# k1 = wavenumber(ome, epsilon=re, sigma=rs, complex=False)[:, None]
# k2 = wavenumber(ome, epsilon=e, sigma=s, complex=False)[:, None]

k1c = solv.wavenumber(omega=ome, epsilon=re, sigma=rs)[:, None]
k2c = solv.wavenumber(omega=ome, epsilon=fe, sigma=fs)[:, None]

#%%
# G = solv.reflection_all(k1, k2, theta=0, aperture=d)
Gc = solv.reflection_all(k1c, k2c, theta=0, aperture=d)
Gt = solv.reflection_all(k1c, k2c, theta=theta, aperture=d)
ap = frac.df['aperture'].to_numpy()
# Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
# Gfresnel = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
#%%
poly_x = np.hstack([v for v in amplitudes.values()])
poly_y = np.hstack([v[:,-1] for v in apertures_fresnel.values()])
par = np.polyfit(poly_x, poly_y, deg=1)
# [par,_] = np.polyfit(poly_x, poly_y, deg=1)
# par = np.polyfit(amplitudes[key], apertures[key], 1)# full=True)
# pl = np.poly1d(par)

#%%
def ffw(o, apertures):
    k1c = solv.wavenumber(omega=o, epsilon=re, sigma=rs)
    k2c = solv.wavenumber(omega=o, epsilon=fe, sigma=fs)
    rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=apertures)
    return np.abs(rc)

def fw(o):
    rc = ffw(o, np.hstack([a[:,-1] for a in apertures_fresnel.values()]))
    return np.sqrt(np.sum(np.square(rc - np.hstack([a for a in amplitudes.values()]))))

def invfw(o, R):
    """
    Input the reflection coefficient and frequency and obtain the apertures
    Args:
        o:
        Re:
    Returns:

    """
    from scipy.interpolate import interp1d
    # k1c = solv.wavenumber(omega=o, epsilon=re, sigma=rs)
    # k2c = solv.wavenumber(omega=o, epsilon=fe, sigma=fs)
    #
    # Rte = solv.reflection_coefficients(k1c, k2c, theta)
    # theta_t = np.arcsin(k1c * np.sin(theta) / k2c)
    # theta_t = 0
    #
    # d = np.log((Rte-R)/(Rte-R*Rte**2))/(-2j*k2c*np.cos(theta_t))

    # exponential = np.exp(-2j * k2 * aperture * arr.cos(theta_t))
    # modulationTE = Rte * (1 - exponential) / (1 - (Rte ** 2) * exponential)

    x_values = np.linspace(0, wavelength, 1000)
    k1c = solv.wavenumber(omega=o, epsilon=re, sigma=rs)
    k2c = solv.wavenumber(omega=o, epsilon=fe, sigma=fs)
    y_values = np.abs(solv.reflection_all(k1c, k2c, theta=theta, aperture=x_values))
    # plt.plot(x_values, y_values);plt.show()
    interp_func = interp1d(y_values, x_values)#, bounds_error=False)#, fill_value="extrapolate")

    return interp_func(R)  # invfw(o, 0.2)

x = sou.center_frequency * 2 * np.pi *1e9

from scipy.optimize import minimize
res = minimize(fw,
                x,
                method='nelder-mead',
                options={#'maxfun': 10000,
                         'xatol': 1e-6,
                         'disp': False})
k1c = solv.wavenumber(omega=res.x, epsilon=re, sigma=rs)
k2c = solv.wavenumber(omega=res.x, epsilon=fe, sigma=fs)
rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=d[0])

#%%
from fracwave.utils.help_decorators import convert_frequency, convert_meter
# f = [convert_frequency(i/(2*np.pi)) for i in omega]
f = [convert_frequency(i/(2*np.pi)) for i in ome]
# f = convert_frequency(res.x[0])

fig, ax = plt.subplots(figsize=(10,5))

ax.plot(d[0]*1e2, np.abs(Gt[2]), alpha=0.5, label=f'Higher frequency: {f[2]}', linestyle='--')
ax.plot(d[0]*1e2, np.abs(Gt[1]), alpha=0.5, label=f'Central frequency: {f[1]}', linestyle='-.')
ax.plot(d[0]*1e2, np.abs(Gt[0]), alpha=0.5, label=f'Lower frequency: {f[0]}', linestyle='-')
ax.fill_between(d[0]*1e2, np.abs(Gt[0]), np.abs(Gt[2]), alpha=0.2, label='Frequency band', color='gray')

[ax.scatter(apertures_fresnel[key][:,-1]*1e2, amplitudes[key], s=5, color='k', label='Data points'
    if key=='0mm' else None, alpha=0.6) for key in midpoints.keys()]

plt.plot(d[0]*1e2, np.abs(rc), label=f'Fitted frequency: {convert_frequency(res.x[0]/(2*np.pi))}', linestyle=':')



# [plt.plot(d[0]/wavelength, np.abs(l), label=f[0]) for l in G]
# ax.axvline(apertures['3mm'].max()*1e2)
ax.set_ylabel('Relative reflection coefficients [-]')
ax.set_xlabel('Aperture [cm]')
ax.legend(fancybox=True, edgecolor='black', frameon=True, loc='upper left')#bbox_to_anchor=(.95, 0.9))#, labels=f)
# ax.set_title(f')
ax.set_xlim(0, 1.4)
ax.grid(linestyle='--')
fig.show()

fig.savefig(odir+'fig05_reflection_coefficients.pdf', bbox_inches='tight')




#%%
# generate_field = np.c_[midpoints[key][:, 0], midpoints[key][:,1], pl(amplitudes[key])]
key = '0mm'
vmin=0
# vmax=apertures_original.max() *1e2
# vmax=pl(amplitudes[key]).max()*1e2
vmax=invfw(res.x, amplitudes[key]).max()*1e2
img = apertures_original.copy() *1e2
img[mask] = None

line = 17
axis=0

fig, [[ax0, ax1, ax2],[ax3,ax4,ax5]] = plt.subplots(2, 3, figsize=(18,10))

x, y = midpoints[key][:, 0]*1e2, midpoints[key][:, 1]*1e2
cax0=ax0.imshow(rotate_matrix_clockwise(img, 3), cmap='viridis', extent=np.asarray(extent_original)*1e2,
                vmin=vmin, vmax=vmax, aspect='equal')

v = np.unique(midpoints[key][:, axis])[line] * 1e2

# xi = np.linspace(x.min(), x.max(), 300)
# yi = np.linspace(y.min(), y.max(), 300)
# xi, yi = np.meshgrid(xi, yi)

# zi = griddata((x, y), amplitudes[key], (xi, yi), method='cubic')
# zi2 = griddata((x, y), invfw(res.x, amplitudes[key])*1e2, (xi, yi), method='cubic')

# cax1 = ax1.imshow(rotate_matrix_clockwise(zi.T, 3), cmap='inferno', extent=(x.min(), x.max(), y.min(), y.max()))
# cax2 = ax2.imshow(rotate_matrix_clockwise(zi2.T, 3), cmap='viridis', extent=(x.min(), x.max(), y.min(), y.max()))

#
# cax1 = ax1.contourf(xi, yi, zi, levels=30, cmap='inferno')
# cax2 = ax2.contourf(xi, yi, zi2, levels=30, cmap='viridis')
s = 60  # 18
cax1=ax1.scatter(x, y, c=amplitudes[key], cmap='inferno', s=s, marker='s')
cax2=ax2.scatter(x, y, c=invfw(res.x, amplitudes[key])*1e2, cmap='viridis', s=s, vmin=vmin, vmax=vmax, marker='s')

ax0.set_title(r'$\bf{(a)}$ True Apertures [cm]', pad =10)
ax1.set_title(r'$\bf{(b)}$ Measured Reflection Coefficients [-]', pad =10)
ax2.set_title(r'$\bf{(c)}$ Calculated Apertures [cm]', pad =10)

ax0.vlines(v, y.min(), y.max(), color='k', linestyle='-')
# ax1.vlines(v, y.min(), y.max(), color='b', linestyle='-')
ax2.vlines(v, y.min(), y.max(), color='r', linestyle='-')

ax1.set_aspect('equal')
ax2.set_aspect('equal')

for ax in [ax0,ax1,ax2]:
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.set_xlim(x.min(), x.max())
    ax.set_ylim(y.min(), y.max())

fig.colorbar(cax0, ax=ax0)
fig.colorbar(cax1, ax=ax1)
fig.colorbar(cax2, ax=ax2)


# fig, (ax0, ax1,ax2) = plt.subplots(1,3, figsize=(15,5))
# fig, AX = plt.subplots(1,3, figsize=(20,5), sharey=True)
for key, ax in zip(midpoints.keys(), [ax3,ax4,ax5]):
    if key == 'alum':
        continue
    if axis==0:
        axis2=1
    else:
        axis2=0

    # axm = ax.twinx()

    # df_frac = frac.get_fracture(name_fracture=f'fracture_{key[0]}')
    m = midpoints_l.copy() *1e2 #df_frac[['x','y']].to_numpy()
    ape = apertures_l + int(key[0])*1e-1 #df_frac['aperture'].to_numpy()
    mask_s = ((m[:, 0] >= out[0]) & (m[:, 0] <= out[1]) &
            (m[:, 1] >= out[0]) & (m[:, 1] <= out[1]))
    m = m[mask_s]
    ape = ape[mask_s]

    mask2 = midpoints[key][:, axis]*1e2 == v
    m2 = midpoints[key][mask2,axis2] * 1e2

    # Get the closes value
    v2 = m[np.argmin(np.abs(m[:,axis] - v)), axis]
    mask3 = m[:, axis] == v2
    s = np.argsort(m2)
    # mask &= midpoints[key][:,1] ==0.155
    l1, = ax.plot(m[mask3,axis2], ape[mask3], color='k', label='True apertures')
    # l2, = axm.plot(m2[s], amplitudes[key][mask2][s], color='b', marker='*', label='Measured Reflection Coeff.')
    l3, = ax.plot(m2[s], invfw(res.x, amplitudes[key])[mask2][s]*1e2, color='r', marker='.', label='Calculated apertures')
    # axm.set_ylabel('Relative Reflection Coefficient [-]')
    ax.set_xlabel('x [cm]' if axis==0 else 'y [cm]')
    # ax.set_title(f'Apertures_{key}')
    ax.set_ylabel('Aperture [cm]')

    ax.grid(linestyle='--')
    ax.set_ylim(-0.1, 1.4)

    # axm.set_ylim(0, 0.5)
    # axm.set_ylim(0, ffw(res.x, 1.4))

    # Set the color of all text and axis elements to blue
    # axm.yaxis.label.set_color('blue')
    # axm.tick_params(axis='y', colors='blue')

    # Set the color of the tick labels
    # for label in axm.get_yticklabels():
    #     label.set_color('blue')

    lines=[l1, l3]
    labels=[l.get_label() for l in lines]
    ax.legend(lines, labels, fancybox=True, edgecolor='black', frameon=True, loc='lower right')

ax3.set_title(r'$\bf{(d)}$ Comparison of Apertures', pad =10)
ax4.set_title(r'$\bf{(e)}$ Comparison of Apertures with 2 mm pad', pad=10)
ax5.set_title(r'$\bf{(f)}$ Comparison of Apertures with 3 mm pad', pad=10)
fig.tight_layout()
plt.show()

fig.savefig(odir+'fig06_generated_apertures.pdf', bbox_inches='tight')

#%%
#### Create variogram
models = {
    "Gaussian": gs.Gaussian,
    "Exponential": gs.Exponential,
    "Matern": gs.Matern,
    # "Stable": gs.Stable,
    # "Rational": gs.Rational,
    # "Circular": gs.Circular,
    # "Spherical": gs.Spherical,
    # "SuperSpherical": gs.SuperSpherical,
    # "JBessel": gs.JBessel,
}
scores = {}
#%%
np.random.seed(1234)
key = '0mm'
nbins=20
n_random_points = 500
# mask = apertures[key]*1e2>.16
mask = apertures[key]*1e2>(apertures[key]*1e2).min()+0.1
fig, (axi, ax0) = plt.subplots(1, 2, figsize=(20,8), gridspec_kw={'width_ratios': [1, 2]})
# plt.hist(apertures[key][~np.isclose(apertures[key], 0)] * 1e2, bins=nbins, alpha=0.5, density=True)

hist, bins = np.histogram(apertures[key][mask] * 1e2, bins=nbins)
logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))

axi.hist(apertures[key][mask] * 1e2, bins=logbins, alpha=0.5, density=True, color='black')
axi.vlines(np.median(apertures[key][mask] * 1e2), ymin=0, ymax = 2.5, color='black', label=f'median true aperture:: {np.median(apertures[key][mask] * 1e2):.2f} cm')
axi.hist(invfw(res.x, amplitudes[key]) * 1e2, bins=nbins, alpha=0.5, density=True, color='red')
axi.vlines(np.median(invfw(res.x, amplitudes[key]) * 1e2), ymin=0, ymax = 2.5, color='red', label=f'median calculated aperture: {np.median(invfw(res.x, amplitudes[key]) * 1e2):.2f} cm')
axi.set_xlabel('Aperture [cm]')
axi.set_ylabel('Density [-]')
axi.set_xscale('log')
axi.legend(fancybox=True, edgecolor='black', frameon=True, loc='upper right')
axi.set_title(r'$\bf{(a)}$ Aperture distribution')
axi.grid(linestyle=':')

plt.show()
#%%
np.random.seed(1234)
key = '0mm'
nbins=20
n_random_points = 500
# mask = apertures[key]*1e2>.16
mask = apertures[key]*1e2>(apertures[key]*1e2).min()+0.1
fig, (axi, ax0) = plt.subplots(1, 2, figsize=(20,8), gridspec_kw={'width_ratios': [1, 2]})
# plt.hist(apertures[key][~np.isclose(apertures[key], 0)] * 1e2, bins=nbins, alpha=0.5, density=True)
axi.hist(apertures[key][mask] * 1e2, bins=nbins, alpha=0.5, density=True, color='black')
axi.vlines(np.median(apertures[key][mask] * 1e2), ymin=0, ymax = 2.5, color='black', label=f'Median true aperture:: {np.mean(apertures[key][mask] * 1e2):.2f} cm')
axi.hist(invfw(res.x, amplitudes[key]) * 1e2, bins=nbins, alpha=0.5, density=True, color='red')
axi.vlines(np.median(invfw(res.x, amplitudes[key]) * 1e2), ymin=0, ymax = 2.5, color='red', label=f'Median calculated aperture: {np.mean(invfw(res.x, amplitudes[key]) * 1e2):.2f} cm')
axi.set_xlabel('Aperture [cm]')
axi.set_ylabel('Density [-]')
axi.legend(fancybox=True, edgecolor='black', frameon=True, loc='upper right')
axi.set_title(r'$\bf{(a)}$ Aperture distribution')
axi.grid(linestyle=':')


# fig, ax0 = plt.subplots()
ax1 = ax0.twinx()

props = []
handles = []
for m, a, n, c, marker, ax in zip([midpoints[key]*1e2, #midpoints[key]*1e2,
                               midpoints[key]*1e2,
                               midpoints[key]*1e2],
                      [apertures[key] * 1e2,
                          amplitudes[key],
                       invfw(res.x, amplitudes[key]) * 1e2],
                      ['True', 'Measured', 'Calculated'],
                      ['black', 'blue', 'red'],
                      ['.','*','x'],
                                  [ax0,ax1,ax0]):

    rand_pos =np.random.permutation(np.arange(len(m)))
    x_random = m[rand_pos[:n_random_points], 0]
    y_random = m[rand_pos[:n_random_points], 1]

    train = a[rand_pos[:n_random_points]]  # Sampled values

    position_xy = np.vstack([x_random, y_random]).T

    num_bins =30


    # Calculate bin edges
    bins = np.linspace(0,  24, num_bins + 1)

    import gstools as gs
    bin_center, gamma = gs.vario_estimate((x_random, y_random), train,
                                      bin_edges=bins,
                                      estimator='cressie'
                                      # sampling_size=3,
                                      # sampling_seed=1234
                                      )

    sc = ax.scatter(bin_center, gamma, label=f'{n} experimental variogram', color=c, marker=marker)
    handles.append(sc)
    fit_model = gs.Matern(dim=2)
    para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)


    x_s = np.linspace(0, max(bin_center)*4/3)
    li, = ax.plot(x_s, fit_model.variogram(x_s),label=f"{n} {fit_model.name} covariance model",color=c)
    handles.append(li)
    # per_scale = fit_model.percentile_scale(0.90)
    li2 = ax.vlines(fit_model.len_scale, ymin=0, ymax=0.1 if n!='Measured' else 0.009, color=c, linestyle=':', label=f'Length scale = {fit_model.len_scale:.2f} cm')
    handles.append(li2)
    # ax.vlines(fit_model.percentile_scale(0.99), ymin=0, ymax=fit_model.var, color=c, linestyle=':', label=f'Range = {fit_model.percentile_scale(0.99):.2f} cm')
    # ax.vlines(fit_model.integral_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dotted')
    # ax.vlines(per_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dashed')
    # ax.hlines(fit_model.var,xmin=0, xmax=x_s.max(), color=c, linestyle='dotted')#, label=f'Sill = {fit_model.var:.2f}')

    props.append((fit_model.var, fit_model.len_scale))

labels = [l.get_label() for l in handles]
ax.legend(handles, labels, fancybox=True, edgecolor='black', frameon=True, loc='lower right')

ax1.set_ylim(-0.0005,0.01)
ax1.yaxis.label.set_color('blue')
ax1.tick_params(axis='y', colors='blue')
ax1.set_ylabel(r'Semi-variogram GPR reflection coefficients [$\gamma$]')
# Set the color of the tick labels
for label in ax1.get_yticklabels():
    label.set_color('blue')


ax.set_xlabel('Lag distance [cm]')
ax.set_ylabel(r'Semi-variogram apertures [$\gamma$]')
ax.grid(linestyle='--')
ax0.set_title(r'$\bf{(b)}$ Variogram')
fig.show()

fig.savefig(odir+'fig07_semivariogram.pdf', bbox_inches='tight')

#%% Not used
# np.random.seed(1234)
# key = '0mm'
# fig, ax = plt.subplots()
# n_random_points = 400
# props = []
#
# for o in [res.x[0],
#           # ome[0],
#           # ome[1],
#           # ome[2]
#
#           ]+np.linspace(ome[0], 3*2*np.pi*1e9, 10).tolist():
#
#     m = midpoints[key]*1e2
#     rand_pos = np.random.permutation(np.arange(len(m)))
#     x_random = m[rand_pos[:n_random_points], 0]
#     y_random = m[rand_pos[:n_random_points], 1]
#
#     train = invfw(o,  amplitudes[key])[rand_pos[:n_random_points]] * 1e2  # Sampled values
#
#     position_xy = np.vstack([x_random, y_random]).T
#
#     num_bins = 30
#
#     # Calculate bin edges
#     bins = np.linspace(0, 25, num_bins + 1)
#
#     import gstools as gs
#
#     bin_center, gamma = gs.vario_estimate((x_random, y_random), train,
#                                           bin_edges=bins,
#                                           estimator='cressie'
#                                           # sampling_size=3,
#                                           # sampling_seed=1234
#                                           )
#
#     sc = ax.scatter(bin_center, gamma, label=f'{convert_frequency(o/(2*np.pi))} experimental variogram')#, color=c, marker=marker)
#     c = sc.get_facecolor()[0]
#     # continue
#     fit_model = gs.Matern(dim=2)
#     para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)
#
#     x_s = np.linspace(0, max(bin_center) * 4 / 3)
#     ax.plot(x_s, fit_model.variogram(x_s), color=c)#, label=f"{n} {fit_model.name} covariance model. {convert_frequency(o/(2*np.pi))}", color=c)
#
#     # per_scale = fit_model.percentile_scale(0.90)
#     ax.vlines(fit_model.len_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='-.',
#               # label=f'Length scale = {fit_model.len_scale:.2f} cm'
#               )
#     # ax.vlines(fit_model.percentile_scale(0.99), ymin=0, ymax=fit_model.var, color=c, linestyle=':',
#     #           label=f'Range = {fit_model.percentile_scale(0.99):.2f} cm')
#     # ax.vlines(fit_model.integral_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dotted')
#     # ax.vlines(per_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dashed')
#     ax.hlines(fit_model.var, xmin=0, xmax=x_s.max(), color=c, linestyle='dotted')#, label=f'Sill = {fit_model.var:.2f}')
#
#     props.append((fit_model.var, fit_model.len_scale))
# ax.legend(fancybox=True, edgecolor='black', frameon=True)#, loc='lower right')
# ax.set_xlabel('Lag distance [cm]')
# ax.set_ylabel(r'Semi-variogram [$\gamma$]')
# ax.grid(linestyle='--')
# fig.show()
