from logging import raiseExceptions

import numpy as np
import matplotlib.pyplot as plt
from bokeh.util.compiler import Implementation

from fracwave import FractureGeo, OUTPUT_DIR

file_h5 = OUTPUT_DIR + 'fracture_aperture/figures_paper/sketch.h5'

odir = OUTPUT_DIR + 'fracture_aperture/figures_paper/'
#%%
frac = FractureGeo()
width = 100
length = 100
# max_element_size = .2  # 1 mm resolution

seed = 2021, 2022
v, f = frac.create_regular_squared_mesh(width=width, length=length, center=(0,0,0), azimuth=0, dip=0, resolution=[10,10])
# vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, center=(0,0,0), azimuth=0, dip=0, resolution=spacing)

# surf = frac.create_pyvista_mesh(vertices, faces)

spacing = [600, 600]
surf, vertices, faces = frac.remesh(points=v, spacing=spacing,
                                    # max_element_size=max_element_size,
                                    plane=2)

#%%
try:
    raise NotImplementedError
    d = np.load(odir+'files.npz')
    top_surface_m=d['top_surface_m']
    bottom_surface_m=d['bottom_surface_m']
    apertures=d['apertures']
    midpoints = d['midpoints']

except:

#%%
    midpoints = vertices#frac.get_midpoints(vertices, faces)
    # model= 'exponential'
    import gstools as gs
    # m = [gs.Gaussian, gs.Exponential, gs.Matern]#, gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
    m = gs.Matern#, gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
         # gs.Circular, gs.Spherical, gs.HyperSpherical, gs.SuperSpherical, gs.JBessel, gs.TPLSimple]

    # fig, axes = plt.subplots(4, 4, figsize=(12, 12))
    # fig, (ax1,ax2) = plt.subplots(1, 2, figsize=(12, 5))
    # fig, axes = plt.subplots(1, 3, figsize=(12, 5))
    # Flatten the 2D array of subplots for easier indexing
    # axes = axes.flatten()
    # for model, ax in zip(m, axes):
    field1, srf1 = frac.generate_crf(points=midpoints, var=0.05, len_scale=6, plane=2, seed=seed[0], model=m)
    field2, srf2 = frac.generate_crf(points=midpoints, var=0.05, len_scale=6, plane=2, seed=seed[1], model=m)

        # #%%
    Z_b = field1.reshape(spacing)
    Z_t = field2.reshape(spacing)
    #
    # caxb = ax1.pcolormesh(Z_b, cmap='turbo', )
    # caxt = ax2.pcolormesh(Z_t, cmap='turbo', )
    # ax1.set_title('Bottom')
    # ax2.set_title('Top')
    # fig.colorbar(caxb, ax=ax1)
    # fig.colorbar(caxt, ax=ax2)
    #
    # # Hide any empty subplots (in case you have fewer than 16 variables)
    # # for j in range(len(m), len(axes)):
    # #     fig.delaxes(axes[j])
    # plt.tight_layout()
    # plt.show()
    # #%%
    # plt.scatter(midpoints[:,0], midpoints[:,1], c=field2, cmap='turbo', )
    # plt.colorbar()
    # plt.show()
    #%%
    b_mean = 0.3

    top_surface=field1
    bottom_surface=field2
    # top_surface2 = top_surface + b_mean
    # index = top_surface2.ravel() <= bottom_surface.ravel()
    # top_surface2[index] =  0
    # bottom_surface[index] = 0
    #
    #
    #
    # tp = top_surface2.reshape(spacing)
    # bt = bottom_surface.reshape(spacing)
    #
    # plt.pcolormesh(tp);
    #
    # plt.colorbar();
    # plt.show()

    apertures = top_surface + b_mean - bottom_surface
    mask = apertures <= 0

    apertures[mask] = 0

    top_surface_m = top_surface.copy()
    # top_surface_m[mask] = -b_mean*0.5  bottom_surface_m[mask] = 0
    top_surface_m[mask] = -b_mean*0.5
    top_surface_m += b_mean*0.5
    # min_val = top_surface.min()
    # top_surface_m[mask] = min_val
    # top_surface_m -= top_surface_m.min()

    bottom_surface_m = bottom_surface.copy()
    bottom_surface_m[mask] = b_mean*0.5
    bottom_surface_m -= b_mean*0.5
    # max_val = top_surface.max()
    # bottom_surface_m[mask] = max_val
    # bottom_surface_m -= bottom_surface_m.min()

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(15, 5))

    caxb = ax1.pcolormesh(Z_b, cmap='turbo', )
    caxt = ax2.pcolormesh(Z_t, cmap='turbo', )
    caxa = ax3.pcolormesh(apertures.reshape(spacing), cmap='turbo', )
    ax1.set_title('Bottom')
    ax2.set_title('Top')
    ax3.set_title('Apertures')
    fig.colorbar(caxb, ax=ax1)
    fig.colorbar(caxt, ax=ax2)
    fig.colorbar(caxa, ax=ax3)

    # Hide any empty subplots (in case you have fewer than 16 variables)
    # for j in range(len(m), len(axes)):
    #     fig.delaxes(axes[j])
    plt.tight_layout()
    plt.show()

    np.savez(odir + 'files.npz',
             top_surface_m=top_surface_m,
             bottom_surface_m=bottom_surface_m,
             apertures=apertures,
             midpoints=midpoints)

#%%
X = midpoints[:, 0].reshape(spacing)
Y = midpoints[:, 1].reshape(spacing)

Z_A = apertures.reshape(spacing)
Z_t = top_surface_m.reshape(spacing)
Z_b = bottom_surface_m.reshape(spacing)
mask_r =mask.reshape(spacing)
ap = Z_t - Z_b
assert np.allclose(ap, Z_A)
assert np.allclose(Z_t[mask_r] + Z_b[mask_r], Z_A[mask_r])

#%% Fracture
frac = FractureGeo()
resolution = (45,45)

def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = odir
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints_l = (d['midpoints'])#+np.asarray([17,17,0]))
extent_original=(midpoints_l[:,0].min(), midpoints_l[:,0].max(), midpoints_l[:,1].min(), midpoints_l[:,1].max())

apertures_original = np.asarray(rotate_matrix_clockwise( apertures_l.reshape(spacing), rotations=0)) *1e-2

#%%
mask = np.isclose(top_surface_m, bottom_surface_m).reshape(spacing)
top = top_surface_m.reshape(spacing)
bot = bottom_surface_m.reshape(spacing)
ape = apertures_l.reshape(spacing)

# top[mask] = None
# bot[mask] = None
ape[mask] = None
#%%
fig, (ax0, ax1, ax2) = plt.subplots(1,3, figsize=(20,10))

cax0=ax0.imshow(rotate_matrix_clockwise(top,rotations=3), cmap='cividis', extent = extent_original, aspect='equal')
cax1=ax1.imshow(rotate_matrix_clockwise(bot,rotations=3), cmap='cividis', extent = extent_original, aspect='equal')
cax2=ax2.imshow(rotate_matrix_clockwise(ape,rotations=3), cmap='viridis', extent = extent_original, aspect='equal')

ax0.set_title(r'$\bf{(a)}$ Top rough surface', fontsize=20, pad=20)
ax1.set_title(r'$\bf{(b)}$ Bottom rough surface',fontsize=20, pad=20)
ax2.set_title(r'$\bf{(c)}$ Thickness Top + Bottom surfaces', fontsize=20, pad=20)


for ax in (ax0,ax1,ax2):
    ax.set_xlabel('x [cm]', fontsize=16)
    ax.set_ylabel('y [cm]', fontsize=16)

cbar0=fig.colorbar(cax0, ax=ax0, label='Height [cm]', orientation= 'horizontal')
cbar1=fig.colorbar(cax1, ax=ax1, label='Height [cm]', orientation= 'horizontal')
cbar2=fig.colorbar(cax2, ax=ax2, label='Aperture [cm]', orientation= 'horizontal')

plt.show()

fig.savefig(odir+'fig01_top_bottom_surfaces.pdf', bbox_inches='tight')