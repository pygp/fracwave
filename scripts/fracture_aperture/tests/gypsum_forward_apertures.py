import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
from hurst import compute_Hc
import scipy.stats

p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain
from gdp.processing.time_lapse import hilbert_phase_difference

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM

file_h5 = OUTPUT_DIR + 'fracture_aperture/gypsum_block_heter_apertures.h5'

#%% Source
sou = SourceEM()
try:
    sou.load_hdf5(file_h5)
except:
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 655))

    sou.set_delay(delay=1)
    # sou.set_source_params(a=9.17809204,
    #                       c=0.95575954,
    #                       loc=0,
    #                       scale=0.28381535)
    sou.set_source_params(a=14.99999994,
                          c=0.68261749,
                          loc=0,
                          scale=0.05180656)
    sou.create_source()
    # sou.widgets().show()

    # fig = sou.plot_waveforms_zoom()
    fig = sou.plot_waveforms_complete()
    fig.show()
    sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5

#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5.split('.')[0] + '_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%% Fracture
frac = FractureGeo()
resolution = (45,45)

def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2
extent_original=(midpoints_l[:,0].min(), midpoints_l[:,0].max(), midpoints_l[:,1].min(), midpoints_l[:,1].max())

apertures_original = np.asarray(rotate_matrix_clockwise( apertures_l.reshape(spacing), rotations=3)) *1e-2

try:
    frac.load_hdf5(file_h5)
    surf = frac.get_surface()
    apertures = frac.fractures['aperture'].to_numpy()
except:

    width = 0.30
    length = 0.30
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0,
                                                       resolution=resolution, center=(.17,.17,0))
    # surf, vertices, faces = frac.remesh(points=vertices, max_element_size=max_element_size, plane=2)

    surf = frac.create_pyvista_mesh(vertices, faces)

    p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)

    # outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
    # d = np.load(outname + 'files.npz')
    # top_surface_m = d['top_surface_m']
    # bottom_surface_m = d['bottom_surface_m']
    # apertures_l = d['apertures']
    # spacing = [600, 600]
    # midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2

    apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing),
                                                 rotations=1)

    scaling_factors = (np.array(resolution) / spacing).tolist()
    from scipy.ndimage import zoom
    # Perform the downsampling using zoom
    downsampled_image = zoom(apertures_original, scaling_factors)
    scaling_factors_up = (spacing / np.array(resolution)).tolist()
    upsampled_image = zoom(downsampled_image, scaling_factors_up)

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
    cax1 = ax1.imshow(apertures_original, cmap='viridis')
    cax2 = ax2.imshow(downsampled_image, cmap='viridis')
    cax3 = ax3.imshow(upsampled_image, cmap='viridis')
    cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.colorbar(cax3, ax=ax3)
    fig.colorbar(cax4, ax=ax4)
    ax1.set_title('Original')
    ax2.set_title('Downsampled')
    ax3.set_title('Upsampled')
    ax4.set_title('Original-Upsampled')
    fig.show()
# apertures = apertures.reshape(spacing)
    for i in [0, 2e-3,3e-3]:

        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2 + i
        kwarg_properties = dict(aperture=apertures, #0.01,
    #     kwarg_properties = dict(aperture=0.1,
                                    electrical_conductivity=0,
                                    electrical_permeability=1,
                                plane=2,
                                overwrite=True)
        frac.set_fracture(name_fracture=f'fracture_{int(i*1000)}',
                          vertices=vertices,
                          faces=faces,
                          **kwarg_properties)

        frac.export_to_hdf5(file_h5, overwrite=True)
p.add_object(frac.get_midpoints(name_fractures='fracture_0'), name='fracture', scalars=frac.df.loc[frac.df['name']=='fracture_0','aperture'].to_numpy())
#%%
# p.add_object(ant2.Receiver, name='rx', color='blue', opacity=0.2)
view = 'D'
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Rx','Ry','Rz']].to_numpy(), name='rx', color='blue', opacity=0.2)
# p.add_object(ant2.Transmitter, name='tx', color='red', opacity=0.2)
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Tx','Ty','Tz']].to_numpy(), name='tx', color='red', opacity=0.2)
#%%
p.remove('rx')
p.remove('tx')
p.add_object(ant.get_midpoints(), name='mid', color='black', opacity=0.2)


#%%
paths_all_apertures = {'0mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/0mm/SEGY_20240412_172224/',
                        '2mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/2mm/SEGY_20240412_172341/',
                        '3mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/3mm/SEGY_20240412_172435/',
                        'alum':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135'}
data_measu = {}
data_together = {}

# When pol=0 then we look at antenna orientation (1,0,0). ['B', 'C']
# When pol = 1 then antenna is (0,1,0). ['A, 'D']
pol = 1 # polarization of the antennas.
th = '2mm'
vmax = 0
# out = [12, 22]
out = [9, 26]
for key, base_path in paths_all_apertures.items():



    # Get all the files in the folder
    folders = os.listdir(base_path)
    # loop through all the folders and get the files. From the filename extract the information.
    # The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

    # Create a dictionary to store the information
    data_measu[key] = {'A':{},  # Up direction (0,1,0)
                       'B':{},  # Right dir (1,0,0)
                       'C':{},  # Up dir (1,0,0)
                       'D':{}}  # Right dir (0,1,0)

    vmax = 0

    for folder in folders:
        # Get the files
        files = os.listdir(os.path.join(base_path, folder))
        for file in files:
            if '.sgy' not in file:
                continue
            info = re.split('_', file)
            # identif = info[0]
            # Get the date
            # date = info[1]
            # Get the ID
            ID = info[0]
            # Get the path
            # experiment_number = int(info[2])
            experiment_number = int(info[1])
            n_e_id = ID + '_' + str(experiment_number)
            midpoints = ant.get_midpoints(n_e_id)

            if ID in ['B', 'D']:
                lims_x = (10,85)
                # ma = ((midpoints[:, 0] >= out[0]*1e-2) & (midpoints[:, 0] <= out[1]*1e-2) &
                #       (midpoints[:, 1] >= out[0]*1e-2) & (midpoints[:, 1] <= out[1]*1e-2))
            elif ID in ['A', 'C']:
                lims_x = (10, 82)
            else:
                raise AttributeError
            ma = ((midpoints[:, 0] >= out[0] * 1e-2) & (midpoints[:, 0] <= out[1] * 1e-2) &
                 (midpoints[:, 1] >= out[0] * 1e-2) & (midpoints[:, 1] <= out[1] * 1e-2))
            if True in ma:
                print(n_e_id)
            else:
                print('no_' + n_e_id)
                continue

            # if experiment_number < out[0] or experiment_number > out[1]:
            #     print('no_'+n_e_id)
            #     continue
            # else:
            #     print(n_e_id)
            path = os.path.join(base_path, folder, file)
            # Get the data
            data_cube, header = load_sgy(path)

            # if ID =/= 'B':  # It is flipped
                # data_cube = np.fliplr(data_cube)
            # Store the data in the dictionary
            # d = {experiment_number: {'header':header, 'data': data_cube}}
            # Processing
            d_de = detrend(data_cube)
            sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
            c0 = 299_792_458  # Speed of light in m/s
            rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
            velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
            # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
            # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
            d_de_ga = d_de.copy()
            data_uncut = d_de_ga.copy()
            mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
            if len(ma) > len(mask_x):
                mask_x = ma[:len(mask_x)]
                midpoints = midpoints[:len(mask_x)]
            else:
                mask_x[:len(ma)] = ma
            # lims_y = (80, 100)
            lims_y = (70, 110)
            d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
            d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
            d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from left
            d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from right
            # d_de_ga[:, mask_x] = 0 # crop all reflections from right
            time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
            depth_vector = time_vector * velocity * 0.5
            vma = np.abs(d_de_ga).max()
            if vma > vmax: vmax=vma
            d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
                 'time_vector':time_vector, 'depth_vector':depth_vector,
                 'lims_x': lims_x, 'lims_y': lims_y,
                 'data_cut': data_uncut[:, mask_x],
                 'data_cut_all': data_uncut[lims_y[0]:lims_y[1], mask_x],
                 'mask_x': mask_x,
                 'midpoints':midpoints,
                 }
            data_measu[key][ID][experiment_number] = d
            if np.abs(np.max(d_de_ga)) > vmax:
                vmax=np.abs(np.max(d_de_ga))

#%%

# Data from which the positions are available
available = []
for key, v in data_measu['alum'].items():
    if len(v) == 0:
        # is empty:
        continue
    n = [key+'_'+str(num) for num, _ in v.items()]
    available.append(n)

#%%

# from impdar.lib import load
# dat = load.load('mat','data/synthetic_radargram.mat')[0]
#%%
inf = data_measu['0mm']['A'][15]
# inf = data_measu['0mm']['C'][10]

# inf = data_together['2mm']['AC'][12]
# lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
# d = inf['data']
# d = inf['data_uncut']
# d = inf['data_cut']
d = inf['data_cut_all']
# from scipy.io import savemat
# savemat(OUTPUT_DIR+'AC_0mm_10', inf)
# vmax=d.max() * 0.05
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  inf['time_vector'] #* 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
vmax =  np.max(np.abs(d))
plt.figure()
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           extent=extent
           )

# plt.ylabel('Distance (cm)')
plt.ylabel('Two way travel time (ns)')
# plt.ylim(2.7,1.5)
# plt.ylim(4,1)
# plt.xlim(20,80)
plt.colorbar()
plt.show()

#%%
t = 15
l = 'D'  # Only A and D

plt.figure()
alum = data_measu['alum'][l][t]['data_cut_all'][:, t].copy()
norm = np.abs(alum).max()
alum/= norm
depth = data_measu['alum'][l][t]['depth_vector'].copy()[lims_y[0]:lims_y[1]]
plt.plot(depth, data_measu['0mm'][l][t]['data_cut_all'].copy()[:, t]/norm, label='0mm')
plt.plot(depth,  data_measu['2mm'][l][t]['data_cut_all'][:, t].copy()/norm, label='2mm')
plt.plot(depth, data_measu['3mm'][l][t]['data_cut_all'][:, t].copy()/norm, label='3mm')
plt.plot(depth, alum, label='Alum')
# plt.xlim(70,110)
# plt.xlim(0.1,0.15)
plt.legend()
plt.show()

#%% lets get the aperture based on the position of the antennas
def find_nearest(x, y, positions):
  """
  Finds the nearest position (index) in a list of positions to a given point.

  Args:
      x: X-coordinate of the target point.
      y: Y-coordinate of the target point.
      positions: List of tuples containing (x, y) coordinates.

  Returns:
      The index of the nearest position in the positions list.
  """
  distance = ((positions[:, 0] - x) ** 2 + (positions[:, 1] - y) ** 2) ** 0.5
  nearest_idx = np.argmin(distance)
  return nearest_idx

#%%
midpoints = ant.get_midpoints()[:, :2]

# sort x
mask_x = (out[0]*1e-2 < midpoints[:,0]) & (midpoints[:,0] < out[1]*1e-2)
mask_y = (out[0]*1e-2 < midpoints[:,1]) & (midpoints[:,1] < out[1]*1e-2)
mask_idx = mask_x & mask_y

position_df = ant.df[mask_idx]
mid_ant_pos = (position_df[['Rx','Ry','Rz']].to_numpy() + position_df[['Tx','Ty','Tz']].to_numpy()) * 0.5
# p.remove('rx')
# p.remove('tx')
p.add_object(mid_ant_pos,
             name='mid_d', color='red', opacity=0.3)

#%%
nearest_idx_pos = np.asarray([find_nearest(po[0],po[1], positions=frac.get_midpoints(name_fractures='fracture_0')[:,:2]) for po in mid_ant_pos[:,:2]])
aperture_from_near = frac.df['aperture'][nearest_idx_pos].to_numpy()
#%%
p.add_object(mid_ant_pos,
             name='mid_d', scalars=aperture_from_near)

#%% Calculate reflection coefficients

midpoints = {}
apertures = {}
amplitudes = {}
for key, a in data_measu.items():
    if key == 'alum':
        continue
    midpoints[key] = []
    apertures[key] = []
    amplitudes[key] = []
    for key2, b in a.items():
        if key2 in ['B', 'C']:
            continue
        for key3, c in b.items():
            data_al = data_measu['alum'][key2][key3]['data_cut_all']
            # norm = np.abs(data_al).max(axis=0)
            norm = np.linalg.norm(data_al,axis=0, ord=2)
            data = c['data_cut_all'] / norm  # Normalize to maximum of aluminium
            lims_x = c['lims_x']
            lims_y = c['lims_y']
            depth_vector = c['depth_vector']
            mask_x = c['mask_x'] #[:101]  # I have only 100 points always
            midp = c['midpoints']
            if len(mask_x) > len(midp):
                mask_x = mask_x[:len(midp)]
            midp = midp[mask_x]

            df_ap = frac.get_fracture(name_fracture=f'fracture_{key[0]}')

            nearest_idx_pos = np.asarray(
                [find_nearest(po[0], po[1], positions=df_ap[['x','y']].to_numpy()) for po in midp[:,:2]])
            aperture_from_near = df_ap['aperture'].to_numpy()[nearest_idx_pos]
            sigma_from_near = df_ap['elec_conductivity'].to_numpy()[nearest_idx_pos]
            epsilon_from_near = df_ap['elec_permeability'].to_numpy()[nearest_idx_pos]

            midpoints[key].append(midp)
            apertures[key].append(aperture_from_near)
            # amplitudes[key].append(np.abs(data).max(axis=0))
            amplitudes[key].append(np.linalg.norm(data,axis=0))

    midpoints[key] = np.vstack(midpoints[key])
    apertures[key] = np.hstack(apertures[key])
    amplitudes[key] = np.hstack(amplitudes[key])
# p.add_object(midpoints, scalars=apertures, name='new')

#%%

for key in  midpoints.keys():
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10,5))
    cax0= ax0.scatter(midpoints[key][:,0], midpoints[key][:,1], c=apertures[key], cmap='viridis', s=10, vmin=0, vmax=apertures['3mm'].max())
    cax1 = ax1.scatter(midpoints[key][:,0], midpoints[key][:,1], c=amplitudes[key], cmap='viridis', s=10, vmin=0, vmax=amplitudes['3mm'].max())
    ax0.set_title(f'Apertures_{key}')
    ax1.set_title(f'Amplitudes_{key}')
    fig.colorbar(cax0, ax=ax0)
    fig.colorbar(cax1, ax=ax1)
    plt.tight_layout()
    plt.show()
    # break

#%%
solv = FracEM()
solv.open_file(file_h5)
rock_epsilon =6.5
solv.rock_epsilon =rock_epsilon

rock_sigma = 0.0
solv.rock_sigma = rock_sigma

epsilon = frac.df['elec_permeability'].unique()[0]
sigma = frac.df['elec_conductivity'].unique()[0]

solv.engine = 'tensor_trace'

from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_frequencies
mask_freq = get_mask_frequencies(sou.source, threshold=0.1)
omega = sou.frequency[mask_freq]* 2*np.pi *1e9
#%%
def average(data, type='arithmetic', distance=None):
    if type == 'arithmetic':
        return np.mean(data)
    elif type == 'median':
        return np.median(data)
    elif type == 'mode':
        counts = np.unique(data, return_counts=True)[1]
        # Find the maximum count
        max_count = np.max(counts)
        # Get all modes (values with max count)
        modes = data[counts == max_count]
        return modes.tolist()  # Convert to list for return
    elif type == 'geometric_mean':
        # Check for negative values or zeros
        if any(value <= 0 for value in data):
            raise ValueError("Geometric mean is not defined for non-positive values.")

        # Calculate the product and take the nth root
        product = np.prod(data)
        n = len(data)
        return np.power(product, 1 / n)
    elif type == 'harmonic_mean':
        if any(value <= 0 for value in data):
            raise ValueError("Harmonic mean is not defined for non-positive values.")

        # Calculate the sum of reciprocals and take the reciprocal
        reciprocals = 1 / np.array(data)
        return 1 / np.mean(reciprocals)
    elif type == 'distance_weight':
        weights = 1 / distance**2  # Weights based on inverse distance
        weights = weights / np.sum(weights)  # Normalize weights to sum to 1
        return np.average(data, weights=weights)
    else:
        raise AttributeError

#%%
def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5


distance = 0.12  # Top to the view
rock_epsilon = 6.5
c0 = 299_792_458
velocity = c0 * 1e-7 / np.sqrt(rock_epsilon)
frequency = 3.6 # GHz
wavelength = solv.velocity / sou.center_frequency  # Result in cm

r = fresnel(distance, wavelength, 1)

#%%
from tqdm.autonotebook import tqdm
# options are ['arithmetic', 'geometric_mean', 'harmonic_mean', 'median', 'mode', 'distance_weight]
typ = 'distance_weight'
try:
    # raise FileNotFoundError
    apertures_fresnel = {key:np.load(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{typ}_{key}.npy') for key in midpoints.keys()}
except FileNotFoundError:

    def apertures_inside_circle(point_cloud, apertures, circle_origin, circle_radius):
        # Convert the point cloud to a NumPy array for easier calculations
        point_cloud_array = np.array(point_cloud) + 100
        circle_origin =  np.array(circle_origin) + 100

        # Calculate the distances from each point to the circle's origin
        distances = np.linalg.norm(point_cloud_array - circle_origin, axis=1)

        # Find indices of points that are inside the circle (distance < radius)
        inside_circle_indices = np.where(distances < circle_radius)[0]
        point_cloud_array -= 100
        circle_origin -= 100
        # Extract the points that are inside the circle
        points_inside_circle = point_cloud_array[inside_circle_indices]
        apertures_inside_cirlce = apertures[inside_circle_indices]
        distance = distances[inside_circle_indices]

        return points_inside_circle, apertures_inside_cirlce, distance
    apertures_fresnel ={}
    for key in midpoints.keys():
        mid = midpoints[key]
        apertures_fresnel2 = []
        position_fresnel = []
        dist_frenel = []
        mask = frac.df['name'] == f'fracture_{key[0]}'
        for m in tqdm(mid):#middle_points):
            points, apert, dista = apertures_inside_circle(frac.get_midpoints()[mask,:2],
                                                    frac.df['aperture'][mask].to_numpy(), m[:2], r)
            if points.size == 0:
                apertures_fresnel2.append(0)
                position_fresnel.append([0])
                dist_frenel.append([0])
                continue
            # if type in ['geometric_mean', 'harmonic_mean']:
            apert = np.abs(apert)

            apertures_fresnel2.append(average(apert, type=typ, distance=dista))
            position_fresnel.append(points)
            dist_frenel.append(dista)
        # break
        apertures_fresnel[key] = np.c_[mid[:,0],mid[:,1], apertures_fresnel2]

    [np.save(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{typ}_{key}.npy', a) for key, a in apertures_fresnel.items()]


#%%
x_mid = 0.036
z = 0.10
theta = np.arctan(x_mid/(2*0.1))
# fe = np.array([1600 *1e6])# * 2 * np.pi#omega
# fe = np.array([sou.center_frequency * 1e9])
# fe = np.array([sou.center_frequency * 1e9])
# ome = omega #fe  * 2 * np.pi
ome = [omega[0], sou.center_frequency * 1e9 * 2 * np.pi, omega[-1]]
re = solv.rock_epsilon  # 9.5
rs = solv.rock_sigma  # 0.002
fe = frac.df['elec_permeability'].unique()[0] # 16
fs = frac.df['elec_conductivity'].unique()[0]  # 0.1

# v = solv.c0 / np.sqrt(re)
# o = sou.center_frequency *1e9
wavelength = solv.wavelength
# m = 0.3 * wavelength #0.02 # m
m = 2 * wavelength #0.02 # m
d = np.linspace(0, m, 1000)[None, :]
# k1 = wavenumber(ome, epsilon=re, sigma=rs, complex=False)[:, None]
# k2 = wavenumber(ome, epsilon=e, sigma=s, complex=False)[:, None]

k1c = solv.wavenumber(omega=ome, epsilon=re, sigma=rs)[:, None]
k2c = solv.wavenumber(omega = ome, epsilon=fe, sigma=fs)[:, None]

#%%
# G = solv.reflection_all(k1, k2, theta=0, aperture=d)
Gc = solv.reflection_all(k1c, k2c, theta=0, aperture=d)
Gt = solv.reflection_all(k1c, k2c, theta=theta, aperture=d)
ap = frac.df['aperture'].to_numpy()
# Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
# Gfresnel = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
#%%
poly_x = np.hstack([v for v in amplitudes.values()])
poly_y = np.hstack([v[:,-1] for v in apertures_fresnel.values()])
par = np.polyfit(poly_x, poly_y, deg=1)
# [par,_] = np.polyfit(poly_x, poly_y, deg=1)
# par = np.polyfit(amplitudes[key], apertures[key], 1)# full=True)
pl = np.poly1d(par)

#%%
# from sklearn.linear_model import LinearRegression
# model = LinearRegression(poly_x, poly_y)#fit_intercept=False)
# model2 = LinearRegression(poly_y, poly_x)#fit_intercept=False)
# 
# # Fit the model to the data
# model.fit(poly_x.reshape(-1, 1), poly_y)
# model.fit(poly_x.reshape(-1, 1), poly_y)
# # slope = model.coef_[0]
# def pl(x):  return model.predict(x)
# def p2l(x):  return model2.predict(x)
#%%
def fw(o):
    k1c = solv.wavenumber(omega=o, epsilon=re, sigma=rs)
    k2c = solv.wavenumber(omega=o, epsilon=fe, sigma=fs)
    rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=apertures_fresnel[key][:,-1])
    return np.sqrt(np.sum(np.square(np.abs(rc) - amplitudes[key])))

x = sou.center_frequency * 2 * np.pi

from scipy.optimize import minimize
res = minimize(fw,
                x,
                method='nelder-mead',
                options={#'maxfun': 10000,
                         'xatol': 1e-6,
                         'disp': False})
k1c = solv.wavenumber(omega=res.x, epsilon=re, sigma=rs)
k2c = solv.wavenumber(omega=res.x, epsilon=fe, sigma=fs)
rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=d[0])

# If this works then
# def pl(amplitudes):

    # return aperture
#%%
# k1c = solv.wavenumber(omega=res.x, epsilon=re, sigma=rs)
# k2c = solv.wavenumber(omega=res.x, epsilon=e, sigma=s)
# rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=d[0])

#%%
from fracwave.utils.help_decorators import convert_frequency, convert_meter
# f = [convert_frequency(i/(2*np.pi)) for i in omega]
# f = [convert_frequency(i/(2*np.pi)) for i in ome]
f = convert_frequency(res.x[0])

fig, ax = plt.subplots(figsize=(10,5))
# [plt.plot(d[0]/wavelength, np.abs(l), alpha=0.1, label=i) for l, i in zip(Gc, f)]
[ax.plot(d[0]*1e2, np.abs(l), alpha=0.5, label=f'Freq: {i}') for l, i in zip(Gt, f)]
# [plt.scatter(ap/wavelength, np.abs(l), s=5)  for l in Gd]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key) for key in midpoints.keys()]
[ax.scatter(apertures_fresnel[key][:,-1]*1e2, amplitudes[key], s=5, color='k', label='Data' if key=='0mm' else None, alpha=0.6) for key in midpoints.keys()]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key+'_fresnel') for key in midpoints.keys()]
# ax.plot(pl(np.linspace(0,0.5,100))*1e2, np.linspace(0,0.5,100), label=f'Lin. Regression')

plt.plot(d[0]*1e2, np.abs(rc), label=f'fitted_{convert_frequency(res.x[0]/(2*np.pi))}')
# [plt.plot(d[0]/wavelength, np.abs(l), label=f[0]) for l in G]
# ax.axvline(apertures['3mm'].max()*1e2)
ax.set_ylabel('Reflection Coefficients (-)')
ax.set_xlabel('Aperture (cm)')
ax.legend()#bbox_to_anchor=(.95, 0.9))#, labels=f)
ax.set_title(f'Wavelength: {convert_meter(wavelength)}')
ax.set_xlim(0, 1.4)
ax.grid()
fig.show()
#%%
from fracwave.utils.help_decorators import convert_frequency, convert_meter
# f = [convert_frequency(i/(2*np.pi)) for i in omega]
f = [convert_frequency(i/(2*np.pi)) for i in ome]

fig, ax = plt.subplots(figsize=(10,5))
# [plt.plot(d[0]/wavelength, np.abs(l), alpha=0.1, label=i) for l, i in zip(Gc, f)]
[ax.plot(d[0]*1e2, np.abs(l), alpha=0.5, label=f'Freq: {i}') for l, i in zip(Gt, f)]
# [plt.scatter(ap/wavelength, np.abs(l), s=5)  for l in Gd]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key) for key in midpoints.keys()]
[ax.scatter(apertures_fresnel[key][:,-1]*1e2, amplitudes[key], s=5, color='k', label='Data' if key=='0mm' else None, alpha=0.6) for key in midpoints.keys()]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key+'_fresnel') for key in midpoints.keys()]
# ax.plot(pl(np.linspace(0,0.5,100))*1e2, np.linspace(0,0.5,100), label=f'Lin. Regression')

plt.plot(d[0]*1e2, np.abs(rc), label=f'fitted_{convert_frequency(res.x[0]/(2*np.pi))}')
# [plt.plot(d[0]/wavelength, np.abs(l), label=f[0]) for l in G]
# ax.axvline(apertures['3mm'].max()*1e2)
ax.set_ylabel('Reflection Coefficients (-)')
ax.set_xlabel('Aperture (cm)')
ax.legend()#bbox_to_anchor=(.95, 0.9))#, labels=f)
ax.set_title(f'Wavelength: {convert_meter(wavelength)}')
# ax.set_xlim(0,1.4)
ax.grid()
fig.show()

#%%
generate_field = np.c_[midpoints[key][:, 0], midpoints[key][:,1], pl(amplitudes[key])]
vmin=0
vmax=0.014

for key in midpoints.keys():
    x, y = midpoints[key][:, 0], midpoints[key][:, 1]
    fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(15,5), sharey=True)
    # cax0=ax0.scatter(x, y, c=apertures[key], cmap='turbo', s=20, vmin=vmin, vmax=vmax)
    # cax0=ax0.scatter(midpoints_l[:,0], midpoints_l[:,1], c=apertures_l*1e-2 + int(key[0])*1e-3, cmap='turbo', vmin=vmin, vmax=vmax)
    cax0=ax0.imshow(apertures_original + int(key[0])*1e-3, cmap='turbo', extent=extent_original, vmin=vmin, vmax=vmax, aspect='auto')
    ax0.set_xlim(x.min(), x.max())
    ax0.set_ylim(y.min(), y.max())
    cax1=ax1.scatter(x, y, c=amplitudes[key], cmap='viridis', s=25)
    cax2=ax2.scatter(x, y, c=pl(amplitudes[key]), cmap='turbo', s=25, vmin=vmin, vmax=vmax)
    ax0.set_title('Apertures (m)')
    ax1.set_title('Amplitude/norm [Reflection Coefficient]')
    ax2.set_title('Generated Apertures')
    fig.colorbar(cax0, ax=ax0)
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.tight_layout()
    plt.show()
    break

#%%
fig, AX = plt.subplots(1,3, figsize=(15,5))
for key, ax in zip(midpoints.keys(), AX):

    ax.hist(apertures[key], alpha=0.5, bins=50, label='Apertures')#, density=True)
    ax.hist(pl(amplitudes[key]), alpha=0.5, bins=50, label='inferred apertures')#, density=True)
    ax.legend()
    ax.grid()
    ax.set_xlabel('Aperture (m)')
    ax.set_ylabel('PDF')
    ax.set_title(key)
plt.show()
# cax0 = ax0.scatter(midpoints_l[:, 0], midpoints_l[:, 1], c=apertures_l * 1e-2 + int(key[0]) * 1e-3, cmap='turbo',
#                    vmin=vmin, vmax=vmax)

#%%
fig, ax = plt.subplots()
ax.hist(np.hstack([apertures[key] for key in midpoints.keys()]), alpha=0.5, bins=50, label='Apertures') #, density=True)
ax.hist(np.hstack([pl(amplitudes[key]) for key in midpoints.keys()]), alpha=0.5, bins=50, label='inferred apertures')#, density=True)
ax.legend()
ax.grid()
ax.set_xlabel('Aperture (m)')
ax.set_ylabel('PDF')
# ax.set_title(key)
plt.show()
#%%
# # plt.plot(apertures[key], 'k.')
# # plt.plot(apertures[key], 'k.')
# fig = plt.figure()
# ax = fig.gca()
# for key in midpoints.keys():
#     # plt.plot((apertures[key] - pl(amplitudes[key]))/wavelength, '.', label=f'error_{key}')
#     # plt.scatter(apertures[key]*1e2, (apertures[key] - pl(amplitudes[key]))*1e2, s=10,label=f'error_{key}')
#     ax.scatter(apertures_fresnel[key][:,-1]*1e2, np.abs((apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2), s=10,label=f'error_{key}')
# # ax.axis('equal')
# ax.set_ylabel(f'abs(Apertures[blue light] - Apertures[predicted])')
# ax.set_xlabel(f'Apertures')
# ax.legend()
# ax.set_aspect('equal', adjustable='box')
# fig.tight_layout()
# fig.show()
#
# #%%
# s = (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2
# n_bins = 50
# mu = s.mean()
# sigma=np.std(s)
# median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)
#
# fig, ax = plt.subplots(figsize=(5,5))
# # ax.hist(v, bins=50)
# n, bins, patches = ax.hist(s, n_bins, density=True, alpha=.3, edgecolor='black')
# x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
# pdf = norm.pdf(x, mu, sigma)
# ax.plot(x, pdf, color='orange', alpha=.6, label=f'Gaussian Distribution (σ = {sigma:.2f})')
# ax.set_ylabel('Probability density')
# ax.set_xlabel('Apertures (Measured - Predicted) [cm]')
# ax.legend()
# plt.show()

#%%
s = (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2
n_bins = 50
mu = s.mean()
sigma=np.std(s)
median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)

fig, (ax,ax2) = plt.subplots(1,2, figsize=(12,5), sharey=True, gridspec_kw={'width_ratios': [3, 1]})
for key in midpoints.keys():
    # plt.plot((apertures[key] - pl(amplitudes[key]))/wavelength, '.', label=f'error_{key}')
    # plt.scatter(apertures[key]*1e2, (apertures[key] - pl(amplitudes[key]))*1e2, s=10,label=f'error_{key}')
    ax.scatter(apertures_fresnel[key][:,-1]*1e2, (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2, s=10,label=f'{key}')
# ax.axis('equal')
ax.set_ylabel('Apertures (Measured - Predicted) [cm]')
ax.set_xlabel(f'Measured Apertures [cm]')
ax.legend()
ax.grid()
ax.set_aspect('equal', adjustable='box')


n, bins, patches = ax2.hist(s, n_bins, density=True, alpha=.3, edgecolor='black', orientation='horizontal' )
# x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
pdf = scipy.stats.norm.pdf(x, mu, sigma)
ax2.plot(pdf, x, color='red', alpha=.6, label=f'Gaussian Distribution \n(σ = {sigma:.2f} cm)')
ax2.set_xlabel('Probability density')
ax2.legend()
# ax2.set_ylabel('Apertures (Measured - Predicted) [cm]')
fig.tight_layout()
fig.show()

#%%
# midpoints[key][mask]o#%%
line = 17
axis=0
# fig, (ax0, ax1,ax2) = plt.subplots(1,3, figsize=(15,5))
fig, AX = plt.subplots(1,3, figsize=(20,5), sharey=True)
for key, ax in zip(midpoints.keys(), AX):
    if axis==0:
        axis2=1
    else:
        axis2=0


    # df_frac = frac.get_fracture(name_fracture=f'fracture_{key[0]}')
    m = midpoints_l #df_frac[['x','y']].to_numpy()
    ape = apertures_l*1e-2 + int(key[0])*1e-3 #df_frac['aperture'].to_numpy()
    mask = ((m[:, 0] >= out[0] * 1e-2) & (m[:, 0] <= out[1] * 1e-2) &
            (m[:, 1] >= out[0] * 1e-2) & (m[:, 1] <= out[1] * 1e-2))

    m = m[mask]
    ape = ape[mask]

    v = np.unique(midpoints[key][:, axis])[line]
    mask2 = midpoints[key][:, axis] == v
    m2 = midpoints[key][mask2,axis2]

    # Get the closes value
    v2 = m[np.argmin(np.abs(m[:,axis] - v)), axis]
    mask3 = m[:, axis] == v2
    s = np.argsort(m2)
    # mask &= midpoints[key][:,1] ==0.155
    ax.plot(m[mask3,axis2], ape[mask3], color='k', marker='*', label='Real')
    ax.plot(m2[s], pl(amplitudes[key])[mask2][s], color='r', marker='*', label='Calculated')
    ax.set_xlabel('x (m)' if axis==0 else 'y (m)')
    ax.set_title(f'Apertures_{key}')
    ax.legend()
    ax.grid()
AX[0].set_ylabel('Aperture (m)')
plt.show()

#### Create variogram
#%%
import gstools as gs
x = midpoints_l[:,0]
y = midpoints_l[:,1]
field = apertures_l / apertures_l.max()
# bins = 100
bin_center, gamma = gs.vario_estimate((x, y), field)

#%%
keys='2mm'
x = midpoints[keys][:,0]
y = midpoints[keys][:,1]
field = apertures[keys] / apertures[keys] .max()
bins = 40
bin_center, gamma = gs.vario_estimate((x, y), field)

#%%
models = {
    "Gaussian": gs.Gaussian,
    "Exponential": gs.Exponential,
    "Matern": gs.Matern,
    "Stable": gs.Stable,
    "Rational": gs.Rational,
    "Circular": gs.Circular,
    "Spherical": gs.Spherical,
    "SuperSpherical": gs.SuperSpherical,
    "JBessel": gs.JBessel,
}
scores = {}

#%%
# plot the estimated variogram
fig, ax = plt.subplots()
ax.scatter(bin_center, gamma, color="k", label="data")
# ax = plt.gca()

# fit all models to the estimated variogram
for model in models:
    fit_model = models[model](dim=2)
    para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)
    # x_s = np.linspace(0, 3 * fit_model.len_scale)
    x_s = np.linspace(0, 2 * max(bin_center))
    ax.plot(x_s, fit_model.variogram(x_s),label=f"{fit_model.name} variogram: {fit_model.len_scale:.2e}")
    # fit_model.plot(ax=ax, x_max=max(bin_center)*2)
    scores[model] = r2
ax.legend()
fig.show()
#%%
ranking = sorted(scores.items(), key=lambda item: item[1], reverse=True)
print("RANKING by Pseudo-r2 score")
for i, (model, score) in enumerate(ranking, 1):
    print(f"{i:>6}. {model:>15}: {score:.5}")

plt.show()
#%%
import numpy as np

# generate a synthetic field with an exponential model
x = np.random.RandomState(19970221).rand(1000) * 100.
y = np.random.RandomState(20011012).rand(1000) * 100.
model = gs.Exponential(dim=2, var=2, len_scale=8)
srf = gs.SRF(model, mean=0, seed=19970221)
field = srf((x, y))
# estimate the variogram of the field
bin_center, gamma = gs.vario_estimate((x, y), field)
# fit the variogram with a stable model. (no nugget fitted)
fit_model = gs.Stable(dim=2)
fit_model.fit_variogram(bin_center, gamma, nugget=False)
# output
fig, ax = plt.subplots()
ax.scatter(bin_center, gamma)
ax = fit_model.plot(x_max=max(bin_center), ax=ax)
plt.show()
print(fit_model)
#%%
def plot_hurst(H, c, data):
    n = "H={:.4f}, c={:.4f}".format(H,c)
    f, ax = plt.subplots()
    ax.plot(data[0], c * data[0] ** H, color="deepskyblue")
    ax.scatter(data[0], data[1], color="purple")
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('Time interval')
    ax.set_ylabel('R/S ratio')
    ax.set_title(n)
    ax.grid(True)
    f.show()
    print(n)
#%%
# line = 10
# axis=1
# skip = 1
# amplitude = 0.001
# fig, (ax0, ax1,ax2) = plt.subplots(1,3, figsize=(15,5))
HURST1 = []
HURST2 = []
y1_all = []
y2_all = []
# fig, AX = plt.subplots(1,3, figsize=(15,5), sharey=True)
plot_2d = []
for axis in [0,1]:
    for i, v in enumerate(np.unique(midpoints[key][:, axis])):
        for key, ax in zip(midpoints.keys(), AX):
            if axis==0:
                axis2=1
            else:
                axis2=0
            # df_frac = frac.get_fracture(name_fracture=f'fracture_{key[0]}')
            m = midpoints_l.copy() #df_frac[['x','y']].to_numpy()
            ape = apertures_l.copy()*1e-2 + int(key[0])*1e-3 #df_frac['aperture'].to_numpy()
            mask = ((m[:, 0] >= out[0] * 1e-2) & (m[:, 0] <= out[1] * 1e-2) &
                    (m[:, 1] >= out[0] * 1e-2) & (m[:, 1] <= out[1] * 1e-2))

            m = m[mask]
            ape = ape[mask]

            # v =
            mask2 = midpoints[key][:, axis] == v
            m2 = midpoints[key][mask2,axis2]

            # Get the closes value
            v2 = m[np.argmin(np.abs(m[:,axis] - v)), axis]

            mask3 = m[:, axis] == v2
            s = np.argsort(m2)
            # mask &= midpoints[key][:,1] ==0.155
            # print(ape[mask3])
            # ax.plot(m[mask3,axis2], ape[mask3]+ia, color='k', label='Real')
            # ax.plot(m2[s], pl(amplitudes[key])[mask2][s]+ia, color='k', label='Calculated')
            # ax.set_xlabel('x (m)' if axis==0 else 'y (m)')
            # ax.set_title(f'Apertures_{key}')

            x1 = m[mask3, axis2]
            s1 = np.argsort(x1)
            x1 = x1[s1]
            y1 = ape[mask3][s1]
            x2 = m2[s]
            y2 = pl(amplitudes[key])[mask2][s]

            # Here
            x1_new = np.linspace(x1[0], x1[-1], 1000)
            y1_new = np.interp(x1_new, x1, y1)
            # plot_2d.append(y1_new)
            # y1_new = CubicSpline(x1, y1)(x1_new)

            x2_new = np.linspace(x2[0], x2[-1], 1000)
            y2_new = np.interp(x2_new, x2, y2)
            # y2_new = CubicSpline(x2, y2)(x2_new)
            kind = 'random_walk' #random_walk, change, price
            # H1, c1, data1 = compute_Hc(y1_new, kind=kind, simplified=False)
            y1_all.append(y1_new)
            y2_all.append(y2_new)
            H1 = hurst_exponent_candela(y1_new, show=False)
            # H2, c2, data2 = compute_Hc(y2_new, kind=kind, simplified=False)
            H2 = hurst_exponent_candela(y2_new, show=False)

            HURST1.append(H1)
            HURST2.append(H2)
        # plot_hurst(H1, c1, data1)
        # plot_hurst(H2, c2, data2)
        # ax.legend()
# AX[0].set_ylabel('Aperture (m)')
# plt.show()
y1_all = np.asarray(y1_all)
y2_all = np.asarray(y2_all)
#%%
H1 = hurst_exponent_candela(y1_all, show=True)
H2 = hurst_exponent_candela(y2_all, show=True)
#%%
# par2 = np.polyfit(HURST1, HURST2, 1)# full=True)
# pl2 = np.poly1d(par2)
fig, ax = plt.subplots()
ax.plot(HURST1, HURST2, '.')
v = np.linspace(0,1,100)
# ax.plot(v, pl2(v))
# ax.set_xlim(0.7, 1.2)
# ax.set_ylim(0.7, 1.2)
ax.set_xlabel('Real Hurst')
ax.set_ylabel('Data Hurst')
ax.set_aspect('equal')
ax.grid()
plt.show()


#%%
from scipy.interpolate import CubicSpline
line = 17
axis=1
# fig, (ax0, ax1,ax2) = plt.subplots(1,3, figsize=(15,5))
fig, AX = plt.subplots(1,3, figsize=(15,5), sharey=True)
for key, ax in zip(midpoints.keys(), AX):
    if axis==0:
        axis2=1
    else:
        axis2=0
    df_frac = frac.get_fracture(name_fracture=f'fracture_{key[0]}')
    m = df_frac[['x','y']].to_numpy()
    ape = df_frac['aperture'].to_numpy()
    mask = ((m[:, 0] >= out[0] * 1e-2) & (m[:, 0] <= out[1] * 1e-2) &
            (m[:, 1] >= out[0] * 1e-2) & (m[:, 1] <= out[1] * 1e-2))

    m = m[mask]
    ape = ape[mask]

    v = np.unique(midpoints[key][:, axis])[line]
    mask2 = midpoints[key][:, axis] == v
    m2 = midpoints[key][mask2,axis2]

    # Get the closes value
    v2 = m[np.argmin(np.abs(m[:,axis] - v)), axis]
    mask3 = m[:, axis] == v2
    s = np.argsort(m2)
    # mask &= midpoints[key][:,1] ==0.155

    # Interpolate values
    x1 = m[mask3,axis2]
    s1 = np.argsort(x1)
    x1=x1[s1]
    y1 =  ape[mask3][s1]
    x2 = m2[s]
    y2 = pl(amplitudes[key])[mask2][s]

    # Here
    x1_new = np.linspace(x1[0], x1[-1], 1000)
    y1_new = np.interp(x1_new, x1, y1)
    # y1_new = CubicSpline(x1, y1)(x1_new)

    x2_new = np.linspace(x2[0], x2[-1], 1000)
    y2_new = np.interp(x2_new, x2, y2)
    # y2_new = CubicSpline(x2, y2)(x2_new)

    ax.plot(x1,y1, color='k', marker='*', linestyle='None')
    ax.plot(x1_new, y1_new, color='k', label='Real')
    ax.plot(x2, y2, color='r', marker='*', linestyle='None')
    ax.plot(x2_new, y2_new, color='r', label='Calculated')
    ax.set_xlabel('x (m)' if axis==0 else 'y (m)')
    ax.set_title(f'Apertures_{key}')
    ax.legend()

    H1, c1, data1 = compute_Hc(y1_new, kind='random_walk', simplified=False)
    H2, c2, data2 = compute_Hc(y2_new, kind='random_walk', simplified=False)

    plot_hurst(H1, c1, data1)
    plot_hurst(H2, c2, data2)
AX[0].set_ylabel('Aperture (m)')
plt.show()


#%%
import numpy as np


def hurst_exponent(data):
    """
    This function calculates the Hurst exponent of a time series data using the Rescaled Range (R/S) analysis.

    Args:
        data: A NumPy array containing the time series data.

    Returns:
        The Hurst exponent (H) as a float.
    """

    def rescaled_range(data, tau):
        """
        This function calculates the Rescaled Range (R/S) for a given time window (tau).

        Args:
            data: A NumPy array containing the time series data.
            tau: The time window size (positive integer).

        Returns:
            The Rescaled Range (R/S) value for the given tau.
        """
        if tau < 1:
            raise ValueError("tau must be a positive integer")

        # Calculate the difference series
        diff = np.diff(data)

        # Calculate the size of the subseries
        n_sub = len(data) - tau + 1

        # Initialize variables
        sums = np.zeros(n_sub)
        range_ = np.zeros(n_sub)

        # Calculate the sum of differences for each subseries
        for i in range(n_sub):
            sums[i] = np.sum(diff[i:i + tau])

        # Calculate the range for each subseries
        range_ = np.max(sums) - np.min(sums)

        # Calculate the standard deviation of the original data
        std = np.std(data)

        # Calculate the Rescaled Range (R/S)
        return range_ / (std * np.sqrt(tau))

    # Check for valid data type
    if not isinstance(data, np.ndarray):
        raise TypeError("data must be a NumPy array")

    # Minimum data length for analysis
    min_length = 2
    if len(data) < min_length:
        raise ValueError(f"Data length ({len(data)}) must be at least {min_length}")

    # Define time window sizes (adjust as needed)
    taus = np.arange(2, len(data) // 2 + 1)

    # Calculate R/S values for each time window
    rs_list = [rescaled_range(data, tau) for tau in taus]

    # Calculate the slope of the log-log plot of R/S vs tau
    # (using linear regression or other fitting methods)
    # Here, we'll use NumPy's polyfit for demonstration
    log_taus = np.log10(taus)
    log_rs = np.log10(rs_list)
    m, _ = np.polyfit(log_taus, log_rs, 1)  # Fit a line, discard intercept

    # The Hurst exponent (H) is half the slope
    hurst_exponent = m / 2

    return hurst_exponent


# Example usage
data = np.random.randn(100)  # Sample data (replace with your actual data)

try:
    hurst_value = hurst_exponent(data)
    print(f"Hurst Exponent (H): {hurst_value:.4f}")
except (ValueError, TypeError) as e:
    print(f"Error: {e}")

#%%
lags = range(2,100)
def hurst_ernie_chan(p):

    variancetau = []; tau = []

    for lag in lags:

        #  Write the different lags into a vector to compute a set of tau or lags
        tau.append(lag)

        # Compute the log returns on all days, then compute the variance on the difference in log returns
        # call this pp or the price difference
        pp = np.subtract(p[lag:], p[:-lag])
        variancetau.append(np.var(pp))

    # we now have a set of tau or lags and a corresponding set of variances.
    #print tau
    #print variancetau

    # plot the log of those variance against the log of tau and get the slope
    m = np.polyfit(np.log10(tau),np.log10(variancetau),1)

    hurst = m[0] / 2

    return hurst

#%%
H1, c1, data1 = compute_Hc(y1_new, kind='random_walk', simplified=False)
H5, c1, data1 = compute_Hc(y1_new, kind='random_walk', simplified=False)
H3 = hurst_exponent(data[0, 1, :])
H4 = hurst_ernie_chan(data[0, 1, :])
H4 = hurst_exponent_candela(y1_new)
H4 = hurst_exponent_candela(plot_2d)
H4 = hurst_exponent_candela(apertures_original, show=False)

print(H1, H3, H4)

#%%
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq, rfft, rfftfreq
from scipy.signal.windows import hann
from scipy.io import loadmat

#%%
def hurst_exponent_candela(data, show=True):
    # Power spectral density (PSD) calculation loop
    if not isinstance(data, np.ndarray): data = np.asarray(data)
    if data.ndim ==1: data = data[None,:]
    psds = []
    fig_psd, ax_psd = plt.subplots(figsize=(8, 6)) if show else (None, None)
    # data = data.copy() - np.mean(data)  # So is centered around 0
    # for ii in range(0, current_data.shape[1], 5):  # Adjust step size as needed
    for data_col in tqdm(data):  # Adjust step size as needed
        # Extract data column
        # data_col = current_data[:, ii]

        # Apply Hann window
        window = hann(len(data_col))
        windowed_data = data_col * window

        # Calculate positive FFT and frequencies
        positive_fft = rfft(windowed_data) / len(windowed_data)  # Normalize
        freqs = rfftfreq(len(data_col), d=1)

        # Calculate power spectral density (PSD)
        psd = np.abs(positive_fft) ** 2

        # Store PSD for averaging and plotting
        psds.append(psd)
        ax_psd.loglog(freqs, psd) if show else None # , label=f"X={ii}")

        # Plot average PSD
    if show:
        ax_psd.loglog(freqs, np.mean(psds, axis=0), color='k', label='Average PSD')
        ax_psd.set_xlabel('Wavenumber, k')
        ax_psd.set_ylabel('Power spectral density, P(k)')
        # ax_psd.set_title(f"Power Spectral Density")
        ax_psd.legend()
        fig_psd.tight_layout()
        fig_psd.show()

    # Calculate Hurst exponent using linear regression on log-log PSD
    log_freqs = np.log10(freqs[1:-1])  # Avoid log(0)
    log_psds = np.log10(np.mean(psds, axis=0)[1:-1])
    slope = np.polyfit(log_freqs, log_psds, 1)
    hurst_exponent = (slope[0] + 1) / -2
    print(f"Hurst exponent: {hurst_exponent:.4f}")
    return hurst_exponent

#%%
# Load data (assuming 'Results_RVE_sythetic.mat' is a valid MAT file)
# data = np.loadmat('Results_RVE_sythetic.mat')['P_new']  # Assuming variable name
data = loadmat('/scripts/fracture_aperture/generate_synthetic/Results_RVE_sythetic.mat')  # Assuming variable name
P_new = data['P_new']  # Assuming variable name
H = data['H'][0]  # Assuming variable name

# Define colormap (replace 'viridis' with your preference)
cmap = plt.cm.viridis

# Loop through data slices
for jj in [-1]:  # Adjust loop range as needed
    # Extract current data slice
    current_data = P_new[jj, :, :]
    # current_data = apertures_original

    # Figure for surface plot
    # fig_surf, ax_surf = plt.subplots(2, 3, figsize=(10, 5), gridspec_kw={'width_ratios': [4, 4, 1], 'height_ratios': [2, 1]})
    fig_surf, ax_surf = plt.subplots(figsize=(10, 5))
    # fig_surf.suptitle(f"Hin = {H[jj]}", fontsize=12)

    # Surface plot
    cax = ax_surf.imshow(current_data, cmap=cmap, interpolation='None', aspect='equal')
    # ax_surf.set_aspect('auto')
    # ax_surf.view_init(elev=0, azim=-90)
    ax_surf.set_title(f'Surface Plot. Hin = {H[jj]}')
    ax_surf.set_xlabel('X')
    ax_surf.set_ylabel('Y')
    fig_surf.colorbar(cax, ax=ax_surf)
    fig_surf.show()
    fig_surf.show()
    # Line plot at specific row (adjust row index as needed)
    row_index = 500
    fig_surf, ax_line = plt.subplots(figsize=(10, 5))
    # ax_line = fig_surf.add_subplot(2, 3, 5)
    ax_line.plot(current_data[row_index, :], color=cmap(jj / len(H)), label=f"Slice {jj}")
    ax_line.set_xlabel('X')
    ax_line.set_ylabel('h')
    ax_line.set_xlim(0, current_data.shape[1])
    ax_line.legend()
    fig_surf.show()
    hurst_exponent_candela(current_data)
  # # Power spectral density (PSD) calculation loop
  # psds = []
  # fig_psd, ax_psd = plt.subplots(figsize=(8, 6))
  # for ii in range(0, current_data.shape[1], 5):  # Adjust step size as needed
  #       # Extract data column
  #       data_col = current_data[:, ii]
  #
  #       # Apply Hann window
  #       window = hann(len(data_col))
  #       windowed_data = data_col * window
  #
  #       # Calculate positive FFT and frequencies
  #       fft_data = fft(windowed_data) / len(windowed_data)  # Normalize
  #       positive_fft = fft_data[:len(data_col) // 2 + 1]
  #       freqs = fftfreq(len(data_col), d=1)[:len(data_col) // 2 + 1]
  #
  #       # Calculate power spectral density (PSD)
  #       psd = np.abs(positive_fft) ** 2
  #
  #       # Store PSD for averaging and plotting
  #       psds.append(psd)
  #       ax_psd.loglog(freqs, psd)#, label=f"X={ii}")
  #
  #   # Plot average PSD
  #   ax_psd.loglog(freqs, np.mean(psds, axis=0), color='k', label='Average PSD')
  #   ax_psd.set_xlabel('Wavenumber, k')
  #   ax_psd.set_ylabel('Power spectral density, P(k)')
  #   ax_psd.set_title(f"Power Spectral Density (Slice {jj})")
  #   ax_psd.legend()
  #   plt.tight_layout()
  #   fig_psd.show()
  #
  #
  #   # Calculate Hurst exponent using linear regression on log-log PSD
  #   log_freqs = np.log10(freqs[1:])  # Avoid log(0)
  #   log_psds = np.log10(np.mean(psds, axis=0)[1:])
  #   slope = np.polyfit(log_freqs, log_psds, 1)
  #   hurst_exponent = (slope[0] + 1) / -2
  #   print(f"Hurst exponent (slice {jj}): {hurst_exponent:.4f}")
  #   return hurst_exponent

#%%

#%%
h7=hurst_exponent_candela(current_data[:,10])