import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
from hurst import compute_Hc
import scipy.stats

p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd

from gdp.processing.gain import apply_gain
from gdp.processing.time_lapse import hilbert_phase_difference

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM

file_h5 = OUTPUT_DIR + 'fracture_aperture/gypsum_block_heter_apertures.h5'

#%% Source
sou = SourceEM()
try:
    sou.load_hdf5(file_h5)
except:
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 655))

    sou.set_delay(delay=1)
    # sou.set_source_params(a=9.17809204,
    #                       c=0.95575954,
    #                       loc=0,
    #                       scale=0.28381535)
    sou.set_source_params(a=14.99999994,
                          c=0.68261749,
                          loc=0,
                          scale=0.05180656)
    sou.create_source()
    # sou.widgets().show()

    # fig = sou.plot_waveforms_zoom()
    fig = sou.plot_waveforms_complete()
    fig.show()
    sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5

#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5.split('.')[0] + '_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%% Fracture
frac = FractureGeo()
resolution = (45,45)

def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2
extent_original=(midpoints_l[:,0].min(), midpoints_l[:,0].max(), midpoints_l[:,1].min(), midpoints_l[:,1].max())

apertures_original = np.asarray(rotate_matrix_clockwise( apertures_l.reshape(spacing), rotations=0)) *1e-2

try:
    frac.load_hdf5(file_h5)
    surf = frac.get_surface()
    apertures = frac.fractures['aperture'].to_numpy()
except:

    width = 0.30
    length = 0.30
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0,
                                                       resolution=resolution, center=(.17,.17,0))
    # surf, vertices, faces = frac.remesh(points=vertices, max_element_size=max_element_size, plane=2)

    surf = frac.create_pyvista_mesh(vertices, faces)

    p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)

    # outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
    # d = np.load(outname + 'files.npz')
    # top_surface_m = d['top_surface_m']
    # bottom_surface_m = d['bottom_surface_m']
    # apertures_l = d['apertures']
    # spacing = [600, 600]
    # midpoints_l = (d['midpoints']+np.asarray([17,17,0]))*1e-2

    apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing),
                                                 rotations=1)

    scaling_factors = (np.array(resolution) / spacing).tolist()
    from scipy.ndimage import zoom
    # Perform the downsampling using zoom
    downsampled_image = zoom(apertures_original, scaling_factors)
    scaling_factors_up = (spacing / np.array(resolution)).tolist()
    upsampled_image = zoom(downsampled_image, scaling_factors_up)

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
    cax1 = ax1.imshow(apertures_original, cmap='viridis')
    cax2 = ax2.imshow(downsampled_image, cmap='viridis')
    cax3 = ax3.imshow(upsampled_image, cmap='viridis')
    cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.colorbar(cax3, ax=ax3)
    fig.colorbar(cax4, ax=ax4)
    ax1.set_title('Original')
    ax2.set_title('Downsampled')
    ax3.set_title('Upsampled')
    ax4.set_title('Original-Upsampled')
    fig.show()
# apertures = apertures.reshape(spacing)
    for i in [0, 2e-3,3e-3]:

        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2 + i
        kwarg_properties = dict(aperture=apertures, #0.01,
    #     kwarg_properties = dict(aperture=0.1,
                                    electrical_conductivity=0,
                                    electrical_permeability=1,
                                plane=2,
                                overwrite=True)
        frac.set_fracture(name_fracture=f'fracture_{int(i*1000)}',
                          vertices=vertices,
                          faces=faces,
                          **kwarg_properties)

        frac.export_to_hdf5(file_h5, overwrite=True)
p.add_object(frac.get_midpoints(name_fractures='fracture_0'), name='fracture', scalars=frac.df.loc[frac.df['name']=='fracture_0','aperture'].to_numpy())

p.add_object(midpoints_l, name='fracture', scalars =apertures_original.ravel()*100, cmap='viridis', label='Aperture (m)')
#%%
# p.add_object(ant2.Receiver, name='rx', color='blue', opacity=0.2)
view = 'D'
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Rx','Ry','Rz']].to_numpy(), name='rx', color='blue', opacity=0.2)
# p.add_object(ant2.Transmitter, name='tx', color='red', opacity=0.2)
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Tx','Ty','Tz']].to_numpy(), name='tx', color='red', opacity=0.2)
#%%
p.remove('rx')
p.remove('tx')
p.add_object(ant.get_midpoints(), name='mid', color='black', opacity=0.2)

#%%
import pyvista as pv
meshD = pv.Cube(center=(0.17,0.17,-0.05), x_length=0.34,y_length=0.34, z_length=0.1)
meshU = pv.Cube(center=(0.17,0.17,0.05), x_length=0.34,y_length=0.34, z_length=0.1)
p.add_object(meshD, name='lower_cube', style='wireframe')
p.add_object(meshU, name='upper_cube', style='wireframe')

#%%
paths_all_apertures = {'0mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/0mm/SEGY_20240412_172224/',
                        '2mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/2mm/SEGY_20240412_172341/',
                        '3mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/3mm/SEGY_20240412_172435/',
                        'alum':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135'}
data_measu = {}
data_together = {}

# When pol=0 then we look at antenna orientation (1,0,0). ['B', 'C']
# When pol = 1 then antenna is (0,1,0). ['A, 'D']
pol = 1 # polarization of the antennas.
th = '2mm'
vmax = 0
# out = [12, 22]
# out = [9, 26]
out = [6, 29]
for key, base_path in paths_all_apertures.items():



    # Get all the files in the folder
    folders = os.listdir(base_path)
    # loop through all the folders and get the files. From the filename extract the information.
    # The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

    # Create a dictionary to store the information
    data_measu[key] = {'A':{},  # Up direction (0,1,0)
                       'B':{},  # Right dir (1,0,0)
                       'C':{},  # Up dir (1,0,0)
                       'D':{}}  # Right dir (0,1,0)

    vmax = 0

    for folder in folders:
        # Get the files
        files = os.listdir(os.path.join(base_path, folder))
        for file in files:
            if '.sgy' not in file:
                continue
            info = re.split('_', file)
            # identif = info[0]
            # Get the date
            # date = info[1]
            # Get the ID
            ID = info[0]
            # Get the path
            # experiment_number = int(info[2])
            experiment_number = int(info[1])
            n_e_id = ID + '_' + str(experiment_number)
            midpoints = ant.get_midpoints(n_e_id)

            if ID in ['B', 'D']:
                lims_x = (10,85)
                # ma = ((midpoints[:, 0] >= out[0]*1e-2) & (midpoints[:, 0] <= out[1]*1e-2) &
                #       (midpoints[:, 1] >= out[0]*1e-2) & (midpoints[:, 1] <= out[1]*1e-2))
            elif ID in ['A', 'C']:
                lims_x = (10, 82)
            else:
                raise AttributeError
            ma = ((midpoints[:, 0] >= out[0] * 1e-2) & (midpoints[:, 0] <= out[1] * 1e-2) &
                 (midpoints[:, 1] >= out[0] * 1e-2) & (midpoints[:, 1] <= out[1] * 1e-2))
            if True in ma:
                print(n_e_id)
            else:
                print('no_' + n_e_id)
                continue

            # if experiment_number < out[0] or experiment_number > out[1]:
            #     print('no_'+n_e_id)
            #     continue
            # else:
            #     print(n_e_id)
            path = os.path.join(base_path, folder, file)
            # Get the data
            data_cube, header = load_sgy(path)

            # if ID =/= 'B':  # It is flipped
                # data_cube = np.fliplr(data_cube)
            # Store the data in the dictionary
            # d = {experiment_number: {'header':header, 'data': data_cube}}
            # Processing
            d_de = detrend(data_cube)
            sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
            c0 = 299_792_458  # Speed of light in m/s
            rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
            velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
            # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
            # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
            d_de_ga = d_de.copy()
            data_uncut = d_de_ga.copy()
            mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
            if len(ma) > len(mask_x):
                mask_x = ma[:len(mask_x)]
                midpoints = midpoints[:len(mask_x)]
            else:
                mask_x[:len(ma)] = ma
            # lims_y = (80, 100)
            lims_y = (70, 110)
            d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
            d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
            d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from left
            d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from right
            # d_de_ga[:, mask_x] = 0 # crop all reflections from right
            time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
            depth_vector = time_vector * velocity * 0.5
            vma = np.abs(d_de_ga).max()
            if vma > vmax: vmax=vma
            d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
                 'time_vector':time_vector, 'depth_vector':depth_vector,
                 'lims_x': lims_x, 'lims_y': lims_y,
                 'data_cut': data_uncut[:, mask_x],
                 'data_cut_all': data_uncut[lims_y[0]:lims_y[1], mask_x],
                 'mask_x': mask_x,
                 'midpoints':midpoints,
                 }
            data_measu[key][ID][experiment_number] = d
            if np.abs(np.max(d_de_ga)) > vmax:
                vmax=np.abs(np.max(d_de_ga))

#%%

# # Data from which the positions are available
# available = []
# for key, v in data_measu['alum'].items():
#     if len(v) == 0:
#         # is empty:
#         continue
#     n = [key+'_'+str(num) for num, _ in v.items()]
#     available.append(n)

#%%

# from impdar.lib import load
# dat = load.load('mat','data/synthetic_radargram.mat')[0]
#%%
inf = data_measu['0mm']['A'][15]
# inf = data_measu['0mm']['C'][10]

# inf = data_together['2mm']['AC'][12]
# lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
# d = inf['data']
# d = inf['data_uncut']
# d = inf['data_cut']
d = inf['data_cut_all']
# from scipy.io import savemat
# savemat(OUTPUT_DIR+'AC_0mm_10', inf)
# vmax=d.max() * 0.05
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  inf['time_vector'] #* 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
vmax =  np.max(np.abs(d))
plt.figure()
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           extent=extent
           )

# plt.ylabel('Distance (cm)')
plt.ylabel('Two way travel time (ns)')
# plt.ylim(2.7,1.5)
# plt.ylim(4,1)
# plt.xlim(20,80)
plt.colorbar()
plt.show()

#%%
t = 15
l = 'D'  # Only A and D

plt.figure()
alum = data_measu['alum'][l][t]['data_cut_all'][:, t].copy()
norm = np.abs(alum).max()
alum/= norm
depth = data_measu['alum'][l][t]['depth_vector'].copy()[lims_y[0]:lims_y[1]]
plt.plot(depth, data_measu['0mm'][l][t]['data_cut_all'].copy()[:, t]/norm, label='0mm')
plt.plot(depth,  data_measu['2mm'][l][t]['data_cut_all'][:, t].copy()/norm, label='2mm')
plt.plot(depth, data_measu['3mm'][l][t]['data_cut_all'][:, t].copy()/norm, label='3mm')
plt.plot(depth, alum, label='Alum')
# plt.xlim(70,110)
# plt.xlim(0.1,0.15)
plt.legend()
plt.show()

#%% lets get the aperture based on the position of the antennas
def find_nearest(x, y, positions):
  """
  Finds the nearest position (index) in a list of positions to a given point.

  Args:
      x: X-coordinate of the target point.
      y: Y-coordinate of the target point.
      positions: List of tuples containing (x, y) coordinates.

  Returns:
      The index of the nearest position in the positions list.
  """
  distance = ((positions[:, 0] - x) ** 2 + (positions[:, 1] - y) ** 2) ** 0.5
  nearest_idx = np.argmin(distance)
  return nearest_idx

#%%
midpoints = ant.get_midpoints()[:, :2]

# sort x
mask_x = (out[0]*1e-2 < midpoints[:,0]) & (midpoints[:,0] < out[1]*1e-2)
mask_y = (out[0]*1e-2 < midpoints[:,1]) & (midpoints[:,1] < out[1]*1e-2)
mask_idx = mask_x & mask_y

position_df = ant.df[mask_idx]
mid_ant_pos = (position_df[['Rx','Ry','Rz']].to_numpy() + position_df[['Tx','Ty','Tz']].to_numpy()) * 0.5
# p.remove('rx')
# p.remove('tx')
p.add_object(mid_ant_pos,
             name='mid_d', color='red', opacity=0.3)

#%%
nearest_idx_pos = np.asarray([find_nearest(po[0],po[1], positions=frac.get_midpoints(name_fractures='fracture_0')[:,:2]) for po in mid_ant_pos[:,:2]])
aperture_from_near = frac.df['aperture'][nearest_idx_pos].to_numpy()
#%%
p.add_object(mid_ant_pos,
             name='mid_d', scalars=aperture_from_near)

#%% Calculate reflection coefficients

midpoints = {}
apertures = {}
amplitudes = {}
for key, a in data_measu.items():
    if key == 'alum':
        continue
    midpoints[key] = []
    apertures[key] = []
    amplitudes[key] = []
    for key2, b in a.items():
        if key2 in ['B', 'C']:
            continue
        for key3, c in b.items():
            data_al = data_measu['alum'][key2][key3]['data_cut_all']
            # norm = np.abs(data_al).max(axis=0)
            norm = np.linalg.norm(data_al,axis=0, ord=2)
            data = c['data_cut_all'] / norm  # Normalize to maximum of aluminium
            lims_x = c['lims_x']
            lims_y = c['lims_y']
            depth_vector = c['depth_vector']
            mask_x = c['mask_x'] #[:101]  # I have only 100 points always
            midp = c['midpoints']
            if len(mask_x) > len(midp):
                mask_x = mask_x[:len(midp)]
            midp = midp[mask_x]

            df_ap = frac.get_fracture(name_fracture=f'fracture_{key[0]}')

            nearest_idx_pos = np.asarray(
                [find_nearest(po[0], po[1], positions=df_ap[['x','y']].to_numpy()) for po in midp[:,:2]])
            aperture_from_near = df_ap['aperture'].to_numpy()[nearest_idx_pos]
            sigma_from_near = df_ap['elec_conductivity'].to_numpy()[nearest_idx_pos]
            epsilon_from_near = df_ap['elec_permeability'].to_numpy()[nearest_idx_pos]

            midpoints[key].append(midp)
            apertures[key].append(aperture_from_near)
            # amplitudes[key].append(np.abs(data).max(axis=0))
            amplitudes[key].append(np.linalg.norm(data,axis=0))

    midpoints[key] = np.vstack(midpoints[key])
    apertures[key] = np.hstack(apertures[key])
    amplitudes[key] = np.hstack(amplitudes[key])
# p.add_object(midpoints, scalars=apertures, name='new')

#%%

for key in  midpoints.keys():
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10,5))
    cax0= ax0.scatter(midpoints[key][:,0], midpoints[key][:,1], c=apertures[key], cmap='viridis', s=10, vmin=0, vmax=apertures['3mm'].max())
    cax1 = ax1.scatter(midpoints[key][:,0], midpoints[key][:,1], c=amplitudes[key], cmap='viridis', s=10, vmin=0, vmax=amplitudes['3mm'].max())
    ax0.set_title(f'Apertures_{key}')
    ax1.set_title(f'Amplitudes_{key}')
    fig.colorbar(cax0, ax=ax0)
    fig.colorbar(cax1, ax=ax1)
    plt.tight_layout()
    plt.show()
    # break

#%%
solv = FracEM()
solv.open_file(file_h5)
rock_epsilon =6.5
solv.rock_epsilon =rock_epsilon

rock_sigma = 0.0
solv.rock_sigma = rock_sigma

epsilon = frac.df['elec_permeability'].unique()[0]
sigma = frac.df['elec_conductivity'].unique()[0]

solv.engine = 'tensor_trace'

from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_frequencies
mask_freq = get_mask_frequencies(sou.source, threshold=0.1)
omega = sou.frequency[mask_freq]* 2*np.pi *1e9
#%%
def average(data, type='arithmetic', distance=None):
    if type == 'arithmetic':
        return np.mean(data)
    elif type == 'median':
        return np.median(data)
    elif type == 'mode':
        counts = np.unique(data, return_counts=True)[1]
        # Find the maximum count
        max_count = np.max(counts)
        # Get all modes (values with max count)
        modes = data[counts == max_count]
        return modes.tolist()  # Convert to list for return
    elif type == 'geometric_mean':
        # Check for negative values or zeros
        if any(value <= 0 for value in data):
            raise ValueError("Geometric mean is not defined for non-positive values.")

        # Calculate the product and take the nth root
        product = np.prod(data)
        n = len(data)
        return np.power(product, 1 / n)
    elif type == 'harmonic_mean':
        if any(value <= 0 for value in data):
            raise ValueError("Harmonic mean is not defined for non-positive values.")

        # Calculate the sum of reciprocals and take the reciprocal
        reciprocals = 1 / np.array(data)
        return 1 / np.mean(reciprocals)
    elif type == 'distance_weight':
        weights = 1 / distance**2  # Weights based on inverse distance
        weights = weights / np.sum(weights)  # Normalize weights to sum to 1
        return np.average(data, weights=weights)
    else:
        raise AttributeError

#%%
def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5


distance = 0.12  # Top to the view
rock_epsilon = 6.5
c0 = 299_792_458
velocity = c0 * 1e-7 / np.sqrt(rock_epsilon)
frequency = 3.6 # GHz
wavelength = solv.velocity / sou.center_frequency  # Result in cm

r = fresnel(distance, wavelength, 1)

#%%
from tqdm.autonotebook import tqdm
# options are ['arithmetic', 'geometric_mean', 'harmonic_mean', 'median', 'mode', 'distance_weight]
typ = 'distance_weight'
try:
    # raise FileNotFoundError
    apertures_fresnel = {key:np.load(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{typ}_{key}.npy') for key in midpoints.keys()}
except FileNotFoundError:

    def apertures_inside_circle(point_cloud, apertures, circle_origin, circle_radius):
        # Convert the point cloud to a NumPy array for easier calculations
        point_cloud_array = np.array(point_cloud) + 100
        circle_origin =  np.array(circle_origin) + 100

        # Calculate the distances from each point to the circle's origin
        distances = np.linalg.norm(point_cloud_array - circle_origin, axis=1)

        # Find indices of points that are inside the circle (distance < radius)
        inside_circle_indices = np.where(distances < circle_radius)[0]
        point_cloud_array -= 100
        circle_origin -= 100
        # Extract the points that are inside the circle
        points_inside_circle = point_cloud_array[inside_circle_indices]
        apertures_inside_cirlce = apertures[inside_circle_indices]
        distance = distances[inside_circle_indices]

        return points_inside_circle, apertures_inside_cirlce, distance
    apertures_fresnel ={}
    for key in midpoints.keys():
        mid = midpoints[key]
        apertures_fresnel2 = []
        position_fresnel = []
        dist_frenel = []
        mask = frac.df['name'] == f'fracture_{key[0]}'
        for m in tqdm(mid):#middle_points):
            points, apert, dista = apertures_inside_circle(frac.get_midpoints()[mask,:2],
                                                    frac.df['aperture'][mask].to_numpy(), m[:2], r)
            if points.size == 0:
                apertures_fresnel2.append(0)
                position_fresnel.append([0])
                dist_frenel.append([0])
                continue
            # if type in ['geometric_mean', 'harmonic_mean']:
            apert = np.abs(apert)

            apertures_fresnel2.append(average(apert, type=typ, distance=dista))
            position_fresnel.append(points)
            dist_frenel.append(dista)
        # break
        apertures_fresnel[key] = np.c_[mid[:,0],mid[:,1], apertures_fresnel2]

    [np.save(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{typ}_{key}.npy', a) for key, a in apertures_fresnel.items()]


#%%
x_mid = 0.036
z = 0.10
theta = np.arctan(x_mid/(2*0.1))
# fe = np.array([1600 *1e6])# * 2 * np.pi#omega
# fe = np.array([sou.center_frequency * 1e9])
# fe = np.array([sou.center_frequency * 1e9])
# ome = omega #fe  * 2 * np.pi
ome = [omega[0], sou.center_frequency * 1e9 * 2 * np.pi, omega[-1]]
re = solv.rock_epsilon  # 9.5
rs = solv.rock_sigma  # 0.002
fe = frac.df['elec_permeability'].unique()[0] # 16
fs = frac.df['elec_conductivity'].unique()[0]  # 0.1

# v = solv.c0 / np.sqrt(re)
# o = sou.center_frequency *1e9
wavelength = solv.wavelength
# m = 0.3 * wavelength #0.02 # m
m = 2 * wavelength #0.02 # m
d = np.linspace(0, m, 1000)[None, :]
# k1 = wavenumber(ome, epsilon=re, sigma=rs, complex=False)[:, None]
# k2 = wavenumber(ome, epsilon=e, sigma=s, complex=False)[:, None]

k1c = solv.wavenumber(omega=ome, epsilon=re, sigma=rs)[:, None]
k2c = solv.wavenumber(omega = ome, epsilon=fe, sigma=fs)[:, None]

#%%
# G = solv.reflection_all(k1, k2, theta=0, aperture=d)
Gc = solv.reflection_all(k1c, k2c, theta=0, aperture=d)
Gt = solv.reflection_all(k1c, k2c, theta=theta, aperture=d)
ap = frac.df['aperture'].to_numpy()
# Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
Gd = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
# Gfresnel = solv.reflection_all(k1c, k2c, theta=0, aperture=ap)
#%%
poly_x = np.hstack([v for v in amplitudes.values()])
poly_y = np.hstack([v[:,-1] for v in apertures_fresnel.values()])
par = np.polyfit(poly_x, poly_y, deg=1)
# [par,_] = np.polyfit(poly_x, poly_y, deg=1)
# par = np.polyfit(amplitudes[key], apertures[key], 1)# full=True)
pl = np.poly1d(par)

#%%
# from sklearn.linear_model import LinearRegression
# model = LinearRegression(poly_x, poly_y)#fit_intercept=False)
# model2 = LinearRegression(poly_y, poly_x)#fit_intercept=False)
# 
# # Fit the model to the data
# model.fit(poly_x.reshape(-1, 1), poly_y)
# model.fit(poly_x.reshape(-1, 1), poly_y)
# # slope = model.coef_[0]
# def pl(x):  return model.predict(x)
# def p2l(x):  return model2.predict(x)
#%%
def fw(o):
    k1c = solv.wavenumber(omega=o, epsilon=re, sigma=rs)
    k2c = solv.wavenumber(omega=o, epsilon=fe, sigma=fs)
    rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=apertures_fresnel[key][:,-1])
    return np.sqrt(np.sum(np.square(np.abs(rc) - amplitudes[key])))

x = sou.center_frequency * 2 * np.pi

from scipy.optimize import minimize
res = minimize(fw,
                x,
                method='nelder-mead',
                options={#'maxfun': 10000,
                         'xatol': 1e-6,
                         'disp': False})
k1c = solv.wavenumber(omega=res.x, epsilon=re, sigma=rs)
k2c = solv.wavenumber(omega=res.x, epsilon=fe, sigma=fs)
rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=d[0])

# If this works then
# def pl(amplitudes):

    # return aperture
#%%
# k1c = solv.wavenumber(omega=res.x, epsilon=re, sigma=rs)
# k2c = solv.wavenumber(omega=res.x, epsilon=e, sigma=s)
# rc = solv.reflection_all(k1c, k2c, theta=theta, aperture=d[0])

#%%
from fracwave.utils.help_decorators import convert_frequency, convert_meter
# f = [convert_frequency(i/(2*np.pi)) for i in omega]
f = [convert_frequency(i/(2*np.pi)) for i in ome]
# f = convert_frequency(res.x[0])

fig, ax = plt.subplots(figsize=(10,5))
# [plt.plot(d[0]/wavelength, np.abs(l), alpha=0.1, label=i) for l, i in zip(Gc, f)]
[ax.plot(d[0]*1e2, np.abs(l), alpha=0.5, label=f'Freq: {i}') for l, i in zip(Gt, f)]
# [plt.scatter(ap/wavelength, np.abs(l), s=5)  for l in Gd]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key) for key in midpoints.keys()]
[ax.scatter(apertures_fresnel[key][:,-1]*1e2, amplitudes[key], s=5, color='k', label='Data' if key=='0mm' else None, alpha=0.6) for key in midpoints.keys()]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key+'_fresnel') for key in midpoints.keys()]
# ax.plot(pl(np.linspace(0,0.5,100))*1e2, np.linspace(0,0.5,100), label=f'Lin. Regression')

plt.plot(d[0]*1e2, np.abs(rc), label=f'fitted_{convert_frequency(res.x[0]/(2*np.pi))}')
# [plt.plot(d[0]/wavelength, np.abs(l), label=f[0]) for l in G]
# ax.axvline(apertures['3mm'].max()*1e2)
ax.set_ylabel('Reflection Coefficients (-)')
ax.set_xlabel('Aperture (cm)')
ax.legend()#bbox_to_anchor=(.95, 0.9))#, labels=f)
ax.set_title(f'Wavelength: {convert_meter(wavelength)}')
ax.set_xlim(0, 2)
ax.grid()
fig.show()
#%%
from fracwave.utils.help_decorators import convert_frequency, convert_meter
# f = [convert_frequency(i/(2*np.pi)) for i in omega]
f = [convert_frequency(i/(2*np.pi)) for i in ome]

fig, ax = plt.subplots(figsize=(10,5))
# [plt.plot(d[0]/wavelength, np.abs(l), alpha=0.1, label=i) for l, i in zip(Gc, f)]
[ax.plot(d[0]*1e2, np.abs(l), alpha=0.5, label=f'Freq: {i}') for l, i in zip(Gt, f)]
# [plt.scatter(ap/wavelength, np.abs(l), s=5)  for l in Gd]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key) for key in midpoints.keys()]
[ax.scatter(apertures_fresnel[key][:,-1]*1e2, amplitudes[key], s=5, color='k', label='Data' if key=='0mm' else None, alpha=0.6) for key in midpoints.keys()]
# [plt.scatter(apertures[key]/wavelength, amplitudes[key], s=5, label=key+'_fresnel') for key in midpoints.keys()]
# ax.plot(pl(np.linspace(0,0.5,100))*1e2, np.linspace(0,0.5,100), label=f'Lin. Regression')

plt.plot(d[0]*1e2, np.abs(rc), label=f'fitted_{convert_frequency(res.x[0]/(2*np.pi))}')
# [plt.plot(d[0]/wavelength, np.abs(l), label=f[0]) for l in G]
# ax.axvline(apertures['3mm'].max()*1e2)
ax.set_ylabel('Reflection Coefficients (-)')
ax.set_xlabel('Aperture (cm)')
ax.legend()#bbox_to_anchor=(.95, 0.9))#, labels=f)
ax.set_title(f'Wavelength: {convert_meter(wavelength)}')
# ax.set_xlim(0,1.4)
ax.grid()
fig.show()

#%%
generate_field = np.c_[midpoints[key][:, 0], midpoints[key][:,1], pl(amplitudes[key])]
vmin=0
vmax=0.014

for key in midpoints.keys():
    x, y = midpoints[key][:, 0], midpoints[key][:, 1]
    fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(15,5), sharey=True)
    # cax0=ax0.scatter(x, y, c=apertures[key], cmap='turbo', s=20, vmin=vmin, vmax=vmax)
    # cax0=ax0.scatter(midpoints_l[:,0], midpoints_l[:,1], c=apertures_l*1e-2 + int(key[0])*1e-3, cmap='turbo', vmin=vmin, vmax=vmax)
    cax0=ax0.imshow(apertures_original + int(key[0])*1e-3, cmap='turbo', extent=extent_original, vmin=vmin, vmax=vmax, aspect='auto')
    ax0.set_xlim(x.min(), x.max())
    ax0.set_ylim(y.min(), y.max())
    cax1=ax1.scatter(x, y, c=amplitudes[key], cmap='viridis', s=25)
    cax2=ax2.scatter(x, y, c=pl(amplitudes[key]), cmap='turbo', s=25, vmin=vmin, vmax=vmax)
    ax0.set_title('Apertures (m)')
    ax1.set_title('Amplitude/norm [Reflection Coefficient]')
    ax2.set_title('Generated Apertures')
    fig.colorbar(cax0, ax=ax0)
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.tight_layout()
    plt.show()
    break

#%%
fig, AX = plt.subplots(1,3, figsize=(15,5))
for key, ax in zip(midpoints.keys(), AX):

    ax.hist(apertures[key], alpha=0.5, bins=50, label='Apertures')#, density=True)
    ax.hist(pl(amplitudes[key]), alpha=0.5, bins=50, label='inferred apertures')#, density=True)
    ax.legend()
    ax.grid()
    ax.set_xlabel('Aperture (m)')
    ax.set_ylabel('PDF')
    ax.set_title(key)
plt.show()
# cax0 = ax0.scatter(midpoints_l[:, 0], midpoints_l[:, 1], c=apertures_l * 1e-2 + int(key[0]) * 1e-3, cmap='turbo',
#                    vmin=vmin, vmax=vmax)

#%%
fig, ax = plt.subplots()
ax.hist(np.hstack([apertures[key] for key in midpoints.keys()]), alpha=0.5, bins=50, label='Apertures') #, density=True)
ax.hist(np.hstack([pl(amplitudes[key]) for key in midpoints.keys()]), alpha=0.5, bins=50, label='inferred apertures')#, density=True)
ax.legend()
ax.grid()
ax.set_xlabel('Aperture (m)')
ax.set_ylabel('PDF')
# ax.set_title(key)
plt.show()
#%%
# # plt.plot(apertures[key], 'k.')
# # plt.plot(apertures[key], 'k.')
# fig = plt.figure()
# ax = fig.gca()
# for key in midpoints.keys():
#     # plt.plot((apertures[key] - pl(amplitudes[key]))/wavelength, '.', label=f'error_{key}')
#     # plt.scatter(apertures[key]*1e2, (apertures[key] - pl(amplitudes[key]))*1e2, s=10,label=f'error_{key}')
#     ax.scatter(apertures_fresnel[key][:,-1]*1e2, np.abs((apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2), s=10,label=f'error_{key}')
# # ax.axis('equal')
# ax.set_ylabel(f'abs(Apertures[blue light] - Apertures[predicted])')
# ax.set_xlabel(f'Apertures')
# ax.legend()
# ax.set_aspect('equal', adjustable='box')
# fig.tight_layout()
# fig.show()
#
# #%%
# s = (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2
# n_bins = 50
# mu = s.mean()
# sigma=np.std(s)
# median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)
#
# fig, ax = plt.subplots(figsize=(5,5))
# # ax.hist(v, bins=50)
# n, bins, patches = ax.hist(s, n_bins, density=True, alpha=.3, edgecolor='black')
# x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
# pdf = norm.pdf(x, mu, sigma)
# ax.plot(x, pdf, color='orange', alpha=.6, label=f'Gaussian Distribution (σ = {sigma:.2f})')
# ax.set_ylabel('Probability density')
# ax.set_xlabel('Apertures (Measured - Predicted) [cm]')
# ax.legend()
# plt.show()

#%%
s = (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2
n_bins = 50
mu = s.mean()
sigma=np.std(s)
median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)

fig, (ax,ax2) = plt.subplots(1,2, figsize=(12,5), sharey=True, gridspec_kw={'width_ratios': [3, 1]})
for key in midpoints.keys():
    # plt.plot((apertures[key] - pl(amplitudes[key]))/wavelength, '.', label=f'error_{key}')
    # plt.scatter(apertures[key]*1e2, (apertures[key] - pl(amplitudes[key]))*1e2, s=10,label=f'error_{key}')
    ax.scatter(apertures_fresnel[key][:,-1]*1e2, (apertures_fresnel[key][:,-1] - pl(amplitudes[key]))*1e2, s=10,label=f'{key}')
# ax.axis('equal')
ax.set_ylabel('Apertures (Measured - Predicted) [cm]')
ax.set_xlabel(f'Measured Apertures [cm]')
ax.legend()
ax.grid()
ax.set_aspect('equal', adjustable='box')


n, bins, patches = ax2.hist(s, n_bins, density=True, alpha=.3, edgecolor='black', orientation='horizontal' )
# x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
x=np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
pdf = scipy.stats.norm.pdf(x, mu, sigma)
ax2.plot(pdf, x, color='red', alpha=.6, label=f'Gaussian Distribution \n(σ = {sigma:.2f} cm)')
ax2.set_xlabel('Probability density')
ax2.legend()
# ax2.set_ylabel('Apertures (Measured - Predicted) [cm]')
fig.tight_layout()
fig.show()

#%%
# midpoints[key][mask]o#%%
line = 17
axis=0
# fig, (ax0, ax1,ax2) = plt.subplots(1,3, figsize=(15,5))
fig, AX = plt.subplots(1,3, figsize=(20,5), sharey=True)
for key, ax in zip(midpoints.keys(), AX):
    if axis==0:
        axis2=1
    else:
        axis2=0


    # df_frac = frac.get_fracture(name_fracture=f'fracture_{key[0]}')
    m = midpoints_l #df_frac[['x','y']].to_numpy()
    ape = apertures_l*1e-2 + int(key[0])*1e-3 #df_frac['aperture'].to_numpy()
    mask = ((m[:, 0] >= out[0] * 1e-2) & (m[:, 0] <= out[1] * 1e-2) &
            (m[:, 1] >= out[0] * 1e-2) & (m[:, 1] <= out[1] * 1e-2))

    m = m[mask]
    ape = ape[mask]

    v = np.unique(midpoints[key][:, axis])[line]
    mask2 = midpoints[key][:, axis] == v
    m2 = midpoints[key][mask2,axis2]

    # Get the closes value
    v2 = m[np.argmin(np.abs(m[:,axis] - v)), axis]
    mask3 = m[:, axis] == v2
    s = np.argsort(m2)
    # mask &= midpoints[key][:,1] ==0.155
    ax.plot(m[mask3,axis2], ape[mask3], color='k', marker='*', label='Real')
    ax.plot(m2[s], pl(amplitudes[key])[mask2][s], color='r', marker='*', label='Calculated')
    ax.set_xlabel('x (m)' if axis==0 else 'y (m)')
    ax.set_title(f'Apertures_{key}')
    ax.legend()
    ax.grid()
AX[0].set_ylabel('Aperture (m)')
plt.show()

#### Create variogram
models = {
    "Gaussian": gs.Gaussian,
    "Exponential": gs.Exponential,
    "Matern": gs.Matern,
    # "Stable": gs.Stable,
    # "Rational": gs.Rational,
    # "Circular": gs.Circular,
    # "Spherical": gs.Spherical,
    # "SuperSpherical": gs.SuperSpherical,
    # "JBessel": gs.JBessel,
}
scores = {}

#%% Here it starts
np.random.seed(1234)
key = '0mm'
fig, ax = plt.subplots()
n_random_points = 200
props = []
for m, a, n, c in zip([midpoints_l, midpoints[key]],
                      [apertures_l, apertures[key] * 1e2],
                      ['Experimental variogram: Original', 'Experimental variogram: Inferred'],
                      ['red', 'blue']):

    rand_pos =np.random.permutation(np.arange(len(m)))

    x_random = m[rand_pos[:n_random_points], 0]
    y_random = m[rand_pos[:n_random_points], 1]

    train = a[rand_pos[:n_random_points]]  # Sampled values

    position_xy = np.vstack([x_random, y_random]).T

    num_bins =20

    # Calculate bin edges
    bins = np.linspace(0,  0.25, num_bins + 1)

    import gstools as gs
    bin_center, gamma = gs.vario_estimate((x_random, y_random), train,
                                      bin_edges=bins,
                                      estimator='cressie'
                                      # sampling_size=3,
                                      # sampling_seed=1234
                                      )

    ax.scatter(bin_center, gamma, label=n, color=c)

    fit_model = gs.Matern(dim=2)
    para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)


    x_s = np.linspace(0, max(bin_center)*4/3)
    ax.plot(x_s, fit_model.variogram(x_s),label=f"{fit_model.name} variogram: {fit_model.len_scale:.2e}",color=c)

    per_scale = fit_model.percentile_scale(0.90)
    ax.vlines(fit_model.len_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='-.')
    # ax.vlines(fit_model.integral_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dotted')
    # ax.vlines(per_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dashed')
    ax.hlines(fit_model.var,xmin=0, xmax=x_s.max(), color=c, linestyle='dotted')

    props.append((fit_model.var, fit_model.len_scale))
ax.legend()
ax.set_xlabel('Lag distance')
ax.set_ylabel(r'Semivariance')
fig.show()

#%%

seed = 1000
midpoints = frac.get_midpoints(name_fractures='fracture_0')
# model= 'exponential'
import gstools as gs

# m = [gs.Gaussian, gs.Exponential, gs.Matern]#, gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
m = gs.Matern  # , gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
# gs.Circular, gs.Spherical, gs.HyperSpherical, gs.SuperSpherical, gs.JBessel, gs.TPLSimple]

# fig, axes = plt.subplots(4, 4, figsize=(12, 12))
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5))
# fig, axes = plt.subplots(1, 3, figsize=(12, 5))
# Flatten the 2D array of subplots for easier indexing
# axes = axes.flatten()
# for model, ax in zip(m, axes):
field1, srf1 = frac.generate_crf(points=midpoints, var=props[0][0], len_scale=props[0][1], plane=2, seed=seed, model=m)
field2, srf2 = frac.generate_crf(points=midpoints, var=props[1][0], len_scale=props[1][1], plane=2, seed=seed, model=m)

if field1.max() > field2.max():
    vmax = field1.max()
else:
    vmax= field2.max()
#%%
fig, AX = plt.subplots(1, 2, figsize=(10,5))
cax1=AX[0].scatter(midpoints[:,0], midpoints[:,1], c=field1, cmap='turbo', s=15, vmax=vmax)
cax2=AX[1].scatter(midpoints[:,0], midpoints[:,1], c=field2, cmap='turbo', s=15, vmax=vmax)
fig.colorbar(cax1, ax=AX[0])
fig.colorbar(cax2, ax=AX[1])
plt.show()
#%% STOP WORKING
















# #%%
Z_b = field1.reshape(spacing)
Z_t = field2.reshape(spacing)

caxb = ax1.pcolormesh(Z_b, cmap='turbo', )
caxt = ax2.pcolormesh(Z_t, cmap='turbo', )
ax1.set_title('Bottom')
ax2.set_title('Top')
fig.colorbar(caxb, ax=ax1)
fig.colorbar(caxt, ax=ax2)

# Hide any empty subplots (in case you have fewer than 16 variables)
# for j in range(len(m), len(axes)):
#     fig.delaxes(axes[j])
plt.tight_layout()
plt.show()
# #%%
# plt.scatter(midpoints[:,0], midpoints[:,1], c=field2, cmap='turbo', )
# plt.colorbar()
# plt.show()
# %%
b_mean = 0.3

top_surface = field1
bottom_surface = field2
# top_surface2 = top_surface + b_mean
# index = top_surface2.ravel() <= bottom_surface.ravel()
# top_surface2[index] =  0
# bottom_surface[index] = 0
#
#
#
# tp = top_surface2.reshape(spacing)
# bt = bottom_surface.reshape(spacing)
#
# plt.pcolormesh(tp);
#
# plt.colorbar();
# plt.show()

apertures = top_surface + b_mean - bottom_surface
mask = apertures <= 0

apertures[mask] = 0

top_surface_m = top_surface.copy()
# top_surface_m[mask] = -b_mean*0.5  bottom_surface_m[mask] = 0
top_surface_m[mask] = -b_mean * 0.5
top_surface_m += b_mean * 0.5
# min_val = top_surface.min()
# top_surface_m[mask] = min_val
# top_surface_m -= top_surface_m.min()

bottom_surface_m = bottom_surface.copy()
bottom_surface_m[mask] = b_mean * 0.5
bottom_surface_m -= b_mean * 0.5
# max_val = top_surface.max()
# bottom_surface_m[mask] = max_val
# bottom_surface_m -= bottom_surface_m.min()

np.savez(outname + 'files.npz',
         top_surface_m=top_surface_m,
         bottom_surface_m=bottom_surface_m,
         apertures=apertures,
         midpoints=midpoints)
