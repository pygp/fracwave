from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR
from fracwave.utils.help_decorators import convert_meter
import numpy as np
import matplotlib.pyplot as plt
import pyvistaqt as pvqt
import pyvista as pv

file = OUTPUT_DIR + 'block.h5'
#%% Create the source
try:
    sou = SourceEM()
    sou.load_hdf5(file)
    fig = sou.plot_waveforms_zoom()
    fig.show()
except:
    sou = SourceEM()
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 599))
    sou.set_source_params(a=6.9,
                          c=1,
                          loc=0,
                          scale=0.5,
                          ph=0,
                          t=1)
    sou.create_source()
    fig = sou.plot_waveforms_complete()
    fig.show()
    fig = sou.plot_waveforms_zoom()
    fig.show()
    sou.export_to_hdf5(file, overwrite=True)
# sou.widgets().show()
# We will define that the maximum element size is a fourth of the wavelength from our source function
c0 = 299_792_458  # Speed of light in m/s
rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)  # To convert in m/ns
max_element_size = velocity / (sou.center_frequency * 5)
print(f'Element max size = {convert_meter(max_element_size)}')
#%% Create the fracture
try:
    frac = FractureGeo()
    frac.load_hdf5(file)
except:
    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=0.34, length=0.34, dip=0, azimuth=0, resolution=(10,10), center=(0.17,0.17,0))
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)

    midpoints = vertices[faces].mean(axis=1)


    # apertures = frac.generate_heterogeneous_apertures(b_mean=1e-3,
    #                                                   points=midpoints,
    #                                                   plane=2,
    #                                                   var=1e-6,
    #                                                   len_scale=0.1)
    apertures = 1e-3
    # grid['aperture'] = apertures
    # grid.plot()
    kwargs_electric_properties = dict(aperture=apertures,  # 10 mm
                                      electrical_conductivity=0, # in S/m
                                      electrical_permeability=81)  # Unitless


    frac.set_fracture(name_fracture='cube',
                      vertices = vertices,
                      faces = faces,
                      overwrite=True,
                      **kwargs_electric_properties)
    frac.export_to_hdf5(file, overwrite=True)

surf = frac.get_surface()
p = frac.plot_geometry(scalars='aperture')
#%%
# apertures = frac.generate_heterogeneous_apertures(b_mean=1e-3,
#                                                       points=midpoints,
#                                                       plane=2,
#                                                       var=1e-6,
#                                                       len_scale=0.05)
# plt.imshow(apertures.reshape((84, 84)) * 1e3, interpolation='None', extent=(-30, 30, -30, 30))
# plt.xlabel('cm')
# plt.ylabel('cm')
# plt.colorbar(label='mm')
# plt.show()
#%%% Now the antennas
try:
    ant = Antenna()
    ant.load_hdf5(file)
except:
    ant = Antenna()
    separation = 3.6e-2  # cm

    positions = []
    pi = 8 - 2.1 - 4.5  # starting 8cm to the right and then 2.1 sm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    positions.append(pi)
    for i in range(1, 14):
        # if the number is even append otherwise not
        if i % 2 == 0:
            positions.append(positions[-1] + 4.5 - 2.17)
        else:
            positions.append(positions[-1] + 2.17)
    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    # x = np.arange(0, 31.5, 4.5)
    #
    # x = np.arange(-0.4,0.41, 0.1)
    #
    # y = np.arange(-0.4,0.41, 0.1)
    # XX, YY = np.meshgrid(x,y)

    top = 0.10  # cm to top
    line = np.arange(-9.73+4.5, 49.73-4.5, 0.5)
    ltx = line + separation / 2
    lrx = line - separation / 2
    for i in x[1:-1]:
        tx = np.zeros((len(line),3))
        tx[:,0] = i
        tx[:,1] = ltx
        tx[:,2] = top

        rx = tx.copy()
        rx[:,1] = lrx

        orient = np.zeros((len(line),3))
        orient[:,1] = 1

        ant.set_profile(name_profile=f'x_{i}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line,
                        depth_Tx=line,
                        overwrite=True)

    line2 = np.arange(34+10-0.27-4.5, 34+10-60+4.5, -0.5)
    ltx2 = line2 + separation / 2
    lrx2 = line2 - separation / 2
    for i in y[1:-1]:
        tx = np.zeros((len(line2), 3))
        tx[:, 0] = ltx2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = lrx2

        orient = np.zeros((len(line2), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'y_{i}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line2,
                        depth_Tx=line2,
                        overwrite=True)
    ant.export_to_hdf5(file, overwrite=True)
ant.plot()
#%%
try:
    solv = FracEM()
    solv.load_hdf5(file)

except:
    # Run in cluster
    solv = FracEM()
    solv.open_file(file)

    solv.rock_epsilon = 8.4  # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium
    # solv.backend = 'torch'  # You can choose between cupy, torch, numpy or dask (dask is for running parallel but maybe not working correctly). Cupy is to run in GPU. Numpy and torch are the most stable ones. We suggest to run torch since is the fastest one.
    solv.backend = 'cupy'  # You can choose between cupy, torch, numpy or dask (dask is for running parallel but maybe not working correctly). Cupy is to run in GPU. Numpy and torch are the most stable ones. We suggest to run torch since is the fastest one.
    solv.engine='tensor_trace'  # This is the engine that will be used to compute the simulation. Is also the most stable one. Don't try to choose another option if you are not sure what you are doing
    solv.mode = 'reflection' # you can change to ['transmission', 'reflection', 'full_reflection', 'incoming_field', 'dipole_response', 'propagation'] depending on what you want to calculate
    # 'propagation' is to calculate the propagation of the field to the fracture

    solv._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have energy in the source function. We also skip some frequencies that don't contribute to the final result. Set to False to include all the frequencies and elements
    solv.apply_causality = False # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those fracture elements that are too far from the antennas and will not show in the time window. Not calculating if _fast_calcualtion_incoming_field is False
    solv.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achieve this, we apply a filtering process that eliminates the lower n% of energy across all fracture elements. This selective approach allows us to reduce computation time by excluding elements that have minimal impact on the final results.
    solv._filter_percentage = 0.01

    _, _2 = solv.time_zero_correction()

    freq = solv.forward_pass()

#%%
freq = solv.get_freq('all')#name_profile='x_0.25')
#%%
time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
#%%
# plt.imshow(time_response.T, aspect='auto',cmap='RdBu')
# plt.colorbar()
# plt.show()
#
# #%%
#
# # plt.plot(time_vector, time_response[0])
# plt.plot(sou.frequency, freq[25])
# plt.show()
# #%%
# plt.plot(time_vector, time_response[12])
# plt.show()
#
# #%%
# plt.plot()
#%%
#
#
# time_response, time_vector = solv.get_ifft(freq, pad=1000)
# time_response = np.real(time_response)
#%%
max_value = np.max(np.abs(time_response))
# Now we can visualize using matplotlib
time_response_n = time_response.T /  max_value
fig, ax = plt.subplots(1,1, figsize=(10,8))

extent = (0, time_response_n.shape[1], time_vector[-1], time_vector[0])
vmax = 1
cax = ax.imshow(time_response_n, aspect='auto', cmap='seismic', extent=extent, vmin=-vmax, vmax=vmax, interpolation=None)
ax.set_xlabel('Trace number (Tx-Rx pair)')
ax.set_ylabel('Two-way Travel-Time (ns)')
ylims_d = (0.7, 0)
# ylims = (300, 0)
ylims = (max(ylims_d) * 2 /  solv.velocity, min(ylims_d) * 2 / solv.velocity)
ax.set_ylim(ylims)

ax2 = ax.twinx()
ax2.set_ylim(ylims_d) # To account for the 2 way travel time
ax2.set_ylabel('Radial distance (m)')
ax2.grid()
plt.show()

#%%
# tv = time_vector
# for i in range(len(time_response)):
#     plt.plot(tv, time_response[i], label=f'{i}')
# plt.xlim(5,10)
# plt.grid()
# plt.show()


#%%
all_profiles = ant.name_profiles
surf = frac.get_surface()
Tx, Rx, Tx_arrows, Rx_arrows = ant.get_geometry(show=False)
#%%

p = pvqt.BackgroundPlotter()
p.add_mesh(surf, name='mesh', scalars='aperture', show_edges=False)
# p.add_mesh(surf, name='mesh', scalars='fracture_field', show_edges=False, clim=(0, 1))
p.add_mesh(Tx, color='red', point_size=3)
p.add_mesh(Rx, color='blue', point_size=3)
# p.add_mesh(Tx_arrows, color='red')
# p.add_mesh(Rx_arrows, color='blue')

p.show_bounds()
p.show_axes()
#%%
from gdp.processing.gain import apply_gain
# all_grids = dict()
energy_all = np.zeros((ant.ntraces, 4))
count = 0
for i in all_profiles:

    freq = solv.get_freq(name_profile=i)
    time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
    time_response = np.real(time_response)
    time_response /= max_value
    energy = np.abs(time_response).max(axis=1)
    energy_all[count:count+len(freq), 3] = energy
    # time_response, _ = apply_gain(time_response.T,
    #                    sfreq= 1 / (np.diff(time_vector).mean()),
    #                    gain_type='spherical',
    #                    exponent= 3,
    #                    velocity = solv.velocity,
    #                    twoway=True)
    # time_response = time_response.T
    mask = (time_vector >=0) & (time_vector < 15)
    time_response = time_response[:, mask]
    time_vector = time_vector[mask]

    axis, const = i.split('_')
    dep = ant.profiles.loc[ant.profiles.profile == i, ('depth_Rx')].to_numpy()
    t = ant.profiles.loc[ant.profiles.profile == i, ('Rz')].mean()
    path = np.zeros((len(dep), 3))
    if axis == 'x':
        path[:, 0] = float(const)
        path[:,1] = dep
        path[:,2] = t
    elif axis == 'y':
        path[:, 0] = dep
        path[:, 1] = float(const)
        path[:, 2] = t

    energy_all[count:count+len(freq), :3] = path
    count += len(freq)
    ntraces, nsamples = time_response.shape

    depth_vetor=time_vector*solv.velocity/2
    z_spacing = np.diff(depth_vetor).mean()

    points = np.repeat(path, nsamples, axis=0)
    # repeat the Z locations across
    tp = np.arange(0, z_spacing * nsamples, z_spacing)
    tp = path[:, 2][:, None] - tp
    points[:, -1] = tp.ravel()

    grid = pv.StructuredGrid()
    grid.points = points
    grid.dimensions = nsamples, ntraces, 1

    # Add the data array - note the ordering
    grid["values"] = time_response.T.ravel(order="F")

    # all_grids[i] = (grid)

    p.add_mesh(grid, name=i, cmap='seismic', scalars='values', opacity=0.9, clim=(-0.5, 0.5))
    # break
#%%
energy_all[:,2] += 0.5
p.add_mesh(energy_all[:,:3], name='on_surface', cmap='turbo', scalars=energy_all[:,3])#, clim=(-1, 1))

#%%
ff = solv.file_read('simulation/fracture_field')
field_at_fracture = ff[i]
field_at_fracture /= np.abs(field_at_fracture).max()
surf['fracture field'] = field_at_fracture
p.add_mesh(surf, name='mesh', scalars='fracture field', show_edges=False)
#%%


#%%
# time_response[time_response < np.abs(time_response).max() * 0.01] = np.NaN
# p.export_html(OUTPUT_DIR+'test.html')

freq = solv.get_freq(name_profile=['x_0.09999999999999987'])
time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
time_response /= np.abs(time_response).max()
import numpy as np
import matplotlib.pyplot as plt

fig,ax = plt.subplots(figsize=(20,10))

y = time_vector
offsets = np.arange(0,len(time_response)+1)


scale =5
for offset, time in zip(offsets, time_response):
    x = offset + scale*time

    ax.plot(x, y, linestyle='-', color='black', linewidth=1)
    ax.fill_betweenx(y,offset,x,where=(x>offset),color='black')#, alpha=0.5)

# ax.set_xlim(93,98)
ax.invert_yaxis()
plt.show()

#%% Fresnel zone
d = 0.3
R = np.sqrt((solv.wavelength * d))

#%%
from matplotlib.patches import Circle
fig, ax = plt.subplots()
for i in range(4):
    circle = Circle((0.5, 0,5), i* np.sqrt((solv.wavelength * d))*0.5, fill=False)
    ax.add_patch(circle)
    ax.set_aspect('equal')

plt.show()

#%%
def fresnel_zone(wavelength, distance):
    return 0.5 * np.sqrt(wavelength * distance)

#%%
aperture_field = np.fliplr(frac.fractures.aperture.to_numpy().reshape((84,84))).T


ff = solv.file_read('simulation/fracture_field')


vertices = frac.vertices
midpoints = frac.midpoints
extent = (vertices[:,0].min(), vertices[:,0].max(),
          vertices[:,1].min(), vertices[:,1].max(),
          #vertices[:,2].min(), vertices[:,2].max(),
          )

positions_antenna = np.mean((ant.profiles[['Tx', 'Ty', 'Tz']].to_numpy(),
               ant.profiles[['Rx', 'Ry', 'Rz']].to_numpy()), axis=0)

infor = ant.profiles.iloc[i]

xy = np.mean((infor[['Tx', 'Ty']].to_numpy(),
               infor[['Rx', 'Ry']].to_numpy()), axis=0)

fz = fresnel_zone(solv.wavelength, positions_antenna[:,2])
#%%
i = 850
fig, ax = plt.subplots()
field_at_fracture = ff[i]
field_at_fracture /= np.abs(field_at_fracture).max()
# field_at_fracture = field_at_fracture.reshape((84,84))

field_at_fracture = np.fliplr(field_at_fracture.reshape((84,84))).T

# ax.imshow(aperture_field, interpolation='None', extent=extent, cmap='viridis')
im = ax.imshow(field_at_fracture, interpolation='None', extent=extent, cmap='viridis')
sc1 = ax.scatter(positions_antenna[:,0], positions_antenna[:,1], c=energy_all[:,3], cmap='turbo',s=10)

sc2 = ax.scatter(positions_antenna[i,0], positions_antenna[i,1], c='red', s=100, marker='*')
circle = Circle((positions_antenna[i,0], positions_antenna[i,1]), fz[i], fill=False, color='red', linewidth=2)
cir=ax. add_patch(circle)
plt.show()
#%%
from matplotlib.animation import FuncAnimation, PillowWriter
from matplotlib.patches import Circle

i = 850
fig, ax = plt.subplots()
def animate(i):
    ax.clear()

    field_at_fracture = ff[i]
    field_at_fracture /= np.abs(field_at_fracture).max()
    # field_at_fracture = field_at_fracture.reshape((84,84))

    field_at_fracture = np.fliplr(field_at_fracture.reshape((84,84))).T




    # ax.imshow(aperture_field, interpolation='None', extent=extent, cmap='viridis')
    im = ax.imshow(field_at_fracture, interpolation='None', extent=extent, cmap='viridis')
    sc1 = ax.scatter(positions_antenna[:,0], positions_antenna[:,1], c=energy_all[:,3], cmap='turbo',s=10)

    sc2 = ax.scatter(positions_antenna[i,0], positions_antenna[i,1], c='red', s=100, marker='*')
    circle = Circle((positions_antenna[i,0], positions_antenna[i,1]), fz[i], fill=False, color='red', linewidth=2)
    cir=ax. add_patch(circle)
    # fig.colorbar(sc, ax=ax)
    return im, sc1, sc2, cir

ani = FuncAnimation(fig, animate, interval=ant.ntraces, blit=True, repeat=True)
ani.save(OUTPUT_DIR+"TLI.gif", dpi=100, writer=PillowWriter(fps=5))
