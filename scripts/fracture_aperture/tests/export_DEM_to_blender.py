import numpy as np
import matplotlib.pyplot as plt
from fracwave import FractureGeo
from bedretto import model
p = model.ModelPlot()
# import cv2
# from shapely import geometry
# import fiona
# import json
# import os
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours2/'
#%%
frac = FractureGeo()
width = 100
length = 100
max_element_size = 0.05  # 1 mm resolution

seed = 2021, 2022
v, f = frac.create_regular_squared_mesh(width=width, length=length, center=(0,0,0), azimuth=0, dip=0, resolution=[10,10])
# vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, center=(0,0,0), azimuth=0, dip=0, resolution=spacing)

# surf = frac.create_pyvista_mesh(vertices, faces)

surf, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)
spacing = [2000, 2000]
p.add_object(surf, name='fracture', color='yellow')
#%%
try:
    d = np.load(outname+'files.npz')
    top_surface_m=d['top_surface_m']
    bottom_surface_m=d['bottom_surface_m']
    apertures=d['apertures']
    midpoints = d['midpoints']
except:

#%%
    midpoints = vertices#frac.get_midpoints(vertices, faces)
    # model= 'exponential'
    import gstools as gs
    # m = [gs.Gaussian, gs.Exponential, gs.Matern]#, gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
    m = gs.Matern#, gs.Integral, gs.Stable, gs.Rational, gs.Cubic, gs.Linear,
         # gs.Circular, gs.Spherical, gs.HyperSpherical, gs.SuperSpherical, gs.JBessel, gs.TPLSimple]

    # fig, axes = plt.subplots(4, 4, figsize=(12, 12))
    fig, (ax1,ax2) = plt.subplots(1, 2, figsize=(12, 5))
    # fig, axes = plt.subplots(1, 3, figsize=(12, 5))
    # Flatten the 2D array of subplots for easier indexing
    # axes = axes.flatten()
    # for model, ax in zip(m, axes):
    field1, srf1 = frac.generate_crf(points=midpoints, var=0.05, len_scale=4, plane=2, seed=seed[0], model=m)
    field2, srf2 = frac.generate_crf(points=midpoints, var=0.05, len_scale=4, plane=2, seed=seed[1], model=m)

        # #%%
    Z_b = field1.reshape(spacing)
    Z_t = field2.reshape(spacing)

    caxb = ax1.pcolormesh(Z_b, cmap='turbo', )
    caxt = ax2.pcolormesh(Z_t, cmap='turbo', )
    ax1.set_title('Bottom')
    ax2.set_title('Top')
    fig.colorbar(caxb, ax=ax1)
    fig.colorbar(caxt, ax=ax2)

    # Hide any empty subplots (in case you have fewer than 16 variables)
    # for j in range(len(m), len(axes)):
    #     fig.delaxes(axes[j])
    plt.tight_layout()
    plt.show()
    # #%%
    # plt.scatter(midpoints[:,0], midpoints[:,1], c=field2, cmap='turbo', )
    # plt.colorbar()
    # plt.show()
    #%%
    b_mean = 0.3

    top_surface=field1
    bottom_surface=field2
    # top_surface2 = top_surface + b_mean
    # index = top_surface2.ravel() <= bottom_surface.ravel()
    # top_surface2[index] =  0
    # bottom_surface[index] = 0
    #
    #
    #
    # tp = top_surface2.reshape(spacing)
    # bt = bottom_surface.reshape(spacing)
    #
    # plt.pcolormesh(tp);
    #
    # plt.colorbar();
    # plt.show()

    apertures = top_surface + b_mean - bottom_surface
    mask = apertures <= 0

    apertures[mask] = 0

    top_surface_m = top_surface.copy()
    # top_surface_m[mask] = -b_mean*0.5  bottom_surface_m[mask] = 0
    top_surface_m[mask] = -b_mean*0.5
    top_surface_m += b_mean*0.5
    # min_val = top_surface.min()
    # top_surface_m[mask] = min_val
    # top_surface_m -= top_surface_m.min()

    bottom_surface_m = bottom_surface.copy()
    bottom_surface_m[mask] = b_mean*0.5
    bottom_surface_m -= b_mean*0.5
    # max_val = top_surface.max()
    # bottom_surface_m[mask] = max_val
    # bottom_surface_m -= bottom_surface_m.min()

    np.savez(outname + 'files.npz',
             top_surface_m=top_surface_m,
             bottom_surface_m=bottom_surface_m,
             apertures=apertures,
             midpoints=midpoints)

#%%
X = midpoints[:, 0].reshape(spacing)
Y = midpoints[:, 1].reshape(spacing)

Z_A = apertures.reshape(spacing)
Z_t = top_surface_m.reshape(spacing)
Z_b = bottom_surface_m.reshape(spacing)
mask_r =mask.reshape(spacing)
ap = Z_t - Z_b
assert np.allclose(ap, Z_A)
assert np.allclose(Z_t[mask_r] + Z_b[mask_r], Z_A[mask_r])
#%%
# apertures = frac.generate_heterogeneous_apertures(b_mean=b_mean, seeds=seed, points=midpoints, var=0.1, len_scale=5,
#                                                  plane=2)
#%%
# plt.scatter(midpoints[:,0], midpoints[:,1], c = apertures, cmap='turbo', )
# plt.colorbar()
# plt.show()

#%%
fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(20,5))
s1 = ax1.pcolormesh(X, Y, Z_t, cmap='turbo')
# s1 = ax1.pcolormesh(midpoints[:,0], midpoints[:,1], c = top_surface_m, cmap='turbo')
ax1.set_title('Top Face')
ax1.axis('equal')
# s2 = ax2.scatter(midpoints[:,0], midpoints[:,1], c = bottom_surface_m, cmap='turbo')
s2 = ax2.pcolormesh(X, Y, Z_b, cmap='turbo')
ax2.set_title('Bottom Face')
ax2.axis('equal')
# s3 = ax3.scatter(midpoints[:,0], midpoints[:,1], c = apertures, cmap='turbo')
s3 = ax3.pcolormesh(X, Y, Z_A, cmap='turbo')
ax3.axis('equal')
ax3.set_title('Apertures')
fig.colorbar(s1, ax=ax1)
fig.colorbar(s2, ax=ax2)
fig.colorbar(s3, ax=ax3)
plt.show()

#%%
# plt.hist(np.log(Z_A[Z_A>0].ravel()), bins=100, )
plt.hist(Z_A[Z_A>0].ravel(), bins=100, )
plt.show()

# add labels to the histogram

#%%
# Define a helper function to rotate the matrix once
def rotate_once(matrix):
    # Transpose the matrix
    transposed_matrix = [list(row) for row in zip(*matrix)]
    # Reverse each row
    rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
    return rotated_matrix


    # Apply multiple rotations
    rotated_matrix = matrix
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix

#%%
levels = 50
fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(20,5))
s1 = ax1.contour(X*1e-2, Y*1e-2, rotate_once(Z_t), levels, cmap='turbo')
# s1 = ax1.pcolormesh(midpoints[:,0], midpoints[:,1], c = top_surface_m, cmap='turbo')
ax1.set_title('Top Face', fontsize=30)
ax1.axis('equal')
# s2 = ax2.scatter(midpoints[:,0], midpoints[:,1], c = bottom_surface_m, cmap='turbo')
s2 = ax2.contour(X*1e-2, Y*1e-2, rotate_once(Z_b), levels, cmap='turbo')
ax2.set_title('Bottom Face',fontsize=30)
ax2.axis('equal')
# s3 = ax3.scatter(midpoints[:,0], midpoints[:,1], c = apertures, cmap='turbo')
s3 = ax3.contour(X*1e-2, Y*1e-2, rotate_once(Z_A), levels, cmap='turbo')
ax3.axis('equal')
ax3.set_title('Apertures',fontsize=30)
fig.colorbar(s1, ax=ax1, label='Height difference (cm)')
fig.colorbar(s2, ax=ax2, label='Height difference (cm)')
fig.colorbar(s3, ax=ax3, label='Apertures (cm)')
plt.show()
#
#%%
# lvl_lookup = dict(zip(s1.collections, s1.levels))
#
# # loop over collections (and polygons in each collection), store in list for fiona
# PolyList = []
# for col in s1.collections:
#     z = lvl_lookup[col]  # the value of this level
#     for contour_path in col.get_paths():
#         # create the polygon for this level
#         for ncp, cp in enumerate(contour_path.to_polygons()):
#             lons = cp[:, 0]
#             lats = cp[:, 1]
#             new_shape = geometry.Polygon([(i[0], i[1]) for i in zip(lons, lats)])
#             if ncp == 0:
#                 poly = new_shape  # first shape
#             else:
#                 poly = poly.difference(new_shape)  # Remove the holes
#
#             PolyList.append({'poly': poly, 'props': {'z': z}})
#
# #%%
# # define ESRI schema, write each polygon to the file
# outfi=os.path.join(outname,'shaped_contour.shp')
# schema = {'geometry': 'Polygon','properties': {'z': 'float'}}
# with fiona.collection(outfi, "w", "ESRI Shapefile", schema) as output:
#     for po in PolyList:
#         output.write({'properties': po['props'],
#             'geometry': geometry.mapping(po['poly'])})
#
# # save the levels and global min/max as a separate json for convenience
# Lvls={'levels':s1.levels.tolist(),'min':Z_t.min(),'max':Z_t.max()}
# with open(os.path.join(outname,'levels.json'), 'w') as fp:
#     json.dump(Lvls, fp)

#%%
# import tifffile as tiff
#
# tiff.imwrite(outname+'top_surface.tiff', Z_t)

#%%
# import numpy as np
#
# # Define the parameters of your heightmap
# width = 512  # Width of the heightmap
# height = 512  # Height of the heightmap
# frequency = 0.1  # Adjust the frequency of the waves
# amplitude = 10  # Adjust the amplitude of the waves
#
# # Generate the heightmap using a sine wave pattern
# x = np.linspace(0, 1, width)
# y = np.linspace(0, 1, height)
# x, y = np.meshgrid(x, y)
# heightmap = amplitude * np.sin(2 * np.pi * frequency * (x + y))
# from PIL import Image
# image = Image.fromarray(heightmap, mode='L')
# image.save(outname+'top_surface.png')

#%%%
import pyvista as pv

surf2 = pv.StructuredGrid(X, Y, Z_t)
surf3 = pv.StructuredGrid(X, Y, Z_b)
# surf3 = pv.StructuredGrid(X, Y, Z_b-5.5)

p.add_object(surf2, name='fracture', color='yellow', opacity=0.5)
p.add_object(surf3, name='fracture3', color='blue', opacity=0.5)

#%%
ver1, fac1 = frac.extract_vertices_and_faces_from_pyvista_mesh(surf2)
ver2, fac2 = frac.extract_vertices_and_faces_from_pyvista_mesh(surf3)

#%%
np.savez(outname+'vertices_faces_surfaces.npz',
         top_vertices=ver1,
         top_faces=fac1,
         bot_vertices=ver2,
         bot_faces=fac2)

# Give me an example using lamda for creating a function where it receives 2 arguments and the output is the sum of the arguments




#%%

# import numpy as np
# import bpy
#
# # Load vertices and faces
# file = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/vertices_faces_surfaces.npz'
# d = np.load(file)
#
# top_vertices=d['top_vertices']
# top_faces=d['top_faces']
# bot_vertices=d['bot_vertices']
# bot_faces=d['bot_faces']
#
# mesh = bpy.data.meshes.new("CustomMesh")
# obj = bpy.data.objects.new("CustomObject", mesh)
#
# bpy.context.scene.collection.objects.link(obj)
#
# mesh.from_pydata(top_vertices, [], top_faces)
# mesh.update()
#
#
# # Adjust the view and rendering settings as needed
# bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME', center='BOUNDS')
#
# # Save or export your Blender project with the model
# bpy.ops.wm.save_mainfile(filepath="your_model.blend")

#%%
## Make a dent in the model


#%%
