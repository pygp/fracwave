import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain
from gdp.processing.time_lapse import hilbert_phase_difference

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM


file_h5 = OUTPUT_DIR + 'fracture_aperture/source_gypsum_block.h5'


#%%
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/Aluminum/Alu-1/'

data_measu = {}
data_together = {}

files = os.listdir(base_path)
vmax=0
i =0
for file in files:
    if '.sgy' not in file:
        continue
    # info = re.split('_', file)
    # # identif = info[0]
    # # Get the date
    # # date = info[1]
    # # Get the ID
    # ID = info[0]
    # # Get the path
    # # experiment_number = int(info[2])
    # experiment_number = int(info[1])
    # print(experiment_number)
    path = os.path.join(base_path, file)
    # Get the data
    data_cube, header = load_sgy(path)

    # if ID =/= 'B':  # It is flipped
        # data_cube = np.fliplr(data_cube)
    # Store the data in the dictionary
    # d = {experiment_number: {'header':header, 'data': data_cube}}
    # Processing
    d_de = detrend(data_cube)
    sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
    c0 = 299_792_458  # Speed of light in m/s
    rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
    velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
    # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
    # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
    d_de_ga = d_de.copy()
    data_uncut = d_de_ga.copy()
    # if ID in ['B', 'D']:
    #     # lims_x = (20,90)
    #     # lims_x = (70,110)
    #
    # elif ID in ['A', 'C']:
    #     lims_x = (10, 82)
    # else:
    #     raise AttributeError
    # lims_y = (80, 100)
    # lims_x = (10, 85)
    # lims_y = (75, 110)
    # d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
    # d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
    # d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from below
    # d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from below
    time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
    depth_vector = time_vector * velocity * 0.5
    vma = np.abs(d_de_ga).max()
    if vma > vmax: vmax=vma
    d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga, 'time_vector':time_vector,
         'depth_vector':depth_vector,
         # 'lims_x': lims_x, 'lims_y': lims_y
         }
    data_measu[i] = d
    if np.abs(np.max(d_de_ga)) > vmax:
        vmax=np.abs(np.max(d_de_ga))
    i+=1
    # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

           #  #%%%5
           #  from impdar.lib.RadarData import RadarData, RadarFlags
           #  imp_data = RadarData(None)
           #  imp_data.fn = path
           #
           #
           #  imp_data.dt = 1/float(header[1]['Sampling Rate [GHz]'])
           #  imp_data.data = data_cube
           #
           #  # Remove pretrigger
           #  # trig_threshold = 0.5  # trigger when mean trace gets up to 50% of maximum
           #  # mean_trace = np.nanmean(np.abs(h5_data.data), axis=1)
           #  # idx_threshold = np.argwhere(mean_trace > trig_threshold * np.nanmax(mean_trace))
           #  # idx_trig = np.nanmin(idx_threshold)
           #  # h5_data.data = h5_data.data[idx_trig:]
           #
           #  # other variables are from the array shape
           # imp_data.snum = imp_data.data.shape[0]
           # imp_data.tnum = imp_data.data.shape[1]
           # imp_data.trace_num = np.arange(imp_data.data.shape[1]) + 1
           # imp_data.trig_level = np.zeros((imp_data.tnum,))
           # imp_data.pressure = np.zeros((imp_data.tnum,))
           # imp_data.flags = RadarFlags()
           # imp_data.travel_time = imp_data.dt * 1.0e6 * np.arange(imp_data.snum)
           # imp_data.trig = np.zeros((imp_data.tnum,))
           # imp_data.lat = np.zeros((imp_data.tnum,))
           # imp_data.long = np.zeros((imp_data.tnum,))
           # imp_data.x_coord = np.zeros((imp_data.tnum,))
           # imp_data.y_coord = np.zeros((imp_data.tnum,))
           # imp_data.elev = np.zeros((imp_data.tnum,))
           # imp_data.decday = np.arange(imp_data.tnum)
           # imp_data.trace_int = np.ones((imp_data.tnum,))
           #
           #  imp_data.dist = np.arange(imp_data.tnum) / 1.0e3
           #  imp_data.chan = -99.
           #  imp_data.check_attrs()
           #
           #  from impdar.lib.plot import plot_traces, plot_radargram
           #  plot_radargram(imp_data)
    #
    # data_together[key] = {'AC':{},
    #                       'BD':{}}
    #
    # max_amount_traces = 86
    # for i in range(28):
    #     print(i)
    #     da = data_measu[key]['A'][i]
    #     dc = data_measu[key]['C'][i]
    #     db = data_measu[key]['B'][i]
    #     dd = data_measu[key]['D'][i]
    #
    #     dac ={'header': da['header'],
    #         'data_raw': (da['data_raw'][:,:max_amount_traces]+dc['data_raw'][:,:max_amount_traces])/vmax ,
    #         'data_uncut':(da['data_uncut'][:,:max_amount_traces] + dc['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (da['data'][:,:max_amount_traces]+dc['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':da['time_vector'],
    #         'depth_vector':da['depth_vector'],
    #         'lims_x': (da['lims_x'][0], max_amount_traces),
    #         'lims_y': da['lims_y']}
    #
    #     dbd ={'header': db['header'],
    #         'data_raw': (db['data_raw'][:,:max_amount_traces]+dd['data_raw'][:,:max_amount_traces])/vmax,
    #         'data_uncut':(db['data_uncut'][:,:max_amount_traces] + dd['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (db['data'][:,:max_amount_traces]+dd['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':db['time_vector'],
    #         'depth_vector':db['depth_vector'],
    #         'lims_x': (db['lims_x'][0],max_amount_traces),
    #         'lims_y': db['lims_y']}
    #
    #     data_together[key]['AC'][i] = dac
    #     data_together[key]['BD'][i] = dbd

# #%%
# from impdar.lib import load
# dat = load.load('mat','data/synthetic_radargram.mat')[0]

#%%

inf = data_measu[3]
# lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
# d = inf['data']
d = inf['data_uncut']
# from scipy.io import savemat
# savemat(OUTPUT_DIR+'AC_0mm_10', inf)
# vmax=d.max() * 0.05
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  inf['time_vector'] #* 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
vmax =  np.max(np.abs(d))
plt.figure()
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           # extent=extent
           )

# plt.ylabel('Distance (cm)')
plt.ylabel('Two way travel time (ns)')
# plt.ylim(2.7,1.5)
# plt.ylim(4,1)
# plt.xlim(20,80)
plt.colorbar()
plt.show()
#%%
from gdp.plotting import plot_frequency_information

crop_from = slice(99,500)
trace = 20
tr = d[crop_from, trace]
# ax = plot_frequency_information(d[200:300,20], 1/float(header.loc['Sampling Rate [GHz]', 1]))
ax = plot_frequency_information(tr, 1/float(header.loc['Sampling Rate [GHz]', 1]))

#%%

t_vec = inf['time_vector'][crop_from]
t_vec -= t_vec[0]

sou = SourceEM()
sou.type = 'generalgamma'
sou.set_time_vector(t_vec)
sou.set_delay(2)

info, (waveform_time, waveform_freq, source_freq) = sou.from_time_to_frequency(t=t_vec, source_time=tr)

plt.plot(info['time'], waveform_time);plt.show()

plt.plot(info['frequency'], source_freq);plt.show()


#%%
real_norm = np.abs(source_freq) / np.abs(source_freq).max()

def opt_source(x):
    """
        Optimization function
        Args:
            x: vector to optimize. Parameters of source function
        Returns:

        """
    sou.set_source_params(a=x[0], c=x[1], loc=x[2], scale=x[3], ph=x[4])
    source_in = sou.create_source()
    op = np.sqrt(np.sum(np.square(np.abs(source_in) - real_norm)))
    print(op)
    return op

x = [9,  # a
     0.2,  # c
     0,  # loc
     0.2,  # scale
     0,  # ph
     ]
bounds = [(0.5, 15),
          (0.5, 1.5),
          (0, 0),
          (0.01, 1),
          (0, 0),
          ]

from scipy import optimize

res = optimize.minimize(opt_source,
                        x,
                        bounds=bounds,
                        method='nelder-mead',
                        options={'maxfun': 10000,
                                 'xatol': 1e-6,
                                 'disp': True})

print(res.x)
# array([9.17809204 0.95575954 0.         0.28381535 0.  ])  Fitting one
# [11.74609167  0.84948015  0.          0.15794249  0.        ]

sou.set_source_params(a=res.x[0], c=res.x[1], loc=res.x[2], scale=res.x[3], ph=res.x[4])

sou.create_source()
fig, ax = plt.subplots()
ax.plot(sou.frequency, real_norm)
ax.plot(sou.frequency, np.abs(sou.source))
ax.set_xlim(0, 7.5)
ax.set_xlabel('Frequency (GHz)')
ax.set_ylabel('Amplitude (-)')
ax.grid()
plt.show()

sou.plot_waveforms_complete().show()
sou.plot_waveforms_zoom().show()



#%%
#%% Source

sou = SourceEM()
sou.type = 'generalgamma'
sou.set_time_vector(np.linspace(0, 15, 655))

sou.set_delay(delay=1)
sou.set_source_params(a=9.17809204,
                      c=0.95575954,
                      loc=0,
                      scale=0.28381535)

# sou.set_source_params(a=11.74609167,
#                       c=0.84948015,
#                       loc=0,
#                       scale=0.15794249)
sou.create_source()
# sou.widgets().show()

fig = sou.plot_waveforms_zoom()
# fig = sou.plot_waveforms_complete()
fig.show()
sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5


