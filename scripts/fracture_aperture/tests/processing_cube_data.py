import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain

from fracwave import FractureGeo, Antenna



#%%
# base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20231017_162905'
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20231116_004356'

# Get all the files in the folder
folders = os.listdir(base_path)

# loop through all the folders and get the files. From the filename extract the information.
# The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

# Create a dictionary to store the information
data = {'A':{},
         'B':{}}
vmax = 0
for folder in folders:
    # Get the files
    files = os.listdir(os.path.join(base_path, folder))
    for file in files:
        info = re.split('_', file)
        # identif = info[0]
        # Get the date
        # date = info[1]
        # Get the ID
        ID = info[1]
        # Get the path
        experiment_number = info[2]

        path = os.path.join(base_path, folder, file)
        # Get the data
        data_cube, header = load_sgy(path)
        # Store the data in the dictionary
        # d = {experiment_number: {'header':header, 'data': data_cube}}
        # Processing
        d_de = detrend(data_cube)
        sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
        c0 = 299_792_458  # Speed of light in m/s
        rock_epsilon = 40  # Relative permitivity of the medium (dielectric constant)
        velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
        d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='spherical', velocity=velocity)
        # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
        d_de_ga[400:,:] = 0 # crop all reflections from below
        time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
        depth_vector = time_vector * velocity * 0.5

        d = {'header': header, 'data_raw': data_cube, 'data': d_de_ga, 'time_vector':time_vector, 'depth_vector':depth_vector}
        data[ID][experiment_number] = d
        if np.abs(np.max(d_de_ga)) > vmax:
            vmax=np.abs(np.max(d_de_ga))
        # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}
#%%
#%% Detrend
# d_de = detrend(dat)

#%% Remove direct wave
# d_de_svd, svd_matrix = remove_svd(d_de)
# #%%
# sf = float(header.loc['Sampling Rate [GHz]',1]) * 1e3 # To convert from GHz to MHz
# c0 = 299_792_458  # Speed of light in m/s
# rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
# velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
# d_de_ga, gain_matrix= apply_gain(d_de, sfreq=sf, gain_type='spherical', velocity=velocity)
# #%%
# from gdp.plotting import plot_frequency_information
# plot_frequency_information(d_de[:, 30], dt=1/sf)
 # No real need to do filtering

 #%%


#%%
d = data['A']['005']['data']
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  data['A']['001']['depth_vector'] * 100
extent = (0, len(d), time_vector[-1], depth_vector[0])
# vmax =  np.max(np.abs(d))
plt.imshow(d, aspect='auto', cmap='RdBu', vmin =-vmax, vmax =vmax, interpolation='None', extent=extent)
plt.ylabel('Penetration (cm)')
plt.colorbar()
plt.show()

#%%%%%%%%%%%% Fracture geometry

width = 40
length = 40

frac = FractureGeo()
vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, center=(0,0,0), azimuth=0, dip=0, resolution=(50,50))
surf = frac.create_pyvista_mesh(vertices, faces)

#%%%
p.add_object(surf, name='fracture', color='yellow', show_edges=True, opacity=0.2)

#%% Antenna positions
z_position = 30
antenna_spacing = 3.6
measurement_spacing = 0.5 # 0.5 cm per measurement

extra_lenght = 0.03
line_length = 60  # 0.06cm are the extra extent that the antenna cannot pass
antenna_position_from_line_length_xy = (-2.1-4.5, 2.7)  # 2.1cm is the thickness of the ruler that sets the straight
# line. 4.5cm is the middle of the antenna in x position. 2,7cm is the distance to the position of the Tx. 3.6cm in

along_x_direction_line_length = np.arange(extra_lenght, line_length-extra_lenght-9+antenna_position_from_line_length_xy[1], measurement_spacing) - 30 # to be centered in origin
along_y_direction_line_length = np.flip(np.arange(extra_lenght, line_length-extra_lenght, measurement_spacing) - 30) - antenna_position_from_line_length_xy[1]
# front should be the Rx (Antenna spacing)

# The first position is 8cm from the first point
starting_point = 8
# The separation between starters is 5.5 cm (for a total of 8 points
separation_stations = 4.5
# This is referenced to the cube coordinates so I need to subtrcat 20 in both to displace to center of model
x_starting = np.asarray([i*separation_stations + starting_point for i in range(8)]) - width/2
y_starting = np.asarray([i*separation_stations + starting_point for i in range(8)]) - length/2

# To set everything with origin in the center I need to substract 30cm so I displace everything to the right location
correction_term = 30
# Initiate antenna
ant = Antenna()
orientation = [[1,0,0]]
for i, x in enumerate(x_starting):

    y_tx = along_x_direction_line_length
    x_tx = np.repeat(x, len(y_tx)) +  antenna_position_from_line_length_xy[0]#+ antenna_position_from_line_length_xy[0] - correction_term

    tx = np.c_[x_tx, y_tx, np.repeat(z_position, len(x_tx))]
    rx = tx.copy()
    rx[:, 1] += antenna_spacing
    orient = np.repeat(orientation, len(tx), axis=0)

    depth = tx[:,1]+ antenna_spacing/2

    ant.set_profile(name_profile=f'AL_00{i+1}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=depth,
                    depth_Tx=depth,
                    overwrite =True)

orientation = [[0,-1,0]]
for i, y in enumerate(y_starting):

    x_tx = along_y_direction_line_length #- antenna_position_from_line_length_xy[1]
    y_tx = np.repeat(y, len(x_tx)) + antenna_position_from_line_length_xy[0]#+ antenna_position_from_line_length_xy[0] - correction_term

    tx = np.c_[x_tx, y_tx, np.repeat(z_position, len(x_tx))]
    rx = tx.copy()
    rx[:, 0] -= antenna_spacing
    orient = np.repeat(orientation, len(tx), axis=0)

    depth = tx[:,0] - antenna_spacing/2

    ant.set_profile(name_profile=f'BL_00{i+1}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=depth,
                    depth_Tx=depth,
                    overwrite =True)


#%%
p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)

# middle_points = (ant.profiles[['Rx', 'Ry', 'Rz']].to_numpy() + ant.profiles[['Tx', 'Ty', 'Tz']].to_numpy() )/2
# p.add_object(middle_points, name='middle', color='black')
#%%

# Plot in the cube
#%%

# max_value = np.max(np.abs(time_response))
# # Now we can visualize using matplotlib
# time_response_n = time_response.T /  max_value
# fig, ax = plt.subplots(1,1, figsize=(10,8))
#
# extent = (0, time_response_n.shape[1], time_vector[-1], time_vector[0])
# vmax = 1
# cax = ax.imshow(time_response_n, aspect='auto', cmap='seismic', extent=extent, vmin=-vmax, vmax=vmax, interpolation=None)
# ax.set_xlabel('Trace number (Tx-Rx pair)')
# ax.set_ylabel('Two-way Travel-Time (ns)')
# ylims_d = (0.7, 0)
# # ylims = (300, 0)
# ylims = (max(ylims_d) * 2 /  solv.velocity, min(ylims_d) * 2 / solv.velocity)
# ax.set_ylim(ylims)
#
# ax2 = ax.twinx()
# ax2.set_ylim(ylims_d) # To account for the 2 way travel time
# ax2.set_ylabel('Radial distance (m)')
# ax2.grid()
# plt.show()

#%%

energy_all = []
energy_all_xyz = []
count = 0

# skip = ['AL_001', 'AL_002', 'AL_008', 'BL_001', 'BL002', 'BL008']
skip = []
for key, di in data.items():
    for key2, di2 in di.items():
        name = f'{key}_{key2}'
        if name in skip:
            continue
        dat = di2['data']
        t = di2['time_vector']
        de = di2['depth_vector'] * 100 # to convert to cm

        mask = (de >=0) & (de < 50)
        dat = dat[mask]
        t = t[mask]
        de = de[mask]
# for i in all_profiles:

        dat /= vmax

        df = ant.profiles.loc[ant.profiles.profile == f'{key}_{key2}']

        path = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy() )/2
        path = path[:dat.shape[1]]

        energy = np.abs(dat).max(axis=0)
        energy_all.append(energy)
        energy_all_xyz.append(path)
        count += len(dat)

        z_spacing = np.diff(de).mean()
        nsamples, ntraces = dat.shape
        points = np.repeat(path, nsamples , axis=0)

        tp = np.arange(0, z_spacing * nsamples, z_spacing)
        tp = path[:, 2][:, None] - tp

        points[:, -1] = tp.ravel()

        grid = pv.StructuredGrid()
        grid.points = points
        grid.dimensions = nsamples, ntraces, 1

        # Add the data array - note the ordering
        grid["values"] = dat.ravel(order="F")

        # all_grids[i] = (grid)

        p.add_object(grid, name=f'{key}_{key2}', cmap='seismic', scalars='values', opacity=0.9, clim=(-1, 1))


#%% Remove all profiles
for key, di in data.items():
    for key2, di2 in di.items():
        p.remove(f'{key}_{key2}')
#%%
energy_all_ = np.hstack(energy_all)
energy_all_xyz_ = np.vstack(energy_all_xyz)
energy_all_xyz_[:,2] += 20
clim = (0.06, 0.65)
p.add_object(energy_all_xyz_, name='on_surface', cmap='turbo', scalars=energy_all_)#, clim=(-1, 1))
energy_all_xyz_[:,0] +=70
p.add_object(energy_all_xyz_, name='on_surface2', cmap='turbo', scalars=energy_all_)#, clim=(-1, 1))

#%% Remove all BL


