import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain

from fracwave import FractureGeo, Antenna



#%%
# base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20231017_162905'
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20240114_124840_GypsumBlock'


# Get all the files in the folder
folders = os.listdir(base_path)

# loop through all the folders and get the files. From the filename extract the information.
# The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

# Create a dictionary to store the information
data = {'A':{},
         'B':{}}
vmax = 0
for folder in folders:
    # Get the files
    files = os.listdir(os.path.join(base_path, folder))
    for file in files:
        info = re.split('_', file)
        # identif = info[0]
        # Get the date
        # date = info[1]
        # Get the ID
        ID = info[1]
        # Get the path
        experiment_number = int(info[2])
        print(experiment_number)
        path = os.path.join(base_path, folder, file)
        # Get the data
        data_cube, header = load_sgy(path)
        # if ID == 'B':  # It is flipped
        #     data_cube = np.fliplr(data_cube)
        # Store the data in the dictionary
        # d = {experiment_number: {'header':header, 'data': data_cube}}
        # Processing
        d_de = detrend(data_cube)
        sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
        c0 = 299_792_458  # Speed of light in m/s
        rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
        velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
        d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
        # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
        data_uncut = d_de_ga.copy()
        lims = (70, 100)
        # lims = (100, 150)
        d_de_ga[:lims[0]:,:] = 0 # crop all reflections from below
        d_de_ga[lims[1]:,:] = 0 # crop all reflections from below
        time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
        depth_vector = time_vector * velocity * 0.5

        d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga, 'time_vector':time_vector, 'depth_vector':depth_vector,
             'lims': lims }
        data[ID][experiment_number] = d
        if np.abs(np.max(d_de_ga)) > vmax:
            vmax=np.abs(np.max(d_de_ga))
        # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

#%% Detrend
# d_de = detrend(dat)

#%% Remove direct wave
# d_de_svd, svd_matrix = remove_svd(d_de)
# #%%
# sf = float(header.loc['Sampling Rate [GHz]',1]) * 1e3 # To convert from GHz to MHz
# c0 = 299_792_458  # Speed of light in m/s
# rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
# velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
# d_de_ga, gain_matrix= apply_gain(d_de, sfreq=sf, gain_type='spherical', velocity=velocity)
# #%%
# from gdp.plotting import plot_frequency_information
# plot_frequency_information(d_de[:, 30], dt=1/sf)
 # No real need to do filtering


#%%
inf = data['A'][10]
lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
d = inf['data']
vmax=d.max()
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  data['A'][7]['depth_vector'] * 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
# vmax =  np.max(np.abs(d))
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           # extent=extent
           )

plt.ylabel('Distance (cm)')
# plt.ylabel('Two way travel time (ns)')
plt.colorbar()
plt.show()

#%%%%%%%%%%%% Fracture geometry
frac = FractureGeo()
width = 30
length = 30
max_element_size = 0.05  # 1 mm resolution

seed = 2021, 2022
v, f = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0, resolution=(10,10), center=(17,17,0))
surf, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)

surf = frac.create_pyvista_mesh(vertices, faces)
#%%
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d[('top_surface_m')]
bottom_surface_m = d['bottom_surface_m']
apertures = d['apertures']
spacing = [600, 600]
midpoints = d['midpoints']
apertures_s = apertures.reshape(spacing)
#%%
# apertures = apertures.reshape(spacing)
apertures = apertures[faces].mean(axis=1)
# apertures = apertures.reshape(spacing)
#%%
surf['apertures'] = apertures
p.add_object(surf, name='fracture', scalars='apertures', show_edges=False, opacity=0.5)
# p.add_object(surf, name='fracture', color='yellow', show_edges=True, opacity=0.2)

#%% Antenna positions
ant = Antenna()
separation = 3.6  # cm

thickness = 1.125  # cm
positions = []
# pi = 8 - 4.5  # starting 8cm to the right and then 2.1 sm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
pi = -0.5
positions.append(pi)
for i in range(0, 28):
    positions.append(positions[-1] + thickness)
    # if the number is even append otherwise not
    # if i % 4 == 0:
    #     positions.append(positions[-1] + 4.5 - 2.17)
    # else:
    #     positions.append(positions[-1] + 2.17)

positions = np.array(positions)

x = positions.copy()
y = positions.copy()

top = 10  # cm to top
line = np.arange(-9.73+4.5, 49.73-4.5, 0.5)
ltx = line + separation / 2
lrx = line - separation / 2
for n, i in enumerate(x):#[1:-1]):
    tx = np.zeros((len(line),3))
    tx[:,0] = i
    tx[:,1] = ltx
    tx[:,2] = top

    rx = tx.copy()
    rx[:,1] = lrx

    orient = np.zeros((len(line),3))
    orient[:,1] = 1

    ant.set_profile(name_profile=f'A_{n}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=line,
                    depth_Tx=line,
                    overwrite=True)

line2 = np.arange(34+10-0.27-4.5, 34+10-60+4.5, -0.5)
ltx2 = line2 #+ separation / 2
lrx2 = line2 #- separation / 2
for n, i in enumerate(y):#[1:-1]):
    tx = np.zeros((len(line2), 3))
    tx[:, 0] = ltx2
    tx[:, 1] = i - separation / 2
    tx[:, 2] = top

    rx = tx.copy()
    # rx[:, 0] = lrx2
    rx[:, 1] = i + separation / 2

    orient = np.zeros((len(line2), 3))
    orient[:, 0] = 1

    ant.set_profile(name_profile=f'B_{n}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=line2,
                    depth_Tx=line2,
                    overwrite=True)
ant.export_to_hdf5(file, overwrite=True)
ant.plot()
#%%
p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)

#%%
# middle_points = (ant.profiles[['Rx', 'Ry', 'Rz']].to_numpy() + ant.profiles[['Tx', 'Ty', 'Tz']].to_numpy() )/2
# p.add_object(middle_points, name='middle', color='black')
#%%

# Plot in the cube
#%%

# max_value = np.max(np.abs(time_response))
# # Now we can visualize using matplotlib
# time_response_n = time_response.T /  max_value
# fig, ax = plt.subplots(1,1, figsize=(10,8))
#
# extent = (0, time_response_n.shape[1], time_vector[-1], time_vector[0])
# vmax = 1
# cax = ax.imshow(time_response_n, aspect='auto', cmap='seismic', extent=extent, vmin=-vmax, vmax=vmax, interpolation=None)
# ax.set_xlabel('Trace number (Tx-Rx pair)')
# ax.set_ylabel('Two-way Travel-Time (ns)')
# ylims_d = (0.7, 0)
# # ylims = (300, 0)
# ylims = (max(ylims_d) * 2 /  solv.velocity, min(ylims_d) * 2 / solv.velocity)
# ax.set_ylim(ylims)
#
# ax2 = ax.twinx()
# ax2.set_ylim(ylims_d) # To account for the 2 way travel time
# ax2.set_ylabel('Radial distance (m)')
# ax2.grid()
# plt.show()

#%%

energy_all = []
energy_all_xyz = []
count = 0

# skip = ['AL_001', 'AL_002', 'AL_008', 'BL_001', 'BL002', 'BL008']
skip = []
for key, di in data.items():
    for key2, di2 in di.items():
        if key == 'A':
            continue
        name = f'{key}_{key2}'
        if name in skip:
            continue
        dat = di2['data']
        t = di2['time_vector']
        de = di2['depth_vector'] * 100 # to convert to cm

        # mask = (de >=0) & (de < 50)
#         mask = (t >=2) & (t < 4.5)
#         dat = dat[mask]
#         t = t[mask]
#         de = de[mask]
# # for i in all_profiles:
#
#         dat /= vmax

        df = ant.profiles.loc[ant.profiles.profile == name]
        print(name, len(df))
        patha = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy() )/2
        end = dat.shape[1]-5
        patha = patha[:end]

        energy = np.abs(dat).max(axis=0)[:end]

        # if key == 'A' and key2 == 10:
        #     plt.plot(patha[:,1], energy, '.')
        #     plt.show()
        #     break

        energy_all.append(energy)
        energy_all_xyz.append(patha)
        assert len(energy) == len(patha), name
        # count += len(dat)

        # z_spacing = np.diff(de).mean()
        # nsamples, ntraces = dat.shape
        # points = np.repeat(path, nsamples , axis=0)

        # tp = np.arange(0, z_spacing * nsamples, z_spacing)
        # tp = path[:, 2][:, None] - tp
        #
        # points[:, -1] = tp.ravel()
        #
        # grid = pv.StructuredGrid()
        # grid.points = points
        # grid.dimensions = nsamples, ntraces, 1
        #
        # # Add the data array - note the ordering
        # grid["values"] = dat.ravel(order="F")
        #
        # # all_grids[i] = (grid)
        #
        # p.add_object(grid, name=f'{key}_{key2}', cmap='seismic', scalars='values', opacity=0.9, clim=(-1, 1))


#%% Remove all profiles
for key, di in data.items():
    for key2, di2 in di.items():
        p.remove(f'{key}_{key2}')
#%%
p.remove_scalar_bar()
energy_all_ = np.hstack(energy_all)
energy_all_xyz_ = np.vstack(energy_all_xyz)
energy_all_xyz_[:,2] += .20
# clim = (5.66e-09, 3.62e-07)
p.add_object(energy_all_xyz_, name='on_surface', cmap='turbo', scalars=energy_all_)#, clim=clim)
energy_all_xyz_[:,0] +=.70
p.add_object(energy_all_xyz_, name='on_surface2', cmap='turbo', scalars=energy_all_)#, clim=clim)

#%% Remove all BL
import pyvista as pv
pcd = pv.PolyData(np.hstack([energy_all_xyz_[:, 0],
            energy_all_xyz_[:, 1],
            energy_all_]))

#%%
plt.tripcolor(energy_all_xyz_[:, 0],
            energy_all_xyz_[:, 1],
            energy_all_, )
plt.show()

#%%
plt.imshow(apertures_s.reshape((600,600)), aspect='auto')
plt.show()
