from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR
from fracwave.utils.help_decorators import convert_meter, convert_frequency
import numpy as np
import matplotlib.pyplot as plt
import pyvistaqt as pvqt
import pyvista as pv
import matplotlib.pyplot as plt

file = OUTPUT_DIR + 'varying_conductivity.h5'

#%%
try:
    sou = SourceEM()
    sou.load_hdf5(file)
    fig = sou.plot_waveforms_zoom()
    fig.show()
except:
    sou = SourceEM()
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 599))
    sou.set_source_params(a=6.9,
                          c=1,
                          loc=0,
                          scale=0.5,
                          ph=0,
                          t=1)
    sou.create_source()
    fig = sou.plot_waveforms_complete()
    fig.show()
    fig = sou.plot_waveforms_zoom()
    fig.show()
    sou.export_to_hdf5(file, overwrite=True)
# sou.widgets().show()
# We will define that the maximum element size is a fourth of the wavelength from our source function
c0 = 299_792_458  # Speed of light in m/s
rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)  # To convert in m/ns
max_element_size = velocity / (sou.center_frequency * 5)
print(f'Element max size = {convert_meter(max_element_size)}')

#%%% Now the antennas
try:
    ant = Antenna()
    ant.load_hdf5(file)
except:
    ant = Antenna()
    separation = 3.6e-2  # cm

    x = np.arange(-0.4,0.41, 0.1)
    y = np.arange(-0.4,0.41, 0.1)
    # XX, YY = np.meshgrid(x,y)

    top = 0.3
    line = np.arange(-0.4, 0.41, 0.1)
    ltx = line + separation / 2
    lrx = line - separation / 2
    for i in x[1:-1]:
        tx = np.zeros((len(line),3))
        tx[:,0] = i
        tx[:,1] = ltx
        tx[:,2] = top

        rx = tx.copy()
        rx[:,1] = lrx

        orient = np.zeros((len(line),3))
        orient[:,1] = 1

        ant.set_profile(name_profile=f'x_{i}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line,
                        depth_Tx=line,
                        overwrite=True)

    for i in y[1:-1]:
        tx = np.zeros((len(line), 3))
        tx[:, 0] = ltx
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = lrx

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'y_{i}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line,
                        depth_Tx=line,
                        overwrite=True)
    ant.export_to_hdf5(file, overwrite=True)
ant.plot()
#%% Distance
# conductivities = np.linspace(0,3, 20)  # 0 to 3 S/m
conductivities = [0, 0.138, 0.312, 0.620, 0.755, 1.2, 1.64] # S/m
apertures = [1e-3, 2e-3, 4e-3, 6e-3, 8e-3, 10e-3]  # 1 micrometer to 10mm
# apertures = np.linspace(1e-6,1e-2, 20)  # 1 micrometer to 10mm

try:
    frac = FractureGeo()
    frac.load_hdf5(file)
except:
    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=0.6, length=0.6, dip=0, azimuth=0, resolution=(10,10), center=(0,0,0))
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)

    for i, cond in enumerate(conductivities):
        for j, apert in enumerate(apertures):
            kwargs_electric_properties = dict(aperture=apert,  # 10 mm
                                              electrical_conductivity=cond, # in S/m
                                              electrical_permeability=1)
            frac.set_fracture(name_fracture=f'aper:{apert}-cond:{cond}-',
                              vertices=vertices,
                              faces=faces,
                              overwrite=True,
                              **kwargs_electric_properties)

    frac.export_to_hdf5(file, overwrite=True)

surf = frac.get_surface(frac.name_fractures[-1])
surf.plot(scalars='aperture')

#%%
try:
    solv = FracEM()
    solv.load_hdf5(file)
    freq = solv.get_freq()
except:
    solv = FracEM()
    solv.mode = 'reflection'
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 8.4   # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'
    solv.open_file(file)
    solv.time_zero_correction()

    freq = solv.forward_pass(overwrite=True)

#%%
solv.name_fractures
solv.name_profiles
freq = solv.get_freq()#name_fracture='aper:0.01-cond:0-', name_profile='x_0.09999999999999987')
time_response, time_vector = solv.get_ifft(freq)
#%%
plt.plot(time_vector, time_response[0]);plt.show()
#%%
plt.imshow(time_response, cmap='RdBu', aspect='auto', interpolation='None')
plt.show()

#%%%% Let's calculate the reflection coefficients
solv = FracEM()
solv.backend = 'numpy'
theta = np.deg2rad(7.5)
freq = np.asarray([25e6, 50e6, 100e6, 200e6, 250e6, 500e6, 1000e6, 3e9])
omega = 2 * np.pi * freq

epsilon_rock = 7  # 8.4
sigma_rock = 0.01e-3

epsilon_fracture = 81
conductivities = np.linspace(0, 3, 50)  # 0 to 3 S/m

k1 = solv.wavenumber(omega, epsilon_rock, sigma_rock)
apertures = np.linspace(1e-9, 10e-3, 50)  # 1 micrometer to 10mm

x, y = np.meshgrid(conductivities, apertures)
reflections = np.zeros((len(apertures), len(conductivities), len(omega)))
phase = np.zeros((len(apertures), len(conductivities), len(omega)))
for j, cond in enumerate(conductivities):
    k2 = solv.wavenumber(omega, epsilon_fracture, cond)
    for i, aper in enumerate(apertures):
        val = solv.reflection_all(k1=k1, k2=k2, theta=theta, aperture=aper)
        phase[i, j] = np.unwrap(np.angle(val, deg=True), period=360)
        # phase[i, j] = np.angle(val, deg=True)
        reflections[i, j] = np.abs(val)
# phase = np.unwrap(phase, period=360)

fig = plt.figure(figsize=(10, 30))
for k, fr in enumerate(freq):
    ax = fig.add_subplot(len(freq), 1, k + 1, projection='3d', )
    # cax =ax.plot_surface(x, y, abs(reflections[...,k]), cmap='turbo', )
    cax = ax.plot_surface(x, y, reflections[..., k], cmap='turbo', )
    ax.set_xlabel('Conductivity (S/m)')
    ax.set_ylabel('Aperture (m)')
    ax.set_zlabel('Reflection magnituude')

    ax.view_init(elev=20., azim=225)
    ax.xaxis.labelpad = 20
    ax.yaxis.labelpad = 20
    ax.zaxis.labelpad = 20
    ax.set_title(f'{convert_frequency(fr)}')
    # ax.set_zlim(0, 1)
    # plt.tight_layout()
    fig.colorbar(cax, ax=ax)
plt.show()
#%%
fig = plt.figure(figsize=(10, 30))
for k, fr in enumerate(freq):
    ax = fig.add_subplot(len(freq), 1, k + 1, projection='3d')

    # cax =ax.scatter(XX, YY, reflections.flatten(), c=reflections.flatten(), cmap='turbo', marker='o')
    cax = ax.plot_surface(x, y, phase[..., k], cmap='turbo')
    ax.set_xlabel('Conductivity (S/m)')
    ax.set_ylabel('Aperture (m)')
    ax.set_zlabel('Reflection magnituude')

    ax.view_init(elev=20., azim=225)
    ax.xaxis.labelpad = 20
    ax.yaxis.labelpad = 20
    ax.zaxis.labelpad = 20
    ax.set_title(f'{convert_frequency(fr)}')
    # plt.tight_layout()
    fig.colorbar(cax, ax=ax)
plt.show()
#%%
fig = plt.figure(figsize=(10,30))
for k, fr in enumerate(freq):
    ax = fig.add_subplot(len(freq), 1, k+1, projection='3d')

    # cax =ax.scatter(XX, YY, reflections.flatten(), c=reflections.flatten(), cmap='turbo', marker='o')
    cax =ax.plot_surface(x, y, phase[...,k], cmap='turbo')
    ax.set_xlabel('Conductivity (S/m)')
    ax.set_ylabel('Aperture (m)')
    ax.set_zlabel('Reflection magnituude')

    ax.view_init(elev=20., azim=225)
    ax.xaxis.labelpad=20
    ax.yaxis.labelpad=20
    ax.zaxis.labelpad=20
    ax.set_title(f'{convert_frequency(fr)}')
    # plt.tight_layout()
    fig.colorbar(cax, ax=ax)
plt.show()


# index =18
# [plt.plot(np.abs(reflections)[index,:, i]) for i in range(len(freq))]
# plt.show()
#%%
for i, aper in enumerate(apertures):
    for f,fr in enumerate(freq):
        plt.plot(conductivities, reflections[i,:,f], label=convert_frequency(fr))
    plt.xlabel('Conductivity (S/m)')
    plt.ylabel('Reflection Coefficient')
    plt.title(convert_meter(aper))
    plt.legend()
    plt.show()

#%%
for i, cond in enumerate(conductivities):
    for f,fr in enumerate(freq):
        plt.plot(apertures, reflections[:,i,f], label=convert_frequency(fr))
    plt.xlabel('Aperture (m)')
    plt.ylabel('Reflection Coefficient')
    plt.title(f'{cond} S/m')
    plt.legend()
    plt.show()

#%%
for i, aper in enumerate(apertures):
    for f,fr in enumerate(freq):
        plt.plot(conductivities, phase[i,:,f], label=convert_frequency(fr))
    plt.xlabel('Conductivity (S/m)')
    plt.ylabel('Phase')
    plt.title(convert_meter(aper))
    plt.legend()
    plt.show()


# #%%
# file = OUTPUT_DIR + 'test_relection_geometry_multiple_dip_and_azimuth.h5'
# ant = Antenna()
# ant.set_profile(name_profile='single',
#                 transmitters=[[0,0,0]],
#                 receivers=[[0,0,0]],
#                 orient_receivers=[[0,0,1]],
#                 orient_transmitters=[[0,0,1]],
#                 overwrite=True)
#
# ant.export_to_hdf5(file, overwrite=True)
# # ant.plot(backend='mpl', show=True)
#
# sou = SourceEM()
# sou.set_time_vector(np.linspace(0, 140, 1001))
# source_parameters = [0.4857619,
#                      2.03456272,
#                      0,
#                      0.14400915,
#                      0,
#                      10.01662033]
# sou.set_source_params(a=source_parameters[0],
#                       c=source_parameters[1],
#                       loc=source_parameters[2],
#                       scale=source_parameters[3],
#                       ph=source_parameters[4],
#                       t=source_parameters[5])
# sou.create_source()
# sou.set_center_frequency(0.1)
# sou.export_to_hdf5(file, overwrite=True)
#
# from fracwave.utils.help_decorators import convert_meter
# max_element_size = 0.1
# print(f'Element max size = {convert_meter(max_element_size)}')
#
# azimuth = np.arange(0,180.1, 15)
# dip = np.arange(0, 180.1, 10)
#
# frac = FractureGeo()
# for i, dip_i in enumerate(dip):
#     for j, azimuth_j in enumerate(azimuth):
#
#         v, f = frac.create_regular_squared_mesh(width=0.1,
#                                                 length=0.1,
#                                                 resolution=[2,2],
#                                                 dip=dip_i,
#                                                 azimuth=azimuth_j,
#                                                 center=((0,2,0)))
#         kwarg_properties = dict(aperture=0.01,
#                                 electrical_conductivity=0,
#                                 electrical_permeability=81)
#         frac.set_fracture(name_fracture=f'azm:{azimuth_j}-dip:{dip_i}-',
#                           vertices=v,
#                           faces=f,
#                           **kwarg_properties)
# # frac.plot_geometry()
# frac.export_to_hdf5(file, overwrite=True)
# #%%
# solv = FracEM()
# solv.mode = 'reflection'
# solv._propagation_mode = 2
# solv._fast_calculation_incoming_field = False
# solv.rock_epsilon = 5  # Electrical permittivity of the medium
# solv.rock_sigma = 0  # Electrical conductivity of the medium
# solv.backend = 'numpy'
# solv.engine = 'tensor_trace'
#
# solv.open_file(file)
# freq = solv.forward_pass(mask_frequencies=False, overwrite=True)
#
# #%%
# indiv_fracture = solv.file_read('simulation/individual_fracture_response', sl=0)
# # time_response, time_vector = solv.get_ifft(indiv_fracture)
# time_response, time_vector = solv.get_ifft(indiv_fracture)
#
# # [plt.plot(time_vector, time_response[i]) for i in range(len(time_response))]
# # plt.show()
# # max_amplitude =  np.abs(indiv_fracture.numpy()).max(1)
# max_amplitude = np.abs(time_response).max(1)
# energy = np.zeros((len(dip), len(azimuth)))
# for i, dip_i in enumerate(dip):
#     for j, azimuth_j in enumerate(azimuth):
#         energy[i,j] = max_amplitude[i*len(azimuth)+j]
#
# extent=(azimuth.min(), azimuth.max(), dip.max(), dip.min())
# plt.imshow(energy, aspect='auto', extent=extent)
# plt.xlabel('Azimuth')
# plt.ylabel('dip')
# plt.title('Reflected energy at different fracture orientations')
# plt.colorbar()
# plt.show()