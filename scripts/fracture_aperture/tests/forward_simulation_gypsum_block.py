import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM

file_h5 = OUTPUT_DIR + 'gypsum_block_5mm.h5'



#%%
# base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20231017_162905'
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20240114_124840_GypsumBlock'


# Get all the files in the folder
folders = os.listdir(base_path)

# loop through all the folders and get the files. From the filename extract the information.
# The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

# Create a dictionary to store the information
data = {'A':{},
         'B':{}}
vmax = 0
for folder in folders:
    # Get the files
    files = os.listdir(os.path.join(base_path, folder))
    for file in files:
        info = re.split('_', file)
        # identif = info[0]
        # Get the date
        # date = info[1]
        # Get the ID
        ID = info[1]
        # Get the path
        experiment_number = int(info[2])
        print(experiment_number)
        path = os.path.join(base_path, folder, file)
        # Get the data
        data_cube, header = load_sgy(path)
        # if ID == 'B':  # It is flipped
        #     data_cube = np.fliplr(data_cube)
        # Store the data in the dictionary
        # d = {experiment_number: {'header':header, 'data': data_cube}}
        # Processing
        d_de = detrend(data_cube)
        sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
        c0 = 299_792_458  # Speed of light in m/s
        rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
        velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
        d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
        # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
        data_uncut = d_de_ga.copy()
        lims = (70, 100)
        d_de_ga[:lims[0]:,:] = 0 # crop all reflections from below
        d_de_ga[lims[1]:,:] = 0 # crop all reflections from below
        time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
        depth_vector = time_vector * velocity * 0.5

        d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga, 'time_vector':time_vector, 'depth_vector':depth_vector,
             'lims': lims }
        data[ID][experiment_number] = d
        if np.abs(np.max(d_de_ga)) > vmax:
            vmax=np.abs(np.max(d_de_ga))
        # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

#%% Detrend
# d_de = detrend(dat)

#%% Remove direct wave
# d_de_svd, svd_matrix = remove_svd(d_de)
# #%%
# sf = float(header.loc['Sampling Rate [GHz]',1]) * 1e3 # To convert from GHz to MHz
# c0 = 299_792_458  # Speed of light in m/s
# rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
# velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
# d_de_ga, gain_matrix= apply_gain(d_de, sfreq=sf, gain_type='spherical', velocity=velocity)
# #%%
# from gdp.plotting import plot_frequency_information
# plot_frequency_information(d_de[:, 30], dt=1/sf)
 # No real need to do filtering


#%%
inf = data['A'][10]
lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
d = inf['data_uncut']
vmax=d.max()
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  data['A'][7]['depth_vector'] * 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
# vmax =  np.max(np.abs(d))
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           # extent=extent
           )

plt.ylabel('Distance (cm)')
# plt.ylabel('Two way travel time (ns)')
plt.colorbar()
plt.show()


#%% Source

sou = SourceEM()
sou.type = 'ricker'
sou.set_time_vector(np.linspace(0, 15, 599))
sou.set_center_frequency(cf=3)
sou.set_delay(delay=1)
sou.set_source_params(a=15,
                  c=1,
                  loc=0,
                  scale=0.2)
sou.create_source()
# sou.widgets().show()

fig = sou.plot_waveforms_complete()
fig.show()
sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency/ 5)
#%%%%%%%%%%%% Fracture geometry
frac = FractureGeo()

width = 0.30
length = 0.30
# max_element_size = 0.05  # 1 mm resolution

v, f = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0, resolution=(10,10), center=(.17,.17,0))
surf, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)

surf = frac.create_pyvista_mesh(vertices, faces)

resolution = (37,37)

#%%
p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)
#%%
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints = d['midpoints']

apertures_original = apertures_l.reshape(spacing)
#%%
scaling_factors = (np.array(resolution) / spacing).tolist()
from scipy.ndimage import zoom
# Perform the downsampling using zoom
downsampled_image = zoom(apertures_original, scaling_factors)
scaling_factors_up = (spacing / np.array(resolution)).tolist()
upsampled_image = zoom(downsampled_image, scaling_factors_up)

#%%
fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
cax1 = ax1.imshow(apertures_original, cmap='viridis')
cax2 = ax2.imshow(downsampled_image, cmap='viridis')
cax3 = ax3.imshow(upsampled_image, cmap='viridis')
cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
fig.colorbar(cax1, ax=ax1)
fig.colorbar(cax2, ax=ax2)
fig.colorbar(cax3, ax=ax3)
fig.colorbar(cax4, ax=ax4)
ax1.set_title('Original')
ax2.set_title('Downsampled')
ax3.set_title('Upsampled')
ax4.set_title('Original-Upsampled')
fig.show()
#%%

apertures = downsampled_image.ravel()[faces].mean(axis=1)
# apertures = apertures.reshape(spacing)

#%%
surf['apertures'] = apertures
p.add_object(surf, name='fracture', scalars='apertures', show_edges=False, opacity=0.5)

#%%
kwarg_properties = dict(aperture=apertures,
                            electrical_conductivity=0,
                            electrical_permeability=1,
                        plane=2,
                        overwrite=True)
frac.set_fracture(name_fracture='fracture',
                  vertices=vertices,
                  faces=faces,
                  **kwarg_properties)

frac.export_to_hdf5(file_h5, overwrite=True)
#%% Antenna positions
ant = Antenna()
try:
    raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1.125 * 1e-2 # cm
    positions = []
    pi = (8 - 4.5) * 1e-2   # starting 8cm to the right and then 2.1 sm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)
        # if the number is even append otherwise not
        # if i % 4 == 0:
        #     positions.append(positions[-1] + 4.5 - 2.17)
        # else:
        #     positions.append(positions[-1] + 2.17)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2# cm to top

    # Scan from bottom to top
    line = np.arange(0.27 + 4.5 - 10, 34 + 16 - 0.27 - 4.5, 0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])
    ltx = line + separation / 2
    lrx = line - separation / 2
    for n, i in enumerate(x):#[1:-1]):
        tx = np.zeros((len(line),3))
        tx[:,0] = i
        tx[:,1] = ltx
        tx[:,2] = top

        rx = tx.copy()
        rx[:,1] = lrx

        orient = np.zeros((len(line),3))
        orient[:,1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line,
                        depth_Tx=line,
                        overwrite=True)

    # Scan left to right
    # line = np.arange(0.27 + 4.5 - 10, 34 + 16 - 0.27 - 4.5,
    #                  0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    line2 = np.arange(34+16-0.27-4.5, 0.27 + 4.5 - 10, -0.5) * 1e-2 # (Starting point = extent block[34] + short end grid [16] - offset pin [0.27] - midpoint antenna [4.5],  end point = Offset pin + midpoint antenna [4.5] - long end grid [10])

    ltx2 = line2 #+ separation / 2
    lrx2 = line2 #- separation / 2
    for n, i in enumerate(y):#[1:-1]):
        tx = np.zeros((len(line2), 3))
        tx[:, 0] = ltx2
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        # rx[:, 0] = lrx2
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line2), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                        receivers=rx,
                        transmitters=tx,
                        orient_receivers=orient,
                        orient_transmitters=orient,
                        depth_Rx=line2,
                        depth_Tx=line2,
                        overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
    # ant.plot()

#%%
p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)
#%%
p.remove('rx')
p.remove('tx')
middle_points = (ant.Receiver + ant.Transmitter)*0.5
p.add_object(middle_points, name='mid', color='black', opacity=0.2)

#%%
solv = FracEM()
solv.open_file(file_h5)
solv._fast_calculation_incoming_field = False
solv.backend = 'torch'
solv.rock_epsilon = 6.5  # Electrical permittivity of the medium
solv.rock_sigma = 0  # Electrical conductivity of the medium
solv._mask_frequencies = False
#%%
freq = solv.forward_pass()
#%%
file_result = os.path.abspath('/output/manuscript2/gypsum_block_1cm.h5')
solv = FracEM()
solv.load_hdf5(filename=file_result, read_only=True)

# solv.forward_pass()
#%%
profiles = {}
for n in ant.name_profiles:
    freq = solv.get_freq(name_profile=n)

    data, time_vector = solv.get_ifft(freq)

    profiles[n] = {'data': data, 'time_vector': time_vector}

# solv.get_ifft()
import pickle
pickle_result = os.path.abspath('/output/manuscript2/gypsum_block_1cm_output_simulation.pickle')
with open(pickle_result, 'wb') as handle:
    pickle.dump(profiles, handle, protocol=pickle.HIGHEST_PROTOCOL)
#%%