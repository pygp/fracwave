import re
import numpy as np
import os
import matplotlib.pyplot as plt

from bedretto import model

p = model.ModelPlot()

from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend

from fracwave import Antenna, SourceEM, OUTPUT_DIR

file_h5 = OUTPUT_DIR + 'fracture_aperture/source_gypsum_block.h5'

#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5.split('.')[0] + '_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%%
p.add_object(ant.get_midpoints(), color='black', name='ant')

#%%
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135/'

# data_measu = {}
data_together = {}

folders = os.listdir(base_path)
vmax=0
i =0


data_measu = {'A':{},  # Up direction (0,1,0)
              # 'B':{},  # Right dir (1,0,0)
              # 'C':{},  # Up dir (1,0,0)
              'D':{}}  # Right dir (0,1,0)

# We are cropping now all the traces we don't want
for folder in folders:
    # Get the files
    files = os.listdir(os.path.join(base_path, folder))
    for file in files:
        if '.sgy' not in file:
            continue
        info = re.split('_', file)
        # # identif = info[0]
        # # Get the date
        # # date = info[1]
        # # Get the ID
        ID = info[0]
        # Get the path
        # experiment_number = int(info[2])
        experiment_number = int(info[1])
        n_e_id = ID+'_'+str(experiment_number)
        midpoints = ant.get_midpoints(n_e_id)
        out =[7, 24]
        if experiment_number < out[0] or experiment_number > out[1]:
            # print('no')
            continue
        else:
            print(experiment_number)
        path = os.path.join(base_path, folder, file)
        # Get the data
        data_cube, header = load_sgy(path)

        # if ID =/= 'B':  # It is flipped
            # data_cube = np.fliplr(data_cube)
        # Store the data in the dictionary
        # d = {experiment_number: {'header':header, 'data': data_cube}}
        # Processing
        d_de = detrend(data_cube)
        sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
        c0 = 299_792_458  # Speed of light in m/s
        rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
        velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
        # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
        # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
        d_de_ga = d_de.copy()
        data_uncut = d_de_ga.copy()
        if ID in ['B', 'D']:
            lims_x = (10, 85)
            ma = (midpoints[:, 0] >= out[0] * 0.01) & (midpoints[:, 0] <= out[1] * 0.01)
        elif ID in ['A', 'C']:
            lims_x = (10, 82)
            ma = (midpoints[:, 1] >= out[0] * 0.01) & (midpoints[:, 1] <= out[1] * 0.01)
        else:
            raise AttributeError
        mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
        if len(ma) > len(mask_x):
            mask_x = ma[:len(mask_x)]
        else:
            mask_x[:len(ma)] = ma

# lims_y = (80, 100)
        # lims_x = (10, 85)
        lims_y = (75, 110)
        d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
        d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
        d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from below
        d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from below
        time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
        depth_vector = time_vector * velocity * 0.5
        vma = np.abs(d_de_ga).max()
        if vma > vmax: vmax=vma
        d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
             'time_vector':time_vector,
             'depth_vector':depth_vector,
             'lims_x': lims_x, 'lims_y': lims_y,
             'data_cut': data_uncut[:, mask_x],
             # 'time_vector_cut': time_vector[mask_x],
             # 'depth_vector_cut': depth_vector[mask_x],
             }
        data_measu[ID][experiment_number] = d
        if np.abs(np.max(d_de_ga)) > vmax:
            vmax=np.abs(np.max(d_de_ga))

    # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

           #  #%%%5
           #  from impdar.lib.RadarData import RadarData, RadarFlags
           #  imp_data = RadarData(None)
           #  imp_data.fn = path
           #
           #
           #  imp_data.dt = 1/float(header[1]['Sampling Rate [GHz]'])
           #  imp_data.data = data_cube
           #
           #  # Remove pretrigger
           #  # trig_threshold = 0.5  # trigger when mean trace gets up to 50% of maximum
           #  # mean_trace = np.nanmean(np.abs(h5_data.data), axis=1)
           #  # idx_threshold = np.argwhere(mean_trace > trig_threshold * np.nanmax(mean_trace))
           #  # idx_trig = np.nanmin(idx_threshold)
           #  # h5_data.data = h5_data.data[idx_trig:]
           #
           #  # other variables are from the array shape
           # imp_data.snum = imp_data.data.shape[0]
           # imp_data.tnum = imp_data.data.shape[1]
           # imp_data.trace_num = np.arange(imp_data.data.shape[1]) + 1
           # imp_data.trig_level = np.zeros((imp_data.tnum,))
           # imp_data.pressure = np.zeros((imp_data.tnum,))
           # imp_data.flags = RadarFlags()
           # imp_data.travel_time = imp_data.dt * 1.0e6 * np.arange(imp_data.snum)
           # imp_data.trig = np.zeros((imp_data.tnum,))
           # imp_data.lat = np.zeros((imp_data.tnum,))
           # imp_data.long = np.zeros((imp_data.tnum,))
           # imp_data.x_coord = np.zeros((imp_data.tnum,))
           # imp_data.y_coord = np.zeros((imp_data.tnum,))
           # imp_data.elev = np.zeros((imp_data.tnum,))
           # imp_data.decday = np.arange(imp_data.tnum)
           # imp_data.trace_int = np.ones((imp_data.tnum,))
           #
           #  imp_data.dist = np.arange(imp_data.tnum) / 1.0e3
           #  imp_data.chan = -99.
           #  imp_data.check_attrs()
           #
           #  from impdar.lib.plot import plot_traces, plot_radargram
           #  plot_radargram(imp_data)
    #
    # data_together[key] = {'AC':{},
    #                       'BD':{}}
    #
    # max_amount_traces = 86
    # for i in range(28):
    #     print(i)
    #     da = data_measu[key]['A'][i]
    #     dc = data_measu[key]['C'][i]
    #     db = data_measu[key]['B'][i]
    #     dd = data_measu[key]['D'][i]
    #
    #     dac ={'header': da['header'],
    #         'data_raw': (da['data_raw'][:,:max_amount_traces]+dc['data_raw'][:,:max_amount_traces])/vmax ,
    #         'data_uncut':(da['data_uncut'][:,:max_amount_traces] + dc['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (da['data'][:,:max_amount_traces]+dc['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':da['time_vector'],
    #         'depth_vector':da['depth_vector'],
    #         'lims_x': (da['lims_x'][0], max_amount_traces),
    #         'lims_y': da['lims_y']}
    #
    #     dbd ={'header': db['header'],
    #         'data_raw': (db['data_raw'][:,:max_amount_traces]+dd['data_raw'][:,:max_amount_traces])/vmax,
    #         'data_uncut':(db['data_uncut'][:,:max_amount_traces] + dd['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (db['data'][:,:max_amount_traces]+dd['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':db['time_vector'],
    #         'depth_vector':db['depth_vector'],
    #         'lims_x': (db['lims_x'][0],max_amount_traces),
    #         'lims_y': db['lims_y']}
    #
    #     data_together[key]['AC'][i] = dac
    #     data_together[key]['BD'][i] = dbd

# #%%
# from impdar.lib import load
# dat = load.load('mat','data/synthetic_radargram.mat')[0]

#%%
l = ['A_'+str(a) for a in list(data_measu['A'].keys())]
l += ['D_'+str(a) for a in list(data_measu['D'].keys())]

#%%
inf = data_measu['A'][15]
# lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
d = inf['data_cut']
# d = inf['data_uncut']
# from scipy.io import savemat
# savemat(OUTPUT_DIR+'AC_0mm_10', inf)
# vmax=d.max() * 0.05
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  inf['time_vector'] #* 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
vmax =  np.max(np.abs(d))
plt.figure()
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           # extent=extent
           )

# plt.ylabel('Distance (cm)')
plt.ylabel('Two way travel time (ns)')
# plt.ylim(2.7,1.5)
# plt.ylim(4,1)
# plt.xlim(20,80)
plt.colorbar()
plt.show()
#%%
from gdp.plotting import plot_frequency_information

crop_from = slice(60,200)
trace = 15
tr = d[crop_from, trace]
# ax = plot_frequency_information(d[200:300,20], 1/float(header.loc['Sampling Rate [GHz]', 1]))
ax = plot_frequency_information(tr, 1/float(header.loc['Sampling Rate [GHz]', 1]))

#%%
sou = SourceEM()
sou.type = 'generalgamma'
freq = []
val = []
f_big = []
for key in data_measu.keys():
    for ID in data_measu[key].keys():
        inf = data_measu[key][ID]
        dc = inf['data_cut']#[crop_from]
        t_vec = inf['time_vector']#[crop_from]
        t_vec -= t_vec[0]
        f = np.zeros((dc.shape[0]//2 + 1, dc.shape[1]))

        for i, d in enumerate(dc.T):
            if np.allclose(d, 0):
                continue
            info, (waveform_time, waveform_freq, source_freq) = sou.from_time_to_frequency(t=t_vec, source_time=d)
            real_norm = np.abs(source_freq) / np.abs(source_freq).max()
            f[:, i] = real_norm
            freq.append(info['frequency'])
            val.append(real_norm)
        f_big.append(f)
        data_measu[key][ID]['source_freq'] = f
        data_measu[key][ID]['info'] = info
        #
        # extent = (0, f.shape[1], info['frequency'][-1], 0)
        # plt.figure()
        # plt.imshow(np.abs(f), aspect='auto', extent=extent)
        # plt.xlabel('Traces')
        # plt.ylabel('Frequency (GHz)')
        # plt.show()
        #
        # plt.figure()
        # plt.plot(np.abs(f)[:,20])
        # plt.show()
        #
        # plt.figure()
        # plt.plot(np.abs(f))
        # plt.show()
        #
        # raise AttributeError

# plt.figure();plt.plot(info['time'], waveform_time);plt.show()
#
# plt.figure();plt.plot(info['frequency'], np.abs(source_freq));plt.show()
plt.figure()
plt.scatter(freq, val)
plt.plot(info['frequency'], np.hstack(f_big).mean(axis=1), color='red')
plt.show()

sou.set_frequency_vector(info['frequency'])
sou.set_delay(1)
#%%
real_norm = np.hstack(f_big).mean(axis=1)
real_norm /= real_norm.max()

def opt_source(x):
    """
        Optimization function
        Args:
            x: vector to optimize. Parameters of source function
        Returns:

        """
    sou.set_source_params(a=x[0], c=x[1], loc=x[2], scale=x[3], ph=x[4])
    source_in = sou.create_source()
    op = np.sqrt(np.sum(np.square(np.abs(source_in) - real_norm)))
    print(op)
    return op

x = [9,  # a
     0.2,  # c
     0,  # loc
     0.2,  # scale
     0,  # ph
     ]
bounds = [(0.5, 15),
          (0.5, 1.5),
          (0, 0),
          (0.01, 1),
          (0, 0),
          ]

from scipy import optimize

res = optimize.minimize(opt_source,
                        x,
                        bounds=bounds,
                        method='nelder-mead',
                        options={#'maxfun': 10000,
                                 'xatol': 1e-6,
                                 'disp': True})

print(res.x)
# array([9.17809204 0.95575954 0.         0.28381535 0.  ])  Fitting one
# [11.74609167  0.84948015  0.          0.15794249  0.        ]
# Gypsum fit : 14.99999994,  0.68261749,  0.        ,  0.05180656,  0.
sou.set_source_params(a=res.x[0], c=res.x[1], loc=res.x[2], scale=res.x[3], ph=res.x[4])

sou.create_source()
fig, ax = plt.subplots()
ax.plot(sou.frequency, real_norm)
ax.plot(sou.frequency, np.abs(sou.source))
ax.set_xlim(0, 7.5)
ax.set_xlabel('Frequency (GHz)')
ax.set_ylabel('Amplitude (-)')
ax.grid()
plt.show()

sou.plot_waveforms_complete().show()
sou.plot_waveforms_zoom().show()


#%% Source
sou = SourceEM()
sou.type = 'generalgamma'
sou.set_time_vector(np.linspace(0, 15, 655))

sou.set_delay(delay=1)
sou.set_source_params(a=14.99999994,
                      c=0.68261749,
                      loc=0,
                      scale=0.05180656)

# sou.set_source_params(a=11.74609167,
#                       c=0.84948015,
#                       loc=0,
#                       scale=0.15794249)
sou.create_source()
# sou.widgets().show()

fig = sou.plot_waveforms_zoom()
# fig = sou.plot_waveforms_complete()
fig.show()
sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5


