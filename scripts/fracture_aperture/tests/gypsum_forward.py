import re
import numpy as np
import os
import matplotlib.pyplot as plt
import pyvista as pv
from bedretto import model
p = model.ModelPlot()
from gdp.import_export.import_data import load_sgy

from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain
from gdp.processing.time_lapse import hilbert_phase_difference

from fracwave import FractureGeo, Antenna, SourceEM, OUTPUT_DIR, FracEM

file_h5 = OUTPUT_DIR + 'fracture_aperture/gypsum_block.h5'

#%% Source
sou = SourceEM()
try:
    sou.load_hdf5(file_h5)
except:
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 655))

    sou.set_delay(delay=1)
    # sou.set_source_params(a=9.17809204,
    #                       c=0.95575954,
    #                       loc=0,
    #                       scale=0.28381535)
    sou.set_source_params(a=14.99999994,
                          c=0.68261749,
                          loc=0,
                          scale=0.05180656)
    sou.create_source()
    # sou.widgets().show()

    # fig = sou.plot_waveforms_zoom()
    fig = sou.plot_waveforms_complete()
    fig.show()
    sou.export_to_hdf5(file_h5, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = (0.1234 / sou.center_frequency)/ 5

#%% We need to focus only in a small section
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    separation = 3.6 * 1e-2  # cm

    thickness = 1 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 cm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 10 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.25 + 4.5 - 10, 34 + 16 - 0.25 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    for n, i in enumerate(x):  # [1:-1]):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 1] = line - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'A_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = i + separation / 2
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = i - separation / 2
        rx[:, 1] = line

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'C_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

    for n, i in enumerate(y):
        tx = np.zeros((len(line), 3))
        tx[:, 0] = line + separation / 2
        tx[:, 1] = i
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line - separation / 2
        rx[:, 1] = i

        orient = np.zeros((len(line), 3))
        orient[:, 0] = 1

        ant.set_profile(name_profile=f'B_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)

        tx = np.zeros((len(line), 3))
        tx[:, 0] = line
        tx[:, 1] = i + separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        rx[:, 0] = line
        rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        ant.set_profile(name_profile=f'D_{n}',
                         receivers=rx,
                         transmitters=tx,
                         orient_receivers=orient,
                         orient_transmitters=orient,
                         depth_Rx=line,
                         depth_Tx=line,
                         overwrite=True)
    ant.export_to_hdf5(file_h5.split('.')[0] + '_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
#%%
# p.add_object(ant2.Receiver, name='rx', color='blue', opacity=0.2)
view = 'D'
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Rx','Ry','Rz']].to_numpy(), name='rx', color='blue', opacity=0.2)
# p.add_object(ant2.Transmitter, name='tx', color='red', opacity=0.2)
p.add_object(ant.profiles.loc[ant.profiles.profile.str.contains(view), ['Tx','Ty','Tz']].to_numpy(), name='tx', color='red', opacity=0.2)
#%%
p.remove('rx')
p.remove('tx')
p.add_object(ant.get_midpoints(), name='mid', color='black', opacity=0.2)


#%%
paths_all_apertures = {'0mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/0mm/SEGY_20240412_172224/',
                        '2mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/2mm/SEGY_20240412_172341/',
                        '3mm':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/3mm/SEGY_20240412_172435/',
                        'alum':'/home/daniel/Documents/PhD_Manuscript2_Data/Gypsum/alum/SEGY_20240515_160135'}
data_measu = {}
data_together = {}

# When pol=0 then we look at antenna orientation (1,0,0). ['B', 'C']
# When pol = 1 then antenna is (0,1,0). ['A, 'D']
pol = 1 # polarization of the antennas.
th = '2mm'
vmax = 0
for key, base_path in paths_all_apertures.items():



    # Get all the files in the folder
    folders = os.listdir(base_path)
    # loop through all the folders and get the files. From the filename extract the information.
    # The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

    # Create a dictionary to store the information
    data_measu[key] = {'A':{},  # Up direction (0,1,0)
                       'B':{},  # Right dir (1,0,0)
                       'C':{},  # Up dir (1,0,0)
                       'D':{}}  # Right dir (0,1,0)

    vmax = 0
    for folder in folders:
        # Get the files
        files = os.listdir(os.path.join(base_path, folder))
        for file in files:
            if '.sgy' not in file:
                continue
            info = re.split('_', file)
            # identif = info[0]
            # Get the date
            # date = info[1]
            # Get the ID
            ID = info[0]
            # Get the path
            # experiment_number = int(info[2])
            experiment_number = int(info[1])
            n_e_id = ID + '_' + str(experiment_number)
            midpoints = ant.get_midpoints(n_e_id)
            out = [7, 24]
            if experiment_number < out[0] or experiment_number > out[1]:
                print('no_'+n_e_id)
                continue
            else:
                print(n_e_id)
            path = os.path.join(base_path, folder, file)
            # Get the data
            data_cube, header = load_sgy(path)

            # if ID =/= 'B':  # It is flipped
                # data_cube = np.fliplr(data_cube)
            # Store the data in the dictionary
            # d = {experiment_number: {'header':header, 'data': data_cube}}
            # Processing
            d_de = detrend(data_cube)
            sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
            c0 = 299_792_458  # Speed of light in m/s
            rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
            velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
            # d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
            # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
            d_de_ga = d_de.copy()
            data_uncut = d_de_ga.copy()
            if ID in ['B', 'D']:
                lims_x = (10,85)
                ma = (midpoints[:, 0] >= out[0] * 0.01) & (midpoints[:, 0] <= out[1] * 0.01)
            elif ID in ['A', 'C']:
                lims_x = (10, 82)
                ma = (midpoints[:, 1] >= out[0] * 0.01) & (midpoints[:, 1] <= out[1] * 0.01)
            else:
                raise AttributeError
            mask_x = np.zeros(d_de_ga.shape[1], dtype=bool)
            if len(ma) > len(mask_x):
                mask_x = ma[:len(mask_x)]
            else:
                mask_x[:len(ma)] = ma
            # lims_y = (80, 100)
            lims_y = (70, 110)
            d_de_ga[:lims_y[0]:,:] = 0 # crop all reflections from below
            d_de_ga[lims_y[1]:,:] = 0 # crop all reflections from below
            d_de_ga[:, :lims_x[0]] = 0 # crop all reflections from below
            d_de_ga[:, lims_x[1]:] = 0 # crop all reflections from below
            time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
            depth_vector = time_vector * velocity * 0.5
            vma = np.abs(d_de_ga).max()
            if vma > vmax: vmax=vma
            d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga,
                 'time_vector':time_vector, 'depth_vector':depth_vector,
                 'lims_x': lims_x, 'lims_y': lims_y,
                 'data_cut': data_uncut[:, mask_x],
                 'data_cut_all': data_uncut[lims_y[0]:lims_y[1], mask_x],
                 }
            data_measu[key][ID][experiment_number] = d
            if np.abs(np.max(d_de_ga)) > vmax:
                vmax=np.abs(np.max(d_de_ga))
            # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

            # #%%%5
            # from impdar.lib.RadarData import RadarData, RadarFlags
            # imp_data = RadarData(None)
            # imp_data.fn = path
            #
            #
            # imp_data.dt = 1/float(header[1]['Sampling Rate [GHz]'])
            # imp_data.data = data_cube
            #
            # # Remove pretrigger
            # # trig_threshold = 0.5  # trigger when mean trace gets up to 50% of maximum
            # # mean_trace = np.nanmean(np.abs(h5_data.data), axis=1)
            # # idx_threshold = np.argwhere(mean_trace > trig_threshold * np.nanmax(mean_trace))
            # # idx_trig = np.nanmin(idx_threshold)
            # # h5_data.data = h5_data.data[idx_trig:]
            #
            # # other variables are from the array shape
            # imp_data.snum = imp_data.data.shape[0]
            # imp_data.tnum = imp_data.data.shape[1]
            # imp_data.trace_num = np.arange(imp_data.data.shape[1]) + 1
            # imp_data.trig_level = np.zeros((imp_data.tnum,))
            # imp_data.pressure = np.zeros((imp_data.tnum,))
            # imp_data.flags = RadarFlags()
            # imp_data.travel_time = imp_data.dt * 1.0e6 * np.arange(imp_data.snum)
            # imp_data.trig = np.zeros((imp_data.tnum,))
            # imp_data.lat = np.zeros((imp_data.tnum,))
            # imp_data.long = np.zeros((imp_data.tnum,))
            # imp_data.x_coord = np.zeros((imp_data.tnum,))
            # imp_data.y_coord = np.zeros((imp_data.tnum,))
            # imp_data.elev = np.zeros((imp_data.tnum,))
            # imp_data.decday = np.arange(imp_data.tnum)
            # imp_data.trace_int = np.ones((imp_data.tnum,))
            #
            # imp_data.dist = np.arange(imp_data.tnum) / 1.0e3
            # imp_data.chan = -99.
            # imp_data.check_attrs()
            #
            # # from impdar.lib.plot import plot_traces, plot_radargram
            # plot_radargram(imp_data, clims=[-np.abs(imp_data.data).max(),np.abs(imp_data.data).max()]);plt.show()
            # # plot_traces(imp_data, tr=10)
            #
            # ### Processing ###
            # # imp_data.nmo(ant_sep=float(header.loc['Antenna Spacing [cm]'].values), uice = )
            # # imp_data.agc()
            # # imp_data.agc()
            # imp_data.migrate(mtype='kirch')
#%%
    # data_together[key] = {'AC':{},
    #                       'BD':{}}
    #
    # max_amount_traces = 86
    # for i in range(28):
    #     print(i)
    #     da = data_measu[key]['A'][i]
    #     dc = data_measu[key]['C'][i]
    #     db = data_measu[key]['B'][i]
    #     dd = data_measu[key]['D'][i]
    #
    #     dac ={'header': da['header'],
    #         'data_raw': (da['data_raw'][:,:max_amount_traces]+dc['data_raw'][:,:max_amount_traces])/vmax ,
    #         'data_uncut':(da['data_uncut'][:,:max_amount_traces] + dc['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (da['data'][:,:max_amount_traces]+dc['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':da['time_vector'],
    #         'depth_vector':da['depth_vector'],
    #         'lims_x': (da['lims_x'][0], max_amount_traces),
    #         'lims_y': da['lims_y']}
    #
    #     dbd ={'header': db['header'],
    #         'data_raw': (db['data_raw'][:,:max_amount_traces]+dd['data_raw'][:,:max_amount_traces])/vmax,
    #         'data_uncut':(db['data_uncut'][:,:max_amount_traces] + dd['data_uncut'][:,:max_amount_traces])/vmax,
    #         'data': (db['data'][:,:max_amount_traces]+dd['data'][:,:max_amount_traces])/vmax,
    #         'time_vector':db['time_vector'],
    #         'depth_vector':db['depth_vector'],
    #         'lims_x': (db['lims_x'][0],max_amount_traces),
    #         'lims_y': db['lims_y']}
    #
    #     data_together[key]['AC'][i] = dac
    #     data_together[key]['BD'][i] = dbd

#%%
# from impdar.lib import load
# dat = load.load('mat','data/synthetic_radargram.mat')[0]
#%%
df = ant.get_midpoints('A_17')

vmax = np.abs(data_measu['alum']['A'][17]['data_uncut']).max()

fig, AX = plt.subplots(1,4, sharey=True, figsize=(20,5))
for i, ax in zip(['0mm', '2mm', '3mm', 'alum'], AX):
    inf = data_measu[i]['A'][17]
    # inf = data_measu['0mm']['C'][10]
    # inf = data_together['2mm']['AC'][12]
    # lims = inf['lims']
    # sl_time = slice(0,400)
    # sl_tr = slice(10,80)
    # d = inf['data']
    d = inf['data_uncut']
    # d = inf['data_cut_all']
    # from scipy.io import savemat
    # savemat(OUTPUT_DIR+'AC_0mm_10', inf)
    # vmax=d.max() * 0.05
    # depth_vector =  data['A']['001']['depth_vector'] * 100
    time_vector =  inf['time_vector'] #* 100#[sl_time]
    # df = df['Ry'][0:d.shape[1]].to_numpy()
    extent = (df[0,1], df[-1,1], time_vector[-1], time_vector[0])
    # vmax =  np.max(np.abs(d))
    # plt.figure()
    cax = ax.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
               extent=extent
               )

    # plt.ylabel('Distance (cm)')
    ax.set_ylim(4,0)
    ax.set_title(i)
    ax.set_xlabel('Y (m)')
    # plt.ylim(4,1)
    # plt.xlim(20,80)
fig.colorbar(cax, ax=AX[-1], label='Amplitude (-)')
AX[0].set_ylabel('Two way travel time (ns)')
fig.tight_layout()
fig.show()

#%%
t = 17
l = 'D'
m = 20
plt.figure()
alum = data_measu['alum'][l][t]['data_cut_all'][:, m].copy()
norm = np.abs(alum).max()
alum/= norm
depth = data_measu['alum'][l][t]['time_vector'].copy()[lims_y[0]:lims_y[1]]
plt.plot(depth, data_measu['0mm'][l][t]['data_cut_all'].copy()[:, m]/norm, label='0mm', marker='*')
plt.plot(depth,  data_measu['2mm'][l][t]['data_cut_all'][:, m].copy()/norm, label='2mm', marker='*')
plt.plot(depth, data_measu['3mm'][l][t]['data_cut_all'][:, m].copy()/norm, label='3mm', marker='*')
plt.plot(depth, alum, label='Alum', marker='*')
# plt.xlim(70,110)
# plt.xlim(0.1,0.15)
plt.xlabel('Two-way Travel time (ns)')
plt.ylabel('Norm. Amplitude (-) ')
plt.grid()
plt.legend()
plt.show()

#%%
plt.plot(sou.frequency_complete,sou.waveform_freq)
plt.show()

# inf = data_measu['0mm']['A'][10]
# inf = data_measu['0mm']['C'][10]
inf = data_together['0mm']['AC'][10]
# lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
d = inf['data_cut']
# d = inf['data_uncut']
# vmax=d.max() * 0.05
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  inf['time_vector'] #* 100#[sl_time]


plt.plot(time_vector, d[:,10])
plt.show()

plt.plot(sou.time, sou.waveform_time)
plt.show()

plt.plot(sou.frequency, sou.source)
plt.show()

plt.plot(sou.frequency_complete, sou.waveform_freq)
plt.show()

from scipy.signal import deconvolve
new = np.zeros(d.shape)
for i, trace in enumerate(d.T):
    recovered_signal, remainder = deconvolve(d[:, i], sou.waveform_time)
    new[:,i] = remainder

# recovered_signal, remainder = deconvolve(d[:,10], sou.waveform_time)

plt.plot(remainder);plt.show()

plt.plot(np.abs(new)-np.abs(remainder/np.abs(remainder).max()));plt.show()
# plt.plot(time_vector, remainder)
# plt.xlim(2.5, 8)
# plt.ylim(-1,1)
# plt.show()
new = d[:,10]
new/=np.abs(new.max())
d/=np.abs(d.max())

plt.imshow(d-new, aspect='auto', cmap='RdBu',interpolation='None')
plt.ylim(150,0)
plt.show()
#%%0.1

extent = (0, d.shape[1], time_vector[-1], time_vector[0])
vmax =  np.max(np.abs(d))
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           extent=extent
           )

# plt.ylabel('Distance (cm)')
plt.ylabel('Two way travel time (ns)')
# plt.ylim(2.7,1.5)
# plt.ylim(4,1)
# plt.xlim(20,80)
plt.colorbar()
plt.show()
#%%
ax = solv.plot_wiggle(d, extent=extent, scale=2, fill=True, linewidth=0.5)
ax.set_ylim(2.5,0)
plt.show()


#%%%%%%%%%%%% Fracture geometry
frac = FractureGeo()
resolution = (45,45)


def rotate_matrix_clockwise(matrix, rotations=1):
    # Define a helper function to rotate the matrix once
    def rotate_once(matrix):
        # Transpose the matrix
        transposed_matrix = [list(row) for row in zip(*matrix)]
        # Reverse each row
        rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
        return rotated_matrix

    # Apply multiple rotations
    rotated_matrix = matrix.copy()
    for _ in range(rotations):
        rotated_matrix = rotate_once(rotated_matrix)

    return rotated_matrix
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d['top_surface_m']
bottom_surface_m = d['bottom_surface_m']
apertures_l = d['apertures']
spacing = [600, 600]
midpoints = d['midpoints']

ap = apertures_l.reshape(spacing)
ap = rotate_matrix_clockwise(ap, rotations=3)

try:
    frac.load_hdf5(file_h5)
    surf = frac.get_surface()
    apertures = frac.fractures['aperture'].to_numpy()
except:

    width = 0.30
    length = 0.30
    # max_element_size = 0.05  # 1 mm resolution



    # v, f = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0, resolution=resolution, center=(.17,.17,0))
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0, resolution=resolution, center=(.17,.17,0))
    # surf, vertices, faces = frac.remesh(points=vertices, max_element_size=max_element_size, plane=2)

    surf = frac.create_pyvista_mesh(vertices, faces)




#%%
    p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)
    #%%
    outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
    d = np.load(outname + 'files.npz')
    top_surface_m = d['top_surface_m']
    bottom_surface_m = d['bottom_surface_m']
    apertures_l = d['apertures']
    spacing = [600, 600]
    midpoints = d['midpoints']

    # apertures_original = np.fliplr(apertures_l.reshape(spacing).T)
    #%%


    #%%
    apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing),
                                                 rotations=1)

    #%%
    scaling_factors = (np.array(resolution) / spacing).tolist()
    from scipy.ndimage import zoom
    # Perform the downsampling using zoom
    downsampled_image = zoom(apertures_original, scaling_factors)
    scaling_factors_up = (spacing / np.array(resolution)).tolist()
    upsampled_image = zoom(downsampled_image, scaling_factors_up)

#%%
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(22,5))
    cax1 = ax1.imshow(apertures_original, cmap='viridis')
    cax2 = ax2.imshow(downsampled_image, cmap='viridis')
    cax3 = ax3.imshow(upsampled_image, cmap='viridis')
    cax4 = ax4.imshow(apertures_original-upsampled_image, cmap='seismic')
    fig.colorbar(cax1, ax=ax1)
    fig.colorbar(cax2, ax=ax2)
    fig.colorbar(cax3, ax=ax3)
    fig.colorbar(cax4, ax=ax4)
    ax1.set_title('Original')
    ax2.set_title('Downsampled')
    ax3.set_title('Upsampled')
    ax4.set_title('Original-Upsampled')
    fig.show()
#%%

# apertures = apertures.reshape(spacing)
    for i in [0, 2e-3,3e-3]:
        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2 + i
        kwarg_properties = dict(aperture=apertures, #0.01,
    #     kwarg_properties = dict(aperture=0.1,
                                    electrical_conductivity=0,
                                    electrical_permeability=1,
                                plane=2,
                                overwrite=True)
        frac.set_fracture(name_fracture=f'fracture_{int(i*1000)}',
                          vertices=vertices,
                          faces=faces,
                          **kwarg_properties)

        frac.export_to_hdf5(file_h5, overwrite=True)

#%%
surf['apertures (m)'] = apertures
p.add_object(surf, name='fracture', scalars='apertures (m)', show_edges=False,  cmap='turbo')


#%%
def _calculate_energy(d, ty):
    dat = d.copy()
    rms = np.sqrt(np.mean(dat ** 2, axis=0))
    total_energy = np.sum(dat ** 2, axis=0)
    if ty == 'rms':
        energy = rms
    elif ty == 'mean energy':
        energy = np.mean(dat ** 2, axis=0)
    elif ty == 'peak energy':
        energy = np.max(dat ** 2, axis=0)
    elif ty == 'total energy':
        energy = total_energy
    elif ty == 'crest factor':
        energy = np.max(np.abs(dat), axis=0) / rms
    elif ty == 'kurtois':
        energy = np.mean((dat - np.mean(dat, axis=0)) ** 4, axis=0) / rms ** 4
    elif ty == 'entropy':
        energy = -np.sum((np.abs(dat) ** 2 / dat) * np.log(np.abs(dat) ** 2 / total_energy), axis=0)
    elif ty == 'waveform area':
        energy = np.sum(np.abs(dat), axis=0)
    elif ty == 'spectral energy':
        energy = np.sum(np.abs(np.fft.fft(dat)) ** 2, axis=0)
    return energy
def get_energy(ty='rms', pol=0, th='0mm', dir='all'):
    energy_all = []
    energy_all_xyz = []
    # count = 0

    # skip = ['AL_001', 'AL_002', 'AL_008', 'BL_001', 'BL002', 'BL008']
    skip = []
    measurements = {}
    if pol==2:
        datmeasu = data_together[th]
    else:
        datmeasu = data_measu[th]
    for key, di in datmeasu.items():
        print(th)
        for key2, di2 in di.items():
            if pol==0:
                if key in ['A', 'D']:
                    continue
            elif pol ==1:
                if key in ['B', 'C']:
                    continue
            # elif pol ==2:
                # if key in ['AC']:
                #     continue

                # if key in ['BD']:
                #     continue
                # key = 'A'
            if dir =='up':
                if key in 'BD':
                    continue
            elif dir =='right':
                if key in 'AC':
                    continue
            else:
                pass

            if 'B' in key:
                key = 'B'
            elif 'A' in key:
                key = 'A'
            name = f'{key}_{key2}'
            if name in skip:
                continue
            dat = di2['data']
            # t = di2['time_vector']
            # de = di2['depth_vector'] #* 100 # to convert to cm


            df = ant2.profiles.loc[ant2.profiles.profile == name]
            print(name, len(df))
            patha = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy() )/2
            end = dat.shape[1]-5
            patha = patha[:end]

            # ty = 'mean energy'

            energy = _calculate_energy(dat, ty)[:end]

            # if key == 'A' and key2 == 10:
            #     plt.plot(patha[:,1], energy, '.')
            #     plt.show()
            #     break

            energy_all.append(energy)
            energy_all_xyz.append(patha)
            assert len(energy) == len(patha), f'{name}: {len(energy)} != {len(patha)}'
            measurements[name] = {'energy': energy, 'path': patha}

    energy_all_ = np.hstack(energy_all)
    energy_all_ /= energy_all_.max()
    energy_all_xyz_ = np.vstack(energy_all_xyz)
    return energy_all_, energy_all_xyz_
    # #%% Remove all profiles
    # for key, di in data.items():
    #     for key2, di2 in di.items():
    #         p.remove(f'{key}_{key2}')
#%%
th = '3mm'
pol=2
dir='right'
options = ['rms','peak energy','total energy',
           #'crest factor','kurtois','entropy',
           'waveform area',
           #'spectral energy'
           ]
fig, AX = plt.subplots(1, len(options)+1,
                       # figsize=(30,5))
                       figsize=(8*len(options)+1,5))
extent = (0.02,0.32,0.02,0.32)
cax = AX[0].imshow(ap, cmap='turbo', extent=extent, aspect='auto')
AX[0].set_title('Measured Apertures', fontsize=30)

for i in range(len(AX)-1):
    # print(i)
    energy_all_, energy_all_xyz_ = get_energy(ty = options[i], pol=pol, th=th, dir=dir)
    # cax2 = ax2.tricontourf(energy_all_xyz_[:,0], energy_all_xyz_[:,1], energy_all_, cmap='turbo', levels=50, vmin=0.15, vmax=1)
    cax2 = AX[i+1].scatter(energy_all_xyz_[:,0], energy_all_xyz_[:,1], c=energy_all_, cmap='turbo',
                           # vmin=0.15, vmax=1,
                           s=15)
    AX[i+1].set_title(options[i], fontsize=30)
# cax3 = ax3.tricontourf(energy_all_xyz__solv[:,0], energy_all_xyz__solv[:,1], energy_all__solv/energy_all__solv.max(), cmap='turbo', levels=100, )
# cax3 = ax3.scatter(energy_all_xyz__solv[:,0], energy_all_xyz__solv[:,1], c=energy_all__solv/energy_all__solv.max(), cmap='turbo', s=30, )
# cax4 = ax4.tricontourf(apertures_fresnel[:,0], apertures_fresnel[:,1], apertures_fresnel[:,2]*100, cmap='turbo', levels=100, )
# cax4 = ax4.scatter(apertures_fresnel[:,0], apertures_fresnel[:,1], c=apertures_fresnel[:,2]*100, cmap='turbo', s=30)

for ax in AX:#, , ax4]:
    ax.set_xlim(0.02,0.32)
    ax.set_ylim(0.02,0.32)



# ax3.set_title('Simulated Data',fontsize=30)
# ax4.set_title(f'Fresnel Zone: {type}',fontsize=30)



# fig.colorbar(cax,  ax=ax1, label='Aperture (cm)', )
# fig.colorbar(cax2,  ax=ax2, label='Norm. GPR Amplitudes (-)', )
# fig.colorbar(cax3,  ax=ax3, label='Norm. GPR Amplitudes (-)', )
# fig.colorbar(cax4,  ax=ax4, label='Fresnel apertures (cm)', )
plt.show()


#%%%% Plot the difference


D1 = data_together['0mm']
D2 = data_together['2mm']
D3 = data_together['3mm']

data_comparison = {'AC':{},
                   'BD':{}}

energy_all = []
energy_all_xyz = []

for orie in ['AC', 'BD']:
    for i in range(28):

        if 'B' in orie:
            orie_ = 'B'
        elif 'A' in orie:
            orie_ = 'A'
        name = f'{orie_}_{i}'

        df = ant2.profiles.loc[ant2.profiles.profile == name]
        patha = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy()) / 2

        end = D1[orie][i]['data_raw'].shape[1] - 5
        patha = patha[:end]

        arr_x = np.arange(end)
        arr_y = np.arange(D1[orie][i]['data_raw'].shape[0])
        lims_x = slice(D1[orie][i]['lims_x'][0], D1[orie][i]['lims_x'][1])
        lims_y = slice(D1[orie][i]['lims_y'][0], D1[orie][i]['lims_y'][1])
        # lims_y = slice(70, 90)
        mask_x = np.logical_or(arr_x < lims_x.start, arr_x >= lims_x.stop)
        mask_y = np.logical_or(arr_y < lims_y.start, arr_y >= lims_y.stop)

        phase_diff_D1_D1 = hilbert_phase_difference(D1[orie][i]['data_raw'],D1[orie][i]['data_raw'])[:, :end]
        phase_diff_D1_D2 = hilbert_phase_difference(D1[orie][i]['data_raw'],D2[orie][i]['data_raw'])[:, :end]
        phase_diff_D1_D3 = hilbert_phase_difference(D1[orie][i]['data_raw'],D3[orie][i]['data_raw'])[:, :end]
        phase_diff_D2_D2 = hilbert_phase_difference(D2[orie][i]['data_raw'],D2[orie][i]['data_raw'])[:, :end]
        phase_diff_D2_D3 = hilbert_phase_difference(D2[orie][i]['data_raw'],D3[orie][i]['data_raw'])[:, :end]
        phase_diff_D3_D3 = hilbert_phase_difference(D3[orie][i]['data_raw'],D3[orie][i]['data_raw'])[:, :end]

        print(name, len(patha), phase_diff_D1_D1.shape[1])

        phase_diff_D1_D1[:, mask_x] = 0
        phase_diff_D1_D2[:, mask_x] = 0
        phase_diff_D1_D3[:, mask_x] = 0
        phase_diff_D2_D2[:, mask_x] = 0
        phase_diff_D2_D3[:, mask_x] = 0
        phase_diff_D3_D3[:, mask_x] = 0

        phase_diff_D1_D1[mask_y] = 0
        phase_diff_D1_D2[mask_y] = 0
        phase_diff_D1_D3[mask_y] = 0
        phase_diff_D2_D2[mask_y] = 0
        phase_diff_D2_D3[mask_y] = 0
        phase_diff_D3_D3[mask_y] = 0

        diff_D1_D1 = (D1[orie][i]['data_raw']-D1[orie][i]['data_raw'])[:,:end]
        diff_D1_D2 = (D1[orie][i]['data_raw']-D2[orie][i]['data_raw'])[:,:end]
        diff_D1_D3 = (D1[orie][i]['data_raw']- D3[orie][i]['data_raw'])[:,:end]
        diff_D2_D2 = (D2[orie][i]['data_raw']- D2[orie][i]['data_raw'])[:,:end]
        diff_D2_D3 = (D2[orie][i]['data_raw']- D3[orie][i]['data_raw'])[:,:end]
        diff_D3_D3 = (D3[orie][i]['data_raw']- D3[orie][i]['data_raw'])[:,:end]

        diff_D1_D1[:, mask_x] = 0
        diff_D1_D2[:, mask_x] = 0
        diff_D1_D3[:, mask_x] = 0
        diff_D2_D2[:, mask_x] = 0
        diff_D2_D3[:, mask_x] = 0
        diff_D3_D3[:, mask_x] = 0

        diff_D1_D1[mask_y] = 0
        diff_D1_D2[mask_y] = 0
        diff_D1_D3[mask_y] = 0
        diff_D2_D2[mask_y] = 0
        diff_D2_D3[mask_y] = 0
        diff_D3_D3[mask_y] = 0

        # plt.imshow(phase_diff_D2_D3);plt.show()
        ty = 'total energy'
        energy_phase_D1_D1 = _calculate_energy(phase_diff_D1_D1, ty=ty)
        energy_phase_D1_D2 = _calculate_energy(phase_diff_D1_D2, ty=ty)
        energy_phase_D1_D3 = _calculate_energy(phase_diff_D1_D3, ty=ty)
        energy_phase_D2_D2 = _calculate_energy(phase_diff_D2_D2, ty=ty)
        energy_phase_D2_D3 = _calculate_energy(phase_diff_D2_D3, ty=ty)
        energy_phase_D3_D3 = _calculate_energy(phase_diff_D3_D3, ty=ty)

        energy_diff_D1_D1 = _calculate_energy(diff_D1_D1, ty=ty)
        energy_diff_D1_D2 = _calculate_energy(diff_D1_D2, ty=ty)
        energy_diff_D1_D3 = _calculate_energy(diff_D1_D3, ty=ty)
        energy_diff_D2_D2 = _calculate_energy(diff_D2_D2, ty=ty)
        energy_diff_D2_D3 = _calculate_energy(diff_D2_D3, ty=ty)
        energy_diff_D3_D3 = _calculate_energy(diff_D3_D3, ty=ty)



        data_comparison[orie][i] = {'D11_phase': phase_diff_D1_D1,
                                    'D12_phase': phase_diff_D1_D2,
                                    'D13_phase': phase_diff_D1_D3,
                                    'D22_phase': phase_diff_D2_D2,
                                    'D23_phase': phase_diff_D2_D3,
                                    'D33_phase': phase_diff_D3_D3,

                                    'D11_diff': diff_D1_D1,
                                    'D12_diff': diff_D1_D2,
                                    'D13_diff': diff_D1_D3,
                                    'D22_diff': diff_D2_D2,
                                    'D23_diff': diff_D2_D3,
                                    'D33_diff': diff_D3_D3,

                                    'D11_energy_phase': energy_phase_D1_D1,
                                    'D12_energy_phase': energy_phase_D1_D2,
                                    'D13_energy_phase': energy_phase_D1_D3,
                                    'D22_energy_phase': energy_phase_D2_D2,
                                    'D23_energy_phase': energy_phase_D2_D3,
                                    'D33_energy_phase': energy_phase_D3_D3,

                                    'D11_energy_diff': energy_diff_D1_D1,
                                    'D12_energy_diff': energy_diff_D1_D2,
                                    'D13_energy_diff': energy_diff_D1_D3,
                                    'D22_energy_diff': energy_diff_D2_D2,
                                    'D23_energy_diff': energy_diff_D2_D3,
                                    'D33_energy_diff': energy_diff_D3_D3,

                                    'path': patha,
                                    }
        # energy_all.append(energy)
        # energy_all_xyz.append(patha)
#%%

for e in ['energy_diff', 'energy_phase']:

    # fig, AX = plt.subplots(3,3)
    fig, AX = plt.subplots(1, 4, figsize=(20,5))
    energy_D11 = []
    energy_D12 = []
    energy_D13 = []
    energy_D22 = []
    energy_D23 = []
    energy_D33 = []
    patha = []
    for orie in ['AC', 'BD']:
        for i in range(28):

            energy_D11.append(data_comparison[orie][i]['D11_'+e])
            energy_D12.append(data_comparison[orie][i]['D12_'+e])
            energy_D13.append(data_comparison[orie][i]['D13_'+e])
            energy_D22.append(data_comparison[orie][i]['D22_'+e])
            energy_D23.append(data_comparison[orie][i]['D23_'+e])
            energy_D33.append(data_comparison[orie][i]['D33_'+e])

            patha.append(data_comparison[orie][i]['path'])

    energy_D11 = np.hstack(energy_D11)
    energy_D12 = np.hstack(energy_D12)
    energy_D13 = np.hstack(energy_D13)
    energy_D22 = np.hstack(energy_D22)
    energy_D23 = np.hstack(energy_D23)
    energy_D33 = np.hstack(energy_D33)

    energy_D11 /= energy_D11.max()
    energy_D12 /= energy_D12.max()
    energy_D13 /= energy_D13.max()
    energy_D22 /= energy_D22.max()
    energy_D23 /= energy_D23.max()
    energy_D33 /= energy_D33.max()

    patha = np.vstack(patha)

    # lim=(0.12,0.22)
    lim=(0.02,0.32)

    mask = (patha[:,0] > lim[0]) & (patha[:,1] > lim[0]) & (patha[:,0] < lim[1]) & (patha[:,1] < lim[1])

    kwargs = dict(cmap='turbo',
                  s=25,
                  # s=15,
                  x=patha[mask,0],
                  y=patha[mask,1],
                  # z=energy_D13[mask]
                  )

    extent = (0.02, 0.32, 0.02, 0.32)
    cax = AX[0].imshow(ap, cmap='turbo', extent=extent, aspect='auto')
    AX[0].set_title('Measured Apertures', fontsize=30)

    AX[1].scatter(c=energy_D12[mask], **kwargs)
    AX[1].set_title('0mm vs 2mm', fontsize=30)
    AX[2].scatter(c=energy_D13[mask], **kwargs)
    # AX[2].tricontour((patha[mask,0],patha[mask,1]),energy_D13[mask] )
    AX[2].set_title('0mm vs 3mm', fontsize=30)
    AX[3].scatter(c=energy_D23[mask], **kwargs)
    AX[3].set_title('2mm vs 3mm', fontsize=30)



    for ax in AX.ravel():
        ax.set_xlim(lim)
        ax.set_ylim(lim)
    plt.show()


#%%
e = 'phase'
d = data_comparison[orie][10]['D13_'+e]

plt.imshow(d)
plt.ylim(110,75)
plt.show()

#%%

p.remove('mid')
p.remove_scalar_bar()

# energy_all_xyz_[:,2] += .20
# clim = (5.66e-09, 3.62e-07)
p.add_object(energy_all_xyz_, name='on_surface', cmap='turbo', scalars=energy_all_, clim=(0.15,1))
# energy_all_xyz_[:,0] +=.70
# p.add_object(energy_all_xyz_, name='on_surface2', cmap='turbo', scalars=energy_all_)#, clim=clim)
p.show(xtitle='X (m)', ytitle='Y (m)', ztitle='Z (m)')
# #%%

#%%%%%%%%%%%%%%
ant = Antenna()
try:
    # raise FileNotFoundError
    ant.load_hdf5(file_h5)
except:
    # separation = 3.6 * 1e-2  # cm

    thickness = 1.125 * 1e-2  # cm
    positions = []
    pi = (8 - 4.5) * 1e-2  # starting 8cm to the right and then 2.1 sm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
    # pi = -0.5 * 1e-2
    positions.append(pi)
    for i in range(0, 28):
        positions.append(positions[-1] + thickness)
        # if the number is even append otherwise not
        # if i % 4 == 0:
        #     positions.append(positions[-1] + 4.5 - 2.17)
        # else:
        #     positions.append(positions[-1] + 2.17)

    positions = np.array(positions)

    x = positions.copy()
    y = positions.copy()

    top = 12 * 1e-2  # cm to top

    # Scan from bottom to top
    line = np.arange(0.27 + 4.5 - 10, 34 + 16 - 0.27 - 4.5,
                     0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])
                     # 2) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])
    # ltx = line + separation / 2
    # lrx = line - separation / 2
    for n, i in enumerate(x):  # [1:-1]):
        # if n != 14:
        #     continue
        tx = np.zeros((len(line), 3))
        tx[:, 0] = i
        tx[:, 1] = line
        tx[:, 2] = top

        rx = tx.copy()
        # rx[:, 1] = lrx

        orient = np.zeros((len(line), 3))
        orient[:, 1] = 1

        midpoints = tx #+ rx) * 0.5

        mask = (0 <= midpoints[:, 1]) & (midpoints[:, 1] <= 0.34)

        ant.set_profile(name_profile=f'A_{n}',
                        receivers=rx[mask],
                        transmitters=tx[mask],
                        orient_receivers=orient[mask],
                        orient_transmitters=orient[mask],
                        depth_Rx=line[mask],
                        depth_Tx=line[mask],
                        overwrite=True)

    # Scan left to right
    # line = np.arange(0.27 + 4.5 - 10, 34 + 16 - 0.27 - 4.5,
    #                  0.5) * 1e-2  # (Starting point = Offset pin + midpoint antenna [4.5] - Short end grid [10], End point = extent block[34] + long end grid [16] - offset pin [0.27] - midpoint antenna [4.5])

    line2 = np.arange(34 + 16 - 0.27 - 4.5, 0.27 + 4.5 - 10,
                      -0.5) * 1e-2  # (Starting point = extent block[34] + short end grid [16] - offset pin [0.27] - midpoint antenna [4.5],  end point = Offset pin + midpoint antenna [4.5] - long end grid [10])
                      # -2) * 1e-2  # (Starting point = extent block[34] + short end grid [16] - offset pin [0.27] - midpoint antenna [4.5],  end point = Offset pin + midpoint antenna [4.5] - long end grid [10])

    # ltx2 = line2  # + separation / 2
    # lrx2 = line2  # - separation / 2
    for n, i in enumerate(y):  # [1:-1]):
        # continue
        tx = np.zeros((len(line2), 3))
        tx[:, 0] = line
        tx[:, 1] = i #+ separation / 2
        tx[:, 2] = top

        rx = tx.copy()
        # rx[:, 0] = lrx2
        # rx[:, 1] = i - separation / 2

        orient = np.zeros((len(line2), 3))
        orient[:, 1] = 1

        midpoints = tx #+ rx) * 0.5

        mask = (0 <= midpoints[:, 0]) & (midpoints[:, 0] <= 0.34)

        ant.set_profile(name_profile=f'B_{n}',
                        receivers=rx[mask],
                        transmitters=tx[mask],
                        orient_receivers=orient[mask],
                        orient_transmitters=orient[mask],
                        depth_Rx=line2[mask],
                        depth_Tx=line2[mask],
                        overwrite=True)
    # ant2.export_to_hdf5(file_h5.split('.')[0]+'_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)


#%%
p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)
#%%
p.remove('rx')
p.remove('tx')
middle_points = (ant.Receiver + ant.Transmitter)*0.5
p.add_object(middle_points, name='mid', color='black', opacity=0.2)

#%%%% Read file and see simulation
# file_h52 = OUTPUT_DIR + 'fracture_aperture/cement_block_5mm.h5'
solv = FracEM()
solv.load_hdf5(file_h5)
solv.backend = 'numpy'
# solv.load_hdf5(file_h5.split('.')[0]+"_30cm.h5")

#%%
solv.time_zero_correction()

#%%
profiles = {}
# ant = ant2
for n in ant.name_profiles:
    freq = solv.get_freq(name_profile=n)

    data_sim, time_vector = solv.get_ifft(freq)

    profiles[n] = {'data': data_sim, 'time_vector': time_vector}

# solv.get_ifft()
import pickle
pickle_result = os.path.abspath('/output/manuscript2/cement_block_5mm_output_simulation.pickle')
with open(pickle_result, 'wb') as handle:
    pickle.dump(profiles, handle, protocol=pickle.HIGHEST_PROTOCOL)

#%%%%%%%%%%%%%%%%%%%%%%


solv = FracEM()
solv.rock_epsilon =6.5
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
solv._propagation_mode = 1
solv.backend = 'torch'
solv._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have >
solv.apply_causality = True # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those >
solv.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achiev>
solv._filter_percentage = 0.01
solv.mode='reflection'
solv._old_solver = True
solv._scaling = True
solv.units = 'SI'
# solv._old_solver = False
info = solv.open_file(file_h5)

# _, _2 = solv.time_zero_correction()

print(solv)
n = 'A_9'
solv.solve_for_profile=n
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, )



#%%
n = 'A_12'
freq = solv.get_freq(name_profile=n, name_fracture='fracture_0')#[...,1]
# # freq = solv.file_read('simulation/full_response')[...,0]
# freq = np.conj(freq)
data_sim, time_vector = solv.get_ifft(freq, apply_t0=False)

data_n = data_sim.T #/ data.T.max(axis=0)
# mask = (2.5<=time_vector) & (time_vector<=3.3)
# data_n[~mask] = 0
vmax=data_n.max()
extent = (0, data_n.shape[1], time_vector[-1], time_vector[0])
plt.figure()
plt.imshow(data_n, aspect='auto', cmap='RdBu',interpolation='None', extent=extent)#, vmin =-vmax, vmax =vmax,)
# plt.ylim(3,2.2)
plt.colorbar()
plt.show()
#%%
n = 'C_12'
freq_0 = solv.get_freq(name_profile=n, name_fracture='fracture_0')#[...,1]
freq_2 = solv.get_freq(name_profile=n, name_fracture='fracture_2')#[...,1]
freq_3 = solv.get_freq(name_profile=n, name_fracture='fracture_3')#[...,1]
data_sim_0, time_vector = solv.get_ifft(freq_0, apply_t0=False)
data_sim_2, time_vector = solv.get_ifft(freq_2, apply_t0=False)
data_sim_3, time_vector = solv.get_ifft(freq_3, apply_t0=False)

extent = (0, data_sim_0.shape[0], time_vector[-1], time_vector[0])
norm = np.abs(data_sim_0).max()
fig, ax = plt.subplots(figsize=(15,5))
ax = solv.plot_wiggle(data_sim_0.copy().T, extent=extent, scale=2, fill=False, linewidth=0.5, normalize_to_value = norm, color='black', ax=ax, label='0')
ax = solv.plot_wiggle(data_sim_2.copy().T, extent=extent, scale=2, fill=False, linewidth=0.5, normalize_to_value = norm, color='red', ax=ax, label='2')
ax = solv.plot_wiggle(data_sim_3.copy().T, extent=extent, scale=2, fill=False, linewidth=0.5, normalize_to_value = norm, color='green', ax=ax, label='3')

ax.set_ylim(4,2)
ax.set_xlabel('No. Trace')
ax.set_ylabel('Time (ns)')
ax.legend(loc='best')
ax.figure.show()
#%%
energy_all_solv = []
energy_all_xyz_solv = []
count = 0

for n in solv.name_profiles:
    # if "A" in n:
    #     continue
    freq = solv.get_freq(name_profile=n)
    # freq = np.conj(freq)
    data_sim, time_vector = solv.get_ifft(freq)
    data_n = data_sim.T  # / data.T.max(axis=0)
    # mask = (2.2 <= time_vector) & (time_vector <= 3)
    # mask = (2.2 <= time_vector) & (time_vector <= 3.5)
    # data_n[~mask] = 0
    # vmax = data_n.max()

    # energy = np.abs(data_n).max(axis=0)
    energy = np.abs(data_n).sum(axis=0)

    df = ant.profiles.loc[ant.profiles.profile == n]
    patha = df[['Rx', 'Ry', 'Rz']].to_numpy() #+ df[['Tx', 'Ty', 'Tz']].to_numpy() )/2
    energy_all_solv.append(energy)
    energy_all_xyz_solv.append(patha)
    assert len(energy) == len(patha), n


#%%
p.remove_scalar_bar()
energy_all__solv = np.hstack(energy_all_solv)
energy_all_xyz__solv = np.vstack(energy_all_xyz_solv)
# energy_all_xyz__solv[:,2] += .20
# clim = (130, 167)
p.add_object(energy_all_xyz__solv, name='on_surface', cmap='turbo', scalars=energy_all__solv)#, clim=clim)
# energy_all_xyz__solv[:,0] +=.70
# p.add_object(energy_all_xyz__solv, name='on_surface2', cmap='turbo', scalars=energy_all__solv)#, clim=clim)


#%%
def average(data, type='arithmetic', distance=None):
    if type == 'arithmetic':
        return np.mean(data)
    elif type == 'median':
        return np.median(data)
    elif type == 'mode':
        counts = np.unique(data, return_counts=True)[1]
        # Find the maximum count
        max_count = np.max(counts)
        # Get all modes (values with max count)
        modes = data[counts == max_count]
        return modes.tolist()  # Convert to list for return
    elif type == 'geometric_mean':
        # Check for negative values or zeros
        if any(value <= 0 for value in data):
            raise ValueError("Geometric mean is not defined for non-positive values.")

        # Calculate the product and take the nth root
        product = np.prod(data)
        n = len(data)
        return np.power(product, 1 / n)
    elif type == 'harmonic_mean':
        if any(value <= 0 for value in data):
            raise ValueError("Harmonic mean is not defined for non-positive values.")

        # Calculate the sum of reciprocals and take the reciprocal
        reciprocals = 1 / np.array(data)
        return 1 / np.mean(reciprocals)
    elif type == 'distance_weight':
        weights = 1 / distance**2  # Weights based on inverse distance
        weights = weights / np.sum(weights)  # Normalize weights to sum to 1
        return np.average(data, weights=weights)
    else:
        raise AttributeError

#%%
def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5


distance = 0.12  # Top to the view
rock_epsilon = 6.5
c0 = 299_792_458
velocity = c0 * 1e-7 / np.sqrt(rock_epsilon)
frequency = 3.6 # GHz
wavelength = solv.velocity / sou.center_frequency  # Result in cm

r = fresnel(distance, wavelength, 1)

#%%
from tqdm.autonotebook import tqdm
# options are ['arithmetic', 'geometric_mean', 'harmonic_mean', 'median', 'mode', 'distance_weight]
type = 'distance_weight'
try:
    raise FileNotFoundError
    apertures_fresnel = np.load(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{type}.npy')
except FileNotFoundError:

    def apertures_inside_circle(point_cloud, apertures, circle_origin, circle_radius):
        # Convert the point cloud to a NumPy array for easier calculations
        point_cloud_array = np.array(point_cloud) + 100
        circle_origin =  np.array(circle_origin) + 100

        # Calculate the distances from each point to the circle's origin
        distances = np.linalg.norm(point_cloud_array - circle_origin, axis=1)

        # Find indices of points that are inside the circle (distance < radius)
        inside_circle_indices = np.where(distances < circle_radius)[0]
        point_cloud_array -= 100
        circle_origin -= 100
        # Extract the points that are inside the circle
        points_inside_circle = point_cloud_array[inside_circle_indices]
        apertures_inside_cirlce = apertures[inside_circle_indices]
        distance = distances[inside_circle_indices]

        return points_inside_circle, apertures_inside_cirlce, distance

    apertures_fresnel2 = []
    position_fresnel = []
    dist_frenel = []
    for m in tqdm(ant.Receiver):#middle_points):
        points, apert, dista = apertures_inside_circle(frac.get_midpoints(frac.vertices, frac.faces)[:,:2],
                                                frac.fractures['aperture'].to_numpy(), m[:2], r)
        if points.size == 0:
            apertures_fresnel2.append(0)
            position_fresnel.append([0])
            dist_frenel.append([0])
            continue
        if type in ['geometric_mean', 'harmonic_mean']:
            apert = np.abs(apert)

        apertures_fresnel2.append(average(apert, type=type, distance=dista))
        position_fresnel.append(points)
        dist_frenel.append(dista)

    apertures_fresnel = np.c_[middle_points[:,0],middle_points[:,1], apertures_fresnel2]
    np.save(OUTPUT_DIR + f'fracture_aperture/apertures_fresnel_{type}.npy', apertures_fresnel)

#%%% Lets put them both together one next to each othe
fig, (ax1, ax2,
      ax3, ax4
      ) = plt.subplots(1, 4,
                       figsize=(30,5))
                       # figsize=(15,5))

extent = (0,0.34,0,0.34)
cax = ax1.imshow(ap, cmap='turbo', extent=extent, aspect='auto')
# cax2 = ax2.tricontourf(energy_all_xyz_[:,0], energy_all_xyz_[:,1], energy_all_, cmap='turbo', levels=50, vmin=0.15, vmax=1)
cax2 = ax2.scatter(energy_all_xyz_[:,0], energy_all_xyz_[:,1], c=energy_all_, cmap='turbo', vmin=0.15, vmax=1, s=30)
# cax3 = ax3.tricontourf(energy_all_xyz__solv[:,0], energy_all_xyz__solv[:,1], energy_all__solv/energy_all__solv.max(), cmap='turbo', levels=100, )
cax3 = ax3.scatter(energy_all_xyz__solv[:,0], energy_all_xyz__solv[:,1], c=energy_all__solv/energy_all__solv.max(), cmap='turbo', s=30, )
# cax4 = ax4.tricontourf(apertures_fresnel[:,0], apertures_fresnel[:,1], apertures_fresnel[:,2]*100, cmap='turbo', levels=100, )
cax4 = ax4.scatter(apertures_fresnel[:,0], apertures_fresnel[:,1], c=apertures_fresnel[:,2]*100, cmap='turbo', s=30)

for ax in [ax1, ax2,
           ax3, ax4
           ]:#, , ax4]:
    ax.set_xlim(0,0.34)
    ax.set_ylim(0,0.34)

ax1.set_title('Measured Apertures', fontsize=30)
ax2.set_title('Real Data',fontsize=30)
ax3.set_title('Simulated Data',fontsize=30)
ax4.set_title(f'Fresnel Zone: {type}',fontsize=30)



fig.colorbar(cax,  ax=ax1, label='Aperture (cm)', )
fig.colorbar(cax2,  ax=ax2, label='Norm. GPR Amplitudes (-)', )
fig.colorbar(cax3,  ax=ax3, label='Norm. GPR Amplitudes (-)', )
fig.colorbar(cax4,  ax=ax4, label='Fresnel apertures (cm)', )
plt.show()


#%%
import numpy as np
import matplotlib.pyplot as plt
from hurst import compute_Hc, random_walk

# Use random_walk() function or generate a random walk series manually:
# series = random_walk(99999, cumprod=True)
np.random.seed(42)
random_changes = 1. + np.random.randn(99999) / 1000.
series = np.cumprod(random_changes)  # create a random walk from random changes

# Evaluate Hurst equation
H, c, data = compute_Hc(series, kind='price', simplified=True)

# Plot
f, ax = plt.subplots()
ax.plot(data[0], c*data[0]**H, color="deepskyblue")
ax.scatter(data[0], data[1], color="purple")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('Time interval')
ax.set_ylabel('R/S ratio')
ax.grid(True)
plt.show()

print("H={:.4f}, c={:.4f}".format(H,c))

#%%
def linear_interpolation(data, new_points):
  """
  Performs linear interpolation between existing data points.

  Args:
      data (list): A list of existing data points (x, y pairs).
      new_points (list): A list of desired new x values for interpolation.

  Returns:
      list: A list of interpolated y values corresponding to the new_points.
  """
  # Convert data to NumPy arrays for easier handling
  x = np.array([point[0] for point in data])
  y = np.array([point[1] for point in data])

  # Use linear interpolation function from NumPy
  interpolated_y = np.interp(new_points, x, y)

  return interpolated_y.tolist()
#%%
n = 12

prof = ap[12]
prof += 0.1
plt.plot(prof);plt.show()

H, c, data = compute_Hc(prof, kind='price', simplified=True)

f, ax = plt.subplots()
ax.plot(data[0], c*data[0]**H, color="deepskyblue")
ax.scatter(data[0], data[1], color="purple")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('Time interval')
ax.set_ylabel('R/S ratio')
ax.grid(True)
plt.show()

print("H={:.4f}, c={:.4f}".format(H,c))


#%%'
d = data_measu['A'][n]['data']
d = np.abs(d).max(axis=0)
d = d[d>0]
x = np.arange(0, len(d))
x_new = np.arange(0,len(d), 0.2)
d_interp = np.interp(x_new,x, d)
plt.plot(d_interp);plt.show()

H, c, data = compute_Hc(d_interp, kind='price', simplified=True)

f, ax = plt.subplots()
ax.plot(data[0], c*data[0]**H, color="deepskyblue")
ax.scatter(data[0], data[1], color="purple")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('Time interval')
ax.set_ylabel('R/S ratio')
ax.grid(True)
plt.show()

print("H={:.4f}, c={:.4f}".format(H,c))

#%%

freq = solv.get_freq(name_profile=f'A_{n}')#[...,1]
# # freq = solv.file_read('simulation/full_response')[...,0]
# freq = np.conj(freq)
data_sim, time_vector = solv.get_ifft(freq, apply_t0=False)
data_sim =data_sim.max(axis=1)
x = np.arange(0, len(data_sim))
x_new = np.arange(0,len(data_sim), 0.2)
d_interp = np.interp(x_new,x, data_sim)
plt.plot(d_interp);plt.show()

H, c, data = compute_Hc(d_interp, kind='price', simplified=True)

f, ax = plt.subplots()
ax.plot(data[0], c*data[0]**H, color="deepskyblue")
ax.scatter(data[0], data[1], color="purple")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('Time interval')
ax.set_ylabel('R/S ratio')
ax.grid(True)
plt.show()

print("H={:.4f}, c={:.4f}".format(H,c))


#%%

from scipy.spatial import KDTree

def make_profile(name):
    """
    Choose the name of the profile to plot and create a cross section plot showing all the apertures, gpr response and
    the fresnel zone
    Args:
        name:

    Returns:

    """

    # Pont cloud of all profiles
    df = ant.profiles.loc[ant.profiles.profile == name]
    prof = (df[['Tx','Ty']].to_numpy() + df[['Rx','Ry']].to_numpy()) * 0.5

    xmin = prof[:,0].min()
    xmax = prof[:,0].max()
    ymin = prof[:, 1].min()
    ymax = prof[:, 1].max()
    extent = [xmin, xmax, ymin, ymax]

    width = xmax - xmin
    height = ymax - ymin

    # Determine the long axis based on width and height
    if width >= height:
        long_axis = 0
        vmin = xmin
        vmax = xmax
        const = ymin
    else:
        long_axis = 1
        vmin = ymin
        vmax = ymax
        const = xmin

    # Create equally spaced vector along the long axis
    l = np.arange(vmin, vmax, 0.01)
    profile_vector = np.ones((len(l), 2)) * const
    # l = np.vstack([profile_vector, profile_vector]).T

    profile_vector[:, long_axis] = l

    outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
    d = np.load(outname + 'files.npz')
    top_surface_m = d['top_surface_m']
    bottom_surface_m = d['bottom_surface_m']
    apertures_l = d['apertures']
    spacing = [600, 600]
    midpoints_real = d['midpoints']

    ap = apertures_l.reshape(spacing)


    midpoints = frac.get_midpoints(frac.vertices, frac.faces)[:, :2]



    # copy_thickness_results = frac.fractures.aperture.to_numpy()
    # copy_thickness_results += np.asarray((17,17, 0))

    kdtree = KDTree(midpoints)  # Assuming 2D point cloud; adjust accordingly
    kdtreereal = KDTree(midpoints_real[:,:2])
    # Find nearest neighbors for each point in the path
    profile_points = []
    profile_points_real = []
    for path_point in profile_vector:
        _, idx = kdtree.query(path_point, workers=-1)#, distance_upper_bound=0.1)
        profile_points.append(idx)
        _, idx = kdtreereal.query(path_point, workers=-1)#, distance_upper_bound=0.1)
        profile_points_real.append(idx)

    freq = solv.get_freq(name_profile=name)  # [...,1]
    # # freq = solv.file_read('simulation/full_response')[...,0]
    # freq = np.conj(freq)
    data_sim, time_vector = solv.get_ifft(freq, apply_t0=False)
    data_sim = data_sim.max(axis=1)
    df = ant.profiles.loc[ant.profiles.profile == name]
    # print(name, len(df))
    patha = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy()) / 2


    fig, ax = plt.subplots(figsize=(10,5))
    # ax.plot(apertures_fresnel[df.index, long_axis], apertures_fresnel[df.index, -1], color='red', marker='.', label='Fresnel apertures')
    ax.plot(patha[:, long_axis], data_sim, color='green', label='Simulated_apertures')
    # ax.plot(midpoints[profile_points, long_axis], apertures[profile_points], color='red', label='Model_apertures')
    ax.plot(midpoints[:, long_axis], apertures[profile_points], color='red', label='Model_apertures')
    ax.plot(midpoints_real[profile_points_real, long_axis], midpoints_real[profile_points_real, 2], color='yellow', label='Real_apertures')

    ax2 = ax.twinx()
    ax2.plot(measurements[name]['path'][:,long_axis], measurements[name]['energy'] , color='blue', marker='.', label='GPR aperture')

    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    ax.set_xlabel('aperture (cm)')
    ax.set_ylabel(f'distance (cm) along axis {long_axis}')
    ax.set_title(name + '   Extent:'+str(extent))
    ax2.set_ylabel('GPR amplitudes (-)')
    ax2.spines['right'].set_color('blue')
    ax2.tick_params(axis='y', colors='blue')
    ax2.yaxis.label.set_color('blue')
    ax.grid()
    ax.set_xlim(0,0.34)
    plt.show()

#%%
make_profile('A_12')
