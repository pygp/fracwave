#%%
from fracwave import OUTPUT_DIR, FracEM

solv = FracEM()
solv.open_file(OUTPUT_DIR + 'FW_sim.h5')
#%%
# Now we can play with the different paramenters
solv.rock_epsilon = 5.9  # Electrical permittivity of the medium
solv.rock_sigma = 0  # Electrical conductivity of the medium
solv.backend = 'cupy'  # You can choose numpy or dask (dask is for running parallel but maybe not working correctly) Numpy and torch are the most stable ones. We suggest to run torch since is the fastest one and the one that we are developing to accelerate with GPU.
# solv.backend = 'torch'  # You can choose numpy or dask (dask is for running parallel but maybe not working correctly) Numpy and torch are the most stable ones. We suggest to run torch since is the fastest one and the one that we are developing to accelerate with GPU.
solv.engine='tensor_trace'  # This is the engine that will be used to compute the  simulation. Is also the most stable one. Don't try to choose another option if you are not sure what you are doing
solv.mode = 'reflection' # you can change to ['transmission', 'reflection', 'full_reflection', 'incoming_field', 'dipole_response', 'propagation'] depending on what you want to calculate
solv._fast_calculation_incoming_field = True  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have energy in the source function. We also skip some frequencies that don't contribute to the final result. Set to False to include all the frequencires and elements
solv.apply_causality = True # This is to check that the computations are inside of the time window we are looking for. This means that it will filter out all those fracture elements that are too far from the antennas and will not show in the time window. Not calculating if _fast_calcualtion_incoming_field is False
solv.filter_energy = True  # Here is to look at the incoming field on the fracture for an specific frequency and look at the energy that is propagating. We will filter out the lower n% of that energy for all those fracture elements. This way we can reduce the computation time by masking out some elements that don't contribute to the final result
solv._filter_percentage = 0.01  # here we set the amount of energy to filter out

#%%
solv.h5_tree()
#%%
solv.file_read('midpoint')
#%%
freqt = solv.time_zero_correction()
# if any errors, try adding a bigger time delay to the source function
print(f'\n++++ \nWe need to remove {solv._t0_correction:.2f} ns to account for the time delay introduced when setting the source function \n++++ \n')
#%%
freq = solv.forward_pass(overwrite=True)
#%%
import numpy as np
import matplotlib.pyplot as plt
time_response, time_vector = solv.get_ifft(freq)
# Now we can visualize using matplotlib
time_response_n = time_response.T /  np.max(np.abs(time_response))
fig, ax = plt.subplots(1,1, figsize=(10,8))

extent = (0, solv.ntraces, time_vector[-1], time_vector[0])
cax = ax.imshow(time_response_n, aspect='auto', cmap='RdBu', extent=extent, vmin=-1, vmax=1 )
ax.set_xlabel('Trace number (Tx-Rx pair)')
ax.set_ylabel('Two-way Travel-Time (ns)')

ax2 = ax.twinx()
radial_distance = time_vector * solv.velocity / 2 # To account for the 2 way travel time
ax2.set_ylim(radial_distance[-1], radial_distance[0])
ax2.set_ylabel('Radial distance (m)')

ax.xaxis.set_ticks(np.arange(0, solv.ntraces, 2))
ax.yaxis.set_ticks(np.arange(time_vector[0], time_vector[-1], 50))
ax2.yaxis.set_ticks(np.arange(time_vector[0], time_vector[-1], 50) * solv.velocity / 2)

ax.tick_params(axis='both', which='both', direction='out', length=5)
ax2.tick_params(axis='both', which='both', direction='out', length=5)


ax.grid(linestyle='--')

plt.tight_layout(pad=0.5)
plt.colorbar(cax, ax=ax, label='Normalized amplitude', orientation='horizontal')
plt.show()
