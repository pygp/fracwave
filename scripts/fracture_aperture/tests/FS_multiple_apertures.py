import numpy as np
import matplotlib.pyplot as plt
from fracwave import SourceEM, FracEM, Antenna, FractureGeo, PACKAGE_DIR, OUTPUT_DIR
from fracwave.utils.help_decorators import (generate_tree_h5_file, generate_tree_h5_file_attrs, convert_frequency,
                                            convert_meter)
import h5py

odir = OUTPUT_DIR+'fracture_aperture/'

file_h5 = odir + 'test_multiple_apertures.h5'
ant = Antenna()
ant.set_profile(name_profile='single',
                transmitters=[[0,0,0]],
                receivers=[[0,0,0]],
                orient_receivers=[[0,0,1]],
                orient_transmitters=[[0,0,1]],
                overwrite=True)

ant.export_to_hdf5(file_h5, overwrite=True)

sou = SourceEM()
sou.type = 'ricker'
sou.set_time_vector(np.linspace(0, 50, 2001))
sou.set_delay(delay=1)
sou.set_center_frequency(2)
# sou.set_source_params(a=15,
#                       c=1,
#                       loc=0,
#                       scale=0.2)
sou.create_source()
# sou.widgets().show()

fig = sou.plot_waveforms_zoom()
fig = sou.plot_waveforms_complete()
fig.show()
sou.export_to_hdf5(file_h5, overwrite=True)

frac = FractureGeo()
v, f = frac.create_regular_squared_mesh(width=0.1,
                                        length=0.1,
                                        resolution=[2, 2],
                                        dip=90,
                                        azimuth=90,
                                        center=((0.5, 0, 0)))
# surf = frac.create_pyvista_mesh(v,f)
# from bedretto import model
# p = model.ModelPlot()
# p.add_object(ant.Transmitter, name='tx')
# p.add_object(surf, name='surface')
for aperture in np.arange(0,0.1,0.001):
    kwarg_properties = dict(aperture=aperture,
                            electrical_conductivity=0,
                            electrical_permeability=81)
    frac.set_fracture(name_fracture=f'ap_{np.round(aperture*1000,0).astype(int)}_mm',
                      vertices=v,
                      faces=f,
                      overwrite=True,
                      **kwarg_properties)
frac.export_to_hdf5(file_h5, overwrite=True)

solv = FracEM()
# solv.mode = 'reflection'
solv.mode = 'reflection'
solv._propagation_mode = 1
solv._fast_calculation_incoming_field = False
solv.rock_epsilon = 5  # Electrical permittivity of the medium
solv.rock_sigma = 0  # Electrical conductivity of the medium

solv.backend = 'torch'
solv.engine = 'tensor_trace'
assert solv._old_solver == True
assert solv._scaling == True

solv.open_file(file_h5)
freq = solv.forward_pass(mask_frequencies=False, overwrite=True)

indiv_fracture_f = solv.file_read('simulation/individual_fracture_response', sl=0)

indiv_fracture = np.zeros((solv.nelements, solv.nfrequencies)).astype(complex)
mask_freq = solv.file_read('simulation/mask_frequencies')
indiv_fracture[:, mask_freq] = indiv_fracture_f

time_response, time_vector = solv.get_ifft(indiv_fracture)

extent=[0, solv.nelements, time_vector[-1], time_vector[0]]

fig, ax = plt.subplots(figsize=(20,5))
ax = solv.plot_wiggle(time_response.T, scale=1, extent=extent, ax=ax, fill=False)
ax.set_xlabel('Aperture (mm)')
ax.set_xlabel('Two-way Travel-time (ns)')
plt.show()


plt.plot(time_vector, time_response[12]);plt.show()
#%%
fig, ax = plt.subplots(figsize=(20,5))
ax = solv.plot_wiggle(time_response.T, scale=2, extent=extent, ax=ax, fill=False)
ax.set_xlabel('Aperture (mm)')
ax.set_xlabel('Two-way Travel-time (ns)')
ax.set_ylim(30, 4)
plt.show()


