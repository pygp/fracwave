import matplotlib.pyplot as plt

from fracwave import FracEM, FractureGeo, DATA_DIR, OUTPUT_DIR, Antenna
from bedretto import model
import numpy as np
import pyvista as pv
from tqdm.autonotebook import tqdm
import os, re
#%%
p = model.ModelPlot()

#%%
from gdp.import_export.import_data import load_sgy
from gdp.processing import detrend
from gdp.processing.image_processing import remove_svd
from gdp.processing.gain import apply_gain


#%%########### 1) Load the geometry from the model ############
frac = FractureGeo()

width = 30
length = 30
max_element_size = 0.05  # 1 mm resolution

v, f = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0, resolution=(10,10), center=(17,17,0))
surf, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)

surf = frac.create_pyvista_mesh(vertices, faces)
#%%
outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
d = np.load(outname + 'files.npz')
top_surface_m = d[('top_surface_m')]
bottom_surface_m = d['bottom_surface_m']
apertures = d['apertures']
spacing = [600, 600]
midpoints = d['midpoints']
apertures_s = apertures.reshape(spacing)
#%%
# apertures = apertures.reshape(spacing)
apertures = apertures[faces].mean(axis=1)
# apertures = apertures.reshape(spacing)
#%%
surf['apertures'] = apertures
p.add_object(surf, name='fracture', scalars='apertures', show_edges=False, opacity=0.5)
# p.add_object(surf, name='fracture', color='yellow', show_edges=True, opacity=0.2)

#%%

# base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20231017_162905'
base_path = '/home/daniel/Documents/PhD_Manuscript2_Data/SEGY_20240114_124840_GypsumBlock'


# Get all the files in the folder
folders = os.listdir(base_path)

# loop through all the folders and get the files. From the filename extract the information.
# The information is divided by the following format {nameexperiment}_{date-YYMMDD}_{ID}_{experimentnumber}_{no useful information}.sgy

# Create a dictionary to store the information
data = {'A':{},
         'B':{}}
vmax = 0
for folder in folders:
    # Get the files
    files = os.listdir(os.path.join(base_path, folder))
    for file in files:
        info = re.split('_', file)
        # identif = info[0]
        # Get the date
        # date = info[1]
        # Get the ID
        ID = info[1]
        # Get the path
        experiment_number = int(info[2])
        print(experiment_number)
        path = os.path.join(base_path, folder, file)
        # Get the data
        data_cube, header = load_sgy(path)
        # if ID == 'B':  # It is flipped
        #     data_cube = np.fliplr(data_cube)
        # Store the data in the dictionary
        # d = {experiment_number: {'header':header, 'data': data_cube}}
        # Processing
        d_de = detrend(data_cube)
        sf = float(header.loc['Sampling Rate [GHz]', 1]) * 1e3  # To convert from GHz to MHz
        c0 = 299_792_458  # Speed of light in m/s
        rock_epsilon = 6.5  # Relative permitivity of the medium (dielectric constant)
        velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
        d_de_ga, gain_matrix = apply_gain(d_de, sfreq=sf, gain_type='linear')
        # d_de_ga = d_de_ga[:400,:]  # Crop the data just to show the top reflections
        data_uncut = d_de_ga.copy()
        lims = (70, 100)
        # lims = (100, 150)
        d_de_ga[:lims[0]:,:] = 0 # crop all reflections from below
        d_de_ga[lims[1]:,:] = 0 # crop all reflections from below
        time_vector = np.linspace(0, float(header.loc['Time Window [ns]', 1]), len(data_cube))
        depth_vector = time_vector * velocity * 0.5

        d = {'header': header, 'data_raw': data_cube, 'data_uncut':data_uncut, 'data': d_de_ga, 'time_vector':time_vector, 'depth_vector':depth_vector,
             'lims': lims }
        data[ID][experiment_number] = d
        if np.abs(np.max(d_de_ga)) > vmax:
            vmax=np.abs(np.max(d_de_ga))
        # data[experiment_number] = {'experiment_name': experiment_name, 'date': date, 'ID': ID, 'data': data_cube}

#%% Detrend
# d_de = detrend(dat)

#%% Remove direct wave
# d_de_svd, svd_matrix = remove_svd(d_de)
# #%%
# sf = float(header.loc['Sampling Rate [GHz]',1]) * 1e3 # To convert from GHz to MHz
# c0 = 299_792_458  # Speed of light in m/s
# rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
# velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)
# d_de_ga, gain_matrix= apply_gain(d_de, sfreq=sf, gain_type='spherical', velocity=velocity)
# #%%
# from gdp.plotting import plot_frequency_information
# plot_frequency_information(d_de[:, 30], dt=1/sf)
 # No real need to do filtering


#%%
inf = data['A'][10]
lims = inf['lims']
# sl_time = slice(0,400)
# sl_tr = slice(10,80)
d = inf['data']
vmax=d.max()
# depth_vector =  data['A']['001']['depth_vector'] * 100
time_vector =  data['A'][7]['depth_vector'] * 100#[sl_time]
extent = (0, d.shape[1], time_vector[-1], time_vector[0])
# vmax =  np.max(np.abs(d))
plt.imshow(d, aspect='auto', cmap='RdBu',interpolation='None', vmin =-vmax, vmax =vmax,
           # extent=extent
           )

plt.ylabel('Distance (cm)')
# plt.ylabel('Two way travel time (ns)')
plt.colorbar()
plt.show()


#%%###
ant = Antenna()
separation = 3.6  # cm

thickness = 1.125  # cm
positions = []
# pi = 8 - 4.5  # starting 8cm to the right and then 2.1 sm to the left as the thickness of the ruler, then 4.5cm to the left as the first antenna position
pi = -0.5
positions.append(pi)
for i in range(0, 28):
    positions.append(positions[-1] + thickness)
    # if the number is even append otherwise not
    # if i % 4 == 0:
    #     positions.append(positions[-1] + 4.5 - 2.17)
    # else:
    #     positions.append(positions[-1] + 2.17)

positions = np.array(positions)

x = positions.copy()
y = positions.copy()

top = 10  # cm to top
line = np.arange(-9.73+4.5, 49.73-4.5, 0.5)
ltx = line + separation / 2
lrx = line - separation / 2
for n, i in enumerate(x):#[1:-1]):
    tx = np.zeros((len(line),3))
    tx[:,0] = i
    tx[:,1] = ltx
    tx[:,2] = top

    rx = tx.copy()
    rx[:,1] = lrx

    orient = np.zeros((len(line),3))
    orient[:,1] = 1

    ant.set_profile(name_profile=f'A_{n}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=line,
                    depth_Tx=line,
                    overwrite=True)

line2 = np.arange(34+10-0.27-4.5, 34+10-60+4.5, -0.5)
ltx2 = line2 #+ separation / 2
lrx2 = line2 #- separation / 2
for n, i in enumerate(y):#[1:-1]):
    tx = np.zeros((len(line2), 3))
    tx[:, 0] = ltx2
    tx[:, 1] = i - separation / 2
    tx[:, 2] = top

    rx = tx.copy()
    # rx[:, 0] = lrx2
    rx[:, 1] = i + separation / 2

    orient = np.zeros((len(line2), 3))
    orient[:, 0] = 1

    ant.set_profile(name_profile=f'B_{n}',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=line2,
                    depth_Tx=line2,
                    overwrite=True)
# ant.export_to_hdf5(file, overwrite=True)
ant.plot()
#%%
# p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
# p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)

middle_points = (ant.Receiver + ant.Transmitter)*0.5
p.add_object(middle_points, name='mid', color='black', opacity=0.2)

#%%
energy_all = []
energy_all_xyz = []
count = 0

# skip = ['AL_001', 'AL_002', 'AL_008', 'BL_001', 'BL002', 'BL008']
skip = []
measurements = {}
for key, di in data.items():
    for key2, di2 in di.items():
        if key =='A':
            continue
        name = f'{key}_{key2}'
        if name in skip:
            continue
        dat = di2['data']
        t = di2['time_vector']
        de = di2['depth_vector'] * 100 # to convert to cm

        # mask = (de >=0) & (de < 50)
#         mask = (t >=2) & (t < 4.5)
#         dat = dat[mask]
#         t = t[mask]
#         de = de[mask]
# # for i in all_profiles:
#
#         dat /= vmax

        df = ant.profiles.loc[ant.profiles.profile == name]
        print(name, len(df))
        patha = (df[['Rx', 'Ry', 'Rz']].to_numpy() + df[['Tx', 'Ty', 'Tz']].to_numpy() )/2
        end = dat.shape[1]-5
        # assert end <= len(patha), 'The length of the data is larger than the length of the path. Check the data and the path.'
        # if len(patha) < end:
        patha = patha[:end]

        energy = np.abs(dat).max(axis=0)[:end]

        # if key == 'A' and key2 == 10:
        #     plt.plot(patha[:,1], energy, '.')
        #     plt.show()
        #     break

        energy_all.append(energy)
        energy_all_xyz.append(patha)
        assert len(energy) == len(patha), name
        # count += len(dat)
        measurements[name] = {'energy': energy, 'path': patha}
        # z_spacing = np.diff(de).mean()
        # nsamples, ntraces = dat.shape
        # points = np.repeat(path, nsamples , axis=0)

        # tp = np.arange(0, z_spacing * nsamples, z_spacing)
        # tp = path[:, 2][:, None] - tp
        #
        # points[:, -1] = tp.ravel()
        #
        # grid = pv.StructuredGrid()
        # grid.points = points
        # grid.dimensions = nsamples, ntraces, 1
        #
        # # Add the data array - note the ordering
        # grid["values"] = dat.ravel(order="F")
        #
        # # all_grids[i] = (grid)
        #
        # p.add_object(grid, name=f'{key}_{key2}', cmap='seismic', scalars='values', opacity=0.9, clim=(-1, 1))


#%% Remove all profiles
for key, di in data.items():
    for key2, di2 in di.items():
        p.remove(f'{key}_{key2}')
#%%
p.remove_scalar_bar()
energy_all_ = np.hstack(energy_all)
energy_all_xyz_ = np.vstack(energy_all_xyz)
# energy_all_xyz_[:,2] += 20
# clim = (5.66e-09, 3.62e-07)
p.add_object(energy_all_xyz_, name='on_surface', cmap='turbo', scalars=energy_all_)#, clim=clim)
# energy_all_xyz_[:,0] +=70
# p.add_object(energy_all_xyz_, name='on_surface2', cmap='turbo', scalars=energy_all_)#, clim=clim)



#%%############################# 2) Load the 3d Model scanned surface #########################
# Import the stl file using pyvista
stl_file = OUTPUT_DIR + 'fracture_aperture/aperture.stl'
stl_mesh = pv.read(stl_file)

#%%
try:
    thickness_results = np.load(OUTPUT_DIR + 'fracture_aperture/thickness_results.npy')
except FileNotFoundError:
    def shoot_rays_and_get_thickness(mesh, xy_positions):
        thickness_results = []

        for xy_position in tqdm(xy_positions):
            # Generate rays along the z-axis at the specified xy position
            rays_start = np.array([*xy_position, -8])
            rays_end = np.array([*xy_position, 8])
            # Shoot rays through the mesh
            intersection_points, _ = mesh.ray_trace(rays_start, rays_end)

            # Extract the intersection points
            # intersection_points = intersections.points

            # Calculate thickness based on the z-values of the intersection points
            if len(intersection_points) == 0:
                thickness_results.append(np.array([*xy_position, 0]))
                continue
            thickness = np.max(intersection_points[:, 2]) - np.min(intersection_points[:, 2])
            thickness_results.append(np.array([*xy_position, thickness]))

        return np.asarray(thickness_results)

    xy_positions = midpoints[:,:2] * 10

    thickness_results = shoot_rays_and_get_thickness(stl_mesh, xy_positions)

    np.save(OUTPUT_DIR + 'fracture_aperture/thickness_results.npy', thickness_results*0.1)
#%%
aperture_model = apertures_s
apertures_real = thickness_results[:,2].reshape(spacing)
difference = apertures_real - aperture_model

vmax = apertures_real.max()
diff_vmax = -0.3#difference.min()
fig, (ax1,ax2,ax3) = plt.subplots(1,3, figsize=(20,5))
cax1 = ax1.imshow(aperture_model, cmap='viridis', vmin=0, vmax=vmax)
cax2 = ax2.imshow(apertures_real, cmap='viridis', vmin=0, vmax=vmax)
cax3 = ax3.imshow(difference, cmap='seismic', clim=(diff_vmax,np.abs(diff_vmax)))
fig.colorbar(cax1, ax=ax1)
fig.colorbar(cax2, ax=ax2)
fig.colorbar(cax3, ax=ax3)
plt.show()
#%%############################# 3) Make a surface based on the fresnel average #########################

def fresnel(distance, wavelength, n_zones):
    return np.sqrt(wavelength * distance * n_zones)*0.5

distance = 10  # Top to the view
rock_epsilon = 6.5
c0 = 299_792_458
velocity = c0 * 1e-7 / np.sqrt(rock_epsilon)
frequency = 3.6 # GHz
wavelength = velocity / frequency  # Result in cm

r = fresnel(distance, wavelength, 1)

#%%
extent = (-15,15,-15,15)

fig, ax = plt.subplots()
ax.imshow(apertures_real, cmap='viridis', extent=extent)
circle = plt.Circle((0,0), r, color='red', fill=False)
ax.add_patch(circle)
plt.show()

#%%
try:
    apertures_fresnel = np.load(OUTPUT_DIR + 'fracture_aperture/apertures_fresnel.npy')
except FileNotFoundError:
    def apertures_inside_circle(point_cloud, apertures, circle_origin, circle_radius):
        # Convert the point cloud to a NumPy array for easier calculations
        point_cloud_array = np.array(point_cloud) + 100
        circle_origin =  np.array(circle_origin) + 100

        # Calculate the distances from each point to the circle's origin
        distances = np.linalg.norm(point_cloud_array - circle_origin, axis=1)

        # Find indices of points that are inside the circle (distance < radius)
        inside_circle_indices = np.where(distances < circle_radius)[0]
        point_cloud_array -= 100
        circle_origin -= 100
        # Extract the points that are inside the circle
        points_inside_circle = point_cloud_array[inside_circle_indices]
        apertures_inside_cirlce = apertures[inside_circle_indices]

        return points_inside_circle, apertures_inside_cirlce

    apertures_fresnel2 = []
    position_fresnel = []
    for m in tqdm(middle_points):
        points, apert = apertures_inside_circle(frac.get_midpoints(vertices, faces)[:,:2], apertures, m[:2], r)
        if points.size == 0:
            apertures_fresnel2.append(0)
            position_fresnel.append([0])
            continue
        apertures_fresnel2.append(np.mean(apert))
        position_fresnel.append(points)

    apertures_fresnel = np.c_[middle_points[:,0],middle_points[:,1], apertures_fresnel2]
    np.save(OUTPUT_DIR + 'fracture_aperture/apertures_fresnel.npy', apertures_fresnel)

#%%
p.remove_scalar_bar()
p.add_object(middle_points, name='on_surface3', cmap='turbo', scalars=apertures_fresnel[:,-1],)#, clim=clim)

#%%
plt.tripcolor(apertures_fresnel[:,0], apertures_fresnel[:,1], apertures_fresnel[:,-1], cmap = 'viridis')
plt.xlim(0,34)
plt.ylim(0,34)
plt.colorbar()
plt.show()

#%%%%%%%%%%%%%%%%%%%5 4) Let's load the simulated response
# Load the data
import pickle
pickle_result = os.path.abspath('/output/manuscript2/gypsum_block_1cm_output_simulation.pickle')

with open(pickle_result, 'rb') as f:
    data_simulation = pickle.load(f)
#%%
profile = 'A_10'

fig, ax = plt.subplots()
dat = data_simulation[profile]['data']
vec = data_simulation[profile]['time_vector']
extent = (0, len(dat), vec[-1], vec[0])
radial_distance = velocity*0.01 * time_vector*0.5
extent = (0, len(dat), radial_distance[-1], radial_distance[0])
cax = ax.imshow(dat.T, aspect='auto', cmap='RdBu',interpolation='None',# vmin =-vmax, vmax =vmax,
             extent=extent
              )
fig.colorbar(cax, ax=ax)
plt.show()
#%%
from scipy.spatial import KDTree

def make_profile(name):
    """
    Choose the name of the profile to plot and create a cross section plot showing all the apertures, gpr response and
    the fresnel zone
    Args:
        name:

    Returns:

    """
    # Pont cloud of all profiles
    df = ant.profiles.loc[ant.profiles.profile == name]
    prof = (df[['Tx','Ty']].to_numpy() + df[['Rx','Ry']].to_numpy()) * 0.5

    xmin = prof[:,0].min()
    xmax = prof[:,0].max()
    ymin = prof[:, 1].min()
    ymax = prof[:, 1].max()
    extent = [xmin, xmax, ymin, ymax]

    width = xmax - xmin
    height = ymax - ymin

    # Determine the long axis based on width and height
    if width >= height:
        long_axis = 0
        vmin = xmin
        vmax = xmax
        const = ymin
    else:
        long_axis = 1
        vmin = ymin
        vmax = ymax
        const = xmin

    # Create equally spaced vector along the long axis
    l = np.arange(vmin, vmax, 0.1)
    profile_vector = np.ones((len(l), 2)) * const
    # l = np.vstack([profile_vector, profile_vector]).T

    profile_vector[:, long_axis] = l


    midpoints = frac.get_midpoints(vertices, faces)[:, :2]

    copy_thickness_results = thickness_results.copy()
    copy_thickness_results += np.asarray((17,17, 0))

    kdtree = KDTree(midpoints)  # Assuming 2D point cloud; adjust accordingly
    kdtreereal = KDTree(copy_thickness_results[:,:2])
    # Find nearest neighbors for each point in the path
    profile_points = []
    profile_points_real = []
    for path_point in profile_vector:
        _, idx = kdtree.query(path_point, workers=-1)#, distance_upper_bound=0.1)
        profile_points.append(idx)
        _, idx = kdtreereal.query(path_point, workers=-1)#, distance_upper_bound=0.1)
        profile_points_real.append(idx)



    fig, ax = plt.subplots(figsize=(10,5))
    ax.plot(apertures_fresnel[df.index, long_axis], apertures_fresnel[df.index, -1], color='red', marker='.', label='Fresnel apertures')
    ax.plot(midpoints[profile_points, long_axis], apertures[profile_points], color='green', label='Model_apertures')
    ax.plot(copy_thickness_results[profile_points_real, long_axis], copy_thickness_results[profile_points_real, 2], color='yellow', label='Real_apertures')

    ax2 = ax.twinx()
    ax2.plot(measurements[name]['path'][:,long_axis], measurements[name]['energy'] , color='blue', marker='.', label='GPR aperture')

    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    ax.set_xlabel('aperture (cm)')
    ax.set_ylabel(f'distance (cm) along axis {long_axis}')
    ax.set_title(name + '   Extent:'+str(extent))
    ax2.set_ylabel('GPR amplitudes (-)')
    ax2.spines['right'].set_color('blue')
    ax2.tick_params(axis='y', colors='blue')
    ax2.yaxis.label.set_color('blue')
    ax.grid()
    plt.show()


#%%%%%%%%%%%%%%%%%%%%%%5 Now plot all in 2D
fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(20,5))
extent = (2, 32, 3, 32)
cax1 = ax1.imshow(np.rot90(apertures_real), cmap='turbo', extent=extent)

# cax2 = ax2.tripcolor(apertures_fresnel[:,0], apertures_fresnel[:,1], apertures_fresnel[:,-1], cmap = 'viridis')
cax2 = ax2.scatter(apertures_fresnel[:,0], apertures_fresnel[:,1], c=apertures_fresnel[:,-1], cmap = 'turbo', s=12)

cax3 = ax3.scatter(energy_all_xyz_[:,0], energy_all_xyz_[:,1], c=energy_all_, cmap = 'turbo', s=12)

for ax in (ax1,ax2, ax3):
    ax.set_xlim(2,32)
    ax.set_ylim(2,32)
fig.colorbar(cax1, ax=ax1)
fig.colorbar(cax2, ax=ax2)
fig.colorbar(cax3, ax=ax3)
ax1.set_title('Real Apertures')
ax2.set_title('Fresnel Apertures')
ax3.set_title('GPR Apertures')
plt.show()

#%%
p = model.ModelPlot()

surf['apertures'] = apertures
p.add_object(surf, name='fracture', scalars='apertures', show_edges=False, opacity=0.5)
# p.add_object(surf, name='fracture', color='yellow', show_edges=True, opacity=0.2)


p.remove_scalar_bar()
energy_all_ = np.hstack(energy_all)
energy_all_xyz_ = np.vstack(energy_all_xyz)
# energy_all_xyz_[:,2] += 20
# clim = (5.66e-09, 3.62e-07)
p.add_object(energy_all_xyz_, name='on_surface', cmap='turbo', scalars=energy_all_)#, clim=clim)
# energy_all_xyz_[:,0] +=70
# p.add_object(energy_all_xyz_, name='on_surface2', cmap='turbo', scalars=energy_all_)#, clim=clim)


# p.add_object(middle_points, name='on_surface', cmap='turbo', scalars=apertures_fresnel[:,-1],)#, clim=clim)
