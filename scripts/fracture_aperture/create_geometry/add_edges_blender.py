import numpy as np
import bpy

# Design de edges: This is done by creating a plane in the contact areas with an extra of 
# 2cm per side, then extrude the plane to a cube to the same size in z. Then create the same size 
# in z but to the same shape of the mold to apply a boolean operator and leave the difference.

# Extract the minimum size 
top_surface = True
name_mesh='Surface'
obj = bpy.data.objects.get(name_mesh)
mesh = obj.data
# Calculate the minimum Z value of the vertices
min_z = min(vert.co.z for vert in mesh.vertices)

# Set the dimensions of the plane (e.g., 2 units by 3 units)
width_o = 34
width_i = 30
length_o = 34
length_i = 30

# Add a plane mesh
bpy.ops.mesh.primitive_plane_add(size=1, enter_editmode=False, align='WORLD', location=(0, 0, 0))
plane_o = bpy.context.object
bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(0, 0, 0))
plane_i = bpy.context.object

# Scale the plane to the desired dimensions
plane_o.scale.x = width_o
plane_i.scale.x = width_i
plane_o.scale.y = length_o
plane_i.scale.y = length_i
plane_i.scale.z = 10

name_plane_o = 'Edge_o'
name_plane_i = 'Edge_i'
plane_o.name = name_plane_o
plane_i.name = name_plane_i

#for n in [name_plane_o, name_plane_i]:
object_name = name_plane_o  # Replace with the name of your object
bpy.ops.object.select_all(action='DESELECT')  # Deselect all objects
bpy.data.objects[object_name].select_set(True)
bpy.context.view_layer.objects.active = bpy.data.objects[object_name]
# Switch to Edit Mode
bpy.ops.object.mode_set(mode='EDIT')

# Select all vertices (you can adjust the selection method as needed)
bpy.ops.mesh.select_all(action='SELECT')

# Extrude the selected vertices in the negative Z direction
bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0, 0, min_z)})
# Switch back to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')

# Update the scene
bpy.context.view_layer.update()


########## Now we need to do the bolean operator

modifier = plane_o.modifiers.new(name="Boolean", type='BOOLEAN')
modifier.operation = 'DIFFERENCE'
modifier.object = plane_i

# Apply the Boolean modifier to create the hollow cube
bpy.context.view_layer.objects.active = plane_o
bpy.ops.object.modifier_apply(modifier=modifier.name)

bpy.data.objects.remove(plane_i)

# Deselect objects
#obj1.select_set(False)
#obj2.select_set(False)

#bpy.data.objects.remove(obj2)

# Update the scene
bpy.context.view_layer.update()

##########
locations1 = (7,16,0)
locations2 = (-2,16,0)
locations3 = (16,5,0)
locations4 = (16,-3,0)
locations5 = (10,-16,0)
locations6 = (-12,-16,0)
locations7 = (-16, 2, 0)
locations8 = (-16,-8, 0)
## Spheres
l = np.asarray([locations1, locations2, locations3, locations4, locations5, locations6, locations7, locations8])

radius=0.5

for loc in l:
    bpy.ops.mesh.primitive_uv_sphere_add(radius=radius, location=loc)
    sphere = bpy.context.object
    # Add the sphere to the collection
    
    modifier = plane_o.modifiers.new(name="Boolean", type='BOOLEAN')
    if top_surface:
        modifier.operation = 'UNION'
    else:
        modifier.operation = 'DIFFERENCE'
        
    modifier.object = sphere

    # Apply the Boolean modifier to create the hollow cube
    bpy.context.view_layer.objects.active = sphere
    bpy.ops.object.modifier_apply(modifier=modifier.name)

    bpy.data.objects.remove(sphere)

    bpy.context.view_layer.update()
#    
#    spheres_collection.objects.link(sphere)

# Update the scene
bpy.context.view_layer.update()