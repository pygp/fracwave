import numpy as np
import bpy
import math
import bmesh


folder = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/'
# Load vertices and faces
file = folder+'contours/vertices_faces_surfaces.npz'
negative = True   # Negative true means to model for the mold. If false then it models the blocks shape to fit the prints
top_surface = True

#################################################################################################### Delete all 
# Deselect all objects (if any are selected)
bpy.ops.object.select_all(action='DESELECT')

# Delete all objects in the scene
bpy.ops.object.select_by_type(type='MESH')
bpy.ops.object.delete()

# Remove all materials from objects if they have any
for obj in bpy.data.objects:
    if obj.type == 'MESH':
        obj.data.materials.clear()

# Delete all textures if they exist
if bpy.data.textures:
    for texture in bpy.data.textures:
        bpy.data.textures.remove(texture)

# Delete all images if they exist
if bpy.data.images:
    for image in bpy.data.images:
        bpy.data.images.remove(image)

# Delete all lamps if they exist
if bpy.data.lights:
    for lamp in bpy.data.lights:
        bpy.data.lights.remove(lamp)

# Delete all cameras if they exist
if bpy.data.cameras:
    for camera in bpy.data.cameras:
        bpy.data.cameras.remove(camera)

# Update the scene
bpy.context.view_layer.update()
####################################################################################################
d = np.load(file)

top_vertices=d['top_vertices']
top_faces=d['top_faces']
bot_vertices=d['bot_vertices']
bot_faces=d['bot_faces']

mesh = bpy.data.meshes.new("CustomMesh")
name_mesh = 'Surface'
obj = bpy.data.objects.new(name_mesh, mesh)

bpy.context.scene.collection.objects.link(obj)

if top_surface:
    mesh.from_pydata(top_vertices, [], top_faces)
else:
    mesh.from_pydata(bot_vertices, [], bot_faces)
mesh.update()

object_name = name_mesh  # Replace with the name of your object
bpy.ops.object.select_all(action='DESELECT')  # Deselect all objects
bpy.data.objects[object_name].select_set(True)
bpy.context.view_layer.objects.active = bpy.data.objects[object_name]
    
if top_surface and not negative:
    # Apply the rotation to flip the mesh upside down
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    # Rotate the object 180 degrees around the X-axis to flip it upside down
    bpy.context.object.rotation_euler[0] += math.pi
    
elif not top_surface and negative:
    # Apply the rotation to flip the mesh upside down
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    # Rotate the object 180 degrees around the X-axis to flip it upside down
    bpy.context.object.rotation_euler[0] += math.pi

# Switch to Edit Mode
bpy.ops.object.mode_set(mode='EDIT')

# Select all vertices (you can adjust the selection method as needed)
bpy.ops.mesh.select_all(action='SELECT')

# Extrude the selected vertices in the negative Z direction
bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0, 0, -5)})

# Switch back to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')

# Update the scene
bpy.context.view_layer.update()


# Clear the selection
bpy.ops.object.select_all(action='DESELECT')
    

######## Cube #############
# Define cube dimensions
length = 30  # 30cm 
width = 30   # 30cm 
name_cube = 'Cube'

l = [vert.co.z for vert in mesh.vertices]
min_val = min(l)
max_val = max(l)

#if top_surface:
#    min_val = top_vertices[:,-1].min()
#    max_val = top_vertices[:,-1].max()
#else:
#    min_val = bot_vertices[:,-1].min()
#    max_val = bot_vertices[:,-1].max()

#height = np.abs(min_val) + np.abs(max_val)
height = 20
zpos = min_val + height *0.5
if top_surface:
    if negative:
        zpos += 0
    else:
        zpos+= 0.2
else:
    if negative:
        zpos += 0.3
        min_val += 0.3
    else:
        zpos += 0.5
            
# Add a cube mesh
bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(0, 0, zpos))
cube = bpy.context.object
# Scale the cube to the desired dimensions
cube.scale = (length, width, height)

# Update the scene
bpy.context.view_layer.update()

########### Add bolean operator 
# Get references to the objects
obj1 = bpy.data.objects.get(name_mesh)
obj2 = bpy.data.objects.get(name_cube)

obj1.select_set(True)
obj2.select_set(True)

bpy.context.view_layer.objects.active = obj1

# Add a Boolean modifier with "INTERSECT" operation
modifier = obj1.modifiers.new(name="Boolean", type='BOOLEAN')
modifier.operation = 'INTERSECT'

modifier.object = obj2

#bpy.context.view_layer.objects.active = obj2
bpy.ops.object.modifier_apply(modifier="Boolean")

# Deselect objects
obj1.select_set(False)
obj2.select_set(False)

bpy.data.objects.remove(obj2)

# Update the scene
bpy.context.view_layer.update()

############## ADDING EDGES ####################

# Design de edges: This is done by creating a plane in the contact areas with an extra of 
# 2cm per side, then extrude the plane to a cube to the same size in z. Then create the same size 
# in z but to the same shape of the mold to apply a boolean operator and leave the difference.

# Extract the minimum size 
#obj = bpy.data.objects.get(name_mesh)
#mesh = obj.data
# Calculate the minimum Z value of the vertices
#min_z = min(vert.co.z for vert in mesh.vertices) # - 0.18
#min_z = min_val 
# Set the dimensions of the plane (e.g., 2 units by 3 units)
width_o = 34
width_i = 30
length_o = 34
length_i = 30

# Add a plane mesh
bpy.ops.mesh.primitive_plane_add(size=1, enter_editmode=False, align='WORLD', location=(0, 0, 0))
#bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(0,0,min_val*0.5))
plane_o = bpy.context.object
bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(0, 0, 0))
plane_i = bpy.context.object

# Scale the plane to the desired dimensions
plane_o.scale.x = width_o
plane_o.scale.y = length_o
#plane_o.scale = (width_o, length_o, np.abs(min_val))
plane_i.scale = (width_i, length_i, 20)

name_plane_o = 'Edge_o'
name_plane_i = 'Edge_i'
plane_o.name = name_plane_o
plane_i.name = name_plane_i

#for n in [name_plane_o, name_plane_i]:
object_name = name_plane_o  # Replace with the name of your object
bpy.ops.object.select_all(action='DESELECT')  # Deselect all objects
bpy.data.objects[object_name].select_set(True)
bpy.context.view_layer.objects.active = bpy.data.objects[object_name]
## Switch to Edit Mode
bpy.ops.object.mode_set(mode='EDIT')

## Select all vertices (you can adjust the selection method as needed)
bpy.ops.mesh.select_all(action='SELECT')

## Extrude the selected vertices in the negative Z direction
bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0, 0, min_val)})
## Switch back to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')

# Update the scene
bpy.context.view_layer.update()


########## Now we need to do the bolean operator

modifier = plane_o.modifiers.new(name="Boolean", type='BOOLEAN')
modifier.operation = 'DIFFERENCE'
modifier.object = plane_i

# Apply the Boolean modifier to create the hollow cube
bpy.context.view_layer.objects.active = plane_o
bpy.ops.object.modifier_apply(modifier=modifier.name)

bpy.data.objects.remove(plane_i)

# Update the scene
bpy.context.view_layer.update()

##########
locations1 = (8,16,0)
locations2 = (-8,16,0)
locations3 = (16,8,0)
locations4 = (16,-8,0)
locations5 = (8,-16,0)
locations6 = (-8,-16,0)
locations7 = (-16, -8, 0)
locations8 = (-16, 8, 0)
## Spheres
l = np.asarray([locations1, locations2, locations3, locations4, locations5, locations6, locations7, 
                locations8])

radius=0.4

for loc in l:
    bpy.ops.mesh.primitive_uv_sphere_add(radius=radius, location=loc)
    sphere = bpy.context.object
    if not top_surface and negative:  # Delete shpere in lower half so it does not interfere
       # Add a plane to cut the sphere in half
        bpy.ops.mesh.primitive_plane_add(size=100, enter_editmode=False, align='WORLD', 
        location=(0,0,0))

        # Get the plane object
        plane = bpy.context.object

        # Add a Boolean modifier to the sphere
        mod = sphere.modifiers.new(name="Cut", type='BOOLEAN')
        mod.operation = 'INTERSECT'
        mod.object = plane

        # Apply the Boolean modifier to cut the sphere in half
        bpy.ops.object.modifier_apply(modifier=mod.name)

        # Delete the plane
        bpy.data.objects.remove(plane)
    
    modifier = plane_o.modifiers.new(name="Boolean", type='BOOLEAN')
    if top_surface:
        if negative:
            modifier.operation = 'DIFFERENCE'
        else:
            modifier.operation = 'UNION'
    else:
        if negative:
            modifier.operation = 'UNION'
        else:
            modifier.operation = 'DIFFERENCE'
        
    modifier.object = sphere

    # Apply the Boolean modifier to create the hollow cube
    bpy.context.view_layer.objects.active = plane_o
    bpy.ops.object.modifier_apply(modifier=modifier.name)

    bpy.data.objects.remove(sphere)

    bpy.context.view_layer.update()
    
# Update the scene
bpy.context.view_layer.update()

#### Join both surfaces
mesh = bpy.data.objects.get(name_mesh)
modifier = mesh.modifiers.new(name="Boolean", type='BOOLEAN')

modifier.operation = 'UNION'
modifier.object = plane_o
bpy.context.view_layer.objects.active = mesh
bpy.ops.object.modifier_apply(modifier=modifier.name)

bpy.data.objects.remove(plane_o)

bpy.context.view_layer.update()

######## SAVE

# Replace with the name of the object that you want to export

# Set the object to be the active object
bpy.context.view_layer.objects.active = bpy.data.objects.get(name_mesh)

# Export the active object to an STL file
if top_surface:
    if negative:
        file_path = folder+"/top_surface_mold.stl"  
        file_blender = folder+"top_surface_model_mold.blend"
    else:
        file_path = folder+"/top_surface.stl"  
        file_blender = folder+"top_surface_model.blend"
else:    
    if negative:
        file_path = folder+"/bot_surface_mold.stl"  
        file_blender = folder+"bot_surface_model_mold.blend"
    else:
        file_path = folder+"/bot_surface.stl"  
        file_blender = folder+"bot_surface_model.blend"

bpy.ops.export_mesh.stl(filepath=file_path)

# Save or export your Blender project with the model
bpy.ops.wm.save_mainfile(filepath=file_blender)

# Optional: If you want to clean up by deleting the exported object
#bpy.data.objects.remove(bpy.data.objects[object_name_to_export], do_unlink=True)

# Update the scene
bpy.context.view_layer.update()


