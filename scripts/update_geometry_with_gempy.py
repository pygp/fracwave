#%% Get the numpy points based on the fracture energy.
from socket import socket

import pyvista as pv
import pandas as pd
import numpy as np
from bedretto import data, geometry, model, DATA_DIR
from fracwave.geometry.fracture_geometry import extrapolate_point
from fracwave import OUTPUT_DIR, Antenna, SourceEM, FracEM, FractureGeo, set_logger
logger = set_logger(__name__)
file = OUTPUT_DIR + 'geometry_inversion.h5'
# Simulation for the 20 MHz data
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.02 # m
#%%
bor_dat = data.BoreholeData()
tn_dat = data.TunnelData()
lo_dat = data.LoggingData(borehole_data=bor_dat)

fa_dat = data.FaultData(borehole_data=bor_dat, tunnel_data=tn_dat)

#%%
# From logging
bb_data = pd.read_csv(DATA_DIR + 'logging_data/BB_logging (copy).csv')
bb_top = bb_data.loc[bb_data['fault_id'] == 'BB-top']
bb_bottom = bb_data.loc[bb_data['fault_id'] == 'BB-bottom']

# fracture_id = '51'  # Bad-boy
tm_range = (1980, 1990)

# t = tn_dat.get_tunnel_fractures(structure_ID=fracture_id)
t = tn_dat.get_tunnel_fractures(TM_range=tm_range)

#%%
tn_geom = geometry.TunnelGeometry(tunnel_data=tn_dat)
tn_mesh = tn_geom.get_tunnel_mesh((1900, 2100))
# bb = tn_geom.get_fracture_planes(fracture_info=t)
#%%
bor_geom = geometry.BoreholeGeometry(borehole_data=bor_dat)
boreholes = {name: bor_geom.construct_borehole_geometry(name) for name in bb_top['Borehole'].unique()}
#%%
p = model.ModelPlot()
[p.add_object(obj, name=name, color='red' if name == 'MB1' else 'black') for name, obj in boreholes.items()]
p.add_object(tn_mesh, name='Tunnel', color='grey', opacity=0.5)
# p.add_object(bb, name='BB', color='yellow', opacity=0.5)
#%%
# xyz_tun = t['Center'][0]
# dir_tun = t[['Azimuth (deg)', 'Dip (deg)']].to_numpy()[0]
xyz_bor = []
dir_bor = []
for i, df in bb_top.iterrows():
    xyz_bor.append(bor_dat.get_xyz_coordinates(df['Borehole'], df['Depth (m)']))
    dir_bor.append(df[['Azimuth (deg)', 'Dip (deg)']].to_numpy())
xyz_bor = np.asarray(xyz_bor)
dir_bor = np.asarray(dir_bor)

# xyz_all = np.vstack([xyz_tun,xyz_bor])
# dir_all = np.vstack([dir_tun,dir_bor])
xyz_all_top = xyz_bor
dir_all_top = dir_bor

#%%
# xyz_tun = t['Center'][0]
# dir_tun = t[['Azimuth (deg)', 'Dip (deg)']].to_numpy()[0]
xyz_bor = []
dir_bor = []
for i, df in bb_bottom.iterrows():
    xyz_bor.append(bor_dat.get_xyz_coordinates(df['Borehole'], df['Depth (m)']))
    dir_bor.append(df[['Azimuth (deg)', 'Dip (deg)']].to_numpy())
xyz_bor = np.asarray(xyz_bor)
dir_bor = np.asarray(dir_bor)

# xyz_all = np.vstack([xyz_tun,xyz_bor])
# dir_all = np.vstack([dir_tun,dir_bor])
xyz_all_bottom = xyz_bor
dir_all_bottom = dir_bor
#%%
tun_point = []
tun_dir = []
for i, df in t.iterrows():
    tun_point.append(df['Center'])
    tun_dir.append(df[['Azimuth (deg)', 'Dip (deg)']].to_numpy())
tun_point = np.asarray(tun_point)
tun_dir = np.asarray(tun_dir)
#%%
xyz_all = np.vstack([xyz_all_bottom, tun_point])
dir_all = np.vstack([dir_all_bottom, tun_dir])
#%%
points = pv.PolyData(xyz_all)
p.add_object(points, name='Points_all', color='red', point_size=10)
#%%


points = pv.PolyData(xyz_all_top)
p.add_object(points, name='Points_top', color='yellow')
#%%
points = pv.PolyData(xyz_all_bottom)
p.add_object(points, name='Points_bottom', color='blue')

#%% All
x = np.linspace(xyz_all[:, 0].min()-20, xyz_all[:, 0].max()+20, 50)
z = np.linspace(xyz_all[:, 2].min()-20, xyz_all[:, 2].max()+5, 50)
xx, zz = np.meshgrid(x, z)

yy = extrapolate_point(X=xx, Y=zz, data=np.c_[xyz_all[:, 0], xyz_all[:, 2], xyz_all[:, 1]], order=2)

grid_all = pv.StructuredGrid(xx, yy, zz)
# #%%
# y = np.linspace(xyz_all_top[:, 1].min()-20, xyz_all_top[:, 1].max()+20, 50)
# z = np.linspace(xyz_all_top[:, 2].min()-20, xyz_all_top[:, 2].max()+60, 50)
# yy, zz = np.meshgrid(y, z)
#
# xx = extrapolate_point(X=yy, Y=zz, data=np.c_[xyz_all_top[:, 1], xyz_all_top[:, 2], xyz_all_top[:, 0]], order=2)
#
# grid_top = pv.StructuredGrid(xx, yy, zz)
#
# #%%
# y = np.linspace(xyz_all_bottom[:, 1].min()-20, xyz_all_bottom[:, 1].max()+20, 50)
# z = np.linspace(xyz_all_bottom[:, 2].min()-20, xyz_all_bottom[:, 2].max()+60, 50)
# yy, zz = np.meshgrid(y, z)
#
# xx = extrapolate_point(X=yy, Y=zz, data=np.c_[xyz_all_bottom[:, 1], xyz_all_bottom[:, 2], xyz_all_bottom[:, 0]], order=2)
#
# grid_bottom = pv.StructuredGrid(xx, yy, zz)

#%%
# p.add_object(grid_top, name='fault_top', color='yellow', opacity=0.7)
# p.add_object(grid_bottom, name='fault_bottom', color='blue', opacity=0.7)
p.add_object(grid_all, name='fault_all', color='blue', opacity=0.5)

#%% Check the distance to the fault in order to add new points. Use borehole positions
def get_points_at_fracture(depths, fracture_points):
    x, y, z = bor_dat.get_xyz_coordinates('MB1', depths)

    # Check with distance matrix
    from scipy.spatial.distance import cdist
    distances = cdist(np.c_[x,y,z], fracture_points)
    pos = []
    for count, po in enumerate(distances):
        pos.append(np.where(po == min(po))[0][0])

    pos = np.asarray(pos)
    return fracture_points[pos]

#%% Check which is the element that has this position
# points_to_add = df.iloc[pos[:,0]]

#%%
# p.add_object(pv.PolyData(points_to_add[['x', 'y', 'z']].to_numpy()), name='close_distance', color = 'red')

#%% Add them to gempy
# for c, i in points_to_add.iterrows():
#     geo_model.add_surface_points(X=i['x'], Y=i['y'], Z=i['z'], surface='F')
# gp.compute_model(geo_model)

#%%
# gp.plot_3d(geo_model, plotter_type='background')
#
# #%%
# vertices = geo_model.solutions.vertices[0]
# faces = geo_model.solutions.edges[0]

# #%%
# surf = frac.remesh(vertices, plane=0, order=2, spacing=resolution)
# #%%
# df = frac.set_fracture_properties(aperture=0.5,
#                                   electrical_conductvity=0.001,
#                                   electrical_permeability=81)
# #%%
# p.add_object(surf, name='Gempy', color='green', opacity=0.7)
# #%%
# frac.export_to_hdf5(filename=file, overwrite=True)
# #%%

def set_default(depths_antenna):
    # ------------- SourceEM
    import matplotlib.pyplot as plt
    from fracwave import SourceEM

    samples = 300
    sou = SourceEM()
    f = np.linspace(0, 0.1, samples)
    sou.frequency = f

    firstp = sou.create_source(a=1,
                               b=0.02,
                               t=5)
    sou.plot()
    plt.xlim(0,0.1)
    plt.show()
    sou.export_to_hdf5(file, overwrite=True)
    # ------------- Antenna
    separation = 2.77
    Rx = depths_antenna
    Tx = depths_antenna + separation
    # xyz_rx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Rx).T
    # xyz_tx = bor_dat.get_coordinates_from_borehole_depth(mb1, depths=Tx).T
    bor_dat = data.BoreholeData()
    xyz_rx = bor_dat.get_xyz_coordinates('MB1', depths=Rx).T
    xyz_tx = bor_dat.get_xyz_coordinates('MB1', depths=Tx).T

    # Orientation dipole
    # -----------
    # Calculate rx orientation by giving 10 cm before and 10 cm after
    rx_1 = Rx - 0.1
    rx_2 = Rx + 0.1

    orient_rx = (bor_dat.get_xyz_coordinates('MB1', depths=rx_1).T - \
                 bor_dat.get_xyz_coordinates('MB1', depths=rx_2).T)
    # orient_rx /= np.linalg.norm(orient_rx, ord=1, axis=1)[:, np.newaxis]
    orient_rx /= np.linalg.norm(orient_rx, ord=1)

    # Calculate tx orientation by giving 10 cm before and 10 cm after
    tx_1 = Tx - 0.1
    tx_2 = Tx + 0.1

    orient_tx = (bor_dat.get_xyz_coordinates('MB1', depths=tx_1).T - \
                 bor_dat.get_xyz_coordinates('MB1', depths=tx_2).T)
    # orient_tx /= np.linalg.norm(orient_tx, ord=1, axis=1)[:, np.newaxis]
    orient_tx /= np.linalg.norm(orient_tx, ord=1)

    ant = Antenna()
    ant.Rx = xyz_rx
    ant.Tx = xyz_tx
    ant.orient_Rx = orient_rx
    ant.orient_Tx = orient_tx

    ant.export_to_hdf5(file, overwrite=True)
    return sou, ant
#%%
traces = 5
depths = np.linspace(20, 200, traces)
sou, ant = set_default(depths_antenna=depths)

#%%
ant.plot(p=p.plotter)

#%%
from fracwave import FracEM

solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.engine = 'tensor_all'
solv.engine = 'tensor_trace'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv.backend = 'torch'

solv.open_file(file)

#%%

freq = solv.forward_pass(overwrite=True, recalculate=False, save_hd5=False)

#%%
time_response, time_vector = solv.get_ifft(freq)

#%%
import matplotlib.pyplot as plt
vmax = np.abs(time_response).max()

plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax)

plt.colorbar()
plt.show()
#%%
incoming_field=np.einsum('tefi->te', freq)
#%%
frac = FractureGeo()
frac.load_hdf5(file)
#%%
solv = FracEM()
solv.load_hdf5(file)
freq = solv.file_read('incoming_field')
incoming_field = np.einsum('tefi->te', freq)

#%%
frac.surface['incoming_field'] = np.abs(incoming_field[0])
#%%
p.add_object(pv.PolyData(frac.vertices), name='Gempy',)# scalars='incoming_field', cmap='viridis', opacity=0.7)

#%%
import os
#Run this cell only if you want to run gempy in cuda mode (requires cuda and a dedicated cpu).
#Initial compilation will take a bit longer but the model will run much faster
# os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cuda"
os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cpu"

import gempy as gp

extent = (-190, 50, -130, 50, 1300, 1490)
resolution = (50,50,50)

geo_model = gp.create_model('Fault zone')
geo_model = gp.init_data(geo_model, extent=extent, resolution=resolution)
gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
geo_model.add_surfaces(['F', "basement"])

#%%
def update_geometry_and_simulate(geo_model, fixed_points, fixed_orientations, move_points, max_element_size,
                                 id_points_fixed: list=[], id_points_move:list=[],
                                 id_orientation_fixed: list=[], id_orientation_move: list=[],
                                 set_properties: bool=True):
    """
    Make the update of the geometry
    Args:
        geo_model: Same geo_model so it is initiallice once
        fixed_points: Points that will not move like the points from logging or tunnel measurements
        fixed_orientations: The orientations (Azimuth and dip) of the points from logging and tunnel measurements.
        move_points: These are the proposed points that will shape the geometry and help constrain it
        extent: Bounding box for the fault geometry
        max_element_size: In the remeshing step, the size of the elements will not be bigger that this value (in meters)
        id_points_fixed: index number of the fixed points. This to not remove them
        id_points_move: index number of points to delete and or move
        id_orinetation_fixed:
        id_orientation_move:
        set_properties: boolean to indicate if properties are set to each element of the geometry
    Returns:
        Simulated response of the fracture
    """
    # Include all fixed points and orientations
    logger.debug(f'Setting {len(fixed_points)} fixed points and orientations from borehole '
                f'logging and tunnel observations')
    s = len(id_points_fixed)
    for i in range(len(fixed_points)):
        if s>0:
            if fixed_points[i] in geo_model.surface_points.df[['X', 'Y', 'Z']].to_numpy():
                continue
        geo_model.add_surface_points(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F')
        geo_model.add_orientations(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F',
                                   orientation=[fixed_orientations[i, 0], fixed_orientations[i, 1], 1])
        id_points_fixed.append(geo_model.surface_points.df.index[-1])
        id_orientation_fixed.append(geo_model.surface_points.df.index[-1])

    # Include all other points
    if move_points is not None:
        logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
        if len(id_points_move)>0:
            [geo_model.delete_surface_points(i) for i in id_points_move]
            id_points_move = []
        for i in range(len(move_points)):
            geo_model.add_surface_points(X=move_points[i, 0], Y=move_points[i, 1], Z=move_points[i, 2], surface='F')
            id_points_move.append(geo_model.surface_points.df.index[-1])
        # TODO: extra orientations
    gp.compute_model(geo_model)
    logger.debug('Model computed')
    vertices = geo_model.solutions.vertices[0]
    faces = geo_model.solutions.edges[0]

    from fracwave import FractureGeo
    frac = FractureGeo()

    resolution = (100, 100)
    logger.debug('Meshing...')
    surf = frac.remesh(vertices, plane=1, order=2, spacing=resolution)
    min_resol = np.ceil(surf.length / max_element_size).astype(int)
    resolution = (min_resol, min_resol)
    if surf.dimensions[0] < min_resol:
        logger.debug('Remeshing...')
        surf = frac.remesh(vertices, plane=1, order=2, spacing=resolution)
    logger.debug(f'Setting properties to {frac.nelements} elements. This can take a few minutes...')
    if set_properties:
        df = frac.set_fracture_properties(aperture=0.5,
                                          electrical_conductvity=0.001,
                                          electrical_permeability=81)
    logger.debug('Done')
    return vertices, faces, geo_model, frac, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move
#%%
vertices, faces, geo_model, frac, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move = \
    update_geometry_and_simulate(geo_model=geo_model,
                                 fixed_points=xyz_all,
                                 fixed_orientations=dir_all,
                                 move_points=None, #move_points,
                                 max_element_size=wavelength/4,
                                 set_properties=False,
                                 )
#%%
frac.export_to_hdf5(file, overwrite=True)

#%%
depths = np.linspace(20, 190, 10)
move_points = get_points_at_fracture(depths, frac.fracture_properties[['x','y','z']].to_numpy())

id_points_fixed=[]
id_orientation_fixed=[]
id_points_move=[]
id_orientation_move=[]

vertices, faces, geo_model, frac, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move = \
    update_geometry_and_simulate(geo_model=geo_model,
                                 fixed_points=xyz_all,
                                 fixed_orientations=dir_all,
                                 move_points=move_points,
                                 max_element_size=wavelength/4,
                                 set_properties=False,
                                 id_points_fixed=id_points_fixed,
                                 id_orientation_fixed=id_points_fixed,
                                 id_points_move=id_points_move,
                                 id_orientation_move=id_points_move
                                 )
#%%
p.add_object(pv.PolyData(move_points), name='move', color='yellow')
p.add_object(frac.surface, name='Gempy', color='green', opacity=0.7)
#%%
gp.plot_3d(geo_model, plotter_type='background')
#%%
#%%
move_points[3, 1] = move_points[3, 1] - 10
id_points_fixed=[]
id_orientation_fixed=[]
id_points_move=[]
id_orientation_move=[]

vertices, faces, geo_model, frac, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move = \
    update_geometry_and_simulate(geo_model=geo_model,
                                 fixed_points=xyz_all,
                                 fixed_orientations=dir_all,
                                 move_points=move_points,
                                 max_element_size=wavelength/4,
                                 set_properties=False,
                                 id_points_fixed=id_points_fixed,
                                 id_orientation_fixed=id_points_fixed,
                                 id_points_move=id_points_move,
                                 id_orientation_move=id_points_move
                                 )
#%%
p.add_object(pv.PolyData(move_points), name='move', color='yellow')
p.add_object(frac.surface, name='Gempy', color='green', opacity=0.7)
#%% Picked surface
from scipy.io import loadmat
import matplotlib.pyplot as plt
file_gpr = '/home/daniel/Documents/gpr_data_mb1/' + 'MB1_20MHz_B200108.mat'
data = loadmat(file_gpr)

travel_time = data['travel_time'][0]
depth = data['depth'][:,0]
data_raw = data['raw_data']
processed_data = data['processed_data']
extent = [depth[0], depth[-1], travel_time[-1], travel_time[0]]
#%% Pick from the image
file_picks_mb1_20mhz = OUTPUT_DIR+'pick_image_MB1_20MHz.npz'
try:
    picks = np.load(file_picks_mb1_20mhz)
    pick_pixels = picks['pick_pixels']
    pick_transform = picks['pick_transform']
except:
    from gdp.processing.image_processing import pick_image
    pick_pixels, pick_transform = pick_image(processed_data, extent=extent, colored=True, interpolate_path=True)
    np.savez(file_picks_mb1_20mhz, pick_pixels=pick_pixels, pick_transform=pick_transform)
#%%
time_cut = (1000,100)
depth_cut = (20,200)
vmax = processed_data.max() /10
plt.imshow(processed_data, cmap = 'seismic', vmin=-vmax,vmax=vmax, extent=extent, aspect='auto')
plt.plot(pick_transform[:,0], pick_transform[:,1], color='green')
plt.ylim(time_cut)
plt.xlim(depth_cut)
plt.ylabel('Time [ns]')
plt.xlabel('Borehole depth [m]')
plt.title('20MHz Processed data')
plt.colorbar()
plt.show()

#%% The picks are my objective function
def check_locations_with_max_energy():#source: SourceEM, antenna: Antenna, geometry: FractureGeo):
    from fracwave import FracEM
    solv = FracEM()
    solv.rock_epsilon = 6.
    solv.rock_sigma = 0.0
    # solv.engine = 'tensor_all'
    solv.engine = 'tensor_trace'

    solv.units = 'SI'
    solv.filter_energy = True
    solv._filter_percentage = 0.5
    solv.backend = 'torch'

    solv.open_file(file)
    solv._fast_calculation_incoming_field = True
    solv.mode = 'incoming_field'

    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=True)

    return solv, freq
#%%
solv, freq = check_locations_with_max_energy()
#%%
inc_field = solv.file_read('simulation/incoming_field')
#%%
frac_field = solv.file_read('simulation/fracture_field')
#%%
# frac = FractureGeo()
ver = pv.PolyData(frac.fracture_properties[['x', 'y', 'z']].to_numpy())
# ver['inc'] = frac_field[2]
#%%
# p.add_object(ver, name='center',)
p.add_object(ver, name='center', )#scalars='inc', cmap='viridis', cmax=0.2)
# p.add_object(frac.vertices, name='center',)
#%%
def run_simulation():
    pass

np.where(np.abs(incoming_field[0]) == np.max(np.abs(incoming_field[0])))