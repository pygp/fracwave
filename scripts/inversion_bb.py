from bedretto import model
from fracwave import FracEM, SourceEM, Antenna, FractureGeo, OUTPUT_DIR
import numpy as np
import pyvista as pv
import pandas as pd
import matplotlib.pyplot as plt


filename = OUTPUT_DIR + 'bb/inversion_bb.h5'
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.02  # m
max_element_size = wavelength / 6
#%%

p = model.ModelPlot()
p.show(zxGrid2=False, yzGrid2=False)
#%% Antenna positions
ant = Antenna()
boreholes = ['ST1', 'ST2', 'MB1', 'MB2', 'MB3', 'MB4', 'MB5', 'MB7', 'MB8']
try:
    ant.load_hdf5(filename)
except:
    from bedretto import data
    bor_dat = data.BoreholeData()

    traces_per_borehole = 100
    # b = boreholes[0]

    # rx = []
    # orx = []
    for b in boreholes:
        end = bor_dat.get_borehole_end_coordinates(b)
        depths = np.linspace(10, end['Depth (m)']-1, traces_per_borehole)
        depths2 = np.linspace(11, end['Depth (m)'], traces_per_borehole)
        xyz = bor_dat.get_xyz_coordinates(b, depths).T
        xyz2 = bor_dat.get_xyz_coordinates(b, depths2).T

        orient_rx = (xyz - xyz2) / np.linalg.norm((xyz - xyz2) , ord=1, axis=1)[:,None]

        # rx.append(xyz)
        # orx.append(orient_rx)
        ant.set_profile(name_profile=b,
                        receivers=xyz,
                        transmitters=xyz,
                        orient_receivers= orient_rx,
                        orient_transmitters=orient_rx
                        )

    # ant.Rx = np.vstack(rx)
    # ant.Tx = np.vstack(rx)
    # ant.orient_Rx = np.vstack(orx)
    # ant.orient_Tx = np.vstack(orx)

    ant.export_to_hdf5(filename, overwrite=True)

#%%
# g_tx, g_rx, g_otx, g_orx = ant.get_geometry(arrows=False)
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')
n_arrows = 20
Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in np.linspace(1, len(ant.Transmitter)-1, n_arrows, dtype=int):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=6)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=6)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')


#%% Source function we will use the traditional 20 MHz antenas
sou = SourceEM()
try:
    sou.load_hdf5(filename)
except:
    f = np.linspace(0, 0.05, 300)
    sou.frequency = f
    sou.create_source(a=1, b=0.02, t=-70)
    sou.export_to_hdf5(filename, overwrite=True)
ax = sou.plot()
ax.figure.show()

#%% Geometry
def get_bb_points():

    from bedretto import data
    from bedretto import DATA_DIR as dat_bedretto
    from bedretto.core.experiments import probabilities

    # set numpy random seed
    np.random.seed(0)
    # initialize classes
    borehole_data = data.BoreholeData()
    p_samp = probabilities.ProbabilisticSampling(borehole_data)

    bb_data = pd.read_csv(dat_bedretto + 'logging_data/BB_logging.csv')
    bb_sample = p_samp.sample_logging_structures(bb_data)
    orientations = bb_sample[['Nx', 'Ny', 'Nz']].to_numpy()
    surface_points = bb_sample[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()

    from fracwave.geometry.vector_calculus import get_azimuth_and_dip_from_normal_vector
    dip, azimuth = get_azimuth_and_dip_from_normal_vector(orientations)

    tun_dat = data.TunnelData()
    bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
    surface_points = np.vstack((bb['Center'].to_numpy()[0], surface_points))
    dip = np.append(bb['Dip (deg)'].to_numpy(), dip)
    azimuth = np.append(bb['Azimuth (deg)'].to_numpy(), azimuth)
    return surface_points, dip, azimuth

frac = FractureGeo()
extent = (-250, 50, -150, 100, 1200, 1500)


try:
    frac.load_hdf5(filename)
except:
    frac.init_geo_model(extent=extent, resolution=(50, 50, 50))
    surface_points, dip, azimuth = get_bb_points()
    fixed_points = surface_points
    fixed_orientations = (surface_points, np.c_[azimuth, dip])
    vertices, faces = frac.create_gempy_geometry(fixed_points=fixed_points,
                                                 fixed_orientations=fixed_orientations,
                                                 move_points=None)
    surf = frac.remesh(vertices, plane=1, max_element_size=max_element_size)
    frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    frac.export_to_hdf5(filename, overwrite=True)
#%%
# p.add_object(pv.PolyData(frac.fracture_properties[['x','y','z']].to_numpy()), name='geometry', color='green', opacity=0.5)
p.add_object(frac.surface, name='geometry', color='green', opacity=0.5)

#%% Use now just 1 point and orientation
# surface_points, dip, azimuth = get_bb_points()
if frac.geo_model is None:
    frac.init_geo_model(extent=extent, resolution=(50, 50, 50))
from bedretto import data
tun_dat = data.TunnelData()
bb = tun_dat.get_tunnel_fractures(structure_ID='-5')

fixed_points = np.asarray(bb['Pointcloud'].to_numpy()[0])
fixed_orientations = (np.asarray(bb['Center'].to_numpy()[0])[None], np.c_[bb['Azimuth (deg)'].to_numpy(), bb['Dip (deg)'].to_numpy()[0]])
# vertices, faces = frac.create_gempy_geometry(fixed_points=fixed_points,
#                                                  fixed_orientations=fixed_orientations,
#                                                  move_points=None)
# surf = frac.remesh(vertices, plane=1, max_element_size=max_element_size)
# p.add_object(pv.PolyData(frac.vertices), name='start', color='red', opacity=0.5)

#%%
from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
                                                   set_fracture_solver,
                                                   forward_operator,
                                                   run_inversion,
                                                   run_inversion_per_profile)

#%%
solv = FracEM()
try:
    solv.load_hdf5(filename)
    pick_true = solv.file_read('simulation/dummy_solution/first_arrival')
    time_vector = solv.file_read('simulation/dummy_solution/time_vector')
    # pick_true = np.load(OUTPUT_DIR+'bb/bb_first_arrival.npy')
except:
    solv.rock_epsilon = 6.
    solv.rock_sigma = 0.0
    # solv.engine = 'tensor_trace'
    solv.engine = 'dummy'
    solv.units = 'SI'
    # solv.backend = 'torch'
    solv.backend = 'numpy'
    # solv._dummy_time_response = True

    solv.open_file(filename)
    f, t0 = solv.time_zero_correction()
    solv.solve_for_profile = 'all'
    freq = solv.forward_pass(overwrite=True, recalculate=True, mask_frequencies=False)
    pick_true = solv.file_read('simulation/dummy_solution/first_arrival')
    time_vector = solv.file_read('simulation/dummy_solution/time_vector')

    np.save(OUTPUT_DIR+'bb/bb_first_arrival.npy', pick_true)

#%%
fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10,25))
for i, ax in enumerate(AX):
    name = solv.name_profiles[i]
    indexes = solv.file_read(f'antenna/profiles/{name}')
    ax.plot(indexes, pick_true[indexes])
    ax.set_xlabel('Trace no.')
    # ax.set_xtick(indexes)
    ax.set_ylabel('Travel Time')
    ax.set_ylim(time_vector[0], pick_true[indexes].max())
    ax.grid()
    ax.set_title(name)
fig.tight_layout()
plt.show()

#%% Add some noise to the data
np.random.seed(0)
noise_level = 0.1
noise = np.random.randn(solv.ntraces)*noise_level

inversion_tolerance = 2*noise_level*np.sqrt(solv.ntraces)

plt.plot(pick_true, label='without noise')
plt.plot(pick_true+noise, label='with noise')
plt.legend()
plt.show()
#%%
set_fracture_geometry(frac)
set_fracture_solver(solv)

#%%

kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81,
                                  set_properties=True)

kwargs_forward_solver = dict(plane=1,
                             order=2,
                             max_element_size=max_element_size)

kwargs_cost_function = dict(tol_all=5, # Tolerance for overall RMS error over the whole picks
                            tol_pos=2,# inversion_tolerance, # Tolerance for a single trace
                            max_iter_all=100, # Maximum amount of control points
                            max_iter_pos=10, # maximum number of trials per position
                            surrounding=5, # ignore traces in this surrounding
                            step_speed=10) # higher the number the step will be smaller

solv_out = None
fixed_points_geometry = None
fixed_points_geometry, solv_out = run_inversion(pick_true=pick_true,
                                            fixed_points_geometry=fixed_points,
                                            fixed_oientations_geometry=fixed_orientations,
                                            constrained_points_geometry=None,
                                            filename=filename,
                                            plot_step=True,
                                            kwargs_fracture_properties=kwargs_fracture_properties,
                                            kwargs_forward_solver=kwargs_forward_solver,
                                            kwargs_cost_function=kwargs_cost_function)



np.save(OUTPUT_DIR+'bb/output_points.npy', fixed_points_geometry)





#%%
p = model.ModelPlot()
p.show(zxGrid2=False, yzGrid2=False, xyGrid=False)
# Antennas positions
ant = Antenna()
ant.load_hdf5(filename)
p.add_object(ant.Transmitter, name='Tx', color='red')
p.add_object(ant.Receiver, name='Rx', color='blue')
# Starting geometry
frac_start = FractureGeo()
frac_start.load_hdf5(filename.split('.')[0] + '_iter_1.h5')
p.add_object(frac_start.surface, name='geometry_start',
             color='red',
             opacity=0.5,
             label='Starting model')

# Ending geometry
frac_out = FractureGeo()
if solv_out is None:
    frac_out.load_hdf5(filename.split('.')[0] + '_pos_99_iter_1.h5')
else:
    frac_out.load_hdf5(solv_out._filename)
p.add_object(frac_out.surface, name='geometry_simulated',
             color='blue',
             opacity=0.5, label='Simulated model')

# Real geometry
frac_true = FractureGeo()
frac_true.load_hdf5(filename)
p.add_object(frac_true.surface, name='geometry_real', color='green',
             opacity=0.5, label='Real model', )

if fixed_points_geometry is None:
    fixed_points_geometry = np.load(OUTPUT_DIR+'bb/output_points.npy')

p.add_object(pv.PolyData(fixed_points_geometry), name='cp', label='Control Points')

p.add_legend()





#%%
#%%
# solv._dummy_time_response = True
freq = solv.forward_pass(overwrite=True, recalculate=True, mask_frequencies=False)
#%%
time_response, time_vector = solv.get_ifft(freq)

#%%
m = np.abs(time_response).max() /100
fig, ax = plt.subplots()
cax = ax.imshow(time_response.T, aspect='auto',
          vmin=-m, vmax=m,
          cmap='seismic',
          extent=(0,time_response.shape[0],time_vector[-1],time_vector[0]))
fig.colorbar(cax, ax=ax)
plt.show()
#%%
fig, ax = plt.subplots()
ax.plot(time_vector, time_response[88])
plt.show()
#%%
first_arrival = solv.file_read('dummy_solution/first_arrival')
time_vector = solv.file_read('dummy_solution/time_vector')
time_response = solv.file_read('dummy_solution/time_response')


plt.plot(first_arrival)
plt.ylim(time_vector[-1],time_vector[0])
plt.show()

#%%
# time_response = dat
fig, ax = plt.subplots()
cax = ax.imshow(time_response.T, aspect='auto',
                vmax=0.5,
          extent=(0, time_response.shape[0], time_vector[-1], time_vector[0])
                )
fig.colorbar(cax, ax=ax)
plt.show()

#%%% Plot the profiles separately

first_arrival = solv.file_read('simulation/dummy_solution/first_arrival')
time_vector = solv.file_read('simulation/dummy_solution/time_vector')
fig, AX = plt.subplots(len(solv.name_profiles), figsize=(10,20))

for p, ax in zip(solv.name_profiles, AX):
    trace_no = solv.file_read(f'antenna/profiles/{p}')
    ax.plot(trace_no, first_arrival[trace_no], label='Pick')
    ax.set_xlabel('Trace no.')
    # ax.set_xtick(indexes)
    ax.set_ylabel('Travel Time')
    ax.set_ylim(time_vector[0], first_arrival[trace_no].max())
    ax.grid()
    ax.set_title(p)
    handles, labels = ax.get_legend_handles_labels()
    for l, h in zip(labels, handles):
        if l not in leg_dict:
            leg_dict[l] = h
fig.legend(leg_dict.values(), leg_dict.keys())
fig.suptitle('All profiles')
fig.tight_layout()
fig.show()



