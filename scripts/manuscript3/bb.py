import numpy as np
import pandas as pd
import pyvista as pv
from bedretto import (data,
                      geometry,
                      model)
from fracwave import OUTPUT_DIR, SourceEM, FractureGeo, Antenna, FracEM
import matplotlib.pyplot as plt
import pickle

import scipy.io as io

odir = OUTPUT_DIR + 'manuscript3/picks/'
filename = odir + 'real_inversion.h5'
from gdp import DATA_DIR as gdpdat
from gdp.processing.gain import apply_gain
path_files = gdpdat + 'VALTER/'
np.random.seed(1234)

#%%
pmhz = {}
pmhz['100MHz'] = {'ST1': [# 'ST1_100MHz_210226.mat', This one seems to not have the time zero correction
                   'ST1_100MHz_210218.mat',
                   # 'ST1_100MHz_210215.mat',
                   # 'ST1_100MHz_210213.mat',
                   # 'ST1_100MHz_210212.mat'
                   ],
           'ST2': ['ST2_100MHz_200728.mat'],
           'MB8':  [#'MB8_100MHz_210525.mat', Here the depth is wrong
                    'MB8_100MHz_210505.mat'],
           'MB7': ['MB7_100MHz_210518.mat'],
           'MB5': ['MB5_100MHz_210324.mat'],
           'MB4': [#'MB4_100MHz_200605.mat', # Here the depth is wrong
                   'MB4_100MHz_200529.mat'
                   # 'MB4_250MHz_200605.mat'  # 250 MHz - Not working
                   ],
           'MB2': ['MB2_100MHz_191212.mat',],
           'MB1': ['MB1_100MHz_200515.mat',]
                   #'MB1_100MHz_200501.mat',
                   #'MB1_100MHz_200218.mat',
                   # 'MB1_100MHz_200211.mat', This I cannot see the bb
                   #'MB1_100MHz_191212.mat'],
                    #]
           }
pmhz['250MHz'] = {'MB2': ['MB2_250MHz_191022.mat'],
                  'MB1': [#'MB1_250MHz_200218.mat',
                          'MB1_250MHz_191016.mat']
                  }

pmhz['20MHz'] = {'MB2': ['MB2_20MHz_B200107.mat'],
                 'MB1': ['MB1_20MHz_B200108.mat']
                  }

#%%
c = {}

c['100MHz'] = {'MB1': (10, 160, 980, 0),
     'ST1': (20, 120, 600, 0), # Depth seems wrong. Fixed by forcing 20 as the first value
     'ST2': (35, 165, 992, 0),
     'MB8':(22,120, 633, 0),  # No convergence
     # 'MB7': (40,250,850,300),
     'MB7': (10,250,500,250),
     'MB5': (5,100, 580,0),
     'MB2': (15,140,800,0),
     'MB4': (5, 82, 410, 0)}
c['250MHz'] = {'MB2': (70, 135, 400, 0),
               'MB1':(95, 145, 350,0)}
c['20MHz'] = {'MB2': (15, 130, 900, 0),
               'MB1':(17, 150, 1000,0)}
picks = {}
# fn = p100mhz['MB4']
# bn = 'MB4'
# for bn in c.keys():
#     with open(odir + f'manual_picks_{bn}', 'rb') as f:
#         picks[bn] = pickle.load(f)
#%%
for freq in ['20MHz', '100MHz', '250MHz']:
    for bn, fn, in pmhz[freq].items():
        try:
            with open(odir + f'manual_picks_{bn}_{freq}', 'rb') as f:
                if bn not in picks.keys():
                    picks[bn] = {}
                picks[bn][freq] = pickle.load(f)
                print(f'Loaded manual_picks_{bn}_{freq}')
        except FileNotFoundError as e:
            print('e')
            print(f'Creating manual_picks_{bn}_{freq}')
            from gdp.processing.image_processing import pick_line
            # freq = '20MHz'
            try:
                picks[bn][freq] = {}
            except:
                picks[bn] = {}
                picks[bn][freq] = {}
            file = fn[0]
            # file =   'MB1_250MHz_200218.mat'
            # file =   'MB1_20MHz_B200108.mat' #'MB1_250MHz_200218.mat'
            from gdp import DATA_DIR as gdpdat
            mat = io.loadmat(gdpdat + 'VALTER/' + file)
            depth = mat['depth'][:, 0]
            if bn == 'ST1':
                depth += 20 - 2.373
            processed_data = mat['processed_data']
            # processed_data = mat['raw_data']
            travel_time = mat['travel_time'][0]
            radius = mat['radius'][0]

            extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
            processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                                       out=np.zeros_like(processed_data),
                                       where=np.abs(processed_data).max(axis=1)[:, None] != 0)

            # from scipy.stats import rv_histogram

            plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto'); plt.grid(); plt.show()
            # plt.imshow(mat['raw_data'], cmap='seismic', aspect='auto');
            # plt.xlabel('borehole depth')
            # plt.ylabel('Travel time (ns)')
            # plt.show() 14
            cut = c[freq][bn] # This will change for all
            mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
            mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
            cut_image = processed_data[mask_time][:, mask_depth]
            plt.imshow(cut_image, cmap='seismic', extent=cut, aspect='auto');plt.grid();plt.show()
            # plt.xlim((cut[0], cut[1]))
            # plt.ylim((cut[2], cut[3]))
            # plt.show()
            for p in range(10):
                picks[bn][freq][p] = pick_line(cut_image, colormap='seismic', interpolate_path=True, radius_point=1, extent=cut,
                                               automatic_naming=True)

            with open(odir + f'manual_picks_{bn}_{freq}', 'wb') as f:
                pickle.dump(picks[bn][freq], f)

#%%
# import scipy.io as io
# from gdp import DATA_DIR as gdpdat
# import pickle
#
# fi = 'MB1_100MHz_200515.mat'
# freq='100MHz'
# #
# t = fi[:3]
# name = fi[:3]
#
# mat = io.loadmat(gdpdat + 'VALTER/' + fi)
#
# depth = mat['depth'][:, 0]
#
# processed_data = mat['processed_data']
# travel_time = mat['travel_time'][0]
# radius = mat['radius'][0]
#
# # extent = (depth[0], depth[-1], radius[-1], radius[0])
# extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
# # processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
# #                            out=np.zeros_like(processed_data),
# #                            where=np.abs(processed_data).max(axis=1)[:, None] != 0)
# #%%
# cut = c[freq][bn]  # This will change for all
# mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
# mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
# cut_image = processed_data[mask_time][:, mask_depth]
# #%%
# vmax = np.abs(cut_image).max()
#     # fig, ax1 = plt.subplots()
# fig, ax = plt.subplots()
# im = ax.imshow(cut_image, vmin=-vmax, vmax=vmax,
#            extent=cut,
#                cmap='seismic', aspect='auto')
# ax.set_title(name + ' 100MHz')
#
#
# xy_t = picks['MB1'][freq][1]['pick_0']['true coordinates']
# xy_p = picks['MB1'][freq][1]['pick_0']['pixel coordinates']
# # xy_p[:,1] = xy_p[:,1] + extra_time
# # ax.plot(xy[:,0], xy[:,1], '.')
#
# # ax.plot(xy_p[:,0], xy_p[:,1], '.')
# ax.plot(xy_t[:,0], xy_t[:,1], '.')
# fig.colorbar(im, ax=ax)
# plt.show()
#
# #%% Now lets shift everything
#
# y_pos = 200#xy_p[:,1].min()
# max_y = cut_image.shape[0]
#
# empty = np.zeros(cut_image.shape)
#
# for xy in xy_p:
#
#     shift = xy[1] - y_pos
#     add = False
#     if shift < 0:
#         # shift = 0
#         add = True
#     if add:
#         y_range = slice(0, max_y)
#         new_t = cut_image[:, xy[0]]
#         y_new_range = slice(np.abs(shift), len(new_t))
#         empty[y_new_range, xy[0]] = cut_image[y_range, xy[0]][:shift]
#     else:
#         y_range = slice(shift, max_y)
#         new_t = cut_image[y_range, xy[0]]
#         y_new_range = slice(0, len(new_t))
#         empty[y_new_range, xy[0]] = cut_image[y_range, xy[0]]
#
# flattened = empty[:,xy_p[:,0].min():xy_p[:,0].max()]
# new_depth = depth[mask_depth][xy_p[:,0].min():xy_p[:,0].max()]
# #%%
# vmax = np.abs(flattened).max()
# bef = 20
# aft = 55
# fig, ax = plt.subplots()
# im = ax.imshow(flattened, vmin=-vmax, vmax=vmax,
#            # extent=cut,
#                cmap='seismic', aspect='auto')
# ax.set_title(name + ' 100MHz')
#
# ax.hlines(y_pos-bef, 0, flattened.shape[1], color='orange')
# ax.hlines(y_pos+aft, 0, flattened.shape[1], color='orange')
#
# ax.set_xlim(0, flattened.shape[1])
#
# # xy_t = picks['MB1'][1]['pick_0']['true coordinates']
# # xy_p = picks['MB1'][1]['pick_0']['pixel coordinates']
# # xy_p[:,1] = xy_p[:,1] + extra_time
# # ax.plot(xy[:,0], xy[:,1], '.')
#
# # ax.plot(xy_p[:,0], xy_p[:,1], '.')
# fig.colorbar(im, ax=ax)
# plt.show()
#
# #%%
# flattened_cropped = flattened[y_pos-bef:y_pos+aft, :]
#
# fig, ax = plt.subplots()
# im = ax.imshow(flattened_cropped, vmin=-vmax, vmax=vmax,
#            # extent=cut,
#                cmap='seismic', aspect='auto')
# # ax.set_title(name + ' 100MHz')
# plt.show()
#
# #%%
# norm = np.linalg.norm(flattened_cropped, axis=0)
#
# plt.plot(norm);plt.show()
#
# #%% Ignore all 0s
# mask_empty = ~np.isclose(norm,0)
# plt.plot(new_depth[mask_empty], norm[mask_empty])
# plt.show()
# #%%
# bins = 20
# plt.hist(norm[mask_empty], bins=bins, density=True)
# plt.show()
#
# #%% Calculate the variogram
# # ignore 0 values
# np.random.seed(1234)
#
# nbins=20
# n_random_points = 500
#
# props = []
# handles = []
#
# m = new_depth[mask_empty]
# a = norm[mask_empty]/np.abs(norm[mask_empty]).max()
#
# rand_pos =np.random.permutation(np.arange(len(m)))
# position_xy = m[rand_pos[:n_random_points]]
#
# train = a[rand_pos[:n_random_points]]  # Sampled values
#
# # position_xy = np.vstack([x_random, y_random]).T
#
# num_bins =nbins
#
#
#     # Calculate bin edges
# bins = np.linspace(0,  50, num_bins + 1)
#
# import gstools as gs
# bin_center, gamma = gs.vario_estimate((position_xy), train,
#                                   bin_edges=bins,
#                                   estimator='cressie'
#                                   # sampling_size=3,
#                                   # sampling_seed=1234
#                                   )
#
# fig, ax = plt.subplots()
# sc = ax.scatter(bin_center, gamma, label=f'experimental variogram')
# # handles.append(sc)
# fit_model = gs.Exponential(dim=1)
# para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)
#
#
# x_s = np.linspace(0, max(bin_center)*4/3)
# li, = ax.plot(x_s, fit_model.variogram(x_s),label=f"covariance model",)
# # handles.append(li)
# # per_scale = fit_model.percentile_scale(0.90)
# # li2 = ax.vlines(fit_model.len_scale, )#ymin=0, ymax=0.1, color=c, linestyle=':', label=f'Length scale = {fit_model.len_scale:.2f} cm')
# # handles.append(li2)
#     # ax.vlines(fit_model.percentile_scale(0.99), ymin=0, ymax=fit_model.var, color=c, linestyle=':', label=f'Range = {fit_model.percentile_scale(0.99):.2f} cm')
#     # ax.vlines(fit_model.integral_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dotted')
#     # ax.vlines(per_scale, ymin=0, ymax=fit_model.var, color=c, linestyle='dashed')
#     # ax.hlines(fit_model.var,xmin=0, xmax=x_s.max(), color=c, linestyle='dotted')#, label=f'Sill = {fit_model.var:.2f}')
#
# # props.append((fit_model.var, fit_model.len_scale))
#
# # labels = [l.get_label() for l in handles]
# # ax.legend(handles, labels, fancybox=True, edgecolor='black', frameon=True, loc='lower right')
#
# # ax1.set_ylim(-0.0005,0.01)
# # ax1.yaxis.label.set_color('blue')
# # ax1.tick_params(axis='y', colors='blue')
# # ax1.set_ylabel(r'Semi-variogram GPR reflection coefficients [$\gamma$]')
# # # Set the color of the tick labels
# # for label in ax1.get_yticklabels():
# #     label.set_color('blue')
#
#
# ax.set_xlabel('Lag distance [m]')
# ax.set_ylabel(r'Semi-variogram apertures [$\gamma$]')
# ax.grid(linestyle='--')
# ax.set_title(r'$\bf{(b)}$ Variogram')
# fig.show()

#%% Now lets do the same for all the other files
try:
    with open(odir + 'all_manual_picks', 'rb') as f:
        all_picks = pickle.load(f)
    print('Loaded manual_picks')
except FileNotFoundError:

    all_picks={}
    # freqs_to_show = '100MHz'
    # bn_to_show = 'ST1'
    for freq in ['20MHz', '100MHz', '250MHz']:
        # if freq != freqs_to_show:
        #     continue
        all_picks[freq] = {}
        for bn, fn, in pmhz[freq].items():
            # if bn != bn_to_show:
            #     continue
            pi = picks[bn][freq]
            true_coordinates = []
            pixel_coordinates=[]
            coordinates=[]
            for _p in pi.values():
                true_coordinates.append(_p['pick_0']['true coordinates'])
                pixel_coordinates.append(_p['pick_0']['pixel coordinates'])
                coordinates.append(np.c_[_p['pick_0']['true coordinates'], _p['pick_0']['pixel coordinates']])
            coordinates = np.vstack(coordinates)

            mean_true = []
            mean_pix = []
            for d in list(set(coordinates[:, 2])):
                pos = np.ravel(np.argwhere(np.isclose(int(d), coordinates[:, 2].astype(int), )))
                if len(pos) == 0:  # No number
                    print(f'Not found, {d, pos}')
                    continue
                if len(pos) < 5:  # "Less than 5 picks, don't take it into account"
                    continue
                mean_true.append((coordinates[pos, 0].mean(), coordinates[pos, 1].mean()))
                mean_pix.append((d, coordinates[pos, 3].mean().astype(int)))
                # break
            mean_true = np.asarray(mean_true)
            mean_pix = np.asarray(mean_pix).astype(int)
            all_picks[freq][bn] = {'true': mean_true, 'pixel': mean_pix}

    with open(odir + 'all_manual_picks', 'wb') as f:
        pickle.dump(all_picks, f)

        # fi = pmhz[freq][bn][0]
        #
        # mat = io.loadmat(gdpdat + 'VALTER/' + fi)
        #
        # depth = mat['depth'][:, 0]
        # if bn == 'ST1':
        #     depth += 20 - 2.373
        # processed_data = mat['processed_data']
        # travel_time = mat['travel_time'][0]
        # radius = mat['radius'][0]
        #
        # # extent = (depth[0], depth[-1], radius[-1], radius[0])
        # extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
        #
        # cut = c[freq][bn]  # This will change for all
        # mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
        # mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
        # cut_image = processed_data[mask_time][:, mask_depth]
        #
        # vmax = np.abs(cut_image).max()
        # # fig, ax1 = plt.subplots()
        # fig, ax = plt.subplots()
        # im = ax.imshow(cut_image, vmin=-vmax, vmax=vmax,
        #                extent=cut,
        #                cmap='seismic', aspect='auto')
        # ax.set_title(bn + '_' + freq)
        #
        # # xy_t = picks['MB1'][1]['pick_0']['true coordinates']
        # # xy_p = picks['MB1'][1]['pick_0']['pixel coordinates']
        # # xy_p[:,1] = xy_p[:,1] + extra_time
        # # ax.plot(xy[:,0], xy[:,1], '.')
        #
        # ax.plot(mean_true[:, 0], mean_true[:, 1], '.')
        # fig.colorbar(im, ax=ax)
        # plt.show()

        # break

    # break
    # val = {}
#%%
def shift_image(image, xy_p, y_pos=0):
    max_y = image.shape[0]
    empty = np.zeros(image.shape)
    for xy in xy_p:

        shift = xy[1] - y_pos
        add = False
        if shift < 0:
            # shift = 0
            add = True
        if add:
            y_range = slice(0, max_y)
            new_t = image[:, xy[0]]
            y_new_range = slice(np.abs(shift), len(new_t))
            empty[y_new_range, xy[0]] = image[y_range, xy[0]][:shift]
        else:
            y_range = slice(shift, max_y)
            new_t = image[y_range, xy[0]]
            y_new_range = slice(0, len(new_t))
            empty[y_new_range, xy[0]] = image[y_range, xy[0]]

    flattened = empty[:, xy_p[:, 0].min():xy_p[:, 0].max()]
    new_depth = depth[mask_depth][xy_p[:, 0].min():xy_p[:, 0].max()]
    return flattened, new_depth

#%%
freqs_to_show = '100MHz'
bn_to_show = 'MB1'
for freq in ['20MHz', '100MHz', '250MHz']:
    if freq != freqs_to_show:
        continue
    for bn, fn, in pmhz[freq].items():
        if bn != bn_to_show:
            continue

        fi = pmhz[freq][bn][0]

        mat = io.loadmat(gdpdat + 'VALTER/' + fi)

        depth = mat['depth'][:, 0]
        if bn == 'ST1':
            depth += 20 - 2.373
        processed_data = mat['processed_data']
        travel_time = mat['travel_time'][0]
        radius = mat['radius'][0]

        # extent = (depth[0], depth[-1], radius[-1], radius[0])
        extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])

        cut = c[freq][bn]  # This will change for all
        mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
        mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
        cut_image = processed_data[mask_time][:, mask_depth]

        vmax = np.abs(cut_image).max()
        # fig, ax1 = plt.subplots()

        tr = all_picks[freq][bn]['true']
        px = all_picks[freq][bn]['pixel']
#%%
        fig, ax = plt.subplots()
        im = ax.imshow(cut_image, vmin=-vmax, vmax=vmax,
                       extent=cut,
                       cmap='seismic', aspect='auto')
        ax.set_title(bn + '_' + freq)

        # xy_t = picks['MB1'][1]['pick_0']['true coordinates']
        # xy_p = picks['MB1'][1]['pick_0']['pixel coordinates']
        # xy_p[:,1] = xy_p[:,1] + extra_time
        # ax.plot(xy[:,0], xy[:,1], '.')

        ax.plot(tr[:, 0], tr[:, 1], '.')
        # ax.plot(mean_pix[:, 0], mean_pix[:, 1], '.')
        fig.colorbar(im, ax=ax)
        plt.show()
        #%%
        gimage = cut_image.copy()
        #%%
        gimage, _ = apply_gain(cut_image, gain_type='linear', sfreq=1/np.mean(np.diff(travel_time)),
                               exponent=1)
        fig, (ax0, ax1) = plt.subplots(1, 2)
        im = ax0.imshow(cut_image, vmin=-np.abs(cut_image).max(), vmax=np.abs(cut_image).max(),
                       extent=cut,
                       cmap='seismic', aspect='auto')
        im2 = ax1.imshow(gimage, vmin=-np.abs(gimage).max(), vmax=np.abs(gimage).max(),
                       extent=cut,
                       cmap='seismic', aspect='auto')
        ax0.set_title(bn + '_' + freq)
        ax1.set_title(bn + '_' + freq)
        fig.colorbar(im, ax=ax0)
        fig.colorbar(im2, ax=ax1)
        plt.show()
        #%%

        y_pos = 200
        flattened, new_depth = shift_image(gimage, px, y_pos=y_pos)

        vmax = np.abs(flattened).max()
        bef = 10
        aft = 50
        fig, ax = plt.subplots()
        im = ax.imshow(flattened, vmin=-vmax, vmax=vmax,
                       # extent=cut,
                       cmap='seismic', aspect='auto')
        ax.set_title(f'{bn} {freq}')

        ax.hlines(y_pos - bef, 0, flattened.shape[1], color='orange')
        ax.hlines(y_pos + aft, 0, flattened.shape[1], color='orange')

        ax.set_xlim(0, flattened.shape[1])

        # xy_t = picks['MB1'][1]['pick_0']['true coordinates']
        # xy_p = picks['MB1'][1]['pick_0']['pixel coordinates']
        # xy_p[:,1] = xy_p[:,1] + extra_time
        # ax.plot(xy[:,0], xy[:,1], '.')

        # ax.plot(xy_p[:,0], xy_p[:,1], '.')
        fig.colorbar(im, ax=ax)
        plt.show()

        flattened_cropped = flattened[y_pos - bef:y_pos + aft, :]

        fig, ax = plt.subplots()
        im = ax.imshow(flattened_cropped, vmin=-vmax, vmax=vmax,
                       # extent=cut,
                       cmap='seismic', aspect='auto')
        # ax.set_title(name + ' 100MHz')
        plt.show()

        # norm = np.linalg.norm(flattened_cropped, axis=0)

        norm = np.abs(flattened_cropped).max(axis=0)

        mask_empty = ~np.isclose(norm, 0)
        plt.plot(new_depth[mask_empty], norm[mask_empty])
        # plt.plot(norm[mask_empty])
        plt.show()
#%%
        nbins = 20
        n_random_points = 500

        m = new_depth[mask_empty]
        a = norm[mask_empty] / np.abs(norm[mask_empty]).max()

        rand_pos = np.random.permutation(np.arange(len(m)))
        position_xy = m[rand_pos[:n_random_points]]

        train = a[rand_pos[:n_random_points]]  # Sampled values

        # position_xy = np.vstack([x_random, y_random]).T

        num_bins = nbins

        # Calculate bin edges
        bins = np.linspace(0, 100, num_bins + 1)

        import gstools as gs

        bin_center, gamma = gs.vario_estimate((position_xy), train,
                                              bin_edges=bins,
                                              estimator='cressie'
                                              # sampling_size=3,
                                              # sampling_seed=1234
                                              )

        fig, ax = plt.subplots()
        sc = ax.scatter(bin_center, gamma, label=f'experimental variogram')
        # handles.append(sc)
        fit_model = gs.Stable(dim=1)
        para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)

        x_s = np.linspace(0, max(bin_center) * 4 / 3)
        li, = ax.plot(x_s, fit_model.variogram(x_s), label=f"covariance model", )

        ax.set_xlabel('Lag distance [m]')
        ax.set_ylabel(r'Semi-variogram apertures [$\gamma$]')
        ax.grid(linestyle='--')
        ax.set_title(r'$\bf{(b)}$ Variogram')
        fig.show()
#%%

fig, ax = plt.subplots()
for e in np.linspace(1.0,2,10):
    gimage, _ = apply_gain(cut_image, gain_type='linear', sfreq=1/np.mean(np.diff(travel_time)),
                           exponent=e)


    y_pos = 100
    flattened, new_depth = shift_image(gimage, px, y_pos=y_pos)

    vmax = np.abs(flattened).max()

    flattened_cropped = flattened[y_pos - bef:y_pos + aft, :]

    norm = np.abs(flattened_cropped).max(axis=0)

    mask_empty = ~np.isclose(norm, 0)

    nbins = 20
    n_random_points = 500

    m = new_depth[mask_empty]
    a = norm[mask_empty] / np.abs(norm[mask_empty]).max()

    rand_pos = np.random.permutation(np.arange(len(m)))
    position_xy = m[rand_pos[:n_random_points]]

    train = a[rand_pos[:n_random_points]]  # Sampled values

    # position_xy = np.vstack([x_random, y_random]).T

    num_bins = nbins

    # Calculate bin edges
    bins = np.linspace(0, 50, num_bins + 1)

    import gstools as gs

    bin_center, gamma = gs.vario_estimate((position_xy), train,
                                          bin_edges=bins,
                                          estimator='cressie'
                                          # sampling_size=3,
                                          # sampling_seed=1234
                                          )


    sc = ax.scatter(bin_center, gamma)#, label=f'experimental variogram')
        # handles.append(sc)
    fit_model = gs.Gaussian(dim=1)
    para, pcov, r2 = fit_model.fit_variogram(bin_center, gamma, return_r2=True)

    x_s = np.linspace(0, max(bin_center) * 4 / 3)
    li, = ax.plot(x_s, fit_model.variogram(x_s), label=e)
    li2 = ax.vlines(fit_model.len_scale, ymin=0, ymax=0.05, color='k', linestyle=':',)

ax.set_xlabel('Lag distance [m]')
ax.set_ylabel(r'Semi-variogram apertures [$\gamma$]')
ax.grid(linestyle='--')
ax.set_title('$Variogram')
ax.legend()
fig.show()