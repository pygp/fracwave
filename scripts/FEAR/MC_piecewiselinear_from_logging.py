# %%
# Import all relevant modules
from fracwave import FractureGeo, DATA_DIR
from bedretto import (data, geometry, model)
from bedretto.core.common_helpers import spheres_from_points
from bedretto.core.vector_helpers import get_normal_vector_from_azimuth_and_dip, clip_pv_surface
import pyvista as pv
import pyvistaqt as pvqt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

# %%
fear_data = DATA_DIR + 'FEAR' + os.sep
bh_data = data.BoreholeData()
tu_data = data.TunnelData()

TM_MC = 2380
tu_geom = geometry.TunnelGeometry(tu_data)
tu_mesh = tu_geom.get_tunnel_mesh(TM_range=[TM_MC - 50, TM_MC + 50])

bh_geom = geometry.BoreholeGeometry(bh_data)
boreholes = {}
boreholes['BFE05'] =  pd.read_csv(fear_data + 'BFE_A_05_borehole_information.csv', index_col=0)
boreholes['BFE06'] =  pd.read_csv(fear_data + 'BFE_A_06_borehole_information.csv', index_col=0)
boreholes['BFE07'] =  pd.read_csv(fear_data + 'BFE_A_07_borehole_information.csv', index_col=0)

mc_bfe05 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=boreholes['BFE05'],
                                                              depths=[37.5])
mc_bfe06 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=boreholes['BFE06'],
                                                                depths=[27.8])
mc_bfe07 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=boreholes['BFE07'],
                                                                depths=[71.2])

# create borehole graphics
bh05 = bh_geom.construct_borehole_geometry(borehole_info=boreholes['BFE05'], radius=0.1)
bh06 = bh_geom.construct_borehole_geometry(borehole_info=boreholes['BFE06'], radius=0.1)
bh07 = bh_geom.construct_borehole_geometry(borehole_info=boreholes['BFE07'], radius=0.1)

mc_intersections = pd.read_csv(fear_data + 'mc_top.csv')

# create plane from pyvista given the center and normal
def create_plane_from_pyvista_mesh(center, normal, size=300, res=3000):
    # create a plane from pyvista
    plane = pv.Plane(center=center, direction=normal,
                     i_size=size, j_size=size,
                     i_resolution=res, j_resolution=res)
    return plane


# for each entry in the dataframe, create a fracture surface

mc_surfaces = []
mc_observations = []
for i in range(len(mc_intersections)):
    # get normal from dip and azimuth
    normal = get_normal_vector_from_azimuth_and_dip(dip=mc_intersections['Dip (deg)'].iloc[i],
                                            azimuth=mc_intersections['Azimuth (deg)'].iloc[i])
    # get location along borehole
    borehole_information = boreholes[mc_intersections['Borehole'].iloc[i]]
    location = bh_geom.get_coordinates_from_borehole_depth(depths=mc_intersections['Depth (m)'].iloc[i],
                                                           borehole_information=borehole_information,
                                                           )
    mc_observations.append(location)
    surf = create_plane_from_pyvista_mesh(location, normal, size=100, res=10)
    surf = clip_pv_surface(surface=surf, axis='z', level=1500)
    mc_surfaces.append(surf)


# %%
tunnel_intersection = tu_data.get_tunnel_fractures(structure_ID='48.1')
tunnel_pcd = np.asarray(tunnel_intersection['Pointcloud'].to_numpy()[0])
tunnel_center = np.asarray(tunnel_intersection['Center'].to_numpy()[0])
tunnel_normal = np.asarray(tunnel_intersection['Normal'].to_numpy()[0])
tu_geom = geometry.TunnelGeometry(tu_data)

MC_tunnel = create_plane_from_pyvista_mesh(tunnel_center,
                                           tunnel_normal,
                                             size=100, res=10)
MC_tunnel = clip_pv_surface(surface=MC_tunnel, axis='z', level=1500)


# %%
p = model.ModelPlot()
p.add_object(tu_mesh, name='tunnel', color='grey', opacity=0.5)
# plot all surfaces
for i in range(len(mc_surfaces)):
    p.add_object(mc_surfaces[i], name='MC ' + str(i), color='red', opacity=0.5)

p.add_object(MC_tunnel, name='Tunnel MC', color='red', opacity=0.5)
p.add_object(bh05, name='bh05', color='blue', opacity=1)
p.add_object(bh06, name='bh06', color='blue', opacity=1)
p.add_object(bh07, name='bh07', color='blue', opacity=1)

# show tunnel points
p.add_object(spheres_from_points(tunnel_pcd, radius=0.2),
             name='tunnel pcd', color='black')
p.add_object(spheres_from_points(mc_observations, radius=1),
             name='mc observations', color='green')
p.show()
p.save_scene_as_html('MC_FZ_piecewise_linear.html')

