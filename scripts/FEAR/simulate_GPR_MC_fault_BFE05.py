# %%
from bedretto import data, geometry, model
from bedretto.core.experiments import probabilities
import pandas as pd
import os
import numpy as np
from fracwave import DATA_DIR, OUTPUT_DIR, Antenna, SourceEM, FractureGeo, FracEM
import pyvista as pv
import matplotlib.pyplot as plt

file = OUTPUT_DIR + 'full_model_variable.h5'

# %%
sou = SourceEM()
sou.set_time_vector(np.linspace(0, 1000, 300))
sou.type = 'ricker'
sou.set_center_frequency(0.02)
sou.set_delay(50)
sou.create_source()
sou.plot_waveforms_complete().show()
sou.export_to_hdf5(file, overwrite=True)

# %%
# Load data
fear_data = DATA_DIR + 'FEAR' + os.sep
borehole_data = data.BoreholeData()
bfe05 = pd.read_csv(fear_data + 'BFE_A_05_borehole_information.csv', index_col=0)
MC_fault = pv.read('MC_FZ_smooth.vtk')
bor = bfe05[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()


# %%

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1 # m
max_element_size = wavelength / 6

frac = FractureGeo()
vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(MC_fault)

# %%
grid, vertices, faces = frac.remesh(vertices, plane=1, max_element_size=max_element_size, extrapolate=False)  # Resolution 145,143


aperture = frac.generate_heterogeneous_apertures(b_mean=0.8, seeds=1234,
                                                    points=vertices[faces].mean(axis=1),
                                                    plane=1)

frac.set_fracture(name_fracture='MC', 
                    vertices=vertices,
                    faces=faces, 
                    aperture=aperture,
                    electrical_conductivity=0,
                    electrical_permeability=81,
                    overwrite=True)
frac.export_to_hdf5(file, overwrite=True)


#%% Generate random field
plt.scatter(frac.midpoints[:,0], frac.midpoints[:,2], c=aperture, cmap='viridis')
plt.colorbar()
plt.show()

# %%
df1 = frac.set_fracture(name_fracture='MC', 
                        vertices=vertices, 
                        faces=faces,
                        aperture=aperture,
                        electrical_conductivity=0,
                        electrical_permeability=81,
                        overwrite=True)




# %%
frac.export_to_hdf5(file, overwrite=True)



# %% Antenna positions
ant = Antenna()

from bedretto import data
bor_dat = data.BoreholeData()
tx = np.linspace(0, 60, 100)
rx = tx - 2.77
xyz_tx = bor_dat.get_coordinates_from_borehole_depth(bfe05, tx).T
xyz_tx1 = bor_dat.get_coordinates_from_borehole_depth(bfe05, tx - 0.1).T
xyz_rx = bor_dat.get_coordinates_from_borehole_depth(bfe05, rx).T
xyz_rx1 = bor_dat.get_coordinates_from_borehole_depth(bfe05, rx - 0.1).T

orient_tx = (xyz_tx1 - xyz_tx) / np.linalg.norm((xyz_tx1 - xyz_tx), ord=1, axis=1)[:, None]
orient_rx = (xyz_rx1 - xyz_rx) / np.linalg.norm((xyz_rx1 - xyz_rx), ord=1, axis=1)[:, None]
ant.set_profile(name_profile='gpr',
                receivers=xyz_tx,
                transmitters=xyz_rx,
                orient_receivers=orient_rx,
                orient_transmitters=orient_tx
                )
ant.export_to_hdf5(file, overwrite=True)

# %%
surf = frac.get_surface()
bfe_05 = pd.read_csv(fear_data + 'BFE_A_05_borehole_information.csv', index_col=0)
bfe05_xyz = bfe_05[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()
surf2, vertices, faces = frac.mask_mesh(vertices, faces,
                                       from_maximum_distance_borehole=400,
                                       positions=bfe05_xyz,
                                       )




p = model.ModelPlot()
p.add_object(surf2, name='frac', scalars='aperture')
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')

n_arrows = 20
Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in np.linspace(1, len(ant.Transmitter) - 1, n_arrows, dtype=int):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=1)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=1)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')

# %%
solv = FracEM()

solv.rock_epsilon = 5.9
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
solv.backend = 'torch'
solv.open_file(file)
freq_t0 = solv.time_zero_correction()
solv._fast_calculation_incoming_field = False
# %%
freq = solv.forward_pass(overwrite=False, recalculate=True, save_hd5=False, )


#%%
solv = FracEM()
solv.load_hdf5(file)
freq = solv.file_read('summed_response')
time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
depth_vector = tx.copy() - 2.77*0.5

# %%
xlim = (0, 100)
ylim=(50,0)
ax = solv.plot(time_response,
                time_vector, 
                depth_vector, 
                xlim=xlim, 
                ylim=ylim, 
                clim=np.nanmax(time_response)/100)
