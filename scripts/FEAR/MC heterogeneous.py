# %%
from bedretto import data, geometry, model
from bedretto.core.experiments import probabilities
import pandas as pd
import os
import numpy as np
from fracwave import DATA_DIR, OUTPUT_DIR, Antenna, SourceEM, FractureGeo, FracEM
import pyvista as pv
import matplotlib.pyplot as plt

# %%
# Load data
fear_data = DATA_DIR + 'FEAR' + os.sep
borehole_data = data.BoreholeData()
borehole = pd.read_csv(fear_data + 'BFE_A_06_borehole_information.csv', index_col=0)
MC_fault = pv.read('MC_FZ_smooth.vtk')
name = 'MC_FZ_smooth_tunnel'
fear_data = DATA_DIR + 'FEAR' + os.sep
bh_data = data.BoreholeData()
tu_data = data.TunnelData()

TM_MC = 2380
tu_geom = geometry.TunnelGeometry(tu_data)
tu_mesh = tu_geom.get_tunnel_mesh(TM_range=[TM_MC - 50, TM_MC + 50])


bh_geom = geometry.BoreholeGeometry(bh_data)
bfe_05 = pd.read_csv(fear_data + 'BFE_A_05_borehole_information.csv', index_col=0)
bfe_06 = pd.read_csv(fear_data + 'BFE_A_06_borehole_information.csv', index_col=0)
bfe_07 = pd.read_csv(fear_data + 'BFE_A_07_borehole_information.csv', index_col=0)

mc_bfe05 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_05,
                                                              depths=[37.5])
mc_bfe06 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_06,
                                                                depths=[27.8])
mc_bfe07 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_07,
                                                                depths=[71.2])

# create borehole graphics
bh05 = bh_geom.construct_borehole_geometry(borehole_info=bfe_05, radius=0.1)
bh06 = bh_geom.construct_borehole_geometry(borehole_info=bfe_06, radius=0.1)
bh07 = bh_geom.construct_borehole_geometry(borehole_info=bfe_07, radius=0.1)

# %%

frac = FractureGeo()
vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(MC_fault)

# %%
grid, vertices, faces = frac.remesh(vertices, plane=1, max_element_size=1, extrapolate=False)  # Resolution 145,143                 

aperture = frac.generate_heterogeneous_apertures(b_mean=0.1,
                                                 var=0.001,
                                                  seeds=1234,
                                                    points=vertices[faces].mean(axis=1),
                                                    plane=1)

frac.set_fracture(name_fracture='MC', 
                    vertices=vertices,
                    faces=faces, 
                    aperture=aperture,
                    electrical_conductivity=0.01,
                    electrical_permeability=81,
                    overwrite=True)

surf = frac.create_pyvista_mesh(vertices=vertices, faces=faces)

# %%
surf['Aperture (m)'] = aperture
p = model.ModelPlot()
p.add_object(tu_mesh, name='tunnel', color='grey', opacity=0.9)
p.add_object(surf, name='MC', scalars='Aperture (m)', opacity=0.9)
p.add_object(bh05, name='bh05', color='black', opacity=1)
p.add_object(bh06, name='bh06', color='black', opacity=1)
p.add_object(bh07, name='bh07', color='black', opacity=1)
# add bofos with green color
#p.add_object(bofo08, name='bofo08', color='green', opacity=0.5)
#p.add_object(bofo09, name='bofo09', color='green', opacity=0.5)
#p.add_object(bofo10, name='bofo10', color='green', opacity=0.5)
#p.add_object(bofo11, name='bofo11', color='green', opacity=0.5)

p.show()
p.save_scene_as_html('MC_heterogeneous.html')

# %%
