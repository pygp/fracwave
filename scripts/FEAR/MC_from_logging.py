# %%
# Import all relevant modules
from fracwave import FractureGeo, DATA_DIR
from bedretto import (data, geometry, model)
from bedretto.core.common_helpers import spheres_from_points
import pyvista as pv
import pyvistaqt as pvqt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

# %%
<<<<<<< Updated upstream
name = 'MC_FZ_smooth_tunnel'
=======

>>>>>>> Stashed changes
fear_data = DATA_DIR + 'FEAR' + os.sep
bh_data = data.BoreholeData()
tu_data = data.TunnelData()

TM_MC = 2380
tu_geom = geometry.TunnelGeometry(tu_data)
tu_mesh = tu_geom.get_tunnel_mesh(TM_range=[TM_MC - 50, TM_MC + 50])

bh_geom = geometry.BoreholeGeometry(bh_data)
bfe_05 = pd.read_csv(fear_data + 'BFE_A_05_borehole_information.csv', index_col=0)
bfe_06 = pd.read_csv(fear_data + 'BFE_A_06_borehole_information.csv', index_col=0)
bfe_07 = pd.read_csv(fear_data + 'BFE_A_07_borehole_information.csv', index_col=0)

mc_bfe05 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_05,
                                                              depths=[37.5])
mc_bfe06 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_06,
                                                                depths=[27.8])
mc_bfe07 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bfe_07,
                                                                depths=[71.2])

# create borehole graphics
bh05 = bh_geom.construct_borehole_geometry(borehole_info=bfe_05, radius=0.1)
bh06 = bh_geom.construct_borehole_geometry(borehole_info=bfe_06, radius=0.1)
bh07 = bh_geom.construct_borehole_geometry(borehole_info=bfe_07, radius=0.1)


# %%

# create synthetic BOFO boreholes
bofo08, bofo08_df = bh_geom.create_synthetic_borehole(start_coordinate=[-253.489,
                                                                        271.391,
                                                                        1490.489],
                                           azimuth=325.06,
                                           dip=-57.30,
                                           length=10)
bofo09, bofo09_df = bh_geom.create_synthetic_borehole(start_coordinate=[-259.588,
                                                                        277.934,
                                                                        1490.629],
                                           azimuth=138.23,
                                           dip=-31.65,
                                           length=10)
bofo10, bofo10_df = bh_geom.create_synthetic_borehole(start_coordinate=[-254.593,
                                                                        272.599,
                                                                        1487.780],
                                           azimuth=319.61,
                                           dip=25.84,
                                           length=12)
bofo11, bofo11_df = bh_geom.create_synthetic_borehole(start_coordinate=[-261.485,
                                                                        279.992,
                                                                        1487.772],
                                           azimuth=141.16,
                                           dip=62.34,
                                           length=8)


# BOFO intersections
mc_bofo08 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bofo08_df,
                                                              depths=[6])

mc_bofo09 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bofo09_df,
                                                              depths=[7.2])
mc_bofo10 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bofo10_df,
                                                              depths=[5.6])
mc_bofo11 = bh_geom.get_coordinates_from_borehole_depth(borehole_information=bofo11_df,
                                                              depths=[6.1])


# %%
tunnel_intersection = tu_data.get_tunnel_fractures(structure_ID='48.1')
tunnel_pcd = np.asarray(tunnel_intersection['Pointcloud'].to_numpy()[0])
tunnel_center = np.asarray(tunnel_intersection['Center'].to_numpy()[0])
tu_geom = geometry.TunnelGeometry(tu_data)
tunnel_intersection['Dip (deg)'][0] = 90 - tunnel_intersection['Dip (deg)'][0]

# Create the control points.
<<<<<<< Updated upstream
curved = np.asarray([[tunnel_pcd[0,:]],
[tunnel_pcd[3,:]],
[tunnel_pcd[1,:]],
[tunnel_pcd[5,:]],
                     #mc_bfe05,
                     #mc_bfe06,
                     #mc_bfe07,
#                     mc_bofo08,
 #                    mc_bofo09,
  #                   mc_bofo10,
   #                  mc_bofo11,
=======
curved = np.asarray([mc_bfe05,
                     # mc_bfe06,
                     mc_bfe07,
                     mc_bofo08,
                     mc_bofo09,
                     mc_bofo10,
                     mc_bofo11,
>>>>>>> Stashed changes
                     [tunnel_center]
                        
                     ]).squeeze()

# %%

# Initialize the fracture geometry
frac = FractureGeo()
# Now initialize the gempy model
extent = (-350, -150, 0, 350, 1400, 1550)  # Is super important to set a correct extent because this will be the volume that we interpolate
frac.init_geo_model(extent=extent)
# We have a convenient function to create the dataframe needed for gempy
df_curved = frac.create_df_control_points(pointsA=curved)

df_orientation = frac.create_df_orientations(pointsA=tunnel_intersection['Center'][0],
                                            dipsA=tunnel_intersection['Dip (deg)'][0],
                                            azimuthA=tunnel_intersection['Azimuth (deg)'][0])
df_curved['smooth'] = 0.001
df_orientation['smooth'] = 0.001

vert, fac = frac.create_gempy_geometry(fixed_points=df_curved, fixed_orientations=df_orientation)


#bfe05_xyz = bfe_05[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()
#surf, vertices, faces = frac.mask_mesh(vert, fac,
#                                       from_maximum_distance_borehole=40,
#                                       positions=bfe05_xyz,
#                                       )

surf = frac.create_pyvista_mesh(vertices=vert, faces=fac)


# %%

p = model.ModelPlot()
p.add_object(spheres_from_points(curved, radius=1), name='observations', color='red')
p.add_object(tu_mesh, name='tunnel', color='grey', opacity=0.5)
p.add_object(surf, name='MC', color='blue', opacity=0.75)
#p.add_object(bh05, name='bh05', color='black', opacity=1)
#p.add_object(bh06, name='bh06', color='black', opacity=1)
#p.add_object(bh07, name='bh07', color='black', opacity=1)
# add bofos with green color
#p.add_object(bofo08, name='bofo08', color='green', opacity=0.5)
#p.add_object(bofo09, name='bofo09', color='green', opacity=0.5)
#p.add_object(bofo10, name='bofo10', color='green', opacity=0.5)
#p.add_object(bofo11, name='bofo11', color='green', opacity=0.5)

# show tunnel points
p.add_object(spheres_from_points(tunnel_pcd, radius=0.5),
             name='tunnel_pcd', color='red')

p.show()

p.save_scene_as_html(name + '.html')
surf.save(name + '.vtk')

# %%
