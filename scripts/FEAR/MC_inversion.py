import numpy as np
import pandas as pd
import pyvista as pv
from bedretto import (data,
                      geometry,
                      model)
from fracwave import OUTPUT_DIR, SourceEM, FractureGeo, Antenna, FracEM
import matplotlib.pyplot as plt
import pickle

import scipy.io as io
odir = OUTPUT_DIR + 'FEAR/MC/'
filename = odir + 'MC_real_inversion.h5'
from gdp import DATA_DIR as gdpdat
path_files = gdpdat + 'FEAR/'

p = model.ModelPlot()
from gdp.processing.image_processing import pick_line

frac = FractureGeo()
#%%
tun_dat = data.TunnelData()

gempy_extent = [-500,
                0,
                100,
                450,
                1300,
                1500]
resolution = (50, 50, 50)
TM_MC = 2380
tu_geom = geometry.TunnelGeometry(tun_dat)
tu_mesh = tu_geom.get_tunnel_mesh(TM_range=[TM_MC - 40, TM_MC + 40])

#%%
# mc = tun_dat.get_tunnel_fractures(structure_ID='48.1')
mc1 = tun_dat.get_tunnel_fractures(structure_ID='48')
mc2 = tun_dat.get_tunnel_fractures(structure_ID='49')

from fracwave.utils.help_decorators import generate_points_on_plane

pointsA = generate_points_on_plane(normal_vector=mc1['Normal'][0],
                                   reference_point= mc1['Center'][0],
                                   num_points=2)


pointsB = generate_points_on_plane(normal_vector=mc2['Normal'][0],
                                   reference_point= mc2['Center'][0],
                                   num_points=2)


df_tunnel = frac.create_df_control_points(pointsA=pointsA,
                                          # pointsB=pointsB
                                          )

df_orientations = frac.create_df_orientations(pointsA=mc1['Center'][0],
                                              dipsA= mc1['Dip (deg)'][0],
                                              azimuthA=mc1['Azimuth (deg)'][0],

                                              # pointsB=mc2['Center'][0],
                                              # dipsB=mc2['Dip (deg)'].to_list(),
                                              # azimuthB=mc2['Azimuth (deg)'].to_list()
                                              )
#%%
p100mhz = {'BFE_A_05': ['processed_BFE_A_05_141222_100MHz_v2.npz'],
           'BFE_A_06': ['processed_BFE_A_06_131222_100MHz_v2.npz'],
           'BFE_A_07': ['processed_BFE_A_07_141222_100MHz_v2.npz'],}


#%%
c = {'BFE_A_05': (10, 40, 250, 0),
     'BFE_A_06': (8, 30, 200, 0),
     'BFE_A_07': (40, 75, 200, 0)
     }
picks = {}
#%%

for bn, fn, in p100mhz.items():
    try:
        with open(odir + f'manual_picks_{bn}', 'rb') as f:
            picks[bn] = pickle.load(f)
    except:
        # break

        picks[bn] = {}
        file = fn[0]
        mat = np.load(odir+file)
        data = mat['data']
        travel_time = mat['travel_time']
        borehole_depth = mat['borehole_depth']
        radial_distance = mat['radial_distance']

        extent = (borehole_depth[0], borehole_depth[-1], travel_time[-1], travel_time[0])
        processed_data = np.divide(data, np.abs(data).max(axis=1)[:, None],
                                   out=np.zeros_like(data),
                                   where=np.abs(data).max(axis=1)[:, None] != 0)

        plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto'); plt.grid(); plt.show()

        cut = c[bn] # This will change for all
        mask_depth = (min(cut[0:2]) <= borehole_depth) & (borehole_depth <= max(cut[0:2]))
        mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
        cut_image = processed_data[mask_time][:, mask_depth]
        plt.imshow(cut_image, cmap='seismic', extent=cut, aspect='auto');plt.grid();plt.show()

        for p in range(10):
            if p not in picks[bn].keys():
                picks[bn][p] = pick_line(cut_image, colormap='seismic', interpolate_path=True, radius_point=1, extent=cut,
                                            automatic_naming=True)
            print(p)
        with open(odir + f'manual_picks_{bn}', 'wb') as f:
            pickle.dump(picks[bn], f)

#%% Second face
c_bot = {'BFE_A_05': (40, 150, 1000, 0),
         'BFE_A_06': (30, 50, 350, 0),
         'BFE_A_07': (75, 100, 200, 0)
         }

picks_bot = {}
# fn = p100mhz['MB4']
# bn = 'MB4'
# for bn in c.keys():
#     with open(odir + f'manual_picks_{bn}', 'rb') as f:
#         picks[bn] = pickle.load(f)
#%%
for bn, fn, in p100mhz.items():
    if bn in c_bot.keys():
        try:
            with open(odir + f'manual_picks_bot_{bn}', 'rb') as f:
                picks_bot[bn] = pickle.load(f)
        except:
            picks_bot[bn] = {}
            file = fn[0]
            mat = np.load(odir + file)
            data = mat['data']
            travel_time = mat['travel_time']
            depth = mat['borehole_depth']
            radial_distance = mat['radial_distance']

            extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
            processed_data = np.divide(data, np.abs(data).max(axis=1)[:, None],
                                       out=np.zeros_like(data),
                                       where=np.abs(data).max(axis=1)[:, None] != 0)

            plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto'); plt.grid(); plt.show()
            cut = c_bot[bn] # This will change for all
            mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
            mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
            cut_image = processed_data[mask_time][:, mask_depth]
            plt.imshow(cut_image, cmap='seismic', extent=cut, aspect='auto');plt.grid();plt.show()
            for pi in range(10):
                if pi not in picks_bot[bn].keys():
                    picks_bot[bn][pi] = pick_line(cut_image, colormap='seismic', interpolate_path=True, radius_point=1, extent=cut,
                                                automatic_naming=True)
                print(pi)
            with open(odir + f'manual_picks_bot_{bn}', 'wb') as f:
                pickle.dump(picks_bot[bn], f)
#%% Evaluate the error
from scipy import stats
val = {}
mean_val = {}
iqr = {}
all_normalized_points = {}
error_trace = {}
coordinates = {}
counter_points = {}
all_borehole_picks = set(list(c.keys()) + list(c_bot.keys()))
# all_borehole_picks = ['BFE_A_05']
include_top = True
include_bottom = False
for bn in all_borehole_picks:
    val = []
    if bn in picks and include_top:
        for no in picks[bn].keys():
            for pi in picks[bn][no].keys():
                val.append(np.c_[picks[bn][no][pi]['true coordinates'], picks[bn][no][pi]['pixel coordinates'][:, 0]])
    if bn in picks_bot and include_bottom:
        for no in picks_bot[bn].keys():
            for pi in picks_bot[bn][no].keys():
                val.append(np.c_[picks_bot[bn][no][pi]['true coordinates'], picks_bot[bn][no][pi]['pixel coordinates'][:, 0] + 10_000])  # Add 10_000 so can clearly distinguish between top and bottom
    coordinates[bn] = np.vstack(val)
    mv = []
    iq = []
    ci = []
    anp = [] # all normalized points
    for d in list(set(coordinates[bn][:,2])):
        pos = np.ravel(np.argwhere(np.isclose(int(d), coordinates[bn][:, 2].astype(int),)))
        if len(pos) == 0:  # No number
            print(f'Not found, {d, pos}')
            continue
        if len(pos) < 5:  #"Less than 5 picks, don't take it into account"
            continue
        iq.append(stats.iqr(coordinates[bn][pos, 1], interpolation='midpoint'))
        mv.append((coordinates[bn][pos, 0].mean(), coordinates[bn][pos, 1].mean()))
        anp.append(coordinates[bn][pos, 1] - coordinates[bn][pos, 1].mean())  # Take all the points and substract the mean to create a normal distributed data
        ci.append((d, len(pos)))
    all_normalized_points[bn] = np.hstack(anp)  # Take all the points and substract the mean to create a normal distributed data
    counter_points[bn] = np.asarray(ci, dtype=int)
    mean_val[bn] = np.asarray(mv)
    iqr[bn] = np.sqrt(np.square(iq).mean())
    error_trace[bn] = np.asarray(iq)

# mask = counter_points[bn][:,1] > 5  # More than 4 picks
aaa = np.hstack(all_normalized_points.values())
error_all = stats.iqr(aaa, interpolation='midpoint')

#%%
import seaborn as sns
from scipy.stats import norm
# fig, ax = plt.subplots()
fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(10,8),  gridspec_kw={'height_ratios': [1, 3]})
s = aaa
# sns.histplot(s, ax=axes[1], stat='density', alpha=0.5)
x, pdf  = sns.kdeplot(s, color='orange', ax=axes[1], label='Kernel density estimate').lines[0].get_data()

n_bins = 50
mu = s.mean()
sigma=np.std(s)
median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)

n, bins, patches = axes[1].hist(s, n_bins, density=True, alpha=.3, edgecolor='black' )
# pdf = 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2))
# axes[1].plot(bins, pdf, color='orange', alpha=.6)

mask1 = (x >= q1-1.5*(q3-q1)) & (x <= q1)
mask2 = (x <= q3+1.5*(q3-q1)) & (x >= q3)
bins_1 = x[mask1] # to ensure fill starts from Q1-1.5*IQR
bins_2 = x[mask2]
# pdf_1 = pdf[:int(len(pdf)/2)]
# pdf_2 = pdf[int(len(pdf)/2):]
pdf_1 = pdf[mask1]
pdf_2 = pdf[mask2]
# pdf_2 = pdf_2[(pdf_2 >= norm(mu,sigma).pdf(q3+1.5*(q3-q1))) & (pdf_2 <= norm(mu,sigma).pdf(q3))]

#fill from Q1-1.5*IQR to Q1 and Q3 to Q3+1.5*IQR
axes[1].fill_between(bins_1, pdf_1, 0, alpha=.4, color='orange')
axes[1].fill_between(bins_2, pdf_2, 0, alpha=.4, color='orange')


#top boxplot
axes[0].boxplot(s, 0, 'gD', vert=False)
axes[0].axvline(np.percentile(s, 50), color='red', alpha=.6, linewidth=.5)
axes[0].axis('off')


# axes[1].annotate("{:.1f}%".format(100*norm(mu, sigma).cdf(q1)), xy=((q1-1.5*(q3-q1)+q1)/2, 0), ha='center')
axes[1].annotate("{:.2f}".format(error_all), xy=(median, 0.05), ha='center')
# axes[1].annotate("{:.1f}%".format(100*(norm(mu, sigma).cdf(q3+1.5*(q3-q1)-q3)-norm(mu, sigma).cdf(q3))), xy=((q3+1.5*(q3-q1)+q3)/2, 0), ha='center')
axes[1].annotate('q1', xy=(q1, norm(mu, sigma).pdf(q1)), ha='center')
axes[1].annotate('q3', xy=(q3, norm(mu, sigma).pdf(q3)), ha='center')

axes[1].set_ylabel('Density')
axes[1].set_xlabel('Travel time (ns) - median for all points')

plt.legend()
plt.subplots_adjust(hspace=0)
plt.show()
fig.savefig(odir+'figures/error_all_points_real.pdf')

#%%
for bn in all_borehole_picks:
    file = p100mhz[bn][0]
    mat = np.load(odir + file)
    data = mat['data']
    travel_time = mat['travel_time']
    depth = mat['borehole_depth']
    radial_distance = mat['radial_distance']

    extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
    processed_data = np.divide(data, np.abs(data).max(axis=1)[:, None],
                               out=np.zeros_like(data),
                               where=np.abs(data).max(axis=1)[:, None] != 0)
    plt.figure()
    plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto')
    plt.grid()
    plt.plot(coordinates[bn][:,0], coordinates[bn][:,1], 'b.')
    plt.plot(mean_val[bn][:,0], mean_val[bn][:,1], 'r.')
    plt.title(bn)
    plt.show()

#%%
sou = SourceEM()
try:
    sou.load_hdf5(filename)
except:
    samples=300
    f = np.linspace(0, 0.2, 300)
    sou.frequency = f

    firstp = sou.create_source() #a=1,
    sou.export_to_hdf5(filename, overwrite=True)
    sou.plot()
# wavelength = 0.1234 / sou.peak_frequency
# v = 0.1234  # m/ns -> Velocity in granite
# wavelength = v / sou.peak_frequency  # m
# max_element_size = wavelength / 6
max_element_size = 1
#%%  Boreholes
ant1 = Antenna()
downsample = 10
try:
    ant1.load_hdf5(filename)
except:
    import os
    from bedretto import DATA_DIR, data, geometry
    data_directory = os.path.abspath(DATA_DIR + 'FEAR_data') + os.sep

    borehole_data_directory = data_directory + 'borehole_trajectories' + os.sep
    bor_dat = data.BoreholeData()
    # bh_geometry = geometry.BoreholeGeometry(bor_dat)

    info = {'BFE_A_05': pd.read_csv(borehole_data_directory + 'BFE_A_05_borehole_information.csv'),
            'BFE_A_06': pd.read_csv(borehole_data_directory + 'BFE_A_06_borehole_information.csv'),
            'BFE_A_07': pd.read_csv(borehole_data_directory + 'BFE_A_07_borehole_information.csv')}
    separation = 2.77

    depths_Tx = dict()
    depths_Rx = dict()
    orientations_Tx = dict()
    orientations_Rx = dict()

    # for bor in picks.keys():

    # ant.export_to_hdf5(file, overwrite=True)
    for bn in all_borehole_picks:  # Iterate over boreholes
        depths = mean_val[bn][::downsample, 0]  # Downsample
        depth_r0 = bor_dat.get_coordinates_from_borehole_depth(info[bn], depths-(separation*0.5)).T
        depth_r1 = bor_dat.get_coordinates_from_borehole_depth(info[bn], depths-(separation*0.5) + 0.1).T
        orient_r = (depth_r0 - depth_r1) / np.linalg.norm(depth_r0 - depth_r1, axis=1)[:, None]

        depth_t0 = bor_dat.get_coordinates_from_borehole_depth(info[bn], depths+(separation*0.5)).T
        depth_t1 = bor_dat.get_coordinates_from_borehole_depth(info[bn], depths +(separation*0.5)+ 0.1).T
        orient_t = (depth_t0 - depth_t1) / np.linalg.norm(depth_t0 - depth_t1, axis=1)[:, None]

        ant1.set_profile(bn,
                        receivers=depth_r0,
                        transmitters=depth_t0,
                        orient_receivers=orient_r,
                        orient_transmitters=orient_t,
                        depth_Rx=depths-(separation*0.5),
                        depth_Tx=depths+(separation*0.5))

    ant1.export_to_hdf5(filename, overwrite=True)
#%% Specific boreholes:
boreholes = ['BFE_A_05',
             'BFE_A_06',
             'BFE_A_07'
             ]


# transmitter = []
# receiver = []
measured_time = {}
tol_pos = []
for bn in boreholes:
    # transmitter.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Tx','Ty','Tz')].to_numpy())
    # receiver.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Rx','Ry','Rz')].to_numpy())
    measured_time[bn] = mean_val[bn][::downsample,1]
    tol_pos.append(error_trace[bn][::downsample])
# transmitter = np.vstack(transmitter)
# receiver = np.vstack(receiver)
# measured_time = np.hstack(measured_time)
tol_pos = np.hstack(tol_pos)
#%%
# frac.init_2surface_geo_model(gempy_extent, resolution)
frac.init_geo_model(gempy_extent, resolution)
from fracwave.inversion.geometry_inversion import GeometryInversion
inv1 = GeometryInversion(frac=frac,
                        extent=gempy_extent,
                        velocity=0.1234,
                        antenna=ant1,
                        measured_travel_time=measured_time)
#%%
try:
    inv.clear()
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_allBottom.csv')
    bottom_fit = pd.read_csv(odir + 'inverted_points_selected_allBottom.csv', index_col=0)[['X', 'Y', 'Z', 'pos']]
    top_fit = pd.read_csv(odir + 'inverted_points_selected_allTop.csv', index_col=0)[['X', 'Y', 'Z', 'pos']].dropna()  # Repeated from bottom

    inverted_points_selected = pd.concat((bottom_fit, top_fit))

    # inverted_points_selected = pd.read_csv(odir+'inverted_points_selected.csv')
    inv.fixed_control_points = inverted_points_selected
    # inverted_points_selected = inverted_points_selected[:-1]
    inv.fixed_orientations = df_orientations
    # if satisfied with the model
    v = 0.1234  # m/ns -> Velocity in granite
    wavelength = v / sou.peak_frequency  # m
    max_element_size = wavelength / 5
    #else
    # max_element_size=1
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
    inv.max_distance_from_borehole = 60
    inv.max_distance_from_surface_minimum_distance = 60
    # inv.max_distance_from_borehole = 25
    inv.forward_solver()
    # p.add_object(inv.grid, name='grid2', color='gray', opacity=0.8)

except:
    # bottom_fit = pd.read_csv(odir+'inverted_points_selected_allBottom.csv', index_col =0)
    # bottom_fit['pos'] = np.nan
    inv1.clear()
    tp = tol_pos.copy()
    tp[tp < error_all] = error_all
    inv1.tol_pos =tp # Tolerance for a single trace
    inv1.tol_all = error_all   # Convert to time. Tolerance for overall RMS error over the whole picks

    inv1.surrounding = 2
    inv1.max_iter_all = 20
    inv1.max_iter_pos = 10
    inv1.std_threshold = 0.3  # Standard deviation for the second convergence criteria
    inv1.std_average = 5
    inv1.resting_period = 5
    # inv.max_distance_from_borehole = 50  # mask everything farther away of 60 m
    inv1.max_distance_from_borehole = 60
    inv1.max_distance_from_surface_minimum_distance = 60


    inv1.fixed_control_points = df_tunnel
    # inv.fixed_control_points = None
    inv1.fixed_orientations = df_orientations
    inv.temp_control_points = None
    inv1.directory = odir
    inv1.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
    inv1.ylim=0,700
    inverted_points_selected = inv1.run_inversion()
    plt.plot(inv1.error_history); plt.axhline(error_all, color='r'); plt.xlabel('No. iterations'); plt.ylabel('RMS'); plt.show()
    # inverted_points_selected.to_csv(odir+'inverted_points_selected.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_allTop.csv')
    inverted_points_selected.to_csv(odir+'inverted_points_7_5.csv')

    # inverted_points_selected.to_csv(odir+'inverted_points_selected_top_only.csv')

#%%%%%%%%%%%%
p.add_object(tu_mesh, name='tunnel', color='grey', opacity=0.7)
p.add_object(ant1.Transmitter, name='transmitter', color='blue')
p.add_object(ant1.Receiver, name='receiver', color='red')

vertices, faces = frac.create_gempy_geometry(fixed_points=df_tunnel,
                                             fixed_orientations=df_orientations)


first_arrival, min_position, surface = inv.forward_solver()
# [p.add_object(mesh, name=key, label=key,opacity=0.5, color=np.random.randint(0,255,3)) for key, mesh in inv.grid.items()]
p.add_object(inv1.grid, name='mash',opacity=0.5, color='yellow')
#%%%%%%%%%%%%
#%%
latest = dict(grid = inv.grid,
              vertices =inv.vertices,
              faces = inv.faces)

#%% To make a lot of pictures:
bottom_fit = pd.read_csv(odir + 'inverted_points_selected_allBottom.csv', index_col=0)[['X', 'Y', 'Z', 'pos']]
top_fit = pd.read_csv(odir + 'inverted_points_selected_allTop.csv', index_col=0)[
        ['X', 'Y', 'Z', 'pos']].dropna()  # Repeated from bottom

p = model.ModelPlot()
import os
from bedretto import DATA_DIR, data, geometry

data_directory = os.path.abspath(DATA_DIR + 'FEAR_data') + os.sep

borehole_data_directory = data_directory + 'borehole_trajectories' + os.sep
bor_dat = data.BoreholeData()
# bh_geometry = geometry.BoreholeGeometry(bor_dat)

info = {'BFE_A_05': pd.read_csv(borehole_data_directory + 'BFE_A_05_borehole_information.csv'),
        'BFE_A_06': pd.read_csv(borehole_data_directory + 'BFE_A_06_borehole_information.csv'),
        'BFE_A_07': pd.read_csv(borehole_data_directory + 'BFE_A_07_borehole_information.csv')}

from bedretto import data
bor_dat = data.BoreholeData()
bh_geometry = geometry.BoreholeGeometry(bor_dat)
bfe05_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_05'], radius=0.5)
bfe06_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_06'], radius=0.5)
bfe07_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_07'], radius=0.5)
p.add_object(bfe05_graphic, name='BFE05', color='black')
p.add_object(bfe06_graphic, name='BFE06', color='black')
p.add_object(bfe07_graphic, name='BFE07', color='black')

p.add_3d_label(points=info['BFE_A_05'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA05',name='BFE_A_05',c='gray',
                offset=(0, 20),
                s=8)
p.add_3d_label(points=info['BFE_A_06'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA06',name='BFE_A_06',c='gray',
                offset=(0,20),
                s=8)
p.add_3d_label(points=info['BFE_A_07'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA07',name='BFE_A_07',c='gray',
                offset=(0,80),
                s=8)
p.add_object(tu_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
geometries = {}
for df, name, color in zip([pd.concat((bottom_fit, top_fit)), bottom_fit, top_fit, df_tunnel],
                    ['joint', 'bottom', 'top', 'initial'],
                    ['blue', 'red', 'green', 'yellow']):
    inv.clear()
    inverted_points_selected = df
    inv.fixed_control_points = inverted_points_selected
    inv.fixed_orientations = df_orientations
    v = 0.1234  # m/ns -> Velocity in granite
    wavelength = v / sou.peak_frequency  # m
    max_element_size = wavelength / 5
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
    inv.max_distance_from_borehole = 60
    inv.max_distance_from_surface_minimum_distance =60
    inv.forward_solver()
    geometries[name] = dict(grid = inv.grid,
              vertices =inv.vertices,
              faces = inv.faces)
    p.add_object(inv.grid, name=name, color =color,opacity=0.8, label=name)
    if name == 'joint':
        iv = inverted_points_selected.dropna()
        cp = iv.reset_index()[['X', 'Y', 'Z']].to_numpy()
        p.add_object(cp, color='black', name='cp', render_points_as_spheres=True, point_size=10, label='Contol points')

p.add_legend()
#%%
inv.fixed_control_points = df_tunnel
# inv.fixed_control_points = inverted_points_selected
# inv.fixed_control_points = None
inv.fixed_orientations = df_orientations
inv.max_distance_from_borehole = None
inv.max_distance_from_borehole = 25
inv.kwargs_remesh = dict(max_element_size=1, extrapolate=False, plane=0)
first_arrival, min_position = inv.forward_solver()
initial = dict(grid = inv.grid,
              vertices =inv.vertices,
              faces = inv.faces)

#%%
from scipy import spatial
surf_tree = spatial.KDTree(initial['vertices'])
dd_trajectory, ii = surf_tree.query(latest['vertices'], workers=-1)

latest['grid']['Distance'] = dd_trajectory

#%%


#%%
# p.add_object(inv.grid, name='grid', color='yellow')
p.add_object(latest['grid'], name='gridF', scalars='Distance', cmap='turbo', show_scalar_bar = False)
p.add_object(initial['grid'], name='gridI', color='yellow', show_scalar_bar = False)
p.add_object(bottom['grid'], name='gridB', color='red', show_scalar_bar = False)
p.add_object(top['grid'], name='gridT',    color='green', show_scalar_bar = False)
# p.add_object(initial['grid'], name='grid', color='yellow')#, opacity=0.8)

#%% Plot of pints
import os
from bedretto import DATA_DIR, data, geometry

data_directory = os.path.abspath(DATA_DIR + 'FEAR_data') + os.sep

borehole_data_directory = data_directory + 'borehole_trajectories' + os.sep
bor_dat = data.BoreholeData()
# bh_geometry = geometry.BoreholeGeometry(bor_dat)

info = {'BFE_A_05': pd.read_csv(borehole_data_directory + 'BFE_A_05_borehole_information.csv'),
        'BFE_A_06': pd.read_csv(borehole_data_directory + 'BFE_A_06_borehole_information.csv'),
        'BFE_A_07': pd.read_csv(borehole_data_directory + 'BFE_A_07_borehole_information.csv')}

from bedretto import data
bor_dat = data.BoreholeData()
bh_geometry = geometry.BoreholeGeometry(bor_dat)
bfe05_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_05'], radius=0.5)
bfe06_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_06'], radius=0.5)
bfe07_graphic = bh_geometry.construct_borehole_geometry(borehole_info=info['BFE_A_07'], radius=0.5)
p.add_object(bfe05_graphic, name='BFE05', color='black')
p.add_object(bfe06_graphic, name='BFE06', color='black')
p.add_object(bfe07_graphic, name='BFE07', color='black')

p.add_3d_label(points=info['BFE_A_05'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA05',name='BFE_A_05',c='gray',
                offset=(0, 20),
                s=8)
p.add_3d_label(points=info['BFE_A_06'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA06',name='BFE_A_06',c='gray',
                offset=(0,20),
                s=8)
p.add_3d_label(points=info['BFE_A_07'][-1:][['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy(),labels='BFEA07',name='BFE_A_07',c='gray',
                offset=(0,80),
                s=8)
p.add_object(tu_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# p.remove_3d_label('all')

def project_planes(mesh, name, p, **kwargs):
    mesh = mesh.extract_geometry()
    projectedxy = mesh.project_points_to_plane(origin=(0, 0, p.bounds[4]), normal=(0, 0, 1))
    p.add_object(projectedxy, name=f'{name}_xy', **kwargs)

    projectedxz = mesh.project_points_to_plane(origin=(0, p.bounds[3], 0), normal=(0, 1, 0))
    p.add_object(projectedxz, name=f'{name}_xz', **kwargs)

    projectedyz = mesh.project_points_to_plane(origin=(p.bounds[1], 0, 0), normal=(1, 0, 0))
    p.add_object(projectedyz, name=f'{name}_yz', **kwargs)


# p.add_object(tu_mesh, name='tunnel', opacity=0.1)
# tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
p.add_object(tu_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# project_/planes(tu_mesh, f'TM', p, opacity=0.01, color='black')
#
# points = {}
# for bn in boreholes:
    # from bedretto.core.common_helpers import tube_from_points
    # df = ant.profiles.loc[ant.profiles['profile'] == bn]
    # points[bn] = df.iloc[0][['Tx', 'Ty', 'Tz']].to_numpy()
    # spline = pv.Spline(df[['Tx','Ty','Tz']].to_numpy(), 400)
    # bh_tube = spline.tube()
    # bh_tube = tube_from_points(df[['Tx','Ty','Tz']].to_numpy(), radius=1)

    # p.add_object(df.iloc[0][['Tx', 'Ty', 'Tz']].to_numpy(), name=f'borehole_{bn}', color='black' )#if bn != 'MB1' else 'red')

    # project_planes(bh_tube, f'pb_{bn}', p, opacity=0.1, color='black' )# if bn != 'MB1' else 'red')

# project_planes(latest['grid'], name='grid', p=p, opacity=0.3, scalars='Distance', cmap='turbo')
# p.remove_scalar_bar()
# p.plotter.add_scalar_bar(title=f'Distance (m)', title_font_size=40,
#                          label_font_size=40, vertical=False,
#                          position_x=0.3, position_y=0.05)
p.add_3d_scalar_bar(scalars='Distance', title='Distance from initial model (m)')
#
iv = inverted_points_selected.dropna()
cp = iv.reset_index()[['X', 'Y', 'Z']].to_numpy()
p.add_object(cp, color='black', name='cp', render_points_as_spheres=True, point_size=10)

p.add_label(points=cp,always_visible=True,
            labels=np.arange(0, len(cp)), name='b1', point_size=10, point_color='black', font_size=45)

project_planes(pv.PolyData(cp), color='black', name='cp', p=p, point_size=10)
iv.reset_index()[['X', 'Y', 'Z']].to_numpy()
# p.remove_scalar_bar()
# p.plotter.add_scalar_bar(title=f'Distance (m)', title_font_size=40,
#                              label_font_size=40, vertical=False,
#                              position_x=0.3, position_y=0)

p.show(yzgrid2=True)
p.plotter.window_size = (2100,2000)
p.plotter.camera_position = [(-300.0695993418476, -407.9928034103359, 1510.7549942757203),
 (-51.11014031822393, 30.605904921282036, 1401.5124118870704),
 (0.07201522586759396, 0.20237978441960933, 0.976655635370785)]

p.plotter.add_axes(box=True)
p.snapshot('figures/end_geometry2.pdf', save_dir=odir)
p.save_scene_as_html(odir+'figures/MC_geometry_GPR_only')
#%% What if set these and start moving the other half? Can we constrain further?
#%% o make convergence plot
new_points = inverted_points_selected.dropna().reset_index(drop=True)
current = df_tunnel.copy()
ea = []
for i, df in new_points.iterrows():
    current = current.append(df[['X','Y','Z']])
    # inv.max_distance_from_borehole = 40  # mask everything farther away of 60 m
    inv.max_distance_from_borehole = 60
    inv.max_distance_from_surface_minimum_distance = 60
    # inv.measured_travel_time =
    inv.fixed_control_points = current
    # inv.fixed_control_points = None
    inv.fixed_orientations = df_orientations
    # inv.temp_control_points = df_tunnel
    first_arrival, min_position = inv.forward_solver()
    ea.append(np.sqrt(np.square(measured_time - first_arrival).mean()))

fig, ax = plt.subplots()
ax.plot(ea)
ax.axhline(error_all, color='r')
ax.set_xlabel('No. inserted control points')
ax.set_ylabel('RMS')
plt.show()
# inverted_points_selected.to_csv(odir+'inverted_points_selected.csv')
# inverted_points_selected.to_csv(odir + 'inverted_points_selected_top_only.csv')

#%% Simulation:

frac = FractureGeo()
try:
    frac.load_hdf5(filename)
except:
    # inverted_points_selected = pd.read_csv(odir + 'inverted_points_selected2.csv')
    # frac.init_geo_model(extent=extent, resolution=resolution)
    # v, _ = inv.frac.create_gempy_geometry(fixed_points=inverted_points_selected,
    #                                   fixed_orientations=df_orientations,
    #                                   move_points=None)
    # v = 0.1234  # m/ns -> Velocity in granite
    # wavelength = v / sou.peak_frequency  # m
    # max_element_size = wavelength / 5
    # # max_element_size = 1
    # grid_inverse, vertices_inverse, faces_inverse = inv.frac.remesh(latest['vertices'], max_element_size=max_element_size, extrapolate=False, plane=1)
    #
    kwargs_fracture_properties = dict(aperture=0.1,
                                      electrical_conductivity=0,
                                      electrical_permeability=81)

    # dfinverted = frac.set_fracture('Inverted', vertices=vertices_inverse,
    #                                 faces=faces_inverse, overwrite=True, **kwargs_fracture_properties)
    dfinverted = frac.set_fracture('Inverted', vertices=latest['vertices'],
                                    faces=latest['faces'], overwrite=True, **kwargs_fracture_properties)
    frac.export_to_hdf5(filename, overwrite=True)

#%% Just look at MB1
ant = Antenna()
downsample = 10
try:
    ant.load_hdf5(filename)
except:
    boreholes = ['MB1']
    bor_dat = data.BoreholeData()
    bor_geom = geometry.BoreholeGeometry(borehole_data=bor_dat)

    bh_data_all = {bn: bor_dat.get_borehole_data(borehole_name=bn,
                                                 columns=['Depth (m)',
                                                          'Easting (m)',
                                                          'Northing (m)',
                                                          'Elevation (m)',
                                                          ])
                   for bn in boreholes}
    bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
                                                            radius=1)
                   for bn, info in bh_data_all.items()}

    separation = 2.77

    depths_Tx = dict()
    depths_Rx = dict()
    orientations_Tx = dict()
    orientations_Rx = dict()

    # for bor in picks.keys():

    # ant.export_to_hdf5(file, overwrite=True)
    for bn in boreholes:  # Iterate over boreholes
        depths =  np.arange(0,150,0.15)#e
        depth_r0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5)).T
        depth_r1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5) + 0.1).T
        orient_r = (depth_r0 - depth_r1) / np.linalg.norm(depth_r0 - depth_r1, axis=1)[:, None]

        depth_t0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths+(separation*0.5)).T
        depth_t1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths +(separation*0.5)+ 0.1).T
        orient_t = (depth_t0 - depth_t1) / np.linalg.norm(depth_t0 - depth_t1, axis=1)[:, None]

        ant.set_profile(bn,
                        receivers=depth_r0,
                        transmitters=depth_t0,
                        orient_receivers=orient_r,
                        orient_transmitters=orient_t,
                        depth_Rx=depths-(separation*0.5),
                        depth_Tx=depths+(separation*0.5))

    ant.export_to_hdf5(filename, overwrite=True)

#%%
solv = FracEM()
try:
    solv.load_hdf5(filename)
    freq = solv.get_freq()
except:
    solv.rock_epsilon = 5.9
    solv.rock_sigma = 0.0
    solv.backend = 'torch'
    solv.engine='tensor_trace'
    solv.open_file(filename)
    solv.time_zero_correction()
    # solv.solve_for_profile = 'MB1'
    freq = solv.forward_pass()

#%%
file = 'MB1_100MHz_200515.mat'
from gdp import DATA_DIR as gdpdat
mat = io.loadmat(gdpdat + 'VALTER/' + file)
depth = mat['depth'][:, 0]
processed_data = mat['processed_data']
travel_time = mat['travel_time'][0]
radius = mat['radius'][0]


processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)
from scipy.stats import rv_histogram
#%%
fig = plt.figure(constrained_layout=True, figsize=(12,8))
subfig = fig.subfigures(nrows=1, ncols=1)
AX = subfig.subplots(nrows=1, ncols=2)
extent = (depth[0], depth[-1], radius[-1], radius[0])
AX[0].imshow(processed_data, extent=extent, cmap='seismic', aspect='auto', vmax=1, vmin=-1)

time_response, time_vector = solv.get_ifft(freq)
time_response[np.isnan(time_response)] = 0
from gdp.processing.gain import apply_gain
tr, gm= apply_gain(data=time_response.T,
           sfreq=1/(time_vector[1] - time_vector[0]),
           gain_type='spherical',
           exponent= 2,
           velocity=0.1234,
           twoway= True)
tr /= np.abs(tr).max()

subset_of_noise = [1100, 1400, 5200, 5800]
subset = processed_data[subset_of_noise[0]:subset_of_noise[1], subset_of_noise[2]:subset_of_noise[3]]
histogram, bin_edges = np.histogram(subset, bins=100, )
hist_distribution = rv_histogram((histogram, bin_edges))

noise = hist_distribution.rvs(size=tr.shape)
tr += noise*0.3
tr /= np.abs(tr).max()
extent = (0, 150, time_vector[-1] *0.1234*0.5, time_vector[0] *0.1234*0.5)

cax = AX[1].imshow(tr, aspect='auto', cmap='seismic', vmax=1, vmin=-1, extent=extent)
# ax.plot(coordinates['MB1'][:,0], coordinates['MB1'][:,1] * 0.1234 / 2, '.' )
for ax, na in zip(AX,
    ['MB1 real data', 'MB1 simulated data'],
    # [(travel_time[0], travel_time[-1]),(time_vector[0], time_vector[-1])]
                      ):
    ax.set_ylabel('Borehole depth (m)')
    ax.set_xlabel('Radial distance (m)')
    ax.set_xlim(15, 150)
    ax.set_ylim(60, 0)

    ax1 = ax.twinx()
    ax1.set_ylabel('Two-way travel time (ns)')
    start, end = 0,60

    ax1.set_ylim(end, start)
    ax.yaxis.set_ticks(np.arange(0, 60, 5))
    ax1.yaxis.set_ticks(np.arange(0, 60, 5) * 2 / 0.1234)
    ax.xaxis.set_ticks(np.arange(15, 150, 15))

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(na)
    ax.grid(linestyle='--')

subfig.colorbar(cax, ax=AX, label='Norm. GPR Amplitude (-)', orientation='horizontal')
fig.savefig(odir+'figures/real_boreholes_simulation.pdf')
plt.show()

#%% Pick the image to have a plot and compare
from gdp.processing.image_processing import pick_first_arrival

pick = pick_first_arrival(time_response.T, threshold=0.3)

plt.imshow(tr, aspect='auto')
plt.plot(pick[:,0], pick[:,1])
plt.show()

# Convert to normal coordinates










#%% Where does it cross in the boreholes???
from bedretto.utils.ray_path_calculation import intersection_of_surface_with_trajectory
boreholes = ['ST1',
             'ST2',
             'MB1',
             'MB2',
             'MB4',
             'MB5',
             # 'MB7', # Problematic. Don't know if I'm picking the correct reflector
             'MB8',  # Problematic. Does not converge
             'MB3'

             ]

bor_dat = data.BoreholeData()
frac = FractureGeo()
frac.load_hdf5(filename)
surf = frac.get_surface()
#%%
from bedretto import DATA_DIR as dbdir
real_pos = pd.read_csv(dbdir+'logging_data/BB_logging.csv')
diff={}
table = pd.DataFrame()
for bo in boreholes:
    xyz = bor_dat.get_borehole_data(bo)
    trajectory = xyz[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()
    depth = xyz[['Depth (m)']].to_numpy()
    inter = intersection_of_surface_with_trajectory(trajectory, surf, depth=depth, reduce_meshes=True)
    interse = inter['Intersection depth (m)'][0]
    rpo = real_pos.loc[real_pos['Borehole'] == bo, ['Depth (m)']].to_numpy()
    r = np.mean(rpo)

    table = pd.concat([table, pd.DataFrame({'Depth calc (m)': interse,
                  'Depth real (m)': r,
                  'Uncert (m)': np.abs(rpo[0] - r),
                 'Diff (m)': r - interse,
                  'Borehole': bo} )])

#############################%%%############################
#############################%%%############################
#%%
#############################%%%############################
#############################%%%############################

fract_complete = FractureGeo()
fract_complete.init_2surface_geo_model(gempy_extent, resolution=resolution)