from bedretto import model, data, geometry
from fracwave import FractureGeo, OUTPUT_DIR, SourceEM, Antenna, FracEM
from fracwave.utils.help_decorators import convert_meter
import pandas as pd
import numpy as np
#%% Load data
file = OUTPUT_DIR + 'structural_model.out'
fault_zone_dir = '/home/daniel/GitProjects/temp/general-model/data/logging_data/FaultZones.xlsx'


sou = SourceEM()
sou.set_frequency_vector(np.linspace(0, 0.5, 201))
# sou.set_source_params(a=source_parameters[0],
#                       c=source_parameters[1],
#                       loc=source_parameters[2],
#                       scale=source_parameters[3],
#                       ph=source_parameters[4],
#                       t=source_parameters[5])
sou.type='ricker'
sou.set_center_frequency(0.1)
sou.create_source()
sou.plot_waveforms_complete().show()
# sou.plot_waveforms_zoom().show()
sou.export_to_hdf5(file, overwrite=True)
#%%
df = pd.read_excel(fault_zone_dir, sheet_name='FZs_orig_table')

ids = df['id'].unique()
ids=ids[~pd.isna(ids)]  # Faultzones

c0 = 299_792_458  # Speed of light in m/s
rock_epsilon = 5.9  # Relative permitivity of the medium (dielectric constant)
velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)  # To convert in m/ns
max_element_size = velocity / (sou.center_frequency * 5)
print(f'Element max size = {convert_meter(max_element_size)}')
#%%
bor_dat = data.BoreholeData()
bor_geom= geometry.BoreholeGeometry(bor_dat)

m = {'cb1': 'MB1',
     'cb2': 'MB2',
     'cb3': 'MB3',
     'mb4': 'MB4',
     'tunnel': 'Tunnel',
     'st1': 'ST1',
     'st2': 'ST2'}

boreholes =  {'ST1': ('red', (30, 50)),
              'ST2': ('red', (-60, 40)),

              'MB1': ('blue', (-35, 34)),
              'MB2': ('blue', (-18, -30)),
              'MB3': ('blue', (-54,-8)),
              'MB4': ('blue', (56, 15)),
              # 'MB5': ('blue', (35, 32)),
              # 'MB7': ('blue', (-60, -20)),
              # 'MB8': ('blue', (20,-50)),
              }
# Load all borehole geometries
bh_data_all = {bn: bor_dat.get_borehole_data(borehole_name=bn,
                                             columns=['Depth (m)',
                                                      'Easting (m)',
                                                      'Northing (m)',
                                                      'Elevation (m)',
                                                      ])

               for bn in boreholes.keys()}
bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
                                                        radius=1)
               for bn, info in bh_data_all.items()}

labels = list(bh_data_all.keys())
points = np.asarray([bh.iloc[-1][['Easting (m)',
                                     'Northing (m)',
                                     'Elevation (m)']].to_numpy() for label, bh in bh_data_all.items()])

# Load tunnel mesh
tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)

# bh_colors = {'MB1':'black', 'MB2':'black', 'MB3':'black', 'MB4':'black', 'MB5':'black', 'MB7':'black','MB8':'black',
#              'ST1':'red','ST2':'red','Welltec':'green'}

tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2150])

p = model.ModelPlot()
p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# p.add_label(tunnel_markers, labels='TM markers', name='tunnel markers')

# [p.add_object(mesh, cmap='viridis', scalars='Depth (m)', name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
[p.add_object(bh_geom_all[key], color=color, name='borehole_{}'.format(key)) for key, (color, offset) in boreholes.items()]
# p.add_label(points=points,
#              labels=labels,
#              name='label_boreholes',
#              font_size=16,
#              point_size=1,
#              ) # For tunnel

# Add tunnelmarkers
markers = np.arange(2000, 2101, 100)
pos_markers, df_tunnel = tun_dat.get_xyz_from_TM(markers)
p.add_3d_label(points = pos_markers,
               labels=list(map(lambda x: f'TM {x}', markers)),
               name='TunnelMarker',
               color='grey',
               s=10)

[p.add_3d_label(points = points[i],
               labels= key,
               name=f'BoreholeMarker_{key}',
               c=color,
               offset=offset,
               s=8) for i, (key, (color, offset)) in enumerate(boreholes.items())]
p.show(number_of_divisions=10)
#%%
faults = {}
bor_at_faults = {}
borehole_all_pos = {}
for i in ids:
    temp_df = df.loc[df['id'] == i, ('borehole', 'md', 'dip', 'dipdir')]
    temp_df = temp_df.groupby('borehole').mean().reset_index()
    name = []
    xyz = []
    orientation = []
    borehole_positions = []
    for k, row in temp_df.iterrows():
        info = bor_dat.get_borehole_data(m[row['borehole']])
        name.append(m[row['borehole']])
        xyz.append(np.hstack((bor_dat.get_xyz_coordinates_from_depths(m[row['borehole']], row['md']),
                    row['dip'],
                    row['dipdir'])))
        borehole_positions.append(info[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy())

    bor_at_faults[i] = name
    faults[i] = np.asarray(xyz)
    borehole_all_pos[i] = np.vstack(borehole_positions)


#%%
gempy_extent = [-250,
                20,
                -250,
                20,
                1200,
                1500]
resolution = (50, 50, 50)
frac = FractureGeo()
frac.init_geo_model(extent=gempy_extent)
#%%
# p = model.ModelPlot()
projection_plane={1.0: 1,
                  2.0: 1,
                  3.0: 1,
                  5.0: 1,
                  # 6.0:1,
                  7.0: 1,
                  8.0: 1,
                  9.0: 1,
                  10.0: 1,
                  # 11.0:1,
                  12.0: 2,
                  13.0: 1,
                  # 15.0: 1,
                  17.0: 1,
                  # 18.0: 1,
                  19.0: 1,
                  }
color = {1.0: '#A6CCEE3',
         2.0: '#33A02C',
         3.0: '#1F78B4',
         5.0: '#FB9A99',
         6.0: '#FDBF6D',
         7.0: 'red',
         8.0:  'brown',
         9.0: '#CAB2D6',
         10.0: '#6A3D9A',
         11.0: 'orange',
         12.0: '#1B9E77',
         13.0: '#7570B3',
         15.0: 'pink',
         17.0: '#33CCFF',
         18.0: '#6600FF',
         19.0: '#00D454',
      }
#%%
for i in ids:
    if i==6 or i==11 or i==15:
        continue
    xyz = faults[i]
    if len(xyz) == 1:
        continue
    points = frac.create_df_control_points(pointsA=xyz[:, :3])
    orientations = frac.create_df_orientations(pointsA=xyz[:, :3],dipsA=xyz[:,3], azimuthA=xyz[:,4])

    vert, fac = frac.create_gempy_geometry(fixed_points = points, fixed_orientations=orientations)

    if i in projection_plane.keys():
        surf, vert, fac = frac.remesh(points=vert, spacing=[10, 10], max_element_size=max_element_size, plane=projection_plane[i])
        # surf, vert, fac = frac.remesh(points=vert, spacing=[10,10], max_element_size=15, plane=projection_plane[i])
    else:
        continue
        # surf = frac.create_pyvista_mesh(vert, fac, name=str(i))
    # break
    surf, vertices, faces = frac.mask_mesh(#grid=surf,v
                                            vert, fac,
                                           from_maximum_distance_borehole=10,
                                           positions=borehole_all_pos[i],
                                           )

    if len(vertices) == 0:
        continue
    # surf = frac.create_pyvista_mesh(vertices, faces, name=str(i))
    p.add_object(surf, name=str(i), opacity=0.5, color=color[i], label=str(i))

    frac.set_fracture(name_fracture=str(i),
                      vertices=vertices,
                      faces=faces,
                      aperture=0.05,
                      electrical_conductivity=0.01,
                      electrical_permeability=81,
                      overwrite=True)

p.add_3d_legend()
frac.export_to_hdf5(filename=file, overwrite=True)

#%% Anf I don't want to run all that then i just run the saved one
frac = FractureGeo()
frac.load_hdf5(filename=file)

#%%
for name in frac.name_fractures:
    surf = frac.get_surface(name_fractures=[name])
    p.add_object(surf, name=name, opacity=0.5, color=color[float(name)], label=name)

#%% Antenna
boreholes = ['ST1', 'ST2', 'MB1', 'MB2', 'MB3', 'MB4']

ant = Antenna()
separation = 2.77

depths_Tx = dict()
depths_Rx = dict()
orientations_Tx = dict()
orientations_Rx = dict()

# for bor in picks.keys():

# ant.export_to_hdf5(file, overwrite=True)
for bn in boreholes:  # Iterate over boreholes
    end = bor_dat.get_borehole_end_coordinates(bn)
    depths = np.arange(20, end['Depth (m)'], 1)

    depth_r0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths - (separation * 0.5)).T
    depth_r1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths - (separation * 0.5) + 0.1).T
    orient_r = (depth_r0 - depth_r1) / np.linalg.norm(depth_r0 - depth_r1, axis=1)[:, None]

    depth_t0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths + (separation * 0.5)).T
    depth_t1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths + (separation * 0.5) + 0.1).T
    orient_t = (depth_t0 - depth_t1) / np.linalg.norm(depth_t0 - depth_t1, axis=1)[:, None]

    ant.set_profile(bn,
                    receivers=depth_r0,
                    transmitters=depth_t0,
                    orient_receivers=orient_r,
                    orient_transmitters=orient_t,
                    depth_Rx=depths - (separation * 0.5),
                    depth_Tx=depths + (separation * 0.5))

ant.export_to_hdf5(file, overwrite=True)
ant.plot()
#%%
import shutil
import os

name_fractures = ['1.0', '2.0', '3.0', '7.0', '8.0', '9.0', '10.0', '12.0', '13.0', '17.0', '19.0']
for n in name_fractures:
    new_file = shutil.copy(file, file.split('.')[0] + f'_fracture_{n}.out')
    break

    solv = FracEM()
    solv.open_file(new_file)

    solv.rock_epsilon = 5.9
    solv.rock_sigma = 0.0
    solv.engine = 'tensor_trace'
    solv.backend = 'torch'
    solv._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have energy in the source function. We also skip some frequencies that don't contribute to the final result. Set to False to include all the frequencies and elements
    solv.apply_causality = False # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those fracture elements that are too far from the antennas and will not show in the time window. Not calculating if _fast_calcualtion_incoming_field is False
    solv.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achieve this, we apply a filtering process that eliminates the lower n% of energy across all fracture elements. This selective approach allows us to reduce computation time by excluding elements that have minimal impact on the final results.
    solv._filter_percentage = 0.01
    solv.mode='reflection'
    solv._solve_for_fracture = n
#_, _2 = solv.time_zero_correction()

    info = solv.open_file(file)
    #freq_t0 = solv.time_zero_correction()
    print(solv)
    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, )

#%% Plot the seismicity
catalogs = [OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int7_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int8_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int9_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int10_Ph1_relativeReloc_V1_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int11_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int11_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int12_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int13_Ph1_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase1/Catalog_Int14_Ph1_relativeReloc_V0_2022November.csv',


            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase2/Catalog_Int8_Ph2_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase2/Catalog_Int9_10_Ph2_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase2/Catalog_Int11_Ph2_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase2/Catalog_Int12_Ph2_relativeReloc_V0_2022November.csv',


            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase3/Catalog_Int11_Ph3_relativeReloc_V0_2022November.csv',
            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase3/Catalog_Int12_Ph3_relativeReloc_V0_2022November.csv',


            OUTPUT_DIR + 'SeismicCatalogs_HydroData/Phase5/Catalog_Int11_Ph5_relativeReloc_V0_2022November.csv']


df_seismicity = pd.concat([pd.read_csv(c) for c in catalogs])

xyz = df_seismicity[['Eastern Bedretto', 'Northern Bedretto', 'Depth']].to_numpy()


p.add_object(xyz, name='seismicity', point_size=2, opacity=0.5, render_points_as_spheres=True)