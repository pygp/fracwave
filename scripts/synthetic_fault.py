import os
import numpy as np
import shutil
import pandas as pd
from bedretto import model
os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cpu"
import gempy as gp

import pyvista as pv
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt
from fracwave import OUTPUT_DIR, FracEM, FractureGeo, Antenna, SourceEM
from fracwave import set_logger
logger = set_logger(__name__)
#%%
def init_geo_model(extent, resolution):
    geo_model = gp.create_model()
    geo_model = gp.init_data(geo_model, extent=extent, resolution=resolution)
    gp.set_interpolator(geo_model, theano_optimizer='fast_run', verbose=[])
    geo_model.add_surfaces(['F', "basement"])
    return geo_model
#%%
resolution = (50,50,50)
bounding_box = [-5, 5, -5, 5, -3.5, 3.5]
#%% This will be used for the antenna positions
z = np.arange(bounding_box[-2], bounding_box[-1], 0.2)
borehole = np.c_[np.repeat(0, len(z)),np.repeat(0, len(z)),z]
borehole[:,0] += 6

bor_df = pd.DataFrame()
bor_df['Easting (m)'] = borehole[:,0]
bor_df['Northing (m)'] = borehole[:,1]
bor_df['Elevation (m)'] = borehole[:,2]
bor_df['Azimuth (deg)'] = 0
bor_df['Dip (deg)'] = 90
bor_df['Depth (m)'] = np.linspace(0,np.abs(z[0]) + np.abs(z[-1]), len(z))
# bor_df['Depth (m)'] = np.linspace(np.abs(z[0]) + np.abs(z[-1]),0, len(z))

bor = pv.Line((borehole[:, 0].mean(),
               borehole[:,1].mean(),
               bounding_box[-2]),
              (borehole[:, 0].mean(),

               borehole[:,1].mean(),
              bounding_box[-1]))
tube = bor.tube(radius=0.2)
#%%
from fracwave import FractureGeo
frac1 = FractureGeo()
# frac2 = FractureGeo()
#%%
spacing=(4,4)
p1 = (70, 90, 0) # Dip, azimuth, depth
# p2 = (30, 180, -2) # dip, azimuth, depth
v1, f1 = frac1.create_regular_squared_mesh(10,10,spacing, p1[0], p1[1], p1[2])#, axis=2)
# v2, f2= frac2.create_regular_squared_mesh(30,30,spacing, p2[0], p2[1], p2[2], axis=2)
del frac1.surface #, frac2.surface

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.1 # m
min_element_size = wavelength/6

frac1.remesh(frac1.vertices, spacing=spacing, plane=0, max_element_size=min_element_size)
# frac2.remesh(frac2.vertices, spacing=spacing, plane=1, max_element_size=wavelength/4)
#%% Get the bounds for creating the gempy model
gempy_extent = frac1.surface.bounds
#%%
p = model.ModelPlot(title='Synthetic model')
#%%
p.add_object(tube, name='borehole', color='gray')
p.add_object(frac1.surface, name='fault1', color='red', opacity=1)
# p.add_object(frac2.surface, name='fault2', color='green', opacity=1)
p.show(zxGrid2=False, yzGrid2=False)
#%%
def get_index_closer_values(borehole, vertices):
    distances = cdist(borehole, vertices)
    pos = []
    for count, po in enumerate(distances):
        pos.append(np.where(po == min(po))[0][0])
    pos = np.asarray(pos)
    return vertices[pos]

index1 = get_index_closer_values(borehole, frac1.vertices)
# index2 = get_index_closer_values(borehole, frac2.vertices)

#%%
ind = 8
init_points = index1[10::ind]
p.add_object(pv.PolyData(init_points), name ='close fault1', color='black')
# p.add_object(pv.PolyData(index2[::ind]), name ='close fault2', color='black')

#%%
# np.random.seed(1234)
# scale = 0.5 # meter
# gp_points_1 = index1[::ind] + np.random.uniform(low =-scale, high=scale, size=index1[::ind].shape)
# gp_points_2 = index2[::ind] + np.random.uniform(low =-scale, high=scale, size=index2[::ind].shape)
#
# #%%
geo_model1 = init_geo_model(gempy_extent, resolution)
# geo_model2 = init_geo_model(bounding_box, resolution)
# #%%
# geo_model1.add_surface_points(X=0, Y=0, Z=p1[-1], surface='F')
# geo_model1.add_orientations(X=0, Y=0, Z=p1[-1], surface='F', orientation=[p1[1], p1[0], 1])
#
# geo_model2.add_surface_points(X=0, Y=0, Z=p2[-1], surface='F')
# geo_model2.add_orientations(X=0, Y=0, Z=p2[-1], surface='F', orientation=[p2[1], p2[0], 1])
#
# #%%
# for gm, p in [(geo_model1, gp_points_1), (geo_model2, gp_points_2)]:
#     for i in p:
#         gm.add_surface_points(X=i[0], Y=i[1], Z=i[2], surface='F')
# #%%
# gp.compute_model(geo_model1)
# gp.compute_model(geo_model2)
# #%%%
# gp.plot_3d(geo_model1, plotter_type='background')
# gp.plot_3d(geo_model2, plotter_type='background')

#%%
def create_geometry(geo_model,
                    fixed_points,
                    fixed_orientations,
                    move_points,
                    id_points_fixed: list=[],
                    id_points_move:list=[],
                    id_orientation_fixed: list=[],
                    id_orientation_move: list=[],
                    ):
    """
    Make the update of the geometry
    Args:
        geo_model: Same geo_model so it is initiallice once
        fixed_points: Points that will not move like the points from logging or tunnel measurements
        fixed_orientations: The orientations (Azimuth and dip) of the points from logging and tunnel measurements.
        move_points: These are the proposed points that will shape the geometry and help constrain it
        extent: Bounding box for the fault geometry
        max_element_size: In the remeshing step, the size of the elements will not be bigger that this value (in meters)
        id_points_fixed: index number of the fixed points. This to not remove them
        id_points_move: index number of points to delete and or move
        id_orinetation_fixed:
        id_orientation_move:
        set_properties: boolean to indicate if properties are set to each element of the geometry
    Returns:
        Simulated response of the fracture
    """
    # Include all fixed points and orientations
    logger.debug(f'Setting {len(fixed_points)} fixed points and orientations from borehole '
                f'logging and tunnel observations')
    s = len(id_points_fixed)
    for i in range(len(fixed_points)):
        if s>0:
            if fixed_points[i] in geo_model.surface_points.df[['X', 'Y', 'Z']].to_numpy():
                continue
        geo_model.add_surface_points(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F')
        geo_model.add_orientations(X=fixed_points[i, 0], Y=fixed_points[i, 1], Z=fixed_points[i, 2], surface='F',
                                   orientation=[fixed_orientations[i, 0], fixed_orientations[i, 1], 1])
        id_points_fixed.append(geo_model.surface_points.df.index[-1])
        id_orientation_fixed.append(geo_model.surface_points.df.index[-1])

    # delete all other points
    extra_points = list(geo_model.surface_points.df.index)
    [extra_points.remove(i) for i in id_points_fixed if i in extra_points]
    [geo_model.delete_surface_points(i) for i in extra_points if i in list(geo_model.surface_points.df.index)]
    # Include all other points
    if move_points is not None:
        logger.debug(f'Setting {len(move_points)} of additional control points to constrain the geometry')
        # if len(id_points_move)>0:
        #     [geo_model.delete_surface_points(i) for i in id_points_move]
        #     id_points_move = []
        for i in range(len(move_points)):
            geo_model.add_surface_points(X=move_points[i, 0], Y=move_points[i, 1], Z=move_points[i, 2], surface='F')
            id_points_move.append(geo_model.surface_points.df.index[-1])
        # TODO: extra orientations
    gp.compute_model(geo_model)
    logger.debug('Model computed')
    vertices = geo_model.solutions.vertices[0]
    faces = geo_model.solutions.edges[0]
    return vertices, faces, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move

def mesh_gempy_surface(vertices, max_element_size, set_properties: bool=True, plane=0, order=2):
    from fracwave import FractureGeo
    frac = FractureGeo()

    resolution = (20, 20)
    surf = frac.remesh(vertices, plane=plane, order=order, spacing=resolution, max_element_size=max_element_size)
    min_resol = np.ceil(surf.length / max_element_size).astype(int)
    resolution = (min_resol, min_resol)
    if surf.dimensions[0] < min_resol:
        logger.debug('Remeshing...')
        surf = frac.remesh(vertices, plane=plane, order=order, spacing=resolution)
    if set_properties:
        logger.debug(f'Setting properties to {frac.nelements} elements. This can take a few minutes...')
        df = frac.set_fracture_properties(aperture=0.5,
                                          electrical_conductvity=0.001,
                                          electrical_permeability=81)
    logger.debug('Done')
    return frac, surf

def surface_from_points(vertices, faces):
    faces_all_stacked = np.hstack([np.hstack([len(f), f]) for f in faces])
    surface = pv.PolyData(vertices, faces_all_stacked)
    return surface
#%%
fixed_1 = np.asarray([(0,0,p1[-1])])
fixed_or_1 = np.asarray([(p1[1],p1[0],1)])
# fixed_2 = np.asarray([(0,0,p2[-1])])
# fixed_or_2 = np.asarray([(p2[1],p2[0],1)])


#%%
move_gp_points_1 = init_points
# move_gp_points_2 = index2[::ind]

#%% Calculate planes using gempy = Initial model

id_points_fixed1=[]
id_orientation_fixed1=[]
id_points_move1=[]
# id_orientation_move1=[]
vertices1, faces1, id_points_fixed1, id_orientation_fixed1, id_points_move1, id_orientation_move1 = \
    create_geometry(geo_model=geo_model1,
                                 fixed_points=fixed_1,
                                 fixed_orientations=fixed_or_1,
                                 move_points=move_gp_points_1,
                                 id_points_fixed=id_points_fixed1,
                                 id_orientation_move=id_orientation_fixed1,
                                 id_points_move=id_points_move1,
                                 )
gemp_points_1 = geo_model1.surface_points.df[['X','Y','Z']].to_numpy()
#%%
# id_points_fixed2=[]
# id_orientation_fixed2=[]
# id_points_move2=[]
# vertices2, faces2, geo_model2, frac2_pln, id_points_fixed2, id_orientation_fixed2, id_points_move2, id_orientation_move2 = \
#     update_geometry_and_simulate(geo_model=geo_model2,
#                                  fixed_points=fixed_2,
#                                  fixed_orientations=fixed_or_2,
#                                  move_points=move_gp_points_2,
#                                  max_element_size=min_element_size,
#                                  set_properties=False,
#                                  plane=2,
#                                  id_points_fixed=id_points_fixed2,
#                                  id_orientation_move=id_orientation_fixed2,
#                                  id_points_move=id_points_move2,
#                                  )

#%%
# pm1 = model.ModelPlot(title='Planar gempy model')
#%%
# pm1.add_object(surface_from_points(vertices1, faces1), name='planar_gempy', color='red', opacity=0.7)
p.add_object(surface_from_points(vertices1, faces1), name='fault1', color='blue', opacity=0.7)
#%%
frac1_pln, surf1 = mesh_gempy_surface(vertices=vertices1,
                                  max_element_size=min_element_size,
                                  plane=0,
                                  set_properties=False)
#%%
# p.remove('planar_gempy')
#%%
p.add_object(frac1_pln.surface, name='fault1', color='green', opacity=0.8, label='fault1')
# pm1.add_object(frac2_pln.surface, name='fault2', color='green', opacity=1)

p.add_object(pv.PolyData(gemp_points_1), name ='close fault1', color='green')
# pm1.add_object(pv.PolyData(geo_model2.surface_points.df[['X','Y','Z']].to_numpy()), name ='close fault2', color='black')

p.show(zxGrid2=False, yzGrid2=False)
p.add_legend()
#%%
np.random.seed(1234)
scale = 0.5 # meter
move_gp_points_2 = init_points + np.random.uniform(low =-scale, high=scale, size=init_points.shape)

# move_gp_points_2 = index2[::ind] + np.random.uniform(low =-scale, high=scale, size=index2[::ind].shape)

#%%

id_points_fixed2=[]
id_orientation_fixed2=[]
id_points_move2=[]
# id_orientation_move1=[]
vertices2, faces2, id_points_fixed2, id_orientation_fixed2, id_points_move2, id_orientation_move2 = \
    create_geometry(geo_model=geo_model1,
                    fixed_points=fixed_1,
                                 fixed_orientations=fixed_or_1,
                                 move_points=move_gp_points_2,
                                 id_points_fixed=id_points_fixed2,
                                 id_orientation_move=id_orientation_fixed2,
                                 id_points_move=id_points_move2,
                                 )
gemp_points_2 = geo_model1.surface_points.df[['X','Y','Z']].to_numpy()
#%%
# pm2 = model.ModelPlot(title='Moved points')
#%%
p.add_object(surface_from_points(vertices2, faces2), name='fault2', color='red', label='fault2')
# p.add_object(pv.PolyData(gemp_points_2), name ='close fault2', color='red')
# p.add_legend()
#%%
frac2_syn, surf2 = mesh_gempy_surface(vertices=vertices2,
                                  max_element_size=min_element_size,
                                  plane=0,
                                  set_properties=False, order=2)
#%%
# bor = pv.Line((0,0,bounding_box[-2]), (0,0,bounding_box[-1]))
# tube = bor.tube(radius=0.5)
# p.add_object(tube, name='borehole', color='gray')
p.add_object(frac2_syn.surface, name='fault2', color='red', opacity=0.8, label='fault2')
# pm2.add_object(frac2_syn.surface, name='fault2', color='green', opacity=1)

p.add_object(pv.PolyData(gemp_points_2), name ='close fault2', color='red')
# pm2.add_object(pv.PolyData(geo_model2.surface_points.df[['X','Y','Z']].to_numpy()), name ='close fault2', color='black')

p.show(zxGrid2=False, yzGrid2=False)
p.add_legend()

# #%%
# p_compare = model.ModelPlot(title='Comparison')
# #%%
# bor = pv.Line((0,0,bounding_box[-2]), (0,0,bounding_box[-1]))
# tube = bor.tube(radius=0.5)
# p_compare.add_object(tube, name='borehole', color='gray')
# p_compare.add_object(frac2_syn.surface, name='fault1_curve', color='red', opacity=1)
# # p_compare.add_object(frac2_syn.surface, name='fault2_curve', color='green', opacity=1)
#
# p_compare.add_object(frac1_pln.surface, name='fault1_plane', color='red', opacity=0.5)
# # p_compare.add_object(frac2_pln.surface, name='fault2_plane', color='green', opacity=0.5)
# p_compare.show(zxGrid2=False, yzGrid2=False)


#%% Do the simulation
file_plane = OUTPUT_DIR + 'synthetic_case_simulation_plane.h5'

frac1_pln.set_fracture_properties(aperture=0.5,
                                      electrical_conductvity=0.001,
                                      electrical_permeability=81)
frac1_pln.export_to_hdf5(file_plane, overwrite=True)


file_curve = OUTPUT_DIR + 'synthetic_case_simulation_curved.h5'
frac2_syn.set_fracture_properties(aperture=0.5,
                                      electrical_conductvity=0.001,
                                      electrical_permeability=81)
frac2_syn.export_to_hdf5(file_curve, overwrite=True)
#%%
p.add_object(pv.PolyData(frac1_pln.fracture_properties[['x','y','z']].to_numpy()),
             name='fault1_points', color='green', )
p.add_object(pv.PolyData(frac2_syn.fracture_properties[['x','y','z']].to_numpy()),
             name='fault2_points', color='red', )
#%%
def set_default(file):
    # ------------- SourceEM
    import matplotlib.pyplot as plt
    from fracwave import SourceEM

    samples = 300
    sou = SourceEM()
    f = np.linspace(0, 0.3, samples)
    sou.frequency = f

    firstp = sou.create_source() #a=1,
                               # b=0.02,
                               # t=5)
    # sou.plot()
    # plt.xlim(0,0.1)
    # plt.show()
    sou.export_to_hdf5(file, overwrite=True)
    # ------------- Antenna
    ant = Antenna()
    ant.set_profile('Prof1',
                    transmitters=borehole,
                    receivers=borehole,
                    orient_receivers=np.repeat([(0,0,1)], len(borehole), axis=0),
                    orient_transmitters=np.repeat([(0,0,1)], len(borehole), axis=0))

    ant.export_to_hdf5(file, overwrite=True)
    return sou, ant
#%%
set_default(file_curve)
set_default(file_plane)

#%% Run simulation
def solv_frac(file):
    from fracwave import FracEM
    solv = FracEM()
    solv.rock_epsilon = 6.
    solv.rock_sigma = 0.0
    # solv.engine = 'tensor_all'
    solv.engine = 'dummy'
    # solv.engine = 'tensor_trace'
    # solv.engine = 'tensor_numba'

    solv.units = 'SI'
    solv.filter_energy = True
    solv._filter_percentage = 0.5
    # solv.backend = 'torch'
    solv.backend = 'numpy'

    solv.open_file(file)
    # solv._fast_calculation_incoming_field = True
    # solv.mode = 'incoming_field'

    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    return solv, freq
#%%
solv_curve, freq_curve = solv_frac(file_curve)
#%%
solv_plane, freq_plan = solv_frac(file_plane)
#%%
filename = OUTPUT_DIR + 'synthetic_case_simulation_plane (copy).h5'
# filename = OUTPUT_DIR + 'synthetic_case_simulation_curved (copy).h5'
solv_dummy, time_dummy = solv_frac(filename)

#%%
time_response_curve, time_vector_curve = solv_curve.get_ifft(freq_curve)
time_response_plan, time_vector_plan = solv_plane.get_ifft(freq_plan)

#%%
from gdp.processing.image_processing import pick_first_arrival
threshold = 0.1
import matplotlib.pyplot as plt
vmax = np.abs(time_response_curve).max(axis=1)
tv_syn = time_response_curve / vmax[:,None]
# vmax = vmax
vmax = 1
plt.imshow(tv_syn.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, interpolation='bicubic',
           extent = (0, tv_syn.shape[0], solv_curve.time_window, 0))

pick_syn = pick_first_arrival(tv_syn.T,threshold=threshold)
plt.plot(pick_syn[:,0], pick_syn[:,1], 'g-', label='Curved')
plt.xlabel('Borehole depth (m)')
plt.title('Curved fault')
plt.ylabel('Time [ns]')
plt.colorbar()
plt.show()

#%%
import matplotlib.pyplot as plt
vmax = np.abs(time_response_plan).max()#axis=1)
tv_plan = time_response_plan #/ vmax[:,None]
vmax = vmax
plt.imshow(tv_plan.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, interpolation=None,
           extent = (0, tv_plan.shape[0], solv_plane.time_window, 0))
pick_plan = pick_first_arrival(tv_plan.T,threshold=threshold)
plt.plot(pick_plan[:,0], pick_plan[:,1], 'g-', label='Planar')
plt.xlabel('Borehole depth (m)')
plt.ylabel('Time [ns]')
plt.title('Planar fault')
plt.colorbar()
plt.show()

#%%
# np.save(OUTPUT_DIR + 'pick_plan.npy', pick_plan)
# np.save(OUTPUT_DIR + 'pick_curve.npy', pick)
#%%
from fracwave import FracEM
solv_curve = FracEM()
solv_curve.load_hdf5(file_curve)
freq_curve = solv_curve.file_read('simulation/summed_response')

solv_plane = FracEM()
solv_plane.load_hdf5(file_plane)
freq_plan = solv_plane.file_read('simulation/summed_response')

#%%
from gdp.processing.image_processing import pick_first_arrival

plt.plot(pick_syn[:,0], pick_syn[:,1], 'r-', label='Curved')
plt.plot(pick_plan[:,0], pick_plan[:,1], 'b-', label='Planar')

plt.xlabel('Traces')
plt.ylabel('Samples')
plt.legend()
plt.show()

#%%
plt.plot(pick_syn[:,0], pick_syn[:,1] - pick_plan[:,1], 'r-', label='Curved - Planar')
# plt.plot(pick_plan[:,0], pick_plan[:,1], 'b-', label='Planar')
plt.hlines(0, pick_syn[0,0], pick_syn[-1,0])
plt.xlabel('Traces')
plt.ylabel('Curved - Planar')
plt.show()


#%%
solv_curve = FracEM()
solv_curve.backend='torch'
solv_curve.load_hdf5(OUTPUT_DIR + 'synthetic_case_simulation_curved.h5')
# p.add_object(pv.PolyData(solv_plane.file_read('mesh/geometry/midpoint').numpy()),
#              name='fault1_points', color='green', )
p.add_object(pv.PolyData(solv_curve.file_read('mesh/geometry/midpoint').numpy()),
             name='fault2_points', color='red', )


#%%
import shutil

from gdp.processing.image_processing import pick_first_arrival
gempy_extent = (-1.72, 1.72, -5.0, 5.0, -4.7, 4.7)
resolution = (50,50,50)
geo_model = init_geo_model(gempy_extent, resolution)
fixed = np.asarray([(0,0,p1[-1])])
fixed_or = np.asarray([(p1[1],p1[0],1)])

# Initial Guess


threshold = 0.02
#%%
def forward_operator(constrain_gempy_points, fixed, fixed_or):
    """

    Returns:

    """
    constrain_points = np.zeros((len(constrain_gempy_points) //3, 3))
    c = 0
    for i, p in enumerate(constrain_gempy_points):
        val = i % 3
        constrain_points[c, val] = p
        if val == 2:
            c += 1

    filename = OUTPUT_DIR + 'synthetic_case_simulation_curved.h5'
    if not os.path.isfile(OUTPUT_DIR+'synthetic_case_simulation_curved_iter_0.h5'):
        shutil.copy2(filename, OUTPUT_DIR+'synthetic_case_simulation_curved_iter_0.h5')
    c = 0
    while True:
        filename = OUTPUT_DIR + f'synthetic_case_simulation_curved_iter_{c}.h5'
        if os.path.isfile(filename):
            filename_next = OUTPUT_DIR + f'synthetic_case_simulation_curved_iter_{c+1}.h5'
            if os.path.isfile(filename_next):
                c += 1
                continue
            shutil.copy2(filename, filename_next)
            filename = filename_next
            break
        else:
            shutil.copy2(OUTPUT_DIR + f'synthetic_case_simulation_curved_iter_{c-1}.h5', filename)
            break

    # Is this really neccesary?
    id_points_fixed2 = []
    id_orientation_fixed2 = []
    id_points_move2 = []
    # id_orientation_move1=[]
    vertices, faces, id_points_fixed, id_orientation_fixed, id_points_move, id_orientation_move = \
        create_geometry(geo_model=geo_model,
                        fixed_points=fixed,
                        fixed_orientations=fixed_or,
                        move_points=constrain_points,
                        id_points_fixed=id_points_fixed2,
                        id_orientation_move=id_orientation_fixed2,
                        id_points_move=id_points_move2,
                        )
    # gemp_points = geo_model.surface_points.df[['X', 'Y', 'Z']].to_numpy()

    frac, surf = mesh_gempy_surface(vertices=vertices,
                                    max_element_size=min_element_size,
                                    plane=0,
                                    set_properties=True,
                                    order=2)
    frac.export_to_hdf5(filename, overwrite=True)

    # return solv_frac(filename)

    solv, freq = solv_frac(filename)
    time_response, time_vector = solv.get_ifft(freq)
    pick = pick_first_arrival(time_response.T, threshold=threshold)
    np.save(filename.split('.')[0] + '_pick.npy', pick)

    # error = np.sqrt(np.square(np.subtract(pick_plan[:,1], pick[:,1])).mean())
    error = np.sqrt(np.square(np.subtract(pick_curved[:,1], pick[:,1])).mean())
    logger.info(f'Root Mean Squared error: {error}')
    # return pick, time_response, time_vector
    return pick, time_response, time_vector, solv

pick_plan = np.load(OUTPUT_DIR + 'pick_plan.npy')
pick_curved = np.load(OUTPUT_DIR + 'pick_curve.npy')
def objective_function(constrain_gempy_points):
    logger.info(f'Testing for contrain points: \n{constrain_gempy_points}')
    pick, time_response, time_vector, solv = forward_operator(constrain_gempy_points.ravel())
    return np.sqrt(np.square(np.subtract(pick_plan[:,1], pick[:,1])).mean())

def get_minimum_position(data_sim, data_true, delta_t, velocity):
    """
    Get the position of the biggest difference and a distance
    Args:
        data_sim: Simulated travel time
        data_true: True travel time
        delta_t: time per sample [ns / sample]
        velocity: Velocity of EM [m / ns]

    Returns:

    """
    dif = np.abs(data_true[:, 1] - data_sim[:, 1])
    pos = np.where(dif == dif.max())[0][0]  # Take the first found value
    # Now that we have the max position whe identify the elements from which the borehole is sensitive
    sample_diff = data_true[:, 1][pos] - data_sim[:, 1][pos]
    step = sample_diff * delta_t * velocity / 2 # two way travel time
    # Positive step means move towards borehole
    # Negative step means move away borehole
    return pos, step
    # f = solv.file_read('simulation/fracture_field')
    #
    # points = solv.file_read('mesh/geometry/midpoint')
    # # Initial, target, obtained
    # p = model.ModelPlot(title='Minimize')
    # p.add_object(tube, name='borehole', color='gray')
    #
    # #
    # import pyvista as pv
    # surf = pv.PolyData(points.numpy())
    # surf['f'] = f[pos]
    # p.add_object(surf, name='frcature_field', cmap='viridis', scalars='f')
    # #
    # element_higher_energy = np.where(f[pos] == f[pos].max())[0][0]
    # pos_high_energy = points[element_higher_energy].numpy()
    # p.add_object(pv.PolyData(new_gp_point), name='b', color='blue', point_size=10)


def run_simulation(pick_true, fixed_points_geometry, fixed_oientations_geometry, constrained_points_geometry):
    fixed = fixed_points_geometry
    fixed_or = fixed_oientations_geometry
    pick, time_response, time_vector, solv = forward_operator(constrain_gempy_points.ravel(), fixed, fixed_or)

    pos, step = get_minimum_position(pick, pick_true, sou.dtime, v)

    f = solv.file_read('simulation/fracture_field').numpy()
    points = solv.file_read('mesh/geometry/midpoint').numpy()
    element_higher_energy = np.where(f[pos] == f[pos].max())[0][0]
    pos_high_energy = points[element_higher_energy]

    # borehole position
    Tx = solv.file_read('antenna/Tx').numpy()[pos]

    vector = (pos_high_energy - Tx) / np.linalg.norm(pos_high_energy - Tx)
    move = vector * step

    new_gp_point = pos_high_energy + move





#%%
constrain_gempy_points = np.asarray([[0.89864941,  0.12210877, -3.37883444],
                                     [0.93922058,  0.27997581, -2.02387851],
                                     [-0.07264459,  0.30187218,  0.04356908],
                                     [0.02385303, -0.14218273,  0.96832576]])
real_points = np.asarray([[1.20713, 0, -3.3165622],
                          [0.653862, 0, -1.7964711],
                          [0.15089116, 0, -0.41457027],
                          [-0.3520796, 0, 0.96733063]])

pick, time_response, time_vector, solv = forward_operator(constrain_gempy_points.ravel())

#%%
import matplotlib.pyplot as plt
vmax = np.abs(time_response).max()#axis=1)
vmax = vmax
pick = pick_first_arrival(time_response.T, threshold=threshold)
plt.imshow(time_response.T, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax, interpolation=None,
           extent = (0, time_response.shape[0], time_vector[-1], time_vector[0]))
plt.plot(pick[:,0], pick[:,1], 'g-', label='Planar')
plt.xlabel('Borehole depth (m)')
plt.ylabel('Time [ns]')
plt.title('Simulated fault')
plt.colorbar()
plt.show()

#%%

plt.plot(pick[:,0], pick[:,1], 'r-', label='Curved')
plt.plot(pick_plan[:,0], pick_plan[:,1], 'b-', label='Planar')

plt.xlabel('Traces')
plt.ylabel('Samples')
plt.legend()
plt.show()

#%% What do we need for the inversion?

def create_bounds(points, extent):
    bounds = []
    for ir, row in enumerate(points):
        for ic, col in enumerate(row):
            index = ic % 3
            if index == 0: # x
                b = (extent[0], extent[1])
            elif index == 1: # y
                b = (extent[2], extent[3])
            elif index == 2: # z
                b = (extent[4], extent[5])
            bounds.append(b)
    return bounds

#%%
bounds = create_bounds(constrain_gempy_points, gempy_extent)
#%%
from scipy.optimize import minimize

res = minimize(fun = objective_function,
               x0=constrain_gempy_points.ravel(),
               bounds=bounds,
               method='nelder-mead',
               options={'maxfun': 10,
                        'xatol': 1e-6,
                        'disp': False}
               )

#%% Read the values
max_iter = 467

from fracwave import FracEM
import matplotlib.pyplot as plt
# fig, ax = plt.subplots()
x = []
y = []
all_picks = []
solv = FracEM()
for i in range(1, max_iter):
    # filename = OUTPUT_DIR+f'synthetic_case_simulation_curved_iter_{i}.h5'
    filename = OUTPUT_DIR+f'dummy_plane_iter_{i}.h5'


    solv.load_hdf5(filename)
    # freq = solv.file_read('dummy_/summed_response')
    # time_response, time_vector = solv.get_ifft(freq)
    # pick = pick_first_arrival(time_response.T, threshold=threshold)
    pick = solv.file_read('dummy_solution/first_arrival')
    all_picks.append(pick)
    # all_picks.append(pick[:,1])
    error = np.sqrt(np.square(np.subtract(pick_curved, pick)).mean())
    # error = np.sqrt(np.square(np.subtract(pick_plan[:,1], pick[:,1])).mean())

    x.append(i)
    y.append(error)

#%%
all_picks = np.asarray(all_picks)
from sklearn.metrics import mean_squared_error
# rmse = [mean_squared_error(pick_plan[:,1],pic, squared=False) for pic in all_picks]
rmse = [mean_squared_error(pick_curved, pic, squared=False) for pic in all_picks]

#%%
plt.plot(x, rmse)
plt.title('Inversion using Scipy Nelder-Mead')
plt.xlabel('# iterations')
plt.ylabel('RMSE')
plt.show()

#%% Initial, target, obtained
pl = model.ModelPlot(title='Comparison')
pl.add_object(tube, name='borehole', color='gray')
#%%
geometry_init = FractureGeo()
g_i = OUTPUT_DIR + 'synthetic_case_simulation_curved.h5'
geometry_init.load_hdf5(g_i)
solv_init = FracEM()
solv_init.load_hdf5(g_i)
time_response, time_vector = solv_init.get_ifft(solv_init.file_read('simulation/summed_response'))
pick = pick_first_arrival(time_response.T, threshold=0.03)

plt.imshow(time_response.T,vmax=np.abs(time_response).max(),vmin=-np.abs(time_response).max(),  aspect='auto', cmap='seismic')
plt.plot(pick[:,0], pick[:,1])
plt.title('Initial')
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()
#%%
pl.add_object(geometry_init.surface, name='Initial', color ='red', label='Initial')


geometry_target = FractureGeo()
g_t = OUTPUT_DIR+'synthetic_case_simulation_plane.h5'
geometry_target.load_hdf5(g_t)
solv_target = FracEM()
solv_target.load_hdf5(g_t)
time_response, time_vector = solv_target.get_ifft(solv_target.file_read('simulation/summed_response'))
plt.imshow(time_response.T,vmax=np.abs(time_response).max(),vmin=-np.abs(time_response).max(),  aspect='auto', cmap='seismic')
plt.title('Target')
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()

pl.add_object(geometry_target.surface, name='Target', color ='green', label='Target')


geometry_inv = FractureGeo()
g_inv = OUTPUT_DIR + f'synthetic_case_simulation_curved_iter_{321}.h5'
geometry_inv.load_hdf5(g_inv)
solv_inv = FracEM()
solv_inv.load_hdf5(g_inv)
time_response, time_vector = solv_inv.get_ifft(solv_inv.file_read('simulation/summed_response'))
plt.imshow(time_response.T,vmax=np.abs(time_response).max(),vmin=-np.abs(time_response).max(),  aspect='auto', cmap='seismic')
plt.title('Inverse')
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()

pl.add_object(geometry_inv.surface, name='Inverted', color ='yellow', label='Inverted geometry')

pl.add_legend()

#%%

#%%
filenames_pics = []
for i in range(1, max_iter):
    # plot the line chart
    plt.plot(all_picks[i], 'b', label='Simulated')
    plt.plot(pick_curved, 'r--', label='Objective')
    # plt.plot(pick_plan[:,1], 'r--', label='Objective')
    plt.ylim(60, 120)
    plt.xlabel('Traces')
    plt.ylabel('Samples')
    plt.title(f'Iteration: {i}')
    plt.legend()

    # create file name and append it to a list
    filename_pic = OUTPUT_DIR + f'{i}.png'
    filenames_pics.append(filename_pic)

    # save frame
    plt.savefig(filename_pic)
    plt.close()  # build gif

import imageio
with imageio.get_writer(OUTPUT_DIR + 'mygif.gif', mode='I') as writer:
    for fnp in filenames_pics:
        image = imageio.v2.imread(fnp)
        writer.append_data(image)

# Remove files
for fi in set(filenames_pics):
    os.remove(fi)

#%%
from fracwave import FracEM
solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
# solv.engine = 'tensor_all'
# solv.engine = 'loop'
solv.engine = 'tensor_trace'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv._fast_calculation_incoming_field = True
solv.backend = 'torch'
# solv.backend = 'numpy'
filename = OUTPUT_DIR + 'synthetic_case_simulation_plane (copy).h5'
solv.open_file(filename)
# solv._fast_calculation_incoming_field = True
# solv.mode = 'incoming_field'
#%%
from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_frequencies
a=np.abs(solv.file_read('source/source'))
freq_mask = get_mask_frequencies(a, threshold=0.01, levels=50)
# pos_max_freq = np.where(a == a.max())[0][0]
# freq_mask[pos_max_freq-2 : pos_max_freq+2] = False
plt.plot(solv.file_read('source/frequency'), solv.file_read('source/source'), 'b')
plt.plot(solv.file_read('source/frequency')[freq_mask], solv.file_read('source/source')[freq_mask], 'r*')
plt.show()

#%%
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, mask_frequencies=freq_mask)
#%%
freq_true = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, mask_frequencies=None)
# freq_mask = np.ones(freq_mask.shape).astype(bool)

#%%
# plt.plot(np.abs(freq_true[10]), 'b')
# plt.plot(np.abs(freq_l[10]), 'r')
plt.plot(np.abs(freq[10]), 'b')
# plt.plot(time_response[10], 'b')
# plt.plot(time_response_true[10], 'b')
# plt.plot(np.abs(freq_final[10] - freq_true[10].numpy()), 'orange')
# plt.plot(np.abs(freq_l[10] - freq_true[10].numpy()), 'orange')
plt.show()
# time_response_true, time_vector_tru = solv.get_ifft(freq_true)
#%%
time_response, time_vector = solv.get_ifft(freq)
# from gdp.processing.image_processing import pick_first_arrival
from gdp.processing.filtering import filter_data
from gdp.plotting.plot import plot_frequency_information
sou = SourceEM()
sou.load_hdf5(filename)
plot_frequency_information(time_response_true[10],dt=1/(sou.sampling_frequency*2))
plot_frequency_information(time_response[10],dt=1/(sou.sampling_frequency*2))

data_f = filter_data(time_response.T, btype='lowpass', fq=0.04, sfreq=sou.sampling_frequency*2)
plot_frequency_information(data_f[:,10],dt=1/(sou.sampling_frequency*2))

#%%
time_response, time_vector = solv.get_ifft(freq)
from gdp.processing.image_processing import _pick_first_arrival
pick = pick_first_arrival(time_response.T, threshold=0.05)
# time_response, time_vector = solv.get_ifft(freq_true - freq_final)
plt.imshow(time_response.T, vmax=np.abs(time_response).max(), vmin=-np.abs(time_response).max(),  aspect='auto', cmap='seismic')
plt.plot(pick[:,0], pick[:,1], 'green')
# plt.title('Curved loop solver')
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.colorbar()
plt.show()

#%% detect the place with the major error

dif = np.abs(pick[:,1] - pick_curved[:, 1])
pos = np.where(dif == dif.max())[0][0] # Take the first found value
# Now that we have the max position whe identify the elements from which the borehole is sensitive

#%%
solv = FracEM()
solv.load_hdf5(filename)
f = solv.file_read('simulation/fracture_field')

points = solv.file_read('mesh/geometry/midpoint')
#%% Initial, target, obtained
p = model.ModelPlot(title='Minimize')
p.add_object(tube, name='borehole', color='gray')

#%%
import pyvista as pv
surf = pv.PolyData(points)
surf['f']=f[5]
p.add_object(surf, name='frcature_field', cmap='viridis', scalars='f')
#%%
element_higher_energy = np.where(f[pos] == f[pos].max())[0][0]
pos_high_energy = points[element_higher_energy].numpy()
p.add_object(pv.PolyData(pos_high_energy), name='a', color='black', point_size=10)



#%%

pick, time_response, time_vector, solv = forward_operator(pos_high_energy.ravel())


#%%
#%%

plt.imshow(time_response.T,vmax=np.abs(time_response).max(),vmin=-np.abs(time_response).max(),  aspect='auto', cmap='seismic')
plt.title('Initial')
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.show()

#%%

plt.plot(pick[:,1], 'b', label='Simulated')
plt.plot(pick_curved[:, 1], 'r--', label='Objective')
plt.ylim(60, 110)
plt.xlabel('Traces')
plt.ylabel('Samples')
plt.legend()
plt.show()

#%%
filename = OUTPUT_DIR + 'synthetic_case_simulation_plane (copy).h5'
solv = FracEM()
solv.load_hdf5(filename)

###################### Borehole cone
from bedretto.core.experiments import GPRtools

gpr = GPRtools()
GPR_cone_curved, int_depth_curved, int_radius_curved = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
                                                   # radius=first_arrival_distance/ 2,#Radial distance
                                                   radius=((pick_sample /solv.file_read_attribute('source/dtime'))*v),#Radial distance
                                                   # radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
                                                   # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
                                                   trajectory =bor_df,
                                                   depth_resolution=0.1,
                                                   radial_resolution=0.1
                                                   )
GPR_cone_plane, int_depth_plane, int_radius_plane = gpr.get_gpr_cone(depth=bor_df['Depth (m)'].to_numpy()[1:-1],
                                                   radius=pick_syn[1:-1,1] * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
                                                   # radius=picked * solv.file_read_attribute('source/dtime') * v / 2,#Radial distance
                                                   trajectory =bor_df,
                                                   depth_resolution=0.1,
                                                   radial_resolution=0.1
                                                   )
p.add_object(pv.PolyData(GPR_cone_curved), color='black', name='curved_cone', opacity=0.3)
p.add_object(pv.PolyData(GPR_cone_plane), color='red', name='plane_cone', opacity=0.3)

#%%
from gdp.processing.image_processing import pick_first_arrival
threshold = 0.001
import matplotlib.pyplot as plt
# vmax = np.abs(time_response).max(axis=1)
# tv_syn = time_response_curve / vmax[:,None]
# # vmax = vmax
# vmax = 1
time_response = solv.file_read('dummy_solution/time_response')
time_vector = solv.file_read('dummy_solution/time_vector')
picked = solv.file_read('dummy_solution/first_arrival') * solv.file_read_attribute('source/dtime')

tv_syn = time_response

plt.imshow(tv_syn.T, aspect='auto', cmap='Greys', #vmin=-vmax, vmax=vmax,
           interpolation='bicubic',)
           # extent = (0, tv_syn.shape[0], solv_curve.time_window, 0))

pick_syn = pick_first_arrival(tv_syn.T,threshold=threshold)
plt.plot(pick_syn[:,0], pick_syn[:,1], 'g-', label='Curved')
plt.plot(picked, 'r-', label='calculated')
plt.plot(pick_sample, 'b', label='pick_smple')
plt.xlabel('Borehole depth (m)')
plt.title('Curved fault')
plt.colorbar()
plt.show()