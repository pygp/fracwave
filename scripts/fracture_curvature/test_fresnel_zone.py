import numpy as np
from fracwave import FractureGeo, OUTPUT_DIR, Antenna, SourceEM, FracEM
from bedretto import model
import pyvista as pv
import matplotlib.pyplot as plt

file=OUTPUT_DIR + 'fracture_curvature/fresnel_zone.h5'
p = model.ModelPlot()
#%%
try:
    sou = SourceEM()
    sou.load_hdf5(file)
except:
    sou = SourceEM()
    sou.type='ricker'
    sou.set_center_frequency(0.1)
    sou.set_time_vector(np.linspace(0, 500, 401))
    sou.create_source()
    sou.export_to_hdf5(file, overwrite=True)
sou.plot_waveforms_complete().show()
#%%
try:
    frac = FractureGeo()
    frac.load_hdf5(file)
except:
    frac0 = FractureGeo()
    frac0.load_hdf5(OUTPUT_DIR + 'fracture_curvature/curve_fracture.h5')

    df = frac0.fractures
    name_fracture = 'Frac2'
    dff = df.loc[df['name'] == name_fracture]
    surf = frac0.get_surface(name_fracture)

    frac = FractureGeo()
    vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(surf)
    frac.set_fracture(name_fracture='Frac2',
                      vertices=vertices,
                      faces=faces,
                      aperture=0.001,
                      electrical_conductivity=0.01,
                      electrical_permeability=81,
                      overwrite=True,
                      plane=0)
    frac.export_to_hdf5(file, overwrite=True)

normal = np.vstack(frac.fractures['nz'].to_numpy())
midpoints = frac.fractures[['x', 'y', 'z']].to_numpy()
surf = frac.get_surface()
p.add_object(surf, color='yellow', name='frac', opacity=0.5, show_edges=True)

#%%
try:
    ant = Antenna()
    ant.load_hdf5(file)
except:
    ant = Antenna()
    rx = np.asarray([[0, -10, 0],
                     [20, 10, 0]])
    tx = np.asarray([[0, 10, 0],
                     [20, -10, 0]])
    orientation = np.asarray([[0.,0., 1.]])
    orientation /= np.linalg.norm(orientation, axis=-1,keepdims=True)

    orientation = np.repeat(orientation, len(rx), axis=0)
    ant.set_profile(name_profile='test',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=orientation,
                    orient_transmitters=orientation,)
    ant.export_to_hdf5(file, overwrite=True)
rx =ant.Receiver
tx = ant.Transmitter
orientation = ant.orient_Receiver

#%%

p.add_object(tx, color='red', name='tx')
p.add_object(rx, color='blue', name='rx')

min_distance, min_position, surface = frac.get_position_reflection_point(midpoints,
                                                                         tx,
                                                                         rx)
distance_to_element = np.linalg.norm(min_position - tx, axis=-1)
p.add_object(min_position, color='green', name='min_point')


#%%
try:
    solv = FracEM()
    solv.load_hdf5(file)
except:
    solv = FracEM()
    solv.open_file(file)
    solv.rock_epsilon = 5.9
    solv.rock_sigma = 0.0
    # solv.backend = 'torch'
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'
    # _ = solv.time_zero_correction()
    # solv.solve_for_profile = 'MB1'
    solv._fast_calculation_incoming_field = False

    solv.mode = 'incoming_field'
    field = solv.forward_pass()
field = solv.file_read('simulation/fracture_field')
field /=field.max()#axis=-1, keepdims=True)

field_abs = field[0]
# field_abs[field[1] > field_abs] = field[1][field[1] > field_abs]
surf['fracture_field']= field_abs

p.add_object(surf, scalars='fracture_field', cmap='viridis', name='frac',
             opacity=0.8, show_edges=False, clim=(0.3,1))

#%%
# direction_vector_rx = midpoints - rx
# direction_vector_rx /= np.linalg.norm(direction_vector_rx, axis=-1)[..., None]
# direction_vector_tx = midpoints - tx
# direction_vector_tx /= np.linalg.norm(direction_vector_tx, axis=-1)[..., None]
#
# orthogonal_tx = 1 - np.abs(np.einsum('ei, ti -> e', direction_vector_tx, orientation))
#
# #%% normal of the plane with the direction_vector
#
# reflection_strength_tx = np.abs(np.einsum('ei, ei -> e', normal, direction_vector_tx))
# reflection_strength_rx = np.abs(np.einsum('ei, ei -> e', normal, direction_vector_rx))

#%% Fresnel zone
from fracwave.geometry.vector_calculus import create_fresnel_cone_of_points, find_points_near_surface
f = sou.center_frequency # Hz
v = solv.velocity
wavelength = v / f
n_zone = 10
cones, distances = create_fresnel_cone_of_points(center=tx,
                                                 orientation=orientation,
                                                 distance=distance_to_element,
                                                 wavelength=wavelength,
                                                 n_zone=n_zone)

intersection, mask = find_points_near_surface(midpoints, cones, minimum_distance=wavelength*0.25)
distances_geometry = [np.linalg.norm(intersection[i] - tx[i], axis=-1) for i in range(len(cones))]

min_distances = [i.min() for i in distances_geometry]

cones, distances = create_fresnel_cone_of_points(center=tx,
                                                 orientation=orientation,
                                                 distance=min_distances,
                                                 wavelength=wavelength,
                                                 n_zone=n_zone)

intersection, mask = find_points_near_surface(midpoints, cones, minimum_distance=wavelength*0.25)
distances_geometry = [np.linalg.norm(intersection[i] - tx[i], axis=-1) for i in range(len(cones))]

# [p.add_object(cones[i], scalars=distances[i], name=f'fresnel_{i}', opacity=0.2, cmap='hot') for i in range(len(cones))]
[p.add_object(intersection[i], name=f'fresnel_{i}', scalars=distances_geometry[i], opacity=0.2, cmap='hot') for i in range(len(cones))]

#%%
hull_points = convex_hull_from_pointcloud(intersection)



#%%


points_hull = midpoints[mask]

plane = frac.get_projection_plane(midpoints[mask])
plane=0
if plane == 0: #yz
    scalar = points_hull[:,0]
    xy = points_hull[:,(1,2)]
elif plane == 1: #xz
    scalar = points_hull[:,1]
    xy = points_hull[:,(0,2)]
elif plane == 2: #xy
    scalar = points_hull[:,2]
    xy = points_hull[:,(0,1)]
else:
    raise ValueError('plane must be 0, 1 or 2')
cv = ConvexHull(xy)
hull_points = cv.vertices


hull_points = points_hull[cv.vertices]
hull_points = np.vstack((hull_points, hull_points[0]))
#%%

p.add_object(hull_points,  name='line', color='black')

#%%
po = pv.MultipleLines(hull_points)
p.add_object(po,  name='line', color='black')



# %%
