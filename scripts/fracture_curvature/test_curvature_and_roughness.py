from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR
from bedretto import model
import numpy as np
import matplotlib.pyplot as plt
import pyvista as pv

file = OUTPUT_DIR + 'manuscript1/roughness_curvature.h5'

sou = SourceEM()
sou.frequency = np.linspace(0, 0.2, 300)
firstp = sou.create_source()
sou.plot(); plt.show()
sou.export_to_hdf5(file, overwrite=True)

v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / sou.peak_frequency  # m
max_element_size = wavelength / 6

ant = Antenna()
separation = 0
traces = 10
mid = np.linspace(-1.5, 1.5, traces)

tx = np.zeros((traces, 3))
rx = np.zeros((traces, 3))
tx[:,-1] = mid - separation * 0.5
tx[:,1] = -4
rx[:,-1] = mid + separation * 0.5
rx[:,1] = -4

orient = np.zeros((traces, 3))
orient[:,-1] = 1
ant.set_profile(name_profile='prof1',
                receivers=rx,
                transmitters=tx,
                orient_receivers=orient,
                orient_transmitters=orient
                )

ant.export_to_hdf5(file, overwrite=True)

#%%
p = model.ModelPlot()

p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')

Tx_arrows = pv.PolyData()
Rx_arrows = pv.PolyData()
for i in range(len(ant.Transmitter)):
    Tx_arrows += pv.Arrow(start=ant.Transmitter[i], direction=ant.orient_Transmitter[i], scale=0.2)
    Rx_arrows += pv.Arrow(start=ant.Receiver[i], direction=ant.orient_Receiver[i], scale=0.2)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')

#%%
geom = FractureGeo()
import gstools as gs

x = z = np.linspace(-2, 2, 100)
xx, zz = np.meshgrid(x,z)
vertices = np.c_[xx.ravel(),np.zeros(len(x)**2), zz.ravel(),]

grid, vertices, faces = geom.remesh(vertices, plane=1, max_element_size=max_element_size)
df = geom.set_fracture(name_fracture='Frac1', vertices=vertices, faces=faces,
                         aperture=0.005,
                         electrical_conductivity=0,
                       electrical_permeability=81,
                       overwrite=True)

model = gs.Gaussian(dim=2, var=0.05, len_scale=0.5)
srf = gs.SRF(model, seed=1234)
srf((x, z), mesh_type='structured')
srf.plot()

geom = FractureGeo()
vertices[:,1] = vertices[:,1] + srf.field.ravel()
df = geom.set_fracture(name_fracture='Frac1',
                       vertices=vertices,
                       faces=faces,
                         aperture=0.005,
                         electrical_conductivity=0,
                       electrical_permeability=81,
                       overwrite=True)


apertures = geom.generate_heterogeneous_apertures(geom.midpoints,
                                                  b_mean=0.3,
                                                  seeds=(1234, 4321),
                                                  var=0.01,
                                                  len_scale=1,
                                                  plane=1)
geom._fractures['aperture'] = apertures
geom.export_to_hdf5(file, overwrite=True)
#%%
surf = geom.surface
p.add_object(surf, name='mesh', color='yellow')#, opacity=0.8)
p.add_object(surf, name='mesh', scalars='aperture')#, opacity=0.8)

#%%
solv = FracEM()

solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
# solv.engine = 'dummy'
# solv.units = 'SI'
solv.backend = 'torch'
# solv.backend = 'numpy'
solv._filter_energy = 0.01
solv.open_file(file)
# solv.load_hdf5(OUTPUT_DIR + 'full_model/full_model_variable.h5')
freq_t0 = solv.time_zero_correction()

#%%
freq = solv.forward_pass()
#%%
time_response, time_vector = solv.get_ifft(freq)#, pad=1000)
ax = solv.plot(time_response, time_vector, (-1.5,1.5))
