from fracwave import FracEM, FractureGeo, OUTPUT_DIR, Antenna, SourceEM
from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
                                                   set_fracture_solver,
                                                   forward_operator,
                                                   run_inversion)
from bedretto.core.vector_helpers import get_normal_vector_from_azimuth_and_dip, get_azimuth_and_dip_from_normal_vector
from bedretto import model

import pandas as pd
import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
odir = OUTPUT_DIR + 'fracture_curvature/synthetic_inversion_2boreholes/'
file_initial = odir + 'inital_plane_inversion.h5'
file_real = odir + 'real_synthetic_inversion.h5'


window_size = (1900,2000)

cam  = [(-18.907178733372504, -22.034166956110397, 3.5844890381604393),
 (-1.0264334207419537, 1.6118521444655671, -2.223608177583993),
 (0.08587692647219669, 0.17593490386009694, 0.9806487970233928)]

show_kwargs = dict(xtitle_offset=0.02,
                   ztitle_offset=0.02,
                   ytitle_offset=0.1,
                   xtitle='X (m)',
                   ytitle= 'Y (m)',
                   ztitle='Z (m)',
                   xrange=(-6,2),
                   yrange=(-5,6),
                   zrange=(-6,5),
                   number_of_divisions=10)

#%%
frac = FractureGeo()
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)
traces = 50
borehole = np.zeros((traces,3))
borehole[:,0] = -6
borehole[:,-1] = np.linspace(-4, 4, traces)

orient = np.zeros(((traces,3)))
orient[:,-1] = 1

resolution = (50, 50, 50)
frac.init_geo_model(gempy_extent, resolution)
solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
solv.backend = 'numpy'
solv.engine='dummy'

set_fracture_geometry(frac)
set_fracture_solver(solv)
#%%
ant = Antenna()
ant.set_profile('vertical', receivers=borehole, transmitters=borehole, orient_receivers=orient, orient_transmitters=orient)
ant.export_to_hdf5(file_initial, overwrite=True)
ant.export_to_hdf5(file_real, overwrite=True)

sou = SourceEM()
samples=300
sou.frequency = np.linspace(0, 0.3, samples)

firstp = sou.create_source() #a=1,
sou.export_to_hdf5(file_initial, overwrite=True)
sou.export_to_hdf5(file_real, overwrite=True)
#%%
# v = 0.1234  # m/ns -> Velocity in granite
# wavelength = v / sou.peak_frequency # m
# max_element_size = wavelength/5
max_element_size=0.1

p1 = (70, 270, 0)

# fixed = np.asarray([(0,0,p1[-1])])
fixed = np.asarray([(-0.07264459,  0.30187218,  0.04356908)])
fixed_or = [fixed.copy(), np.asarray([(p1[1],p1[0],1)])]
normal = get_normal_vector_from_azimuth_and_dip(azimuth=p1[1], dip=p1[0])

def plane_z_value(center, normal, xy_coord):
    x, y = xy_coord
    x0, y0, z0 = center
    a, b, c = normal
    d = -(a * x0 + b * y0 + c * z0)
    z = -(a * x + b * y + d) / c
    return z

z = plane_z_value(fixed[0], normal, [-1,0])
planar = np.asarray([(-1, 0, z)])
df_plane = pd.DataFrame(planar, columns=['X', 'Y', 'Z'])
df_fixed = pd.DataFrame(fixed, columns=['X', 'Y', 'Z'])
df_orientations = pd.DataFrame(np.hstack((fixed,[normal])), columns=['X','Y','Z','G_x','G_y','G_z'])

# curved = np.asarray([[0.89864941,  0.12210877, -3.37883444],
#                                      [0.93922058,  0.27997581, -2.02387851],
#                                     #[-0.07264459,  0.30187218,  0.04356908],
#                                      #[0.02385303, -0.14218273,  0.96832576]])
#                                      [0.02385303, 0,  0.96832576]])

curved = np.asarray([[-0.85,  0, -3.4],
                     [-0.92,  0, -2.1],
                     [0.2, 0, 1.5],
                     [0, 0,  3],

                     [-0.5, 3, 0],
                     [1, -3, 0],
                     ])


df_curve = pd.DataFrame(curved, columns=['X','Y','Z'])

kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductivity=0.001,
                                  electrical_permeability=81)

#%%
vp, fp = frac.create_gempy_geometry(fixed_points=df_fixed,
                                   fixed_orientations=df_orientations,
                                   move_points=df_plane
                                   )
grid_plane, vertices_plane, faces_plane = frac.remesh(vp, max_element_size=max_element_size, extrapolate=False, plane=0)
# grid_plane, vertices_plane, faces_plane = frac.remesh_automatic(vp, max_element_size=.1, extrapolate=False)
vc, fc = frac.create_gempy_geometry(fixed_points=df_fixed,
                                   fixed_orientations=df_orientations,
                                   move_points=df_curve
                                   )
grid_curve, vertices_curve, faces_curve = frac.remesh(vc, max_element_size=max_element_size, extrapolate=False, plane=0)
# grid_curve, vertices_curve, faces_curve = frac.remesh_automatic(vc, max_element_size=1, extrapolate=True)

frac1 = FractureGeo()
frac1.set_fracture(name_fracture='plane',
                   vertices=vertices_plane,
                   faces=faces_plane,
                   **kwargs_fracture_properties)
frac1.export_to_hdf5(file_initial, overwrite=True)

frac2 = FractureGeo()
frac2.set_fracture(name_fracture='curved',
                  vertices=vertices_curve,
                  faces=faces_plane,
                  **kwargs_fracture_properties)
frac2.export_to_hdf5(file_real, overwrite=True)
#%% Generate the first figure. Compare gempy block model and curved initial mesh
import gempy as gp
vc, fc = frac.create_gempy_geometry(fixed_points=df_fixed,
                                   fixed_orientations=df_orientations,
                                   move_points=df_curve
                                   )
plotter = gp.plot_3d(frac.geo_model, plotter_type='background', show_surfaces=False,)
plotter.plot_structured_grid(opacity=0.5)
plotter.p.remove_bounds_axes()

actors = plotter.p.renderer._actors
p2 = model.ModelPlot()
[p2.plotter.add_actor(a) for k, a in actors.items()]
p2.show(**show_kwargs)
p2.show(yzgrid2=True, xrange=(-2, 3))
p2.plotter.add_axes(box=True, viewport=(0.1, 0.05, 0.3, 0.3))
p2.plotter.window_size = window_size#(1750, 1900)
p2.plotter.camera_position = [(-15.455137758037772, -20.148353549143433, 13.085369680465325),
 (0.5473828427661188, 0.8974323933810469, -1.5291263419702086),
 (0.26420340990556196, 0.40646584710680206, 0.8746325361716332)]
p2.snapshot('../curved_geometry.pdf', save_dir=odir)

#%% Mesh representation
faces = np.hstack([np.hstack([len(f), f]) for f in fc])
surf =pv.PolyData(vc, faces)

p3 = model.ModelPlot()
p3.add_object(surf, name='Mesh', color='blue', style='wireframe')

p3.add_object(surf, name='Mesh', color='blue', opacity=0.5, show_edges=True)
projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p3.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p3.add_object(projected2, name='MeshProjected2', opacity=0.2)
projected3 = surf.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1,0, 0))
p3.add_object(projected3, name='MeshProjected3', opacity=0.2)
p3.show(**show_kwargs)
p3.show(yzgrid2=True, xrange=(-2, gempy_extent[1] + 1))
p3.plotter.add_axes(box=True)#, viewport=(0.1, 0.05, 0.3, 0.3))

# Add projected control points to planes
po = pv.PolyData(np.vstack((df_fixed.to_numpy(), df_curve.to_numpy())))
p3.add_object(po, name='Points', point_size=20, render_points_as_spheres=True, color='red')
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p3.add_object(projectedxy, name='PointProjected', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p3.add_object(projectedz, name='PointProjected2', point_size=10)
projectedy = po.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1, 0, 0))
p3.add_object(projectedy, name='PointProjected3', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction=normal)
p3.add_object(arrow, name='arrow', color='blue')

p3.plotter.window_size = (1700, 2000)#(1750, 1900)
p3.plotter.camera_position = [(-14.669616901789219, -20.779014374946655, 13.03730513847674),
 (1.3329036990146672, 0.2667715675778106, -1.5771908839588005),
 (0.22249978799983883, 0.4362622551670062, 0.871876762539652)]
p3.snapshot('../wire_mesh_irregular_curved.pdf', save_dir=odir)

#%% Axes plots
plt.scatter(vc[:,0], vc[:,1], c=vc[:,2], cmap='gist_earth', s=10)
plt.xlabel('X (m)')
plt.ylabel('Y (m)')
plt.title('View from axis = 2 (Top view)')
cbar = plt.colorbar()
cbar.set_label('Z (m)')
# plt.gca().set_aspect('equal', adjustable='box')
plt.tight_layout()
plt.savefig(odir+'../axis2_view_curved.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vc[:,0], vc[:,2], c=vc[:,1], cmap='gist_earth', s=10)
plt.xlabel('X (m)')
plt.ylabel('Z (m)')
plt.title('View from axis = 1 (North view)')
cbar = plt.colorbar()
cbar.set_label('Y (m)')
plt.tight_layout()
plt.savefig(odir+'../axis1_view_curved.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vc[:,1], vc[:,2], c=vc[:,0], cmap='gist_earth', s=10)
plt.xlabel('Y (m)')
plt.ylabel('Z (m)')
plt.title('View from axis = 0 (East view)')
cbar = plt.colorbar()
cbar.set_label('X (m)')
plt.tight_layout()
plt.savefig(odir+'../axis0_view_curved.pdf', transparent=True)
plt.show()

#%%
surf =frac2.get_surface()

p4 = model.ModelPlot()
p4.add_object(surf, name='Mesh', color='blue', style='wireframe')

p4.add_object(surf, name='Mesh', color='blue', opacity=0.5, show_edges=True)
projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p4.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p4.add_object(projected2, name='MeshProjected2', opacity=0.2)
projected3 = surf.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1,0, 0))
p4.add_object(projected3, name='MeshProjected3', opacity=0.2)
p4.show(**show_kwargs)
p4.show(yzgrid2=True, xrange=(-2, gempy_extent[1] + 1))
p4.plotter.add_axes(box=True)#, viewport=(0.1, 0.05, 0.3, 0.3))


# Add projected control points to planes
po = pv.PolyData(np.vstack((df_fixed.to_numpy(), df_curve.to_numpy())))
p4.add_object(po, name='Points', point_size=20, render_points_as_spheres=True, color='red')
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p4.add_object(projectedxy, name='PointProjected', point_size=10)
p4ojectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p4.add_object(projectedz, name='PointProjected2', point_size=10)
projectedy = po.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1, 0, 0))
p4.add_object(projectedy, name='PointProjected3', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction=normal)
p4.add_object(arrow, name='arrow', color='blue')

p4.plotter.window_size = (1700, 2000)#(1750, 1900)
p4.plotter.camera_position = [(-14.669616901789219, -20.779014374946655, 13.03730513847674),
 (1.3329036990146672, 0.2667715675778106, -1.5771908839588005),
 (0.22249978799983883, 0.4362622551670062, 0.871876762539652)]
p4.snapshot('../wire_mesh_regular_curved.pdf', save_dir=odir)

#%% Generate plots
for fr, df, c, fn in zip([frac1, frac2], [df_plane, df_curve], ['red', 'green'],['initial_synthetic_mesh.pdf', 'real_synthetic_mesh.pdf']):
    p = model.ModelPlot()
    p.add_object(ant.Transmitter, name='Tx', color='Red')
    # p.add_object(ant.Receiver, name='Rx', color='blue')

    # if 'initial' in fn: continue
    surf = fr.get_surface()
    p.add_object(surf, name='Mesh', color=c, opacity=0.5)
    projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
    p.add_object(projected, name='MeshProjected', opacity=0.2)
    projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
    p.add_object(projected2, name='MeshProjected2', opacity=0.2)

    if not 'initial' in fn:
        p.add_label(points=df.to_numpy(), labels=[str(k) for k, p in df.iterrows()], name='cp', point_size=10, point_color='black',font_size=40)
        # Add projected control points to planes
        po = pv.PolyData(df.to_numpy())
        projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
        p.add_object(projectedxy, name='PointProjected', point_size=10)
        projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
        p.add_object(projectedz, name='PointProjected2', point_size=10)

    p.add_label(points=df_fixed.to_numpy(), labels=['Fixed'], name='cp_fixed', point_size=10, point_color='black', font_size=40, always_visible=True)
    po = pv.PolyData(df_fixed.to_numpy())
    projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projectedxy, name='PointProjected3', point_size=10)
    projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projectedz, name='PointProjected4', point_size=10)

    arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
    p.add_object(arrow, name='arrow', color='blue')

    p.plotter.window_size = window_size
    p.plotter.camera_position = cam
    p.show(**show_kwargs)
    p.plotter.add_axes(box=True)
    p.snapshot(fn, save_dir=odir)

#%% What are their travel times:

solv.engine = 'dummy'
solv.backend = 'numpy'
solv.open_file(file_initial)
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
pick_plane = solv.file_read('dummy_solution/first_arrival')

solv.open_file(file_real)
freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
pick_curve = solv.file_read('dummy_solution/first_arrival')

#%% Plot curves
fig, ax = plt.subplots(figsize=(4,8))
distance_plane = pick_plane * solv.velocity / 2
distance_curve = pick_curve * solv.velocity / 2

distance_plot = (4,7.5)

ax.plot(distance_plane, borehole[:,-1], color='red', label='Initial')
ax.plot(distance_curve, borehole[:,-1], color='green', label='True')
ax.set_xlim(distance_plot[0], distance_plot[1])
ax.set_ylabel('Borehole Depth (m)')
ax.set_xlabel('Radial distance (m)')
start, end = ax.get_xlim()
ax.xaxis.set_ticks(np.arange(start, end+1, 1))
start, end = ax.get_ylim()
ax.yaxis.set_ticks(np.arange(-5, 6, 1))
ax.set_ylim(-4.5, 4.5)
ax.set_xlim(4, 7)

ax2 = ax.twiny()
ax2.set_xlabel('Time (ns)')
start, end = distance_plot[0]*2/solv.velocity, distance_plot[1]*2/solv.velocity
ax2.set_xlim(start, end)
ax2.xaxis.set_ticks(np.arange(4, 8, 1)*2/solv.velocity )

ax.tick_params(axis='both', which='both', direction='out', length=5)
ax2.tick_params(axis='both', which='both', direction='out', length=5)

ax.grid()
legend = ax.legend(shadow=True, frameon=True)
plt.tight_layout()
plt.show()

fig.savefig(odir + 'travel_time_synthetic.pdf' )
#%% Run inversion
kwargs_forward_solver = dict(plane=0,
                             order=2,
                             max_element_size=max_element_size)

kwargs_cost_function = dict(tol_all=0.5,  # Tolerance for overall RMS error over the whole picks
                            tol=0.25,  # Tolerance for a single trace
                            surrounding=2,
                            step_speed=1,
                            max_iter_all=20,
                            max_iter_pos=15)

fixed_points_geometry, solv = run_inversion(pick_true=pick_curve,
                                            fixed_points_geometry=df_fixed,
                                            fixed_oientations_geometry=df_orientations,
                                            constrained_points_geometry=df_plane,
                                            filename=file_initial,
                                            plot_step=True,
                                            kwargs_fracture_properties=kwargs_fracture_properties,
                                            kwargs_forward_solver=kwargs_forward_solver,
                                            kwargs_cost_function=kwargs_cost_function)
#%%
# vf, _ = frac.create_gempy_geometry(fixed_points=fixed_points_geometry,
#                                    fixed_orientations=df_orientations,
#                                    )
# grid, vertices, faces = frac.remesh(vf, max_element_size=max_element_size, extrapolate=False, plane=0)
#
# p.add_object(grid, name='result', color='blue', opacity=0.5)
#
# p.add_label(points=fixed_points_geometry.to_numpy(), labels=[str(k) for k, p in fixed_points_geometry.reset_index().iterrows()], name='cp2', point_size=10,
#             point_color='red', font_size=30)

#%%
rejected = pd.DataFrame([[-0.9374584,  0.,        -4.606747 ]], columns=('X','Y', 'Z'))
results_of_inversion = {'pos_49_iter_3': fixed_points_geometry[:2][['X', 'Y', 'Z']].reset_index(drop=True),
                        'pos_0_iter_1': pd.concat((fixed_points_geometry[:2], rejected))[['X', 'Y', 'Z']].reset_index(drop=True),
                        'pos_17_iter_1': pd.concat((fixed_points_geometry[:2], rejected, pd.DataFrame(fixed_points_geometry.iloc[2]).T))[['X', 'Y', 'Z']].reset_index(drop=True),
                        'again_pos_0_iter_1': pd.concat((fixed_points_geometry[:2], rejected, fixed_points_geometry.iloc[2:4]))[['X', 'Y', 'Z']].reset_index(drop=True).drop(2), #'pos_3_iter_10',
                        }

p = model.ModelPlot()
p.add_object(ant.Transmitter, name='Tx', color='Red')
# p.add_object(ant.Receiver, name='Rx', color='blue')
prev = frac1.vertices

for i, (n, fix) in enumerate(results_of_inversion.items()):
    import re
    match = re.search(r'pos_(\d+)_iter_(\d+)', n)
    if match:
        pos = int(match.group(1))
        iter = int(match.group(2))
    else:
        print("No match found.")
    p.remove_scalar_bar()
    filename = file_initial.split('.')[0] + '_' + n +'.h5'
    fr = FractureGeo()
    fr.load_hdf5(filename)
    # if 'initial' in fn: continue
    surf = fr.get_surface()
    distt = np.linalg.norm(prev - fr.vertices, axis=1)
    # p.add_object(surf, name='Mesh', color='blue', opacity=0.5)
    surf['Distance (m)'] = distt
    p.add_object(surf, name='Mesh', scalars='Distance (m)', clim=(0,distt.max()), cmap='viridis', show_scalar_bar=False)
    p.plotter.add_scalar_bar(title='Distance (m)\n', title_font_size=40, label_font_size=40, vertical=True,
                             height=0.6, position_x=0.88, position_y=0.3)
    projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
    p.add_object(projected, name='MeshProjected', opacity=0.2)
    projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
    p.add_object(projected2, name='MeshProjected2', opacity=0.2)

    # fix = fixed_points_geometry.reset_index(drop=True)[:i+2]

    p.add_label(points=fix.to_numpy(), labels=[str(k) for k, p in fix.iterrows()], name='cp', point_size=10, point_color='black',font_size=45, always_visible=True)
    # Add projected control points to planes
    po = pv.PolyData(fix.to_numpy())
    projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projectedxy, name='PointProjected', point_size=10)
    projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
    p.add_object(projectedz, name='PointProjected2', point_size=10)

    p.add_label(points=df_fixed.to_numpy(), labels=['Fixed'], name='cp_fixed', point_size=10, point_color='black', font_size=45, always_visible=True)
    po = pv.PolyData(df_fixed.to_numpy())
    projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projectedxy, name='PointProjected3', point_size=10)
    projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projectedz, name='PointProjected4', point_size=10)

    arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
    p.add_object(arrow, name='arrow', color='blue')

    p.plotter.window_size = window_size
    p.plotter.camera_position = cam
    p.show(**show_kwargs)
    p.plotter.add_axes(box=True)

    solv.open_file(filename)
    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    pick_current = solv.file_read('dummy_solution/first_arrival')

    fig, ax = plt.subplots(figsize=(4, 8))
    distance_plane = pick_plane * solv.velocity / 2
    distance_curve = pick_curve * solv.velocity / 2
    distance_current = pick_current * solv.velocity / 2

    distance_plot = (4, 7.5)

    ax.plot(distance_plane, borehole[:, -1], color='red', label='Initial', linestyle='dotted')
    ax.plot(distance_curve, borehole[:, -1], color='green', label='True', linestyle='dotted')
    ax.plot(distance_current, borehole[:, -1], color='blue', label='Update')

    ax.set_xlim(distance_plot[0], distance_plot[1])
    ax.set_ylabel('Borehole Depth (m)')
    ax.set_xlabel('Radial distance (m)')
    start, end = ax.get_xlim()
    ax.xaxis.set_ticks(np.arange(start, end + 1, 1))
    start, end = ax.get_ylim()
    ax.yaxis.set_ticks(np.arange(-5, 6, 1))
    ax.set_ylim(-4.5, 4.5)
    ax.set_xlim(4, 7)

    ax2 = ax.twiny()
    ax2.set_xlabel('Time (ns)')
    start, end = distance_plot[0] * 2 / solv.velocity, distance_plot[1] * 2 / solv.velocity
    ax2.set_xlim(start, end)
    ax2.xaxis.set_ticks(np.arange(4, 8, 1) * 2 / solv.velocity)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax2.tick_params(axis='both', which='both', direction='out', length=5)

    ax.axhline(y=borehole[pos, -1], color='orange', linestyle='--', label='Position')

    ax.grid()
    legend = ax.legend(shadow=True, frameon=True, bbox_to_anchor=(1.05, 0.4),
                       bbox_transform=ax.transAxes, fancybox=True)
    legend = ax.legend(shadow=True, frameon=True, fancybox=True)
    legend.set_zorder(100)
    plt.tight_layout()
    plt.show()

    p.add_object(pv.Sphere(radius=0.3, center=borehole[pos]), name='position', color='red')

    p.snapshot(f'iter_{i}.pdf', save_dir=odir)
    fig.savefig(odir + f'iter_{i}_plot.pdf')

    prev = fr.vertices

#%% Make a comparison between final plot and initial plot
p = model.ModelPlot()
p.add_object(ant.Transmitter, name='Tx', color='Red')
# p.add_object(ant.Receiver, name='Rx', color='blue')
fix = results_of_inversion[n]

p.remove_scalar_bar()
filename = file_initial.split('.')[0] + '_' + n +'.h5'
fr = FractureGeo()
fr.load_hdf5(filename)
    # if 'initial' in fn: continue
surf = fr.get_surface()
distt = np.linalg.norm(frac2.vertices - fr.vertices, axis=1)
# p.add_object(surf, name='Mesh', color='blue', opacity=0.5)
surf['Distance (m)'] = distt
p.add_object(surf, name='Mesh', scalars='Distance (m)', clim=(0,distt.max()), cmap='viridis', show_scalar_bar=False)
p.plotter.add_scalar_bar(title='Distance (m)\n', title_font_size=40, label_font_size=40, vertical=True,
                         height=0.6, position_x=0.88, position_y=0.3)
projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p.add_object(projected2, name='MeshProjected2', opacity=0.2)

# fix = fixed_points_geometry.reset_index(drop=True)[:i+2]

p.add_label(points=fix.to_numpy(), labels=[str(k) for k, p in fix.iterrows()], name='cp', point_size=10, point_color='black',font_size=45)
# Add projected control points to planes
po = pv.PolyData(fix.to_numpy())
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p.add_object(projectedxy, name='PointProjected', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p.add_object(projectedz, name='PointProjected2', point_size=10)

p.add_label(points=df_fixed.to_numpy(), labels=['Fixed'], name='cp_fixed', point_size=10, point_color='black', font_size=45, always_visible=True)
po = pv.PolyData(df_fixed.to_numpy())
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p.add_object(projectedxy, name='PointProjected3', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p.add_object(projectedz, name='PointProjected4', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
p.add_object(arrow, name='arrow', color='blue')


# Add the inital control points

p.add_label(points=df_curve.to_numpy(), labels=[str(k)+' orig' for k, p in df.iterrows()], name='cpOrig', point_size=10,
            point_color='red', font_size=40)
# Add projected control points to planes
po = pv.PolyData(df_curve.to_numpy())
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p.add_object(projectedxy, name='PointProjectedOrig', point_size=10, color='red')
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p.add_object(projectedz, name='PointProjectedOrig2', point_size=10, color='red')

p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(**show_kwargs)
p.plotter.add_axes(box=True)

#%% Now add a new borehole
p1 = (-6, 3, 4)
p2 = (-1, -3, -4)



