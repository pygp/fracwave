from bedretto import data, geometry, DATA_DIR, model
import pandas as pd
import os
import numpy as np
from fracwave import OUTPUT_DIR, FractureGeo
import pyvista as pv
import matplotlib.pyplot as plt


file = OUTPUT_DIR + 'fracture_curvature/fracture_mesh.h5'

cam  = [(-44.60124149157894, -90.65576797613528, 25.65369292174598),
 (7.035092859780924, -2.257296465430356, -5.431195831622366),
 (0.10932170089281376, 0.2723073690752898, 0.9559798441709929)]

window_size = (1400, 1900)

#%%
points1 = np.asarray([[10, -10, -10],
                      [10, 0, -10],
                      [10, 10, -10],  # Lower part

                      [10, -10, 0],
                      [10, 0, 0],
                      [10, 10, 0],  # middle part

                      [10, -10, 10],
                      [10, 0, 10],
                      [10, 10, 10], # upper part
                     ])
df_points1 = pd.DataFrame.from_dict({'X': points1[:,0],
                                     'Y': points1[:,1],
                                     'Z': points1[:,2],
                                     })
df_points1['smooth']=0.001
df_points1['surface'] = 'F'

points2 = np.asarray([[7, -10, -10],
                      [9, 0, -10],
                      [10, 10, -10],  # Lower part

                      [11, -10, 0],
                      [10, 0, 0],
                      [8, 10, 0],  # middle part

                      [11, -10, 10],
                      [10, 0, 10],
                      [13, 10, 10],  # upper part
                      ])
df_points2 = pd.DataFrame.from_dict({'X': points2[:,0],
                                     'Y': points2[:,1],
                                     'Z': points2[:,2],
                                     })
df_points2['smooth'] = 0.001
df_points2['surface'] = 'F'

orientations = pd.DataFrame.from_dict({'X':[10],
                                       'Y':[0],
                                       'Z':[0],
                                       'G_x':[-1],
                                       'G_y':[0],
                                       'G_z':[0],
                                       'smooth': [1000],
                                       'surface': ['F']})

#%%
extent = [0, 20, -20, 20, -20, 20]
frac = FractureGeo()
frac.init_geo_model(extent = extent, resolution=(15, 15, 15))

geo_model = frac.geo_model
#%%
vertices, faces = frac.create_gempy_geometry(fixed_points=df_points1, fixed_orientations=orientations)
#%%
import gempy as gp
plotter = gp.plot_3d(geo_model, plotter_type='background', show_surfaces=False,
                     )
plotter.plot_structured_grid(opacity=0.5)

actors = plotter.p.renderer._actors
p2 = model.ModelPlot()
[p2.plotter.add_actor(a) for k, a in actors.items()]
p2.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
p2.plotter.add_axes(box=True, viewport=(0.0, 0.9, 0.1, 1))
p2.plotter.window_size = (1750, 1900)
p2.plotter.camera_position = [(-44.728290098311845, -90.65679546451867, 25.43972552454888),
 (6.908044253048027, -2.258323953813732, -5.645163228819456),
 (0.12016454170726003, 0.2661279811978832, 0.9564185174597069)]


p2.snapshot('fracture_curvature/planar_geometry.pdf', save_dir=OUTPUT_DIR)
#%% Second geometry gempy
vertices2, faces2 = frac.create_gempy_geometry(fixed_points=df_points2, fixed_orientations=orientations)
#%%
import gempy as gp
plotter = gp.plot_3d(geo_model, plotter_type='background', show_surfaces=False,
                     )
plotter.plot_structured_grid(opacity=0.5)

actors = plotter.p.renderer._actors
p2 = model.ModelPlot()
[p2.plotter.add_actor(a) for k, a in actors.items()]
p2.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
p2.plotter.add_axes(box=True, viewport=(0.0, 0.9, 0.1, 1))
p2.plotter.window_size = window_size
p2.plotter.camera_position = [(-44.728290098311845, -90.65679546451867, 25.43972552454888),
 (6.908044253048027, -2.258323953813732, -5.645163228819456),
 (0.12016454170726003, 0.2661279811978832, 0.9564185174597069)]

p2.snapshot('fracture_curvature/curved_geometry.pdf', save_dir=OUTPUT_DIR)

#%%
frac.set_fracture('frac1', vertices=vertices, faces=faces,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf = frac.get_surface('frac1')
#%%
p = model.ModelPlot()
#%%
p.add_object(surf, name='mesh', color='blue', style='wireframe')
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.snapshot('fracture_curvature/wire_mesh_irregular_simple.pdf', save_dir=OUTPUT_DIR)


#%%
p.add_object(surf.points, name='mesh', color='blue', style='wireframe')
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.plotter.camera_position = cam
p.plotter.window_size = window_size
p.snapshot('fracture_curvature/points_mesh_irregular_simple.pdf', save_dir=OUTPUT_DIR)

#%%
grid, vert, fac = frac.remesh(vertices, plane=0, spacing=(15,15), extrapolate=False)  # Resolution 145,143
#%%
frac2 = FractureGeo()
frac2.set_fracture('frac1', vertices=vert, faces=fac,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf = frac2.get_surface('frac1')
#%%
p.add_object(surf, name='mesh', color='blue', style='wireframe')
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.snapshot('fracture_curvature/wire_mesh_regular_simple.pdf', save_dir=OUTPUT_DIR)

#%%
p.add_object(surf.points, name='mesh', color='blue', style='wireframe')
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.snapshot('fracture_curvature/points_mesh_regular_simple.pdf', save_dir=OUTPUT_DIR)


#%% Now the curved one
frac.set_fracture('frac1', vertices=vertices2, faces=faces2,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf2 = frac.get_surface('frac1')

#%%
p.add_object(surf2, name='mesh', color='blue', opacity=0.1)
p.add_object(surf2, name='meshp', color='black', style='points')
p.add_object(surf2, name='meshw', color='black', style='wireframe')

extent_plot = (0,30,-30,30,-30,20)
projected = surf2.project_points_to_plane(origin=(extent_plot[0], extent_plot[3], extent_plot[4]), normal=(0,0,1))
projected2= surf2.project_points_to_plane(origin=(extent_plot[0], extent_plot[3], extent_plot[4]), normal=(0,1,0))
projected3= surf2.project_points_to_plane(origin=(extent_plot[1], extent_plot[3], extent_plot[4]), normal=(1,0,0))
p.add_object(projected, name='MeshProjected', opacity=0.2, color='blue')
p.add_object(projected, name='MeshProjectedp', opacity=0.05, color='black', style='points')
p.add_object(projected2, name='MeshProjected1', opacity=0.2, color='blue')
p.add_object(projected2, name='MeshProjected1p', opacity=0.05, color='black', style='points')
p.add_object(projected3, name='MeshProjected2', opacity=0.2, color='blue')
p.add_object(projected3, name='MeshProjected2p', opacity=0.05, color='black', style='points')
p.show(xrange=extent_plot[0:2], yrange=extent_plot[2:4], zrange=extent_plot[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0.05, 0.05, 0.2, 0.2))
p.plotter.window_size = (1400, 1900)
p.plotter.camera_position = [(-65.65490143695106, -134.3546884183316, 37.313418717418415),
 (9.945855686874964, -4.930486279508232, -8.19796690638818),
 (0.10932170089281378, 0.27230736907528985, 0.955979844170993)]

p.snapshot('fracture_curvature/wire_mesh_irregular_curved.pdf', save_dir=OUTPUT_DIR)
#
# #%%
# from scipy.spatial import ConvexHull
#
# hull = ConvexHull(frac.vertices)
#
# axis=[(0,1), (0,2), (1,2)]
# for a in axis:
#     #points = np.c_[frac.vertices[hull.vertices, a[0]],frac.vertices[hull.vertices, a[1]]]
#     points = np.c_[frac.vertices[:,a[0]],frac.vertices[:, a[1]]]
#     hull2 = ConvexHull(points)
#     plt.plot(points[:, 0], points[:, 1], 'o')
#
#     for simplex in hull2.simplices:
#         plt.plot(points[simplex, 0], points[simplex, 1], 'k-')
#     plt.show()
#%%
p.add_object(surf2.points, name='mesh', color='blue', style='wireframe')
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.snapshot('fracture_curvature/points_mesh_irregular_curved.pdf', save_dir=OUTPUT_DIR)

#%%
grid2, vert2, fac2 = frac.remesh(vertices2, plane=0, spacing=(15,15), extrapolate=False)  # Resolution 145,143
#%%
frac2 = FractureGeo()
frac2.set_fracture('frac1', vertices=vert2, faces=fac2,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf = frac2.get_surface('frac1')
#%%
# p.add_object(surf, name='mesh', color='blue', style='wireframe')
# p.plotter.window_size = window_size
# p.plotter.camera_position = cam
# p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
# _ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
# p.snapshot('fracture_curvature/wire_mesh_regular_curved.pdf', save_dir=OUTPUT_DIR)
#%%
p.add_object(surf, name='mesh', color='blue', opacity=0.1)
p.add_object(surf, name='meshp', color='black', style='points')
p.add_object(surf, name='meshw', color='black', style='wireframe')

extent_plot = (0,30,-30,30,-30,20)
projected = surf.project_points_to_plane(origin=(extent_plot[0], extent_plot[3], extent_plot[4]), normal=(0,0,1))
projected2= surf.project_points_to_plane(origin=(extent_plot[0], extent_plot[3], extent_plot[4]), normal=(0,1,0))
projected3= surf.project_points_to_plane(origin=(extent_plot[1], extent_plot[3], extent_plot[4]), normal=(1,0,0))
p.add_object(projected, name='MeshProjected', opacity=0.2, color='blue')
p.add_object(projected, name='MeshProjectedp', opacity=0.05, color='black', style='points')
p.add_object(projected2, name='MeshProjected1', opacity=0.2, color='blue')
p.add_object(projected2, name='MeshProjected1p', opacity=0.05, color='black', style='points')
p.add_object(projected3, name='MeshProjected2', opacity=0.2, color='blue')
p.add_object(projected3, name='MeshProjected2p', opacity=0.05, color='black', style='points')
p.show(xrange=extent_plot[0:2], yrange=extent_plot[2:4], zrange=extent_plot[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0.05, 0.05, 0.2, 0.2))
p.plotter.window_size = (1400, 1900)
p.plotter.camera_position = [(-65.65490143695106, -134.3546884183316, 37.313418717418415),
 (9.945855686874964, -4.930486279508232, -8.19796690638818),
 (0.10932170089281378, 0.27230736907528985, 0.955979844170993)]

p.snapshot('fracture_curvature/wire_mesh_regular_curved.pdf', save_dir=OUTPUT_DIR)
#%%
p.add_object(surf.points, name='mesh', color='blue', style='wireframe')
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.snapshot('fracture_curvature/points_mesh_regular_curved.pdf', save_dir=OUTPUT_DIR)


#%%
plt.scatter(vertices2[:,0], vertices2[:,1], c=vertices2[:,2], cmap='gist_earth')
plt.xlabel('Local Easting (m) = X')
plt.ylabel('Local Northing (m) = Y')
plt.title('View from axis = 2 (Top view)')
cbar = plt.colorbar()
cbar.set_label('Elevation (m) = Z')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis2_view_curved.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vertices2[:,0], vertices2[:,2], c=vertices2[:,1], cmap='gist_earth')
plt.xlabel('Local Easting (m) = X')
plt.ylabel('Elevation (m) = Z')
plt.title('View from axis = 1 (North view)')
cbar = plt.colorbar()
cbar.set_label('Local Northing (m) = Y')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis1_view_curved.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vertices2[:,1], vertices2[:,2], c=vertices2[:,0], cmap='gist_earth')
plt.xlabel('Local Northing (m) = Y')
plt.ylabel('Elevation (m) = Z')
plt.title('View from axis = 0 (East view)')
cbar = plt.colorbar()
cbar.set_label('Local Easting (m) = X')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis0_view_curved.pdf', transparent=True)
plt.show()
