import numpy as np
import pandas as pd
import pyvista as pv
from bedretto import (data,
                      geometry,
                      model)
from fracwave import OUTPUT_DIR, SourceEM, FractureGeo, Antenna, FracEM
import matplotlib.pyplot as plt
import pickle

odir = OUTPUT_DIR + 'fracture_curvature/real_inversion/'

tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
# tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1980, 1990])

bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
d = pd.read_excel(odir+'2016571_FractureMapping_20230209.xlsx', header=8)
from bedretto.core.common_helpers import shift_wrt_lab_origin
xyz = shift_wrt_lab_origin(d[['E','N','H']].to_numpy())
points = np.vstack([bb['Pointcloud'][0], xyz])
from bedretto.core.vector_helpers import get_normal_from_points, get_azimuth_and_dip_from_normal_vector
normal, center = get_normal_from_points(xyz, algorithm='SVD')

df_tunnel = pd.DataFrame(points, columns=['X','Y','Z'])
df_orientations = pd.DataFrame({'X': [center[0]],
                                'Y': [center[1]],
                                'Z': [center[2]],
                                'G_x': normal[0],
                                'G_y': normal[1],
                                'G_z': normal[2]})

vk82 = tun_dat.get_VK_info(82)[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy() # p2
vk83 = tun_dat.get_VK_info(83)[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy() # p1
vk84 = tun_dat.get_VK_info(84)[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy() # p3

vk_82_83 = np.linalg.norm(vk82 - vk83)
vk_82_84 = np.linalg.norm(vk82 - vk84)
vk_83_84 = np.linalg.norm(vk83 - vk84)
#%% Initial geometry
struc_dat = tun_dat.get_tunnel_fractures(structure_ID='-5')
# struc, mark = tun_geom.get_disks_from_tunnel_structures(struc_dat, radius=3.5, c_res=20, r_res=10)
struc = pv.Disc(center=center, normal=normal, inner=0, outer =4, c_res=20, r_res=10 )
# struc = tun_geom.get_fracture_planes(structure_ID='-5')
# p.add_object(struc, name='fault', color='yellow', opacity=0.5, show_edges=True)

arrow = pv.Arrow(start=df_orientations[['X','Y','Z']].to_numpy(),
                 direction=df_orientations[['G_x','G_y','G_z']].to_numpy())
# p.add_object(arrow, name='direction', color='blue')

#%% Load the tunnel mesh
obj_path="/home/daniel/Downloads/bb pcd/cluster/model.obj"
texture_path = "/home/daniel/Downloads/bb pcd/cluster/model.jpg"
output_path = odir + 'fig8_a_initial_points_BB_VALTER'

mesh = pv.read(obj_path)
tex = pv.read_texture(texture_path)
#%% For plotting this. Having multiple screens is a problem
p = model.ModelPlot()
p2 = pv.Plotter()
p2.add_mesh(mesh, name="mesh", texture=tex)
p2.add_mesh(arrow, name='direction', color='blue')
p2.add_mesh(struc, name='fault', color='yellow', opacity=0.5, show_edges=True)
p2.add_mesh(df_tunnel[['X', 'Y', 'Z']].to_numpy(), name='points', color='red',
             render_points_as_spheres=True, point_size=30)
axes = p._start_axes(**p.p_axes.kwargs_axes)

axes.axes(p2,
          xrange=p2.bounds[0:2],
          yrange=p2.bounds[2:4],
          zrange=p2.bounds[4:6],
          )
p2.window_size = (1700,1200)
# p2.window_size = (1000,1600)
p2.camera_position = [(-13.380146698362958, -15.221118153471723, 1495.5405465304048),
 (9.502180064067337, -8.12805428369932, 1484.7465157803867),
 (0.3591210209672194, 0.2212436001542296, 0.9066881281291034)]
p2.add_axes(box=True)
# p2.export_vtkjs(output_path
p2.save_graphic(output_path+'.pdf', raster=False, painter=False)
p2.show()

#%% Save as HTML
# import panel as pn
# pn.extension('vtk')
# kwargs_panel = {}
# title = kwargs_panel.pop('title', 'Initial_model')
# sizing_mode=kwargs_panel.pop('sizing_mode', 'stretch_both')
# enable_keybindings=kwargs_panel.pop('enable_keybindings', True)
# orientation_widget=kwargs_panel.pop('orientation_widget', True)
# #%%
# a = pn.pane.VTK(output_path + '.vtkjs',
#                 sizing_mode=sizing_mode,
#                 enable_keybindings=enable_keybindings,
#                 orientation_widget=orientation_widget,
#                 **kwargs_panel)
# a.save(output_path, title=title)

#%%
# p.add_object(df_tunnel[['X', 'Y', 'Z']].to_numpy(), name='points', color='red',
#              render_points_as_spheres=True, point_size=10)
# p.add_object(struc, name='fault', color='yellow', opacity=0.5, show_edges=True)
# p.add_object(arrow, name='direction', color='blue')

#%%
# extent = [-200,
#                 20,
#                 -120,
#                 20,
#                 1300,
#                 1500]
# resolution = (50, 50, 50)
# ant = Antenna()
# ant.load_hdf5(odir + 'real_inversion.h5')
#
# boreholes = ['MB1',
#              'MB2',
#              'MB4',
#              'MB5',
#              # 'MB7', # Problematic. Don't know if I'm picking the correct reflector
#              # 'MB8',  # Problematic. Does not converge
#              'ST1',
#              'ST2',
#              ]
# transmitter = []
# receiver = []
#
# for bn in boreholes:
#     transmitter.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Tx','Ty','Tz')].to_numpy())
#     receiver.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Rx','Ry','Rz')].to_numpy())
#
# transmitter = np.vstack(transmitter)
# receiver = np.vstack(receiver)
# from fracwave.inversion.geometry_inversion import GeometryInversion
#
# inv = GeometryInversion(extent=extent,
#                         gempy_resolution=resolution,
#                         velocity=0.1234,
#                         antenna=ant,
#                         measured_travel_time=None)
# inv.max_distance_from_borehole = 50
# inv.max_distance_from_surface_minimum_distance = 50
# inv.fixed_control_points = df_tunnel
# # inv.fixed_control_points = None
# inv.fixed_orientations = df_orientations
# inv.kwargs_remesh = dict(max_element_size=1, extrapolate=False, plane=1)
#
# _ = inv.forward_solver()
# bor_dat = data.BoreholeData()
# bor_geom = geometry.BoreholeGeometry(bor_dat)
# bh_data_all_u = {bn: bor_dat.get_borehole_data(borehole_name=bn,
#                                              columns=['Depth (m)',
#                                                       'Easting (m)',
#                                                       'Northing (m)',
#                                                       'Elevation (m)',
#                                                       ])
#                for  bn in boreholes}
# # Filter all below 1300
# bh_data_all = {bn: df.loc[df['Elevation (m)'] >= 1300] for bn, df in bh_data_all_u.items()}
# bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
#                                                         radius=1)
#                for bn, info in bh_data_all.items()}
# p2 = model.ModelPlot()
# [p2.add_object(mesh, color='black', name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
# p2.add_object(inv.grid, name='initial_model',  color='yellow')#, opacity=0.8, )
#
# tunnel_mesh2 = tun_geom.get_tunnel_mesh(TM_range=[1980,2100])
# p2.add_object(tunnel_mesh2, name='tunnel_mesh', color='gray', opacity=0.5)
#
# def project_planes(mesh, name, p, color):
#     mesh = mesh.extract_geometry()
#     projectedxy = mesh.project_points_to_plane(origin=(0, 0, p.bounds[4]), normal=(0, 0, 1))
#     p.add_object(projectedxy, name=f'{name}_xy', color=color, opacity=0.1)
#
#     projectedxz = mesh.project_points_to_plane(origin=(0, p.bounds[3], 0), normal=(0, 1, 0))
#     p.add_object(projectedxz, name=f'{name}_xz', color=color, opacity=0.1)
#
#     projectedyz = mesh.project_points_to_plane(origin=(p.bounds[1], 0, 0), normal=(1, 0, 0))
#     p.add_object(projectedyz, name=f'{name}_yz', color=color, opacity=0.1)
#
# [project_planes(mesh, name='borehole_{}'.format(key),p=p2, color='black') for key, mesh in bh_geom_all.items()]
# project_planes(tunnel_mesh2, 'tunnel', p2, 'gray')
# project_planes(inv.grid, 'init', p2, 'yellow')
# p2.show(yzgrid2=True)
#
# points = {bn: df.loc[df['Depth (m)'] == 0, ('Easting (m)', 'Northing (m)', 'Elevation (m)')].to_numpy() for bn, df in bh_data_all.items()}
# order = ['MB4', 'MB5', 'ST1', 'MB2', 'MB1', 'ST2']
# offset =  {'MB4':(35,20),
#            'MB5': (20, 30),
#            'ST1': (0, 32),
#            'MB2': (-12, 32),
#            'MB1':(-30,25),
#            'ST2':(-40,15)}
# [p2.add_3d_label(points[o], name=f'po {o}', labels=o, s=6, offset=off) for o, off in offset.items()]
#
# p2.plotter.window_size = (1855, 1584)
# p2.plotter.camera_position = [(-428.9705066560481, -449.2065456513038, 1506.5065033917674),
#  (-65.27826953584375, 38.678011998314844, 1366.640832122879),
#  (0.11730737886134149, 0.1918869364873254, 0.9743810253028282)]
#
# p2.plotter.add_axes(box=True)
# p2.snapshot('fig8_b_Initial_model_all.pdf', save_dir=odir)


#%%