import numpy as np
import pandas as pd
import pyvista as pv
from bedretto import (data,
                      geometry,
                      model)
from fracwave import OUTPUT_DIR, SourceEM, FractureGeo, Antenna, FracEM
import matplotlib.pyplot as plt
import pickle

import scipy.io as io
odir = OUTPUT_DIR + 'fracture_curvature/real_inversion/'
filename = odir + 'real_inversion.h5'
from gdp import DATA_DIR as gdpdat
path_files = gdpdat + 'VALTER/'

tun_dat = data.TunnelData()

gempy_extent = [-200,
                20,
                -120,
                20,
                1300,
                1500]
resolution = (50, 50, 50)
# tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
#
# tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
# # tunnel_markers = tun_geom.interpolate_tunnel_markers(TM_range=[1950,2100], step_size=50)
# # Plot all data
# p = model.ModelPlot()
# p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# # p.add_label(tunnel_markers, labels='TM markers', name='tunnel markers')
#
# bh_colors = {'MB1':'green',
#              'MB2':'green',
#              #'MB3':'green',
#              'MB4':'gray',
#              'MB5':'gray',
#              'MB7':'gray',
#              'MB8':'gray',
#              'ST1':'red',
#              'ST2':'red'}
#
# # [p.add_object(mesh, color=bh_colors[key], name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
# [p.add_object(depths[key], color=bh_colors[key], name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
#
bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
d = pd.read_excel(odir+'2016571_FractureMapping_20230209.xlsx', header=8)
from bedretto.core.common_helpers import shift_wrt_lab_origin
xyz = shift_wrt_lab_origin(d[['E','N','H']].to_numpy())
points = np.vstack([bb['Pointcloud'][0], xyz])
from bedretto.core.vector_helpers import get_normal_from_points, get_azimuth_and_dip_from_normal_vector
normal, center = get_normal_from_points(xyz, algorithm='SVD')

df_tunnel = pd.DataFrame(points, columns=['X','Y','Z'])
df_tunnel['surface'] = 'A'
df_orientations = pd.DataFrame({'X': [center[0]],
                                'Y': [center[1]],
                                'Z': [center[2]],
                                'G_x': normal[0],
                                'G_y': normal[1],
                                'G_z': normal[2]})
df_orientations['surface'] = 'A'

#%%
p100mhz = {'ST1': [# 'ST1_100MHz_210226.mat', This one seems to not have the time zero correction
                   'ST1_100MHz_210218.mat',
                   # 'ST1_100MHz_210215.mat',
                   # 'ST1_100MHz_210213.mat',
                   # 'ST1_100MHz_210212.mat'
                   ],
           'ST2': ['ST2_100MHz_200728.mat'],
           'MB8':  [#'MB8_100MHz_210525.mat', Here the depth is wrong
                    'MB8_100MHz_210505.mat'],
           'MB7': ['MB7_100MHz_210518.mat'],
           'MB5': ['MB5_100MHz_210324.mat'],
           'MB4': [#'MB4_100MHz_200605.mat', # Here the depth is wrong
                   'MB4_100MHz_200529.mat'
                   ],
           'MB2': ['MB2_100MHz_191212.mat'],
           'MB1': ['MB1_100MHz_200515.mat'],
                   #'MB1_100MHz_200501.mat',
                   #'MB1_100MHz_200218.mat',
                   # 'MB1_100MHz_200211.mat', This I cannot see the bb
                   #'MB1_100MHz_191212.mat'],
                    #]
           }

#%%

c = {'MB1': (10, 160, 980, 0),
     'ST1': (20, 120, 600, 0), # Depth seems wrong. Fixed by forcing 20 as the first value
     'ST2': (35, 165, 992, 0),
     'MB8':(22,120, 633, 0),  # No convergence
     # 'MB7': (40,250,850,300),
     'MB7': (10,250,500,250),
     'MB5': (5,100, 580,0),
     'MB2': (15,140,800,0),
     'MB4': (5, 82, 410, 0)}
picks = {}
# fn = p100mhz['MB4']
# bn = 'MB4'
# for bn in c.keys():
#     with open(odir + f'manual_picks_{bn}', 'rb') as f:
#         picks[bn] = pickle.load(f)
#%%
for bn, fn, in p100mhz.items():
    try:
        with open(odir + f'manual_picks_{bn}', 'rb') as f:
            picks[bn] = pickle.load(f)
    except:
        from gdp.processing.image_processing import pick_line
        picks[bn] = {}
        file = fn[0]
        from gdp import DATA_DIR as gdpdat
        mat = io.loadmat(gdpdat + 'VALTER/' + file)
        depth = mat['depth'][:, 0]
        if bn == 'ST1':
            depth += 20 - 2.373
        processed_data = mat['processed_data']
        travel_time = mat['travel_time'][0]
        radius = mat['radius'][0]

        extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
        processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                                   out=np.zeros_like(processed_data),
                                   where=np.abs(processed_data).max(axis=1)[:, None] != 0)

        # from scipy.stats import rv_histogram

        plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto'); plt.grid(); plt.show()
        # plt.imshow(mat['raw_data'], cmap='seismic', aspect='auto');
        # plt.xlabel('borehole depth')
        # plt.ylabel('Travel time (ns)')
        # plt.show() 14
        cut = c[bn] # This will change for all
        mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
        mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
        cut_image = processed_data[mask_time][:, mask_depth]
        plt.imshow(cut_image, cmap='seismic', extent=cut, aspect='auto');plt.grid();plt.show()
        # plt.xlim((cut[0], cut[1]))
        # plt.ylim((cut[2], cut[3]))
        # plt.show()
        for p in range(10):
            picks[bn][p] = pick_line(cut_image, colormap='seismic', interpolate_path=True, radius_point=1, extent=cut,
                                        automatic_naming=True)

        with open(odir + f'manual_picks_{bn}', 'wb') as f:
            pickle.dump(picks[bn], f)

#%% Second face

c_bot = {'MB1': (160, 225, 500, 0),
     'ST1': (100, 150, 500, 0), # Maybe a small section but not sure
     # 'ST2': (35, 165, 992, 0),
     # 'MB8':(22,120, 633, 0),  # No convergence
     # 'MB7': (40,250,850,300),
     # 'MB7': (10,250,500,250),
     'MB5': (80, 200, 700, 0), # Strong reflector
     'MB2': (140,180,300,0), # At the beggining clear
     'MB4': (80, 190, 750, 0)} # Clear by sections
picks_bot = {}
# fn = p100mhz['MB4']
# bn = 'MB4'
# for bn in c.keys():
#     with open(odir + f'manual_picks_{bn}', 'rb') as f:
#         picks[bn] = pickle.load(f)
#%%
for bn, fn, in p100mhz.items():
    if bn in c_bot.keys():
        try:
            with open(odir + f'manual_picks_bot_{bn}', 'rb') as f:
                picks_bot[bn] = pickle.load(f)
        except:
            from gdp.processing.image_processing import pick_line
            picks_bot[bn] = {}
            file = fn[0]
            from gdp import DATA_DIR as gdpdat
            mat = io.loadmat(gdpdat + 'VALTER/' + file)
            depth = mat['depth'][:, 0]
            if bn == 'ST1':
                depth += 20 - 2.373
            processed_data = mat['processed_data']
            travel_time = mat['travel_time'][0]
            radius = mat['radius'][0]

            extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
            processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                                       out=np.zeros_like(processed_data),
                                       where=np.abs(processed_data).max(axis=1)[:, None] != 0)

            # from scipy.stats import rv_histogram

            plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto'); plt.grid(); plt.show()
            # plt.imshow(mat['raw_data'], cmap='seismic', aspect='auto');
            # plt.xlabel('borehole depth')
            # plt.ylabel('Travel time (ns)')
            # plt.show() 14
            cut = c_bot[bn] # This will change for all
            mask_depth = (min(cut[0:2]) <= depth) & (depth <= max(cut[0:2]))
            mask_time = (min(cut[2:4]) <= travel_time) & (travel_time <= max(cut[2:4]))
            cut_image = processed_data[mask_time][:, mask_depth]
            plt.imshow(cut_image, cmap='seismic', extent=cut, aspect='auto');plt.grid();plt.show()
            # plt.xlim((cut[0], cut[1]))
            # plt.ylim((cut[2], cut[3]))
            # plt.show()
            for pi in range(10):
                if pi not in picks_bot[bn].keys():
                    picks_bot[bn][pi] = pick_line(cut_image, colormap='seismic', interpolate_path=True, radius_point=1, extent=cut,
                                                automatic_naming=True)

            with open(odir + f'manual_picks_bot_{bn}', 'wb') as f:
                pickle.dump(picks_bot[bn], f)
#%% Evaluate the error
from scipy import stats
val = {}
mean_val = {}
iqr = {}
all_normalized_points = {}
error_trace = {}
coordinates = {}
counter_points = {}
all_borehole_picks = set(list(c.keys()) + list(c_bot.keys()))
include_bottom = False
all_coordinates = {}
coor = []
for bn in all_borehole_picks:
    val = []
    if bn in picks:
        for no in picks[bn].keys():
            for pi in picks[bn][no].keys():
                val.append(np.c_[picks[bn][no][pi]['true coordinates'], picks[bn][no][pi]['pixel coordinates'][:, 0]])
    if bn in picks_bot and include_bottom:
        for no in picks_bot[bn].keys():
            for pi in picks_bot[bn][no].keys():
                val.append(np.c_[picks_bot[bn][no][pi]['true coordinates'], picks_bot[bn][no][pi]['pixel coordinates'][:, 0] + 10_000])  # Add 10_000 so can clearly distinguish between top and bottom
    coordinates[bn] = np.vstack(val)
    mv = []
    iq = []
    ci = []
    anp = [] # all normalized points
    coor = []
    for d in list(set(coordinates[bn][:,2])):
        pos = np.ravel(np.argwhere(np.isclose(int(d), coordinates[bn][:, 2].astype(int),)))
        if len(pos) == 0:  # No number
            print(f'Not found, {d, pos}')
            continue
        if len(pos) < 5:  #"Less than 5 picks, don't take it into account"
            continue
        coor.append((min(coordinates[bn][pos, 1]), max(coordinates[bn][pos, 1])))
        iq.append(stats.iqr(coordinates[bn][pos, 1], interpolation='midpoint'))
        mv.append((coordinates[bn][pos, 0].mean(), coordinates[bn][pos, 1].mean()))
        anp.append(coordinates[bn][pos, 1] - coordinates[bn][pos, 1].mean())  # Take all the points and substract the mean to create a normal distributed data
        ci.append((d, len(pos)))
    all_coordinates[bn] = np.asarray(coor)
    all_normalized_points[bn] = np.hstack(anp)  # Take all the points and substract the mean to create a normal distributed data
    counter_points[bn] = np.asarray(ci, dtype=int)
    mean_val[bn] = np.asarray(mv)
    iqr[bn] = np.sqrt(np.square(iq).mean())
    error_trace[bn] = np.asarray(iq)

# mask = counter_points[bn][:,1] > 5  # More than 4 picks
aaa = np.hstack(list(all_normalized_points.values()))
error_all = stats.iqr(aaa, interpolation='midpoint')
#%%
# import seaborn as sns
# from scipy.stats import norm
# # fig, ax = plt.subplots()
# fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(10,8),  gridspec_kw={'height_ratios': [1, 3]})
# s = aaa
# # sns.histplot(s, ax=axes[1], stat='density', alpha=0.5)
# x, pdf  = sns.kdeplot(s, color='orange', ax=axes[1], label='Kernel density estimate').lines[0].get_data()
#
# n_bins = 50
# mu = s.mean()
# sigma=np.std(s)
# median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)
#
# n, bins, patches = axes[1].hist(s, n_bins, density=True, alpha=.3, edgecolor='black' )
# # pdf = 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2))
# # axes[1].plot(bins, pdf, color='orange', alpha=.6)
#
# mask1 = (x >= q1-1.5*(q3-q1)) & (x <= q1)
# mask2 = (x <= q3+1.5*(q3-q1)) & (x >= q3)
# bins_1 = x[mask1] # to ensure fill starts from Q1-1.5*IQR
# bins_2 = x[mask2]
# # pdf_1 = pdf[:int(len(pdf)/2)]
# # pdf_2 = pdf[int(len(pdf)/2):]
# pdf_1 = pdf[mask1]
# pdf_2 = pdf[mask2]
# # pdf_2 = pdf_2[(pdf_2 >= norm(mu,sigma).pdf(q3+1.5*(q3-q1))) & (pdf_2 <= norm(mu,sigma).pdf(q3))]
#
# #fill from Q1-1.5*IQR to Q1 and Q3 to Q3+1.5*IQR
# axes[1].fill_between(bins_1, pdf_1, 0, alpha=.4, color='orange')
# axes[1].fill_between(bins_2, pdf_2, 0, alpha=.4, color='orange')
#
#
# #top boxplot
# axes[0].boxplot(s, 0, 'gD', vert=False)
# axes[0].axvline(np.percentile(s, 50), color='red', alpha=.6, linewidth=.5)
# axes[0].axis('off')
#
#
# # axes[1].annotate("{:.1f}%".format(100*norm(mu, sigma).cdf(q1)), xy=((q1-1.5*(q3-q1)+q1)/2, 0), ha='center')
# axes[1].annotate("{:.2f}".format(error_all), xy=(median, 0.05), ha='center')
# # axes[1].annotate("{:.1f}%".format(100*(norm(mu, sigma).cdf(q3+1.5*(q3-q1)-q3)-norm(mu, sigma).cdf(q3))), xy=((q3+1.5*(q3-q1)+q3)/2, 0), ha='center')
# axes[1].annotate('q1', xy=(q1, norm(mu, sigma).pdf(q1)), ha='center')
# axes[1].annotate('q3', xy=(q3, norm(mu, sigma).pdf(q3)), ha='center')
#
# axes[1].set_ylabel('Density')
# axes[1].set_xlabel('Travel-time (ns) normalized to median for all points')
#
# plt.legend()
# plt.subplots_adjust(hspace=0)
# plt.show()
# fig.savefig(odir+'figures/error_all_points_real.pdf')

#%%
# for bn in all_borehole_picks:
#     # if bn != 'MB4': continue
#     from gdp import DATA_DIR as gdpdat
#     file = p100mhz[bn][0]
#     mat = io.loadmat(gdpdat + 'VALTER/' + file)
#     depth = mat['depth'][:, 0]
#     if bn == 'ST1':
#         depth += 20 - 2.373
#     processed_data = mat['processed_data']
#     travel_time = mat['travel_time'][0]
#     radius = mat['radius'][0]
#
#     extent = (depth[0], depth[-1], travel_time[-1], travel_time[0])
#     processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
#                                out=np.zeros_like(processed_data),
#                                where=np.abs(processed_data).max(axis=1)[:, None] != 0)
#     plt.imshow(processed_data, cmap='seismic', extent=extent, aspect='auto');
#     plt.plot(coordinates[bn][:,0], coordinates[bn][:,1], 'b.')
#     plt.plot(mean_val[bn][:,0], mean_val[bn][:,1], 'r.')
#     plt.title(bn)
#     plt.show()

#%%
sou = SourceEM()
try:
    # sou.load_hdf5(filename.split('.')[0]+'_energy.h5')
    sou.load_hdf5(filename)
except:
    samples=400
    f = np.linspace(0, 0.22, samples)
    sou.set_frequency_vector(f)

    firstp = sou.create_source() #a=1,
    print(sou)
    sou.plot_waveforms_zoom().show()
    sou.export_to_hdf5(filename, overwrite=True)
    sou.export_to_hdf5(filename.split('.')[0]+'_energy.h5', overwrite=True)
# wavelength = 0.1234 / sou.peak_frequency
# v = 0.1234  # m/ns -> Velocity in granite
# wavelength = v / sou.peak_frequency  # m
# max_element_size = wavelength / 6
max_element_size = 1
#%%  Boreholes
ant = Antenna()
downsample = 10
try:
    raise ValueError
    # ant.load_hdf5(filename.split('.')[0]+'_energy.h5')
    ant.load_hdf5(filename)
    # ant.load_hdf5(filename.split('.')[0]+'_energy.h5')
except:
    b = picks.keys()
    b = ['ST2', 'MB1', 'MB2', 'ST1', 'MB5', 'MB4']
    bor_dat = data.BoreholeData()
    bor_geom = geometry.BoreholeGeometry(borehole_data=bor_dat)

    bh_data_all = {bn: bor_dat.get_borehole_data(borehole_name=bn,
                                                 columns=['Depth (m)',
                                                          'Easting (m)',
                                                          'Northing (m)',
                                                          'Elevation (m)',
                                                          ])
                   for bn in b }
    bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
                                                            radius=1)
                   for bn, info in bh_data_all.items()}

    separation = 2.77

    depths_Tx = dict()
    depths_Rx = dict()
    orientations_Tx = dict()
    orientations_Rx = dict()

    # for bor in picks.keys():

    # ant.export_to_hdf5(file, overwrite=True)
    for bn in b:  # Iterate over boreholes
        depths = mean_val[bn][::downsample, 0]  # Downsample
        depth_r0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5)).T
        depth_r1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5) + 0.1).T
        orient_r = (depth_r0 - depth_r1) / np.linalg.norm(depth_r0 - depth_r1, axis=1)[:, None]

        depth_t0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths+(separation*0.5)).T
        depth_t1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths +(separation*0.5)+ 0.1).T
        orient_t = (depth_t0 - depth_t1) / np.linalg.norm(depth_t0 - depth_t1, axis=1)[:, None]

        ant.set_profile(bn,
                        receivers=depth_r0,
                        transmitters=depth_t0,
                        orient_receivers=orient_r,
                        orient_transmitters=orient_t,
                        depth_Rx=depths-(separation*0.5),
                        depth_Tx=depths+(separation*0.5))

    ant.export_to_hdf5(filename.split('.')[0]+'_energy.h5', overwrite=True)
    ant.export_to_hdf5(filename, overwrite=True)


#%%

# #%%
# tun_dat = data.TunnelData()
# tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
#
# tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
# # tunnel_markers = tun_geom.interpolate_tunnel_markers(TM_range=[1950,2100], step_size=50)
# # Plot all data
# p = model.ModelPlot()
# p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# # p.add_label(tunnel_markers, labels='TM markers', name='tunnel markers')
#
# bh_colors = {'MB1':'green',
#              'MB2':'green',
#              #'MB3':'green',
#              'MB4':'gray',
#              'MB5':'gray',
#              'MB7':'gray',
#              'MB8':'gray',
#              'ST1':'red',
#              'ST2':'red'}
#
# # [p.add_object(mesh, color=bh_colors[key], name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
# [p.add_object(depths[key], color=bh_colors[key], name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
#

#%% Multiple boreholes
# extent = [-200,
#                 20,
#                 -120,
#                 20,
#                 1300,
#                 1510]
# resolution = (50, 50, 50)
# from fracwave.inversion.geometry_inversion import GeometryInversion
# inv = GeometryInversion(extent=extent,
#                         gempy_resolution=resolution,
#                         velocity=0.1234,
#                         Tx=ant.Transmitter,
#                         Rx=ant.Receiver,
#                         measured_travel_time=np.vstack(mean_val.values())[::downsample,1])
#
# #%%
# try:
#     inverted_points_all = pd.read_csv(odir+'inverted_points_all.csv')
# except:
#     inv.clear()
#     inv.tol_pos = np.hstack(error_trace.values())   # Tolerance for a single trace
#     inv.tol_all = error_all   # Convert to time. Tolerance for overall RMS error over the whole picks
#     inv.surrounding = 10
#     inv.max_iter_all = 20
#     inv.max_iter_pos = 5
#     inv.std_threshold = 1  # Standard deviation for the second convergence criteria
#     inv.std_average = 3
#     # inv.measured_travel_time =
#     inv.fixed_control_points = df_tunnel
#     inv.fixed_orientations = df_orientations
#     # inv.temp_control_points = None
#     inv.directory = odir
#     inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
#
#     inverted_points_all = inv.run_inversion()
#     inverted_points_all.to_csv(odir+'inverted_points_all.csv')

#%% Specific boreholes:


transmitter = []
receiver = []
measured_time = dict()
tol_pos = []
# for bn in boreholes:
for bn in ant.name_profiles:
    transmitter.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Tx','Ty','Tz')].to_numpy())
    receiver.append(ant.profiles.loc[ant.profiles['profile'] == bn, ('Rx','Ry','Rz')].to_numpy())
    measured_time[bn] = mean_val[bn][::downsample,1]
    tol_pos.append(error_trace[bn])

tol_pos = np.hstack(tol_pos)
#%%
frac = FractureGeo()
frac.init_geo_model(gempy_extent, resolution)
#%%
from fracwave.inversion.geometry_inversion import GeometryInversion
inv = GeometryInversion(frac = frac,
                        extent = gempy_extent,
                        velocity=0.1234,
                        # Tx=transmitter,
                        # Rx=receiver,
                        antenna = ant,
                        measured_travel_time=measured_time)
 #%%
max_element_size = 1
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.07884711779448621 #sou.peak_frequency  # m
max_element_size = wavelength / 5
try:
    # inverted_points_selected = pd.read_csv(odir+'inverted_points_selected2.csv')
    # inverted_points_selected = pd.read_csv(odir+'inverted_points_selected_final2.csv')
    inverted_points_selected = pd.read_csv(odir+'inverted_points_selected_final_all.csv')
    # inverted_points_selected = inverted_points_selected[:-1]
    inv.fixed_control_points = inverted_points_selected
    inv.fixed_orientations = df_orientations
    # if satisfied with the model

    #else
    # max_element_size=1
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
    inv.max_distance_from_borehole = None
    inv.max_distance_from_surface_minimum_distance = 30
    inv.forward_solver()
    # p.add_object(inv.grid, name='grid2', color='gray', opacity=0.8)
    # with open(odir + f'history_simulation2', 'rb') as f:
    with open(odir + f'history_simulation_all', 'rb') as f:
        history = pickle.load(f)
except:
    inv.clear()
    inv.tol_pos = tol_pos   # Tolerance for a single trace
    inv.tol_all = error_all   # Convert to time. Tolerance for overall RMS error over the whole picks
    inv.surrounding = 10
    inv.max_iter_all = 20
    inv.max_iter_pos = 5
    inv.std_threshold = 0.3  # Standard deviation for the second convergence criteria
    inv.std_average = 5
    inv.resting_period = 2
    # inv.max_distance_from_borehole = 50  # mask everything farther away of 60 m
    inv.max_distance_from_borehole = 60
    inv.max_distance_from_surface_minimum_distance = 60
    # inv.measured_travel_time =
    inv.fixed_control_points = df_tunnel
    # inv.fixed_control_points = None
    inv.fixed_orientations = df_orientations
    # inv.temp_control_points = df_tunnel
    inv.directory = odir
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)

    inverted_points_selected = inv.run_inversion()
    plt.plot(inv.error_history); plt.axhline(error_all, color='r'); plt.xlabel('No. iterations'); plt.ylabel('RMS'); plt.show()
    # inverted_points_selected.to_csv(odir+'inverted_points_selected.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected2.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_final.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_final2.csv')
    inverted_points_selected.to_csv(odir+'inverted_points_selected_final_all.csv')
    inverted_points_selected = inv.run_inversion()
    plt.plot(inv.error_history); plt.axhline(error_all, color='r'); plt.xlabel('No. iterations'); plt.ylabel('RMS'); plt.show()
    # inverted_points_selected.to_csv(odir+'inverted_points_selected.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected2.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_final.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_final2.csv')
    inverted_points_selected.to_csv(odir+'inverted_points_selected_final_all.csv')
    # inverted_points_selected.to_csv(odir+'inverted_points_selected_top_only.csv')
    import imageio
    with imageio.get_writer(odir + 'pics/mygif.gif', mode='I') as writer:
        for fnp in inv.filenames_pics:
            image = imageio.v2.imread(fnp)
            writer.append_data(image)

    with open(odir + f'history_simulation_all', 'wb') as f:
        history = {'CP_history': np.vstack(inv.points_all_history),
                   'error_all_history': inv.error_all_history,
                   'error_history': inv.error_history}
        pickle.dump(history, f)

#%%
fig, ax = plt.subplots(figsize=(10,5))
hd0 =plt.plot(history['error_history'], '*-', label='RMS curve')
hd1=plt.axhline(history['error_history'][-1],linestyle=':', color='red', label=f'Minimim model error: {history["error_history"][-1]: .2f} ns')
# hd1=plt.axhline(17,linestyle=':', color='red', label=f'Minimim model error: {17: .2f} ns')
# hd1_=plt.axhline(2, linestyle=':',color='green', label=f'Picking error: {2: .2f} ns')
hd1_=plt.axhline(error_all, linestyle=':',color='green', label=f'Picking error: {error_all: .2f} ns')
# plt.yscale('log')
plt.xlabel('Global iterations', fontsize=20)
plt.ylabel('RMS (ns)', fontsize=20)
plt.grid()
plt.axvspan(2,9, color='black', alpha=0.1)
plt.axvspan(23,27, color='black', alpha=0.1)
plt.axvspan(30,32, color='black', alpha=0.1)
plt.axvspan(37,43, color='black', alpha=0.1)
plt.axvspan(47,49, color='black', alpha=0.1)
plt.axvspan(53,55, color='black', alpha=0.1)
hd2 = plt.axvspan(58, 62, color='black', alpha=0.1, label='Areas of increasing error (Backtracking applied)')
plt.axvspan(72,76, color='black', alpha=0.1)
plt.ylim(ymin=-10)
plt.xlim(xmin=-2,xmax=88)
plt.legend(frameon=True, fontsize=20)
ax.set_xticks(np.arange(0,90,10))
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontsize(22)
# plt.ylim(0,300)
# plt.gca().set_yscale('symlog')
plt.tight_layout()
plt.savefig(odir+'figures/fig7_d_convergence.pdf')

plt.show()

#%%
latest = dict(grid = inv.grid,
              vertices =inv.vertices,
              faces = inv.faces,
              element_positions=np.mean(inv.vertices[inv.faces], axis=1)
              )
#%%
inv.fixed_control_points = df_tunnel
# inv.fixed_control_points = inverted_points_selected
# inv.fixed_control_points = None
inv.fixed_orientations = df_orientations
inv.max_distance_from_borehole = 100
inv.max_distance_from_surface_minimum_distance = 100
inv.kwargs_remesh = dict(max_element_size=1, extrapolate=False, plane=1)
first_arrival_initial_all, min_position_initial, surf_initial = inv.forward_solver()
initial = dict(grid = inv.grid,
               vertices =inv.vertices,
               faces = inv.faces,
               element_positions=np.mean(inv.vertices[inv.faces], axis=1))

initial_model_first_arrival = {}
initial_model_reflection_point = {}
for bn in ant.name_profiles:
    min_distance_init, min_position_init = inv.frac.get_position_reflection_point(
        element_positions=initial['element_positions'],
        tx=ant.profiles.loc[ant.profiles['profile'] == bn][['Tx', 'Ty', 'Tz']].to_numpy(),
        rx=ant.profiles.loc[ant.profiles['profile'] == bn][['Rx', 'Ry', 'Rz']].to_numpy())
    initial_model_first_arrival[bn] = min_distance_init / inv.velocity
    initial_model_reflection_point = min_position_init


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure_to_plot = 'c'  # Options are 'a', 'b,, 'c'

if figure_to_plot=='a':
    all_cp = history['CP_history']
    all_error = history['error_all_history']
    accept = [0]
    positions_accept = all_cp[accept, 3].astype(int)
    df_current_points = pd.DataFrame(all_cp[accept, :3], columns=['X', 'Y', 'Z'])
    df_current_points['surface'] = 'A'
    inv.fixed_control_points = pd.concat((df_tunnel, df_current_points))
    iter = 1
    title = f'Position: 260 | (RMS pos.=1.11 ns; RMS all=25.84 ns) | (Iter. pos.=1; Global iter.={iter})\n'
elif figure_to_plot=='b':   # iteration 7
    all_cp = history['CP_history']
    all_error = history['error_all_history']
    accept = [2, 3, 5, 6]
    positions_accept = all_cp[accept, 3].astype(int)
    df_current_points = pd.DataFrame(all_cp[accept, :3], columns=['X', 'Y', 'Z'])
    df_current_points['surface'] = 'A'
    inv.fixed_control_points = pd.concat((df_tunnel, df_current_points))
    iter = 7
    title = f'Position: 7 | (RMS pos.=33.91 ns; RMS all=100.99 ns) | (Iter. pos.=1; Global iter.={iter})\n'

elif figure_to_plot=='c':  # Final iteration
    positions_accept = inverted_points_selected.loc[inverted_points_selected['pos'].notna(), 'pos'].to_numpy().astype(int)
    df_current_points= inverted_points_selected[['X', 'Y', 'Z']].copy()
    df_current_points['surface'] = 'A'
    inv.fixed_control_points = df_current_points
    iter = 86
    title = f'Position: 870 | (RMS pos.=1.77 ns; RMS all=19.45 ns) | (Iter. pos.=1; Global iter.={iter})\n'

# Let's plot the steps
# Options are 7 and 86


# for iter 7 the accepted positions are:

# inv.fixed_control_points = inverted_points_selected
# inv.fixed_control_points = None
inv.fixed_orientations = df_orientations
inv.max_distance_from_borehole = 100
inv.max_distance_from_surface_minimum_distance = 100
inv.kwargs_remesh = dict(max_element_size=1, extrapolate=False, plane=1)
first_arrival, min_position, surf = inv.forward_solver()

#%%
fig, ax = plt.subplots(figsize=(15, 8))
j = 0
xticks = [0]
sall = []
colors = {'MB1': 'orange',
          'MB2': 'green',
          # 'MB3':(-54,-8),
          'MB4': 'black',
          'MB5': 'blue',
          # 'MB7':(-40, -5),
          # 'MB8':(12,6),
          'ST1': 'magenta',
          'ST2': 'red'}
np.random.seed(1234)
for bn in ant.name_profiles:
    measu_t = measured_time[bn]

    # I need to do the forward solver based on the points

    simul_t = first_arrival[inv.map_antenna[bn]]
    # sall += simul_t.tolist()
    first_arrival_initial = first_arrival_initial_all[inv.map_antenna[bn]]

    hnd_m, = ax.plot(np.arange(j, j + len(measu_t)), measu_t, color='green', linestyle='--', alpha=0.8,
                     label='Field data')
    hnd_i, = ax.plot(np.arange(j, j + len(first_arrival_initial)), first_arrival_initial, color='red',
                     linestyle='--', alpha=0.8, label='Initial model')
    hnd_s, = ax.plot(np.arange(j, j + len(simul_t)), simul_t, color='blue', linestyle='-', label='Simulated data')
    ax.axvspan(j, j + len(first_arrival_initial), color=colors[bn] if bn in colors else np.random.rand(3),
               alpha=0.1)
    ax.text(j + len(first_arrival_initial) / 4, 90, bn,
            bbox=dict(edgecolor=colors[bn] if bn in colors else np.random.rand(3), linewidth=1,
                      facecolor='white'), fontsize=22)
    j += len(first_arrival_initial)  # + 10
    xticks.append(j)

optimized_positions = []
if len(positions_accept) >1:
    fix_points = positions_accept[:-1]
    hnd_o = ax.vlines(fix_points, color='gray', linestyles=':',
                      # ymin=self.ymin, ymax=self.ymax,
                      ymin=0, ymax=1200,
                      alpha=0.9,
                      label='Optimized CP')
    optimized_positions = fix_points.tolist()

current_pos = positions_accept[-1]
hnd_c = ax.vlines(current_pos, color='orange',
                  # ymin=self.ymin, ymax=self.ymax,
                  ymin=0, ymax=1200,
                  linestyles=':', label='CP current location')
optimized_positions += [current_pos]
from matplotlib.patches import Ellipse
circle1 = Ellipse((current_pos, first_arrival[current_pos]), 20, 30, zorder=10000, color='orange',
                  linewidth=3, fill=False)
ax.add_patch(circle1, )

ax1 = ax.twiny()
ax1.set_xlabel('Optimized Source-Receiver pair (ID number)', color='blue')
start, end = 0, len(np.hstack(np.hstack(list(measured_time.values()))))
ax1.set_xlim(start, end)
ax1.set_xticks(optimized_positions)
# ax.grid()
ax.set_xlabel('Source-Receiver pair (ID number)')
ax.set_ylabel('Two-way travel-time (ns)')
ax.set_xlim(start, end)
ax.set_ylim(0,1200)
ax.invert_yaxis()
ax.set_xticks(xticks, )
# handles, labels = ax.get_legend_handles_labels()
# ax.legend(handles, labels, loc='lower right',
#           frameon=True)
handles = [hnd_m, hnd_i, hnd_s, hnd_c, hnd_o]


labels = ['Field data', 'Initial model', 'Simulated data', 'Current location CP', 'Optimized CP']

ax.legend(handles,
          labels,
          loc='lower right',
          frameon=True,
          fontsize=20)
ax.tick_params(axis='both', which='both', direction='out', length=5)
ax1.tick_params(axis='both', which='both', direction='out', length=5, colors='blue', labelrotation=45)
ax1.spines["top"].set_edgecolor('blue')
ax.set_title(
    # f'Position: {currrent_pos} | (RMS position={self.error_pos:.2f} ns; RMS all= {self.error_all:.2f} ns)  | '
    # f'(Iter. position={self.iter_pos}; Global iter.={self.c})\n', fontsize=24)
    title, fontsize=24)

for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels() +
             [ax1.xaxis.label] + ax1.get_xticklabels()):
    item.set_fontsize(26)
fig.tight_layout()
#
# # save frame
fig.tight_layout()
fig.savefig(odir+f'figures/fig7_{figure_to_plot}_{iter}.pdf')
fig.show()


#%%
from scipy.spatial import KDTree
surf_tree = KDTree(initial['vertices'])
dd_trajectory, ii = surf_tree.query(latest['vertices'], workers=-1)

latest['grid']['Distance misfit (m)'] = dd_trajectory

max_val = max(dd_trajectory)
min_val = min(dd_trajectory)

#%%
def project_planes(mesh, name, p, **kwargs):
    mesh = mesh.extract_geometry()
    projectedxy = mesh.project_points_to_plane(origin=(0, 0, p.bounds[4]), normal=(0, 0, 1))
    p.add_object(projectedxy, name=f'{name}_xy', **kwargs)

    projectedxz = mesh.project_points_to_plane(origin=(0, p.bounds[3], 0), normal=(0, 1, 0))
    p.add_object(projectedxz, name=f'{name}_xz', **kwargs)

    projectedyz = mesh.project_points_to_plane(origin=(p.bounds[1], 0, 0), normal=(1, 0, 0))
    p.add_object(projectedyz, name=f'{name}_yz', **kwargs)

#%%
tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
#%%
p=model.ModelPlot()
#%%
# p.add_object(inv.grid, name='grid', color='yellow')
p.add_object(latest['grid'], name='grid', scalars='Distance misfit (m)', cmap='turbo', show_scalar_bar = False)


tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
project_planes(tunnel_mesh, f'TM', p, opacity=0.01, color='black')

points = {}
# for bn in boreholes:
for bn in ant.name_profiles:
    df = ant.profiles.loc[ant.profiles['profile'] == bn]
    points[bn] = df.iloc[0][['Tx', 'Ty', 'Tz']].to_numpy()
    spline = pv.Spline(df[['Tx','Ty','Tz']].to_numpy(), 400)
    bh_tube = spline.tube()

    p.add_object(bh_tube, name=f'borehole_{bn}', color='black' )#if bn != 'MB1' else 'red')

    project_planes(bh_tube, f'pb_{bn}', p, opacity=0.1, color='black' )# if bn != 'MB1' else 'red')

project_planes(latest['grid'], name='grid', p=p, opacity=0.3, color='gray')
p.remove_scalar_bar()
p.plotter.add_scalar_bar(title=f'Distance misfit (m)', title_font_size=40,
                         label_font_size=40, vertical=False,
                         position_x=0.3, position_y=0.05)

offset =  {'MB1':(-35,34),
             'MB2':(-15,50),
             # 'MB3':(-54,-8),
             'MB4':(56,25),
             'MB5':(20,32),
             # 'MB7':(-40, -5),
             # 'MB8':(12,6),
             'ST1':(0,50),
             'ST2':(-60,40)}

colors =  {'MB1':'orange',
             'MB2':'green',
             # 'MB3':(-54,-8),
             'MB4':'black',
             'MB5':'blue',
             # 'MB7':(-40, -5),
             # 'MB8':(12,6),
             'ST1':'magenta',
             'ST2':'red'}

[p.add_3d_label(points=poin,
             labels=labl,
             name=f'label_boreholes_{labl}',
                c=colors[labl],# if labl != 'MB1' else 'red',
                offset=offset[labl],
                s=8) for poin, labl in zip(np.vstack((points.values())).astype(float), list(points.keys()),)]

iv = inverted_points_selected.dropna()
cp = iv.reset_index()[['X', 'Y', 'Z']].to_numpy()

p.add_label(points=cp,always_visible=True,
            labels=np.arange(0, len(cp)), name='b1', point_size=10, point_color='black', font_size=45)
p.add_object(cp, color='black', name='cp')
project_planes(pv.PolyData(cp), color='black', name='cp', p=p, point_size=10)
iv.reset_index()[['X', 'Y', 'Z']].to_numpy()
# p.remove_scalar_bar()
# p.plotter.add_scalar_bar(title=f'Distance (m)', title_font_size=40,
#                              label_font_size=40, vertical=False,
#                              position_x=0.3, position_y=0)

p.show(yzgrid2=True)
p.plotter.window_size = (2100, 2000)
p.plotter.camera_position = [(-300.0695993418476, -407.9928034103359, 1510.7549942757203),
 (-51.11014031822393, 30.605904921282036, 1401.5124118870704),
 (0.07201522586759396, 0.20237978441960933, 0.976655635370785)]

p.plotter.add_axes(box=True)
# p.snapshot('figures/end_geometry2.pdf', save_dir=odir)
# p.save_scene_as_html(odir+'figures/end_geometry')

#%% o make convergence plot
# new_points = inverted_points_selected.dropna().reset_index(drop=True)
# current = df_tunnel.copy()
# ea = []
# for i, df in new_points.iterrows():
#     current = current.append(df[['X','Y','Z']])
#     # inv.max_distance_from_borehole = 40  # mask everything farther away of 60 m
#     inv.max_distance_from_borehole = 60
#     inv.max_distance_from_surface_minimum_distance = 60
#     # inv.measured_travel_time =
#     inv.fixed_control_points = current
#     # inv.fixed_control_points = None
#     inv.fixed_orientations = df_orientations
#     # inv.temp_control_points = df_tunnel
#     first_arrival, min_position = inv.forward_solver()
#     ea.append(np.sqrt(np.square(measured_time - first_arrival).mean()))
#
# fig, ax = plt.subplots()
# ax.plot(ea)
# ax.axhline(error_all, color='r')
# ax.set_xlabel('No. inserted control points')
# ax.set_ylabel('RMS')
# plt.show()
# inverted_points_selected.to_csv(odir+'inverted_points_selected.csv')
# inverted_points_selected.to_csv(odir + 'inverted_points_selected_top_only.csv')

#%%############ Plots of the step by step
all_grids = {}
#%%############ Plots of the step by step
points_to_show = [0,-1]#, 0, 18, 19, 23, 26]  # -1 is the whole model, 0 is for the initial model
p=model.ModelPlot()

tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)

points = {}
offset = {'MB1': (-35, 34),
          'MB2': (-15, 50),
          # 'MB3':(-54,-8),
          'MB4': (56, 25),
          'MB5': (20, 32),
          # 'MB7':(-40, -5),
          # 'MB8':(12,6),
          'ST1': (0, 50),
          'ST2': (-60, 40)}
colors =  {'MB1':'orange',
             'MB2':'green',
             # 'MB3':(-54,-8),
             'MB4':'black',
             'MB5':'blue',
             # 'MB7':(-40, -5),
             # 'MB8':(12,6),
             'ST1':'magenta',
             'ST2':'red'}
#%%
for i in points_to_show:
    p.remove_scalar_bar()
    if i == -1:
        df = inverted_points_selected
    elif i == 0:
        break
        df = df_tunnel
    else:
        break
        df = inverted_points_selected[:i + 1]
    if i not in all_grids:

        inv.fixed_control_points = df
        inv.fixed_orientations = df_orientations

        # max_element_size=1
        inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=1)
        inv.max_distance_from_borehole = None
        inv.max_distance_from_surface_minimum_distance = 30
        inv.forward_solver()

        grid = inv.grid
        vertices = inv.vertices
        faces = inv.faces
        all_grids[i] = dict(grid = inv.grid,
                            vertices = inv.vertices,
                            faces = inv.faces)
        if i != 0:
            surf_tree_temp = KDTree(initial['vertices'])
            dd_trajectory_temp, ii = surf_tree_temp.query(vertices, workers=-1)

            grid['Distance misfit (m)'] = dd_trajectory_temp
            # grid['Distance misfit (m)'] = dd_trajectory
    else:
        grid = all_grids[i]['grid']
        vertices = all_grids[i]['vertices']
        faces = all_grids[i]['faces']

    if i != 0:
        p.add_object(grid, name='grid', scalars='Distance misfit (m)', cmap='turbo', clim= (min_val, max_val), show_scalar_bar=False)
        p.remove_scalar_bar()
        p.plotter.add_scalar_bar(title=f'Distance misfit (m)', title_font_size=40,
                                 label_font_size=40, vertical=True,
                                 # position_x=0.3, position_y=0.05,
                                 position_x=0.85, position_y=0.5,
                                 )
    else:
        p.add_object(grid, name='grid', color='yellow', )
    p.show(xrange=[-150, 35],yrange=[-106, 75], zrange=[1354, 1499], yzgrid2=True, number_of_divisions=10,
           xtitle_offset=0.03,
           ztitle_offset=0.03,
           ytitle_offset=0.03,
           )
    project_planes(grid, name='grid', p=p, opacity=0.3, color='gray', show_scalar_bar=False)

    if i != 0:
        iv = df.dropna()
        cp = iv.reset_index()[['X', 'Y', 'Z']].to_numpy()

        p.add_label(points=cp, always_visible=True,
                    labels=np.arange(0, len(cp)), name='b1', point_size=10, point_color='black', font_size=45)
        p.add_object(cp, color='black', name='cp')
        project_planes(pv.PolyData(cp), color='black', name='cp', p=p, point_size=10)
    else:
        for rem in ['cp', 'cp_xy', 'cp_xz', 'cp_yz']:
            p.remove(rem)
        p.remove_label('b1')
    travel_time = {}
    ### Plot boreholes
    mp = {}
    for bn in ant.name_profiles:
        ant_pos = ant.profiles.loc[ant.profiles['profile'] == bn][['Tx', 'Ty', 'Tz']].to_numpy()
        ant_pos_rx = ant.profiles.loc[ant.profiles['profile'] == bn][['Rx', 'Ry', 'Rz']].to_numpy()
        # points[bn] = ant_pos[0]
        from bedretto.core.common_helpers import tube_from_points
        bh_tube = tube_from_points(ant_pos, 0.8)

        p.add_object(bh_tube, name=f'borehole_{bn}', color=colors[bn])  # if bn != 'MB1' else 'red')

        project_planes(bh_tube, f'pb_{bn}', p, opacity=0.1, color=colors[bn])  # if bn != 'MB1' else 'red')

        p.add_3d_label(points=ant_pos[0],
                       labels=bn,
                       name=f'label_boreholes_{bn}',
                       c=colors[bn],  # if labl != 'MB1' else 'red',
                       offset=offset[bn],
                       s=8)

        min_distance_t, min_position_t =inv.frac.get_position_reflection_point(
            # element_positions=np.mean(vertices[faces], axis=1),
            element_positions=np.mean(vertices[faces], axis=1),
                                                                               tx=ant_pos,
                                                                               rx=ant_pos_rx)
        travel_time[bn] = min_distance_t / inv.velocity
        mp[bn] = min_position_t
        # p.add_object(min_position_t, color=colors[bn], name=f'reflection_point_{bn}')

    project_planes(tunnel_mesh, f'TM', p, opacity=0.01, color='black')

    # p.show(yzgrid2=True, number_of_divisions=10)
    p.plotter.add_axes(box=True)

    if i != 0:
        [p.remove(f'reflection_point_{bn}') for bn in ant.name_profiles]

        p.plotter.window_size = (2200, 2000)
        p.plotter.camera_position = [(-482.1593981569464, -183.6140055648634, 1540.2511103266356),
                                     (-30.856656822928944, 21.88955906382619, 1397.5006231689997),
                                     (0.21992539826387153, 0.18266991011649802, 0.9582611977621279)]
        # p.plotter.camera_position = [(-300.0695993418476, -407.9928034103359, 1510.7549942757203),
        #                              (-51.11014031822393, 30.605904921282036, 1401.5124118870704),
        #                              (0.07201522586759396, 0.20237978441960933, 0.976655635370785)]
        p.snapshot(f'figures/fig8_d_end_geometry2_{i}.pdf', save_dir=odir)

        p.remove_scalar_bar()
        p.add_object(grid, name='grid', color='yellow')
        [p.add_object(mp[bn], color=colors[bn], name=f'reflection_point_{bn}') for bn in ant.name_profiles]
        p.plotter.window_size = (1900, 2000)
        p.plotter.camera_position = [(-54.8191999037508, -467.8872922091296, 1415.1812764696097),
 (-54.8191999037508, -31.73852902451207, 1415.1812764696097),
 (-0.008932909266923625, 0.0, 0.9999601007700403)]
        p.show(zaxis_rotation=0, yaxis_rotation=0)
        p.plotter.add_axes(box=True)

        p.snapshot(f'figures/fig8_c_end_geometry3_{i}.pdf', save_dir=odir)
        break
    else:
        [p.add_object(mp[bn], color=colors[bn], name=f'reflection_point_{bn}') for bn in ant.name_profiles]

        p.plotter.window_size = (2200, 2000)
        p.plotter.camera_position = [(-482.1593981569464, -183.6140055648634, 1540.2511103266356),
 (-30.856656822928944, 21.88955906382619, 1397.5006231689997),
 (0.21992539826387153, 0.18266991011649802, 0.9582611977621279)]
        # p.show(xrange==(-150,
        p.snapshot(f'figures/fig8_b_end_geometry0.pdf', save_dir=odir)

    break
    # 2D plot
    fig, ax = plt.subplots(figsize=(15,7))
    j = 0
    xticks = [0]
    sall = []
    for bn in travel_time.keys():

        measu_t = mean_val[bn][::downsample, 1]
        simul_t = travel_time[bn]
        sall += simul_t.tolist()
        first_arrival_initial = initial_model_first_arrival[bn]

        hnd_m, = ax.plot(np.arange(j, j+len(measu_t)), measu_t, color='green', linestyle='--', alpha=0.8)
        if i != 0:
            hnd_s, = ax.plot(np.arange(j, j+len(simul_t)), simul_t, color='blue',linestyle='-')
        hnd_i, = ax.plot(np.arange(j, j + len(first_arrival_initial)), first_arrival_initial, color='red',
                         linestyle='--', alpha=0.8)

        ax.axvspan(j, j + len(first_arrival_initial), color=colors[bn], alpha=0.1)
        ax.text(j + len(first_arrival_initial)/4, 80, bn, bbox=dict(edgecolor=colors[bn], linewidth=1, facecolor='white'))
        j += len(first_arrival_initial) #+ 10
        xticks.append(j)

    if i != 0:
        optimized_positions = iv['pos'].to_numpy().tolist()
        hnd_o = [ax.axvline(x=pos, color='gray', linestyle=':') for pos in optimized_positions]

        ax1 = ax.twiny()
        ax1.set_xlabel('Optimized position Source-Receiver pair (ID number)')
        start, end = 0, len(inv.measured_travel_time)
        ax1.set_xlim(start, end)
        ax1.set_xticks(optimized_positions)
        ax1.tick_params(axis='both', which='both', direction='out', length=5)
        handles = [hnd_m, hnd_i, hnd_s, hnd_o]
        labels = ['Field data', 'Initial model', 'Simulated data', 'Optimized CP']

    else:
        handles = [hnd_m, hnd_i]
        labels = ['Field data', 'Initial model']

    ax.legend(handles,
              labels,
              loc='lower right',
              frameon=True,
              fontsize=20)
    # ax.grid()
    ax.set_xlabel('Source-Receiver pair (ID number)')
    ax.set_ylabel('Two way travel time (ns)')
    ax.set_xlim(0,1200)
    ax.invert_yaxis()
    ax.set_xticks(xticks, )
    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(f'Initial RMS error: {np.sqrt(np.square(inv.measured_travel_time - sall).mean()):.2f} ns', fontsize=20)
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(20)
    if i != 0:
        for item in ([ax1.xaxis.label] + ax1.get_xticklabels()):
            item.set_fontsize(20)
    fig.tight_layout()
    if i == 0:
        fig.savefig(odir+'figures/initial_simulation.pdf')
    fig.show()
    break





















#%% Simulation:

frac = FractureGeo()
try:
    # frac.load_hdf5(filename.split('.')[0]+'_energy.h5')
    frac.load_hdf5(filename)
except:
    # inverted_points_selected = pd.read_csv(odir + 'inverted_points_selected2.csv')
    # frac.init_geo_model(extent=extent, resolution=resolution)
    # v, _ = inv.frac.create_gempy_geometry(fixed_points=inverted_points_selected,
    #                                   fixed_orientations=df_orientations,
    #                                   move_points=None)
    # v = 0.1234  # m/ns -> Velocity in granite
    # wavelength = v / sou.peak_frequency  # m
    # max_element_size = wavelength / 5
    # # max_element_size = 1
    # grid_inverse, vertices_inverse, faces_inverse = inv.frac.remesh(latest['vertices'], max_element_size=max_element_size, extrapolate=False, plane=1)
    #
    kwargs_fracture_properties = dict(aperture=0.1,
                                      electrical_conductivity=0,
                                      electrical_permeability=81)

    # dfinverted = frac.set_fracture('Inverted', vertices=vertices_inverse,
    #                                 faces=faces_inverse, overwrite=True, **kwargs_fracture_properties)
    dfinverted = frac.set_fracture('Inverted', vertices=latest['vertices'],
                                    faces=latest['faces'], overwrite=True, **kwargs_fracture_properties)
    frac.export_to_hdf5(filename.split('.')[0]+'_energy.h5', overwrite=True)
    frac.export_to_hdf5(filename, overwrite=True)

#%% Just look at MB1
ant = Antenna()
downsample = 10
try:
    ant.load_hdf5(filename)
except:
    boreholes = ['MB1']
    bor_dat = data.BoreholeData()
    bor_geom = geometry.BoreholeGeometry(borehole_data=bor_dat)

    bh_data_all = {bn: bor_dat.get_borehole_data(borehole_name=bn,
                                                 columns=['Depth (m)',
                                                          'Easting (m)',
                                                          'Northing (m)',
                                                          'Elevation (m)',
                                                          ])
                   for bn in boreholes}
    bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
                                                            radius=1)
                   for bn, info in bh_data_all.items()}

    separation = 2.77

    depths_Tx = dict()
    depths_Rx = dict()
    orientations_Tx = dict()
    orientations_Rx = dict()

    # for bor in picks.keys():

    # ant.export_to_hdf5(file, overwrite=True)
    for bn in boreholes:  # Iterate over boreholes
        depths =  np.arange(0,150,0.15)#e
        depth_r0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5)).T
        depth_r1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths-(separation*0.5) + 0.1).T
        orient_r = (depth_r0 - depth_r1) / np.linalg.norm(depth_r0 - depth_r1, axis=1)[:, None]

        depth_t0 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths+(separation*0.5)).T
        depth_t1 = bor_dat.get_coordinates_from_borehole_depth(bh_data_all[bn], depths +(separation*0.5)+ 0.1).T
        orient_t = (depth_t0 - depth_t1) / np.linalg.norm(depth_t0 - depth_t1, axis=1)[:, None]

        ant.set_profile(bn,
                        receivers=depth_r0,
                        transmitters=depth_t0,
                        orient_receivers=orient_r,
                        orient_transmitters=orient_t,
                        depth_Rx=depths-(separation*0.5),
                        depth_Tx=depths+(separation*0.5))

    ant.export_to_hdf5(filename, overwrite=True)


#%%
solv = FracEM()
try:
    solv.load_hdf5(filename.split('.')[0]+'_energy.h5')
    solv.load_hdf5(filename)
    freq = solv.get_freq()
except:
    solv.rock_epsilon = 5.9
    solv.rock_sigma = 0.0
    # solv.backend = 'torch'
    solv.backend = 'cupy'
    solv.engine='tensor_trace'
    solv.open_file(filename.split('.')[0]+'_energy.h5')
    _=solv.time_zero_correction()
    # solv.solve_for_profile = 'MB1'
    req = solv.forward_pass()

#%% Simulation to find all fracture field, approximately (use less frequencies)
# from fracwave.solvers.fracEM.tensor_element_solver_gnloop import get_mask_frequencies
#
# mas_freq = get_mask_frequencies(solv.file_read('source/source'),
#                                 threshold=0.98,
#                                 levels=4)
# solv.mode = 'incoming_field'
# solv._fast_calculation_incoming_field = True
# freq = solv.forward_pass()#mask_frequencies=mas_freq)
# ax = sou.plot()
# ax.plot(solv.file_read('source/frequency')[mas_freq], np.abs(solv.file_read('source/source')[mas_freq]), '.r')
# plt.show()
#
# #%%
# solv.load_hdf5(filename.split('.')[0]+'_energy.h5')
# fracture_field = np.load(filename.split('.')[0]+'_energy_incoming_field.npy')
# fracture_field = solv.file_read('fracture_field')

# #%%
# p = model.ModelPlot()
# #%%
# def project_planes2(mesh, name, p, **kwargs):
#     mesh = mesh.extract_geometry()
#     projectedxy = mesh.project_points_to_plane(origin=(0, 0, p.bounds[4]), normal=(0, 0, 1))
#     p.add_object(projectedxy, name=f'{name}_xy', **kwargs)
#
#     projectedxz = mesh.project_points_to_plane(origin=(0, p.bounds[2], 0), normal=(0, 1, 0))
#     p.add_object(projectedxz, name=f'{name}_xz', **kwargs)
#
#     projectedyz = mesh.project_points_to_plane(origin=(p.bounds[1], 0, 0), normal=(1, 0, 0))
#     p.add_object(projectedyz, name=f'{name}_yz', **kwargs)
#
# ant.load_hdf5(filename.split('.')[0]+"_energy.h5")
# mask_mb1 = ant.profiles.loc[ant.profiles['profile']=='MB1'].index.to_numpy()
#
# p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# project_planes2(tunnel_mesh, f'TM', p, opacity=0.01, color='black')
#
# grid2 = frac._create_mesh(frac.vertices, frac.faces)
# # grid2['Energy'] = fracture_field.max(axis=0)
# grid2['Energy'] = fracture_field[mask_mb1].max(axis=0)
# # grid2['Energy'] = fracture_field[200]
# # grid2['Energy'] = (fracture_field / fracture_field.max(axis=1)[:,None]).max(axis=0)
# # grid2['Energy'] = (fracture_field[mask_mb1] / fracture_field[mask_mb1].max(axis=1)[:, None]).max(axis=0)
# grid2['Energy'] /= grid2['Energy'].max()
# p.add_object(grid2, name='mesh', scalars='Energy', cmap='inferno', clim=(0,0.3))
#
# p.remove_scalar_bar()
# p.plotter.add_scalar_bar(title=f'Normalized energy at fracture surface (-)', title_font_size=40,
#                              label_font_size=40, vertical=False,
#                              position_x=0.3, position_y=0
#                          )
# project_planes2(grid2, name='grid', p=p, opacity=0.3, color='gray', show_scalar_bar=False)
#
# for bn in ['MB1']:
#     ant_pos = ant.profiles.loc[ant.profiles['profile'] == bn][['Tx', 'Ty', 'Tz']].to_numpy()
#     ant_pos_rx = ant.profiles.loc[ant.profiles['profile'] == bn][['Rx', 'Ry', 'Rz']].to_numpy()
#     points[bn] = ant_pos[0]
#     from bedretto.core.common_helpers import tube_from_points
#
#     bh_tube = tube_from_points(ant_pos, 0.8)
#
#     p.add_object(bh_tube, name=f'borehole_{bn}', color=colors[bn])  # if bn != 'MB1' else 'red')
#
#     project_planes2(bh_tube, f'pb_{bn}', p, opacity=0.1, color=colors[bn])  # if bn != 'MB1' else 'red')
#
#     # p.add_3d_label(points=ant_pos[0],
#     #                labels=bn,
#     #                name=f'label_boreholes_{bn}',
#     #                c=colors[bn],  # if labl != 'MB1' else 'red',
#     #                offset=offset[bn],
#     #                s=8,
#     #                rotate_dip=0,
#     #                )
#     p.add_label(points=[ant_pos[0]],
#                    labels=[bn],
#                    name=f'label_boreholes_{bn}',
#                 font_size=45)
#
#     min_distance_t, min_position_t = frac.get_position_reflection_point(
#         # element_positions=np.mean(vertices[faces], axis=1),
#         element_positions=np.mean(frac.vertices[frac.faces], axis=1),
#         tx=ant_pos,
#         rx=ant_pos_rx)
#     # travel_time[bn] = min_distance_t / solv.velocity
#     p.add_object(min_position_t, color=colors[bn], name=f'reflection_point_{bn}', opacity=0.5, point_size=3)
#
# p.show(yzgrid2=True, zxgrid=True, zxgrid2=False, number_of_divisions=10, zshift_along_y=1, zshift_along_x=1, zaxis_rotation= -120,
#        ztitle_rotation= 0, zlabel_rotation=0, xshift_along_y=1, xaxis_rotation= 0, xtitle_rotation=180, ytitle_offset=0.03,
#        xlabel_rotation=180)
#
#
# p.plotter.add_axes(box=True)
# p.plotter.window_size = (2200, 2150)
# p.plotter.camera_position =[(-423.96198171916416, 279.51548412422903, 1543.3618486432194),
#  (-45.16658673349831, -11.29116396536449, 1378.296033843515),
#  (0.3062642298496018, -0.1358753875699737, 0.9421996076030493)]
#
# p.snapshot(f'figures/energy_field_MB1.pdf', save_dir=odir)

#%%
file = 'MB1_100MHz_200515.mat'
from gdp import DATA_DIR as gdpdat
mat = io.loadmat(gdpdat + 'VALTER/' + file)
depth = mat['depth'][:, 0]
processed_data = mat['processed_data']
raw_data = mat['raw_data']
travel_time = mat['travel_time'][0]
radius = mat['radius'][0]

#Average trace amplitude
from gdp.processing.filtering import detrend, filter_data
from gdp.plotting.plot import plot_frequency_information
raw_data2 = detrend(raw_data)
dt = travel_time[-1] - travel_time[-2]
raw_data_ = raw_data2[100]/raw_data2[100].max()
plot_frequency_information(raw_data_[300:800], dt=dt)
raw_data3 = filter_data(raw_data2, fq =() ,sfreq=1/(dt))
raw_data3 = raw_data2
# raw_data3 = np.divide(raw_data2, np.abs(raw_data2).max())
# raw_data3 = np.divide(raw_data2, np.abs(raw_data2).max(axis=0))
fig, (ax1,ax2) = plt.subplots(1,2)
ax1.imshow(raw_data3, aspect='auto')
# ax2.plot(np.abs(raw_data2).sum(axis=1), np.arange(raw_data2.shape[0]))
ax2.plot(np.average((raw_data3)**2, axis=1), np.arange(raw_data3.shape[0]))
ax2.invert_yaxis()
# ax2.set_xscale('log')
ax1.set_ylim(1500,0)
ax2.set_ylim(1500,0)
plt.show()

fig, (ax1,ax2) = plt.subplots(1,2)
ax1.imshow(processed_data, aspect='auto')
# ax2.plot(np.abs(raw_data2).sum(axis=1), np.arange(raw_data2.shape[0]))
ax2.plot(np.average(abs(processed_data), axis=1), np.arange(processed_data.shape[0]))
ax2.invert_yaxis()
# ax2.set_xscale('log')
ax1.set_ylim(1500,0)
ax2.set_ylim(1500,0)
plt.show()

processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)

# Define a time window for the signal portion
signal_start = 200
signal_end = 300

# Define a time window for the noise portion
noise_start = 1300
noise_end = 1400

# Extract the signal portion
signal = raw_data[signal_start:signal_end, 1000:]

# Extract the noise portion
noise = raw_data[noise_start:noise_end, 1000:]

# Calculate the signal power
signal_power = np.mean(signal ** 2)

# Calculate the noise power
noise_power = np.mean(noise ** 2)

# Calculate the SNR
snr = 10 * np.log10(signal_power / noise_power)

fig, (ax1,ax2) = plt.subplots(1,2)
a1=ax1.imshow(signal, aspect='auto', extent =(0, signal.shape[1], signal_start, signal_end ))
fig.colorbar(a1, ax=ax1)
a2 = ax2.imshow(noise, aspect='auto', extent =(0, noise.shape[1], noise_start, noise_end))
fig.colorbar(a2, ax=ax2)
plt.show()


# signal_power = np.sum(processed_data**2) / (processed_data.shape[0] * processed_data.shape[1])
# subset_of_noise = [1100, 1400, 5200, 5900]
# subset = processed_data[subset_of_noise[0]:subset_of_noise[1], subset_of_noise[2]:subset_of_noise[3]]
subset = processed_data[noise_start:noise_end, 1000:]
noise_std = np.std(subset)

# noise_power = np.sum(subset**2) / (subset.shape[0]*subset.shape[1])
# snr = 20*np.log10((signal_power-noise_power)/noise_power)

#
# noise_power = np.sum(subset**2, axis=0) / subset.shape[0]
# noise_signal_power = np.sum(noise_power)
# snr = 20*np.log10((signal_power-noise_signal_power)/noise_signal_power)
# snr = 20*np.log10(signal_power/noise_std)
#
# snr = 20*np.log10(abs(processed_data.mean()/processed_data.std()))
# processed_data
# histogram, bin_edges = np.histogram(subset, bins=100, )
# hist_distribution = rv_histogram((histogram, bin_edges))

# noise = hist_distribution.rvs(size=tr.shape)

#
# snr = 20*np.log10(signal_power/noise_std)
# snr = 20*np.log10(abs(np.where(noise_std == 0, 0, signal_power/noise_std)))
print(f'The data have SNR of {snr:.2f} dB')

from scipy.stats import rv_histogram
#%%
# fig = plt.figure(constrained_layout=True, figsize=(12,8))
# subfig = fig.subfigures(nrows=1, ncols=1)
# AX = subfig.subplots(nrows=1, ncols=2, sharey=True)
# extent = (depth[0], depth[-1], radius[-1], radius[0])
# processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
#                                out=np.zeros_like(processed_data),
#                                where=np.abs(processed_data).max(axis=1)[:, None] != 0)
#
# AX[0].imshow(processed_data, extent=extent, cmap='seismic', aspect='auto', vmax=1, vmin=-1)
#
# time_response, time_vector = solv.get_ifft(freq)
# time_response[np.isnan(time_response)] = 0
# from gdp.processing.gain import apply_gain
# tri, gm= apply_gain(data=time_response.T,
#            sfreq=1/(time_vector[1] - time_vector[0]),
#            gain_type='spherical',
#            exponent= 2,
#            velocity=0.1234,
#            twoway= True)
# tri /= np.abs(tri).max()
#
#
# # Calculate the power of each trace
# trace_power = np.sum(tri**2, axis=0) / tri.shape[0]
# # Sum the powers of all the traces
# signal_power = np.sum(trace_power)
#
# desired_snr = 30
#
# noise_power = signal_power / (10 ** (desired_snr / 10))
#
# noise = np.random.normal(0, np.sqrt(noise_power), size=tri.shape)
#
# # tr += noise*0.3
# tr = tri + noise
# tr /= np.abs(tr).max()
# extent = (0, 150, time_vector[-1] *0.1234*0.5, time_vector[0] *0.1234*0.5)
#
# cax = AX[1].imshow(tr, aspect='auto', cmap='seismic', vmax=1, vmin=-1, extent=extent)
# # ax.plot(coordinates['MB1'][:,0], coordinates['MB1'][:,1] * 0.1234 / 2, '.' )
# for i, (ax, na) in enumerate(zip(AX,
#     ['MB1 field data', 'MB1 simulated data'],
#     # [(travel_time[0], travel_time[-1]),(time_vector[0], time_vector[-1])]
#                       )):
#     ax.set_xlabel('Borehole depth (m)')
#     ax.set_ylabel('Radial distance (m)') if i ==0 else None
#     ax.set_xlim(15, 145)
#     ax.set_ylim(55, 0)
#
#     ax1 = ax.twinx()
#     start, end = 0,55
#     ax1.set_ylim(end, start)
#     ax1.tick_params(axis='both', which='both', direction='out', length=5)
#     if i == 1:
#         ax1.yaxis.set_ticks(np.arange(0, 56, 5) * 2 / 0.1234)
#         ax1.set_ylabel('Two-way travel time (ns)')
#     else:
#         ax1.yaxis.set_ticks(np.arange(0, 56, 5) * 2 / 0.1234, labels=[])
#
#     ax.yaxis.set_ticks(np.arange(0, 56, 5))
#     ax.xaxis.set_ticks(np.arange(15, 145, 15))
#
#     ax.tick_params(axis='both', which='both', direction='out', length=5)
#     ax.set_title(na)
#     ax.grid(linestyle='--')
#     for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
#                  ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
#         item.set_fontsize(15)
#
# subfig.colorbar(cax, ax=AX, label='Normalized GPR Amplitude (-)', orientation='horizontal')
#
# fig.savefig(odir+'figures/real_boreholes_simulation.pdf')
# plt.show()



#%% Pick the image to have a plot and compare
from gdp.processing.image_processing import pick_first_arrival, transform_coordinates

pick = pick_first_arrival(tri, threshold=0.1)

picks_real = np.asarray([transform_coordinates(a[0], a[1], tri.shape, extent = extent) for a in pick]   )

xlim = (15, 145)
ylim = (0, 55)
plt.imshow(tri, aspect='auto', extent=extent)
plt.plot(picks_real[:,0], picks_real[:,1])
plt.xlim(xlim)
plt.ylim(max(ylim), min(ylim))
plt.show()

# Convert to normal coordinates





#%%
# fig, ax = plt.subplots(  figsize=(7,8))
#
# radial_distance = time_vector * solv.velocity / 2
#
# ax.set_xlabel('Borehole depth (m)')
# ax.set_ylabel('Radial distance (m)')
#
# ylims = (0,55)
# xlims = (15, 145)
#
# ax1 = ax.twinx()
# ax1.set_ylabel('Two-way travel time (ns)')
# start, end = max(ylims) * 2 / solv.velocity, min(ylims) * 2 / solv.velocity
# ax1.set_ylim(start, end)
#
#
# ax.plot(mean_val['MB1'][:, 0] , mean_val['MB1'][:, 1]*solv.velocity*0.5, 'g--', label='Field data')
#
# ax.fill_between(mean_val['MB1'][:,0], all_coordinates['MB1'][:, 0] *solv.velocity*0.5 , all_coordinates['MB1'][:, 1] *solv.velocity*0.5, color='red', alpha=0.9,
#                 label='Uncertainty')
# ax.plot(picks_real[:,0], picks_real[:,1] , '--', label='Simulated values', color='blue')
#
# ax.set_ylim(max(ylims), min(ylims))
# ax.set_xlim(min(xlims), max(xlims))
#
# ax.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 5))
# ax.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))
# ax1.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 5) * 2 / solv.velocity)
#
# ax.tick_params(axis='both', which='both', direction='out', length=5)
# ax1.tick_params(axis='both', which='both', direction='out', length=5)
# # ax.set_title()
# ax.grid(linestyle='--')
# ax.legend(frameon=True)
# ax.set_title('MB1 Travel-time comparison')
# for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
#              ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
#     item.set_fontsize(15)
# fig.tight_layout()
#
# fig.show()
# fig.savefig(odir+'figures/real_boreholes_picks_comparison.pdf')
#
# #%%
# fig, ax = plt.subplots( figsize=(10,7))
#
# radial_distance = time_vector * solv.velocity / 2
# mean = mean_val['MB1'][:,1] * solv.velocity / 2
# resampled_picks = np.interp(mean_val['MB1'][:,0], picks_real[:,0], picks_real[:,1])
#
# ax.set_xlabel('Borehole depth (m)')
# ax.set_ylabel('(Field  - Synthetic) distance (m)')
#
# ax.plot(mean_val['MB1'][:,0], mean_val['MB1'][:,1]*solv.velocity*0.5-resampled_picks, linestyle=':', color='blue', label='Simulated - Field')
# ax.fill_between(mean_val['MB1'][:,0], all_coordinates['MB1'][:, 0] *solv.velocity*0.5 - mean , all_coordinates['MB1'][:, 1] *solv.velocity*0.5 - mean, color='red', alpha=0.3,
#                 label='Uncertainty')
# ax.axhline(0, linestyle='--', color='green',)
#
# ax.grid(linestyle='--')
# ax.legend(frameon=True)
# ax.set_title('MB1 Travel-time comparison')
# ylims = -1.7, 1.04
# ax1 = ax.twinx()
# ax1.set_ylabel('(Field  - Synthetic) time (ns)')
# start, end = min(ylims) * 2 / solv.velocity, max(ylims) * 2 / solv.velocity
# ax1.set_ylim(start, end)
#
#
# ax.set_ylim(ylims)
# ax.yaxis.set_ticks(np.arange(-1.5, 1.1, 0.5))
# ax1.yaxis.set_ticks(np.arange(-1.5, 1.1, 0.5) * 2 / solv.velocity)
# ax.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))
#
# ax.tick_params(axis='both', which='both', direction='out', length=5)
# ax1.tick_params(axis='both', which='both', direction='out', length=5)
#
#
# for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
#              ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
#     item.set_fontsize(15)
# fig.tight_layout()
#
# fig.show()
# # fig.savefig(odir+'figures/real_boreholes_picks_comparison.pdf')

#%%
# fig, (ax, ax2) = plt.subplots(2,1, figsize=(7,11), gridspec_kw={'height_ratios': [4, 1]}, sharex=True)
#
# radial_distance = time_vector * solv.velocity / 2
#
# # ax.set_xlabel('Borehole depth (m)')
# ax.set_ylabel('Radial distance (m)')
#
# ylims = (0, 55)
# xlims = (15, 140)
#
# ax1 = ax.twinx()
# ax1.set_ylabel('Two-way travel time (ns)')
# start, end = max(ylims) * 2 / solv.velocity, min(ylims) * 2 / solv.velocity
# ax1.set_ylim(start, end)
#
#
# ax.plot(mean_val['MB1'][:, 0] , mean_val['MB1'][:, 1]*solv.velocity*0.5, 'g--',
#         label='$P_m$: Mean picks from field data')
#
# ax.fill_between(mean_val['MB1'][:,0], all_coordinates['MB1'][:, 0] *solv.velocity*0.5 , all_coordinates['MB1'][:, 1] *solv.velocity*0.5, color='red', alpha=0.3,
#                 label='$\delta$: Picking uncertainty')
# ax.plot(picks_real[:,0], picks_real[:,1] , '--', label='$P_s$: Picks from simulated data', color='blue')
#
# ax.set_ylim(max(ylims), min(ylims))
# ax.set_xlim(min(xlims), max(xlims))
#
# ax.yaxis.set_ticks(np.arange(min(ylims), max(ylims)+1, 5))
# ax.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))
# ax1.yaxis.set_ticks(np.arange(min(ylims), max(ylims)+1, 5) * 2 / solv.velocity)
#
# ax.tick_params(axis='both', which='both', direction='out', length=5)
# ax1.tick_params(axis='both', which='both', direction='out', length=5)
# # ax.set_title()
# ax.grid(linestyle='--')
# ax.legend(frameon=True, loc='lower right')
# ax.set_title('MB1 Travel-time comparison')
# for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
#              ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
#     item.set_fontsize(15)
#
# ############ radial_distance = time_vector * solv.velocity / 2
# mean = mean_val['MB1'][:,1] * solv.velocity / 2
# resampled_picks = np.interp(mean_val['MB1'][:,0], picks_real[:,0], picks_real[:,1])
#
# ax2.set_xlabel('Borehole depth (m)')
# ax2.set_ylabel('$\Delta$ distance (m)')
#
# ax2.plot(mean_val['MB1'][:,0], mean_val['MB1'][:,1]*solv.velocity*0.5-resampled_picks,
#          linestyle=':', color='purple', label='$P_m - P_s$')
# ax2.fill_between(mean_val['MB1'][:,0], all_coordinates['MB1'][:, 0] *solv.velocity*0.5 - mean , all_coordinates['MB1'][:, 1] *solv.velocity*0.5 - mean,
#                  color='gray', alpha=0.3, label='$P_m - \delta$')
# ax2.axhline(0, linestyle=':', color='black',)
#
# ax2.grid(linestyle='--')
# ax2.legend(frameon=True)
# # ax2.set_title('MB1 Travel-time comparison')
# ylims = -1.7, 1
# ax3 = ax2.twinx()
# ax3.set_ylabel('$\Delta$ time (ns)')
# start, end = min(ylims) * 2 / solv.velocity, max(ylims) * 2 / solv.velocity
# ax3.set_ylim(start, end)
# ax2.set_ylim(ylims)
# # ax2.set_ylim(xlims)
# ax2.yaxis.set_ticks(np.arange(-1.5, 1.1, 0.5))
# ax3.yaxis.set_ticks(np.arange(-1.5, 1.1, 0.5) * 2 / solv.velocity)
# ax2.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))
#
# ax2.tick_params(axis='both', which='both', direction='out', length=5)
# ax3.tick_params(axis='both', which='both', direction='out', length=5)
#
#
# for item in ([ax2.title, ax2.xaxis.label, ax2.yaxis.label] +
#              ax2.get_xticklabels() + ax2.get_yticklabels() + [ax3.yaxis.label] + ax3.get_yticklabels()):
#     item.set_fontsize(15)
# fig.tight_layout()
#
# fig.show()
# fig.savefig(odir+'figures/real_boreholes_picks_comparison.pdf')


#%%%%%%%%
fig = plt.figure(constrained_layout=True, figsize=(19,9))
subfig1, subfig2 = fig.subfigures(nrows=1, ncols=2, width_ratios=[2, 1])
# fig, (ax, ax2) = plt.subplots(2,1, figsize=(7,11), gridspec_kw={'height_ratios': [4, 1]}, sharex=True)

AX = subfig1.subplots(nrows=1, ncols=2, sharey=True)
extent = (depth[0], depth[-1], radius[-1], radius[0])
processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)

AX[0].imshow(processed_data, extent=extent, cmap='seismic', aspect='auto', vmax=1, vmin=-1)

freq = solv.get_freq()
time_response, time_vector = solv.get_ifft(freq)
time_response[np.isnan(time_response)] = 0
from gdp.processing.gain import apply_gain
tri, gm= apply_gain(data=time_response.T,
           sfreq=1/(time_vector[1] - time_vector[0]),
           gain_type='spherical',
           exponent= 2,
           velocity=0.1234,
           twoway= True)
tri /= np.abs(tri).max()


# Calculate the power of each trace
trace_power = np.sum(tri**2, axis=0) / tri.shape[0]
# Sum the powers of all the traces
signal_power = np.sum(trace_power)

desired_snr = 30

noise_power = signal_power / (10 ** (desired_snr / 10))

noise = np.random.normal(0, np.sqrt(noise_power), size=tri.shape)

# tr += noise*0.3
tr = tri + noise
tr /= np.abs(tr).max()
extent = (0, 150, time_vector[-1] *0.1234*0.5, time_vector[0] *0.1234*0.5)

cax = AX[1].imshow(tr, aspect='auto', cmap='seismic', vmax=1, vmin=-1, extent=extent)
# ax.plot(coordinates['MB1'][:,0], coordinates['MB1'][:,1] * 0.1234 / 2, '.' )
for i, (ax, na) in enumerate(zip(AX,
    ['MB1 field data', 'MB1 simulated data'],
    # [(travel_time[0], travel_time[-1]),(time_vector[0], time_vector[-1])]
                      )):
    ax.set_xlabel('Borehole depth (m)')
    ax.set_ylabel('Radial distance (m)') if i ==0 else None
    ax.set_xlim(15, 145)
    ax.set_ylim(55, 0)

    ax1 = ax.twinx()
    start, end = 0,55
    ax1.set_ylim(end, start)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    if i == 1:
        ax1.yaxis.set_ticks(np.arange(0, 56, 5) * 2 / 0.1234)
        ax1.set_ylabel('Two-way travel-time (ns)')
    else:
        ax1.yaxis.set_ticks(np.arange(0, 56, 5) * 2 / 0.1234, labels=[])

    ax.yaxis.set_ticks(np.arange(0, 56, 5))
    ax.xaxis.set_ticks(np.arange(15, 145, 15))

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(na)
    ax.grid(linestyle='--')
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
        item.set_fontsize(15)

subfig1.colorbar(cax, ax=AX, label='Normalized GPR Amplitude (-)', orientation='horizontal')

###########
# fig, (ax, ax2) = plt.subplots(2,1, figsize=(7,11), gridspec_kw={'height_ratios': [4, 1]}, sharex=True)

(ax, ax2) = subfig2.subplots(nrows=2, ncols=1, gridspec_kw={'height_ratios': [2.5, 1]}, sharex=True)

radial_distance = time_vector * solv.velocity / 2

# ax.set_xlabel('Borehole depth (m)')
ax.set_ylabel('Radial distance (m)')

ylims = (0, 55)
xlims = (15, 140)

ax1 = ax.twinx()
ax1.set_ylabel('Two-way travel-time (ns)')
start, end = max(ylims) * 2 / solv.velocity, min(ylims) * 2 / solv.velocity
ax1.set_ylim(start, end)


ax.plot(mean_val['MB1'][:, 0] , mean_val['MB1'][:, 1]*solv.velocity*0.5, 'g--',
        label='$P_m$: Mean picks from field data')

ax.fill_between(mean_val['MB1'][:,0], all_coordinates['MB1'][:, 0] *solv.velocity*0.5 , all_coordinates['MB1'][:, 1] *solv.velocity*0.5, color='red', alpha=0.3,
                label='$\delta$: Picking uncertainty')
ax.plot(picks_real[:,0], picks_real[:,1] , '--', label='$P_s$: Picks from simulated data', color='blue')

ax.set_ylim(max(ylims), min(ylims))
ax.set_xlim(min(xlims), max(xlims))

ax.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 5))
ax.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))
ax1.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 5) * 2 / solv.velocity)

ax.tick_params(axis='both', which='both', direction='out', length=5)
ax1.tick_params(axis='both', which='both', direction='out', length=5)
# ax.set_title()
ax.grid(linestyle='--')
ax.legend(frameon=True, loc='lower right')
ax.set_title('MB1 Travel-time comparison')
for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels() + [ax1.yaxis.label] + ax1.get_yticklabels()):
    item.set_fontsize(15)

############ radial_distance = time_vector * solv.velocity / 2
mean = mean_val['MB1'][:,1] * solv.velocity / 2
resampled_picks = np.interp(mean_val['MB1'][:,0], picks_real[:,0], picks_real[:,1])

ax2.set_xlabel('Borehole depth (m)')
ax2.set_ylabel('$\Delta$ distance (m)')

ax2.plot(mean_val['MB1'][:,0], mean-resampled_picks,
         linestyle=':', color='purple', label='$P_m - P_s$')
ax2.fill_between(mean_val['MB1'][:,0],
                 mean - all_coordinates['MB1'][:, 0] *solv.velocity*0.5,
                 mean - all_coordinates['MB1'][:, 1] *solv.velocity*0.5,
                 color='gray', alpha=0.3, label='$P_m - \delta$')
# ax2.axhline(0, linestyle=':', color='black',)

ax2.grid(linestyle='--')
ax2.legend(frameon=True, loc='lower left')
# ax2.set_title('MB1 Travel-time comparison')
ylims = -1.7, 1.5
ax3 = ax2.twinx()
ax3.set_ylabel('$\Delta$ time (ns)')
start, end = min(ylims) * 2 / solv.velocity, max(ylims) * 2 / solv.velocity
ax3.set_ylim(start, end)
ax2.set_ylim(ylims)
# ax2.set_ylim(xlims)

ax2.yaxis.set_ticks(np.arange(-1.5, 1.6, 0.5))
ax3.yaxis.set_ticks(np.arange(-1.5, 1.6, 0.5) * 2 / solv.velocity)
ax2.xaxis.set_ticks(np.arange(min(xlims), max(xlims), 15))

ax2.tick_params(axis='both', which='both', direction='out', length=5)
ax3.tick_params(axis='both', which='both', direction='out', length=5)


for item in ([ax2.title, ax2.xaxis.label, ax2.yaxis.label] +
             ax2.get_xticklabels() + ax2.get_yticklabels() + [ax3.yaxis.label] + ax3.get_yticklabels()):
    item.set_fontsize(15)

# fig.savefig(odir+'figures/real_boreholes_simulation.pdf')
fig.savefig(odir+'figures/real_boreholes_simulation.pdf')
plt.show()



















#%%%%%%%%
#%%
from bedretto.utils.ray_path_calculation import intersection_of_surface_with_trajectory
boreholes = ['ST1',
             'ST2',
             'MB1',
             'MB2',
             'MB4',
             'MB5',
             # 'MB7', # Problematic. Don't know if I'm picking the correct reflector
             'MB8',  # Problematic. Does not converge
             'MB3'

             ]

bor_dat = data.BoreholeData()
frac = FractureGeo()
frac.load_hdf5(filename)
surf = frac.create_pyvista_mesh(frac.vertices, frac.faces)
#%%
from bedretto import DATA_DIR as dbdir
real_pos = pd.read_csv(dbdir+'logging_data/BB_logging.csv')
diff={}
table = pd.DataFrame()
for bo in boreholes:
    xyz = bor_dat.get_borehole_data(bo)
    trajectory = xyz[['Easting (m)', 'Northing (m)', 'Elevation (m)']].to_numpy()
    depth = xyz[['Depth (m)']].to_numpy()
    inter = intersection_of_surface_with_trajectory(trajectory, surf, depth=depth, reduce_meshes=True)
    interse = inter['Intersection depth (m)'][0]
    rpo = real_pos.loc[real_pos['Borehole'] == bo, ['Depth (m)']].to_numpy()
    r = np.mean(rpo)

    table = pd.concat([table, pd.DataFrame({'Depth calc (m)': interse,
                  'Depth real (m)': r,
                  'Uncert (m)': np.abs(rpo[0] - r),
                 'Diff (m)': r - interse,
                  'Borehole': bo} )])


#%%%%% Testing


import numpy as np

# Generate synthetic data
nt = 500
ntr = 50
dt = 0.002
t = np.arange(nt) * dt
data = np.zeros((nt, ntr))

# Add a synthetic signal to the data
signal = np.sin(2 * np.pi * 20 * t)
data[:, 10] = signal

# Calculate the signal power
signal_power = np.sum(signal ** 2) / nt

# Specify the desired SNR level
desired_snr = 10

# Calculate the noise power required to achieve the desired SNR level
noise_power = signal_power / (10 ** (desired_snr / 10))

# Add noise to the data with the calculated noise power
noise = np.random.normal(0, np.sqrt(noise_power), size=(nt, ntr))
noisy_data = data + noise

plt.imshow(data, aspect='auto');plt.show()
plt.imshow(noise, aspect='auto');plt.show()
plt.imshow(noisy_data, aspect='auto');plt.show()
