from bedretto import data, geometry, DATA_DIR, model
from bedretto.core.experiments import probabilities
import pandas as pd
import os
import numpy as np
from fracwave import OUTPUT_DIR, FractureGeo
import pyvista as pv
import matplotlib.pyplot as plt


file = OUTPUT_DIR + 'fracture_curvature/fracture_mesh_fault_zone.h5'

cam  = [(-396.04009744568333, -520.9151253434819, 1587.625018337407),
 (-82.49998130298371, 9.397606348938316, 1340.7179679637172),
 (0.1673222775826511, 0.3331035306123411, 0.9279252627869055)]

window_size = (1800, 2100)

#%%
borehole_data = data.BoreholeData()
bor_geom = geometry.BoreholeGeometry(borehole_data)
bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_name=bn, data_type='KDE-ML') for bn in borehole_data.boreholes_MB + borehole_data.boreholes_ST}

tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)
tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1984, 2070])

bb_data = pd.read_csv(DATA_DIR + 'logging_data' + os.sep + 'BB_logging.csv')

# initialize classes
p_samp = probabilities.ProbabilisticSampling(borehole_data)
fg = geometry.fault_geometry.FaultGeometry()

bb_top = bb_data[bb_data['fault_id'] == 'BB-top']
bb_bottom = bb_data[bb_data['fault_id'] == 'BB-bottom']

logging_weights = 1e-3  # this will put a high priority on fitting the points
orientation_weights = 1000  # and a very low (negligible) priority on fitting the orientations

# Split the dataframe into top and bottom surfaces and sample (randomly) the location of the fault
# using borehole trajectory information
logging_df = p_samp.sample_logging_structures(structures_df=bb_top,
                                              borehole_dict=borehole_data.boreholes_kde_dict)
logging_df['surface'] = 'F'
logging_df['smooth'] = logging_weights / logging_df['Weight (-)']


# create an input dataframe and feed it to gempy
input_data = {'logging': logging_df}
fields = ['logging']
surf_points_df, orientations_df = fg.create_gempy_df_from_dict(input_data, fields)
orientations_df['smooth'] = orientation_weights

bb = tun_dat.get_tunnel_fractures(structure_ID='-5')
surf_points_df = pd.concat([surf_points_df, pd.DataFrame.from_dict({'X': [bb['Center'][0][0]],
                                                                    'Y': [bb['Center'][0][1]],
                                                                    'Z': [bb['Center'][0][2]],
                                                                    'surface': 'F',
                                                                    'smooth': 0.001})], ignore_index=True)
from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip

pole = get_normal_vector_from_azimuth_and_dip(bb['Azimuth (deg)'][0], bb['Dip (deg)'][0])
orientations_df = pd.concat([orientations_df, pd.DataFrame.from_dict({'X': [bb['Center'][0][0]],
                                                                      'Y': [bb['Center'][0][1]],
                                                                      'Z': [bb['Center'][0][2]],
                                                                      'G_x': pole[0],
                                                                      'G_y': pole[1],
                                                                      'G_z': pole[2],

                                                                      'surface': 'F',
                                                                      'smooth': 1000})], ignore_index=True)


#%%
p = model.ModelPlot()

#%%
extent = [-202, 11, -100, 44, 1278, 1489]
frac = FractureGeo()
frac.init_geo_model(extent = extent, resolution=(15,15,15))

geo_model = frac.geo_model
#%%
vertices, faces = frac.create_gempy_geometry(fixed_points=surf_points_df, fixed_orientations=orientations_df)
#%%
import gempy as gp
plotter = gp.plot_3d(geo_model, plotter_type='background', show_surfaces=False,
                     )
plotter.plot_structured_grid(opacity=0.5)

actors = plotter.p.renderer._actors
p2 = model.ModelPlot()
[p2.plotter.add_actor(a) for k, a in actors.items()]
p2.plotter.camera_position = cam
p2.plotter.window_size = (1800, 1800)
p2.show(xrange=extent[0:2], yrange=extent[2:4], zrange=extent[4:6])
p2.plotter.add_axes(box=True, viewport=(0.0, 0.9, 0.1, 1))

# [p2.add_object(bn, name=key, color='black') for key, bn in bh_geom_all.items()]
# p2.add_object(tunnel_mesh, name='tunnel', color='gray', opacity=0.2)

p2.snapshot('fracture_curvature/gempy_mesh.pdf', save_dir=OUTPUT_DIR)

#%%
frac.set_fracture('frac1', vertices=vertices, faces=faces,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0)
surf = frac.get_surface('frac1')
#%%
p.add_object(surf, name='mesh', color='blue', style='wireframe')
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.snapshot('fracture_curvature/wire_mesh_irregular.pdf', save_dir=OUTPUT_DIR)


#%%
p.add_object(surf.points, name='mesh', color='blue', style='wireframe')
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.snapshot('fracture_curvature/points_mesh_irregular.pdf', save_dir=OUTPUT_DIR)

#%%
grid, vert, fac = frac.remesh(vertices, plane=1, spacing=(15,15), extrapolate=False)  # Resolution 145,143
#%%
frac2 = FractureGeo()
frac2.set_fracture('frac1', vertices=vert, faces=fac,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf = frac2.get_surface('frac1')
#%%
p.add_object(surf, name='mesh', color='blue', style='wireframe')
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.snapshot('fracture_curvature/wire_mesh_regular.pdf', save_dir=OUTPUT_DIR)

#%%
p.add_object(surf.points, name='mesh', color='blue', style='wireframe')
_ = p.plotter.add_axes(box=True, viewport=(0, 0, 0.15, 0.15))
p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.snapshot('fracture_curvature/points_mesh_regular.pdf', save_dir=OUTPUT_DIR)

#%%
plt.scatter(vertices[:,0], vertices[:,1], c=vertices[:,2], cmap='gist_earth')
plt.xlabel('Local Easting (m) = X')
plt.ylabel('Local Northing (m) = Y')
plt.title('View from axis = 2 (Top view)')
cbar = plt.colorbar()
cbar.set_label('Elevation (m) = Z')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis2_view.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vertices[:,0], vertices[:,2], c=vertices[:,1], cmap='gist_earth')
plt.xlabel('Local Easting (m) = X')
plt.ylabel('Elevation (m) = Z')
plt.title('View from axis = 1 (North view)')
cbar = plt.colorbar()
cbar.set_label('Local Northing (m) = Y')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis1_view.pdf', transparent=True)
plt.show()

#%%
plt.scatter(vertices[:,1], vertices[:,2], c=vertices[:,0], cmap='gist_earth')
plt.xlabel('Local Northing (m) = Y')
plt.ylabel('Elevation (m) = Z')
plt.title('View from axis = 0 (East view)')
cbar = plt.colorbar()
cbar.set_label('Local Easting (m) = X')
plt.tight_layout()
plt.savefig(OUTPUT_DIR+'fracture_curvature/axis0_view.pdf', transparent=True)
plt.show()
