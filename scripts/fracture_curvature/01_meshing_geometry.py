from fracwave import FracEM, FractureGeo, OUTPUT_DIR, Antenna, SourceEM
# from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
#                                                    set_fracture_solver,
#                                                    forward_operator,
#                                                    run_inversion)
from bedretto.core.vector_helpers import get_normal_vector_from_azimuth_and_dip, get_azimuth_and_dip_from_normal_vector
from bedretto import model

import pandas as pd
import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
odir = OUTPUT_DIR + 'fracture_curvature/'
file = odir + 'curved.h5'


window_size = (1900,2000)

cam  = [(-15.455137758037772, -20.148353549143433, 13.085369680465325),
 (0.5473828427661188, 0.8974323933810469, -1.5291263419702086),
 (0.26420340990556196, 0.40646584710680206, 0.8746325361716332)]
#%%
show_kwargs = dict(xtitle_offset=0.08,
                   ztitle_offset=0.04,
                   ytitle_offset=0.15,
                   xtitle='X (m)',
                   ytitle= 'Y (m)',
                   ztitle='Z (m)',
                   xrange=(-2, 3),
                   yrange=(-5,6),
                   zrange=(-6,5),
                   number_of_divisions=7,
                   yzgrid2=True,
                   xlabel_size=0.03,
                   ylabel_size=0.03,
                   zlabel_size=0.03,
                   xtitle_size=0.03,
                   ytitle_size=0.03,
                   ztitle_size=0.03
                   )

#%% Create fracture geometry
frac = FractureGeo()
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)

resolution = (15, 15, 15)
frac.init_geo_model(gempy_extent, resolution)

#%% Get the control points and orientations
max_element_size=0.1

p1 = (70, 270, 0)
# fixed = np.asarray([(0,0,p1[-1])])
fixed = np.asarray([(-0.07264459,  0.30187218,  0.04356908)])
fixed_or = [fixed.copy(), np.asarray([(p1[1],p1[0],1)])]
normal = get_normal_vector_from_azimuth_and_dip(azimuth=p1[1], dip=p1[0])
df_fixed = pd.DataFrame(fixed, columns=['X', 'Y', 'Z'])
df_orientations = pd.DataFrame(np.hstack((fixed,normal)), columns=['X','Y','Z','G_x','G_y','G_z'])

curved = np.asarray([[-0.85,  0, -3.4],
                     [-0.92,  0, -2.1],
                     [0.2, 0, 1.5],
                     [0, 0,  3],

                     [-0.5, 3, 0],
                     [1, -3, 0],
                     ])


df_curve = pd.DataFrame(curved, columns=['X','Y','Z'])

kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductivity=0.001,
                                  electrical_permeability=81)

#%% Generate the first figure. Compare gempy block model and curved initial mesh
import gempy as gp
vc, fc = frac.create_gempy_geometry(fixed_points=df_fixed,
                                   fixed_orientations=df_orientations,
                                   move_points=df_curve
                                   )
plotter = gp.plot_3d(frac.geo_model, plotter_type='background', show_surfaces=False,)
plotter.p.remove_bounds_axes()

actors = plotter.p.renderer._actors
p2 = model.ModelPlot()
[p2.plotter.add_actor(a) for k, a in actors.items()]
p2.show(**show_kwargs)
p2.plotter.add_axes(box=True, viewport=(0.11, 0.0, 0.3, 0.3))
p2.plotter.window_size = window_size#(1750, 1900)
p2.plotter.camera_position = [(-17.703100547507642, -20.13591162391876, 12.787377151965682),
 (0.2994200532962349, 0.9098743186057114, -1.8271188704698547),
 (0.22899682137241362, 0.4317228943732864, 0.8724538946415927)]
p2.snapshot('fig1_a_curved_geometry.pdf', save_dir=odir)

#%% Mesh representation
faces = np.hstack([np.hstack([len(f), f]) for f in fc])
surf =pv.PolyData(vc, faces)

p3 = model.ModelPlot()
p3.add_object(surf, name='Mesh', color='blue', style='wireframe')

# p3.add_object(surf, name='Mesh', color='blue', opacity=0.5, show_edges=True)
projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p3.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p3.add_object(projected2, name='MeshProjected2', opacity=0.2)
projected3 = surf.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1,0, 0))
p3.add_object(projected3, name='MeshProjected3', opacity=0.2)
show_kwargs['yzgrid2']=True
show_kwargs['xrange']=(-2, gempy_extent[1] + 1)
p3.show(**show_kwargs)
p3.plotter.add_axes(box=True)#, viewport=(0.1, 0.05, 0.3, 0.3))

# Add projected control points to planes
po = pv.PolyData(np.vstack((df_fixed.to_numpy(), df_curve.to_numpy())))
p3.add_object(po, name='Points', point_size=20, render_points_as_spheres=True, color='red')
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p3.add_object(projectedxy, name='PointProjected', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p3.add_object(projectedz, name='PointProjected2', point_size=10)
projectedy = po.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1, 0, 0))
p3.add_object(projectedy, name='PointProjected3', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction=normal)
p3.add_object(arrow, name='arrow', color='blue')

p3.plotter.window_size = (1700, 2000)
p3.plotter.camera_position = [(-16.669616901789219, -20.779014374946655, 13.03730513847674),
 (1.3329036990146672, 0.2667715675778106, -1.5771908839588005),
 (0.22249978799983883, 0.4362622551670062, 0.871876762539652)]
p3.snapshot('fig1_b_wire_mesh_irregular_curved.pdf', save_dir=odir)


#%% Remeshed structure
max_element_size = 0.4
grid_curve, vertices_curve, faces_curve = frac.remesh(vc, max_element_size=max_element_size, extrapolate=False, plane=0)
frac.set_fracture('frac1', vertices=vertices_curve, faces=faces_curve,
                  aperture=0, electrical_conductivity=0, electrical_permeability=0, overwrite=True)
surf = frac.get_surface('frac1')
p4 = model.ModelPlot()
p4.add_object(surf, name='Mesh', color='blue', style='wireframe')
# p4.add_object(surf, name='Mesh', color='blue', opacity=0.5, show_edges=True)
projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p4.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p4.add_object(projected2, name='MeshProjected2', opacity=0.2)
projected3 = surf.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1,0, 0))
p4.add_object(projected3, name='MeshProjected3', opacity=0.2)
show_kwargs['yzgrid2']=True
show_kwargs['xrange']=(-2, gempy_extent[1] + 1)
p4.show(**show_kwargs)
# p4.show(yzgrid2=True, xrange=(-2, gempy_extent[1] + 1))
p4.plotter.add_axes(box=True)#, viewport=(0.1, 0.05, 0.3, 0.3))


# Add projected control points to planes
po = pv.PolyData(np.vstack((df_fixed.to_numpy(), df_curve.to_numpy())))
p4.add_object(po, name='Points', point_size=20, render_points_as_spheres=True, color='red')
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p4.add_object(projectedxy, name='PointProjected', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p4.add_object(projectedz, name='PointProjected2', point_size=10)
projectedy = po.project_points_to_plane(origin=(gempy_extent[1] + 1, 0, 0), normal=(1, 0, 0))
p4.add_object(projectedy, name='PointProjected3', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction=normal)
p4.add_object(arrow, name='arrow', color='blue')

p4.plotter.window_size = (1700, 2000)#(1750, 1900)
p4.plotter.camera_position = [(-16.669616901789219, -20.779014374946655, 13.03730513847674),
 (1.3329036990146672, 0.2667715675778106, -1.5771908839588005),
 (0.22249978799983883, 0.4362622551670062, 0.871876762539652)]
p4.snapshot('fig1_c_wire_mesh_regular_curved.pdf', save_dir=odir)
