from fracwave import FracEM, FractureGeo, OUTPUT_DIR, Antenna, SourceEM
# from fracwave.inversion.geometry_inversion import (set_fracture_geometry,
#                                                    set_fracture_solver,
#                                                    run_inversion)
from bedretto.core.vector_helpers import get_normal_vector_from_azimuth_and_dip, get_azimuth_and_dip_from_normal_vector
from bedretto import model
import pandas as pd
import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
import scipy.io as io
import pickle

kwargs_fracture_properties = dict(aperture=0.0005,
                                  electrical_conductivity=0.001,
                                  electrical_permeability=81)


odir = OUTPUT_DIR + 'fracture_curvature/synthetic_inversion_2boreholes/'
file_initial = odir + 'inital_plane_inversion.h5'
file_real = odir + 'real_synthetic_inversion.h5'
file_inverted = odir + 'inverted_synthetic_inversion.h5'

window_size = (1850, 2000)

cam  = [(-19.114921767863798, -21.93010099492415, 3.3686085978532727),
 (-1.2341764552332481, 1.7159181056518094, -2.4394886178911594),
 (0.08587692647219669, 0.17593490386009694, 0.9806487970233928)]
#%%
show_kwargs = dict(xtitle_offset=0.07,
                   ztitle_offset=0.02,
                   ytitle_offset=0.1,
                   xtitle='X (m)',
                   ytitle='Y (m)',
                   ztitle='Z (m)',
                   xrange=(-6, 2),
                   yrange=(-5, 6),
                   zrange=(-5.7, 5),
                   number_of_divisions=6,
                   xlabel_size=0.03,
                   ylabel_size=0.03,
                   zlabel_size=0.03,
                   xtitle_size=0.03,
                   ytitle_size=0.03,
                   ztitle_size=0.03
                   )
#%%
sou = SourceEM()
try:
    sou.load_hdf5(file_real)
except:
    samples=300
    sou.type = 'ricker'
    sou.set_frequency_vector(np.linspace(0, 0.3, samples))
    sou.set_delay(60)  # ns
    firstp = sou.create_source() #a=1,
    sou.export_to_hdf5(file_initial, overwrite=True)
    sou.export_to_hdf5(file_real, overwrite=True)
    sou.export_to_hdf5(file_inverted, overwrite=True)
    # fig = sou.plot_waveforms_complete()
    fig = sou.plot_waveforms_zoom()
    fig.show()
#%%
gempy_extent = (-2, 3, -5.0, 5.0, -4.7, 4.7)
resolution = (20, 20, 20)

#%% out
ant = Antenna()
try:
    ant.load_hdf5(file_real)
except:
    traces = 50
    borehole = np.zeros((traces, 3))
    borehole[:, 0] = -6
    borehole[:, -1] = np.linspace(-4, 4, traces)

    spacing = np.linalg.norm(borehole[0] - borehole[-1]) / (traces - 1)
    depths = np.linspace(0, np.linalg.norm(borehole[0] - borehole[-1]), traces)

    borehole1 = pd.DataFrame({'depth': np.flip(depths),
                              'X': borehole[:, 0],
                              'Y': borehole[:, 1],
                              'Z': borehole[:, 2]})

    orient = np.zeros(((traces, 3)))
    orient[:, -1] = 1

    p1 = np.array([-2.5, -3, -4])
    p2 = np.array([-6, 3, 4])

    n_points = 50  # Number of points you want to generate
    spacing = np.linalg.norm(p2 - p1) / (n_points - 1)
    orientation = np.tile((p2 - p1) / np.linalg.norm(p2 - p1), (n_points, 1))

    depths = np.linspace(0, np.linalg.norm(p2 - p1), n_points)
    points = np.array([p1 + (p2 - p1) * d / np.linalg.norm(p2 - p1) for d in depths])

    borehole2 = pd.DataFrame({'depth': np.flip(depths), 'X': points[:, 0], 'Y': points[:, 1], 'Z': points[:, 2]})

    ant.set_profile('BH1', receivers=borehole, transmitters=borehole, orient_receivers=orient, orient_transmitters=orient,
                    depth_Rx=borehole1['depth'].to_numpy(), depth_Tx=borehole1['depth'].to_numpy())
    ant.set_profile('BH2', receivers=points, transmitters=points, orient_receivers=orientation, orient_transmitters=orientation,
                    depth_Rx=borehole2['depth'].to_numpy(), depth_Tx=borehole2['depth'].to_numpy())
    # ant.export_to_hdf5(file_initial, overwrite=True)
    ant.export_to_hdf5(file_real, overwrite=True)
    ant.export_to_hdf5(file_inverted, overwrite=True)



#%%
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / sou.center_frequency  # m
max_element_size = wavelength / 6
# max_element_size=0.1

p1 = (70, 270, 0)

fixed = np.asarray([(-0.07264459, 0.30187218, 0.04356908)])
fixed_or = [fixed.copy(), np.asarray([(p1[1], p1[0], 1)])]
normal = get_normal_vector_from_azimuth_and_dip(azimuth=p1[1], dip=p1[0])


def plane_z_value(center, normal, xy_coord):
    x, y = xy_coord
    x0, y0, z0 = center
    a, b, c = normal
    d = -(a * x0 + b * y0 + c * z0)
    z = -(a * x + b * y + d) / c
    return z


z = plane_z_value(fixed[0], normal[0], [-1, 0])
planar = np.asarray([(-1, 0, z)])
df_plane = pd.DataFrame(planar, columns=['X', 'Y', 'Z'])
df_fixed = pd.DataFrame(fixed, columns=['X', 'Y', 'Z'])
df_orientations = pd.DataFrame(np.hstack((fixed, normal)), columns=['X', 'Y', 'Z', 'G_x', 'G_y', 'G_z'])

curved = np.asarray([[-0.85, 0, -3.4],
                     [-0.92, 0, -2.1],
                     [0.2, 0, 1.5],
                     [0, 0, 3],

                     [-0.5, 3, 0],
                     [1, -3, 0],
                     ])
df_curve = pd.DataFrame(curved, columns=['X', 'Y', 'Z'])
try:
    frac1 = FractureGeo()
    frac1.load_hdf5(file_initial)
    frac2 = FractureGeo()
    frac2.load_hdf5(file_real)
except:
    frac = FractureGeo()
    frac.init_geo_model(extent=gempy_extent, resolution=resolution)
    plane = 0
    v, _ = frac.create_gempy_geometry(fixed_points=df_fixed,
                               fixed_orientations=df_orientations,
                               move_points=df_plane)
    grid_plane, vertices_plane, faces_plane = frac.remesh(v, max_element_size=max_element_size, extrapolate=False,
                                                          plane=plane)

    v, _ = frac.create_gempy_geometry(fixed_points=df_fixed,
                               fixed_orientations=df_orientations,
                               move_points=df_curve)
    grid_curve, vertices_curve, faces_curve = frac.remesh(v, max_element_size=max_element_size, extrapolate=False,
                                                          plane=plane)

    frac1 = FractureGeo()
    dfplane =frac1.set_fracture('Plane', vertices=vertices_plane, faces=faces_plane, plane=plane, **kwargs_fracture_properties)
    frac1.export_to_hdf5(file_initial, overwrite=True)

    frac2 = FractureGeo()
    dfcurve =frac2.set_fracture('Curve', vertices=vertices_curve, faces=faces_curve, plane=plane, **kwargs_fracture_properties)
    frac2.export_to_hdf5(file_real, overwrite=True)


#%% Generate plots
from bedretto.core.common_helpers import tube_from_points
# for surf, df, c, fn in zip([frac1.get_surface(), frac2.get_surface()], [df_plane, df_curve], ['red', 'green'],['initial_synthetic_mesh.pdf', 'real_synthetic_mesh.pdf']):
for surf, df, c, fn in zip([frac2.get_surface()], [df_curve], ['green'],['fig2_a_real_synthetic_mesh.pdf']):
    p = model.ModelPlot()
    # bor = pv.PolyData(ant.Transmitter)
    for i in ant.name_profiles:
        bor = tube_from_points(ant.profiles.loc[ant.profiles['profile'] == i, ['Tx','Ty','Tz']].to_numpy(),
                               radius=0.05)
        p.add_object(bor, name=f'Tx{i}', color='Red', opacity=0.8)
        projborx = bor.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
        p.add_object(projborx, name=f'projTx2{i}', color='Red', opacity=0.2)
        projborz=bor.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
        p.add_object(projborz, name=f'projTx3{i}', color='Red', opacity=0.2)
        # p.add_object(ant.Receiver, name='Rx', color='blue')

        p.add_label(points=ant.profiles.loc[
            (ant.profiles['depth_Tx'] == 0) & (ant.profiles['profile'] == i), ('Tx', 'Ty', 'Tz')].to_numpy(),
                    labels=[i], name=i, point_size=11, point_color='black', font_size=48)

    p.add_object(surf, name='Mesh', color=c, opacity=0.5)
    projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
    p.add_object(projected, name='MeshProjected', opacity=0.2)
    projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
    p.add_object(projected2, name='MeshProjected2', opacity=0.2)

    if not 'initial' in fn:
        p.add_label(points=df.to_numpy(), labels=[str(k) for k, p in df.iterrows()], name='cp', point_size=11,
                    point_color='black',font_size=48)
        # Add projected control points to planes
        po = pv.PolyData(df.to_numpy())
        projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
        p.add_object(projectedxy, name='PointProjected', point_size=10)
        projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
        p.add_object(projectedz, name='PointProjected2', point_size=10)

    p.add_label(points=df_fixed.to_numpy(), labels=['Fixed'], name='cp_fixed', point_size=11, point_color='black',
                font_size=48, always_visible=True)
    po = pv.PolyData(df_fixed.to_numpy())
    projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projectedxy, name='PointProjected3', point_size=10)
    projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projectedz, name='PointProjected4', point_size=10)

    arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
    p.add_object(arrow, name='arrow', color='blue')


    p.plotter.window_size = window_size
    p.plotter.camera_position = cam
    p.show(**show_kwargs)
    p.plotter.add_axes(box=True)
    p.snapshot(fn, save_dir=odir+'figures')
#%% Solve
# solv_p = FracEM()
# try:
#     solv_p.load_hdf5(file_initial)
#     freq_p = solv_p.get_freq()
# except:
#     solv_p.rock_epsilon = 5.9
#     solv_p.rock_sigma = 0.0
#     solv_p.backend = 'torch'
#     solv_p.engine='tensor_trace'
#     solv_p.open_file(file_initial)
#     solv_p.time_zero_correction()
#
#     freq_p = solv_p.forward_pass()
# #%%
# time_response_p, time_vector_p = solv_p.get_ifft(freq_p)
# plt.imshow(time_response_p.T, aspect='auto', cmap='seismic')
# plt.colorbar()
# plt.show()
def get_ifft(self, input_signal, pad: int = 0):
    """

    Args:
        input_signal: Shape (traces x frequencies)
        pad: int specifying the amount of zeroes to pad at the end of the frequency solution to
            increase the sampling time.


    Returns:
        time_response:
        time_vector:

    """
    import torch
    if isinstance(input_signal, torch.Tensor):
        input_signal = input_signal.numpy()
    traces, frequencies = input_signal.shape
    if pad != 0:
        input_signal = np.concatenate((input_signal,
                                       np.zeros((traces, pad), dtype=complex)), axis=1)

    nfft = 2 ** np.ceil(np.log2(frequencies * 2)).astype(int)
    resampled_response = np.concatenate((input_signal,
                                         np.zeros((traces, nfft // 2 - frequencies), dtype=complex)),
                                        axis=1)
    # resampled_response = input_signal
    concatenated_complex_signal = np.concatenate((resampled_response,
                                                  np.conj(np.flip(resampled_response, axis=1))),
                                                 axis=1)
    # time_response = np.real(np.fft.ifft(concatenated_complex_signal, n=nfft, axis=1))
    time_response = np.real(np.fft.ifft(np.conj(concatenated_complex_signal), n=nfft, axis=1))
    # time_responseL = time_response[:, :time_response.shape[1] // 2]
    # time_responseL = time_response[:, :nfft // 2]
    time_response = time_response[:, :nfft // 2]
    # time_responseR = np.flip(time_response[:, -time_response.shape[1] // 2:], axis=1)
    # time_responseR = np.flip(time_response[:, -nfft // 2:], axis=1)
    # a =time_response2[2]- np.flip(time_response)[2]

    # plt.plot(time_response[2]);plt.show()
    # plt.plot(time_response2[2]);plt.show()
    #
    # plt.plot(np.flip(time_response, axis=1)[2])
    # plt.plot(time_response2[2,1:])
    # plt.show()
    # if np.all(time_responseL == time_responseR):
    #     time_response = time_responseL
    # logger.debug('IFFT is mirrored')
    # else:
    #     time_response = time_responseR
    #     logger.debug('IFFT is not mirrored. Taking right side')

    # time_vector = np.arange(0, self.time_window, self.time_window / nfft)
    # time_vector = np.linspace(0, self.time_window, nfft // 2, endpoint=True)
    time_vector = np.linspace(0,
                              self.file_read_attribute('source/time_window'),
                              nfft // 2)
    if self._t0_correction is not None:
        mask = time_vector >= self._t0_correction
        # time_response = time_response[:, self._t0_correction:]
        time_response = time_response[:, mask]
        samples_to_remove = sum(~mask)
        # time_vector = time_vector[:-self._t0_correction]
        time_vector = time_vector[:-samples_to_remove]
        print(
            f'Applying time zero correction to data. Removing {self._t0_correction:.2f} ns from data == {samples_to_remove} samples')
    else:
        print('No time zero correction')

    # sf = self.file_read_attribute('source/dfreq') * nfft // 2  # New sampling frequency
    # dt = 1/(2*sf)  # New delta_t
    # tw = self.file_read_attribute('source/time_window')

    # time_vector = np.arange(0,
    #                         time_response.shape[1] * dtime + dtime,
    #                         dtime,
    #                         )

    return time_response, time_vector
#%% Solve
solv_c = FracEM()
try:
    solv_c.load_hdf5(file_real)
    freq_c = solv_c.get_freq()
except:
    solv_c.rock_epsilon = 5.9
    solv_c.rock_sigma = 0.0
    solv_c.backend = 'torch'
    solv_c.engine='tensor_trace'
    solv_c.open_file(file_real)
    f, t0 = solv_c.time_zero_correction()

    freq_c = solv_c.forward_pass()
#%%
# time_response_c, time_vector_c = solv_c.get_ifft(f)
# plt.plot(time_vector_c, time_response_c[0])
# # plt.xlim(700,time_vector_c[-1])
# plt.show()
#%%
# time_response_c, time_vector_c = solv_c.get_ifft(freq_c)
time_response_c, time_vector_c = get_ifft(solv_c, freq_c)
plt.imshow(time_response_c.T, aspect='auto', cmap='seismic')
plt.colorbar()
plt.show()
#%% Adding real noise
file = 'MB1_100MHz_200515.mat'
from gdp import DATA_DIR as gdpdat
mat = io.loadmat(gdpdat + 'VALTER/' + file)
depth = mat['depth'][:, 0]
processed_data = mat['processed_data']
raw_data = mat['raw_data']
travel_time = mat['travel_time'][0]
radius = mat['radius'][0]

extent = (depth[0], depth[-1], radius[-1], radius[0])
processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)
from scipy.stats import rv_histogram

subset_of_noise = [1100, 1400, 5200, 5800]
subset = processed_data[subset_of_noise[0]:subset_of_noise[1], subset_of_noise[2]:subset_of_noise[3]]
histogram, bin_edges = np.histogram(subset, bins=100, )
hist_distribution = rv_histogram((histogram, bin_edges))


# #%%
# fig, AX = plt.subplots(1, 2, figsize=(8,10))
#
# for ax, n in zip(AX, solv_c.name_profiles):
#     freq = solv_c.get_freq(name_profile=n)
#     # freq = solv_p.get_freq(name_profile=n)
#     time_response, time_vector = solv_c.get_ifft(freq)
#     time_response /= np.abs(time_response).max()
#     noise = hist_distribution.rvs(size=time_response.shape)
#     time_response += noise*0.15
#     time_response /= np.abs(time_response).max()
#     # time_response, time_vector = solv_p.get_ifft(freq)
#     radial_distance = time_vector * solv_c.velocity / 2
#     ant_position = ant.profiles.loc[ant.profiles['profile']==n, ('depth_Tx')].to_numpy()
#     # extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
#     extent = (radial_distance[0], radial_distance[-1], ant_position[-1], ant_position[0])
#     cax = ax.imshow(time_response, aspect='auto', cmap='seismic', vmax=1,vmin=-1, extent=extent)
#     ax.set_ylabel('Borehole depth (m)')
#     ax.set_xlabel('Radial distance (m)')
#     xlims = (2, 10)
#     ylim = (10.5, 0)
#     ax.set_xlim(xlims[0], xlims[1])
#     ax.set_ylim(ylim[0], ylim[1])
#
#     ax1 = ax.twiny()
#     ax1.set_xlabel('Two-way travel-time (ns)')
#     start, end = xlims[0] * 2 / solv_c.velocity, xlims[1] * 2 / solv_c.velocity
#
#     ax1.set_xlim(start, end)
#     ax.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2))
#     ax1.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2) * 2 / solv_c.velocity)
#
#     ax.tick_params(axis='both', which='both', direction='out', length=5)
#     ax1.tick_params(axis='both', which='both', direction='out', length=5)
#     ax.set_title(n)
#     ax.grid(linestyle='--')
#
#     fig.colorbar(cax, ax=ax)
# fig.tight_layout()
# fig.savefig(odir+'figures/synthetic_boreholes_simulation.pdf')
# plt.show()

#%%
# fig = plt.figure(constrained_layout=True, figsize=(8,10))
# subfig = fig.subfigures(nrows=1, ncols=1)
# AX = subfig.subplots(nrows=1, ncols=2)
#
# for ax, n in zip(AX, solv_c.name_profiles):
#     freq = solv_c.get_freq(name_profile=n)
#     # freq = solv_p.get_freq(name_profile=n)
#     time_response, time_vector = solv_c.get_ifft(freq)
#     time_response /= np.abs(time_response).max()
#     # noise = hist_distribution.rvs(size=time_response.shape)
#
#     trace_power = np.sum(time_response ** 2, axis=0) / time_response.shape[0]
#     # Sum the powers of all the traces
#     signal_power = np.sum(trace_power)
#
#     desired_snr = 25
#
#     # np.std(subset)
#
#     noise_power = signal_power / (10 ** (desired_snr / 10))
#
#     noise = np.random.normal(0, np.sqrt(noise_power), size=time_response.shape)
#     # noise = np.random.normal(0, np.sqrt(np.std(subset)), size=time_response.shape)
#
#     # time_response += noise*0.15
#     time_response += noise
#     time_response /= np.abs(time_response).max()
#     # time_response, time_vector = solv_p.get_ifft(freq)
#     radial_distance = time_vector * solv_c.velocity / 2
#     ant_position = ant.profiles.loc[ant.profiles['profile']==n, ('depth_Tx')].to_numpy()
#     # extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
#     extent = (radial_distance[0], radial_distance[-1], ant_position[-1], ant_position[0])
#     cax = ax.imshow(time_response, aspect='auto', cmap='seismic', vmax=1,vmin=-1, extent=extent)
#     ax.set_ylabel('Borehole depth (m)')
#     ax.set_xlabel('Radial distance (m)')
#     # xlims = (2, 10)
#     # ylim = (10.5, 0)
#     if n == 'BH1':
#         ylims = (8,0);xlims = (2, 10)
#     elif n == 'BH2':
#         ylims = (10.5, 0);xlims = (2, 10)
#     else:
#         n = None
#     ax.set_xlim(xlims[0], xlims[1])
#     ax.set_ylim(ylims[0], ylims[1])
#
#     ax1 = ax.twiny()
#     ax1.set_xlabel('Two-way travel-time (ns)')
#     start, end = xlims[0] * 2 / solv_c.velocity, xlims[1] * 2 / solv_c.velocity
#
#     ax1.set_xlim(start, end)
#     ax.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2))
#     ax1.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2) * 2 / solv_c.velocity)
#
#     ax.tick_params(axis='both', which='both', direction='out', length=5)
#     ax1.tick_params(axis='both', which='both', direction='out', length=5)
#     ax.set_title(n)
#     ax.grid(linestyle='--')
#
# subfig.colorbar(cax, ax=AX, label='Normalized GPR Amplitude (-)', orientation='horizontal')
#
# # fig.tight_layout()
# # fig.savefig(odir+'figures/synthetic_boreholes_simulation.pdf')
# plt.show()
#%%
import matplotlib.pyplot as plt

fig = plt.figure(constrained_layout=True, figsize=(12,8))
subfig = fig.subfigures(nrows=1, ncols=1)
AX = subfig.subplots(nrows=2, ncols=1)

for ax, n in zip(AX, solv_c.name_profiles):
    freq = solv_c.get_freq(name_profile=n)
    # freq = solv_p.get_freq(name_profile=n)
    time_response, time_vector = solv_c.get_ifft(freq)
    time_response /= np.abs(time_response).max()
    # noise = hist_distribution.rvs(size=time_response.shape)

    trace_power = np.sum(time_response ** 2, axis=0) / time_response.shape[0]
    # Sum the powers of all the traces
    signal_power = np.sum(trace_power)

    desired_snr = 25

    # np.std(subset)

    noise_power = signal_power / (10 ** (desired_snr / 10))

    noise = np.random.normal(0, np.sqrt(noise_power), size=time_response.shape)
    # noise = np.random.normal(0, np.sqrt(np.std(subset)), size=time_response.shape)

    # time_response += noise*0.15
    time_response += noise
    time_response /= np.abs(time_response).max()
    # time_response, time_vector = solv_p.get_ifft(freq)
    radial_distance = time_vector * solv_c.velocity / 2
    ant_position = ant.profiles.loc[ant.profiles['profile']==n, ('depth_Tx')].to_numpy()
    # extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
    extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
    cax = ax.imshow(time_response.T, aspect='auto', cmap='seismic', vmax=1,vmin=-1, extent=extent)
    ax.set_xlabel('Borehole depth (m)', fontsize=20)
    ax.set_ylabel('Radial distance (m)', fontsize=20)
    # xlims = (2, 10)
    # ylim = (10.5, 0)
    if n == 'BH1':
        xlims = (0,8); ylims = (2, 9)
    elif n == 'BH2':
        xlims = (0, 10.5); ylims = (2, 9)
    else:
        n = None
    ax.set_xlim(min(xlims), max(xlims))
    ax.set_ylim(max(ylims), min(ylims))

    ax1 = ax.twinx()
    ax1.set_ylabel('Two-way travel-time (ns)', fontsize=20)
    start, end = max(ylims) * 2 / solv_c.velocity, min(ylims) * 2 / solv_c.velocity

    ax1.set_ylim(start, end)
    ax.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 2))
    ax1.yaxis.set_ticks(np.arange(min(ylims), max(ylims), 2) * 2 / solv_c.velocity)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(n, fontsize=25, fontweight="bold")
    ax.grid(linestyle='--')

    # Set tick font size
    for label in (ax.get_xticklabels() + ax.get_yticklabels() + ax1.get_yticklabels()):
        label.set_fontsize(20)

cl = subfig.colorbar(cax, ax=AX, orientation='vertical')
cl.set_label(label='Normalized GPR Amplitude (-)', fontsize=20)
cl.ax.tick_params(labelsize=20)
# fig.tight_layout()
fig.savefig(odir+'figures/fig2_b_synthetic_boreholes_simulation.pdf')
plt.show()
#%% I need to pick the reflector
# I will pick the reflector 10 times to be able to do some statistics
try:
    with open(odir + 'manual_picks', 'rb') as f:
        picks = pickle.load(f)
except:
    from gdp.processing.image_processing import pick_line
    picks = {}
    for p in range(10):
        picks[p] = {}
        for n in solv_c.name_profiles:
            freq = solv_c.get_freq(name_profile=n)
            # freq = solv_p.get_freq(name_profile=n)
            time_response, time_vector = solv_c.get_ifft(freq)
            #### TEmporal
            time_response = np.flipud(time_response)
            #####
            mask = time_vector < 130 # ns
            # time_response, time_vector = solv_p.get_ifft(freq)
            time_response = time_response[:, mask]
            radial_distance = time_vector[mask] * solv_c.velocity / 2
            time_response /= np.abs(time_response).max()
            ant_position = ant.profiles.loc[ant.profiles['profile'] == n, ('depth_Tx')].to_numpy()
            extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])

            pi = pick_line(time_response.T, extent=extent, colormap='seismic',
                              interpolate_path=True, radius_point=1)

            picks[p][n] = pi
    with open(odir + 'manual_picks', 'wb') as f:
        pickle.dump(picks, f)

#%%
true_picks = {}
bh_picks = {}
for n in solv_c.name_profiles:
    element_positions = solv_c.file_read('mesh/geometry/midpoint')
    df = ant.profiles.loc[ant.profiles['profile']==n]
    tx = df[['Tx', 'Ty', 'Tz']].to_numpy()
    rx = df[['Rx', 'Ry', 'Rz']].to_numpy()

    t_e = cdist(tx, element_positions)
    r_e = cdist(rx, element_positions)
    distance = t_e + r_e  # This returns a borehole position x midpoints matrix
    pos_min_distance = np.argmin(distance, axis=1)  # so we look at the borehole position
    min_distance = distance[np.arange(len(pos_min_distance)), pos_min_distance]  # Minimum distance
    # min_position = element_positions[pos_min_distance]  # Real position at minimum distance
    # first_arrival = min_distance / solv_c.velocity
    # true_picks[n] = np.c_[df['depth_Tx'], first_arrival * 0.5]
    true_picks[n] = np.c_[df['depth_Tx'], min_distance * 0.5]

    bh_picks[n] = np.vstack([n0[n]['pick_0']['true coordinates'] for i0, n0 in picks.items()])
#%% Go through all the depths and make the comparison. I will calculate the error for each trace and I will look at the error of all traces
from sklearn.metrics import mean_squared_error
from scipy import stats
val = {}
mean_val = {}
iqr = {}
all_normalized_points = []
error_trace = {}
for n in solv_c.name_profiles:
    mv = []
    iq = []
    for d in ant.profiles.loc[ant.profiles['profile']==n, 'depth_Tx'].to_numpy():
        if d == 0:  # Solve a problem that 0 is never having data
            pass
        else:
            pos = np.ravel(np.argwhere(np.isclose(round(d, 2), bh_picks[n][:,0].round(2), rtol=0.1)))
        if len(pos) == 0:  # No number
            print(f'Not found, {d,pos}')
            continue
        iq.append(stats.iqr(bh_picks[n][pos, 1], interpolation='midpoint'))
        mv.append((d, np.percentile(bh_picks[n][pos, 1], 50)))
        all_normalized_points.append(bh_picks[n][pos, 1] - np.percentile(bh_picks[n][pos, 1], 50))  # Take all the points and substract the median to create a normal distributed data
    mean_val[n] = np.asarray(mv)
    iqr[n] = np.sqrt(np.square(iq).mean())
    error_trace[n] = np.asarray(iq)

all_normalized_points = np.hstack(all_normalized_points)
error_all = stats.iqr(all_normalized_points, interpolation='midpoint')

#%%
fig = plt.figure(constrained_layout=True, figsize=(10,8))
subfig = fig.subfigures(nrows=1, ncols=1)
AX = subfig.subplots(nrows=2, ncols=1)

for ax, n in zip(AX, solv_c.name_profiles):
    freq = solv_c.get_freq(name_profile=n)
    # freq = solv_p.get_freq(name_profile=n)
    time_response, time_vector = solv_c.get_ifft(freq)
    time_response /= np.abs(time_response).max()
    # noise = hist_distribution.rvs(size=time_response.shape)
    # time_response += noise*0.15
    trace_power = np.sum(time_response ** 2, axis=0) / time_response.shape[0]
    # Sum the powers of all the traces
    signal_power = np.sum(trace_power)

    desired_snr = 25

    # np.std(subset)

    noise_power = signal_power / (10 ** (desired_snr / 10))

    noise = np.random.normal(0, np.sqrt(noise_power), size=time_response.shape)
    time_response += noise
    time_response /= np.abs(time_response).max()
    # time_response, time_vector = solv_p.get_ifft(freq)
    radial_distance = time_vector * solv_c.velocity / 2
    ant_position = ant.profiles.loc[ant.profiles['profile']==n, ('depth_Tx')].to_numpy()
    # extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
    extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0], )
    cax = ax.imshow(time_response.T, aspect='auto', cmap='seismic', vmax=1,vmin=-1, extent=extent,
                    alpha=0.5)
    ax.set_xlabel('Borehole depth (m)')
    ax.set_ylabel('Radial distance (m)')
    if n == 'BH1': ylims = (6.5, 4.5);xlims = (0, 8)
    elif n == 'BH2': ylims = (5.5, 2.5);xlims = (0, 10.5)
    else: n = None
    ax.set_xlim(xlims[0], xlims[1])
    ax.set_ylim(ylims[0], ylims[1])

    ax1 = ax.twinx()
    ax1.set_ylabel('Two-way travel-time (ns)')
    start, end = ylims[0] * 2 / solv_c.velocity, ylims[1] * 2 / solv_c.velocity

    ax1.set_ylim(start, end)
    ax.yaxis.set_ticks(np.arange(ylims[1], ylims[0], 0.5))
    ax1.yaxis.set_ticks(np.arange(ylims[1], ylims[0], 0.5) * 2 / solv_c.velocity)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(n)
    ax.grid(linestyle='--')

    pos_all = []
    depths = ant.profiles.loc[ant.profiles['profile'] == n, 'depth_Tx'].to_numpy()#[::step]
    for d in depths:
        if d == 0:  # Solve a problem that 0 is never having data
            pass
        else:
            pos = np.ravel(np.argwhere(np.isclose(round(d, 2), bh_picks[n][:, 0].round(2), rtol=0.1)))
        if len(pos) == 0:  # No number
            print(f'Not found, {d, pos}')
            continue
        pos_all.append((bh_picks[n][pos, 0].mean(), bh_picks[n][pos, 1].min(), bh_picks[n][pos, 1].max()))
    pos_all = np.vstack(pos_all)
    # fig, ax = plt.subplots()
        # for n in range(10):
        #     ax.plot(picks[n][bn]['pick_0']['true coordinates'][:, 0],
        #             picks[n][bn]['pick_0']['true coordinates'][:, 1],
        #             alpha=0.2)
    ax.fill_between(pos_all[:, 0], pos_all[:, 1], pos_all[:, 2], alpha=0.2, color='gray', label='All picks')
    ax.plot(true_picks[n][:, 0], true_picks[n][:, 1], 'g-', label='Ground-truth')
    ax.plot(mean_val[n][:, 0], mean_val[n][:, 1], '-', label='Mean value', color='black')
    # plt.show()
    ax.legend()
subfig.colorbar(cax, ax=AX, label='Normalized GPR Amplitude (-)')

# fig.tight_layout()
fig.savefig(odir+'figures/synthetic_boreholes_picks.pdf')
plt.show()
#%%
import seaborn as sns
from scipy.stats import norm
# fig, ax = plt.subplots()
fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(6,8),  gridspec_kw={'height_ratios': [1, 3]})
s = all_normalized_points * 2 / 0.1234
# sns.histplot(s, ax=axes[1], stat='density', alpha=0.5)
x, pdf  = sns.kdeplot(s, color='orange', ax=axes[1], label='Kernel density estimate').lines[0].get_data()

n_bins = 50
mu = s.mean()
sigma=np.std(s)
median, q1, q3 = np.percentile(s, 50), np.percentile(s, 25), np.percentile(s, 75)

n, bins, patches = axes[1].hist(s, n_bins, density=True, alpha=.3, edgecolor='black' )
# pdf = 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2))
# axes[1].plot(bins, pdf, color='orange', alpha=.6)

mask1 = (x >= q1-1.5*(q3-q1)) & (x <= q1)
mask2 = (x <= q3+1.5*(q3-q1)) & (x >= q3)
bins_1 = x[mask1] # to ensure fill starts from Q1-1.5*IQR
bins_2 = x[mask2]
# pdf_1 = pdf[:int(len(pdf)/2)]
# pdf_2 = pdf[int(len(pdf)/2):]
pdf_1 = pdf[mask1]
pdf_2 = pdf[mask2]
# pdf_2 = pdf_2[(pdf_2 >= norm(mu,sigma).pdf(q3+1.5*(q3-q1))) & (pdf_2 <= norm(mu,sigma).pdf(q3))]

#fill from Q1-1.5*IQR to Q1 and Q3 to Q3+1.5*IQR
axes[1].fill_between(bins_1, pdf_1, 0, alpha=.4, color='orange')
axes[1].fill_between(bins_2, pdf_2, 0, alpha=.4, color='orange')


#top boxplot
axes[0].boxplot(s, 0, 'gD', vert=False)
axes[0].axvline(np.percentile(s, 50), color='red', alpha=.6, linewidth=.5)
axes[0].axis('off')


# axes[1].annotate("{:.1f}%".format(100*norm(mu, sigma).cdf(q1)), xy=((q1-1.5*(q3-q1)+q1)/2, 0), ha='center')
axes[1].annotate("{:.3f}".format(error_all*2/0.1234), xy=(median, 0.05), ha='center')
# axes[1].annotate("{:.1f}%".format(100*(norm(mu, sigma).cdf(q3+1.5*(q3-q1)-q3)-norm(mu, sigma).cdf(q3))), xy=((q3+1.5*(q3-q1)+q3)/2, 0), ha='center')
axes[1].annotate('q1', xy=(q1, norm(mu, sigma).pdf(q1)), ha='center')
axes[1].annotate('q3', xy=(q3, norm(mu, sigma).pdf(q3)), ha='center')

axes[1].set_ylabel('Density')
axes[1].set_xlabel('Travel-time (ns) - median for all points')

plt.legend()
plt.subplots_adjust(hspace=0)
plt.show()
fig.savefig(odir+'figures/error_all_points_synthetic.pdf')

#%% We will do the inversion using just the vertical borehole
ant = Antenna()
traces = 50
borehole = np.zeros((traces, 3))
borehole[:, 0] = -6
borehole[:, -1] = np.linspace(-4, 4, traces)

spacing = np.linalg.norm(borehole[0] - borehole[-1]) / (traces - 1)
depths = np.linspace(0, np.linalg.norm(borehole[0] - borehole[-1]), traces)

borehole1 = pd.DataFrame({'depth': np.flip(depths),
                          'X': borehole[:, 0],
                          'Y': borehole[:, 1],
                          'Z': borehole[:, 2]})

orient = np.zeros(((traces, 3)))
orient[:, -1] = 1

p1 = np.array([-2.5, -3, -4])
p2 = np.array([-6, 3, 4])

n_points = 50  # Number of points you want to generate
spacing = np.linalg.norm(p2 - p1) / (n_points - 1)
orientation = np.tile((p2 - p1) / np.linalg.norm(p2 - p1), (n_points, 1))

depths = np.linspace(0, np.linalg.norm(p2 - p1), n_points)
points = np.array([p1 + (p2 - p1) * d / np.linalg.norm(p2 - p1) for d in depths])

borehole2 = pd.DataFrame({'depth': np.flip(depths), 'X': points[:, 0], 'Y': points[:, 1], 'Z': points[:, 2]})

ant.set_profile('BH1', receivers=borehole, transmitters=borehole, orient_receivers=orient, orient_transmitters=orient,
                depth_Rx=borehole1['depth'].to_numpy(), depth_Tx=borehole1['depth'].to_numpy())

#%%
from fracwave.inversion.geometry_inversion import GeometryInversion
inv = GeometryInversion(gempy_extent=gempy_extent,
                        gempy_resolution=resolution,
                        velocity=solv_c.velocity,
                        # Tx=ant.profiles.loc[ant.profiles['profile'] == 'BH1', ('Tx','Ty','Tz')].to_numpy(),
                        # Rx=ant.profiles.loc[ant.profiles['profile'] == 'BH1', ('Rx','Ry','Rz')].to_numpy(),
                        antenna=ant,
                        measured_travel_time={'BH1':mean_val['BH1'][:,1] * 2 / solv_c.velocity})
#%% Run inversion only BH1 borehole
try:
    inverted_points_bh1 = pd.read_csv(odir+'inverted_points_BH1.csv')
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=0)

except:
    inv.clear()
    inv.tol_pos = error_trace['BH1'] * 2 / solv_c.velocity   # Tolerance for a single trace
    inv.tol_all = error_all * 2 / solv_c.velocity  # Convert to time. Tolerance for overall RMS error over the whole picks
    inv.surrounding = 2
    inv.max_iter_all = 20
    inv.max_iter_pos = 15
    # inv.measured_travel_time =
    inv.fixed_control_points = df_fixed
    inv.fixed_orientations = df_orientations
    inv.temp_control_points = df_plane
    inv.directory = odir
    inv.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=0)

    inverted_points_bh1 = inv.run_inversion()
    inverted_points_bh1.to_csv(odir+'inverted_points_BH1.csv')

inverted_points_bh1.reset_index(drop=True, inplace=True)
#%%
# p = model.ModelPlot()
# p.add_object(ant.Transmitter, name='Tx', color='Red')
# p.add_object(frac1.get_surface(), name='Initial', color='red', opacity=0.3)
# p.add_object(frac2.get_surface(), name='Real', color='green', opacity=0.3)
#
# # p.add_object(inv.grid, name='Simulated', cmap='viridis', scalars = 'distt')
# p.add_object(inv.grid, name='Simulated', color='blue', opacity=0.3)
#
# fix = inverted_points[['X','Y','Z']].reset_index(drop=True)
#
# p.add_label(points=fix.to_numpy(), labels=[str(k) for k, p in fix.iterrows()], name='cp', point_size=10, point_color='black',font_size=45)
# # Add projected control points to planes
# po = pv.PolyData(fix.to_numpy())
# projectedxy = po.project_points_to_plane(origin=(0, 0, extent[4] - 1), normal=(0, 0, 1))
# p.add_object(projectedxy, name='PointProjected', point_size=10)
# projectedz = po.project_points_to_plane(origin=(0, extent[3]+1, 0), normal=(0, 1, 0))
# p.add_object(projectedz, name='PointProjected2', point_size=10)
#
# p.add_label(points=df_fixed.to_numpy(), labels=['Fixed'], name='cp_fixed', point_size=10, point_color='black', font_size=45, always_visible=True)
# po = pv.PolyData(df_fixed.to_numpy())
# projectedxy = po.project_points_to_plane(origin=(0, 0, extent[4] - 1), normal=(0, 0, 1))
# p.add_object(projectedxy, name='PointProjected3', point_size=10)
# projectedz = po.project_points_to_plane(origin=(0, extent[3] + 1, 0), normal=(0, 1, 0))
# p.add_object(projectedz, name='PointProjected4', point_size=10)


#%%
p = model.ModelPlot()
for i in ['BH1']:
    points = ant.profiles.loc[ant.profiles['profile'] == i, ('Tx', 'Ty', 'Tz')].to_numpy()

    bor = tube_from_points(points,
                           radius=0.05)
    p.add_object(bor, name=f'Tx{i}', color='Red', opacity=0.8)
    projborx = bor.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projborx, name=f'projTx2{i}', color='Red', opacity=0.2)
    projborz = bor.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projborz, name=f'projTx3{i}', color='Red', opacity=0.2)
    # p.add_object(ant.Receiver, name='Rx', color='blue')

    p.add_label(points=ant.profiles.loc[
        (ant.profiles['depth_Tx'] == 0) & (ant.profiles['profile'] == i), ('Tx', 'Ty', 'Tz')].to_numpy(),
                labels=[i], name=i, point_size=10, point_color='black', font_size=45)

# points = ant.profiles.loc[ant.profiles['profile']=='BH1', ('Tx','Ty','Tz')].to_numpy()
# p.add_object(points, name='Tx', color='Red')
# p.add_object(ant.Receiver, name='Rx', color='blue')
prev = frac2.vertices
inverted_points_bh1 = inverted_points_bh1.reset_index(drop=True)
for i in range(len(inverted_points_bh1)):
    p.remove_scalar_bar()
    if i ==0:
        inv.fixed_control_points = df_fixed
        inv.fixed_orientations = df_orientations
        inv.temp_control_points = df_plane
        first_arrival_initial, min_position = inv.forward_solver()
    else:
        inv.fixed_control_points = inverted_points_bh1[:i+1]
        inv.fixed_orientations = df_orientations
        inv.temp_control_points = None
        first_arrival, min_position = inv.forward_solver()

        fix = inverted_points_bh1[1:i+1]

        sp = inverted_points_bh1[1:i+1][['X','Y','Z']].to_numpy()[-1]

        p.add_label(points=fix[['X','Y','Z']].to_numpy(), labels=[f' {k} ' for k, p in fix.iterrows()], name='cp', point_size=10,
                    point_color='black', font_size=50, always_visible=True)
        # Add projected control points to planes
        po = pv.PolyData(fix[['X','Y','Z']].to_numpy())
        projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
        p.add_object(projectedxy, name='PointProjected', point_size=10)
        projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
        p.add_object(projectedz, name='PointProjected2', point_size=10)



    surf = pv.PolyData(inv.vertices, np.hstack([np.hstack([len(f), f]) for f in inv.faces]))
    # if 'initial' in fn: continue
    distt = np.linalg.norm(prev - surf.points, axis=1)
    # p.add_object(surf, name='Mesh', color='blue', opacity=0.5)
    surf['Distance (m)'] = distt
    if i == 0:
        cmax = distt.max()
    p.add_object(surf, name='Mesh', scalars='Distance (m)', clim=(0,cmax), cmap='turbo', show_scalar_bar=False)
    # if i ==0:

    # p.plotter.add_scalar_bar(title=f'Distance (m)\n[{distt.min():.2f}-{distt.max():.2f}]\n', title_font_size=40, label_font_size=40, vertical=True,
    #                          height=0.55, position_x=0.89, position_y=0.25)
    p.plotter.add_scalar_bar(title=f'Distance misfit [{distt.min():.2f}-{distt.max():.2f}] (m)', title_font_size=40,
                             label_font_size=40, vertical=False,
                             position_x=0.3, position_y=0)

    # p.remove_scalar_bar()
    # else:
    #     p.plotter.add_scalar_bar(title='Distance (m)\n', title_font_size=40, label_font_size=40, vertical=True,
    #                              height=0.6, position_x=0.88, position_y=0.3)

    projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
    p.add_object(projected, name='MeshProjected', opacity=0.2)
    projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
    p.add_object(projected2, name='MeshProjected2', opacity=0.2)

    # fix = fixed_points_geometry.reset_index(drop=True)[:i+2]

    p.add_label(points=df_fixed.to_numpy(), labels=[' Fixed '], name='cp_fixed', point_size=10, point_color='black', font_size=45, always_visible=True)
    po = pv.PolyData(df_fixed.to_numpy())
    projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projectedxy, name='PointProjected3', point_size=10)
    projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projectedz, name='PointProjected4', point_size=10)

    arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
    p.add_object(arrow, name='arrow', color='blue')

    p.add_label(points=[points[-1]],
                labels=['BH1'], name='b1', point_size=10, point_color='black', font_size=45)



    # solv.open_file(filename)
    # freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False)
    # pick_current = solv.file_read('dummy_solution/first_arrival')

    #########
    # fig, AX = plt.subplots(2, 1, figsize=(8, 4))
    distance_plane = first_arrival_initial * 0.1234 / 2
    # for ax in AX:
    fig, ax = plt.subplots(figsize=(10, 5))
    distance_curve = mean_val['BH1'][:,1]
    if i != 0:
        distance_current = first_arrival * 0.1234 / 2
        dep = ant.profiles.loc[ant.profiles['profile']=='BH1', ('depth_Tx')].to_numpy()
        ax.plot(dep,distance_current, color='blue', label='Updated model')
        #ax.axvline(x=dep[int(fix[-1:]['pos'].to_numpy()[0])], color='orange', linestyle='--',)# label='Position')
        bp = points[int(fix[-1:]['pos'].to_numpy()[0])]
        p.add_object(pv.Sphere(radius=0.3, center=bp),
                     name='position', color='red')

        # Plot arrow showing direction
        arrowdir = pv.Arrow((sp+bp)/2, direction=(bp-sp)/np.linalg.norm(sp-bp, ), scale = 1.5)
        p.add_object(arrowdir, name='direction', color='orange',opacity=0.8)
    elif i==0:
        p.remove('direction')

    p.plotter.window_size = window_size
    p.plotter.camera_position = cam
    p.show(**show_kwargs)
    p.plotter.add_axes(box=True)
    distance_plot = (4, 7)

    ax.plot(ant.profiles.loc[ant.profiles['profile'] == 'BH1', ('depth_Tx')], distance_plane, color='red', label='Initial model', linestyle='dotted')
    ax.plot(ant.profiles.loc[ant.profiles['profile'] == 'BH1', ('depth_Tx')], distance_curve, color='green', label='Ground-truth', linestyle='dotted')

    ax.set_ylim(distance_plot[1], distance_plot[0])
    ax.set_xlabel('Borehole Depth (m)')
    ax.set_ylabel('Radial distance (m)')
    start, end = distance_plot[1], distance_plot[0]
    ax.yaxis.set_ticks(np.arange(end, start + 1, 0.5))
    # start, end = ax.get_xlim()
    # ax.yaxis.set_ticks(np.arange(4, 8, 1))
    ax.set_xlim(0, 8)
    # ax.set_ylim(7, 4)

    ax2 = ax.twinx()
    ax2.set_ylabel('Two-way travel-time (ns)')
    # start, end = distance_plot[1] * 2 / 0.1234, distance_plot[0] * 2 / 0.1234
    ax2.set_ylim(start * 2 / 0.1234, end * 2 / 0.1234)
    ax2.yaxis.set_ticks(np.arange(end, start+1, 0.5) * 2 / 0.1234)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax2.tick_params(axis='both', which='both', direction='out', length=5)


    ax.grid()
    ax.set_title('BH1')
    legend = ax.legend(shadow=True, frameon=True, bbox_to_anchor=(1.05, 0.4),
                       bbox_transform=ax.transAxes, fancybox=True)
    legend = ax.legend(shadow=True, frameon=True, fancybox=True)
    legend.set_zorder(100)
    fig.set_tight_layout(True)
    plt.show()

    letters = 'abcdef'
    p.snapshot(f'fig3_{letters[i]}_iter_{i}.pdf', save_dir=odir+'figures/')
    fig.savefig(odir + f'figures/fig3_{letters[i+3]}_iter_{i}_plot.pdf', transparent=True, bbox_inches='tight')


#%%
ant = Antenna()
try:
    ant.load_hdf5(file_real)
except:
    traces = 50
    borehole = np.zeros((traces, 3))
    borehole[:, 0] = -6
    borehole[:, -1] = np.linspace(-4, 4, traces)

    spacing = np.linalg.norm(borehole[0] - borehole[-1]) / (traces - 1)
    depths = np.linspace(0, np.linalg.norm(borehole[0] - borehole[-1]), traces)

    borehole1 = pd.DataFrame({'depth': np.flip(depths),
                              'X': borehole[:, 0],
                              'Y': borehole[:, 1],
                              'Z': borehole[:, 2]})

    orient = np.zeros(((traces, 3)))
    orient[:, -1] = 1

    p1 = np.array([-2.5, -3, -4])
    p2 = np.array([-6, 3, 4])

    n_points = 50  # Number of points you want to generate
    spacing = np.linalg.norm(p2 - p1) / (n_points - 1)
    orientation = np.tile((p2 - p1) / np.linalg.norm(p2 - p1), (n_points, 1))

    depths = np.linspace(0, np.linalg.norm(p2 - p1), n_points)
    points = np.array([p1 + (p2 - p1) * d / np.linalg.norm(p2 - p1) for d in depths])

    borehole2 = pd.DataFrame({'depth': np.flip(depths), 'X': points[:, 0], 'Y': points[:, 1], 'Z': points[:, 2]})

    ant.set_profile('BH1', receivers=borehole, transmitters=borehole, orient_receivers=orient, orient_transmitters=orient,
                    depth_Rx=borehole1['depth'].to_numpy(), depth_Tx=borehole1['depth'].to_numpy())
    ant.set_profile('BH2', receivers=points, transmitters=points, orient_receivers=orientation, orient_transmitters=orientation,
                    depth_Rx=borehole2['depth'].to_numpy(), depth_Tx=borehole2['depth'].to_numpy())
    ant.export_to_hdf5(file_initial, overwrite=True)
    ant.export_to_hdf5(file_real, overwrite=True)
    ant.export_to_hdf5(file_inverted, overwrite=True)
#%%
# Convert the previous distance to time
# measured_travel_time = np.ravel([a[:,1] for a in mean_val.values()]) * 2 / solv_c.velocity
measured_time = dict()
# for bn in boreholes:
for bn in ant.name_profiles:
    measured_time[bn] = mean_val[bn] * 2 / solv_c.velocity
from fracwave.inversion.geometry_inversion import GeometryInversion
inv2 = GeometryInversion(gempy_extent=gempy_extent,
                        gempy_resolution=resolution,
                        velocity=solv_c.velocity,
                        antenna=ant,
                        measured_travel_time=measured_time)

#%% Run inversion
try:
    inverted_points = pd.read_csv(odir+'inverted_points.csv')
except:
    inv2.clear()
    inv2.tol_pos = np.hstack([e * 2 / solv_c.velocity for e in error_trace.values()])   # Tolerance for a single trace
    inv2.tol_all = error_all * 2 / solv_c.velocity  # Convert to time. Tolerance for overall RMS error over the whole picks
    inv2.surrounding = 2
    inv2.max_iter_all = 20
    inv2.max_iter_pos = 15
    # i2nv.measured_travel_time =
    inv2.fixed_control_points = df_fixed
    inv2.fixed_orientations = df_orientations
    inv2.temp_control_points = df_plane
    inv2.directory = odir
    inv2.kwargs_remesh = dict(max_element_size=max_element_size, extrapolate=False, plane=0)

    inverted_points = inv2.run_inversion()
    inverted_points.to_csv(odir+'inverted_points.csv')

inverted_points.reset_index(drop=True, inplace=True)


#%%
p = model.ModelPlot()
# points = ant.profiles[['Tx','Ty','Tz']].to_numpy()
# p.add_object(points, name='Tx', color='Red')
# bor = pv.PolyData(ant.Transmitter)
# p.add_object(bor, name='Tx', color='Red')
# projborx = bor.project_points_to_plane(origin=(0, 0, extent[4] - 1), normal=(0, 0, 1))
# p.add_object(projborx, name='projTx2', color='Red', opacity=0.2)
# projborz = bor.project_points_to_plane(origin=(0, extent[3] + 1, 0), normal=(0, 1, 0))
# p.add_object(projborz, name='projTx3', color='Red', opacity=0.2)
for i in ['BH1', 'BH2']:
    points = ant.profiles.loc[ant.profiles['profile'] == i, ('Tx', 'Ty', 'Tz')].to_numpy()

    bor = tube_from_points(points,
                           radius=0.05)
    p.add_object(bor, name=f'Tx{i}', color='Red', opacity=0.8)
    projborx = bor.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
    p.add_object(projborx, name=f'projTx2{i}', color='Red', opacity=0.2)
    projborz = bor.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
    p.add_object(projborz, name=f'projTx3{i}', color='Red', opacity=0.2)
    # p.add_object(ant.Receiver, name='Rx', color='blue')

    p.add_label(points=ant.profiles.loc[
        (ant.profiles['depth_Tx'] == 0) & (ant.profiles['profile'] == i), ('Tx', 'Ty', 'Tz')].to_numpy(),
                labels=[i], name=i, point_size=10, point_color='black', font_size=45)

# p.add_object(ant.Receiver, name='Rx', color='blue')
prev = frac2.vertices
# inverted_points_bh1 = inverted_points_bh1.reset_index(drop=True)
# inverted_points = inverted_points.reset_index(drop=True)

inv2.fixed_control_points = inverted_points
inv2.fixed_orientations = df_orientations
inv2.temp_control_points = None
first_arrival, min_position = inv2.forward_solver()

fix = inverted_points[1:]
p.add_label(points=fix[['X','Y','Z']].to_numpy(), labels=[f' {k} ' for k, p in fix.iterrows()], name='cp', point_size=10,
            point_color='black', font_size=45, always_visible=True)
# Add projected control points to planes
po = pv.PolyData(fix[['X','Y','Z']].to_numpy())
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p.add_object(projectedxy, name='PointProjected', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p.add_object(projectedz, name='PointProjected2', point_size=10)



surf = pv.PolyData(inv2.vertices, np.hstack([np.hstack([len(f), f]) for f in inv2.faces]))
# if 'initial' in fn: continue
distt = np.linalg.norm(prev - surf.points, axis=1)
# p.add_object(surf, name='Mesh', color='blue', opacity=0.5)
surf['Distance (m)'] = distt
p.add_object(surf, name='Mesh', scalars='Distance (m)', clim=(0,cmax), cmap='turbo', show_scalar_bar=False)
# p.plotter.add_scalar_bar(title='Distance (m)\n', title_font_size=40, label_font_size=40, vertical=True,
#                          height=0.6, position_x=0.88, position_y=0.3)
p.plotter.add_scalar_bar(title=f'Distance misfit [{distt.min():.2f}-{distt.max():.2f}] (m)', title_font_size=40,
                             label_font_size=40, vertical=False,
                             position_x=0.3, position_y=0)

projected = surf.project_points_to_plane(origin=(0, 0, gempy_extent[4]-1), normal=(0, 0, 1))
p.add_object(projected, name='MeshProjected', opacity=0.2)
projected2 = surf.project_points_to_plane(origin=(0, gempy_extent[3]+1, 0), normal=(0, 1, 0))
p.add_object(projected2, name='MeshProjected2', opacity=0.2)

    # fix = fixed_points_geometry.reset_index(drop=True)[:i+2]

p.add_label(points=df_fixed.to_numpy(), labels=[' Fixed '], name='cp_fixed', point_size=10, point_color='black', font_size=45, always_visible=True)
po = pv.PolyData(df_fixed.to_numpy())
projectedxy = po.project_points_to_plane(origin=(0, 0, gempy_extent[4] - 1), normal=(0, 0, 1))
p.add_object(projectedxy, name='PointProjected3', point_size=10)
projectedz = po.project_points_to_plane(origin=(0, gempy_extent[3] + 1, 0), normal=(0, 1, 0))
p.add_object(projectedz, name='PointProjected4', point_size=10)

arrow = pv.Arrow(df_fixed.to_numpy(), direction = normal)
p.add_object(arrow, name='arrow', color='blue')

p.add_label(points=[ant.profiles.loc[ant.profiles['profile']=='BH1', ('Tx','Ty','Tz')].to_numpy()[-1]],
            labels=['BH 1'], name='b1', point_size=10, point_color='black', font_size=45)
p.add_label(points=[ant.profiles.loc[ant.profiles['profile']=='BH2', ('Tx','Ty','Tz')].to_numpy()[-1]],
            labels=['BH 2'], name='b2', point_size=10, point_color='black', font_size=45)

p.plotter.window_size = window_size
p.plotter.camera_position = cam
p.show(**show_kwargs)
p.plotter.add_axes(box=True)

p.snapshot(f'fig5_a_synthetic_final_geometry.pdf', save_dir=odir+'figures/')


#%%
#########

fig, AX = plt.subplots(2, 1, figsize=(10, 8))
prev = frac2.vertices
inverted_points_bh1 = inverted_points_bh1.reset_index(drop=True)
inverted_points = inverted_points.reset_index(drop=True)

index_bh1 = ant.profiles.loc[ant.profiles['profile']=='BH1'].index.to_numpy(dtype=int)
index_bh2 =ant.profiles.loc[ant.profiles['profile']=='BH2'].index.to_numpy(dtype=int)

inv2.fixed_control_points = df_fixed
inv2.fixed_orientations = df_orientations
inv2.temp_control_points = df_plane
first_arrival_plane, min_position = inv2.forward_solver()

distance_true_bh1 = mean_val['BH1'][:,1]
distance_true_bh2 = mean_val['BH2'][:,1]

inv2.fixed_control_points = inverted_points
inv2.fixed_orientations = df_orientations
inv2.temp_control_points = None
first_arrival_inverted, min_position = inv2.forward_solver()

distance_plane = first_arrival_plane * 0.1234 / 2

distance_inverted = first_arrival_inverted * 0.1234 / 2


dep1 = ant.profiles.loc[ant.profiles['profile']=='BH1', ('depth_Tx')].to_numpy()
AX[0].plot(dep1,distance_plane[index_bh1], color='red', label='Initial model', linestyle='dotted')
AX[0].plot(dep1,distance_true_bh1, color='green', label='Ground-truth', linestyle='dotted')
AX[0].plot(dep1,distance_inverted[index_bh1], color='blue', label='Updated model')
dep2 = ant.profiles.loc[ant.profiles['profile']=='BH2', ('depth_Tx')].to_numpy()
AX[1].plot(dep2,distance_plane[index_bh2], color='red', label='Initial model', linestyle='dotted')
AX[1].plot(dep2,distance_true_bh2, color='green', label='Ground-truth', linestyle='dotted')
AX[1].plot(dep2,distance_inverted[index_bh2], color='blue', label='Updated model')


for name, ax in zip(['BH1','BH2'], AX):
    if name == 'BH1':
        ylims = (7, 4)
        xlims = (0, 8)
        xspace = 1
        yspace = 0.5
    elif name== 'BH2':
        ylims = (7, 1)
        xlims = (0, 10.5)
        xspace = 1
        yspace = 1
    else:
        raise AttributeError
    ax.set_ylim(ylims[0], ylims[1])
    ax.set_xlim(xlims[0], xlims[1])
    ax.yaxis.set_ticks(np.arange(min(ylims), max(ylims) + yspace, yspace))
    ax.xaxis.set_ticks(np.arange(min(xlims), max(xlims) + xspace, xspace))

    ax.set_xlabel('Borehole Depth (m)')
    ax.set_ylabel('Radial distance (m)')

    ax2 = ax.twinx()
    ax2.set_ylabel('Two-way travel-time (ns)')
# start, end = distance_plot[1] * 2 / 0.1234, distance_plot[0] * 2 / 0.1234
    ax2.set_ylim(ylims[0] * 2 / 0.1234, ylims[1] * 2 / 0.1234)
    ax2.yaxis.set_ticks(np.arange(min(ylims), max(ylims) + yspace, yspace) * 2 / 0.1234)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax2.tick_params(axis='both', which='both', direction='out', length=5)

    ax.grid()
    ax.set_title(name)
    # legend = ax.legend(shadow=True, frameon=True, bbox_to_anchor=(1.05, 0.4),
    #                    bbox_transform=ax.transAxes, fancybox=True)
    legend = ax.legend(shadow=True, frameon=True, fancybox=True, loc='upper left')
    legend.set_zorder(100)
plt.tight_layout()
plt.show()
fig.savefig(odir + 'figures/fig5_b_synthetic_all_inverted_travel_times.pdf')

#%% Now the new geometry
frac3 = FractureGeo()
try:
    frac3.load_hdf5(file_inverted)
except:
    v, _ = inv2.frac.create_gempy_geometry(fixed_points=inverted_points,
                                      fixed_orientations=df_orientations,
                                      move_points=None)
    grid_inverse, vertices_inverse, faces_inverse = inv2.frac.remesh(v, max_element_size=max_element_size, extrapolate=False, plane=0)


    dfinverted = frac3.set_fracture('Inverted', vertices=vertices_inverse,
                                    faces=faces_inverse, **kwargs_fracture_properties)
    frac3.export_to_hdf5(file_inverted, overwrite=True)

#%%
solv_i = FracEM()
try:
    solv_i.load_hdf5(file_inverted)
    freq_i = solv_i.get_freq()
except:
    solv_i.rock_epsilon = 5.9
    solv_i.rock_sigma = 0.0
    solv_i.backend = 'torch'
    solv_i.engine='tensor_trace'
    solv_i.open_file(file_inverted)
    solv_i.time_zero_correction()

    freq_i = solv_i.forward_pass()

#%%
fig = plt.figure(constrained_layout=True, figsize=(8,10))
subfig = fig.subfigures(nrows=1, ncols=1)
AX = subfig.subplots(nrows=1, ncols=2)

for ax, n in zip(AX, solv_i.name_profiles):
    solv_i.backend = 'numpy'
    solv_c.backend = 'numpy'
    freq = solv_i.get_freq(name_profile=n)
    freq2 = solv_c.get_freq(name_profile=n)
    # freq = solv_p.get_freq(name_profile=n)
    time_response, time_vector = solv_i.get_ifft(freq)
    time_response /= np.abs(time_response).max()
    time_response2, time_vector2 = solv_c.get_ifft(freq2)
    time_response2 /= np.abs(time_response2).max()

    time_response = np.abs(time_response) - np.abs(time_response2)
    noise = hist_distribution.rvs(size=time_response.shape)
    time_response += noise*0.15
    time_response /= np.abs(time_response).max()
    # time_response, time_vector = solv_p.get_ifft(freq)
    radial_distance = time_vector * solv_c.velocity / 2
    ant_position = ant.profiles.loc[ant.profiles['profile']==n, ('depth_Tx')].to_numpy()
    # extent = (ant_position[0], ant_position[-1], radial_distance[-1], radial_distance[0])
    extent = (radial_distance[0], radial_distance[-1], ant_position[-1], ant_position[0])
    cax = ax.imshow(time_response, aspect='auto', cmap='seismic', vmax=1,vmin=-1, extent=extent)
    ax.set_ylabel('Borehole depth (m)')
    ax.set_xlabel('Radial distance (m)')
    # xlims = (2, 10)
    # ylim = (10.5, 0)
    if n == 'BH1':
        ylims = (8,0);xlims = (2, 10)
    elif n == 'BH2':
        ylims = (10.5, 0);xlims = (2, 10)
    else:
        n = None
    ax.set_xlim(xlims[0], xlims[1])
    ax.set_ylim(ylims[0], ylims[1])

    ax1 = ax.twiny()
    ax1.set_xlabel('Two-way travel-time (ns)')
    start, end = xlims[0] * 2 / solv_c.velocity, xlims[1] * 2 / solv_c.velocity

    ax1.set_xlim(start, end)
    ax.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2))
    ax1.xaxis.set_ticks(np.arange(xlims[0], xlims[1], 2) * 2 / solv_c.velocity)

    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    ax.set_title(n)
    ax.grid(linestyle='--')

subfig.colorbar(cax, ax=AX, label='Norm. GPR Amplitude (-)', orientation='horizontal')

# fig.tight_layout()
# fig.savefig(odir+'figures/synthetic_boreholes_simulation.pdf')
plt.show()