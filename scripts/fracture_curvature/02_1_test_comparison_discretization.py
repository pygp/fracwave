import numpy as np
from fracwave import FractureGeo, OUTPUT_DIR, Antenna, SourceEM, FracEM
from bedretto import model
import pyvista as pv
import matplotlib.pyplot as plt
import pandas as pd
from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip

file = OUTPUT_DIR + 'fracture_curvature/curve_fracture_borders_test.h5'
# file = '/home/daniel/GitProjects/fracwave/output/fracture_curvature/curve_fracture_borders_test.h5'

window_size = (1050,1700)
cam  = [(-66.0672720642332, -90.25728299069901, 12.389921568324116),
 (5.91389718566417, -2.516198335938955, -4.362908164462409),
 (0.08403615707045056, 0.11993459487587896, 0.9892186903090687)]

show_kwargs = dict(xtitle_offset=0.02,
                   ztitle_offset=0.02,
                   ytitle_offset=0.1,
                   xtitle='X (m)',
                   ytitle='Y (m)',
                   ztitle='Z (m)',
                   xrange=(0, 14),
                   number_of_divisions=10)

#%%
sou = SourceEM()
try:
    sou.load_hdf5(file)
except:
    sou.type = 'ricker'
    sou.set_time_vector(np.linspace(0, 200, 501))
    sou.set_center_frequency(0.08)
    sou.set_delay(10)
    source = sou.create_source()

    fig = sou.plot_waveforms_complete()
    fig.show()

    fig = sou.plot_waveforms_zoom()
    fig.show()

    sou.export_to_hdf5(file, overwrite=True)

    # max_element_size = 0.1234 / sou.peak_frequency / 15
max_element_size = 0.1234 / sou.center_frequency / 6
#%% Borehole Antennas
ant = Antenna()
try:
    ant.load_hdf5(file)
except:
    traces = 5
    # traces = 200
    pos = np.linspace(-25, 25, traces)
    tx = np.zeros((traces, 3))
    tx[:, -1] = pos

    orient = np.zeros((traces, 3))
    orient[:,-1] = 1

    ant.set_profile('Prof1', transmitters=tx, receivers=tx, orient_transmitters=orient, orient_receivers=orient)

    ant.export_to_hdf5(file, overwrite=True)

#%%
# we need 5 control points to create a fracture
points1 = np.asarray([[10, 0, 0],
                      [10, 10, 0],
                      [10, -10, 0],
                      [10, 0, 10],
                      [10, 0, -10]
                     ])

points2 = np.asarray([[10, 0, 0],
                      [7, 10, 0],
                      [7, -10, 0],
                      [10, 0, 10],
                      [10, 0, -10]
                     ])

points3 = np.asarray([[10, 0, 0],
                      [10, 10, 0],
                      [10, -10, 0],
                      [7, 0, 10],
                      [7, 0, -10]
                     ])

normal = get_normal_vector_from_azimuth_and_dip(azimuth=90, dip=90)
orientations = pd.DataFrame.from_dict({'X': [0], 'Y': [0], 'Z': [0],
                                       'G_x': [normal[0, 0]], 'G_y': [normal[0, 1]], 'G_z': [normal[0, 2]]})

extent = [0, 15, -15, 15, -15, 15]

points = points2

# p.add_object(pv.PolyData(points), name='cp')
# p.add_label(points=points, labels=[str(i) for i in range(len(points))], name='cp', font_size=30)
#%%
# import pyvista as pv
#
# azimuth = 45
# dip = 30
# from fracwave.geometry.vector_calculus import get_orientation_of_plane
#
# n, tx, ty = get_orientation_of_plane(azimuth, dip)  # TODO: Deprecated
# plan1 = pv.Plane(center=(0,0,0), direction=n)
#
# from fracwave.geometry.vector_calculus import get_normal_vector_from_azimuth_and_dip
#
# nz2 = get_normal_vector_from_azimuth_and_dip(azimuth, dip)
# plan2 = pv.Plane(center=(0,0,0), direction=nz2)
#
# #%%
# p2 = model.ModelPlot()
# p2.add_object(plan1, name='plane1', color='red', opacity=0.5)
# p2.add_object(plan2, name='plane2', color='blue', opacity=0.5)

#%%
frac = FractureGeo()
try:
    frac.load_hdf5(file)
except:
    frac.init_geo_model(extent=extent)
    for i, points in enumerate([points1, points2, points3]):
        fixed_points = pd.DataFrame(points, columns=[['X','Y','Z']])
        vertices, faces = frac.create_gempy_geometry(fixed_points=fixed_points, fixed_orientations=orientations)
        max_element_size = 0.1
        plane=0
        grid, vertices1, faces1 = frac.remesh(vertices, max_element_size=max_element_size,
                                              spacing=[10, 10],
                                              plane=plane,
                                              extrapolate=False)

        frac.set_fracture(f'Frac{i}', vertices=vertices1, faces=faces1,
                          aperture=0.005, electrical_conductivity=0, electrical_permeability=81,
                          overwrite=True, plane=plane)
        # break

    frac.export_to_hdf5(file, overwrite=True)

#%%
solv = FracEM()
try:
    solv.load_hdf5(file)
except:
    solv.open_file(file)
    solv.rock_epsilon = 5.90216
    solv.rock_sigma = 0.0
    solv.engine = 'tensor_trace'
    solv.mode = 'reflection'
    # # solv.engine = 'dummy'
    # solv.units = 'SI'
    # solv.backend = 'cupy'
    # solv.backend = 'numpy'
    solv.backend = 'torch'
    # # solv.backend = 'numpy'
    # solv._filter_energy = 0.01
    # solv.open_file(file)


    # # solv.load_hdf5(OUTPUT_DIR + 'full_model/full_model_variable.h5')
    # freq_t0 = solv.time_zero_correction()
    # solv.solve_for_fracture = 'Frac1'  # 'all'
    solv.solve_for_profile = 'all'
    solv.solve_for_fracture = 'Frac0'#'all'
    solv._fast_calculation_incoming_field = True
    solv.filter_fresnel = False
    solv.apply_causality = True
    solv.filter_energy = False
    solv._propagation_mode = 1
    solv._fracture_field = True
    solv._mask_frequencies = True
    solv._old_solver = True
    # solv._devices = None
    _ = solv.time_zero_correction()
    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, mask_frequencies=True)

#%%
import matplotlib.pyplot as plt

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

for i, n in enumerate(solv.name_fractures):
    # if n != 'Frac2':
    #     continue
    depth_vector = np.linspace(-25, 25, 200)

    freq = solv.get_freq_from_fracture(n)
    # freq = solv.file_read('summed_response')
    time_response, time_vector = solv.get_ifft(freq)
    time_response /= np.abs(time_response).max()

    distance = time_vector * solv.velocity * 0.5

    traces, samples = time_response.shape
    t = np.arange(traces)
    s = np.arange(samples)

    fig, ax = plt.subplots(figsize=(10,4), constrained_layout=True)

    vmax = np.abs(time_response).max()

    cax = ax.imshow(time_response.T,
                    aspect='auto',
                    cmap='seismic',
                    vmax=vmax,
                    vmin=-vmax,
                    extent=(depth_vector[0],depth_vector[-1], distance[-1], distance[0]),
                    interpolation='None'
                    )
    xlims = (-20, 20)
    ylims= (0,18)
    ax.set_xlim(min(xlims), max(xlims))
    ax.set_ylim(max(ylims), min(ylims))
    ax1 = ax.twinx()
    ax1.set_ylabel('Two-way travel-time (ns)')
    ax1.set_ylim(max(ylims) * 2 / solv.velocity, min(ylims)* 2 / solv.velocity)

    ax.set_xlabel('Borehole depth (m)')
    ax.set_ylabel('Radial distance (m)')
    ax.grid(linestyle='--')

    ax.set_xticks(np.arange(min(xlims), max(xlims)+1, 5))
    ax.set_yticks(np.arange(min(ylims), max(ylims)+1, 3))
    ax1.set_yticks(np.arange(min(ylims), max(ylims)+1, 3)* 2 / solv.velocity)
    ax.tick_params(axis='both', which='both', direction='out', length=5)
    ax1.tick_params(axis='both', which='both', direction='out', length=5)

    fig.colorbar(cax, ax=ax, label='Norm. GPR amplitude (-)', orientation='vertical',)
    plt.show()
    letter = 'def'
    # fig.savefig(OUTPUT_DIR+f'fracture_curvature/fig4_{letter[i]}_{n}_response_border.pdf', transparent=True)
#%% See the energy on the fracture from all fractures

elements = solv.file_read('simulation/fracture_field').numpy()
# Now we want to get the maximum
all_energy = np.zeros(solv.nelements)
for t in elements:
    mask = t > all_energy
    all_energy[mask] = t[mask]

#%% Create figures of fracture field sensitive area
p = model.ModelPlot()
points = ant.Transmitter
from bedretto.core.common_helpers import tube_from_points
bor = tube_from_points(points,
                       radius=0.2)
p.add_object(bor, name='Tx', color='Red', opacity=0.8)
p.add_object(points, name='Tx', color='Red', opacity=0.8)

for i, (n, poi) in enumerate(zip(solv.name_fractures, [points1, points2, points3])):
    # n = 'Frac1'
    surf = frac.get_surface(n)
    mask_elements = solv.file_read(f'mesh/geometry/fractures/{n}')
    energy = all_energy[mask_elements]
    energy /= energy.max()
    surf['energy'] = energy

    p.remove_scalar_bar()
    p.add_object(surf, name='Mesh', scalars='energy', cmap='inferno',show_scalar_bar=False, clim=(0.3,1))
    p.plotter.add_scalar_bar(title=f'Norm. energy (-)', title_font_size=25,
                             label_font_size=25, vertical=True,
                             position_x=0.86, position_y=0.4, )
    projected = surf.project_points_to_plane(origin=(0, 0, -25))
    p.add_object(projected, name='MeshProjected', opacity=0.2, show_scalar_bar=False)
    p.add_label(points=poi, labels=[str(i) for i in range(len(poi))], name='cp', font_size=30)
    poiproj = pv.PolyData(poi).project_points_to_plane(origin=(0, 0, -25))
    p.add_object(poiproj, name='PointsProjected',)
    # p.add_object(pv.PolyData(frac.midpoints), name='mesh')
    # Camera position:

    p.plotter.window_size = (1050,1700)
    p.plotter.camera_position = cam
    p.show(**show_kwargs)
    p.plotter.add_axes(box=True)
    letter = 'abc'
    p.snapshot(f'fig4_{letter[i]}_{n}_fracture_field_border.pdf', save_dir=OUTPUT_DIR+'fracture_curvature/')
    break

