"""
This introduces the laboratory with all MB and ST boreholes from the Valter volume.
"""
import numpy as np
import pandas as pd
import pyvista as pv
from bedretto import (data,
                      geometry,
                      model)
from fracwave import OUTPUT_DIR

odir = OUTPUT_DIR + 'fracture_curvature/real_inversion/figures/'

#%%
bor_dat = data.BoreholeData()
bor_geom = geometry.BoreholeGeometry(borehole_data=bor_dat)

boreholes_MB =  bor_dat.boreholes_MB
boreholes_ST =  bor_dat.boreholes_ST

# Load all borehole geometries
bh_data_all = {bn: bor_dat.get_borehole_data(borehole_name=bn,
                                             columns=['Depth (m)',
                                                      'Easting (m)',
                                                      'Northing (m)',
                                                      'Elevation (m)',
                                                      ])

               for bn in boreholes_MB + boreholes_ST}

bh_geom_all = {bn: bor_geom.construct_borehole_geometry(borehole_info=info,
                                                        radius=1)
               for bn, info in bh_data_all.items()}

labels = list(bh_data_all.keys())
points = np.asarray([bh.iloc[-1][['Easting (m)',
                                     'Northing (m)',
                                     'Elevation (m)']].to_numpy() for label, bh in bh_data_all.items()])

# Load tunnel mesh
tun_dat = data.TunnelData()
tun_geom = geometry.TunnelGeometry(tunnel_data=tun_dat)

bh_colors = {'MB1':'red',
             'MB2':'green',
             'MB3':'gray',
             'MB4':'green',
             'MB5':'green',
             'MB7':'gray',
             'MB8':'gray',
             'ST1':'red',
             'ST2':'green'}

tunnel_mesh = tun_geom.get_tunnel_mesh(TM_range=[1950,2100])
tunnel_markers = tun_geom.interpolate_tunnel_markers(TM_range=[1950,2100], step_size=50)
# Plot all data
p = model.ModelPlot()
p.add_object(tunnel_mesh, name='tunnel_mesh', color='gray', opacity=0.5)
# p.add_label(tunnel_markers, labels='TM markers', name='tunnel markers')

[p.add_object(mesh, color=bh_colors[key], name='borehole_{}'.format(key)) for key, mesh in bh_geom_all.items()]
# p.add_label(points=points,
#              labels=labels,
#              name='label_boreholes',
#              font_size=45,
#              point_size=1,
#             always_visible=True
#              )
#%%
offset =  {'MB1':(10,-4),
             'MB2':(-14,-15),
             'MB3':(-54,-8),
             'MB4':(-6,-20),
             'MB5':(15,-4),
             'MB7':(-40, -5),
             'MB8':(12,6),
             'ST1':(12,10),
             'ST2':(8,-6)}
[p.add_3d_label(points=poin,
             labels=labl,
             name=f'label_boreholes_{labl}',
                c=bh_colors[labl],
                offset=offset[labl],
                s=8) for poin, labl in zip(points, labels)]


# Add projections
def project_planes(mesh, name, p, color):
    mesh = mesh.extract_geometry()
    projectedxy = mesh.project_points_to_plane(origin=(0, 0, p.bounds[4]), normal=(0, 0, 1))
    p.add_object(projectedxy, name=f'{name}_xy', color=color, opacity=0.1)

    projectedxz = mesh.project_points_to_plane(origin=(0, p.bounds[3], 0), normal=(0, 1, 0))
    p.add_object(projectedxz, name=f'{name}_xz', color=color, opacity=0.1)

    projectedyz = mesh.project_points_to_plane(origin=(p.bounds[1], 0, 0), normal=(1, 0, 0))
    p.add_object(projectedyz, name=f'{name}_yz', color=color, opacity=0.1)

for key, mesh in bh_geom_all.items():
    project_planes(mesh, f'pb_{key}', p, bh_colors[key])

project_planes(tunnel_mesh, 'tunnel', p, 'gray')

p.show(yzgrid2=True)
p.plotter.window_size = (2000,2000)
p.plotter.camera_position = [(-388.0518813370975, -736.5142666857374, 1711.8448756806238),
 (-99.07973023235196, -40.630555187316226, 1300.51700896743),
 (0.1030005796561807, 0.47409220369572413, 0.874429793056837)]

p.plotter.add_axes(box=True)
p.snapshot('../fig6_b_BULGG.pdf', save_dir=odir)

#%% Load some files to display
import scipy.io as io
from gdp import DATA_DIR as gdpdat
import pickle

files = ['MB1_100MHz_200515.mat', 'ST1_100MHz_210218.mat']
# files = ['MB1_100MHz_200515.mat', 'MB5_100MHz_210324.mat']
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 60})
# fig, AX = plt.subplots(2,1,figsize=(25,10))
fig = plt.figure(constrained_layout=True, figsize=(20,10))
subfig = fig.subfigures(nrows=1, ncols=1)
AX = subfig.subplots(nrows=2, ncols=1)#, sharex=True)
for fi, ax1 in zip(files, AX):
    t = fi[:3]
    name = fi[:3]
    mat = io.loadmat(gdpdat + 'VALTER/' + fi)

    depth = mat['depth'][:, 0]
    if t == 'ST1':
        depth += 20
    processed_data = mat['processed_data']
    travel_time = mat['travel_time'][0]
    radius = mat['radius'][0]

    extent = (depth[0], depth[-1], radius[-1], radius[0])
    processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)

    # fig, ax1 = plt.subplots()
    im = ax1.imshow(processed_data, vmin=-1, vmax=1,
               extent=extent, cmap='seismic', aspect='auto')
    ax1.set_title(name + ' 100MHz')

    ax1.set_ylabel('Radial distance (m)')
    xlim = (20, 300)
    ylim = (60, 0)
    ax1.set_xlim(xlim[0], xlim[1])
    ax1.set_ylim(ylim[0], ylim[1])
    ax1.yaxis.set_ticks(np.arange(min(ylim), max(ylim)+1, 10))
    # xlim = (20, 290) if t == 'MB1' else (20, 220)
    ax1.xaxis.set_ticks(np.arange(min(xlim), max(xlim), 20))
    ax1.tick_params(axis='both', which='both', direction='out', length=5)
    # ax1.grid()

    ax2 = ax1.twinx()
    ax2.yaxis.set_ticks(np.arange(min(ylim), max(ylim)+1, 10) * 2 / 0.1234)
    ax2.invert_yaxis()
    ax2.set_ylabel('Travel-time (ns)')
    ax2.tick_params(axis='both', which='both', direction='out', length=5)

    ax1.grid()
    ax1.set_zorder(3)
    ax2.grid()
    ax2.set_zorder(3)
    xy = ((100, 25), (170, 15)) if t == 'MB1' else ((70,20), (135,18))
    xytext = (140, 40) if t == 'MB1' else (100,40)
    an = 'Major open fracture'
    ann=ax1.annotate(an,
                   xy=xy[0], xycoords='data',
                   xytext=xytext, textcoords='data',
                   size=25, va="center", ha="center",
                   bbox=dict(boxstyle="round4", fc="w"),
                   arrowprops=dict(arrowstyle="simple",
                                   connectionstyle="arc3,rad=-0.2",
                                   relpos=(0., 0.),  fc="k"
                                   ))

    ann = ax1.annotate(an,
                      xy=xy[1], xycoords='data',
                      xytext=xytext, textcoords='data',
                      size=25, va="center", ha="center",
                      bbox=dict(boxstyle="round4", fc="w"),
                      arrowprops=dict(arrowstyle="simple",
                                      connectionstyle="arc3,rad=0.2",
                                      relpos=(1., 0.),  fc="k"
                                      ),
                       )
ax1.set_xlabel('Borehole depth (m)')

subfig.colorbar(im, ax=AX, label='Norm. GPR Amplitude (-)')

fig.show()

fig.savefig(odir + 'fig6_c_MB1-ST1_GPR.pdf')


