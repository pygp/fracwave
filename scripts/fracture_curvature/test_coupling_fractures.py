# How can we avoid proving that we need coupling between fractures.
# When looking individual fractures, most of the energy is reflected to another areas, and not to the fracture areas.

import numpy as np
from fracwave import FractureGeo, OUTPUT_DIR, Antenna, SourceEM, FracEM
from bedretto import model
import pyvista as pv
import matplotlib.pyplot as plt

file = OUTPUT_DIR + 'fracture_curvature/curve_fracture.h5'
p = model.ModelPlot()
#%%
frac = FractureGeo()
frac.load_hdf5(file)

df = frac.fractures
name_fracture = 'Frac2'
dff = df.loc[df['name'] == name_fracture]

normal = np.vstack(dff[['nz']].to_numpy()[:, 0])#[::100]
midpoints = np.vstack(dff[['x', 'y', 'z']].to_numpy())#[::100]

#%%
frac.get_coupling_between_fractures(midpoints, normal)

#%%
e, i = midpoints.shape

a = np.broadcast_to(midpoints, (e, e, i))
b = np.transpose(a, axes=(1, 0, 2))

midpoints_v = b-a
norm = np.linalg.norm(midpoints_v, axis=-1)
midpoints_v = midpoints_v / norm[..., None]
# normal_v = np.broadcast_to(normal, (e, e, i))


# final = np.abs(np.einsum('abi, abi -> ab', midpoints_v, normal_v)) ** 2
final_p = np.abs(np.einsum('abi, ai -> ab', midpoints_v, normal))
final = final_p * final_p.T
# final[np.isnan(final)] = 0
final[np.isnan(final)] = 0

# assert np.allclose(final, final2.T)

coupling = final.max(axis=0)
#%% Check the coupling
pos2 = 20
pos1 = 40
m1 = midpoints[pos1]
m2 = midpoints[pos2]
n1 = normal[pos1]
n2 = normal[pos2]

md = m1-m2
md /= np.linalg.norm(md)

d1 = np.abs(np.dot(n1, md))
d2 = np.abs(np.dot(n2, md))

result = d1*d2

assert final[pos1, pos2] == result

#%%
surf = frac.get_surface(name_fracture)
surf['coupling'] = coupling
p.add_object(surf, name='mesh', scalars='coupling')
#%%
plt.imshow(final)
plt.colorbar()
plt.show()

