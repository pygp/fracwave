import scipy.io as io
from gdp import DATA_DIR as gdpdat
import pickle
import matplotlib.pyplot as plt
import numpy as np

file = 'MB1_100MHz_200515.mat'
plt.rcParams.update({'font.size': 60})

fig, ax = plt.subplots(figsize=(20,10))
mat = io.loadmat(gdpdat + 'VALTER/' + file)

depth = mat['depth'][:, 0]
processed_data = mat['processed_data']
travel_time = mat['travel_time'][0]
radius = mat['radius'][0]

extent = (depth[0], depth[-1], radius[-1], radius[0])
processed_data = np.divide(processed_data, np.abs(processed_data).max(axis=1)[:, None],
                               out=np.zeros_like(processed_data),
                               where=np.abs(processed_data).max(axis=1)[:, None] != 0)

# fig, ax1 = plt.subplots()
im = ax.imshow(processed_data, vmin=-1, vmax=1,
           extent=extent, cmap='seismic', aspect='auto')
ax.set_title('100MHz')

ax.set_ylabel('Radial distance (m)')
ax.set_xlim(10, 300)
ax.set_ylim(60, 0)
ax.yaxis.set_ticks(np.arange(0, 61, 10))
ax.xaxis.set_ticks(np.arange(10, 301, 20))
ax.tick_params(axis='both', which='both', direction='out', length=5)
# ax1.grid()

ax2 = ax.twinx()
ax2.yaxis.set_ticks(np.arange(0, 61, 10) * 2 / 0.1234)
ax2.invert_yaxis()
ax2.set_ylabel('Travel time (ns)')
ax2.tick_params(axis='both', which='both', direction='out', length=5)

ax.set_xlabel('Borehole depth (m)')

fig.colorbar(im, ax=ax, label='Norm. GPR Amplitude (-)')

fig.show()

#%%
from scipy.stats import rv_histogram

subset_of_noise = [1100, 1400, 5200, 5800]
subset = processed_data[subset_of_noise[0]:subset_of_noise[1], subset_of_noise[2]:subset_of_noise[3]]

plt.imshow(subset, cmap='seismic', aspect='auto');plt.show()
# Create a histogram of the noise data
histogram, bin_edges = np.histogram(subset, bins=100, )
plt.plot(bin_edges[0:-1], histogram); plt.show()

# Create a distribution based on the histogram
hist_distribution = rv_histogram((histogram, bin_edges))

# Generate random numbers from the distribution
sample = hist_distribution.rvs(size=subset.shape)
plt.imshow(sample, cmap='seismic', aspect='auto');plt.show()
