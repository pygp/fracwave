from fracwave import (OUTPUT_DIR, SourceEM,
                      FractureGeo, FracEM, Antenna)
from bedretto import model
import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt

file = OUTPUT_DIR + 'amplitude/amplitude.h5'
#%%
p = model.ModelPlot()
p.show()
#%% Source
v = 0.1234  # m/ns -> Velocity in granite
wavelength = v / 0.02  # m
max_element_size = wavelength / 6
sou = SourceEM()
try:
    sou.load_hdf5(file)
except:
    f = np.linspace(0, 0.25, 500)
    sou.frequency = f
    sou.create_source(t=-70)
    sou.export_to_hdf5(file, overwrite=True)
ax = sou.plot()
ax.figure.show()
#%%
# Antenna. 100MHz have a separation of 2.77 m from midpoint
ant = Antenna()
try:
    ant.load_hdf5(file)
except:
    # c = 2.77
    c=0
    m = np.asarray([[0,0,0]])
    Rx = m + [[0,0,c*0.5]]
    Tx = m - [[0,0,c*0.5]]
    orient_Rx = [[0,0,1]]

    ant.set_profile(name_profile='p1',
                    receivers=Rx,
                    transmitters=Tx,
                    orient_transmitters=orient_Rx,
                    orient_receivers=orient_Rx)
    ant.export_to_hdf5(file, overwrite=True)
#%%
Tx_arrows = pv.Arrow(start=ant.Transmitter[0], direction=ant.orient_Transmitter[0], scale=1)
Rx_arrows = pv.Arrow(start=ant.Receiver[0], direction=ant.orient_Receiver[0], scale=1)
p.add_object(Tx_arrows, name='orientation_Tx', color='red')
p.add_object(Rx_arrows, name='orientation_Rx', color='blue')
p.add_object(pv.PolyData(ant.Transmitter), name='Tx', color='red')
p.add_object(pv.PolyData(ant.Receiver), name='Rx', color='blue')

#%% Create the element
midpoint = np.asarray([3, 0, 0])
ed = (1 / 4) * max_element_size * 0.5
vertices = np.zeros((4, 3))
vertices[0] = np.asarray([[midpoint[0], midpoint[1]-ed, midpoint[2]-ed]])
vertices[1] = np.asarray([[midpoint[0], midpoint[1]-ed, midpoint[2]+ed]])
vertices[2] = np.asarray([[midpoint[0], midpoint[1]+ed, midpoint[2]+ed]])
vertices[3] = np.asarray([[midpoint[0], midpoint[1]+ed, midpoint[2]-ed]])

faces = np.array([[0,1,2,3]])

from fracwave.geometry.vector_calculus import rotate_points_according_to_dip_and_azimuth, arbitrary_rotation

vertices = rotate_points_according_to_dip_and_azimuth(vertices,
                                                      azimuth=0,
                                                      dip=90)

frac = FractureGeo()
frac.vertices = vertices
frac.faces = faces

frac.set_fracture_properties(aperture=0.0005,
                             electrical_conductvity=0.001,
                             electrical_permeability=81,)

frac.export_to_hdf5(file, overwrite=True)
#%%
p.add_object(frac.surface, color='black', opacity=0.5, name='vertices')

# p.add_object(pv.PolyData(frac.vertices), color='black', opacity=0.5, name='vertices')

# %% Solve
solv = FracEM()

solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
# solv.engine = 'dummy'
# solv.units = 'SI'
# solv.backend = 'torch'
solv.backend = 'numpy'
solv.open_file(file)
# freq_t0 = solv.time_zero_correction()

#%%
freq = solv.forward_pass(save_hd5=True, mask_frequencies=False)
#%% Mesh
nx = solv.file_read('mesh/geometry/nx', sl=0)
ny = solv.file_read('mesh/geometry/ny', sl=0)
nz = solv.file_read('mesh/geometry/nz', sl=0)

nx_arr = pv.Arrow(start=midpoint, direction=nx, scale=0.2)
ny_arr = pv.Arrow(start=midpoint, direction=ny, scale=0.2)
nz_arr = pv.Arrow(start=midpoint, direction=nz, scale=0.2)
p.add_object(nx_arr, name='nx', color='red', opacity=0.3)
p.add_object(ny_arr, name='ny', color='green', opacity=0.3)
p.add_object(nz_arr, name='nz', color='blue', opacity =0.3)

#%%
p.remove('nz')
p.remove('nx')
p.remove('ny')
#%% Arriving to the elements: Incoming field
inc = solv.file_read('simulation/incoming_field', sl=(0,0))
inc = np.einsum('fi->i', inc).real
inc = inc/np.linalg.norm(inc)
inc_arr = pv.Arrow(start=midpoint, direction=inc, scale=1)
p.add_object(inc_arr, name='inc', color='orange', opacity=0.3)
#%%
p.remove('inc')
#%% Energy
nfz = solv.file_read('simulation/projected/nfz', sl=(0,0))
tfx = solv.file_read('simulation/projected/tfx', sl=(0,0))
tfy = solv.file_read('simulation/projected/tfy', sl=(0,0))
nfz = np.einsum('fi->i', nfz).real
tfx = np.einsum('fi->i', tfx).real
tfy = np.einsum('fi->i', tfy).real
norm = np.linalg.norm((nfz,tfx,tfy))

#%%
nfz_arr = pv.Arrow(start=midpoint, direction=nfz/norm, scale=np.linalg.norm(nfz)/norm)
tfx_arr = pv.Arrow(start=midpoint, direction=tfx/norm, scale=np.linalg.norm(tfx)/norm)
tfy_arr = pv.Arrow(start=midpoint, direction=tfy/norm, scale=np.linalg.norm(tfy)/norm)
p.add_object(nfz_arr, name='nfz', color='blue', opacity=0.3)
p.add_object(tfx_arr, name='tfx', color='red', opacity=0.3)
p.add_object(tfy_arr, name='tfy', color='green', opacity=0.3)
#%%
p.remove('nfz')
p.remove('tfx')
p.remove('tfy')

#%%
time_response, time_vector = solv.get_ifft(freq)
#%%
plt.plot(time_vector*solv.velocity, time_response[0])
plt.show()

#%% Plot all
dips = np.arange(0, 181, 5)  # Rotate the element all 180 deg
azimuths = np.arange(0, 181, 5)  # Rotate the element all 180 deg
amplitude = []
midpoint = np.asarray([3, 0, 0])
ed = (1 / 4) * max_element_size * 0.5
vertices = np.zeros((4, 3))
vertices[0] = np.asarray([[midpoint[0], midpoint[1]-ed, midpoint[2]-ed]])
vertices[1] = np.asarray([[midpoint[0], midpoint[1]-ed, midpoint[2]+ed]])
vertices[2] = np.asarray([[midpoint[0], midpoint[1]+ed, midpoint[2]+ed]])
vertices[3] = np.asarray([[midpoint[0], midpoint[1]+ed, midpoint[2]-ed]])

faces = np.array([[0, 1, 2, 3]])
frac = FractureGeo()
solv = FracEM()
solv.rock_epsilon = 5.90216
solv.rock_sigma = 0.0
solv.engine = 'tensor_trace'
# solv.engine = 'loop'
# solv.engine = 'dummy'
# solv.units = 'SI'
# solv.backend = 'torch'
solv.backend = 'numpy'

from fracwave import set_error_level
set_error_level('warning')
from fracwave.geometry.vector_calculus import rotate_points_according_to_dip_and_azimuth, arbitrary_rotation

arr = np.zeros((len(dips), len(azimuths)))

for i, d in enumerate(dips):
    for j, a in enumerate(azimuths):
        vertices_new = rotate_points_according_to_dip_and_azimuth(vertices.copy(),
                                                                  azimuth=a,
                                                                  dip=d)
        frac.vertices = vertices_new
        frac.faces = faces

        frac.set_fracture_properties(aperture=0.0005,
                                     electrical_conductvity=0.001,
                                     electrical_permeability=81, )

        frac.export_to_hdf5(file, overwrite=True)

        solv.open_file(file)
        # freq_t0 = solv.time_zero_correction()
        freq = solv.forward_pass(mask_frequencies=False)

        # time_response, time_vector = solv.get_ifft(freq)

        # amplitude.append(np.abs(freq).sum())
        amplitude.append([d, a, np.abs(freq).sum()])
        arr[i, j] = np.abs(freq).sum()
# p.add_object(frac.surface, color='black', opacity=0.5, name='vertices')

#%%
plt.plot(dips, amplitude)
# plt.xlabel('Azimuth (deg)')
plt.xlabel('Dip (deg)')
plt.ylabel('Energy')
# plt.ylim(0,3e-8)
plt.show()
#%%
plt.imshow(arr.T, extent=(0,180,0,180), aspect='auto', cmap='viridis')
plt.colorbar()
plt.show()
#%%
amplitude4 = np.asarray(amplitude).copy()
plt.scatter(amplitude4[:, 0], amplitude4[:, 1], c=amplitude4[:, 2], cmap='viridis')
plt.colorbar()
plt.xlabel('Dip (deg)')
plt.ylabel('Azimuth (deg)')
plt.show()

#%%
plt.contour(arr.T, cmap='viridis')
plt.colorbar()
plt.show()
