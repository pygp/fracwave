from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR
from bedretto import data, geometry, model
import pyvista as pv
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Qt5Agg')

file = OUTPUT_DIR + 'variable_aperture.h5'
p = model.ModelPlot(title='FracEM')
#%%
# Sources
sou = SourceEM()
sou.frequency = np.linspace(0,0.30, 200)
source_in = sou.create_source()

sou.export_to_hdf5(file, overwrite=True)
print(sou)

sou.plot(show=True)
rock_bedreto = 0.1236  # [m/ns] velocity of propagation
wavelength = rock_bedreto / sou.peak_frequency

#%%

traces = 15
ant = Antenna()
#ant.Tx = np.array([[0,0,-0.5]])
#ant.Rx = np.array([[0,0,0.5]])

z = np.zeros((traces, 3))
z[:,-1] = np.linspace(-3, 3, 15)
ant.Rx = ant.Tx = z

ant.orient_Rx = ant.orient_Tx = np.tile([0,0,1], (traces, 1))
#ant.orient_Tx = np.array([[0,0,1]])

sources = pv.PolyData(ant.Tx)
p.add_object(sources,name='Tx', color='red', label='Transmitter')

receivers = pv.PolyData(ant.Rx)
p.add_object(receivers, name='Rx', color='blue', label='Receiver')
p.show(xrange=[-1, 6])
#p.plotter.add_legend()
ant.export_to_hdf5(filename=file, overwrite=True)

#%%################################## Planar surface
resolution = (200,200)
geom = FractureGeo()
geom.create_regular_squared_mesh(width=14,
                                 length=14,
                                 resolution=resolution,
                                 dip=90,
                                 azimuth=90,
                                 radius=5,
                                 axis=0)
df = geom.set_fracture_properties(aperture=0.0005,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)

#geom.surface['aperture'] = geom.fracture_properties['aperture']
p.add_object(geom.surface, name='Fracture', color='yellow')# scalar_bar_args=dict(vmax=vmax, vmin=0))

geom.export_to_hdf5(filename=file, overwrite=True)

#%%
solv = FracEM()
try:
    solv.load_hdf5(file)
    freq1 = solv.file_read('simulation/summed_response')
except:
    solv.rock_epsilon = 6.0
    solv.rock_sigma = 0.0
    solv.engine = 'tensor'
    solv.backend = 'numpy'
    solv.open_file(file)
    freq1 = solv.forward_pass(overwrite=True, recalculate=True)

#%%
time_response, time_vector = solv.get_ifft(freq1)
time_response /= np.abs (time_response).max()
fig, ax = plt.subplots()
cax1 = ax.imshow(time_response.T, aspect='auto', vmax=1, vmin=-1, cmap='seismic', interpolation=None)
fig.colorbar(cax1, ax=ax)
ax.set_xlabel('Trace #')
ax.set_ylabel('Time (ns)')

fig.savefig(OUTPUT_DIR + 'variable_aperture_planar.png', transparent=True)

plt.show()

#%%################################## Calculation

correlation_length = 1.5 #[0.5, 1.5, 3]
H = 0.55 #[0.25, 0.55, 1]
n = 100
roughness=0.1

seed = 1234
np.random.seed(seed)
extent=[-7.5,7.5,-7.5,7.5]

#%%
#geom.plot_geometry(p=p.plotter, backend='pyvista',)
x = np.linspace(extent[0], extent[1], num=n)  # + np.random.uniform(-5, 5, size=n)
z = np.linspace(extent[0], extent[1], num=n)  # + np.random.uniform(-5, 5, size=n)
xx, zz = np.meshgrid(x, z)

X = np.random.randn(*[n, n])

from fracwave.geometry.fracture_geometry import FractureGeo

frac = FractureGeo()
yy = frac.generate_heterogeneous_apertures(20, 20, resolution,
                                           roughness,
                                           0.1,
                                           correlation_length,
                                           H)

        #yy = frac.generate_crf(X, [0, 20, 0, 20], roughness, lc, h)
plt.figure('Fig1')
cax = plt.imshow(yy, extent=extent, cmap='viridis')
plt.colorbar(cax)
plt.show()

frac2 = FractureGeo()
yy2 = frac2.generate_heterogeneous_apertures(20, 20, resolution,
                                           roughness,
                                           0.01,
                                           correlation_length,
                                           H)

        #yy = frac.generate_crf(X, [0, 20, 0, 20], roughness, lc, h)
plt.figure('Fig2')
cax2 = plt.imshow(yy2, extent=extent, cmap='viridis')
plt.colorbar(cax2)
plt.show()
#points = np.c_[xx.reshape(-1), yy.reshape(-1), zz.reshape(-1)]
#cloud = pv.PolyData(points)
#surf = cloud.delaunay_2d()
#%%
y_ = yy.copy()
vmax = np.abs(y_).max()
#y_ /= vmax
y_2 = yy2.copy()
#y_2 /= vmax

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(17,7))
cax1 = ax1.imshow(y_, extent=extent, vmax=vmax, vmin=0, cmap='viridis')
#fig.colorbar(cax1, ax=ax1)

cax2 = ax2.imshow(y_2, extent=extent, vmax=vmax, vmin=0, cmap='viridis')
#fig.colorbar(cax2, ax=ax2)

from mpl_toolkits.axes_grid1 import make_axes_locatable
divider = make_axes_locatable(ax2)
cax = divider.append_axes("right", size="5%", pad="2%")
norm = matplotlib.colors.Normalize(vmin=0, vmax=vmax)
mappeable = matplotlib.cm.ScalarMappable(norm=norm, cmap='viridis')
cbar = ax2.figure.colorbar(mappeable, cax=cax, ax=[ax1, ax2], format="%.1f", label='Aperture [mm]')

plt.show()

fig.savefig(OUTPUT_DIR + 'Combined_apertures.png', transparent=True)

#%% ############################################# High aperture
import shutil
file2 = OUTPUT_DIR + "variable_aperture_high.h5"
shutil.copyfile(file, file2)
geom = FractureGeo()
geom.create_regular_squared_mesh(width=20,
                                 length=20,
                                 resolution=resolution,
                                 dip=90,
                                 azimuth=90,
                                 radius=4,
                                 axis=0)
df = geom.set_fracture_properties(aperture=y_[:-1, :-1].ravel() / 1000,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)

geom.surface['aperture'] = geom.fracture_properties['aperture']
p.add_object(geom.surface, name='Fracture', scalars='aperture', clim=(0,vmax/1000))# scalar_bar_args=dict(vmax=vmax, vmin=0))

geom.export_to_hdf5(filename=file2, overwrite=True)

#%%
solv = FracEM()
try:
    solv.load_hdf5(file2)
    freq2 = solv.file_read('simulation/summed_response')
except:
    solv.rock_epsilon = 6.0
    solv.rock_sigma = 0.0
    solv.engine = 'tensor'
    solv.backend = 'numpy'
    solv.open_file(file2)
    freq2 = solv.forward_pass(overwrite=True, recalculate=True)

#%%
time_response, time_vector = solv.get_ifft(freq2)
time_response /= np.abs (time_response).max()
fig, ax = plt.subplots()
cax1 = ax.imshow(time_response.T, aspect='auto', vmax=1, vmin=-1, cmap='seismic', interpolation=None)
fig.colorbar(cax1, ax=ax)
ax.set_xlabel('Trace #')
ax.set_ylabel('Time (ns)')

fig.savefig(OUTPUT_DIR + 'variable_aperture_high.png', transparent=True)

plt.show()

#%% ############################################# Low aperture
import shutil
file3 = OUTPUT_DIR + "variable_aperture_low.h5"
shutil.copyfile(file, file3)
geom = FractureGeo()
geom.create_regular_squared_mesh(width=20,
                                 length=20,
                                 resolution=resolution,
                                 dip=90,
                                 azimuth=90,
                                 radius=4,
                                 axis=0)
df = geom.set_fracture_properties(aperture=y_2[:-1, :-1].ravel() / 1000,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)

geom.surface['aperture'] = geom.fracture_properties['aperture']
p.add_object(geom.surface, name='Fracture', scalars='aperture', clim=(0,vmax/1000))# scalar_bar_args=dict(vmax=vmax, vmin=0))

geom.export_to_hdf5(filename=file3, overwrite=True)

#%%
solv = FracEM()
try:
    solv.load_hdf5(file3)
    freq3 = solv.file_read('simulation/summed_response')
except:
    solv.rock_epsilon = 6.0
    solv.rock_sigma = 0.0
    solv.engine = 'tensor'
    solv.backend = 'numpy'
    solv.open_file(file3)
    freq3 = solv.forward_pass(overwrite=True, recalculate=True)

#%%
time_response, time_vector = solv.get_ifft(freq3)
time_response /= np.abs (time_response).max()
fig, ax = plt.subplots()
cax1 = ax.imshow(time_response.T, aspect='auto', vmax=1, vmin=-1, cmap='seismic', interpolation=None)
fig.colorbar(cax1, ax=ax)
ax.set_xlabel('Trace #')
ax.set_ylabel('Time (ns)')

fig.savefig(OUTPUT_DIR + 'variable_aperture_low.png', transparent=True)

plt.show()


#%% comparison all
time_response1, time_vector1 = solv.get_ifft(freq1)
time_response2, time_vector2 = solv.get_ifft(freq2)
time_response3, time_vector3 = solv.get_ifft(freq3)

vmax = np.abs(time_response1).max()

time_response1 /= vmax
time_response2 /= vmax
time_response3 /= vmax
vmax=0.2
fig, (ax1,ax2,ax3) = plt.subplots(1,3, figsize=(20,5))
cax1 = ax1.imshow(time_response1.T, aspect='auto', vmax=vmax, vmin=-vmax, cmap='seismic', interpolation=None)
cax2 = ax2.imshow(time_response2.T, aspect='auto', vmax=vmax, vmin=-vmax, cmap='seismic', interpolation=None)
cax3 = ax3.imshow(time_response3.T, aspect='auto', vmax=vmax, vmin=-vmax, cmap='seismic', interpolation=None)


[ax.set_xlabel('Trace #') for ax in [ax1,ax2,ax3]]
[ax.set_ylabel('Time (ns)') for ax in [ax1,ax2,ax3]]

from mpl_toolkits.axes_grid1 import make_axes_locatable
divider = make_axes_locatable(ax3)
cax = divider.append_axes("right", size="5%", pad="2%")
norm = matplotlib.colors.Normalize(vmin=-vmax, vmax=vmax)
mappeable = matplotlib.cm.ScalarMappable(norm=norm, cmap='seismic')
cbar = ax3.figure.colorbar(mappeable, cax=cax, ax=[ax1, ax2, ax3], format="%.1f",)


fig.savefig(OUTPUT_DIR + 'variable_aperture_all_responses.png', transparent=True)

plt.show()

#%% Plot apertures
ap1 = np.ones(resolution) * 0.5
ap2 = y_.copy()
ap3 = y_2.copy()

extent = (7.5, -7.5, -7.5, 7.5)
vmax = 0.5
fig, (ax1,ax2,ax3) = plt.subplots(1,3, figsize=(20,5))
cax1 = ax1.imshow(ap1, aspect='auto', vmax=vmax, vmin=0, cmap='viridis', extent=extent, interpolation=None)
cax2 = ax2.imshow(np.fliplr(ap2), aspect='auto', vmax=vmax, vmin=0, cmap='viridis', extent=extent, interpolation=None)
cax3 = ax3.imshow(np.fliplr(ap3), aspect='auto', vmax=vmax, vmin=0, cmap='viridis', extent=extent, interpolation=None)

[ax.plot(ant.Tx[:,-1],ant.Tx[:,0] , 'r*', markersize=5) for ax in [ax1,ax2,ax3]]
[ax.set_xlabel('Elevation (m)') for ax in [ax1,ax2,ax3]]
[ax.set_ylabel('Local Northing (m)') for ax in [ax1,ax2,ax3]]

from mpl_toolkits.axes_grid1 import make_axes_locatable
divider = make_axes_locatable(ax3)
cax = divider.append_axes("right", size="5%", pad="2%")
norm = matplotlib.colors.Normalize(vmin=0, vmax=vmax)
mappeable = matplotlib.cm.ScalarMappable(norm=norm, cmap='viridis')
cbar = ax3.figure.colorbar(mappeable, cax=cax, ax=[ax1, ax2, ax3], format="%.1f",label='Aperture (mm)')


fig.savefig(OUTPUT_DIR + 'variable_aperture_all_apertures.png', transparent=True)

plt.show()