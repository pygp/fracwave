import numpy as np
from gdp.processing.image_processing import pick_first_arrival
#%%
from gdp.import_export import load_mala
import matplotlib.pyplot as plt

time_zero, info2= load_mala("/home/daniel/Documents/OpenBis_Data/MB2-MB1/PROF58.RD3")

pick_time_zero = pick_first_arrival(time_zero, threshold=0.10)
fig, ax = plt.subplots()
ma = ax.imshow(time_zero, aspect='auto', cmap='seismic', interpolation=None)
ax.plot(pick_time_zero[:,0], pick_time_zero[:,1], '*g', markersize=10)
fig.colorbar(ma, ax=ax)
plt.show()

#%% Remove time_zero
vb = 0.299792458 # velocity of light in air m/ns
dt = 1 / (info2['frequency']/1000)  # ns / sample

sample_distance = np.round(np.mean(pick_time_zero[:, 1])).astype(int)

t = sample_distance * dt  # Time it takes to arrive to the first arrival

# Objective is 1m
d_o = 1
t_o = d_o / vb  # Convert the distance to time

s = np.round(t_o / dt).astype(int)  # Convert the time it takes to arrive to 1 m in samples

to_remove = sample_distance - s # Remove the excess of samples

#%% remove time zero
dat, info = load_mala("/home/daniel/Documents/OpenBis_Data/MB2-MB1/PROF56.RD3")

dat = dat[to_remove:]
plt.imshow(dat, aspect='auto', cmap='seismic'); plt.colorbar(); plt.show()

#%%
from gdp.plotting.plot import plot_frequency_information

fa = plot_frequency_information(dat[:, 1500], dt=1/info['frequency'])
# %% Basic processing
# Filtering
hp_20MHz = [10,100]

from gdp.processing import filter_data, apply_gain

dat_f = filter_data(dat,
                    fq = hp_20MHz,
                    sfreq=info['frequency'],
                    btype='bandpass',
                    N= 4,
                    filter_type = 'acausal')
vmax = dat_f.max()
plt.imshow(dat_f, aspect='auto', cmap='seismic', vmin=-vmax, vmax=vmax); plt.colorbar(); plt.show()

##%% SVD
#from gdp.processing.image_processing import remove_svd
#dat_svg, svd_decomp = remove_svd(dat_f, low_s=0, high_s=1)
#plt.imshow(dat_svg, aspect='auto', cmap='seismic'); plt.colorbar(); plt.show()

#%% Gain
dat_g, gm = apply_gain(dat_f,
                       sfreq=info['frequency'],
                       exponent=2,
                       gain_type='spherical',
                       velocity = 0.1249,
                       twoway=False)
vmax = dat_g.max()
fig, ax = plt.subplots()
ma = ax.imshow(dat_g, aspect='auto', cmap='seismic',
           vmin=-vmax, vmax=vmax, interpolation=None)
#fig.colorbar(ma, ax=ax)
#fig.savefig("temp_file.png")
ax.set_xlabel('Traces')
ax.set_ylabel('Time (ns)')
ax.set_ylim(1250, 0)
plt.show()

#%% Automatic picking

# dat_g = new_dat_f
pick = pick_first_arrival(dat_g, threshold=0.1)

vmax = dat_g.max()
fig, ax = plt.subplots()
ma = ax.imshow(dat_g, aspect='auto', cmap='seismic',
           vmin=-vmax, vmax=vmax, interpolation=None)
ax.plot(pick[:,0], pick[:,1], '*g', markersize=0.5)
ax.set_xlabel('Traces')
ax.set_ylabel('Time (ns)')
ax.set_ylim(1250, 0)
# fig.colorbar(ma, ax=ax)
plt.show()

#%%% Get the tunnel coordinates
samples, depths = dat_g.shape
from bedretto import data, geometry, model

bh = data.BoreholeData()

mb1 = bh.get_borehole_data('MB1')
mb2 = bh.get_borehole_data('MB2')
range_mb1 = (50, 215)
range_mb2 = (49.5, 215)
depths_mb1_rx = np.linspace(range_mb1[0], range_mb1[1], depths)
depths_mb2_tx = np.linspace(range_mb2[0], range_mb2[1], depths)
rx = bh.get_coordinates_from_borehole_depth(borehole_information=mb1, depths=depths_mb1_rx)
tx = bh.get_coordinates_from_borehole_depth(borehole_information=mb2, depths=depths_mb2_tx)

# Calculate orientation of point
orient_rx = (bh.get_coordinates_from_borehole_depth(mb1, depths=depths_mb1_rx - 0.1).T - \
             bh.get_coordinates_from_borehole_depth(mb1, depths=depths_mb1_rx + 0.1).T)
orient_rx /= np.linalg.norm(orient_rx, axis=1)[:, np.newaxis]

orient_tx = (bh.get_coordinates_from_borehole_depth(mb2, depths=depths_mb2_tx - 0.1).T - \
             bh.get_coordinates_from_borehole_depth(mb2, depths=depths_mb2_tx + 0.1).T)

orient_tx /= np.linalg.norm(orient_tx, axis=1)[:, np.newaxis]
#%% Geometries for plotting
bh_geom = geometry.BoreholeGeometry(bh)
mb1_geom = bh_geom.construct_borehole_geometry(borehole_info=mb1)
mb2_geom = bh_geom.construct_borehole_geometry(borehole_info=mb2)


mb1_rx = bh_geom.get_tube_along_borehole(borehole_name='MB1',
                                         depth_range=range_mb1)
mb2_tx = bh_geom.get_tube_along_borehole(borehole_name='MB2',
                                         depth_range=range_mb1)

#%%
p = model.ModelPlot(title='Geometry')
p.add_object(mb1_geom, 'MB1', color='black', label='MB1')
p.add_object(mb2_geom, 'MB2', color='green', label='MB2')
p.add_object(mb1_rx, 'rx', color='blue', label='receiver MB1')
p.add_object(mb2_tx, 'tx', color='red', label='transmitter MB2')

try:
    import pyvista as pv
    from bedretto import DATA_DIR
    bb_surf = pv.read(DATA_DIR + 'logging_data/bb_surf_boreholes_and_tunnel.vtk')
    p.add_object(bb_surf, 'fz1', color='yellow', label='FZ1', opacity=0.5)
except:
    print('FZ1 Not found')

p.plotter.add_legend()
#%% Simulation

from fracwave import OUTPUT_DIR
file = OUTPUT_DIR + 'MB1_MB2_20MHz.h5'

#%%
from fracwave import Antenna

ant = Antenna()

ant.Rx = rx
ant.Tx = tx

ant.orient_Rx = orient_rx
ant.orient_Tx = orient_tx

ant.export_to_hdf5(file, overwrite=True)

#%%
from fracwave import SourceEM

sou = SourceEM()
f = np.linspace(0, info['frequency'] / 2000, samples)
sou.frequency = f

# firstp = sou.create_source(a = 36.359,
#                            b= 5.1779,
#                            g=0.639,
#                            ph =-0.150,
#                            t=0.2107)
firstp = sou.create_source(a=1,
                           b=0.02,
                           t=5)
sou.plot()
plt.xlim(0,0.1)
plt.show()
sou.export_to_hdf5(file, overwrite=True)

#%%
from fracwave import FractureGeo
geom = FractureGeo()
# Create dummy geometry
mesh = geom.create_regular_squared_mesh(10,10,(5,5),90, 0, 0)
df = geom.set_fracture_properties(aperture=0.0005,
                                  electrical_conductvity=0.001,
                                  electrical_permeability=81)

geom.export_to_hdf5(file, overwrite=True)

#%%
from fracwave import OUTPUT_DIR
file = OUTPUT_DIR + 'MB1_MB2_20MHz.h5'

from fracwave import FracEM

solv = FracEM()
solv.rock_epsilon = 6.
solv.rock_sigma = 0.0
solv.engine = 'tensor_all'
solv.units = 'SI'

solv.units = 'SI'
solv.filter_energy = True
solv._filter_percentage = 0.5
solv.backend = 'torch'

solv.open_file(file)
#%%Come from checkpoint
from fracwave import SourceEM, FractureGeo, FracEM
#%%
solv.mode = 'propagation'
solv.filter_energy = False
dat = solv.forward_pass(True, True, None, True)

#%%
import numpy as np
import matplotlib.pyplot as plt

freqspec = np.einsum('tefi->tf', dat)
time_response, time_vector = solv.get_ifft(freqspec)
time_response = np.fliplr(time_response.T)
time_response /= np.abs(time_response).max()
plt.imshow(time_response, aspect='auto', cmap='seismic', vmin=-1,vmax=1, interpolation=None)
plt.show()

#%%
from gdp.processing.image_processing import pick_first_arrival

pick2 = pick_first_arrival(time_response, threshold=0.10)


fig, ax = plt.subplots()
ma = ax.imshow(time_response, aspect='auto', cmap='seismic',
           vmin=-1, vmax=1, interpolation=None)
#ax.plot(pick2[:,0], pick2[:,1], '*g', markersize=5)
ax.set_xlabel('Traces')
ax.set_ylabel('Time (ns)')
ax.set_ylim(1250, 0)
# fig.colorbar(ma, ax=ax)
plt.show()

#%% compare traces
sl = slice(100, 700)
fig, ax = plt.subplots()
ax.plot(dat_f[sl, 1000] / np.abs(dat_f[sl, 1000]).max() , 'b')
ax.plot(time_response[sl, 1000] / np.abs(time_response[sl, 1000]).max(), 'r')
plt.show()

#%%
new_dat_f = dat_f[to_remove:]
plt.imshow(new_dat_f, aspect='auto', cmap='seismic'); plt.colorbar(); plt.show()

#%%
sl = slice(50, 700)
fig, ax = plt.subplots()
ax.plot(new_dat_f[sl, 1000] / np.abs(new_dat_f[sl, 1000]).max() , 'b', label='Real')
ax.plot(time_response[sl, 1000] / np.abs(time_response[sl, 1000]).max(), 'r', label='Synthetic')
plt.legend()
plt.show()

#%%
from scipy.optimize import minimize
objective = new_dat_f / np.abs(new_dat_f).max()

trace = 1000

ant2 = Antenna()
ant2.Rx = ant.Rx[trace, None]
ant2.Tx = ant.Tx[trace, None]
ant2.orient_Rx = ant.orient_Rx[trace, None]
ant2.orient_Tx = ant.orient_Tx[trace, None]
ant2.export_to_hdf5(file, overwrite=True)

def fit_source(x):
    sl = slice(0, 400)
    trace = 1000
    sou.create_source(x[0], x[1], x[2], x[3], x[4])
    sou.export_to_hdf5(file, overwrite=True)

    solv.open_file(file)
    rep = solv.forward_pass(True, True, None, True)

    freqspec = np.einsum('tefi->tf', rep)
    time_response, time_vector = solv.get_ifft(freqspec)
    #time_response = np.fliplr(time_response.T)
    time_response /= np.abs(time_response).max()
    # plt.imshow(objective[sl] - time_response[sl], aspect='auto')
    # plt.show()
    plt.plot(time_response.T[sl], 'r')
    plt.plot(objective[sl, trace], 'b')
    plt.show()
    #cost_function = np.linalg.norm(objective[sl, trace] - time_response[sl, trace], ord=1)
    cost_function = np.linalg.norm(objective[sl, trace] - time_response.T[sl], ord=1)
    return cost_function

x0 = np.array([ 1, 0.02, 2, 0, 0])
bounds = [(0.1, 5),
          (0.01, 1),
          (1.2, 2.5),
          (-6.283185307179586, 6.283185307179586),
          (-20, -6)]

res = minimize(fit_source,
                x0,
                bounds=bounds,
               method='nelder-mead',
               options={'maxfun': 10,
                        'xatol': 1e-6,
                        'disp': False}
               )
#%%
import numpy as  np
dat, info = load_mala("/home/daniel/Documents/OpenBis_Data/MB2-MB1/PROF56.RD3")
dat = dat[to_remove:]
objective = np.fliplr(dat / np.abs(dat).max())

sl = slice(0, 500)
trace = 1000
x0 = np.array([ 1, 0.02, 2, 0, 0])
sou.create_source(x0[0], x0[1], x0[2], x0[3], x0[4])
sou.export_to_hdf5(file, overwrite=True)

solv.open_file(file)
rep = solv.forward_pass(True, True, None, True)

freqspec = np.einsum('tefi->tf', rep)
time_response, time_vector = solv.get_ifft(freqspec)
# time_response = np.fliplr(time_response.T)
time_response /= np.abs(time_response).max()

fig, ax = plt.subplots()
ax.plot(objective[sl, trace], 'b', label ='original')
ax.plot(time_response.T[sl], 'r', label='x0')

x1 = np.array([ 1, 0.02, 2, 0, -10])
sou.create_source(x1[0], x1[1], x1[2], x1[3], x1[4])
sou.export_to_hdf5(file, overwrite=True)

solv.open_file(file)
rep = solv.forward_pass(True, True, None, True)

freqspec = np.einsum('tefi->tf', rep)
time_response, time_vector = solv.get_ifft(freqspec)
# time_response = np.fliplr(time_response.T)
time_response /= np.abs(time_response).max()
ax.plot(time_response.T[sl], 'g', label='x1')
plt.legend()
plt.show()
#%%
dat, info = load_mala("/home/daniel/Documents/OpenBis_Data/MB2-MB1/PROF56.RD3")
dat = dat[to_remove:]

hp_20MHz = [10,100]

from gdp.processing import filter_data, apply_gain

dat_f = filter_data(dat,
                    fq = hp_20MHz,
                    sfreq=info['frequency'],
                    btype='bandpass',
                    N= 4,
                    filter_type = 'causal')

plt.plot(dat[sl, trace]/np.abs(dat[sl, trace]).max(), 'b', label='raw')
plt.plot(dat_f[sl, trace]/np.abs(dat_f[sl, trace]).max(), 'r', label='filter')
plt.legend()
plt.show()

#%%

fig, ax = plt.subplots()
ax.plot(pick[:,0], pick[:,1], '*b', label='Real')
ax.plot(pick2[:,0], pick2[:,1], '*r', label='Synthetic')
ax.set_xlabel('Traces')
ax.set_ylabel('Time (ns)')
ax.set_ylim(400,0)
plt.legend()
plt.show()


