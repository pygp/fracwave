{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7fd75207",
   "metadata": {},
   "source": [
    "## Welcome to the Source wavelet tutorial\n",
    "Welcome to the Source Wavelet Tutorial! This guide will walk you through the process of setting up the source wavelet for GPR antenna. In GPR, the source wavelet is a key component used to characterize the signal that is transmitted into the subsurface. It determines the frequency content and shape of the transmitted pulse. A well-chosen source wavelet is essential for obtaining clear reflections and accurate subsurface images during the inversion process. \n",
    "### Contents:\n",
    "1. [Choose the type of source function](#1)\n",
    "2. [Define the frequency vector and/or time vector](#2)\n",
    "3. [Define the center frequency and time delay](#3)\n",
    "4. [Compute the source wavelet](#4)\n",
    "5. [Plotting](#5)\n",
    "6. [The Generalized Gamma Distribution for source function modeling](#6)\n",
    "7. [Interactive widgets for source function creation](#7)  \n",
    "8. [Save data](#8)\n",
    "9. [Create user defined source function](#9)\n",
    "    1. [From time to frequency domain](#91)\n",
    "    2. [From frequency to time domain](#92)\n",
    "    3. [Save user defined source function](#93)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "78b3d18c",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:17.604670899Z",
     "start_time": "2023-07-30T14:40:44.722904792Z"
    }
   },
   "source": [
    "from fracwave import SourceEM\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "afd922cf",
   "metadata": {},
   "source": [
    "First we need to define the source wavelet. This is defined by using a generalized gamma function. But you can also create any source wavelet you want by defining the source function in the frequency domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7160d618",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:17.631855327Z",
     "start_time": "2023-07-30T14:41:17.575580800Z"
    }
   },
   "source": [
    "# Create and instance of the source wavelet\n",
    "sou = SourceEM()\n",
    "print(sou)  # Notice that here is all empty"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "b49eb2ff514bb3d2",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "We have incorporated various source functions into our implementation, including the commonly used Ricker wavelet and a versatile generalized gamma distribution. Defining the time vector or frequency vector is a crucial step in this process, where specifying the center frequency and time delay is of utmost importance. These parameters play a crucial role in shaping the characteristics of the source wavelet, determining its frequency content, and overall behavior.\n",
    "## 1. Choose the type of source function <a class=\"anchor\" id=\"1\"></a>\n",
    "Feel free to try out different source function shapes to check which one fits the best your data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f6688e2fc5a9ac57",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:17.679081259Z",
     "start_time": "2023-07-30T14:41:17.587339884Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "print(f\"Possible source wavelets include: {sou._option_wavelets}\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c98e184a0177fe08",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:17.719977561Z",
     "start_time": "2023-07-30T14:41:17.610030582Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou.type = 'ricker'"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "c421a610ef5664ab",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 2. Define the frequency vector and/or time vector <a class=\"anchor\" id=\"2\"></a>\n",
    "We've simplified the process by providing a convenient function that automatically generates the frequency vector from the constructed time vector, and vice versa. This function efficiently extracts all the essential values required to define the source function in both the time and frequency domains. With this functionality, you can seamlessly switch between representations and have greater flexibility in working with different aspects of the source wavelet. This streamlines the setup process and empowers you to focus on other critical aspects of your GPR inversion without getting bogged down in technical complexities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "24c900a8c4f60d71",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.079830719Z",
     "start_time": "2023-07-30T14:41:17.665780387Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Define the time vector\n",
    "time_window = 500  # ns\n",
    "nsamples = 1001 # number of samples\n",
    "\n",
    "time_vector = np.linspace(0, time_window, nsamples)\n",
    "sou.set_time_vector(time_vector)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "27c3cd03babf2662",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "That is all you need to do! Please take a moment to verify all the automatically generated parameters based on the class information. You can always access the same parameter values from the class instance.\n",
    "\n",
    "**Important note:** We intentionally chose to create an odd number of time samples for practical reasons. Since the source wavelet is symmetric, having the symmetry axis at sample 0 is crucial. When an even number of samples is used, the symmetry axis falls between two samples, which can lead to issues in the inverse Fourier transform (ifft) as we only consider positive frequencies in our calculations. By ensuring an odd number of samples, we guarantee the correct symmetry and avoid any potential problems during the inversion process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "12edb432c6a93070",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.088162692Z",
     "start_time": "2023-07-30T14:41:17.666233436Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Notice that nfrequencies = nsamples // 2 + 1, so if you use an even or odd number of time samples, in the frequency domain we assume it is an odd number of samples always. \n",
    "print(sou)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "4e39b00b3161f6c3",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "As stated before, we can also define the frequency vector to obtain the time vector. This is useful when you want to define the frequency content of the source wavelet only."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "e582f3153f432db9",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.101297109Z",
     "start_time": "2023-07-30T14:41:17.666679181Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sampling_freq = 2 # GHz\n",
    "n_frequencies = 501 # number of frequencies\n",
    "freq_vector = np.linspace(0, sampling_freq/2, n_frequencies)  # GHz, where sampling_freq/2 is the Nyquist frequency\n",
    "sou.set_frequency_vector(freq_vector)\n",
    "print(sou)  # it should look the same as before"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "ee4173807e02b013",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 3. Define the center frequency and time delay <a class=\"anchor\" id=\"3\"></a>\n",
    "The center frequency and time delay are two critical parameters that determine the shape and frequency content of the source wavelet. The center frequency is the frequency at which the source wavelet has the highest amplitude. The time delay is the time at which the source wavelet reaches its peak amplitude. These two parameters are used to define the source wavelet in both the time and frequency domains."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "fb6b04ff500ba54",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.102423998Z",
     "start_time": "2023-07-30T14:41:17.713839129Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "center_frequency = 0.1 # GHz, or 100 MHz, corresponding to the center frequency of the GPR antenna you want to use \n",
    "delay = 20  # ns. This is important since we want to capture the first arrival of the signal in the time window\n",
    "\n",
    "sou.set_center_frequency(center_frequency)\n",
    "sou.set_delay(delay)\n",
    "print(sou)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "3d3bd343a8bcfb5a",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 4. Compute the source wavelet <a class=\"anchor\" id=\"4\"></a>\n",
    "Having successfully defined all the parameters, we can proceed to compute the source wavelet using the ```sou.create_source()``` method to obtain the source function for the positive frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "cb6ed56061ef4752",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.102942223Z",
     "start_time": "2023-07-30T14:41:17.714219151Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "source = sou.create_source()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "54654b7e0c92a54f",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:18.628709680Z",
     "start_time": "2023-07-30T14:41:17.714452664Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "plt.plot(sou.frequency, np.abs(source), 'r')\n",
    "plt.xlabel('Frequency (GHz)')\n",
    "plt.ylabel('Amplitude (-)')\n",
    "plt.title('Source wavelet in frequency')\n",
    "plt.grid()\n",
    "plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "972037a206776161",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:19.392262630Z",
     "start_time": "2023-07-30T14:41:18.683370773Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Notice that if you want to visualize the negative frequencies as well, you can use the following code\n",
    "left_side = source.copy()\n",
    "right_side = np.flip(np.conj(source[1:]))\n",
    "full_source = np.concatenate((left_side, right_side))\n",
    "\n",
    "freq_p = sou.frequency\n",
    "freq_n = np.flip(freq_p[1:]*-1)\n",
    "full_freq = np.concatenate((freq_p, freq_n))\n",
    "\n",
    "plt.plot(full_freq, np.abs(full_source), 'r.')\n",
    "plt.xlabel('Frequency (GHz)')\n",
    "plt.ylabel('Amplitude spectrum (-)')\n",
    "plt.grid()\n",
    "plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "b8788e2ed62d31d5",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:20.548301574Z",
     "start_time": "2023-07-30T14:41:19.374719782Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# And if you want to check the source in the time domain, you can use the following code\n",
    "time_source = np.real_if_close(np.fft.ifft(full_source))\n",
    "plt.plot(sou.time, time_source, 'r')\n",
    "plt.xlabel('Time (ns)')\n",
    "plt.ylabel('Amplitude (-)')\n",
    "plt.title('Source wavelet in time')\n",
    "plt.grid()\n",
    "plt.show()\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "83d735ae42600c52",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Of course, we have already taken care of all the computations and storage, so you can effortlessly access the source wavelet in both the time domain and frequency domain:\n",
    "\n",
    "Frequency domain:\n",
    "* ```sou.frequency```: This provides access to the positive frequency vector.\n",
    "* ```sou.frequency_complete```: For further versatility, you can use this to access the positive and negative frequency vector, ensuring that the symmetry axis is at 0.\n",
    " \n",
    "Time domain:\n",
    "* ```sou.time```: Access the time vector\n",
    "\n",
    "Source wavelets:\n",
    "* ```sou.waveform_time```: This grants access to the source wavelet in the time domain.\n",
    "* ```sou.waveform_freq```: Here, you can access the source wavelet in the full frequency domain.\n",
    "* ```sou.source```: This one we use for our calculations since we are only interested in the positive frequencies. This is the source wavelet in the frequency domain.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "9c34de7ffdfe9dfc",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:21.319196893Z",
     "start_time": "2023-07-30T14:41:20.557337363Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "assert np.allclose(sou.waveform_time, time_source, atol=1e-6)\n",
    "assert np.allclose(sou.waveform_freq, full_source)\n",
    "assert np.allclose(sou.source, source)\n",
    "assert np.allclose(sou.frequency, freq_p)\n",
    "assert np.allclose(sou.frequency_complete, full_freq)\n",
    "assert np.allclose(sou.time, time_vector)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "d6c3fdba1ad4d1b",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 5. Plotting <a class=\"anchor\" id=\"5\"></a>\n",
    "In addition to providing you with an efficient setup process, we have implemented user-friendly plotting functionalities to streamline your methodology further. After creating the wavelets, you can easily visualize them with just a few lines of code, saving you time and effort."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "f21e23e62ab61129",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:22.604080310Z",
     "start_time": "2023-07-30T14:41:20.598662573Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Visualize the complete wavelet\n",
    "sou.plot_waveforms_complete()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "f981306a25a33439",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:24.023243961Z",
     "start_time": "2023-07-30T14:41:22.607326652Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Plot a zoomed version of the previos plot\n",
    "sou.plot_waveforms_zoom()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "d4b9189c6fbce90d",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 6. The Generalized Gamma Distribution for source function modeling <a class=\"anchor\" id=\"6\"></a>\n",
    "In certain scenarios, capturing the complexity of the frequency source function of the antenna becomes challenging, requiring a more flexible approach. To address this, we adopt a generalized gamma distribution to model the source function. Specifically, we generate a dipole moment for the antenna, which is characterized by two shape parameters (a, c), a location and scaling values (loc, scale), along with a complex phase (ph) and a time shift (t).\n",
    "\n",
    "* a: (>0) positive value controlling the shape of the dipole moment distribution\n",
    "* c: (!=0)\n",
    "* loc: location \n",
    "* scale: Scaling factor\n",
    "* ph: Orientation of the antenna\n",
    "* t: time shift of the signal\n",
    "\n",
    "Special cases are:\n",
    "* Weibull distribution (a=1)\n",
    "* Half-normal distribution (a=0.5, c=2)\n",
    "* Ordinary gamma distributions (c=1)\n",
    "* If (c=-1) then it is the inverted gamma distribution.\n",
    "  \n",
    "Implementation is following scipy: https://docs.scipy.org/doc/scipy/tutorial/stats/continuous_gengamma.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "id": "137a73fa",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:24.374952638Z",
     "start_time": "2023-07-30T14:41:24.027548077Z"
    }
   },
   "source": [
    "# Change the type of the source\n",
    "sou.type = 'generalgamma'\n",
    "# Set the time vector\n",
    "sou.set_time_vector(np.linspace(0,600,501))\n",
    "# Change these values to explore the shape of the source wavelet. You can also leave the value as None to not make any changes\n",
    "parameters_source = dict(a = 6,\n",
    "                        c = 1,\n",
    "                        loc = 0,\n",
    "                        scale = 0.02,\n",
    "                        ph =  0,\n",
    "                        t = 50)\n",
    "sou.set_source_params(**parameters_source)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "id": "357df881e4bd278c",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:24.376279252Z",
     "start_time": "2023-07-30T14:41:24.049840092Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Compute the source wavelet\n",
    "source = sou.create_source()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "id": "bcb6b58dbe622647",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:25.466866632Z",
     "start_time": "2023-07-30T14:41:24.082116719Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Plot the source wavelet\n",
    "sou.plot_waveforms_complete()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "9345cae0d2cf5147",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:26.821831242Z",
     "start_time": "2023-07-30T14:41:25.610763855Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou.plot_waveforms_zoom()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "fa1f5af5",
   "metadata": {},
   "source": [
    "## 7. Interactive Widgets for Source Function Creation <a class=\"anchor\" id=\"7\"></a>\n",
    "To simplify the process of creating source functions and to provide a more hands-on experience, we've developed a set of interactive widgets. These widgets allow you to iteratively construct the source functions while visualizing them in real-time. By using the following code, you can access the widgets and experiment with different configurations. Take your time to explore and play with the app to become familiar with the variety of source functions you can generate.\n",
    "\n",
    "This feature is particularly valuable for the generalized gamma source type, as it provides a deeper understanding of the impact of each shape parameter. Through the interactive widgets, you can intuitively grasp how variations in these parameters affect the resulting source function's shape and characteristics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "id": "778893573aae03ba",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:27.984087641Z",
     "start_time": "2023-07-30T14:41:26.871099326Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Create the widgets\n",
    "widget = sou.widgets()\n",
    "widget.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "49636815b953ef0f",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 8. Save data <a class=\"anchor\" id=\"8\"></a>\n",
    "Once you are happy with the source function you can save it to the h5 file for future usage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "id": "7f9dad4b",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:28.208828265Z",
     "start_time": "2023-07-30T14:41:27.990336020Z"
    }
   },
   "source": [
    "# Save the source wavelet\n",
    "from fracwave import OUTPUT_DIR\n",
    "sou.export_to_hdf5(OUTPUT_DIR + '03_tutorial_source_function.h5', overwrite=True)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "id": "4d3a7178ab97b65f",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:28.259190787Z",
     "start_time": "2023-07-30T14:41:28.034800678Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Check if it was correctly saved\n",
    "sou2 = SourceEM()\n",
    "sou2.load_hdf5(OUTPUT_DIR + '03_tutorial_source_function.h5')\n",
    "print(sou)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "id": "5522ac535a8319b4",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:29.597873553Z",
     "start_time": "2023-07-30T14:41:28.094235708Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou2.plot_waveforms_zoom()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "171aaafffb71e435",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 9. Create user defined source function <a class=\"anchor\" id=\"9\"></a>\n",
    "In addition to using predefined source functions, you have the flexibility to create your own custom source functions. To do this, follow these steps:\n",
    "1. Define your source function either in the frequency domain or the time domain.\n",
    "2. Once you have defined the source function, you can derive all other necessary values by using the class functions ```from_time_to_frequency``` or ```from_time_to_frequency```, depending on the specific case.\n",
    "3. If you choose to define your source function in the frequency domain, ensure it is specified within the positive frequency domain. The negative frequencies will be automatically computed by taking the complex conjugate of the positive frequencies.\n",
    "\n",
    "### 9.1. From time to frequency domain <a class=\"anchor\" id=\"91\"></a>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 84,
   "id": "db23bec17a94437a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:29.633722716Z",
     "start_time": "2023-07-30T14:41:29.593748199Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# First on the time domain\n",
    "time_window = 50 # ns\n",
    "nsamples = 401\n",
    "signal1Frequency =  0.1 # GHz\n",
    "signal2Frequency =  0.3 # GHz\n",
    "time = np.linspace(0, time_window, nsamples)\n",
    "\n",
    "# Create 2 sine waves\n",
    "amplitude1 = np.sin(2*np.pi*signal1Frequency*time)\n",
    "amplitude2 = np.sin(2*np.pi*signal2Frequency*time)\n",
    "\n",
    "# Add the sine waves\n",
    "amplitude = amplitude1 + amplitude2\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "id": "ef02500c0db11cf7",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:29.972552241Z",
     "start_time": "2023-07-30T14:41:29.605071266Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "info, (waveform_time, waveform_freq, source) = SourceEM.from_time_to_frequency(t=time, source_time=amplitude)\n",
    "print(info.keys())\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "id": "65688ae41c7828fc",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:30.806805752Z",
     "start_time": "2023-07-30T14:41:29.636102064Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Plot the source wavelet\n",
    "fig, (ax1,ax2) = plt.subplots(1,2)\n",
    "ax1.plot(info['time'], waveform_time)\n",
    "ax1.set_xlabel('Time (ns)')\n",
    "ax1.set_ylabel('Amplitude')\n",
    "ax1.grid()\n",
    "\n",
    "ax2.plot(info['frequency'], np.abs(source))\n",
    "ax2.set_xlabel('Frequency (GHz)')\n",
    "ax2.set_ylabel('Amplitude spectrum')\n",
    "ax2.grid()\n",
    "plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "id": "92cd278859ce6096",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:30.857706158Z",
     "start_time": "2023-07-30T14:41:30.805762951Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou3 = SourceEM()\n",
    "sou3.type = 'user_defined'\n",
    "sou3.set_frequency_vector(info['frequency'])\n",
    "sou3.source = source\n",
    "sou3.waveform_freq =waveform_freq\n",
    "sou3.waveform_time = waveform_time\n",
    "\n",
    "sou3.set_center_frequency(0.1)  # We always need to set a center frequency but this will not affect the source function\n",
    "sou3.set_delay(0)  # We always need to set a delay but this will not affect the source function\n",
    "print(sou3)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "id": "cf03b111f66e99b8",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:34.122487982Z",
     "start_time": "2023-07-30T14:41:31.910414012Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou3.plot_waveforms_complete()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 89,
   "id": "e97aff704be514c7",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:35.887689590Z",
     "start_time": "2023-07-30T14:41:34.253084711Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou3.plot_waveforms_zoom() "
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "98655164fa8b21e6",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "### 9.2. From frequency to time domain <a class=\"anchor\" id=\"92\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 90,
   "id": "af02df1a96f1656c",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:36.312594616Z",
     "start_time": "2023-07-30T14:41:35.902843974Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "nyquist = 1\n",
    "nfreq = 100\n",
    "signal1Frequency =  0.02 # GHz\n",
    "signal2Frequency =  0.2 # GHz\n",
    "frequency = np.linspace(0, nyquist, nfreq)\n",
    "\n",
    "posfre1 = np.argmin(np.abs(frequency - signal1Frequency))\n",
    "posfre2 = np.argmin(np.abs(frequency - signal2Frequency))\n",
    "source = np.zeros(nfreq, dtype=complex)\n",
    "source[posfre1] = 1\n",
    "source[posfre2] = 1\n",
    "\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 91,
   "id": "82275ab64ab71d7a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:36.314139770Z",
     "start_time": "2023-07-30T14:41:35.920427248Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "info2, (waveform_time2, waveform_freq2, source2) = SourceEM.from_frequency_to_time(f=frequency, source_freq=source)\n",
    "print(info2.keys())\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 92,
   "id": "57a69f9018c6dc9",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:38.134033814Z",
     "start_time": "2023-07-30T14:41:36.747754434Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Plot the source wavelet\n",
    "fig2, (ax1,ax2) = plt.subplots(1,2)\n",
    "ax1.plot(info2['frequency'], np.abs(source2))\n",
    "ax1.set_xlabel('Frequency (GHz)')\n",
    "ax1.set_ylabel('Amplitude spectrum')\n",
    "ax1.grid()\n",
    "\n",
    "ax2.plot(info2['time'], waveform_time2)\n",
    "ax2.set_xlabel('Time (ns)')\n",
    "ax2.set_ylabel('Amplitude')\n",
    "ax2.grid()\n",
    "\n",
    "plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 93,
   "id": "9fe46a203c386bfa",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:38.684201615Z",
     "start_time": "2023-07-30T14:41:38.074083510Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou4 = SourceEM()\n",
    "sou4.type = 'user_defined'\n",
    "sou4.set_frequency_vector(info2['frequency'])\n",
    "sou4.source = source2\n",
    "sou4.waveform_freq =waveform_freq2\n",
    "sou4.waveform_time = waveform_time2\n",
    "\n",
    "cf = 0.1\n",
    "sou4.set_center_frequency(cf)  # We always need to set a center frequency\n",
    "print(sou4)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 94,
   "id": "e5a9f9be0ba69b94",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:39.712475379Z",
     "start_time": "2023-07-30T14:41:38.104321045Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou4.plot_waveforms_complete()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 95,
   "id": "dc3dd0e67715dcad",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:41.397019121Z",
     "start_time": "2023-07-30T14:41:39.752653790Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou4.plot_waveforms_zoom()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "70a1ae4dde556bd6",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "### 9.3. Save user defined source function <a class=\"anchor\" id=\"93\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 96,
   "id": "bf92e66be7eebec6",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:41.881741161Z",
     "start_time": "2023-07-30T14:41:41.388295838Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Save the source wavelet\n",
    "from fracwave import OUTPUT_DIR\n",
    "sou4.export_to_hdf5(OUTPUT_DIR + '03_tutorial_source_function.h5', overwrite=True)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 97,
   "id": "313109842748d5a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:41.930306400Z",
     "start_time": "2023-07-30T14:41:41.432338208Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Check if it was correctly saved\n",
    "sou5 = SourceEM()\n",
    "sou5.load_hdf5(OUTPUT_DIR + '03_tutorial_source_function.h5')\n",
    "print(sou5)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 98,
   "id": "51717deda6aea382",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:43.127138488Z",
     "start_time": "2023-07-30T14:41:41.472240557Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "sou5.plot_waveforms_zoom()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18594d58d585558d",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-30T14:41:43.181381951Z",
     "start_time": "2023-07-30T14:41:43.129494317Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0c92bbc-6ec4-4305-94ec-3aee5b497814",
   "metadata": {},
   "source": [],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c4b9497-3b2c-4667-bf53-4c46f11ab5cb",
   "metadata": {},
   "source": [],
   "outputs": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
