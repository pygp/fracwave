# FarcWave Tutorials

Welcome to the tutorials section of the FracWave documentation! Here, you will find a collection of tutorials that 
will guide you through the different features and functionalities of the FracWave library. These tutorials are designed 
to help you understand and utilize the various tools available to characterize fracture and fault geometries within 
homogeneous mediums, such as granitic rock.

The tutorials are organized into the following notbooks:

01_FractureGeometry
-------------------
The [01_FractureGeometry.ipynb](01_A_FractureGeometry.ipynb) notebook focuses on creating different geometric surfaces 
that represent fractures. In these tutorials, you will learn how to use various tools for fracture creation, 
remeshing techniques, and convenient ways to manipulate them. These tutorials will provide you with the necessary 
knowledge to generate accurate fracture representations.

02_AntennaPositions
-------------------
The [02_AntennaPositions.ipynb](02_AntennaPositions.ipynb) notebook teaches you how to place Source-Receiver pairs 
of antennas and store them. By learning how to represent borehole configurations, you will be able to set up the 
necessary antenna positions for your simulations. These tutorials will guide you through the process of defining 
antenna locations effectively.

03_SourceWavelet
----------------
Before conducting the electromagnetic (EM) simulations, it is essential to use a specific source waveform. 
The [03_SourceWavele.ipynb](03_SourceWavelet.ipynb) notebook covers the generation of different source wavelets using 
a generalized gamma distribution. This tutorial will provide you with the knowledge and tools to create customized 
source wavelets for your simulations.

04_ForwardSimulation_DipoleModel
---------------------------------
The [04_ForwardSimulation_DipoleModel.ipynb](04_ForwardSimulation_DipoleModel.ipynb) notebook combines the learnings 
from the previous notebooks and applies them to run a forward GPR simulation. By following this tutorial, you will 
understand how to use FracWave to simulate GPR signals and obtain valuable insights into fracture and fault geometries.

05_Deterministic_InversionGeometry
-----------------------------------
In the [05_Deterministic_InversionGeometry.ipynb](06_Deterministic_InversionGeometry.ipynb) notebook, you will learn 
how to utilize GPR data to infer the geometry of fractures. This tutorial focuses on deterministic inversion 
techniques, allowing you to extract fracture and fault information from your GPR measurements accurately.