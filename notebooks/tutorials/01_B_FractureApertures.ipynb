{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "317f2473",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    " # Welcome to the Fracture Aperture tutorial!\n",
    "In this tutorial, we assume that you have already completed the first part, where we covered the generation of fracture geometries. Now, we will delve deeper into exploring different representations of aperture and roughness in fractures. To achieve this, we will rely on the powerful [GSTools](https://geostat-framework.readthedocs.io/projects/gstools/en/stable/) package. GSTools is a geostatistical tool that enables the creation of random fields, making it possible to simulate realistic fracture fields. Through the incorporation of aperture and roughness, we aim to create fracture representations that closely resemble the complexities observed in natural fracture systems.\n",
    "### Contents:\n",
    "1. [Heterogeneous Apertures](#1)\n",
    "2. [Generating surface roughness](#2)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "34ecf680",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T08:08:51.083671130Z",
     "start_time": "2023-07-24T08:08:51.013907975Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Import all relevant modules\n",
    "import numpy as np\n",
    "from fracwave import FractureGeo\n",
    "import pyvistaqt as pvqt\n",
    "import pyvista as pv\n",
    "pv.set_jupyter_backend('static')"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "2618b770",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T08:05:23.046185672Z",
     "start_time": "2023-07-24T08:05:23.027529090Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Create an instance of the FractureGeo class\n",
    "frac = FractureGeo(fracture_name='Aperture') # (optional) You can set the name of your fractures to keep track of them\n",
    "print(frac)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "f557e655",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 1. Heterogeneous Apertures <a class=\"anchor\" id=\"1\"></a>\n",
    "In this section, we will explore the concept of constructing a fracture aperture distribution with heterogeneity. The fundamental approach involves generating two random fields, resembling the rough surfaces of the two faces of the fracture. By adding a mean aperture to these random fields, we can effectively create variations in void space and contact areas within the fracture. We have created convenient wrapping functions around gstools, but feel free to check their documentation and try different covariance models for fracture field generation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "20a8523f",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T09:21:34.312737080Z",
     "start_time": "2023-07-24T09:21:34.119827649Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# We first need to create our fracture geometry.\n",
    "x = np.arange(-25, 25, 0.5)\n",
    "y = np.arange(-25, 25, 0.5)\n",
    "x, y = np.meshgrid(x, y)\n",
    "z = 0.5 * np.sin(np.sqrt(x**2 + y**2))\n",
    "grid = pv.StructuredGrid(x, y, z)\n",
    "vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(grid)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "1e6f7286",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T08:05:27.360176353Z",
     "start_time": "2023-07-24T08:05:25.931131710Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# We can use the vertices of the mesh or the centroids to create the random field. Here we show how to use the centroids.\n",
    "centroids = vertices[faces].mean(axis=1)  # Otherwise you can use the internal function centroids = frac.get_midpoints(vertices, faces)\n",
    "plane = 2\n",
    "apertures = frac.generate_heterogeneous_apertures(b_mean=0.1, # Mean aperture to separate both random fields\n",
    "                                                  seeds=1234,  # If you want more control on both surfaces then pass 2 seeds (e.g. seeds=(1234, 4567))\n",
    "                                                  points=centroids,\n",
    "                                                  var=0.1,  # Variance\n",
    "                                                  len_scale=5,  # Correlation length. For orientation of the field pass a tuple (e.g. [1,0.1]). This is the same as adding ani=10 as an extra argument\n",
    "                                                  model='exponential',  # or use gaussian\n",
    "                                                  plane=plane  # If plane=None, then it will calculate automatically, but depending on the size of the fracture this can take a while\n",
    "                                                  )"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "ad2cd6a8",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "kwargs_electric_properties = dict(aperture=apertures, # in m\n",
    "                                  electrical_conductivity=0, # in S/m\n",
    "                                  electrical_permeability=81)  # Unitless\n",
    "\n",
    "df = frac.set_fracture(name_fracture='FracCurvature',\n",
    "                       vertices=vertices,\n",
    "                       faces=faces,\n",
    "                       overwrite=False,\n",
    "                       plane=plane,\n",
    "                       **kwargs_electric_properties)  # To avoid accidental overwriting, we set overwrite to False. Turn to True if you want to overwrite the fracture\n",
    "print(frac)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "edb307b2",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Now we can plot our geometry that includes curvature and an heterogeneous aperture field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "e289386a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T09:21:45.473903205Z",
     "start_time": "2023-07-24T09:21:44.720301643Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "surf1 = frac.get_surface(name_fractures='FracCurvature')\n",
    "p = pvqt.BackgroundPlotter()\n",
    "p.show_bounds()\n",
    "p.show_axes()\n",
    "p.add_mesh(surf1, scalars='aperture', show_edges=True, cmap='viridis')\n",
    "p.show()\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "cc3ece6c",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T08:11:14.002799098Z",
     "start_time": "2023-07-24T08:11:13.731249021Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "surf1.plot()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "id": "bcc54322",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## 2. Generating Surface Roughness <a class=\"anchor\" id=\"2\"></a>\n",
    "\n",
    "In this section, we will utilize the aperture field to introduce physical roughness to the fracture model. This will involve displacing the vertices of the fracture. However, you need to be cautious, as the previous approach using squared elements will not be suitable in this context. The reason is that the four vertices no longer form a plane due to the introduction of roughness. To address this, we will create a triangular surface to represent the fractured region.\n",
    "\n",
    "By introducing surface roughness, we aim to simulate the irregularities and complexities often observed in real-world fractures. This is a significant step in improving the accuracy and realism of our fracture model. The triangular surface will allow us to account for the variation in fracture height and create a more realistic representation of the fractured medium."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "3afac715",
   "metadata": {
    "ExecuteTime": {
     "start_time": "2023-07-24T09:53:29.372836446Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Triangulate surface\n",
    "frac2 = FractureGeo()\n",
    "grid_t = grid.triangulate()\n",
    "vertices_t, faces_t = frac2.extract_vertices_and_faces_from_pyvista_mesh(grid_t)\n",
    "# Create a random field from the vertices\n",
    "\n",
    "disp, _ = frac2.generate_crf(seed=1234,  # If you want more control on both surfaces then pass 2 seeds (e.g. seeds=(1234, 4567))\n",
    "                         points=vertices_t,\n",
    "                         var=0.1,  # Variance\n",
    "                         len_scale=5,  # Correlation length. For orientation of the field pass a tuple (e.g. [1,0.1]). This is the same as adding ani=10 as an extra argument\n",
    "                         model='exponential',  # or use gaussian\n",
    "                         plane=plane  # If plane=None, then it will calculate automatically, but depending on the size of the fracture this can take a while\n",
    "                                                  )"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "bdc23ee7",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T09:53:31.223440604Z",
     "start_time": "2023-07-24T09:53:31.190840108Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "vertices_t[:,-1] += disp"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "919eaa0e",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T09:53:33.847651485Z",
     "start_time": "2023-07-24T09:53:32.135961786Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "surft = frac2.create_pyvista_mesh(vertices_t, faces_t)\n",
    "surft.plot(show_edges=True)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "e9f0c60e",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T09:54:41.342679748Z",
     "start_time": "2023-07-24T09:54:39.941615533Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "p = pvqt.BackgroundPlotter()\n",
    "p.show_bounds()\n",
    "p.show_axes()\n",
    "p.add_mesh(surft, color='yellow', show_edges=True)\n",
    "p.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "f6793fbb",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2023-07-24T10:13:58.821397538Z",
     "start_time": "2023-07-24T10:13:58.706075174Z"
    },
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "kwargs_electric_properties = dict(aperture=0.005, # in m\n",
    "                                  electrical_conductivity=0, # in S/m\n",
    "                                  electrical_permeability=81)  # Unitless\n",
    "\n",
    "df = frac2.set_fracture(name_fracture='FracRoughness',\n",
    "                       vertices=vertices_t,\n",
    "                       faces=faces_t,\n",
    "                       overwrite=False,\n",
    "                        plane = plane, \n",
    "                       **kwargs_electric_properties)  # To avoid accidental overwriting, we set overwrite to False. Turn to True if you want to overwrite the fracture\n",
    "print(frac2)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2d83a4a",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [],
   "outputs": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
