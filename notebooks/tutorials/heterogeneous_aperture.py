"""
File created to test the heterogeneous aperture
"""
import numpy as np
import matplotlib.pyplot as plt
from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR
from fracwave.utils.help_decorators import convert_meter
file=OUTPUT_DIR+'heterogeneous_BB.h5'
file2=OUTPUT_DIR+'homogeneous_BB.h5'

#%%
try:
    sou = SourceEM()
    sou.load_hdf5(file)
except:
    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, 1000, 5001))
    sou.set_source_params(a=2.93869036,
                          c=1.5,
                          loc=0,
                          scale=0.008,
                          ph=0,
                          t=150)
    sou.create_source()
    sou.export_to_hdf5(file, overwrite=True)
    sou.export_to_hdf5(file2, overwrite=True)

c0 = 299_792_458  # Speed of light in m/s
rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)  # To convert in m/ns
max_element_size = velocity / (sou.center_frequency * 10)
print(f'Element max size = {convert_meter(max_element_size)}')

sou.plot_waveforms_complete().show()
sou.plot_waveforms_zoom().show()

#%%
try:
    ant = Antenna()
    ant.load_hdf5(file)
except:
    ant = Antenna()
    from bedretto import data

    bor_dat = data.BoreholeData()
    boreholes = ['MB1']
    # df_mb1 = bor.get_borehole_data(borehole_name='MB1')
    # depths = np.linspace(20, 200, 1000)

    traces_per_borehole = 1000
    for b in boreholes:
        bor = bor_dat.get_borehole_data(borehole_name=b)
        # end = bor_dat.get_borehole_end_coordinates(b)
        end = 200
        tx = np.linspace(10, end, traces_per_borehole)
        # tx = np.linspace(10, end['Depth (m)'], traces_per_borehole)
        # tx = np.asarray([50])
        rx = tx - 2.77
        xyz_tx = bor_dat.get_coordinates_from_borehole_depth(bor, tx).T
        xyz_tx1 = bor_dat.get_coordinates_from_borehole_depth(bor, tx - 0.1).T
        xyz_rx = bor_dat.get_coordinates_from_borehole_depth(bor, rx).T
        xyz_rx1 = bor_dat.get_coordinates_from_borehole_depth(bor, rx - 0.1).T

        orient_tx = (xyz_tx1 - xyz_tx) / np.linalg.norm((xyz_tx1 - xyz_tx), ord=1, axis=1)[:, None]
        orient_rx = (xyz_rx1 - xyz_rx) / np.linalg.norm((xyz_rx1 - xyz_rx), ord=1, axis=1)[:, None]
        ant.set_profile(name_profile=b,
                        receivers=xyz_tx,
                        transmitters=xyz_rx,
                        orient_receivers=orient_rx,
                        orient_transmitters=orient_tx,
                        depth_Rx=rx,
                        depth_Tx=tx,
                        overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    ant.export_to_hdf5(file2, overwrite=True)

ant.plot()

#%%
try:
    frac = FractureGeo()
    frac.load_hdf5(file)
except:
    frac = FractureGeo()

    import pyvista as pv

    surf = pv.read('/home/alexis/git/fracwave/output/final_inverted_geometry_BB.vtk')
    v, f = frac.extract_vertices_and_faces_from_pyvista_mesh(surf)

    geom, vertices, faces = frac.remesh(points=v, plane=1, max_element_size=max_element_size)

    aperture = frac.generate_heterogeneous_apertures(b_mean=0.8, seeds=1234,
                                                     points=vertices[faces].mean(axis=1),
                                                     plane=1)

    frac.set_fracture(name_fracture='BB', vertices=vertices,
                      faces=faces, aperture=aperture,
                      electrical_conductivity=0,
                      electrical_permeability=81, overwrite=True)
    frac.export_to_hdf5(file, overwrite=True)

    frac2 = FractureGeo()
    import pyvista as pv

    surf2 = pv.read('/home/alexis/git/fracwave/output/final_inverted_geometry_BB.vtk')
    v2, f2 = frac2.extract_vertices_and_faces_from_pyvista_mesh(surf)

    geom2, vertices2, faces2 = frac2.remesh(points=v2, plane=1, max_element_size=max_element_size)

    frac2.set_fracture(name_fracture='BB', vertices=vertices2,
                      faces=faces2, aperture=0.8,
                      electrical_conductivity=0,
                      electrical_permeability=81, overwrite=True)
    frac2.export_to_hdf5(file2, overwrite=True)

ant.plot()
frac.get_surface().plot(scalars='aperture')
#%% Heterogeneous
try:
    solv = FracEM()
    solv.load_hdf5(file)
    freq = solv.get_freq()
except:
    solv = FracEM()
    solv.mode = 'reflection'
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 5.9  # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'
    solv.open_file(file)
    solv.time_zero_correction()

    freq = solv.forward_pass(overwrite=True)


time_response, time_vector = solv.get_ifft(freq, apply_t0=True)
time_response /= np.max(np.abs(time_response))

extent = [ant.depth_Receiver.min() + 2.77/2, ant.depth_Receiver.max() + 2.77/2, time_vector.max(), time_vector.min()]
vmax=0.01
plt.imshow(time_response.T, aspect='auto', extent=extent, interpolation='None', cmap='RdBu', vmax=vmax, vmin=-vmax)
plt.show()

#%% homogeneous
try:
    solv2 = FracEM()
    solv2.load_hdf5(file2)
    freq2 = solv2.get_freq()
except:
    solv2 = FracEM()
    solv2.mode = 'reflection'
    solv2._fast_calculation_incoming_field = False
    solv2.rock_epsilon = 5.9  # Electrical permittivity of the medium
    solv2.rock_sigma = 0  # Electrical conductivity of the medium
    solv2.backend = 'torch'
    solv2.engine = 'tensor_trace'
    solv2.open_file(file)
    solv2.time_zero_correction()

    freq2 = solv2.forward_pass(overwrite=True)


time_response2, time_vector2 = solv2.get_ifft(freq2, apply_t0=True)
time_response2 /= np.max(np.abs(time_response2))

extent2 = [ant.depth_Receiver.min(), ant.depth_Receiver.max(), time_vector2.max(), time_vector2.min()]
vmax=0.2
plt.imshow(time_response2.T, aspect='auto', extent=extent2, interpolation='None', cmap='RdBu', vmax=vmax, vmin=-vmax)
plt.show()

#%% process for better visualization
from gdp.processing.gain import apply_gain

data_g, _ = apply_gain(time_response.T,
                    gain_type = 'spherical',
                    velocity=solv.velocity,
                    twoway=True,
                    sfreq=sou.sampling_frequency,
                    exponent=2)

extent = [ant.depth_Receiver.min(), ant.depth_Receiver.max(), time_vector.max(), time_vector.min()]
vmax=0.3
plt.imshow(data_g, aspect='auto', extent=extent, interpolation='None', cmap='RdBu', vmax=vmax, vmin=-vmax)
plt.xlabel('Borehole depth (m)')
plt.ylabel('Time (ns)')
plt.show()

#%%
data_g2, _ = apply_gain(time_response2.T,
                    gain_type = 'spherical',
                    velocity=solv2.velocity,
                    twoway=True,
                    sfreq=sou.sampling_frequency,
                    exponent=2)

extent2 = [ant.depth_Receiver.min(), ant.depth_Receiver.max(), time_vector2.max(), time_vector2.min()]
vmax=0.3
plt.imshow(data_g2, aspect='auto', extent=extent2, interpolation='None', cmap='RdBu', vmax=vmax, vmin=-vmax)
plt.xlabel('Borehole depth (m)')
plt.ylabel('Time (ns)')
plt.show()
