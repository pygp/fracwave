from setuptools import setup, find_packages
version = '1.0.0'

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    requirements = fh.read().splitlines()
    requirements = [r for r in requirements if not r.startswith('#') and r != '']

setup(
    name='FracWave',
    version=version,
    packages=find_packages(exclude=('test', 'docs')),
    include_package_data=True,
    install_requires=requirements,
    url='https://github.com/danielsk78/pySeisEM',
    license='LGPL v3',
    author='Daniel Escallon, Alexis Shakas',
    author_email='daniel.escallon@erdw.ethz.ch',
    description='Modeling interaction of Electromagnetic waves with fractured media',
    keywords=['Fracture', 'Seismic', 'GPR', 'Apertures']
)