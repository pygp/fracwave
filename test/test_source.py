import numpy as np
from fracwave.solvers.fracEM.source_EM import SourceEM


def test_source():
    so = SourceEM()
    print(so)

def test_create_source():
    so = SourceEM()
    so.set_frequency_vector(np.linspace(0, 0.2,200))  # frequencies (in GHz!!)
    source_in = so.create_source(a=3, c=0.05, loc=2, scale=0.1, ph=0, t=0)
    so.plot_waveforms_complete().show()
    so.plot_waveforms_zoom().show()

def test_fit_real_source():
    import scipy.io as sio
    import matplotlib.pyplot as plt
    from fracwave import PACKAGE_DIR
    real_data = sio.loadmat(PACKAGE_DIR+'../test/20MHz_dw.mat')

    freq = real_data['f20dw'][0]
    dat = real_data['a20dw'].T[0]
    real_norm = dat / np.abs(dat).max()
    plt.plot(freq, real_norm)
    plt.xlabel('Frequency (GHz)')
    plt.ylabel('Amplitude (-)')
    plt.grid()
    plt.show()
    # Initial point
    so = SourceEM()
    so.set_frequency_vector(freq)
    so.set_delay(150)
    so.type='generalgamma'
    def opt_source(x):
        """
            Optimization function
            Args:
                x: vector to optimize. Parameters of source function
            Returns:

            """
        so.set_source_params(a=x[0], c=x[1], loc=x[2], scale=x[3], ph=x[4])
        source_in = so.create_source()
        op =np.sqrt(np.sum(np.square(np.abs(source_in) - real_norm)))
        print(op)
        return op

    x = [3, # a
         1,  # c
         0,  # loc
         0.01,  # scale
         0,  # ph
         ]
    bounds = [(1.5, 10),
              (0.8, 1.5),
              (0,0),
              (0.008,0.08),
              (0,0),
              ]

    from scipy import optimize
    res = optimize.minimize(opt_source,
                            x,
                            bounds=bounds,
                            method='nelder-mead',
                            options={'maxfun': 10000,
                                     'xatol': 1e-6,
                                     'disp': True})

    print(res.x)
    # array([2.93869036, 1.5       , 0.        , 0.008     , 0.        ])  Fitting one
    # [1.90440554 0.8        0.01       0.00346912 0.        ]
    #[3.07770028e+00 4.70227997e-01 1.05439203e-02 1.00000000e-03 1.97190983e-06]
    so.set_source_params(a=res.x[0], c=res.x[1], loc=res.x[2], scale=res.x[3], ph=res.x[4])


    so.create_source()
    fig, ax = plt.subplots()
    ax.plot(freq, real_norm)
    ax.plot(so.frequency, np.abs(so.source))
    ax.set_xlim(0, 0.1)
    ax.set_xlabel('Frequency (GHz)')
    ax.set_ylabel('Amplitude (-)')
    ax.grid()
    plt.show()

    so.plot_waveforms_complete().show()
    so.plot_waveforms_zoom().show()

def test_export_hdf5():
    so = SourceEM()
    so.frequency = np.linspace(0, 0.2, 200)  # frequencies (in GHz!!)
    source_in = so.create_source(3, 0.05, 2, 0, 0)
    so.export_to_hdf5(overwrite=True)

def test_load_hdf5():
    so = SourceEM()
    so.load_hdf5()
    so.plot(show=True)
