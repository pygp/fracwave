import matplotlib.pyplot as plt
import numpy as np

from fracwave.geometry.fracture_geometry import FractureGeo
from fracwave import PACKAGE_DIR

def test_init_frcture():
    frac = FractureGeo()
    print(frac)

def test_create_square_mesh():
    frac = FractureGeo()

    frac.create_regular_squared_mesh(width=100,
                                     length=100,
                                     resolution=[20, 20],
                                     dip=45,
                                     azimuth=180,
                                     radius=50,
                                     axis=0,
                                     )

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection

    poly = Poly3DCollection(frac.vertices[frac.faces], alpha=0.2, edgecolors="black")
    ax.add_collection3d(poly)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlim([frac.vertices[:,0].min(), frac.vertices[:,0].max()])
    ax.set_ylim([frac.vertices[:,1].min(), frac.vertices[:,1].max()])
    ax.set_zlim([frac.vertices[:,2].min(), frac.vertices[:,2].max()])
    fig.show()

def test_create_triangular_mesh():
    frac = FractureGeo()

    frac.create_regular_triangular_mesh(width=100,
                                     length=100,
                                     resolution=[10, 10],
                                     dip=45,
                                     azimuth=180,
                                     radius=50,
                                     axis=0,
                                     )

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection

    poly = Poly3DCollection(frac.vertices[frac.faces], alpha=0.2, edgecolors="black")
    ax.add_collection3d(poly)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlim([frac.vertices[:,0].min(), frac.vertices[:,0].max()])
    ax.set_ylim([frac.vertices[:,1].min(), frac.vertices[:,1].max()])
    ax.set_zlim([frac.vertices[:,2].min(), frac.vertices[:,2].max()])
    fig.show()

def test_calculate_area_square():
    frac = FractureGeo()
    poly = [[0, 0, 0], [10, 0, 0], [10, 3, 4], [0, 3, 4]]
    ans1 = frac.calc_area_and_orientation_polygon(poly)
    print(ans1)
    poly_translated = [[0 + 5, 0 + 5, 0 + 5], [10 + 5, 0 + 5, 0 + 5], [10 + 5, 3 + 5, 4 + 5], [0 + 5, 3 + 5, 4 + 5]]
    ans2 = frac.calc_area_and_orientation_polygon(poly_translated)
    print(ans2)
    assert ans1[-1] == ans2[-1]

def test_set_properties_triangular():
    frac = FractureGeo()
    width = 100
    length = 100
    frac.create_regular_triangular_mesh(width=width,
                                        length=length,
                                        resolution=[10, 10],
                                        dip=45,
                                        azimuth=180,
                                        radius=50,
                                        axis=0,
                                        )
    frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    assert np.allclose(sum(frac.fracture_properties['area']), width*length), sum(frac.fracture_properties['area'])


def test_set_properties_squared():
    frac = FractureGeo()
    width = 100
    length = 100
    frac.create_regular_squared_mesh(width=width,
                                        length=length,
                                        resolution=[10, 10],
                                        dip=45,
                                        azimuth=180,
                                        radius=50,
                                        axis=0,
                                        )
    frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    assert np.allclose(sum(frac.fracture_properties['area']), width * length), sum(frac.fracture_properties['area'])

def test_construct_irregular_mesh():
    import pygmsh
    with pygmsh.geo.Geometry() as geom:
        geom.add_polygon(
            [
                [0.0, 0.0],
                [1.0, -0.2],
                [1.1, 1.2],
                [0.1, 0.7],
            ],
            mesh_size=0.1,
        )
        mesh = geom.generate_mesh()

def test_geometric_irregular_mesh():
    frac = FractureGeo()
    width = 100
    length = 100
    ver, face = frac.create_irregular_triangular_mesh(width=width,
                                                      length=length,
                                                      mesh_size=10,
                                                      dip=45,
                                                      azimuth=180,
                                                      radius=50,
                                                      axis=0,
                                                      )
    from fracwave.plot.geometry_plot import plot_mesh_mpl, plot_mesh_gmsh
    #plot_mesh(frac._mesh)
    plot_mesh_mpl(ver, face, show=True)

def test_refinement_irregular_mesh():
    frac = FractureGeo()
    width = 100
    length = 100
    ver, face = frac.create_irregular_triangular_mesh(width=width,
                                                      length=length,
                                                      mesh_size=5,
                                                      dip=90,
                                                      azimuth=90,
                                                      radius=50,
                                                      axis=0,
                                                      refine=True
                                                      )
    from fracwave.plot.geometry_plot import plot_mesh_mpl, plot_mesh_gmsh
    plot_mesh_gmsh(frac._mesh)
    plot_mesh_mpl(ver, face, show=True)

def test_refinement_irregular_mesh_fracture_property():
    frac = FractureGeo()
    width = 10
    length = 10
    # refine = True
    mesh_size = 5
    refine = lambda dim, tag, x, y, z: abs(np.sqrt(x ** 2 + z ** 2)) + .1

    ver, face = frac.create_irregular_triangular_mesh(width=width,
                                                      length=length,
                                                      mesh_size=mesh_size,
                                                      dip=90,
                                                      azimuth=90,
                                                      radius=0,
                                                      axis=0,
                                                      refine=refine
                                                      )

    from fracwave.plot.geometry_plot import plot_mesh_mpl, plot_mesh_gmsh
    # plot_mesh(frac._mesh)
    plot_mesh_mpl(ver, face, show=True)

    frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    assert np.allclose(sum(frac.fracture_properties['area']), width * length), sum(frac.fracture_properties['area'])

def test_gempy_mesh():
    a = np.load('low_resolution_mesh.npz')
    # a = np.load('medium_resolution_mesh.npz')
    # a = np.load('high_resolution_mesh.npz')
    vertices = a['arr_0']
    faces = a['arr_1']

    frac = FractureGeo()
    frac.set_vertices(vertices)
    frac.set_faces(faces)

    df = frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    print(df)

def test_export_hdf5():
    frac = FractureGeo()
    frac.create_regular_triangular_mesh(width=100,
                                        length=100,
                                        resolution=[10, 10],
                                        dip=45,
                                        azimuth=180,
                                        radius=10,
                                        axis=0,
                                        )
    frac.set_fracture_properties(aperture=0.0005,
                                 electrical_conductvity=0.001,
                                 electrical_permeability=81)

    frac.export_to_hdf5(overwrite=True)

def test_load_hdf5():
    frac = FractureGeo()
    frac.load_hdf5()
    print(frac.fracture_properties)

def test_import_pyvista_mesh():
    import pyvista as pv
    geom = pv.read(PACKAGE_DIR + '../test/0_model_surface.vtp')

    frac = FractureGeo()
    frac.extract_vertices_and_faces_from_pyvista_mesh(geom)
    frac.plot(backend='mpl')

######################################
def test_efficiency_dask():
    import time
    frac = FractureGeo()
    kwargs = dict(width=100,
                  length=100,
                  resolution=10000,
                  dip=45,
                  azimuth=90,
                  radius=50,
                  axis=0)

    first = time.time()
    frac.create_regular_squared_mesh(**kwargs)
    second = time.time()
    frac.create_regular_squared_mesh(use_dask=True, **kwargs)
    third = time.time()

    print('Without dask = %s S' % (second-first))
    print('Using dask = %s S' % (third - second))

def test_remesh():
    from fracwave import FractureGeo, Antenna, SourceEM, FracEM, OUTPUT_DIR, DATA_DIR
    import pyvista as pv
    import numpy as np

    surf = pv.read(DATA_DIR + 'bb_surf_boreholes_and_tunnel.vtk')

    geom = FractureGeo()

    s = geom.remesh(points=surf.points,
                plane=1, # decide the axes to project the plane into an interpolation functon
                spacing=(100, 100))

    surf.plot()
    geom.surface.plot()
    #import meshio

    #meshio.read()

def test_update_geometry():
    from fracwave import FractureGeo, DATA_DIR
    import pyvista as pv
    surf = pv.read(DATA_DIR + 'bb_surf_boreholes_and_tunnel.vtk')
    surf.plot()
    geom = FractureGeo()
    s = geom.remesh(points=surf.points,
                plane=1, # decide the axes to project the plane into an interpolation functon
                spacing=(100, 100))
    s.plot()
    new_surf = geom.move_surface(s.points,
                                 surf=s,
                                 direction=(0, 1, 1),
                                 radius=300,
                                 magnitude=40,
                                 **dict(slope=0.1))

    new_surf.plot()
    s2 = geom.remesh(points=new_surf.points,
                plane=1, # decide the axes to project the plane into an interpolation functon
                spacing=(100, 100))

    s2.plot()

def test_remesh_geometry_automatically():
    import pyvista as pv
    from fracwave import FractureGeo, DATA_DIR, OUTPUT_DIR
    import numpy as np

    points = np.load(OUTPUT_DIR + 'points.npy')

    geom = FractureGeo()
    grid, vertices, faces = geom.remesh(points, plane=0, spacing=[10,10], max_element_size=0.5, extrapolate= False)
    grid2, vertices2, faces2 = geom.remesh_automatic(points, max_element_size=0.5, spacing=[10,10], extrapolate= False)

    from bedretto import model

    p = model.ModelPlot()
    p.add_object(grid, name='1', color='b')
    p.add_object(grid2, name='2', color='orange')
    # p.add_object(np.c_[X.ravel(), Y.ravel(), Z.ravel()], name='2', color='r')


def no():
    mean = np.mean(points, axis=0)
    points_temp = points - mean
    U, S, V = np.linalg.svd(points_temp)
    normal_vector = V[-1]

    target_normal = np.array([0, 0, 1])
    # Get the rotation axis and angle between the two normal vectors
    rot_axis = np.cross(normal_vector, target_normal)
    theta = np.rad2deg(np.arccos(
        np.dot(normal_vector, target_normal) / (np.linalg.norm(normal_vector) * np.linalg.norm(target_normal))))

    from fracwave.geometry.vector_calculus import arbitrary_rotation
    rot_matrix = arbitrary_rotation(theta, rot_axis)

    # Apply the rotation matrix to the point cloud
    rotated_points = np.dot(points_temp, rot_matrix)


    # To rotate back just use the same rotation matrix and angle but opposite sign for the angle

    a, b, c = normal
    d = -np.dot(normal, mean)

    X, Y = np.meshgrid(np.linspace(-20, 20, 100), np.linspace(-20, 20, 100))
    Z = (-d - a * X - b * Y) / c

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Plot the point cloud
    ax.scatter(points[:, 0], points[:, 1], points[:, 2], color='b')

    # Plot the plane
    ax.plot_surface(X, Y, Z, color='r')

    # Show the plot
    plt.show()

    from bedretto import model

    p = model.ModelPlot()
    p.add_object(points_temp, name='1', color='b')
    p.add_object(rotated_points, name='2', color='orange')
    p.add_object(np.c_[X.ravel(), Y.ravel(), Z.ravel()], name='2', color='r')


def test_gempy_2surfaces():
    extent = [-10,10,-10,10,-10,10]
    resolution=(20,20,20)
    cp = [[0,0,0],
          [1,0.1,3],
          [4, -0.2, 7]]
    cp2 = [[0,0.5,0],
          [1,0.3,3],
          [4, 0.2, 7]]

    p1 = [0,0,0]
    dip, azimuth = 89, 0
    from bedretto import model
    p = model.ModelPlot()
    frac = FractureGeo()
    df_cp = frac.create_df_control_points(pointsA=cp, pointsB=cp2)
    df_or = frac.create_df_orientations(pointsA=p1, dipsA=dip, azimuthA=azimuth)
    frac.init_2surface_geo_model(extent=extent, resolution=resolution)
    vertices, faces = frac.create_gempy_geometry(fixed_points=df_cp,
                                                 fixed_orientations=df_or)

    grid, ve, fa = frac.remesh(points=vertices)
    # mesh = {key: frac.create_pyvista_mesh(v, f) for (key, v), (key, f) in zip(vertices.items(), faces.items())}
    [p.add_object(m, name=key,label=key, color=list(np.random.choice(range(255),size=3)), opacity=0.8)  for key, m in grid.items()]
    p.add_legend()

    grid, v, f = frac.remesh(vertices['A'])
    p.add_object(grid, name='remesh', label='remesh', color=list(np.random.choice(range(255),size=3)), opacity=0.8)


def test_get_surface():
    frac = FractureGeo()
    ver, _ = frac.create_regular_squared_mesh(width=10,
                                              length=10,
                                              resolution=(10, 10),
                                              dip=80,
                                              azimuth=180,
                                              radius=10,  # Separation from origin
                                              axis=1)  # 0 = x-axis, 1 = y-axis, 2 = z-axis. This is the axis to separate from the origin

    grid, vertices, faces = frac.remesh(points=ver, max_element_size=0.5)
    kwargs_electric_properties = dict(aperture=0.005,  # in m
                                      electrical_conductivity=0,  # in S/m
                                      electrical_permeability=81)  # Unitless
    frac.set_fracture(name_fracture='Fracture1',
                      vertices=vertices,
                      faces=faces,
                      overwrite=True,
                      **kwargs_electric_properties, )
    surf = frac.get_surface()
    surf.plot()

