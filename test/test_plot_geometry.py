from fracwave.geometry.fracture_geometry import FractureGeo


def test_plot_mpl():
    from fracwave.plot.geometry_plot import plot_mesh_mpl
    kwargs = dict(width=100,
                  length=100,
                  resolution=[10, 10],
                  dip=45,
                  azimuth=180,
                  radius=50,
                  axis=0,
                  )
    frac = FractureGeo()

    frac.create_regular_squared_mesh(**kwargs)
    plot_mesh_mpl(frac.vertices, frac.faces, show=True)

    frac.create_regular_triangular_mesh(**kwargs)
    plot_mesh_mpl(frac.vertices, frac.faces, show=True)

def test_plot_gmsh():
    import pygmsh
    from fracwave.plot.geometry_plot import plot_mesh_gmsh

    with pygmsh.geo.Geometry() as geom:
        geom.add_rectangle(xmin=-50, xmax=50, ymin=-50, ymax=50, z=0, )
        mesh = geom.generate_mesh()
    plot_mesh_gmsh(mesh)