import unittest
import numpy as np

# Import the function to be tested
from fracwave.geometry.vector_calculus import get_any_orthonormal_vector
class TestGetAnyOrthonormalVector(unittest.TestCase):
    def test_orthonormal_vector_single(self):
        # Test the function with a single input vector
        v = np.array([1, 2, 3])
        u = get_any_orthonormal_vector(v)
        assert(np.dot(u,v)==0), 'Vectors are not orthogonal'

    def test_orthonormal_vector_multiple(self):
        # Test the function with multiple input vectors
        v = np.asarray([
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
            [1, 2, 3],
        ])
        u = get_any_orthonormal_vector(v)
        assert (np.all(np.einsum('ti, ti->t', u, v) == 0)), 'Not all vectors are not orthogonal'

    def test_invalid_input(self):
        # Test the function with invalid input (non-vector)
        v = 5  # Not a vector
        with self.assertRaises(AttributeError):
            u = get_any_orthonormal_vector(v)

if __name__ == '__main__':
    unittest.main()