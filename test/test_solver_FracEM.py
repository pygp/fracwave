
"""
You will end up with 3 files
OUTPUT_DIR + 'test_propagation.h5' => Geometry and all initial conditions for the simulation
OUTPUT_DIR + 'test_propagation_simulation.h5' => If the flag save_hd5 is True, it will store all steps here
OUTPUT_DIR + 'test_propagation_summed_response.npy' => Is the ending frequency file after the complete simulation.

To load any of the simulated attributes just type:
 ----- Incoming field to fracture
'simulation/incoming_field'= shape(ntraces, nelements, nfrequencies, 3) - at the fracture
'simulation/fracture_field' = shape(ntraces, nelements) - at the fracture. Frequencies and components are added together
'simulation/projected' = Field projected to element orientation
'simulation/propagation' = In case of propagation and not reflection

------- Projected incoming field
'simulation/projected/nfz' = Normal
'simulation/projected/tfx' = Transversal x
'simulation/projected/tfy' = Transversal y

--------- Induced field
'simulation/induced_dipole' = (ntraces, nelements, nfrequencies, 3)


'simulation/mask_elements' = Those elements that have near 0 energy

-------- Propagation back to field
'simulation/receiver' = (ntraces, nelements, nfrequencies, 3),  Energy at receivers
'simulation/summed_response' = (ntraces, nelements) Sum of frequencies and components
"""
#%%

import numpy as np
import matplotlib.pyplot as plt
from fracwave import SourceEM, FracEM, Antenna, FractureGeo, PACKAGE_DIR, OUTPUT_DIR
from fracwave.utils.help_decorators import (generate_tree_h5_file, generate_tree_h5_file_attrs, convert_frequency,
                                            convert_meter)
import h5py

odir = OUTPUT_DIR+'tests/'

def get_simulation_properties(conduct=True):
    # Fracture properties
    center = (7, 5, 5)
    width=2
    length=2
    electrical_permeability = 81
    electrical_conductivity = 0.1 if conduct else 0
    aperture=0.2
    max_element_size = 0.1  # 10cmm

    # medium properties
    electrical_permeability_granite = 5
    electrical_conductivity_granite = 0.001 if conduct else 0

    # Source properties
    source_parameters = [0.4857619,
                         2.03456272,
                         0,
                         0.14400915,
                         0,
                         10.01662033]
    time_window = 140  # ns
    center_frequency = 0.1 # GHz
    typ = 'generalgamma'

    # Antenna properties
    rx = np.asarray([[4.0, 5.0, 5.0],
                     [5.0, 5.0, 5.0],
                     [6.0, 5.0, 5.0],
                     [5.0, 5.0, 6.0],
                     [5.0, 5.0, 7.0],
                     [5.0, 5.0, 8.0],

                     #
                     [5.0, 5.0, 4.0],
                     [5.0, 5.0, 3.0],
                     [5.0, 5.0, 2.0],
                     [5.0, 6.0, 5.0],
                     [5.0, 7.0, 5.0],
                     [5.0, 8.0, 5.0],
                     [5.0, 4.0, 5.0],
                     [5.0, 3.0, 5.0],
                     [5.0, 2.0, 5.0],
                     ])
    tx = np.repeat([[2,5,5]], len(rx), axis=0)
    antenna_orient = np.repeat([[0,0,1]], len(rx), axis=0)

    # gprMax file properties
    if conduct:
        file_gprMax = PACKAGE_DIR + '../test/gprmax/homogeneous_conduc.out'
    else:
        file_gprMax = PACKAGE_DIR + '../test/gprmax/homogeneous.out'
    generate_tree_h5_file_attrs(file_gprMax)
    return (center, width, length, electrical_permeability, electrical_conductivity, aperture, max_element_size,
     electrical_permeability_granite, electrical_conductivity_granite, source_parameters, time_window,
     center_frequency, typ, rx, tx, antenna_orient, file_gprMax)
def read_file(f_out):
    """
    Read the h5 file and return the data for the electric field in all 3 coordinates and the time vector
    Args:
        f_out:

    Returns:

    """
    with h5py.File(f_out, 'r') as f:
        nrx = f.attrs['nrx']
        dt = f.attrs['dt']
        iterations = f.attrs['Iterations']
        val = np.zeros((iterations, nrx, 3))
        time_window = (iterations - 1) * dt * 1e9
        time = np.linspace(0,  time_window, num=iterations)
        for i in range(nrx):
            val[:,i, 0] = f[f'rxs/rx{i+1}/Ex'][:]
            val[:,i, 1] = f[f'rxs/rx{i+1}/Ey'][:]
            val[:,i, 2] = f[f'rxs/rx{i+1}/Ez'][:]
    return val, time
def test_propagation_mode_1_to_gprMax():
    (center, width, length, electrical_permeability, electrical_conductivity, aperture, max_element_size,
     electrical_permeability_granite, electrical_conductivity_granite, source_parameters, time_window,
     center_frequency, typ, rx, tx, antenna_orient, file_gprMax) = get_simulation_properties(conduct=False)
    response_gprMax, time_gprMax= read_file(file_gprMax)
    # Normalize to 1st trace
    norm_gprMax = np.abs(response_gprMax).max()
    response_gprMax /= norm_gprMax

    file = odir + 'test_propagation_mode_1.h5'
    ant = Antenna()
    ant.set_profile(name_profile='Prop1',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=antenna_orient,
                    orient_transmitters=antenna_orient,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    # ant.plot(backend='mpl', show=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, time_window, 1001))
    sou.set_source_params(a=source_parameters[0],
                          c=source_parameters[1],
                          loc=source_parameters[2],
                          scale=source_parameters[3],
                          ph=source_parameters[4],
                          t=source_parameters[5])
    sou.create_source()
    sou.set_center_frequency(0.1)
    # sou.plot_waveforms_complete().show()
    # sou.plot_waveforms_zoom().show()
    sou.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    solv.mode = 'propagation'
    solv.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv.backend = 'numpy'
    solv.engine = 'loop'

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=True, overwrite=True)[..., 2]

    # freq = solv.file_read('simulation/prop_full')
    time_response, time_vector = solv.get_ifft(freq, pad=len(time_gprMax) - solv.file_read_attribute('source/ntime'))
    norm_fracEM = np.abs(time_response).max()
    time_response /= norm_fracEM

    solv2 = FracEM()

    solv2.mode = 'propagation'
    solv2.open_file(file)
    solv2._propagation_mode = 1
    # solv2._green_function_fields = None
    solv2._green_function_fields = None # ['NF', 'FF', 'IF']
    solv2._fast_calculation_incoming_field = False
    solv2.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv2.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv2.backend = 'numpy'
    solv2.engine = 'tensor_trace'
    solv2._old_solver = True
    solv2._scaling = False

    freq2 = solv2.forward_pass(mask_frequencies=True, overwrite=True)

    freq2 = solv2.file_read('simulation/prop_full')[..., 2]
    # freq2 = np.conjugate(freq2)
    time_response2, time_vector2 = solv2.get_ifft(freq2, pad=len(time_gprMax) - solv2.file_read_attribute('source/ntime') +1000)
    norm_fracEM2 = np.abs(time_response2).max()
    time_response2 /= norm_fracEM2

    fig, AX = plt.subplots(solv.ntraces, 1, figsize=(10, 25), sharex=True)
    for i, ax in enumerate(AX):
        ax.plot(time_gprMax, response_gprMax[:,i,2], color='red', linestyle='dotted', label='gprMax')
        ax.plot(time_vector+20, time_response[i], color='blue', label='Loop', linestyle='--')
        ax.plot(time_vector2, time_response2[i], color='green', label='Tensor')
        # if solv.engine == 'loop':
        #     ax.plot(time_vector+20, np.flip(time_response[i]), color='blue', label='FracEM')
        # else:
        #     ax.plot(time_vector, time_response[i], color='blue', label='FracEM')
        ax.set_xlim((20, 48))
        ax.grid()
        ax.legend()
        ax.set_title(f'rx{i+1}')
    plt.show()

    print(f'Normalization gprMax = {norm_gprMax}\n'
          f'Normalization FracEM = {norm_fracEM}\n'
          f'Ratio = {norm_fracEM/norm_gprMax}')
    return solv
def test_propagation_mode_2_to_gprMax():
    (center, width, length, electrical_permeability, electrical_conductivity, aperture, max_element_size,
     electrical_permeability_granite, electrical_conductivity_granite, source_parameters, time_window,
     center_frequency, typ, rx, tx, antenna_orient, file_gprMax) = get_simulation_properties(conduct=True)
    response_gprMax, time_gprMax= read_file(file_gprMax)
    # response_gprMax=response_gprMax.sum(axis=-1)
    # Normalize to 1st trace
    norm_gprMax = np.abs(response_gprMax).max()
    response_gprMax /= norm_gprMax

    file = odir + 'test_propagation_mode_2.h5'
    ant = Antenna()
    ant.set_profile(name_profile='Prop1',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=antenna_orient,
                    orient_transmitters=antenna_orient,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    # ant.plot(backend='mpl', show=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, time_window, 1001))
    sou.set_source_params(a=source_parameters[0],
                          c=source_parameters[1],
                          loc=source_parameters[2],
                          scale=source_parameters[3],
                          ph=source_parameters[4],
                          t=source_parameters[5])
    sou.create_source()
    sou.set_center_frequency(0.1)
    # sou.source = sou.source / 1000
    # sou.plot_waveforms_complete().show()
    sou.plot_waveforms_zoom().show()
    sou.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    solv.mode = 'propagation'
    solv._propagation_mode = 2
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=True, overwrite=True)
    freq = solv.file_read('simulation/prop_full')[...,2]
    time_response, time_vector = solv.get_ifft(freq, pad=len(time_gprMax) - solv.file_read_attribute('source/ntime'))
    norm_fracEM = np.abs(time_response).max()
    time_response /= norm_fracEM

    fig, AX = plt.subplots(solv.ntraces, 1, figsize=(10, 25), sharex=True)
    for i, ax in enumerate(AX):
        ax.plot(time_gprMax, response_gprMax[:,i,2], color='red', linestyle='--', label='gprMax')
        # ax.plot(time_gprMax, response_gprMax[:,i], color='red', linestyle='--', label='gprMax')
        ax.plot(time_vector, time_response[i], color='blue', label='FracEM')
        ax.set_xlim((20, 48))
        ax.grid()
        ax.legend()
        ax.set_title(f'rx{i+1}')
    plt.show()
    print(f'Normalization gprMax = {norm_gprMax}\n'
          f'Normalization FracEM = {norm_fracEM}\n'
          f'Ratio = {norm_fracEM/norm_gprMax}')
    return solv
def test_propagation_equal():
    solv1 = test_propagation_mode_1_to_gprMax()
    freq1 = solv1.file_read('simulation/prop_full')[..., 2]
    time_response1, time_vector1 = solv1.get_ifft(freq1)
    norm_fracEM1 = np.abs(time_response1).max()
    time_response1 /= norm_fracEM1

    solv2 = test_propagation_mode_2_to_gprMax()
    freq2 = solv2.file_read('simulation/prop_full')[..., 2]
    time_response2, time_vector2 = solv2.get_ifft(freq2)
    norm_fracEM2 = np.abs(time_response2).max()
    time_response2 /= norm_fracEM2
    fig, AX = plt.subplots(solv1.ntraces, 1, figsize=(10, 25), sharex=True)
    for i, ax in enumerate(AX):
        ax.plot(time_response1[i] - time_response2[i])
        ax.set_xlim((150, 350))
    plt.show()

    assert np.allclose(time_response1, time_response2, atol= 1e-7)  # Below this value they are not the same

def test_reflection_to_gprMax():
    (center, width, length, electrical_permeability, electrical_conductivity, aperture, max_element_size,
     electrical_permeability_granite, electrical_conductivity_granite, source_parameters, time_window,
     center_frequency, typ, rx, tx, antenna_orient, file_gprMax) = get_simulation_properties(conduct=True)
    file = odir + 'test_reflection.h5'
    ant = Antenna()
    ant.set_profile(name_profile='ref',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=antenna_orient,
                    orient_transmitters=antenna_orient,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    # ant.plot(backend='mpl', show=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, time_window, 1001))
    sou.set_source_params(a=source_parameters[0],
                          c=source_parameters[1],
                          loc=source_parameters[2],
                          scale=source_parameters[3],
                          ph=source_parameters[4],
                          t=source_parameters[5])
    sou.create_source()
    sou.set_center_frequency(0.1)

    sou.export_to_hdf5(file, overwrite=True)

    from fracwave.utils.help_decorators import convert_meter
    print(f'Element max size = {convert_meter(max_element_size)}')


    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=2,
                                            length=2,
                                            resolution=[5, 5],
                                            dip=90,
                                            azimuth=90,
                                            center=center)
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=0)
    kwarg_properties = dict(aperture=aperture,
                            electrical_conductivity=electrical_conductivity,
                            electrical_permeability=electrical_permeability)
    frac.set_fracture(name_fracture='compare_gprmax',
                      vertices=vertices,
                      faces=faces,
                      **kwarg_properties)
    # frac.plot_geometry()
    frac.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    solv.mode = 'full-reflection'
    solv.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv.backend = 'numpy'
    solv.open_file(file)
    solv.engine = 'loop'
    solv._old_solver = True
    # solv._scaling = True

    freq = solv.forward_pass(mask_frequencies=False, overwrite=True)
    # freq = np.conj(freq)

    response_gprMax, time_gprMax = read_file(file_gprMax)
    mask = time_gprMax < 50
    response_gprMax[mask] = 0
    #
    solv2 = FracEM()
    # solv2.mode = 'dipole_response'
    solv2.mode = 'full-reflection'
    solv2.units = 'SI'
    solv2._propagation_mode = 1

    solv2._fast_calculation_incoming_field = False
    solv2.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv2.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv2.backend = 'torch'
    # solv.engine = 'tensor_trace'
    solv2._green_function_fields = None  #['NF', 'FF', 'IF']
    solv2.engine = 'tensor_trace'
    # solv.engine = 'loop'
    solv2.open_file(file)
    solv2._old_solver = True
    solv2._scaling = False

    freq2 = solv2.forward_pass(mask_frequencies=True, overwrite=True, save_hd5=False)

    for j, title in enumerate(['X', 'Y', 'Z']):
        if j !=2:
            continue
        time_response, time_vector = solv.get_ifft(freq[...,j], pad=len(time_gprMax) - solv.file_read_attribute('source/ntime'))
        time_response2, time_vector2 = solv2.get_ifft(freq2[...,j], pad=len(time_gprMax) - solv2.file_read_attribute('source/ntime'))
        norm_fracEM = np.abs(time_response).max()
        norm_fracEM2 = np.abs(time_response2).max()
        time_response /= norm_fracEM
        time_response2 /= norm_fracEM2

        norm = np.abs(response_gprMax[..., j]).max()
        response = response_gprMax[...,j] / norm

        fig, AX = plt.subplots(solv.ntraces, 1, figsize=(10, 25), sharex=True)
        for i, ax in enumerate(AX):
            ax.plot(time_gprMax, response[:, i], color='red', linestyle='dotted', label='gprMax')
            # ax.plot(time_gprMax, response_gprMax[:,i], color='red', linestyle='--', label='gprMax')
            # if solv.engine == 'loop':
            # ax.plot(time_vector+20, np.flip(time_response[i]), color='blue', label='Loop', linestyle='--')
            ax.plot(time_vector+20, time_response[i], color='blue', label='Loop', linestyle='--')
            # else:
            # ax.plot(time_vector2, time_response2[i], color='green', label='Tensor', linestyle='--')
            ax.plot(time_vector2, time_response2[i], color='green', label='Tensor', linestyle='--')
            # ax.set_xlim((40, 110))
            ax.grid()
            ax.legend()
            ax.set_title(f'rx{i + 1}')
        fig.suptitle(title, fontsize=30)
        plt.show()
        # print(f'Normalization gprMax = {norm}\n'
        #       f'Normalization FracEM = {norm_fracEM}\n'
        #       f'Ratio = {norm_fracEM / norm}')

def test_full_profile_reflection():
    hetero = False
    from bedretto import model
    p = model.ModelPlot()
    file_h5 = odir + 'test_reflection_heterogeneous.h5'
    ant = Antenna()

    # top = 12 * 1e-2  # cm to top
    top = 12 * 1e-2  # cm to top

    # Scan from bottom to top
    # line = np.arange(0, 3, 0.2) * 1e-2
    line = np.linspace(0, 0.3, 10)

    tx = np.zeros((len(line), 3))
    tx[:, 0] = 0.15
    tx[:, 1] = line
    tx[:, 2] = top

    rx = tx.copy()

    orient = np.zeros((len(line), 3))
    orient[:, 1] = 1

    ant.set_profile(name_profile=f'A',
                    receivers=rx,
                    transmitters=tx,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    depth_Rx=line,
                    depth_Tx=line,
                    overwrite=True)


    # ant2.export_to_hdf5(file_h5.split('.')[0]+'_data.h5', overwrite=True)
    ant.export_to_hdf5(file_h5, overwrite=True)
    p.add_object(ant.Receiver, name='rx', color='blue', opacity=0.2)
    p.add_object(ant.Transmitter, name='tx', color='red', opacity=0.2)

    sou = SourceEM()
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 599))

    sou.set_delay(delay=1)
    sou.set_source_params(a=15,
                          c=1,
                          loc=0,
                          scale=0.2)
    sou.create_source()
    # sou.widgets().show()

    fig = sou.plot_waveforms_zoom()
    fig = sou.plot_waveforms_complete()
    fig.show()
    sou.export_to_hdf5(file_h5, overwrite=True)

    max_element_size = (0.1234 / sou.center_frequency) / 5

    from fracwave.utils.help_decorators import convert_meter
    print(f'Element max size = {convert_meter(max_element_size)}')

    frac = FractureGeo()
    resolution = (45, 45)


    width = 0.30
    length = 0.30
    vertices, faces = frac.create_regular_squared_mesh(width=width, length=length, dip=0, azimuth=0,
                                                       resolution=resolution, center=(.17, .17, 0))

    surf = frac.create_pyvista_mesh(vertices, faces)

    p.add_object(surf, name='fracture', show_edges=False, opacity=0.5)

    if hetero:
        outname = '/home/daniel/GitProjects/fracwave/scripts/fracture_aperture/contours/'
        d = np.load(outname + 'files.npz')
        apertures_l = d['apertures']
        spacing = [600, 600]
        def rotate_matrix_clockwise(matrix, rotations=1):
            # Define a helper function to rotate the matrix once
            def rotate_once(matrix):
                # Transpose the matrix
                transposed_matrix = [list(row) for row in zip(*matrix)]
                # Reverse each row
                rotated_matrix = [list(reversed(row)) for row in transposed_matrix]
                return rotated_matrix

            # Apply multiple rotations
            rotated_matrix = matrix
            for _ in range(rotations):
                rotated_matrix = rotate_once(rotated_matrix)

            return rotated_matrix

        apertures_original = rotate_matrix_clockwise(apertures_l.reshape(spacing), rotations=2)

        scaling_factors = (np.array(resolution) / spacing).tolist()
        from scipy.ndimage import zoom
        # Perform the downsampling using zoom
        downsampled_image = zoom(apertures_original, scaling_factors)
        # scaling_factors_up = (spacing / np.array(resolution)).tolist()
        # upsampled_image = zoom(downsampled_image, scaling_factors_up)

        apertures = downsampled_image.ravel()[faces].mean(axis=1) * 1e-2
        # apertures = apertures.reshape(spacing)
    else:
        apertures = 0.1
    kwarg_properties = dict(aperture=apertures,
                            electrical_conductivity=0,
                            electrical_permeability=1,
                            plane=2,
                            overwrite=True)

    frac.set_fracture(name_fracture='fracture',
                      vertices=vertices,
                      faces=faces,
                      **kwarg_properties)

    frac.export_to_hdf5(file_h5, overwrite=True)

    surf['apertures (m)'] = frac.fractures.aperture.to_numpy()
    p.add_object(surf, name='fracture', scalars='apertures (m)', show_edges=False, cmap='turbo')

    solv = FracEM()
    solv.rock_epsilon = 6.5
    solv.rock_sigma = 0.0
    solv.engine = 'tensor_trace'
    solv._propagation_mode = 1
    solv.backend = 'torch'
    solv._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have >
    solv.apply_causality = True  # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those >
    solv.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achiev>
    solv._filter_percentage = 0.01
    solv.mode = 'reflection'
    solv._old_solver = True
    solv._scaling = True
    # solv._old_solver = False
    solv.units = 'SI'
    # solv._old_solver = False
    info = solv.open_file(file_h5)

    freq = solv.forward_pass(overwrite=True, recalculate=True, save_hd5=False, )

    solv2 = FracEM()
    solv2.rock_epsilon = 6.5
    solv2.rock_sigma = 0.0
    solv2.engine = 'tensor_trace'
    solv2._propagation_mode = 1
    solv2.backend = 'torch'
    solv2._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have >
    solv2.apply_causality = True  # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those >
    solv2.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achiev>
    solv2._filter_percentage = 0.01
    solv2.mode = 'reflection'
    solv2._old_solver = False
    solv2._scaling = False
    # so2lv._old_solver = False
    solv2.units = 'SI'
    # solv._old_solver = False
    info = solv2.open_file(file_h5)

    freq2 = solv2.forward_pass(overwrite=True, recalculate=True, save_hd5=False, )

    solv3 = FracEM()
    solv3.rock_epsilon = 6.5
    solv3.rock_sigma = 0.0
    solv3.engine = 'loop'
    solv3._propagation_mode = 1
    solv3.backend = 'torch'
    solv3._fast_calculation_incoming_field = False  # This is a trick to speed up the calculation of the incoming field by calculating only the frequencies that have >
    solv3.apply_causality = True  # This is to check that the computations are inside the time window we are looking for. This means that it will filter out all those >
    solv3.filter_energy = False  # Here we focus on studying the incoming field on the fracture for a specific frequency and examine the propagating energy. To achiev>
    solv3._filter_percentage = 0.01
    solv3.mode = 'reflection'
    solv3._old_solver = True
    # so3lv._old_solver = False
    solv3.units = 'SI'
    # solv._old_solver = False
    info = solv3.open_file(file_h5)
    # solv._scaling = True

    freq3 = solv3.forward_pass(overwrite=True, recalculate=True, save_hd5=False, )

    # for j, title in enumerate(['X', 'Y', 'Z']):
    #     if j !=2:
    #         continue
    time_response, time_vector = solv.get_ifft(freq)
    time_response2, time_vector2 = solv2.get_ifft(freq2)
    time_response3, time_vector3 = solv2.get_ifft(freq3)
    time_vector3 +=2
    norm_fracEM = np.abs(time_response).max()
    norm_fracEM2 = np.abs(time_response2).max()
    norm_fracEM3 = np.abs(time_response3).max()
    time_response /= norm_fracEM
    time_response2 /= norm_fracEM2
    time_response3 /= norm_fracEM3

    extent = [0, len(time_response), time_vector[0], time_vector[-1]]
    extent2 = [0, len(time_response2), time_vector2[0], time_vector2[-1]]
    extent3 = [0, len(time_response3), time_vector3[0], time_vector3[-1]]

    fig, ax = plt.subplots()
    ax = solv3.plot_wiggle(time_response3.T, ax=ax, color='blue', fill=False, extent=extent3, linestyle='--', label='Loop')
    ax = solv.plot_wiggle(time_response.T, ax=ax, color='black', fill=False, extent =extent, linestyle='dotted', label='Old')
    ax = solv2.plot_wiggle(time_response2.T, ax=ax, color='red', fill=False, extent=extent2, linestyle='dotted', label='New')
    ax.set_ylim(6,2)
    ax.legend()
    plt.show()


def test_reflection_and_propagation():
    (center, width, length, electrical_permeability, electrical_conductivity, aperture, max_element_size,
     electrical_permeability_granite, electrical_conductivity_granite, source_parameters, time_window,
     center_frequency, typ, rx, tx, antenna_orient, file_gprMax) = get_simulation_properties(conduct=True)
    response_gprMax, time_gprMax = read_file(file_gprMax)
    # norm_gprMax = np.abs(response_gprMax).max()
    # response_gprMax /= norm_gprMax

    file = odir + 'test_reflection_and_propagation_scaling.h5'
    ant = Antenna()
    ant.set_profile(name_profile='ref',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=antenna_orient,
                    orient_transmitters=antenna_orient,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    # ant.plot(backend='mpl', show=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, time_window, 1001))
    sou.set_source_params(a=source_parameters[0],
                          c=source_parameters[1],
                          loc=source_parameters[2],
                          scale=source_parameters[3],
                          ph=source_parameters[4],
                          t=source_parameters[5])
    sou.create_source()
    sou.set_center_frequency(0.1)
    # sou.source = sou.source / 1000
    # sou.plot_waveforms_complete().show()
    # sou.plot_waveforms_zoom().show()
    sou.export_to_hdf5(file, overwrite=True)

    from fracwave.utils.help_decorators import convert_meter
    print(f'Element max size = {convert_meter(max_element_size)}')

    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=2,
                                            length=2,
                                            resolution=[5, 5],
                                            dip=90,
                                            azimuth=90,
                                            center=center)
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=0)
    kwarg_properties = dict(aperture=aperture,
                            electrical_conductivity=electrical_conductivity,
                            electrical_permeability=electrical_permeability)
    frac.set_fracture(name_fracture='compare_gprmax',
                      vertices=vertices,
                      faces=faces,
                      **kwarg_properties)
    # frac.plot_geometry()
    frac.export_to_hdf5(file, overwrite=True)

    ########### Propagation first
    solv = FracEM()
    solv.mode = 'propagation'
    solv._propagation_mode = 1
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'
    solv._scaling = False

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=False, overwrite=True)
    freq = solv.file_read('simulation/prop_full')[..., 2]
    time_response, time_vector = solv.get_ifft(freq,
                                               pad=len(time_gprMax) - solv.file_read_attribute('source/ntime'))
    # norm_fracEM = np.abs(time_response).max()
    # time_response /= norm_fracEM

    ######### Now reflection
    solv2 = FracEM()
    solv2.mode = 'full-reflection'
    solv2._propagation_mode = 1
    solv2._fast_calculation_incoming_field = False
    solv2.rock_epsilon = electrical_permeability_granite  # Electrical permittivity of the medium
    solv2.rock_sigma = electrical_conductivity_granite  # Electrical conductivity of the medium
    solv2.backend = 'torch'
    solv2.engine = 'tensor_trace'
    solv2._scaling = False

    solv2.open_file(file)

    freq2 = solv2.forward_pass(mask_frequencies=False, overwrite=True)
    freq2 = freq2[..., 2]

    time_response_r, time_vector_r = solv2.get_ifft(freq2,
                                                    pad=len(time_gprMax) - solv2.file_read_attribute('source/ntime'))
    # time_response_r /= norm_fracEM

    time_response += time_response_r

    fig, AX = plt.subplots(solv.ntraces, 1, figsize=(10, 25), sharex=True)
    for i, ax in enumerate(AX):
        ax.plot(time_gprMax, response_gprMax[:, i, 2], color='red', linestyle='--', label='gprMax')
        # ax.plot(time_gprMax, response_gprMax[:,i], color='red', linestyle='--', label='gprMax')
        ax.plot(time_vector, time_response[i], color='blue', label='FracEM')
        # ax.set_xlim((48, 100))
        ax.grid()
        ax.legend()
        ax.set_title(f'rx{i + 1}')
    plt.show()
    # print(f'Normalization gprMax = {norm_gprMax}\n'
    #       f'Normalization FracEM = {norm_fracEM}\n'
    #       f'Ratio = {norm_fracEM / norm_gprMax}')
def test_reflection_coefficients_and_phase():
    solv = FracEM()
    solv.backend = 'numpy'
    theta = np.deg2rad(7.5)
    freq = np.asarray([25e6, 50e6, 100e6, 200e6, 250e6, 500e6, 1000e6, 3e9])
    omega = 2 * np.pi * freq

    epsilon_rock = 7  # 8.4
    sigma_rock = 0.01e-3

    epsilon_fracture = 81
    conductivities = np.linspace(0, 3, 50)  # 0 to 3 S/m

    k1 = solv.wavenumber(omega, epsilon_rock, sigma_rock)
    apertures = np.linspace(1e-9, 10e-3, 50)  # 1 micrometer to 10mm

    x, y = np.meshgrid(conductivities, apertures)
    reflections = np.zeros((len(apertures), len(conductivities), len(omega)), dtype=complex)
    phase = np.zeros((len(apertures), len(conductivities), len(omega)))
    for j, cond in enumerate(conductivities):
        k2 = solv.wavenumber(omega, epsilon_fracture, cond)
        for i, aper in enumerate(apertures):
            val = solv.reflection_all(k1 =k1, k2 =k2, theta=theta, aperture=aper)
            phase[i, j] = np.unwrap(np.angle(val, deg=True), period=360)
            reflections[i, j] = np.abs(val)

    fig = plt.figure(figsize=(10, 30))
    for k, fr in enumerate(freq):
        ax = fig.add_subplot(len(freq), 1, k + 1, projection='3d', )
        cax = ax.plot_surface(x, y, reflections[..., k], cmap='turbo', )
        ax.set_xlabel('Conductivity (S/m)')
        ax.set_ylabel('Aperture (m)')
        ax.set_zlabel('Reflection magnitude')

        ax.view_init(elev=20., azim=225)
        ax.xaxis.labelpad = 20
        ax.yaxis.labelpad = 20
        ax.zaxis.labelpad = 20
        ax.set_title(f'{convert_frequency(fr)}')
        ax.set_zlim(0, 1)
        # plt.tight_layout()
        fig.colorbar(cax, ax=ax)
    plt.show()


    fig = plt.figure(figsize=(10, 30))
    for k, fr in enumerate(freq):
        ax = fig.add_subplot(len(freq), 1, k + 1, projection='3d')

        # cax =ax.scatter(XX, YY, reflections.flatten(), c=reflections.flatten(), cmap='turbo', marker='o')
        cax = ax.plot_surface(x, y, phase[..., k], cmap='turbo')
        ax.set_xlabel('Conductivity (S/m)')
        ax.set_ylabel('Aperture (m)')
        ax.set_zlabel('Phase')

        ax.view_init(elev=20., azim=225)
        ax.xaxis.labelpad = 20
        ax.yaxis.labelpad = 20
        ax.zaxis.labelpad = 20
        ax.set_title(f'{convert_frequency(fr)}')
        # plt.tight_layout()
        fig.colorbar(cax, ax=ax)
    plt.show()
    
def test_relection_geometry_multiple_dip_and_azimuth(): #TODO: Check again not working properly with the reflection coefficients
    file = odir + 'test_reflection_geometry_multiple_dip_and_azimuth.h5'
    ant = Antenna()
    ant.set_profile(name_profile='single',
                    transmitters=[[0,0,0]],
                    receivers=[[0,0,0]],
                    orient_receivers=[[0,0,1]],
                    orient_transmitters=[[0,0,1]],
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, 140, 1001))
    source_parameters = [0.4857619,
                         2.03456272,
                         0,
                         0.14400915,
                         0,
                         # 10.01662033]
                         0]
    sou.set_source_params(a=source_parameters[0],
                          c=source_parameters[1],
                          loc=source_parameters[2],
                          scale=source_parameters[3],
                          ph=source_parameters[4],
                          t=source_parameters[5])
    sou.create_source()
    sou.set_center_frequency(0.1)
    sou.export_to_hdf5(file, overwrite=True)

    from fracwave.utils.help_decorators import convert_meter
    max_element_size = 0.1
    print(f'Element max size = {convert_meter(max_element_size)}')

    azimuth = np.arange(0,180.1, 14)
    dip = np.arange(0, 180.1, 15)

    frac = FractureGeo()
    for i, dip_i in enumerate(dip):
        for j, azimuth_j in enumerate(azimuth):

            v, f = frac.create_regular_squared_mesh(width=0.1,
                                                    length=0.1,
                                                    resolution=[2,2],
                                                    dip=dip_i,
                                                    azimuth=azimuth_j,
                                                    center=((2,0,0)))
            kwarg_properties = dict(aperture=0.01,
                                    electrical_conductivity=0,
                                    electrical_permeability=81)
            frac.set_fracture(name_fracture=f'azm:{azimuth_j}-dip:{dip_i}',
                              vertices=v,
                              faces=f,
                              **kwarg_properties)

    frac.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    # solv.mode = 'reflection'
    solv.mode = 'reflection'
    solv._propagation_mode = 1
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 5  # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium

    solv.backend = 'numpy'
    solv.engine = 'tensor_trace'
    solv._old_solver = False
    solv._scaling = True

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=False, overwrite=True)

    assert solv.name_fractures == frac.name_fractures, 'The order of the fractures is not the same'

    # Check the angle between the fracture element and the incoming field
    a = np.cos(solv.file_read('simulation/theta', sl=0))
    angle = np.zeros((len(dip), len(azimuth)))
    for i, dip_i in enumerate(dip):
        for j, azimuth_j in enumerate(azimuth):
            angle[i, j] = a[i * len(azimuth) + j]

    extent = (azimuth.min(), azimuth.max(), dip.max(), dip.min())
    plt.imshow(angle, aspect='auto', extent=extent)
    plt.xlabel('Azimuth')
    plt.ylabel('dip')
    plt.title('Reflection angle')
    plt.colorbar()
    plt.show()

    # Check the angle of the new polarized orientation
    b = np.linalg.norm(solv.file_read('simulation/new_orient_dipole', sl=0), axis=-1)
    orient = np.zeros((len(dip), len(azimuth)))
    for i, dip_i in enumerate(dip):
        for j, azimuth_j in enumerate(azimuth):
            orient[i, j] = b[i * len(azimuth) + j]

    extent = (azimuth.min(), azimuth.max(), dip.max(), dip.min())
    plt.imshow(orient, aspect='auto', extent=extent)
    plt.xlabel('Azimuth')
    plt.ylabel('dip')
    plt.title('Polarization amplitude')
    plt.colorbar()
    plt.show()

    # This is how it should look like
    plt.imshow(orient*angle, aspect='auto', extent=extent)
    plt.xlabel('Azimuth')
    plt.ylabel('dip')
    plt.title('Reflection angle and Polarization amplitude')
    plt.colorbar()
    plt.show()

    ## Now calculate the reflection coefficients
    frequencies = solv.file_read('source/frequency')
    mask_frequencies = np.zeros(solv.nfrequencies, dtype=bool)
    mask_frequencies[np.where(np.absolute(solv.file_read('source/source')) / np.absolute(solv.file_read('source/source')).max() >
                  1e-4)[0]] = True  # Don't Look at anything below 0.01% of the energy of the frequencies

    omega = frequencies[mask_frequencies] * 2*np.pi
    # omega = frequencies * 2*np.pi

    k1 = solv.wavenumber(omega, solv.rock_epsilon, solv.rock_sigma)

    fracture_epsilon=frac.fractures['elec_permeability'].to_numpy().astype(float)
    fracture_sigma=frac.fractures['elec_conductivity'].to_numpy().astype(float)
    aperture=frac.fractures['aperture'].to_numpy().astype(float)

    k2 = solv.wavenumber(omega[None, :],  # To fit nelement x frequencies
                              fracture_epsilon[:, None],
                              fracture_sigma[:, None])

    theta = solv.file_read('simulation/theta', sl=0)

    val = solv.reflection_coefficients(k1=k1[None, :],
                              k2=k2,
                              # aperture=aperture[:, None],
                             theta=theta[:, None], )
    # val[np.abs(val) > 0.98] = 0


    R = np.zeros((len(dip), len(azimuth)))
    for i, dip_i in enumerate(dip):
        for j, azimuth_j in enumerate(azimuth):
            R[i, j] = np.abs(val[i * len(azimuth) + j, 40])

    extent = (azimuth.min(), azimuth.max(), dip.max(), dip.min())
    plt.imshow(R, aspect='auto', extent=extent)
    plt.xlabel('Azimuth')
    plt.ylabel('dip')
    plt.title('Reflection coefficients')
    plt.colorbar()
    plt.show()

    # val /= np.abs(val).max()
    # [plt.plot(frequencies[mask_frequencies], v) for v in val]; plt.show()
    # plt.plot(frequencies[mask_frequencies], val[2]); plt.show()


    #################
    # All together
    indiv_fracture = solv.file_read('simulation/individual_fracture_response', sl=0)
    time_response, time_vector = solv.get_ifft(indiv_fracture)
    max_amplitude = np.abs(time_response).max(1)
    energy = np.zeros((len(dip), len(azimuth)))
    for i, dip_i in enumerate(dip):
        for j, azimuth_j in enumerate(azimuth):
            energy[i,j] = max_amplitude[i*len(azimuth)+j]

    extent=(azimuth.min(), azimuth.max(), dip.max(), dip.min())
    plt.imshow(energy, aspect='auto', extent=extent)
    plt.xlabel('Azimuth')
    plt.ylabel('dip')
    plt.title('Reflected energy at different fracture orientations')
    plt.colorbar()
    plt.show() # TDDO: Not working

def test_incoming_field_on_sphere():
    file = odir + 'test_incoming_field_on_sphere.h5'
    ant = Antenna()
    ant.set_profile(name_profile='s',
                    transmitters=[[0,0,0]],
                    receivers=[[0,0,0]],
                    orient_receivers=[[0,0,1]],
                    orient_transmitters=[[0,0,1]],
                    overwrite=True)
    ant.export_to_hdf5(file, overwrite=True)

    sou = SourceEM()
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 599))
    sou.set_source_params(a=6.9,
                          c=1,
                          loc=0,
                          scale=0.5,
                          ph=0,
                          t=1)
    sou.create_source()
    sou.export_to_hdf5(file, overwrite=True)
    
    import pyvista as pv
    sphere = pv.Sphere(radius=0.75, center=(0,0,0), direction=(0,0,1), theta_resolution=30, phi_resolution=30)

    frac = FractureGeo()
    vertices, faces = frac.extract_vertices_and_faces_from_pyvista_mesh(sphere)
    kwargs_electric_properties = dict(aperture=0.1,
                                      electrical_conductivity=0,  # in S/m
                                      electrical_permeability=81)  # Unitless

    frac.set_fracture(name_fracture='sphere',
                      vertices=vertices,
                      faces=faces,
                      overwrite=True,
                      **kwargs_electric_properties)
    frac.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    solv.mode = 'incoming_field'
    solv._propagation_mode = 1
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 8.4  # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'
    solv._old_solver = False
    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=True, overwrite=True).numpy()
    # field = solv.file_read('simulation/fracture_field').numpy()
    field = freq
    import pyvistaqt as pvqt
    p = pvqt.BackgroundPlotter()
    sphere['field'] = field[0]
    p.add_mesh(sphere, name='mesh', scalars='field', show_edges=False)
    p.show_bounds()
    p.show_axes()

    plt.imshow(p.image)
    plt.show()

def test_fracture_field_and_incomming_field():
    file = odir + 'test_fracture_field_and_incomming_field.h5'
    xyz = np.arange(-0.4, 0.41, 0.1)
    pos = np.zeros((len(xyz)*2, 3))

    pos[:len(xyz), 0] = xyz
    pos[len(xyz):, 1] = xyz
    pos[:, 2] = 0.1
    orient = np.repeat([[0,1,0]], len(pos), axis=0)

    ant = Antenna()
    ant.set_profile(name_profile='1',
                    transmitters=pos,
                    receivers=pos,
                    orient_receivers=orient,
                    orient_transmitters=orient,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)

    sou = SourceEM()
    sou.type = 'generalgamma'
    sou.set_time_vector(np.linspace(0, 15, 599))
    sou.set_source_params(a=6.9,
                          c=1,
                          loc=0,
                          scale=0.5,
                          ph=0,
                          t=1)
    sou.create_source()
    sou.export_to_hdf5(file, overwrite=True)

    c0 = 299_792_458  # Speed of light in m/s
    rock_epsilon = 8.4  # Relative permitivity of the medium (dielectric constant)
    velocity = c0 * 1e-9 / np.sqrt(rock_epsilon)  # To convert in m/ns
    max_element_size = velocity / (sou.center_frequency * 2)
    print(f'Element max size = {convert_meter(max_element_size)}')

    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=1, length=1, dip=0, azimuth=0, resolution=(10, 10),
                                            center=(0, 0, 0))
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=2)
    kwargs_electric_properties = dict(aperture=0.1,
                                      electrical_conductivity=0.01,  # in S/m
                                      electrical_permeability=81)  # Unitless

    frac.set_fracture(name_fracture='cube',
                      vertices=vertices,
                      faces=faces,
                      overwrite=True,
                      **kwargs_electric_properties)

    frac.export_to_hdf5(file, overwrite=True)

    solv = FracEM()
    solv.mode = 'incoming_field'
    solv._propagation_mode = 1
    solv._green_function_fields = None #['FF']
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 8.4  # Electrical permittivity of the medium
    solv.rock_sigma = 0.01  # Electrical conductivity of the medium

    solv.backend = 'cupy'
    solv.engine = 'tensor_trace'

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=True, overwrite=True)
    if solv.backend == 'cupy':
        freq = freq.get()
    vertices = frac.vertices
    midpoints = frac.midpoints
    extent = (vertices[:,0].min(), vertices[:,0].max(),
              vertices[:,1].min(), vertices[:,1].max(),
              #vertices[:,2].min(), vertices[:,2].max(),
              )

    positions_antenna = np.mean((ant.profiles[['Tx', 'Ty', 'Tz']].to_numpy(),
                   ant.profiles[['Rx', 'Ry', 'Rz']].to_numpy()), axis=0)

    i = 5
    # infor = ant.profiles.iloc[i]

    # xy = np.mean((infor[['Tx', 'Ty']].to_numpy(),
    #                infor[['Rx', 'Ry']].to_numpy()), axis=0)


    mask_frequencies = np.zeros(solv.nfrequencies, dtype=bool)
    mask_frequencies[np.where(np.absolute(solv.file_read('source/source').get()) / np.absolute(solv.file_read('source/source').get()).max() >
              1e-4)[0]]=True

    frequencies = solv.file_read('source/frequency').get()[mask_frequencies]

    wavelength = solv.velocity / frequencies

    def fresnel_zone(wavelength, distance, n=1):
        return 0.5 * np.sqrt(n*np.einsum('d,w->dw', distance, wavelength))

    fz = fresnel_zone(wavelength, positions_antenna[:,2], n=3)

    # ff2 = solv.file_read('simulation/fracture_field').get()
    ff = freq
    from matplotlib.patches import Circle

    field_at_fracture = ff[i]
    field_at_fracture /= np.abs(field_at_fracture).max()
    # field_at_fracture = field_at_fracture.reshape((84,84))

    # field_at_fracture = np.fliplr(field_at_fracture.reshape((84,84))).T

    # ax.imshow(aperture_field, interpolation='None', extent=extent, cmap='viridis')
    # im = ax.imshow(field_at_fracture, interpolation='None', extent=extent, cmap='viridis')
    fig, ax = plt.subplots()
    sc0 = ax.scatter(midpoints[:,0], midpoints[:,1], c=field_at_fracture, s=35, cmap='viridis')
    plt.show()

    fig, ax = plt.subplots()
    sc0 = ax.scatter(midpoints[:, 0], midpoints[:, 1], c=field_at_fracture, s=35, cmap='viridis')

    sc1 = ax.scatter(positions_antenna[:, 0], positions_antenna[:, 1], s=10, color='black')

    sc2 = ax.scatter(positions_antenna[i, 0], positions_antenna[i, 1], c='red', s=100, marker='*')
    circle = [Circle((positions_antenna[i, 0], positions_antenna[i, 1]), j, fill=False, color='red', linewidth=2) for j
              in fz[i]]
    cir = [ax.add_patch(cir) for cir in circle]
    plt.show()

def test_fresnel_zone_mask_elements(): #TODO: Not for testing
    file = odir + 'test_fresnel_zone.h5'

    orientation = np.asarray([[0, 0, 1],
                              [0, 1, 1],
                              [0, -1, 1],
                              [1, 0, 1],
                              [1, 1, 1],
                              [1, -1, 1],
                              [-1, 0, 1],
                              [-1, 1, 1],
                              [-1, -1, 1],
                              ])
    tx = rx = np.repeat([[0, 0, 0]], len(orientation), axis=0)

    ant = Antenna()
    ant.set_profile(name_profile='Prop1',
                    transmitters=tx,
                    receivers=rx,
                    orient_receivers=orientation,
                    orient_transmitters=orientation,
                    overwrite=True)

    ant.export_to_hdf5(file, overwrite=True)
    # ant.plot(backend='mpl', show=True)

    sou = SourceEM()
    sou.set_time_vector(np.linspace(0, 250, 1001))
    sou.type = 'ricker'
    sou.set_center_frequency(0.1)
    sou.create_source()
    # sou.plot_waveforms_complete().show()
    # sou.plot_waveforms_zoom().show()
    sou.export_to_hdf5(file, overwrite=True)
    max_element_size = (0.1234 / sou.center_frequency) * 0.25

    frac = FractureGeo()
    v, f = frac.create_regular_squared_mesh(width=20,
                                            length=20,
                                            resolution=[5, 5],
                                            dip=90,
                                            azimuth=90,
                                            center=[10,0,0])
    grid, vertices, faces = frac.remesh(points=v, max_element_size=max_element_size, plane=0)
    kwarg_properties = dict(aperture=0.05,
                            electrical_conductivity=0.01,
                            electrical_permeability=81)
    frac.set_fracture(name_fracture='compare_fresnel',
                      vertices=vertices,
                      faces=faces,
                      plane=0,
                      **kwarg_properties)
    # frac.plot_geometry()
    frac.export_to_hdf5(file, overwrite=True)


    midpoints = frac.midpoints
    tx = ant.Transmitter
    rx = ant.Receiver

    min_distance_t, min_position_t, surface = frac.get_position_reflection_point(midpoints, tx, rx)
    tx_distance = np.linalg.norm(tx - min_position_t, axis=-1)
    surf = frac.get_surface()
    Tx, Rx, Tx_arrows, Rx_arrows = ant.get_geometry(show=False)
    from bedretto import model
    p = model.ModelPlot()
    p.add_object(surf, name='mesh', show_edges=True, color='yellow')
    p.add_object(Tx, name='Tx', color='red')
    p.add_object(Tx_arrows, name='Tx_arrows', color='red')
    p.add_object(Rx, name='Rx', color='blue')

    p.add_object(min_position_t, name='reflection', color='green')

    solv = FracEM()
    solv.rock_epsilon = 5.9
    solv.rock_sigma = 0.0
    solv.open_file(file)

    from fracwave.geometry.vector_calculus import create_fresnel_cone_of_points, find_points_near_surface

    f = sou.center_frequency  # Hz
    v = solv.velocity
    wavelength = v / f
    n_zone = 4

    cones, distances = create_fresnel_cone_of_points(center=ant.Transmitter,
                                                     orientation=ant.orient_Transmitter,
                                                     distance=tx_distance,
                                                     wavelength=wavelength,
                                                     n_zone=n_zone)

    intersection, mask = find_points_near_surface(midpoints, cones, minimum_distance=wavelength * 0.25)
    distances_geometry = [np.linalg.norm(intersection[i] - tx[i], axis=-1) for i in range(len(cones))]

    p.add_object(intersection[8], name='cone', color='yellow')
    p.add_object(intersection[4], name='cone4', color='red')
    p.add_object(cones[8][::10], name='cone', color='black', opacity=0.1)




    # solv = FracEM()
    solv.mode = 'incoming_field'
    solv._fast_calculation_incoming_field = False
    solv.rock_epsilon = 5.9  # Electrical permittivity of the medium
    solv.rock_sigma = 0  # Electrical conductivity of the medium
    solv.backend = 'torch'
    solv.engine = 'tensor_trace'

    solv.open_file(file)
    freq = solv.forward_pass(mask_frequencies=True, overwrite=True)

    freq = solv.file_read('simulation/prop_full')[..., 2]
    time_response, time_vector = solv.get_ifft(freq, pad=len(time_gprMax) - solv.file_read_attribute('source/ntime'))
    norm_fracEM = np.abs(time_response).max()
    time_response /= norm_fracEM

    fig, AX = plt.subplots(solv.ntraces, 1, figsize=(10, 25), sharex=True)
    for i, ax in enumerate(AX):
        ax.plot(time_gprMax, response_gprMax[:,i,2], color='red', linestyle='--', label='gprMax')
        ax.plot(time_vector, time_response[i], color='blue', label='FracEM')
        ax.set_xlim((20, 48))
        ax.grid()
        ax.legend()
        ax.set_title(f'rx{i+1}')
    plt.show()

    return solv