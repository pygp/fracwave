import numpy as np
from fracwave.geometry.source_receiver_geometry import Antenna

def test_antenna():
    ant = Antenna()
    print(ant)

def test_set_antenna():
    Rx = np.random.randn(100,3)
    Tx = np.random.randn(100,3)

    orient_Rx = np.repeat(np.array([0,0,1])[:,None], 100, axis=1)
    orient_Tx = np.repeat(np.array([0,0,1])[:,None], 100, axis=1)

    ant = Antenna()
    ant.Rx = Rx.T
    ant.Rx = Rx
    ant.Tx = Tx.T
    ant.Tx = Tx
    ant.orient_Rx = orient_Rx
    ant.orient_Tx = orient_Tx

    print(ant.Rx.shape, ant.Tx.shape)

    # Should appear an error
    ant.Rx = np.random.randn(100, 100)
    ant.Tx = np.random.randn(100, 100)
    #---
    print(ant.Rx.shape)

def test_export_antenna():
    Rx = np.random.randn(10, 3)
    Tx = np.random.randn(10, 3)
    orient_Rx = orient_Tx = np.repeat(np.array([0, 0, 1])[:, None], 10, axis=1)

    ant = Antenna()
    ant.Rx = Rx
    ant.Tx = Tx
    ant.orient_Rx = orient_Rx
    ant.orient_Tx = orient_Tx

    ant.export_to_hdf5(overwrite=True)

def test_load_antenna():
    ant = Antenna()
    ant.load_hdf5()
    ant.plot(backend='mpl')

